<?php
//jimport('joomla.application.component.controller');
$pop_model_path = JPATH_ROOT.'/components/'.'com_propertylisting/models';
JModelLegacy::addIncludePath( $pop_model_path );
$pop_model =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

// language loading
  $user = JFactory::getUser();
  $session = JFactory::getSession();
  $session->set('user', new JUser($user->id));

      $language =& JFactory::getLanguage();
      $extension = 'com_nrds';
      $base_dir = JPATH_SITE;
      $language_tag = JFactory::getUser()->currLanguage;
      $language->load($extension, $base_dir, $language_tag, true);
  // language loading

$uid = JFactory::getUser()->id;
$db = JFactory::getDbo();
$query= $db->getQuery(true);
$query->select('lastActivityLogVisit')
->from('#__users')
->where('id = '.$uid);
$db->setQuery($query);
$lastvisit = $db->loadObject()->lastActivityLogVisit;

$query= $db->getQuery(true);
$query->select('count(*) as  count')
->from('#__activities')
->where('user_id = '.$uid." and date > '".$lastvisit."'");
$db->setQuery($query);
$activity_count = $db->loadObject()->count;

$query= $db->getQuery(true);
$query->select('count(*) as count')
->from('#__pocket_listing')
->where('user_id = '.$uid.' and closed = 0 and sold = 0');
$db->setQuery($query);
$listingcount = $db->loadObject()->count;

$query= $db->getQuery(true);
$query->select('*')
->from('#__user_registration')
->where('email = \''.JFactory::getUser()->email.'\'');
$db->setQuery($query);
$user = JFactory::getUser();
$user_reg = $db->loadObject();
$query= $db->getQuery(true);
$query->select('count(1) as count')
->from('#__request_network')
->where('user_id ='.JFactory::getUser()->id. ' AND status=1');
$db->setQuery($query);
$network_count = $db->loadObject()->count;

/*$query= $db->getQuery(true);
$query->select('count(*) as count')
->from('#__referral r')
->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id')
->where('r.agent_a = \''.JFactory::getUser()->id.'\' OR '.' r.agent_b = \''.JFactory::getUser()->id.'\' AND ba.buyer_id != "" ');
$db->setQuery($query);
$referrals_count = $db->loadObject()->count;*/

$refins=$pop_model->get_referral_in(JFactory::getUser()->id);
$refout=$pop_model->get_referral_out(JFactory::getUser()->id);
$referrals_count = (count($this->refin))+(count($this->refout));

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('*')->from('#__buyer')->where(array_merge(array('agent_id = '.JFactory::getUser()->id.' AND buyer_type != "Inactive"')));
$buyers = $db->setQuery($query)->loadObjectList();
foreach ($buyers as $buyer){
	$query = $db->getQuery(true);
	$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id );
	$buyer->needs = $db->setQuery($query)->loadObjectList();
}

$db = JFactory::getDbo();
$query = $db->getQuery(true);
if($user_reg->brokerage){
	$query->select('broker_name')->from('#__broker')->where('broker_id = '.$user_reg->brokerage);
	$db->setQuery($query);
	$user_reg->brokerage_label = $db->loadObject()->broker_name;
}
if(isset($_GET) && isset($_GET['task']) && $_GET['task']=="profile"){
	$label = JText::_('COM_SIDEBAR_EPROF');
	$route = JRoute::_('index.php?option=com_userprofile&task=edit');
}
else{
	$label = JText::_('COM_SIDEBAR_VPROF');
	$route = JRoute::_('index.php?option=com_userprofile&task=profile');
}




?>

<div class="sidebar left">
	<!-- start sidebar -->
	<div class="blocks">
		<a 
			class="user-avatar left" 
			href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile'); ?>" 
			style="width: 60px; height: 60px; overflow: hidden"> 
		<?php if ($user_reg->image!=="") { ?>
		<img style="width:60px;" src="<?php echo strpos($user_reg->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $user_reg->image).'?'.microtime(true) :  JURI::base().'/uploads/thumb_'.$user_reg->image.'?'.microtime(true)?>" />
		<?php } else { ?>
		<img style="width:60px;" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" />
		<?php } ?></a>
		<span class="user_name"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile'); ?>" ><?php echo stripslashes_all($user->name); ?></a></span><br />
		<span class="realty" style="line-height:16px"><?php echo $user_reFg->brokerage_label; ?> </span>
	</div>
	<?php
		$buyerctr = 0;
		$new = 0;
		foreach($buyers as $buyer){
			/////if(!empty($buyer->listingcount)){
				$buyerctr+=$buyer->listingcount;
				//	$buyerctr+=	$pop_model->countlisting($buyer->buyer_id);
				$new +=$buyer->listingcount_new;
			//}
		}
	?>
	<div class="blocks">
		<ul class="user-menu">
			<li class="default-li">
			<a href="<?php echo JRoute::_('index.php?option=com_activitylog'); ?>"> <?php echo JText::_('COM_SIDEBAR_ACT') ?>  <?php if($activity_count != 0) { ?> <span class="activity_number"> <?php echo "(".$activity_count.")"; ?> </span> <?php }?> </a>
			</li>
			<li class="view-li">
			<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=view'); ?>"><?php echo JText::_('COM_SIDEBAR_POP') ?>
			<span class="pocket_number"><?php echo ($listingcount) ? "(".$listingcount.")" : ""; ?></span>
			</a>
			</li>
			<li><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=buyers'); ?>" class="r_buyers"><?php echo JText::_('COM_SIDEBAR_BUY') ?> <span class="referrals_number buyerCount" <?php if($new) {?>style="font-weight:bold" <?php } ?>><?php if($buyerctr != 0){ echo "(".$buyerctr.")";} ?></span></a>
			</li>
			<li class="referrals-li"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=referrals'); ?>"> <?php echo JText::_('COM_SIDEBAR_REF') ?> <span class="referrals_number" ><?php echo ($referrals_count) ? "(".$referrals_count.")" : ""; ?></span>
			</a>
			</li>
            <!--<li class="contacts-li"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=contacts'); ?>">My Network <span class="referrals_number"><?php if($network_count != 0){ echo "(".$network_count.")";} ?></span> </a> 
			</li>-->
		</ul>
	</div>

	<div class="blocks">
		<ul class="user-menu">
			<li>
			<a href="<?php echo $route; ?>" class="view-profile"><?php echo $label;?></a>
			</li>
			<li>
			<a 
				id="accsettings"
			    href="javascript:void(0)"
				class="account-setting"><?php echo JText::_('COM_SIDEBAR_SET') ?></a>
				<ul class="user-menu-low">
					<li><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=contact'); ?>" class="text-link contact-li"><?php echo JText::_('COM_SIDEBAR_CON') ?></a></li>
					<li><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=broanddesig'); ?>" class="text-link broanddesig-li"><?php echo JText::_('COM_SIDEBAR_BRKDES') ?></a></li>
					<li><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=emailsnotif'); ?>" class="text-link emailsnotif-li"><?php echo JText::_('COM_SIDEBAR_NOT') ?></a></li>
					<!--<li><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=setting'); ?>" class="text-link setting-li">POPs&#8482; Settings</a></li>-->
					<li><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=membership'); ?>" class="text-link membership-li"><?php echo JText::_('COM_SIDEBAR_MEM') ?></a></li>
					<li><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=changepass'); ?>" class="text-link changepass-li"><?php echo JText::_('COM_SIDEBAR_CPWD') ?></a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<div class="clear-float"></div>


<?php
	$menulow = array('changepass', 'contact', 'broanddesig', 'emailsnotif', 'setting', 'membership');
	if(in_array($_GET['task'],$menulow)){
?>
	<script>
		jQuery('.user-menu-low').show()
		jQuery('.<?php echo $_GET["task"]; ?>-li').css('font-weight','bold');
	</script>
<?php
	}
	else{
?>
		<script>
			<?php if(!isset($_GET['uid']) || (isset($_GET['uid']) && $_GET['uid'] == JFactory::getUser()->id)) { ?>
			jQuery('.<?php echo $_GET["task"]; ?>-li a').css('font-weight','bold');
			<?php } ?>
		</script>
<?php
	}
?>
<!-- end sidebar -->
