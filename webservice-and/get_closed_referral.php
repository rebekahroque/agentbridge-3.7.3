<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $referral_id = $_GET["referral_id"];

    $query = "SELECT `tbl_closed_referrals`.* 
    FROM `tbl_closed_referrals`
    WHERE `tbl_closed_referrals`.`referral_id` = '$referral_id'";

    $result = $mysqli->query($query) or die($mysqli->error);
    
    $num = $result->num_rows;
    
    $mysqli->close();
    
    $response = array();
    if($num == 0) {
        $response = array('status'=>0, 'message'=>"Failed to Get Closed Referral", 'data'=>array());
    }
    else {
        $rows = array();
        while ($r = $result->fetch_assoc())
        {
            $rows[] = $r;
        }
        
        $response = array('status'=>1, 'message'=>"Successfully Get Closed Referral", 'data'=>$rows);
    }
    
    echo json_encode($response);

?>