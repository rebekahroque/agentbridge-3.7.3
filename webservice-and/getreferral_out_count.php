<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



$user_id = $_GET["user_id"];

$query = "SELECT `tbl_referral`.`referral_id`
FROM `tbl_referral`
LEFT JOIN `tbl_users` ag_a ON `tbl_referral`.`agent_a` = ag_a.`id`
LEFT JOIN `tbl_users` ON `tbl_referral`.`agent_b` = `tbl_users`.`id`
    LEFT JOIN (
               SELECT `tbl_buyer`.`buyer_id`, `tbl_buyer`.`name` AS `client_name`, `tbl_buyer_email`.`email`, `tbl_buyer_mobile`.`number`, `tbl_buyer_address`.`address_1`, `tbl_buyer_address`.`address_2`, `tbl_buyer_address`.`city`, `tbl_buyer_address`.`zip`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_zones`.`zone_code`, `tbl_zones`.`zone_name`
               FROM `tbl_buyer`
               LEFT JOIN `tbl_buyer_email` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_email`.`buyer_id`
               LEFT JOIN `tbl_buyer_mobile` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_mobile`.`buyer_id`
               LEFT JOIN `tbl_buyer_address` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_address`.`buyer_id`
               LEFT JOIN `tbl_countries` ON `tbl_buyer_address`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_buyer_address`.`state` = `tbl_zones`.`zone_id` AND `tbl_buyer_address`.`country` = `tbl_zones`.`zone_country_id`
               ) `buyer_data` ON `tbl_referral`.`client_id` = `buyer_data`.`buyer_id`
LEFT JOIN (
			SELECT `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_code` AS `state_code`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_user_registration`.`country` AS `country_id`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`image`
			FROM `tbl_user_registration`
			INNER JOIN `tbl_users` ON `tbl_user_registration`.`email` = `tbl_users`.`email`
			LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
           LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
			) AS `user_data` ON `tbl_referral`.`agent_b` = `user_data`.`user_id`
WHERE `tbl_referral`.`agent_a`='$user_id' ";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;



$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Referrals Found", 'data'=>array());
}
else {
	$rows = array();
	 $rows_u = array();

  while ($r = $result->fetch_assoc())
  {
    $rows[] = $r;
  }

	$response = array('status'=>1, 'message'=>"Found Referrals", 'data'=>count($rows));
}

$mysqli->close();
echo json_encode($response);

?>