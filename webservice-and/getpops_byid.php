<?php
    
    include('_dbconn.php');

      //$user = JFactory::getUser();
  ////$session = JFactory::getSession();
  //$session->set('user', new JUser($user->id));
  $language = JFactory::getLanguage();
  $extension = 'com_nrds';
  $base_dir = JPATH_SITE;
  // $language_tag = JFactory::getUser()->currLanguage;
  $language_tag = "english-US";
  $language->load($extension, $base_dir, $language_tag, true);
    
  $con = $mysqli = new mysqli("localhost", $username, $password, $database);

  $mysqli->set_charset('utf8');



    $user_id = $_GET["user_id"];
    $listing_id = $_GET["listing_id"];
    $pl_id=$listing_id;

 /*   $query = "SELECT `tbl_zones`.`zone_name` AS state_name,`tbl_countries`.`countries_iso_code_3` AS country_name,`tbl_pocket_listing`.`country`, `tbl_country_currency`.`currency`,`tbl_country_currency`.`symbol`,`tbl_users`.name AS username, `tbl_pocket_listing`.`sold`, `tbl_user_sales`.`verified_2013` as v2013,`tbl_user_sales`.`verified_2014` as v2014,`tbl_pocket_listing`.`user_id`, `tbl_pocket_listing`.`listing_id`, `tbl_pocket_listing`.`property_type` AS `type_property_type`, `tbl_pocket_listing`.`property_name`, `tbl_property_type`.`type_name`, `tbl_pocket_listing`.`sub_type`, `tbl_property_sub_type`.`name` AS `sub_type_name`, `tbl_property_price`.`price_type`, `tbl_property_price`.`price1`, `tbl_property_price`.`price2`, `tbl_property_price`.`disclose`, `tbl_pocket_listing`.`expiry`, `tbl_pocket_listing`.`user_id`, `tbl_users`.`name`, `tbl_pocket_listing`.`city`, `tbl_pocket_listing`.`zip`, `tbl_pocket_listing`.`state`, `tbl_pocket_listing`.`bedroom`, `tbl_pocket_listing`.`bathroom`, `tbl_pocket_listing`.`unit_sqft`, `tbl_pocket_listing`.`view`, `tbl_pocket_listing`.`style`, `tbl_pocket_listing`.`year_built`, `tbl_pocket_listing`.`pool_spa`, `tbl_pocket_listing`.`condition`, `tbl_pocket_listing`.`garage`, `tbl_pocket_listing`.`units`, `tbl_pocket_listing`.`cap_rate`, `tbl_pocket_listing`.`grm`, `tbl_pocket_listing`.`occupancy`, `tbl_pocket_listing`.`type`, `tbl_pocket_listing`.`listing_class`, `tbl_pocket_listing`.`parking_ratio`, `tbl_pocket_listing`.`ceiling_height`, `tbl_pocket_listing`.`stories`, `tbl_pocket_listing`.`room_count`, `tbl_pocket_listing`.`type_lease`, `tbl_pocket_listing`.`type_lease2`, `tbl_pocket_listing`.`available_sqft`, `tbl_pocket_listing`.`lot_sqft`, `tbl_pocket_listing`.`lot_size`, `tbl_pocket_listing`.`bldg_sqft`, `tbl_pocket_listing`.`term`, `tbl_pocket_listing`.`furnished`, `tbl_pocket_listing`.`pet`, `tbl_pocket_listing`.`possession`, `tbl_pocket_listing`.`zoned`, `tbl_pocket_listing`.`bldg_type`, `tbl_pocket_listing`.`features1`, `tbl_pocket_listing`.`features2`, `tbl_pocket_listing`.`features3`, `tbl_pocket_listing`.`setting`, `tbl_pocket_listing`.`description` AS `desc`, `tbl_pocket_listing`.`closed`, `tbl_pocket_listing`.`date_created`, `tbl_pocket_listing`.`date_expired`, `tbl_property_price`.`pocket_id`, `pocket_images`.`images`, `tbl_permission_setting`.`selected_permission`
    
    FROM `tbl_pocket_listing`
    INNER JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
    INNER JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
    INNER JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
    LEFT JOIN `tbl_permission_setting` ON `tbl_permission_setting`.`listing_id` = `tbl_pocket_listing`.`listing_id`
    LEFT JOIN `tbl_countries` ON `tbl_pocket_listing`.`country` = `tbl_countries`.`countries_id`
    LEFT JOIN `tbl_zones` ON `tbl_pocket_listing`.`state` = `tbl_zones`.`zone_id`
    LEFT JOIN `tbl_country_currency` ON `tbl_pocket_listing`.`currency` = `tbl_country_currency`.`currency`
    LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`
    LEFT JOIN `tbl_user_sales` ON `tbl_pocket_listing`.`user_id` = `tbl_user_sales`.`agent_id`
    LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
               FROM `tbl_pocket_images`
               GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
    WHERE `tbl_pocket_listing`.`listing_id` = '$listing_id' GROUP BY `tbl_pocket_listing`.`listing_id`";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;
*/



$langValues = $propertylist_model->checkLangFiles();
$individual = $propertylist_model->getPOPsById_model($listing_id,$user_id);
//$userDetails = $userprof_model->get_user_registration($user_id);

$num = count($individual);
//echo "<pre>";
//var_dump($individual);

$response = array();
if($num == 0) {
    $response = array('status'=>0, 'message'=>"No User Properties Found", 'data'=>array());
}
else {
    $rows = array();
/*  while ($r = $result->fetch_assoc())
    {
        $rows[] = $r;
    }*/

    $rows = $individual;

     foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='user_id'){
                 $pl_userId=$value2;
            }
            if($key2=='name'){
                $this_line['name']=stripslashes_all($value2);
            }
            if($key2=='username'){
                $this_line['username']=stripslashes($value2);
            }

            if($key2=='date_expired'){
                 $now = time();
                $timedifference=strtotime($value2)-$now;
                $this_line['expiry']=strval(round($timedifference/(60*60*24)));
            }

            if($key2 == 'selected_permission'){
                $this_line['req_access_per']= "1";
                    if($user_id == $pl_userId){
                        $this_line['selected_permission'] = "1";
                    } else {
                        if($pl_userId && $this_line['selected_permission'] != 1){
                               
                                if( $this_line['setting'] == 1) {
                                    $this_line['selected_permission'] = "1";    
                                    $this_line['req_access_per'] = "1";                                                             
                                } else {                                    
                                    if ($this_line['permission']=="1"){
                                        $this_line['selected_permission'] = "1";
                                        $this_line['req_access_per'] = "1";
                                    } else if ($this_line['permission']=="0"){
                                        $this_line['req_access_per'] = "pending_private";
                                    } else if ($this_line['permission']=="2"){
                                        $this_line['req_access_per'] = "denied_private";
                                    } else {
                                        $this_line['req_access_per'] = "request_private";
                                    }
                                }

                        } else {
                          //  $this_line['selected_permission'] = "1";
                        }

                    }
            }
            
            if($key2 == 'images'){
                  $db = JFactory::getDbo();
                  $query = $db->getQuery(true);
                  $query = "SELECT image FROM `tbl_pocket_images` WHERE `tbl_pocket_images`.`listing_id` = ".$this_line['listing_id']." ORDER BY order_image ASC,image_id DESC";
                  $db->setQuery($query);           
                 //$resultss = $db->execute();
                  $arra_images = $db->loadColumn();
                  $new_arr_images = implode(",", $arra_images);
                  $this_line['images'] = $new_arr_images;
                 // var_dump($new_arr_images);
            }

        }

        $this_line['show_button']= "false";
        $this_line['type_name'] = JText::_($this_line['proptype_name']);
        $this_line['sub_type_name'] = JText::_($this_line['subtype_name']);

        $current_user_info_currency = "USD";
        $current_user_info_symbol = "$";

        if($user_id!=$this_line['user_id']){
            if($this_line['currency']!=$current_user_info_currency){
              $ex_rates_con = $ex_rates->rates->{$this_line['currency']};
              $ex_rates_can = $ex_rates->rates->{$current_user_info_currency};
              if($this_line['currency']=="USD"){                  
                //$user->price1=$listing->price1 * $ex_rates_CAD;
             //   $user_details->ave_price_2012=$user_details->ave_price_2012 * $ex_rates_can;
                $this_line['price1'] = $this_line['price1'] * $ex_rates_can;
                if($this_line['price2']){
                    $this_line['price2'] = $this_line['price2'] * $ex_rates_can;
                }

              } else {
                //$user->price1=$listing->price1 / $ex_rates_CAD;
                //$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
                //$user_details->ave_price_2012=($user_details->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
                $this_line['price1'] = ($this_line['price1']  / $ex_rates_con) * $ex_rates_can;
                if($this_line['price2']){
                    $this_line['price2'] = ($this_line['price2']  / $ex_rates_con) * $ex_rates_can;
                }

              }
              $this_line['currency'] = $current_user_info_currency;
              $this_line['symbol'] = $current_user_info_symbol;
            }
        }


        if($user_id != $individual['user_id']){
            $country_used = $userDetails->country;
            $sqftMeasurement_used= $userDetails->sqftMeasurement;
        } else {
            $country_used = $individual['country'];
            $sqftMeasurement_used= $individual['sqftMeasurement'];
        }


        $sqM = "";
        
        if($sqftMeasurement_used=="sq. meters"){
            $getMeasurement = $propertylist_model->getSqMeasureByCountry($this_line['country']);
            foreach ($getMeasurement['bldg'] as $key => $value) {
                if($individual['bldg_sqft']){
                    if($individual['bldg_sqft'] == $value[1]){
                        $individual['bldg_sqft']=$value[0];
                    }
                }
                if($individual['available_sqft']){
                    if($individual['available_sqft'] == $value[1]){
                        $individual['available_sqft']=$value[0];
                    }
                    
                }
                if($individual['unit_sqft']){
                    if($individual['unit_sqft'] == $value[1]){
                        $individual['unit_sqft']=$value[0];
                    }
                    
                }
            }

            foreach ($getMeasurement['lot'] as $key => $value) {
                # code...
                if($individual['lot_sqft']){
                    if($individual['lot_sqft'] == $value[1]){
                        $individual['lot_sqft']=$value[0];
                    }
                    
                }
                if($individual['lot_size']){
                    if($individual['lot_size'] == $value[1]){
                        $individual['lot_size']=$value[0];
                    }
                    
                }
            }

            $sqM = "_M";
        }

        $pops_feats = $propertylist_model->getPOPsDetails($individual[0],$sqM,$individual['sub_type'],$langValues);

        foreach ($pops_feats['feats_array'] as $key => $value) {
            # code...
            $arr_value= explode(": ", $value);
            if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type" && $arr_value[0]!="Term" && $arr_value[0]!="Possession"){
                $pops_feats['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
            } else {
                $pops_feats['feats_array_edited'][]=$value;
            }
            
        }

        $this_line['initial_feats'] =$pops_feats['initial_feats'];
        $this_line['feats_array'] =implode(", ", $pops_feats['feats_array_edited']);

        $rows_r[]=$this_line;
        $rows_u = array_reverse($rows_r);
    }
    
    $response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$rows_u);
}
$mysqli->close();
echo json_encode($response);

?>