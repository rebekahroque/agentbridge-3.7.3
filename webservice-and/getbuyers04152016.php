<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $user_id = $_GET["user_id"];
    $buyer_id = $_GET["buyer_id"];


    $query = "SELECT DISTINCT `tbl_property_sub_type`.name AS sub_name,`tbl_property_type`.type_name AS ptype_name,`tbl_buyer`.`listingcount_new` as new_pops, `tbl_buyer`.`buyer_id`, `tbl_buyer_needs`.`listing_id`, `tbl_buyer`.`name`, `tbl_buyer`.`buyer_type`, `tbl_buyer`.`price_type`, `tbl_buyer`.`price_value`, `tbl_buyer`.`desc`, `tbl_buyer`.`listingcount`, `saved_list`.`saved_pops`, `tbl_buyer`.`hasnew`, `tbl_buyer`.`hasnew_2`, `tbl_buyer`.`contact_type`, `tbl_buyer`.`contact_method`, `tbl_buyer_needs`.`zip`, `tbl_buyer_needs`.`property_name`, `tbl_buyer_needs`.`city`, `tbl_buyer_needs`.`state`, `tbl_buyer_needs`.`property_type`, `tbl_buyer_needs`.`sub_type`, `tbl_buyer_needs`.`bedroom`, `tbl_buyer_needs`.`bathroom`, `tbl_buyer_needs`.`unit_sqft`, `tbl_buyer_needs`.`view`, `tbl_buyer_needs`.`style`, `tbl_buyer_needs`.`year_built`, `tbl_buyer_needs`.`pool_spa`, `tbl_buyer_needs`.`condition`, `tbl_buyer_needs`.`garage`, `tbl_buyer_needs`.`units`, `tbl_buyer_needs`.`grm`, `tbl_buyer_needs`.`cap_rate`, `tbl_buyer_needs`.`occupancy`, `tbl_buyer_needs`.`type`, `tbl_buyer_needs`.`type_lease`, `tbl_buyer_needs`.`type_lease2`, `tbl_buyer_needs`.`listing_class`, `tbl_buyer_needs`.`parking_ratio`, `tbl_buyer_needs`.`ceiling_height`, `tbl_buyer_needs`.`stories`, `tbl_buyer_needs`.`room_count`, `tbl_buyer_needs`.`available_sqft`, `tbl_buyer_needs`.`lot_size`, `tbl_buyer_needs`.`lot_sqft`, `tbl_buyer_needs`.`bldg_sqft`, `tbl_buyer_needs`.`term`, `tbl_buyer_needs`.`furnished`, `tbl_buyer_needs`.`pet`, `tbl_buyer_needs`.`possession`, `tbl_buyer_needs`.`zoned`, `tbl_buyer_needs`.`bldg_type`, `tbl_buyer_needs`.`features1`, `tbl_buyer_needs`.`features2`, `tbl_buyer_needs`.`features3`, `tbl_buyer_needs`.`setting`, `tbl_buyer_needs`.`closed`, `tbl_buyer_needs`.`expiry`
    FROM `tbl_buyer`
    LEFT JOIN `tbl_buyer_needs` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_needs`.`buyer_id`
    LEFT JOIN `tbl_property_sub_type` ON `tbl_property_sub_type`.`sub_id` = `tbl_buyer_needs`.`sub_type`
    LEFT JOIN `tbl_property_type` ON `tbl_property_type`.`type_id` = `tbl_buyer_needs`.`property_type`
    LEFT JOIN (SELECT DISTINCT `tbl_buyer_saved_listing`.`buyer_id`, COUNT(DISTINCT listing_id) AS 'saved_pops' FROM `tbl_buyer_saved_listing` GROUP BY `tbl_buyer_saved_listing`.`buyer_id`) AS `saved_list` ON `saved_list`.`buyer_id` = `tbl_buyer`.`buyer_id`
    /*LEFT JOIN (SELECT `tbl_buyer_new_listing`.`buyer_id`, COUNT(*) AS 'new_pops' FROM `tbl_buyer_new_listing` GROUP BY `tbl_buyer_new_listing`.`buyer_id`) AS `new_list` ON `new_list`.`buyer_id` = `tbl_buyer`.`buyer_id`*/";

    if(isset($_GET["buyer_id"])){
        $query.=" WHERE `tbl_buyer`.`buyer_id`='$buyer_id'  ";
    } else{
        $query.=" WHERE `tbl_buyer`.`agent_id`='$user_id' AND `tbl_buyer`.`buyer_type` <> '' ";
        $query.="GROUP BY `tbl_buyer`.`buyer_id`";
        $query.="ORDER BY `tbl_buyer`.`buyer_type` ASC";
    }      

    

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
}

echo json_encode($response);

?>