<?php include('_dbconn.php');
    


//$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);

$con = $mysqli = new mysqli("localhost", $username, $password, $database);

$mysqli->set_charset('utf8');



$user_id = $_GET["user_id"];
$limit_one = $_GET["limit_one"];

$langValues = $propertylist_model->checkLangFiles();
$userDetails = $userprof_model->get_user_registration($user_id);

$now_date = date("Y-m-d",time());


//     public String popsId;
//     public String popsUserId;
//     public String popsUserName;
//     public String popsZip;
//     public String popsPropertyName;
//     public String popsPropertyTypeName;
//     public String popsSubPropTypeName;
//     public String popsCurrency;
//     public String popsSymbol;
//     public String popsPrices;
//     public String popsImages;
//     public String popsFeatures;
//     public String popsIsSold;
//     public String popsExpiry;

$query = "SELECT `tbl_pocket_listing`.`listing_id` AS `pops_id`, 
                 `tbl_pocket_listing`.`user_id` AS `pops_user_id`,
                 `tbl_users`.`name` AS `pops_user_name`,
                 `tbl_pocket_listing`.`zip` AS `pops_zip`,  
                 `tbl_pocket_listing`.`property_name` AS `pops_name`,  
                 `tbl_property_type`.`type_name` AS `pops_type_name`, 
                 `tbl_property_sub_type`.`name` AS `pops_subtype_name`, 
                 `tbl_pocket_listing`.`currency` AS `pops_currency`, 
                 `tbl_country_currency`.`symbol` AS `pops_symbol`,       
                 `tbl_property_price`.`price_type`, 
                 `tbl_property_price`.`price1`, 
                 `tbl_property_price`.`price2`, 
                 `tbl_property_price`.`disclose`, 
                 `tbl_pocket_listing`.`expiry` AS `pops_disclose`,
                 `tbl_pocket_listing`.`setting`,                
                 `tbl_pocket_listing`.`closed`, 
                 `tbl_pocket_listing`.`date_created`, 
                 `tbl_pocket_listing`.`date_expired`,
                 `tbl_property_price`.`pocket_id`,
                 `tbl_pocket_listing`.`sold` AS `pops_sold`,
                 `pocket_images`.`images`

FROM `tbl_pocket_listing`
LEFT JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
LEFT JOIN `tbl_country_currency` ON `tbl_country_currency`.`currency` = `tbl_pocket_listing`.`currency`
LEFT JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
LEFT JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`
LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
           FROM `tbl_pocket_images`
           GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
WHERE `tbl_pocket_listing`.`user_id`='$user_id' ORDER BY `tbl_pocket_listing`.`listing_id` DESC";
 $query .= " limit ".$limit_one.",3";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;


$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Properties Found", 'data'=>array());
}
else {
	$rows = array();
	$rows_u = array();
    while ($r = $result->fetch_assoc())
    {
        if(key($r)=='date'){
            
            
        }   

        $rows[] = $r;
    }
    foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {

               if($key2=='listing_id'){
                 $query2 = "SELECT `tbl_buyer`.`buyer_id`, `tbl_buyer_saved_listing`.`listing_id`, `tbl_buyer`.`name`
                  FROM `tbl_buyer`
                  LEFT JOIN `tbl_buyer_needs` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_needs`.`buyer_id`
                  LEFT JOIN `tbl_buyer_saved_listing` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_saved_listing`.`buyer_id`
                  LEFT JOIN (SELECT `tbl_buyer_saved_listing`.`buyer_id`, COUNT(*) AS 'saved_pops' FROM `tbl_buyer_saved_listing`  GROUP BY `tbl_buyer_saved_listing`.`buyer_id`) AS `saved_list` ON `saved_list`.`buyer_id` = `tbl_buyer`.`buyer_id`
                  LEFT JOIN (SELECT `tbl_buyer_new_listing`.`buyer_id`, COUNT(*) AS 'new_pops' FROM `tbl_buyer_new_listing` GROUP BY `tbl_buyer_new_listing`.`buyer_id`) AS `new_list` ON `new_list`.`buyer_id` = `tbl_buyer`.`buyer_id`";

              //    $query.=" WHERE `tbl_buyer_saved_listing`.`listing_id`!='$listing_id'";
                  $query2.=" WHERE `tbl_buyer`.`agent_id`='$user_id' AND `tbl_buyer`.`buyer_type` <> '' AND `tbl_buyer_saved_listing`.`listing_id`='$value2'";

                  $query2.=" GROUP BY `tbl_buyer`.`buyer_id`";

                $result2 = $mysqli->query($query2) or die($mysqli->error);

                $num2 = $result2->num_rows;


                
                if($num2 == 0) {
                    $this_line['show_button'] = "true";
                    $this_line['buyer_name'] = "none";
                }
                else {                  
                    $this_line['show_button'] = "true";
                    $this_line['buyer_name'] = "false";                    
                }

            }

            if($key2=='name'){
                $this_line['name']=stripslashes($value2);
            }
            if($key2=='date_expired'){
                $now = time();
                $timedifference=strtotime($value2)-$now;
                $this_line['expiry']=strval(round($timedifference/(60*60*24)));
            }
           
        }

        $this_line['req_access_per'] = "1";  
                          $this_line['selected_permission'] = "1";

       // $ex_rates_con = $ex_rates->rates->{$this_line['currency']};
       // $ex_rates_can = $ex_rates->rates->USD;
        
       // if($this_line['currency']!="USD"){            
        //    $this_line['price1']=($this_line['price1'] / $ex_rates_con)*$ex_rates_can;
       //     if($this_line['price2']){                
       //          $this_line['price2']=($this_line['price2'] / $ex_rates_con)*$ex_rates_can;
       //     }
       // }

        //$items->currency = $userDetails->currency;
        //$items->symbol = $userDetails->symbol;

         if($user_id != $this_line['user_id']){
            $country_used = $userDetails->country;
            $sqftMeasurement_used= $userDetails->sqftMeasurement;
        } else {
            $country_used = $this_line['country'];
            $sqftMeasurement_used= $this_line['sqftMeasurement'];
        }


        $sqM = "";
        
        if($sqftMeasurement_used=="sq. meters"){
            $getMeasurement = $propertylist_model->getSqMeasureByCountry($this_line['country']);
            foreach ($getMeasurement['bldg'] as $key => $value) {
                if($this_line['bldg_sqft']){
                    if($this_line['bldg_sqft'] == $value[1]){
                        $this_line['bldg_sqft']=$value[0];
                    }
                }
                if($this_line['available_sqft']){
                    if($this_line['available_sqft'] == $value[1]){
                        $this_line['available_sqft']=$value[0];
                    }
                    
                }
                if($this_line['unit_sqft']){
                    if($this_line['unit_sqft'] == $value[1]){
                        $this_line['unit_sqft']=$value[0];
                    }
                    
                }
            }

            foreach ($getMeasurement['lot'] as $key => $value) {
                # code...
                if($this_line['lot_sqft']){
                    if($this_line['lot_sqft'] == $value[1]){
                        $this_line['lot_sqft']=$value[0];
                    }
                    
                }
                if($this_line['lot_size']){
                    if($this_line['lot_size'] == $value[1]){
                        $this_line['lot_size']=$value[0];
                    }
                    
                }
            }

            $sqM = "_M";
        }

        $pops_feats = $propertylist_model->getPOPsDetails($this_line,$sqM,$this_line['sub_type'],$langValues);

         foreach ($pops_feats['feats_array'] as $key => $value) {
            # code...
            $arr_value= explode(": ", $value);
            if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type" && $arr_value[0]!="Term" && $arr_value[0]!="Possession"){
                $pops_feats['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
            } else {
                $pops_feats['feats_array_edited'][]=$value;
            }
            
        }

        $this_line['initial_feats'] =$pops_feats['initial_feats'];
        $this_line['feats_array'] =implode(", ", $pops_feats['feats_array_edited']);

        $this_line['pops_type_name'] = JText::_($this_line['pops_type_name']);
        $this_line['pops_subtype_name'] = JText::_($this_line['pops_subtype_name']);
        $rows_r[]=$this_line;
        $rows_u = $rows_r;
    }
    $response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$rows_u);
    $mysqli->close();
}

echo json_encode($response);

?>