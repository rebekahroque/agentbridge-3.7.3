<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $user_id = $_GET["user_id"];
    $activities_id = $_GET["activities_id"];

    $query = "SELECT `tbl_activities`.`pkId` AS `activities_id`,
    `tbl_activities`.`activity_type`,
    `tbl_activities`.`user_id`,
    `tbl_activities`.`date`,
    `tbl_activities`.`buyer_id`,
    `tbl_activities`.`other_user_id`,
    `status_referral_table`.`referral_id`,
    `status_referral_table`.`status` AS `referral_status`,
    `status_referral_table`.`response` AS `referral_response`,
    `status_referral_table`.`update_id` AS `referral_update_id`,
    `activity_user`.`name` AS `user_name`,
    `tbl_user_registration`.`image`,
    `buyer_data`.`client_name`, `buyer_data`.`email` AS `client_email`, `buyer_data`.`number` AS `client_number`, `buyer_data`.`address_1` AS `client_address_1`, `buyer_data`.`address_2` AS `client_address_2`, `buyer_data`.`city` AS `client_city`, `buyer_data`.`zip` AS `client_zip`, `buyer_data`.`countries_name` AS `client_country_name`, `buyer_data`.`countries_iso_code_3` AS `client_country_code`, `buyer_data`.`zone_code` AS `client_state_code`, `buyer_data`.`zone_name` AS `client_state_name`,
    `tbl_referral`.`client_id`,
    `tbl_referral`.`referral_fee`,
    `tbl_referral`.`referral_id`,
    `status_referral_table`.`created_date` AS `referral_date`
    FROM `tbl_activities`
    LEFT JOIN `tbl_referral_status_update` AS `referral_table` ON `referral_table`.`update_id` = `tbl_activities`.`activity_id`
    LEFT JOIN `tbl_referral` ON `tbl_referral`.`referral_id` = `referral_table`.`referral_id`
    LEFT JOIN (
               SELECT `tbl_buyer`.`buyer_id`, `tbl_buyer`.`name` AS `client_name`, `tbl_buyer_email`.`email`, `tbl_buyer_mobile`.`number`, `tbl_buyer_address`.`address_1`, `tbl_buyer_address`.`address_2`, `tbl_buyer_address`.`city`, `tbl_buyer_address`.`zip`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_zones`.`zone_code`, `tbl_zones`.`zone_name`
               FROM `tbl_buyer`
               LEFT JOIN `tbl_buyer_email` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_email`.`buyer_id`
               LEFT JOIN `tbl_buyer_mobile` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_mobile`.`buyer_id`
               LEFT JOIN `tbl_buyer_address` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_address`.`buyer_id`
               LEFT JOIN `tbl_countries` ON `tbl_buyer_address`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_buyer_address`.`state` = `tbl_zones`.`zone_id` AND `tbl_buyer_address`.`country` = `tbl_zones`.`zone_country_id`
               ) `buyer_data` ON `buyer_data`.`buyer_id` = `tbl_referral`.`client_id`
    LEFT JOIN `tbl_referral_status_update` AS `status_referral_table` ON `status_referral_table`.`referral_id` = `referral_table`.`referral_id`
   
    LEFT JOIN `tbl_users` AS `activity_user` ON
    CASE `tbl_activities`.`user_id`
    WHEN '$user_id'
    THEN
        CASE `status_referral_table`.`status`
        WHEN 9
        THEN
        `activity_user`.`id` = `tbl_activities`.`other_user_id`
        WHEN 7
        THEN
        `activity_user`.`id` = `tbl_activities`.`other_user_id`
        ELSE
        `activity_user`.`id` = `tbl_activities`.`other_user_id`
        END
    ELSE
--        CASE `status_referral_table`.`status`
--        WHEN 9
--        THEN
        `activity_user`.`id` = `tbl_activities`.`user_id`
--        WHEN 7
--        THEN
--        `activity_user`.`id` = `tbl_activities`.`user_id`
--        ELSE
--        `activity_user`.`id` = `tbl_activities`.`other_user_id`
--        END
    END
    LEFT JOIN `tbl_buyer` ON `tbl_buyer`.`buyer_id` = `tbl_activities`.`buyer_id`
    LEFT JOIN `tbl_user_registration` ON `tbl_user_registration`.`email` = `activity_user`.`email`
    WHERE `tbl_activities`.`pkid` = '$activities_id' AND (`tbl_activities`.`other_user_id` = '$user_id' OR `tbl_activities`.`user_id` = '$user_id') AND `tbl_activities`.`activity_type` = '15'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Activity Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Activity", 'data'=>$rows);
}

echo json_encode($response);

?>