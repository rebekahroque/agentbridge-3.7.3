<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $profile_id = $_GET["profile_id"];

    $query = "SELECT *
    FROM `tbl_invitation_tracker` a
    LEFT JOIN

				tbl_user_registration b

			ON

				LOWER(b.email) = LOWER(a.invited_email)
    
    WHERE a.`agent_id` = '$profile_id'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Invites Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Invites", 'data'=>$rows);
}

echo json_encode($response);

?>