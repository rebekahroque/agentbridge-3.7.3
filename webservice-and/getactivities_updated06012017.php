<?php  include('_dbconn.php');
    
	

	$user_id = $_GET["user_id"];
	$limit_one = $_GET["limit_one"];

	$db = JFactory::getDbo();
	$query = $db->getQuery(true);	
	$query->select('at.*, b.name as buyer_name, u.name as user_name, uo.name as other_user_name')
	->from('#__activities at')
	->leftJoin('#__buyer b ON b.buyer_id = at.buyer_id')
	->leftJoin('#__users u ON u.id = at.user_id')
	->leftJoin('#__users uo ON uo.id = at.other_user_id')
/*	->where("at.user_id=".$user_id." AND ((at.activity_type = 23 AND b.home_buyer = 1 ) OR at.activity_type = 8 OR at.activity_type = 30 OR at.activity_type = 25 OR at.activity_type = 15 OR at.activity_type = 11 OR at.activity_type = 6 )")*/
	->where("at.user_id=".$user_id." AND at.activity_type = 15")
	->order('at.date DESC')
	->setLimit(5,$limit_one);
	$activities = $db->setQuery($query)->loadObjectList();
	/*print_r($activities); die();*/

	$activitylog=array();
	foreach ($activities as $key => $value) {	
	
		if($value->activity_type == 23){ //Home Buyer Market

			$activitylog[]=array(
				"bluetitle"=>0,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>"",
				"userId"=>$user_id,
				"other_username"=>"",
				"other_userId"=>"",
				"popname"=>"",
				"popId"=>"",
				"buyername"=>$value->buyer_name,
				"buyerId"=>$value->buyer_id,
				"title"=>"Home Market Buyer",
				"message"=>"My Home Market Buyer has been created just for you"
			   );


		} else if($value->activity_type == 25){ //POPs Buyer Matches

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$value->activity_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('permset.selected_permission')
			->from('#__permission_setting permset')
			->where("permset.listing_id=".$value->activity_id);			
			$permset_permission = $db->setQuery($query)->loadResult();

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('reqa.permission')
			->from('#__request_access reqa')
			->where("reqa.property_id=".$value->activity_id." AND reqa.user_b = ".$user_id);			
			$reqa_permission = $db->setQuery($query)->loadResult();

			$ou_name = "";

			if($user_id!=$value->other_user_id){
				$ou_name = $value->other_user_name;
				if($permset_permission==1){
					$message = $value->other_user_name."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
				} else {
					if($popsetting==1){
						$message = $value->other_user_name."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
					} else {
						$popname=$popzip;
						if($reqa_permission==1){
							$message = $value->other_user_name."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
							$popname=$popname;
						} else if($reqa_permission==0){
							$message = $value->other_user_name."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						} else if($reqa_permission==2){
							$message = $value->other_user_name."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						} else {
							$message = $value->other_user_name."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						}						
					}
				}				
			} else {
				$message = "Your POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
			}
		

			$activitylog[]=array(
					"bluetitle"=>1,
					"actlogId"=>$value->pkId,
					"actlogtype"=>$value->activity_type,
					"username"=>$value->user_name,
					"userId"=>$user_id,
					"other_username"=>$ou_name,
					"other_userId"=>$value->other_user_id,
					"popname"=>$popname,
					"popId"=>$value->activity_id,
					"buyername"=>$value->buyer_name,
					"buyerId"=>$value->buyer_id,
					"title"=>"Buyer Match",
					"message"=>$message
				   );

		} else if($value->activity_type == 6){ //R2 Request POPs Access

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select("property_id,permission")
			->from('#__request_access')
			->where('pkId = '.$value->activity_id);
			$db->setQuery($query);
			$listing_id = $db->loadObject()->property_id;
			$permset_permission = $db->loadObject()->permission;
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$listing_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			if($permset_permission==1){
				$title = "POPs™ Access Granted";
				$message = $value->other_user_name." can now view your POPs™, ".$popname;
			} else {				
				$title = "Request To View Your POPs™";
				$message = $value->other_user_name." is requesting to view your private POPs™, ".$popname;
			}

			$activitylog[]=array(
				"bluetitle"=>1,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>$value->user_name,
				"userId"=>$user_id,
				"other_username"=>$value->other_user_name,
				"other_userId"=>$value->other_user_id,
				"popname"=>$popname,
				"popId"=>$listing_id,
				"buyername"=>"",
				"buyerId"=>"",
				"title"=>$title,
				"message"=>$message
			   );

		} else if($value->activity_type == 30){ //R1 Request POPs Access

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select("property_id,permission")
			->from('#__request_access')
			->where('pkId = '.$value->activity_id);
			$db->setQuery($query);
			$listing_id = $db->loadObject()->property_id;
			$permset_permission = $db->loadObject()->permission;

			if($listing_id==""){
				continue;
			}

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$listing_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			if($permset_permission==1){
				$title = "POPs™ Access Granted";
				$message = "You may now view ".$value->other_user_name."'s POPs™, ".$popname;
			} else {				
				$popname = $popzip;
				$listing_id="";
				$title = "Request POPs™ Access";
				$message = "Your request to access ".$value->other_user_name."'s POPs™ in ".$popzip." is pending";
			}

			$activitylog[]=array(
				"bluetitle"=>1,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>$value->user_name,
				"userId"=>$user_id,
				"other_username"=>$value->other_user_name,
				"other_userId"=>$value->other_user_id,
				"popname"=>$popname,
				"popId"=>$listing_id,
				"buyername"=>"",
				"buyerId"=>"",
				"title"=>$title,
				"message"=>$message
			   );

		} else if($value->activity_type == 15){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')->from('#__referral_status_update rsu')
			->leftJoin('#__referral r ON r.referral_id = rsu.referral_id')
			->where('rsu.update_id = '.$value->activity_id);
			$db->setQuery($query);
			$referral_value = $db->loadObject();
			//$value->value = $this->get_referral_value($value->value_id);

			

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('b.name')
			->from('#__buyer b')
			->where("b.buyer_id=".$referral_value->client_id);			
			$cliresults = $db->setQuery($query)->loadObjectList();
			$clientname = $cliresults[0]->name;
			
			$title = "";
			$message = "";

			$title="New Referral";
			$message ="You have sent referral ".$clientname." to ".$value->other_user_name;


			$activitylog[]=array(
							"bluetitle"=>1,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>$value->user_name,
							"userId"=>$user_id,
							"other_username"=>$value->other_user_name,
							"other_userId"=>$value->other_user_id,
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>$title,
							"message"=>$message
						   );

		} /* else if($value->activity_type == 11){

			$activitylog[]=array(
							"bluetitle"=>0,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>"",
							"userId"=>$user_id,
							"other_username"=>"",
							"other_userId"=>"",
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>"",
							"message"=>""
						   );

		} else if($value->activity_type == 7){

			$activitylog[]=array(
							"bluetitle"=>0,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>"",
							"userId"=>$user_id,
							"other_username"=>"",
							"other_userId"=>"",
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>"",
							"message"=>""
						   );

		} else if($value->activity_type == 8){

			$activitylog[]=array(
							"bluetitle"=>0,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>"",
							"userId"=>$user_id,
							"other_username"=>"",
							"other_userId"=>"",
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>"",
							"message"=>""
						   );

		} */
	}

	$response = array('status'=>1, 'message'=>"Found Activities", 'data'=>$activitylog);

	echo json_encode($response);

?>