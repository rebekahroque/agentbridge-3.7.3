<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);
    
    
    
    require_once 'anet_php_sdk/AuthorizeNet.php'; // Make sure this path is correct.
    $transaction = new AuthorizeNetAIM('3Q92aKyGR', '9u8RB637ZnzQ8c3P');

    $amount = number_format($_POST['amount'],2);
    $service_fee = $_POST['service_fee'];
    $card_num = $_POST['card_num'];
    $card_exp = $_POST['card_exp'];
    
    $user_id = $_POST['user_id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $country = $_POST['country'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    
    $referral_id = $_POST['referral_id'];
    
    $transaction->amount = $amount;
    $transaction->card_num = $card_num;
    $transaction->exp_date = $card_exp;
    
    $transaction->first_name = $firstname;
    $transaction->last_name = $lastname;
    
    $transaction->address = $address;
    $transaction->city = $city;
    $transaction->state = $state;
    $transaction->country = $country;
    $transaction->phone = $phone;
    $transaction->email = $email;
    $transaction->zip = $zip;
    
    $transaction->description = "Mobile Referral Transaction Fee";
    
    $response = $transaction->authorizeAndCapture();
    
    if ($response->approved) {
        $transaction_id = $response->transaction_id;
    } else {
        $error = $response->error_message;
    }
    
    $invoice = "Fee_".$user_id."_1_".rand(0, 102);
    
    $error = explode("Text:", $error);
    $error = $error[1];
    if ($error != null) {
        $responseJSON = array('status'=>0, 'message'=> "Failed to Submit Transaction -  " .$error, 'data'=>array(), 'error'=>$error);
    }
    else {
        $query = "INSERT INTO `tbl_authorize_transactions` (`invoice`, `trans_id`, `amount`, `state`, `country`, `zip`, `city`, `address`, `card_expiry`, `ref_id`, `firstname`, `lastname`, `email`, `phone`) VALUES ('$invoice', '$transaction_id', '$service_fee', '$state', '$country', '$zip', '$city', '$address', '$card_exp', '$referral_id', '$firstname', '$lastname', '$email', '$phone')";
        
        $result = $mysqli->query($query) or die($mysqli->error);
        
        $num = $mysqli->affected_rows;
        
        $mysqli->close();
        
        $responseJSON = array();
        if($num == 0) {
            $responseJSON = array('status'=>0, 'message'=>"Failed to Submit Transaction (no error)", 'data'=>array(), 'error'=>$error);
        }
        else {
            
            $responseJSON = array('status'=>1, 'message'=>"Successfully Submitted Transaction", 'data'=>array('name'=>$firstname.' '.$lastname, 'address'=>$address, 'city'=>$city, 'state'=>$state, 'zip'=>$zip, 'country'=>$country, 'invoice_number'=>$invoice, 'expiry-date'=>$card_exp, 'phone-number'=>$phone, 'referral_id'=>$referral_id, 'service_fee'=>$service_fee, 'user_id'=>$user_id), 'error'=>$error);
        }
    }
        
    echo json_encode($responseJSON);
?>