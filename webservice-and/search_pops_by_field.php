<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $keyword = $_GET['keyword'];
    $field = $_GET['field'];
    $user_id = $_GET['user_id'];
    
    if($field == 'bedroom' || $field == 'bedrooms') {
        
        $query = "SELECT `tbl_pocket_listing`.*, `tbl_pocket_images`.image
        FROM `tbl_pocket_listing`
        LEFT JOIN `tbl_users` ON `tbl_users`.id = `tbl_pocket_listing`.user_id
        LEFT JOIN `tbl_pocket_images` ON `tbl_pocket_images`.listing_id = `tbl_pocket_listing`.listing_id
        WHERE `bedroom` LIKE '%$keyword%'";
    }
    else if($field == 'bathroom' || $field == 'bathrooms') {
        
        $query = "SELECT `tbl_pocket_listing`.*, `tbl_pocket_images`.image
        FROM `tbl_pocket_listing`
        LEFT JOIN `tbl_users` ON `tbl_users`.id = `tbl_pocket_listing`.user_id
        LEFT JOIN `tbl_pocket_images` ON `tbl_pocket_images`.listing_id = `tbl_pocket_listing`.listing_id
        WHERE `bathroom` LIKE '%$keyword%'";
    }
    else if($field == 'garage') {
        
        $query = "SELECT `tbl_pocket_listing`.*, `tbl_pocket_images`.image
        FROM `tbl_pocket_listing`
        LEFT JOIN `tbl_users` ON `tbl_users`.id = `tbl_pocket_listing`.user_id
        LEFT JOIN `tbl_pocket_images` ON `tbl_pocket_images`.listing_id = `tbl_pocket_listing`.listing_id
        WHERE `garage` LIKE '%$keyword%'";
    }
    else if($field == 'sqft') {
        
        $query = "SELECT `tbl_pocket_listing`.*, `tbl_pocket_images`.image
        FROM `tbl_pocket_listing`
        LEFT JOIN `tbl_users` ON `tbl_users`.id = `tbl_pocket_listing`.user_id
        LEFT JOIN `tbl_pocket_images` ON `tbl_pocket_images`.listing_id = `tbl_pocket_listing`.listing_id
        WHERE `unit_sqft` LIKE '%$keyword%' OR `lot_sqft` LIKE '%$keyword%' OR `bldg_sqft` LIKE '%$keyword%'";
    }

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();
    
    $response = array();
    if($num == 0) {
        $response = array('status'=>0, 'message'=>"No POPs Found", 'data'=>array());
    }
    else {
        $rows = array();
        while ($r = $result->fetch_assoc())
        {
            $pops_user_id = $r["user_id"];
            $pops_listing_id = $r["listing_id"];
            
            
            $network_status = 0;
            
            $queryNetwork = "SELECT `tbl_request_network`.* FROM `tbl_request_network`
            WHERE `tbl_request_network`.`user_id` = $pops_user_id AND `tbl_request_network`.`other_user_id` = $user_id";
            
            
            $resultNetwork = $mysqli->query($queryNetwork) or die($mysqli->error);
            
            while($row = $resultNetwork->fetch_assoc())
            {
                $network_status = $rowN["status"];
                echo $rowN["user_id"];
            }
            
            
            $access_status = 0;
            if($network_status == 1) {
                
                $queryAccess = "SELECT `tbl_request_access`.`pkId` AS `access_id`, `tbl_request_access`.`user_a`, `tbl_request_access`.`user_b`, `tbl_request_access`.`property_id`, `tbl_request_access`.`permission` FROM `tbl_request_access`
                WHERE `tbl_request_access`.`user_a` = '$pops_user_id' AND `tbl_request_access`.`user_b` = '$user_id' AND `tbl_request_access`.`property_id` = '$pops_listing_id'";
                
                
                $resultAccess = $mysqli->query($queryAccess) or die($mysqli->error);
                
                while($rowA = $resultAccess->fetch_assoc())
                {
                    $access_status = $rowA["permission"];
                }
            }
            
            array_push($r, $network_status, $access_status);
            
            $rows[] = $r;
        }
        
        
        $mysqli->close();
        
        $response = array('status'=>1, 'message'=>"Found POPs", 'data'=>$rows);
    }
    
    echo json_encode($response);

?>