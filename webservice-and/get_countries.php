<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);

	


    $query = "SELECT `tbl_countries`.* FROM `tbl_countries`";

	$result = $mysqli->query($query) or die($mysqli->error);

	$num = $result->num_rows;

	$mysqli->close();

	$response = array();

	if($num == 0) {
		$response = array('status'=>0, 'message'=>"No Countries Found", 'data'=>array());
	}
	else {
		$rows = array();
		while ($r = $result->fetch_assoc())
		{
			$rows[] = $r;
		}
		
		$response = array('status'=>1, 'message'=>"Found Countries", 'data'=>$rows);
	}

	echo json_encode($response);

?>