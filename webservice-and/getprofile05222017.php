<?php include('_dbconn.php');

//$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);


    
    
  $con = $mysqli = new mysqli("localhost", $username, $password, $database);

    $mysqli->set_charset('utf8');



$user_id = $_GET["user_id"];

    $query = "SELECT `tbl_country_currency`.*,`tbl_users`.*,`tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`firstname`, `tbl_user_registration`.`lastname`, `tbl_user_registration`.`image`, `tbl_broker`.`broker_name`, `tbl_user_registration`.`brokerage_license`, `tbl_user_registration`.`street_address`, `tbl_user_registration`.`suburb`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_zones`.`zone_code`  AS `state_code`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`zip`, `user_mobile_numbers`.`mobile_numbers` AS `mobile_number`, `user_mobile_numbers`.`main` AS `mobile_number_main`, `user_mobile_numbers`.`show` AS `mobile_number_show`, `tbl_user_registration`.`email`, `tbl_user_registration`.`about`, `tbl_user_registration`.`licence`,  `tbl_user_registration`.`status`, `tbl_user_registration`.`registration_date`, `tbl_user_registration`.`is_premium`, `tbl_user_registration`.`activation_status`, `tbl_user_registration`.`tax_id_num`, `pocket_listing`.`zipcodes`, `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`is_term_accepted`
    FROM `tbl_users`
    LEFT JOIN `tbl_user_registration` ON `tbl_user_registration`.`email` = `tbl_users`.`email`
    LEFT JOIN `tbl_broker` ON `tbl_user_registration`.`brokerage` = `tbl_broker`.`broker_id`
    LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
    LEFT JOIN `tbl_country_currency` ON `tbl_user_registration`.`country` = `tbl_country_currency`.`country`
    LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
    LEFT JOIN (SELECT `tbl_user_mobile_numbers`.`user_id`,`tbl_user_mobile_numbers`.`show`, `tbl_user_mobile_numbers`.`main`, GROUP_CONCAT(DISTINCT `tbl_user_mobile_numbers`.`value`) AS `mobile_numbers`
          FROM `tbl_user_mobile_numbers`
          GROUP BY `tbl_user_mobile_numbers`.`user_id`) AS `user_mobile_numbers` ON `tbl_user_registration`.`user_id` = `user_mobile_numbers`.`user_id`
    LEFT JOIN (SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
          FROM `tbl_pocket_listing`
          GROUP BY `tbl_pocket_listing`.`user_id`) AS `pocket_listing` ON `pocket_listing`.`user_id` = `tbl_users`.`id`

/*    LEFT JOIN (SELECT `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`firstname`, `tbl_user_registration`.`lastname`, `tbl_user_registration`.`image`, `tbl_broker`.`broker_name`, `tbl_user_registration`.`brokerage_license`, `tbl_user_registration`.`street_address`, `tbl_user_registration`.`suburb`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_zones`.`zone_code`  AS `state_code`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`zip`, `user_mobile_numbers`.`mobile_numbers` AS `mobile_number`, `user_mobile_numbers`.`main` AS `mobile_number_main`, `user_mobile_numbers`.`show` AS `mobile_number_show`, `tbl_user_registration`.`email`, `tbl_user_registration`.`about`, `tbl_user_registration`.`licence`,  `tbl_user_registration`.`status`, `tbl_user_registration`.`registration_date`, `tbl_user_registration`.`is_premium`, `tbl_user_registration`.`activation_status`, `tbl_user_registration`.`tax_id_num`, `tbl_user_sales`.`ave_price_2012`, `tbl_user_sales`.`volume_2012`, `tbl_user_sales`.`sides_2012`, `pocket_listing`.`zipcodes`, `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`is_term_accepted`
               FROM `tbl_user_registration`
               LEFT JOIN `tbl_user_sales` ON `tbl_user_registration`.`user_id` = `tbl_user_sales`.`agent_id`
         LEFT JOIN `tbl_broker` ON `tbl_user_registration`.`brokerage` = `tbl_broker`.`broker_id`
               LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
               LEFT JOIN (SELECT `tbl_user_mobile_numbers`.`user_id`,`tbl_user_mobile_numbers`.`show`, `tbl_user_mobile_numbers`.`main`, GROUP_CONCAT(DISTINCT `tbl_user_mobile_numbers`.`value`) AS `mobile_numbers`
                          FROM `tbl_user_mobile_numbers`
                          GROUP BY `tbl_user_mobile_numbers`.`user_id`) AS `user_mobile_numbers` ON `tbl_user_registration`.`user_id` = `user_mobile_numbers`.`user_id`
               LEFT JOIN `tbl_users` ON `tbl_users`.`email` = `tbl_user_registration`.`email`
               LEFT JOIN (SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
                          FROM `tbl_pocket_listing`
                          GROUP BY `tbl_pocket_listing`.`user_id`) AS `pocket_listing` ON `pocket_listing`.`user_id` = `tbl_users`.`id`) AS `user_info` ON  `user_info`.`email`=`tbl_users`.`email`
   */ 
    WHERE `tbl_users`.`id` = '$user_id'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Info Found", 'data'=>array());
}
else {
	 $rows = array();    
   $rows_u = array();
  while ($r = $result->fetch_assoc())
  {
    $rows[] = $r;
  }
  foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='firstname'){
                $this_line['firstname']=stripslashes($value2);
            }
            if($key2=='lastname'){
                $this_line['lastname']=stripslashes($value2);
            }
            if($key2=='zipcodes'){
                $value2=explode(",",$value2);                
               // $this_line['zipcodes']=implode(", ",$value2);
                $this_line['works_zip'] = $userprof_model->get_pocket_zips($user_id);
                $this_line['works_zip_b'] = $userprof_model->get_buyer_zips($user_id);
                $this_line['w_zips'] = $userprof_model->get_w_zips($this_line['email'])[0]->zip_workaround;

                $zip_arr= array();
                $zip_arr_val= array();
                $zip_arr_country= array();
                  if(count($this_line['works_zip'])>=1 || count($this_line['works_zip_b'])>=1){
                    foreach($this_line['works_zip'] as $zip){
                      $zip_arr[] = $zip->zip;
                      $zip_arr_val[$zip->zip]["pop"]+=$zip->count;
                    }
                    foreach($this_line['works_zip_b'] as $zip){
                      if(strpos($zip->zip,',') !== false){
                        $zip_ex=explode(",",$zip->zip);
                        $zip_arr[] = $zip_ex[0];
                        $zip_arr_val[$zip_ex[0]]["buyer"]+=$zip->count;
                        $zip_arr[] = $zip_ex[1];
                        $zip_arr_val[$zip_ex[1]]["buyer"]+=$zip->count;
                      } 
                      else{
                        $zip_arr[] = $zip->zip;                               
                          $zip_arr_val[$zip->zip]["buyer"]+=$zip->count;
                      }
                    }
                  }

                  $expl_w_zips = explode(",",$this_line['w_zips']);

                  foreach ($expl_w_zips as $key => $value) {
                    # code...
                    if($value){
                      $zip_arr[] = $value;
                      $zip_arr_val[$value]["pop"]+=0;                               
                        $zip_arr_val[$value]["buyer"]+=0;
                      }
                  }

                
                $zips_imploded="'(".implode("|", array_unique(array_filter($zip_arr))).")'";



                $zips_b_raw = $userprof_model->get_buyer_zip_country($user_id, $zips_imploded);
                $zips_p_raw = $userprof_model->get_pops_zip_country($user_id, $zips_imploded);
                $zips_w_raw = $userprof_model->get_w_zips_country($zips_imploded);



                $zips_b=array();  
                $i=0;
                foreach ($zips_b_raw as $value) {
                  $this_country=$value->country;
                  $this_country_iso=$value->countries_iso_code_2;

                  $exploded_zips = explode(",", $value->zip);
                  foreach ($exploded_zips as $zip_e) {

                    $zips_b[$i]['zip']=$zip_e;
                    $zips_b[$i]['country']=$this_country;
                    $zips_b[$i]['this_country_iso']=$this_country_iso;
                    $i++;
                  }

                  
                }



                foreach ($zips_p_raw as $value) {
                  $this_country=$value->country;
                  $this_country_iso=$value->countries_iso_code_2;
                  $exploded_zips = explode(",", $value->zip);
                  foreach ($exploded_zips as $zip_e) {

                    $zips_b[$i]['zip']=$zip_e;
                    $zips_b[$i]['country']=$this_country;
                    $zips_b[$i]['this_country_iso']=$this_country_iso;
                    $i++;
                  }
                  
                }

                foreach ($expl_w_zips as $zip_e) {
                  if($zip_e){
                    $zips_b[$i]['zip']=$zip_e;
                    $zips_b[$i]['country']=$zips_w_raw[0]->countries_id;
                    $zips_b[$i]['this_country_iso']=$zips_w_raw[0]->countries_iso_code_2;
                    $i++;
                  }
                }


                  

                $zip_arr[] = $user_info->zip;
                $zip_arr_val[$user_info->zip]["pop"]+=0;                                
                $zip_arr_val[$user_info->zip]["buyer"]+=0;
                $last_row = count($zips_b);
                $zips_b[$last_row]['zip']=$this_line['zip'];
                $zips_b[$last_row]['country']=$this_line['countryId'];
                $zips_b[$last_row]['this_country_iso']=$this_line['countries_iso_code_2'];

                
                $t=0;
                $l=0;
                $up_zip_arr_val=array();
                //var_dump($zips_b);
                foreach ($zip_arr_val as $key => $value) {
                  //var_dump($key);
                  foreach ($zips_b as $key2 => $value2) {
                    if($key==$value2['zip']){

                      if($value2['country']==222){

                        if($value2['country']==222 && (strcspn($value2['zip'], '0123456789') != strlen($value2['zip'])) ){          
                          preg_match('/\d/', $value2['zip'], $m, PREG_OFFSET_CAPTURE);
                            if (sizeof($m))
                                 $m[0][1]; // 24 in your example
                              $newkey=substr($value2['zip'], 0, ($m[0][1]+1));
                              $otherhalf_key = substr($value2['zip'], ($m[0][1]+1), strlen($value2['zip']));
                                if(is_numeric($otherhalf_key[0])){
                                  $newkey = $newkey.$otherhalf_key[0];
                                }
                        } else {
                          $newkey = $value2['zip'];
                        }
                        if($zip_arr_val[$value2['zip']]['pop'])
                          $up_zip_arr_val[$newkey]["pop"] += $zip_arr_val[$value2['zip']]['pop'];
                        if($zip_arr_val[$value2['zip']]['buyer'])
                          $up_zip_arr_val[$newkey]["buyer"] += $zip_arr_val[$value2['zip']]['buyer'];

                        if(!$zip_arr_val[$value2['zip']]['buyer'] && !$zip_arr_val[$value2['zip']]['pop']){
                            $up_zip_arr_val[$newkey]=$zip_arr_val[$value2['zip']];
                          }
                        //unset($zip_arr_val[$value2['zip']]);
                        //$zip_arr_val[$t]
                        //var_dump($value2['zip']);
                        //var_dump( $value['buyer']);
                        //var_dump( $value['pop']);
                      } else {
                        $up_zip_arr_val[$value2['zip']]=$zip_arr_val[$value2['zip']];
                      } 
                      continue 2;     
                    }
                    $l++;
                  }
                  $t++;       
                }

                uasort( $up_zip_arr_val, function($a, $b) {
                    if( $a['pop'] == $b['pop'] )
                        return 0;
                    return ( $a['pop'] < $b['pop'] ) ? 1 : -1;
                } );

                 $this_line['zipcodes']=implode(", ",array_keys($up_zip_arr_val));

            }
        }

        $user_details = get_profile_info($user_id);


        $current_user_info_currency = "USD";
        $current_user_info_symbol = "$";

        if($user_details->currency!=$current_user_info_currency){

          $ex_rates_con = $ex_rates->rates->{$user_details->currency};
          $ex_rates_can = $ex_rates->rates->{$current_user_info_currency};
          if($user_info->currency=="USD"){                  


            $previous_year = date("Y", strtotime("-1 year"));

            while(intval($previous_year) >= 2012) {
              $this_line['ave_price'] .= ($user_details->{"ave_price_".$previous_year} * $ex_rates_can)."--";
              $this_line['volume'] .= ($user_details->{"volume_".$previous_year} * $ex_rates_can)."--";
              $previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
            }

          } else {

            $previous_year = date("Y", strtotime("-1 year"));

            while(intval($previous_year) >= 2012) {
              $this_line['ave_price'] .= (($user_details->{"ave_price_".$previous_year} / $ex_rates_con)*$ex_rates_can)."--";
              $this_line['volume'] .= (($user_details->{"volume_".$previous_year} / $ex_rates_con)*$ex_rates_can)."--";
              $this_line['sides'][$previous_year] = $user_details->{"sides_".$previous_year};
              $this_line['verified'][$previous_year] = $user_details->{"verified_".$previous_year};
              $previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
            }

          }
          $this_line['currency'] = $current_user_info_currency;
          $this_line['symbol'] = $current_user_info_symbol;

        } else {
            
            $previous_year = date("Y", strtotime("-1 year"));
            while(intval($previous_year) >= 2012) {
              
              $this_line['ave_price'][$previous_year] = $user_details->{"ave_price_".$previous_year};
              $this_line['volume'][$previous_year] = $user_details->{"volume_".$previous_year};
              $this_line['sides'][$previous_year] = $user_details->{"sides_".$previous_year};
              $this_line['verified'][$previous_year] = $user_details->{"verified_".$previous_year};
              $previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
            }
            
        }


        $compound_ave_price = array_values($this_line['ave_price']);
        $compound_volume = array_values($this_line['volume']);
        $compound_sides= array_values($this_line['sides']);
        $compound_verified =  array_values($this_line['verified']);

        if($compound_verified[0]=="1" && $compound_verified[1]=="1"){
         

          $sales_val=((($compound_volume[0]/$compound_sides[0])+($compound_volume[1]/$compound_sides[1]))/2);
          $this_line['ave_price_value'] =  format_currency_global($sales_val,$this_line['symbol'],$this_line['currency']);
          $this_line['ave_price_label'] = "Average Sales Price previous 2 years";


          $sales_vol=$compound_volume[0]+$compound_volume[1];
          $this_line['volume_value'] = format_currency_global($sales_vol,$this_line['symbol'],$this_line['currency']);
          $this_line['volume_label'] = "Total Volume previous 2 years";

          $sales_sides=$compound_sides[0]+$compound_sides[1];
          $this_line['sides_value'] = $sales_sides;
          $this_line['sides_label'] = "Total Sides previous 2 years.";

        } else {

              $this_line['ave_price_value'] = "AgentBridge is verifying numbers.";
              $this_line['ave_price_label'] = "Average Sales Price";
              
              $this_line['volume_value'] = "AgentBridge is verifying numbers.";
              $this_line['volume_label'] = "Total Volume";
              
              $this_line['sides_value'] = "AgentBridge is verifying numbers.";
              $this_line['sides_label'] = "Total Sides";


              for( $i = 0; $i < count($compound_verified); $i++)
              {   
                  if($compound_verified[$i]=="1"){
                      $previous_year = date("Y", strtotime("-".($i+1)." year"));

                      //ave price                      
                      $this_line['ave_price_value'] = format_currency_global($compound_ave_price[$i],$this_line['symbol'],$this_line['currency']);
                      $this_line['ave_price_label'] = "Average Sales Price ".$previous_year;

                      //volume
                      $this_line['volume_value'] = format_currency_global($compound_volume[$i],$this_line['symbol'],$this_line['currency']);
                      $this_line['volume_label'] = "Total Volume ".$previous_year;

                      //sides
                      $this_line['sides_value'] = $compound_sides[$i];
                      $this_line['sides_label'] = "Total Sides ".$previous_year;
                      break;
                  }
              }

        }

        $this_line['image'] = strpos($this_line['image'], JURI::base()) !== false ? str_replace("loads/", "loads/", $this_line['image']).'?'.microtime(true) :  JURI::base().'uploads/'.$this_line['image'].'?'.microtime(true);


        $rows_u[]=$this_line;
    }



  $response = array('status'=>1, 'message'=>"Found User Info", 'data'=>$rows_u);
//	$response = array('status'=>1, 'message'=>"Found User Info", 'data'=>$rows);
}



  function get_profile_info($userid, $own = false){

    $db = JFactory::getDbo();

    $query = $db->getQuery(true);

    $query->select("u.*, conc.*,

      b.broker_name, 

      z.zone_code, 

      con.*, 

      b.zone_id, 

      b.broker_id, 

      ur.tax_id_num,

      us.*, ur.*,temptable.*");

    $query->from('#__users u');

    $query->where('id = '.$userid);

    $query->leftJoin('#__user_registration ur ON u.email = ur.email');

    $query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');

    $query->leftJoin('#__zones z ON ur.state = z.zone_id');

    $query->leftJoin('#__country_currency conc ON conc.country = z.zone_country_id');

    $query->leftJoin('#__countries con ON con.countries_id = z.zone_country_id');

    $query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
    

    //$query->leftJoin('(select temptable1.user_id, group_concat(temptable1.value) from (select user_id, value from #__user_work_numbers where user_id = '.$userid.' limit 3) temptable1 ) temptable ON temptable.user_id = '.$userid);

    $query->leftJoin('(

      SELECT 

        temptable1.newid,

        temptable1.user_id, 

        group_concat(temptable1.value)  as newcontact

      FROM 

        (

          SELECT 

            us.id as newid,

            ss.user_id, 

            ss.value 

          FROM 

            #__user_work_numbers ss

          LEFT JOIN 

            #__user_registration ur

          ON 

            ss.user_id = ur.user_id

          LEFT JOIN

            #__users us 

          ON 

            us.email = ur.email

          WHERE

            us.id = '.$userid.' 

          LIMIT 3

        ) temptable1 

      ) temptable 

      ON 

        temptable.newid = '.$userid

    );

  //  echo $query; die();

    $db->setQuery($query);


    $stripped_name= stripslashes($db->loadObject()->name);

    $stripped_firstname = stripslashes($db->loadObject()->firstname);

    $stripped_lastname = stripslashes($db->loadObject()->lastname);

    $db->loadObject()->lastname = $stripped_lastname ;


    $ret = $db->loadObject();

    $ret->name = $stripped_name;

    $ret->firstname = $stripped_firstname;

    $ret->lastname = $stripped_lastname;

    return $ret;

  }

echo json_encode($response);

?>