<?php
defined('_JEXEC') or die;
/**
 * Registration model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class PropertyListingModelPropertyListing extends JModelLegacy{
	/**
	 * @var		object	The user registration data.
	 * @since   1.6
	 */
	protected $data;
	/**
	 * Method to activate a user account.
	 *
	 * @param   string  The activation token.
	 * @return  mixed  	False on failure, user object on success.
	 * @since   1.6
	 */
		function getExchangeRates($price,$currency_user,$currency_pops){
			$file = 'latest.json';
			$appId = 'd73f8525552048a7a39aaac9977299fd';
			// Open CURL session:
			$ch = curl_init("https://openexchangerates.org/api/{$file}?app_id={$appId}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);
			// Decode JSON response:
			$exchangeRates = json_decode($json);
			// You can now access the rates inside the parsed object, like so:
			/*printf(
			    "1 %s in GBP: %s (as of %s)",
			    $exchangeRates->base,
			    $exchangeRates->rates->GBP,
			    date('H:i jS F, Y', $exchangeRates->timestamp)
			);*/
			if(!$currency_pops){
				$currency_pops="USD";
			}
			if($currency_user!="USD" && $currency_user!=""){
				$converted_price = $price * $exchangeRates->rates->{$currency_user};
			} else {
				$converted_price = $price / $exchangeRates->rates->{$currency_pops};
			}
			return $converted_price;
		}
		function getExchangeRates_indi(){
			$file = 'latest.json';
			$appId = 'd73f8525552048a7a39aaac9977299fd';
			// Open CURL session:
			//$ch = curl_init("https://openexchangerates.org/api/latest.json?app_id=d73f8525552048a7a39aaac9977299fd");
		//	$ch = curl_init("https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452");
			//curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
			//curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
			///curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,1); 
			//curl_setopt($ch, CURLOPT_TIMEOUT, 2);
			//$json = curl_exec($ch);
			//if($json === false){
			    //echo 'error:' . curl_error($ch);
			    $json = '{
  "disclaimer": "Exchange rates provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://openexchangerates.org/terms/",
  "license": "Data sourced from various providers; resale prohibited; no warranties given of any kind. All usage subject to License Agreement: https://openexchangerates.org/license/",
  "timestamp": 1465347615,
  "base": "USD",
  "rates": {
    "AED": 3.672917,
    "AFN": 69.089998,
    "ALL": 121.8642,
    "AMD": 478.412497,
    "ANG": 1.788675,
    "AOA": 165.776165,
    "ARS": 13.83634,
    "AUD": 1.342943,
    "AWG": 1.793333,
    "AZN": 1.504413,
    "BAM": 1.722264,
    "BBD": 2,
    "BDT": 78.438639,
    "BGN": 1.722308,
    "BHD": 0.377039,
    "BIF": 1550.3125,
    "BMD": 1,
    "BND": 1.352801,
    "BOB": 6.87674,
    "BRL": 3.456372,
    "BSD": 1,
    "BTC": 0.001733141301,
    "BTN": 66.804132,
    "BWP": 10.858288,
    "BYR": 19818.2,
    "BZD": 2.005158,
    "CAD": 1.27823,
    "CDF": 928.225,
    "CHF": 0.9671,
    "CLF": 0.024598,
    "CLP": 679.8636,
    "CNY": 6.569682,
    "COP": 2969.538337,
    "CRC": 538.5086,
    "CUC": 1,
    "CUP": 1.00005,
    "CVE": 97.105801,
    "CZK": 23.79149,
    "DJF": 177.849626,
    "DKK": 6.547766,
    "DOP": 45.85847,
    "DZD": 109.72196,
    "EEK": 13.779225,
    "EGP": 8.878855,
    "ERN": 14.9985,
    "ETB": 21.69395,
    "EUR": 0.88041,
    "FJD": 2.07745,
    "FKP": 0.687722,
    "GBP": 0.687722,
    "GEL": 2.12346,
    "GGP": 0.687722,
    "GHS": 3.88605,
    "GIP": 0.687722,
    "GMD": 42.53155,
    "GNF": 7283.287598,
    "GTQ": 7.628026,
    "GYD": 205.509669,
    "HKD": 7.766793,
    "HNL": 22.63407,
    "HRK": 6.612642,
    "HTG": 62.430737,
    "HUF": 273.5563,
    "IDR": 13296.116667,
    "ILS": 3.837074,
    "IMP": 0.687722,
    "INR": 66.75168,
    "IQD": 1168.316771,
    "IRR": 30317.5,
    "ISK": 122.7022,
    "JEP": 0.687722,
    "JMD": 124.665099,
    "JOD": 0.708308,
    "JPY": 107.0652,
    "KES": 101.01357,
    "KGS": 68.379202,
    "KHR": 4066.357451,
    "KMF": 431.119568,
    "KPW": 899.91,
    "KRW": 1160.333333,
    "KWD": 0.301338,
    "KYD": 0.821673,
    "KZT": 333.529588,
    "LAK": 8072.467598,
    "LBP": 1504.951667,
    "LKR": 145.8001,
    "LRD": 90.49095,
    "LSL": 14.93877,
    "LTL": 3.022291,
    "LVL": 0.619469,
    "LYD": 1.353158,
    "MAD": 9.644765,
    "MDL": 19.70151,
    "MGA": 3260.741634,
    "MKD": 54.22468,
    "MMK": 1186.280012,
    "MNT": 1984,
    "MOP": 7.984382,
    "MRO": 355.257667,
    "MTL": 0.683602,
    "MUR": 35.452075,
    "MVR": 15.243333,
    "MWK": 702.8137,
    "MXN": 18.43727,
    "MYR": 4.06996,
    "MZN": 58.264999,
    "NAD": 14.93849,
    "NGN": 198.555401,
    "NIO": 28.47576,
    "NOK": 8.132514,
    "NPR": 106.7317,
    "NZD": 1.434897,
    "OMR": 0.385049,
    "PAB": 1,
    "PEN": 3.308185,
    "PGK": 3.12995,
    "PHP": 46.08972,
    "PKR": 104.4272,
    "PLN": 3.837365,
    "PYG": 5630.135013,
    "QAR": 3.640518,
    "RON": 3.970572,
    "RSD": 108.587401,
    "RUB": 64.82996,
    "RWF": 762.68562,
    "SAR": 3.750218,
    "SBD": 7.79857,
    "SCR": 13.05456,
    "SDG": 6.070176,
    "SEK": 8.130433,
    "SGD": 1.353627,
    "SHP": 0.687722,
    "SLL": 3946,
    "SOS": 587.660375,
    "SRD": 6.77325,
    "STD": 21577.175,
    "SVC": 8.7228,
    "SYP": 219.168998,
    "SZL": 14.93847,
    "THB": 35.2447,
    "TJS": 7.869,
    "TMT": 3.5013,
    "TND": 2.132362,
    "TOP": 2.263959,
    "TRY": 2.899422,
    "TTD": 6.629287,
    "TWD": 32.20679,
    "TZS": 2184.430016,
    "UAH": 24.91866,
    "UGX": 3352.283333,
    "USD": 1,
    "UYU": 30.87264,
    "UZS": 2939.319947,
    "VEF": 9.972213,
    "VND": 22309.266667,
    "VUV": 111.582501,
    "WST": 2.533302,
    "XAF": 578.720424,
    "XAG": 0.0609855,
    "XAU": 0.000802,
    "XCD": 2.70302,
    "XDR": 0.706872,
    "XOF": 579.207224,
    "XPD": 0.001808,
    "XPF": 105.092601,
    "XPT": 0.001002,
    "YER": 250.116999,
    "ZAR": 14.94318,
    "ZMK": 5252.024745,
    "ZMW": 10.615963,
    "ZWL": 322.322775
  }
}';
			//} else {

			//}


			// Get the data:
		///	$json = curl_exec($ch);
			//curl_close($ch);
			// Decode JSON response:
			//$exchangeRates = json_decode($json);
			
			if(!isset($_SESSION['exchange_rates']) || $_SESSION['exchange_rates'] == "") {
				//$exchangeRates = file_get_contents("https://openexchangerates.org/api/latest.json?app_id=d73f8525552048a7a39aaac9977299fd", false);
				$data["app_id"] = "d73f8525552048a7a39aaac9977299fd";
				$result = $this->post_request("https://openexchangerates.org/api/latest.json", $data, false);
				$exchangeRates = $result['content'];
				//var_dump($exchangeRates);
				$_SESSION['exchange_rates'] = $exchangeRates;
			} else {
				$exchangeRates = $_SESSION['exchange_rates'];
			}

			
			
			//$exchangeRates = file_get_contents("https://openexchangerates.org/api/latest.json?app_id=d73f8525552048a7a39aaac9977299fd", false);
			// You can now access the rates inside the parsed object, like so:
			/*printf(
			    "1 %s in GBP: %s (as of %s)",
			    $exchangeRates->base,
			    $exchangeRates->rates->GBP,
			    date('H:i jS F, Y', $exchangeRates->timestamp)
			);*/
			return json_decode($exchangeRates);
		}

		function checkLangFiles(){

			$ini_file = file_get_contents(JPATH_SITE."/language/english-US/english-US.com_nrds.ini");
			$ini_lines = explode("\n", $ini_file);
			$duplicated = array();

			foreach ($ini_lines as $line)
			{
			    $parts = explode('=', $line);

			    if (count($parts) != 2)
			    {
			        continue;
			    }

			    if(strpos($parts[0], "COM_POPS") !== false){
			    	$k = $parts[0];
			    	$v = $parts[1];

			    	$duplicated[$k] = trim(str_replace('"', '', $v));
			    }
		    
			}

			return $duplicated;

		}

		function getAppUserToken($user_id,$device_os){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('device_token');
			$query->from('#__user_device_token');
			$query->where('device_os = "'.$device_os.'" AND user_id ='.$user_id);
			$db->setQuery($query);
			$tokens = $db->loadColumn();
			return $tokens;

		}

		function getPOPsDetails($pop,$sqM,$subtype,$langValues){

				

			$pop = (array) $pop;

			$pop_desc_array = array();
			$pop_desc_init_array = array();

			if($subtype==1){ //residential purchase + sfr
				$initial_features = array( 
									"bedroom",
									"bathroom",								
									"bldg_sqft"																
									);

			} else if($subtype==2 || $subtype==3) { //residential purchase + condo / townhouse
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"unit_sqft",															
									);

			} else if($subtype==4) { //residential purchase + land
				$initial_features = array( 
									"lot_size",
									"view",
									"zoned"							
									);
				
			} else if($subtype==5) { //residential lease + sfr
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"bldg_sqft",
									"term",
									"possession",
									"pet",
									"furnished"								
									);
				
			} else if($subtype==6 || $subtype==7) { //residential lease + condo / townhouse
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"unit_sqft",
									"term",
									"furnished",
									"pet"								
									);
				
			} else if($subtype==8) { //residential lease + land
				$initial_features = array( 
									"lot_size",
									"zoned",
									"view"							
									);

			} else if($subtype==9) { //comm purchase + multi family
				$initial_features = array( 
									"units",
									"cap_rate",
									"bldg_sqft"							
									);
				
			} else if($subtype==10) { //comm purchase + office
				$initial_features = array( 
									"type",
									"listing_class",
									"cap_rate",
									"bldg_sqft"						
									);
				
			} else if($subtype==11 || $subtype==12) { //comm purchase + industrial / retail
				$initial_features = array( 
									"type",
									"cap_rate",
									"bldg_sqft"						
									);
				
			} else if($subtype==13 || $subtype==14) { //comm purchase + hotel / assisted care
				$initial_features = array( 
									"type",
									"cap_rate",
									"room_count"						
									);
				
			} else if($subtype==15) { //comm purchase + special purpose
				$initial_features = array( 
									"type",
									"cap_rate"						
									);
				
			} else if($subtype==16) { //comm lease + office
				$initial_features = array( 
									"type",
									"listing_class",
									"available_sqft"						
									);
				
			} else if($subtype==17) { //comm lease + industrial
				$initial_features = array( 
									"type",
									"type_lease",
									"available_sqft"						
									);
				
			} else if($subtype==18) {
				$initial_features = array( 
									"type",
									"bldg_sqft"						
									);
				
			} else if(!$subtype){
				$initial_features = array();
			}

			$pops_feats_array = array( 
								"bedroom",
								"bathroom",
								"unit_sqft",
								"view",
								"style",
								"year_built",
								"pool_spa",
								"condition",
								"garage",
								"units",
								"cap_rate",
								"grm",
								"occupancy",
								"type",
								"listing_class",
								"parking_ratio",
								"ceiling_height",
								"stories",
								"room_count",
								"type_lease",
								"type_lease2",
								"available_sqft",
								"lot_sqft",
								"lot_size",
								"bldg_sqft",
								"term",
								"furnished",
								"pet",
								"possession",
								"zoned",
								"bldg_type",
								"features1",
								"features2",
								"features3",								
								);

			foreach ($pop as $key => $value) {
				if(!$value){
					$initial_features = array_diff($initial_features, array($key));
				}
				if($key=='bedroom' && $value != "" && $value != 0 ){
					if($value==1){
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_BED');
						$value = JText::_('COM_POPS_TERMS_BED').": ".$value;					
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_BEDS');
						$value = JText::_('COM_POPS_TERMS_BEDS').": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='bathroom' && $value != "" && $value != 0 ){
					if($value==1){
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_BATH');
						$value = JText::_('COM_POPS_TERMS_BATH').": ".$value;
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_BATHS');
						$value = JText::_('COM_POPS_TERMS_BATHS').": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='unit_sqft' && $value != "" ){
					$value=str_replace("+", "", $value);
					$arr_val = explode("-", $value);
					if($value=="Undisclosed"){
						continue;
					} else if(!$arr_val[1]){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_UNITSQ'.$sqM);
						$value = JText::_('COM_POPS_TERMS_UNITSQ'.$sqM).": ".$value;
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_UNITSQ'.$sqM);
						$value = JText::_('COM_POPS_TERMS_UNITSQ'.$sqM).": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='view' && $value != "" && $value != "None" ){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}

					$pop_desc = JText::_($value);		
					$value = JText::_('COM_POPS_TERMS_VIEW').": ".$pop_desc;			
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='style' && $value != "" ){
					
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value;			
					$value = JText::_('COM_POPS_TERMS_STYLE').": ".$value;		
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='year_built' && $value ){
					
					$value =JText::_('COM_POPS_TERMS_YEAR_BUILT').": ".$value;
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				}else
				if($key=='pool_spa' && $value != ""){

					if ($value == "Other") { $value = JText::_('COM_POPS_TERMS_POOL_SPA').": ".JText::_('COM_POPS_TERMS_POOL_OTHER');} 
					else if ($value == "Pool") { $value = JText::_('COM_POPS_TERMS_POOL').": ".JText::_('COM_POPS_TERMS_PETYES');}
					else { $value = JText::_('COM_POPS_TERMS_POOL_SPA').": ".JText::_('COM_POPS_TERMS_PETYES');}

					$pop_desc = $value;					
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='condition' && $value != "" ){

					if($value=="Not Disclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					}

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value;	
					$value =JText::_('COM_POPS_TERMS_CONDITION').": ".$value;

					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='garage' && $value ){
					if($value==8){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_GARAGE');
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_GARAGE');
					}	
					$value =JText::_('COM_POPS_TERMS_GARAGE').": ".$value;
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='units' && $value != "" ){


					if($value == "2"){
						$pop_desc = JText::_('COM_POPS_TERMS_DUPLEX')."";
						$value =JText::_('COM_POPS_TERMS_UNIT').": ".$pop_desc;
					} else if ($value == "3") {
						$pop_desc = JText::_('COM_POPS_TERMS_TRIPLEX')."";
						$value =JText::_('COM_POPS_TERMS_UNIT').": ".$pop_desc;
					} else if ($value == "4") {
						$pop_desc = JText::_('COM_POPS_TERMS_QUAD')."";
						$value =JText::_('COM_POPS_TERMS_UNIT').": ".$pop_desc;
					} else if ($value == "251") {
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_UNIT')."";
						$value =JText::_('COM_POPS_TERMS_UNIT').": ".$value."+";
					} else if ($value == "Land") {
						$pop_desc = JText::_('COM_POPS_TERMS_LAND');
						$value = JText::_('COM_POPS_TERMS_LAND');
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_UNIT')."";
						$value =JText::_('COM_POPS_TERMS_UNIT').": ".$value;
					}	

				
					
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='cap_rate' && $value != "" ){

					if($value=="Not Disclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else
					if($value == "15") {
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_CAP')."";
						$value =JText::_('COM_POPS_TERMS_CAP').": ".$value."+";
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_CAP')."";
						$value =JText::_('COM_POPS_TERMS_CAP').": ".$value;
					}	
					
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='grm' && $value != ""){
					
					if($value==0){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else if ($value == "20") {
						$pop_desc = $value ."+ ".JText::_('COM_POPS_TERMS_GRM');
						$value =JText::_('COM_POPS_TERMS_GRM').": ".$value."+";
					 } else { 
					    $pop_desc = $value." ".JText::_('COM_POPS_TERMS_GRM');
					    $value =JText::_('COM_POPS_TERMS_GRM').": ".$value;
					 }	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='occupancy' && $value != "" ){
					if($value=="Undisclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_OCCUPANCY');
					    $value =JText::_('COM_POPS_TERMS_OCCUPANCY').": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='type' && $value != ""){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value;
					$value =JText::_('COM_POPS_TERMS_TYPE2').": ".$value;
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='listing_class' && $value != "" ){
					if($value=="Not Disclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else {
						switch($value) {
								case 4:
									$pop_desc = "Class A";
									$value = "Class: A";									
									break;
								case 3:
									$pop_desc = "Class B";
									$value = "Class: B";	
									break;
								case 2:
									$pop_desc = "Class C";
									$value = "Class: C";
									break;
								case 1:
									$pop_desc = "Class D";
									$value = "Class: D";
									break;
								default:
									$pop_desc = "Class ".$value;
									$value = "Class: ".$value;
									break;
							}
						//$pop_desc = $value." ".JText::_('COM_POPS_TERMS_CLASS');
						//$value =JText::_('COM_POPS_TERMS_CLASS').": ".$value;
					}		
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='parking_ratio' && $value != "" ){
					$pop_desc = $value." ".JText::_('COM_POPS_TERMS_PARKING_RATIO');
					$value =JText::_('COM_POPS_TERMS_PARKING_RATIO').": ".$value;
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='ceiling_height' && $value != ""  ){
					$pop_desc = $value." ".JText::_('COM_POPS_TERMS_CEILING_HEIGHT');
					$value =JText::_('COM_POPS_TERMS_CEILING_HEIGHT').": ".$value;
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='stories' && $value != "" ){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value." ".JText::_('COM_POPS_TERMS_STORIES');
					$value =JText::_('COM_POPS_TERMS_STORIES').": ".JText::_($value);
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='room_count' && $value){
					if($value == 200){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_ROOMS')."";
						$value =JText::_('COM_POPS_TERMS_ROOM_COUNT').": ".$value."+";
					} else if ($value < 200) {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_ROOMS')."";
						$value =JText::_('COM_POPS_TERMS_ROOM_COUNT').": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='type_lease' && $value){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					if($value=="Not Disclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else {
						$pop_desc = $value;
						$value =JText::_('COM_POPS_TERMS_LEASE').": ".$value;
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='type_lease2' && $value  ){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}
					if($value=="Not Disclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_TYPE_LEASE')."";
						$value =JText::_('COM_POPS_TERMS_TYPE_LEASE').": ".$value;
					}		
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='available_sqft' && $value != ""  ){
					$value=str_replace("+", "", $value);
					$arr_val = explode("-", $value);

					if($value=="Undisclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else if (!$arr_val[1]) {
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_AVAIL'.$sqM);
						$value =JText::_('COM_POPS_TERMS_AVAIL'.$sqM).": ".$value."+";
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_AVAIL'.$sqM);
						$value =JText::_('COM_POPS_TERMS_AVAIL'.$sqM).": ".$value."";
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='lot_sqft' && $value != ""  ){
					$value=str_replace("+", "", $value);
					$arr_val = explode("-", $value);
					if($value=="Undisclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else if(!$arr_val[1]){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_LOT'.$sqM);
						$value =JText::_('COM_POPS_TERMS_LOT'.$sqM).": ".$value."+";
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_LOT'.$sqM);
						$value =JText::_('COM_POPS_TERMS_LOT'.$sqM).": ".$value."";
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='lot_size' && $value != "" ){

					$value=str_replace("+", "", $value);
					$value = trim($value);

					
					$arr_val = explode("-", $value);
					if($value=="Undisclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else if(!$arr_val[1]){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_LOT_SIZE'.$sqM);
						$value =JText::_('COM_POPS_TERMS_LOT_SIZE'.$sqM).": ".$value."+";
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_LOT_SIZE'.$sqM);
						$value =JText::_('COM_POPS_TERMS_LOT_SIZE'.$sqM).": ".$value."";
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='bldg_sqft' && $value != "" ){
					//echo $value;
					$value=str_replace("+", "", $value);
					$arr_val = explode("-", $value);
					if($value=="Undisclosed"){
						$initial_features = array_diff($initial_features, array($key));
						continue;
					} else if(!$arr_val[1]){
						$pop_desc = $value."+ ".JText::_('COM_POPS_TERMS_BLDG'.$sqM);
						$value =JText::_('COM_POPS_TERMS_BLDG'.$sqM).": ".$value."+";
					} else {
						$pop_desc = $value." ".JText::_('COM_POPS_TERMS_BLDG'.$sqM);
						$value =JText::_('COM_POPS_TERMS_BLDG'.$sqM).": ".$value."";
					}	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='term' && $value ){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value;
					$value =JText::_('COM_POPS_TERMS_TERM').": ".$value."";	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='furnished' && $value ){
					$pop_desc = JText::_('COM_POPS_TERMS_FURNISHED');	
					$value =JText::_('COM_POPS_TERMS_FURNISHED').": ".JText::_('COM_POPS_TERMS_PETYES');
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='pet' && $value ){
					$pop_desc = JText::_('COM_POPS_TERMS_PET');	
					$value =JText::_('COM_POPS_TERMS_PET');	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='possession' && $value ){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = JText::_(trim($key2));
						}
					}

					$pop_desc = $value;	
					$value =JText::_('COM_POPS_TERMS_POSSESSION').": ".$value;	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='zoned' && $value != ""  ){
					if($value == "20") {
						$pop_desc =  $value."+ ".JText::_('COM_POPS_TERMS_UNIT');
						$value =JText::_('COM_POPS_TERMS_ZONED').": ".$pop_desc;	
					} else if ($value == "1"){
						$pop_desc =  $value." ".JText::_('COM_POPS_TERMS_UNITONE');
						$value =JText::_('COM_POPS_TERMS_ZONED').": ".$pop_desc;	
					} else {
						$pop_desc =  $value." ".JText::_('COM_POPS_TERMS_UNIT');
						$value =JText::_('COM_POPS_TERMS_ZONED').": ".$pop_desc;		
					}
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='bldg_type' && $value != ""){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}
					$pop_desc =  JText::_($value);
					$value =JText::_('COM_POPS_TERMS_BLDG_TYPE').": ".JText::_($value);
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='features1' && $value != ""){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}
					$pop_desc = JText::_($value);	
					$value = JText::_($value);
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='features2' && $value != ""){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}
					$pop_desc = JText::_($value);
					$value = JText::_($value);	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				} else
				if($key=='features3' && $value != ""){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}
					$pop_desc = JText::_($value);
					$value = JText::_($value);	
					if(in_array($key, $initial_features)){
						$pop_desc_init_array[$key]=$pop_desc;
					}					
					$pop_desc_array[$key]=$value;
				}
			}

			//$pop_desc = substr($pop_desc,0,strlen($pop_desc)-2);
			//var_dump($pop_desc_array); $properOrderedArray = array_merge(array_flip(array('name', 'dob', 'address')), $customer);
			//var_dump($initial_features);
			//var_dump(array_merge(array_flip($initial_features), $pop_desc_init_array));
			
			$pop_desc_init_array = array_merge(array_flip($initial_features), $pop_desc_init_array);

						
			$return_array['initial_feats'] = implode(", ", $pop_desc_init_array);
			$return_array['feats_array'] = $pop_desc_array;

			return $return_array;

		}

		function getBuyerActDate($d_format,$buyer_id){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select('at.*')
			->from('#__activities at')
			->order('date DESC')
			->where('at.buyer_id = ' . $buyer_id);
			$db->setQuery($query);
			$objects = $db->loadObjectList();
			$last_update = $objects[0]->date;
			$last_update = explode(" ", $last_update);
			$last_update = date($d_format,strtotime($last_update[0]));
			return $last_update;
		}
			public function get_totalave2012(){
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select('volume_2012,ur.country, c.currency, ur.user_id');
				$query->from('#__user_sales us');
				$query->where('verified_2012 = 1');
				$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');
				$query->leftJoin('#__country_currency c ON c.country  = ur.country');
				$db->setQuery($query);
				$totalave2012 = $db->loadObjectList();
				return $totalave2012;
			}
			public function get_totalave2013(){
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select('volume_2013,ur.country, c.currency, ur.user_id');
				$query->from('#__user_sales us');
				$query->where('verified_2013=1');
				$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');
				$query->leftJoin('#__country_currency c ON c.country  = ur.country');
				$db->setQuery($query);
				$totalave2013 = $db->loadObjectList();
				return $totalave2013;
			}
			public function get_totalave2014(){
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select('volume_2014, ur.country, c.currency, ur.user_id');
				$query->from('#__user_sales us');
				$query->where('verified_2014=1');
				$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');
				$query->leftJoin('#__country_currency c ON c.country  = ur.country');
				$db->setQuery($query);
				$totalave2014 = $db->loadObjectList();
				return $totalave2014;
			}
		function checkuserVolume($userid, $otheruser, $lid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('volume_2014');
			$query->from('#__user_sales us');
			$query->where('us.agent_id='.$userid);
			$db->setQuery($query);
			$vol2014 = $db->loadObject();
			$queryb = $db->getQuery(true);
			$queryb->select('ps.values');
			$queryb->from('#__permission_setting AS ps');
			$queryb->where('ps.listing_id='.$lid);
			$db->setQuery($queryb);
			$value = $db->loadObject();
			if ($vol2014>$value) {
			  $allowed==1;
			} else {
			  $allowed==$this->checkuserAccess($userid, $otheruser, $lid);
			}
			return	$allowed;		
		}
		function checkuserPrice($userid, $otheruser, $lid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('ave_price_2014');
			$query->from('#__user_sales us');
			$query->where('us.agent_id='.$userid);
			$db->setQuery($query);
			$ave2014 = $db->loadObject();
			$queryb = $db->getQuery(true);
			$queryb->select('ps.values');
			$queryb->from('#__permission_setting ps');
			$queryb->where('ps.listing_id='.$lid);
			$db->setQuery($queryb);
			$value = $db->loadObject();
			if ($ave2014>$value) {
			  $allowed==1;
			} else {
			   $allowed==$this->checkuserAccess($userid, $otheruser, $lid);
			}
			return	$allowed;		
		}
		function checkuserSides($userid, $otheruser, $lid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('sides2014');
			$query->from('#__user_sales us');
			$query->where('us.agent_id='.$userid);
			$db->setQuery($query);
			$sides2014 = $db->loadObject();
			$queryb = $db->getQuery(true);
			$queryb->select('ps.values');
			$queryb->from('#__permission_setting ps');
			$queryb->where('ps.listing_id='.$lid);
			$db->setQuery($queryb);
			$value = $db->loadObject();
			if ($sides2014>$value) {
			  $allowed==1;
			} else {
			   $allowed==$this->checkuserAccess($userid, $otheruser, $lid);
			}
			return	$allowed;		
		}
		function checkuserState($userid, $otheruser){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__users u');
			$query->leftjoin('#__user_registration ur on u.email = ur.email');
			$query->where('u.id='.$userid);
			$db->setQuery($query);
			$state = $db->loadObject()->state;
			$queryb = $db->getQuery(true);
			$queryb->select('*');
			$queryb->from('#__users u');
			$queryb->leftjoin('#__user_registration ur on u.email = ur.email');
			$queryb->where('u.id='.$otheruser);
			$db->setQuery($query);
			$stateother = $db->loadObject()->state;
			if ($state==$stateother) {
			  $allowed==1;
			} else {
			   $allowed==$this->checkuserAccess($userid, $otheruser, $lid);
			}
			return	$allowed;		
		}
		function checkuserCountry($userid, $otheruser){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__users u');
			$query->leftjoin('#__user_registration ur on u.email = ur.email');
			$query->where('u.id='.$userid);
			$db->setQuery($query);
			$country = $db->loadObject()->country;
			$queryb = $db->getQuery(true);
			$queryb->select('*');
			$queryb->from('#__users u');
			$queryb->leftjoin('#__user_registration ur on u.email = ur.email');
			$queryb->where('u.id='.$otheruser);
			$db->setQuery($query);
			$countryother = $db->loadObject()->country;
			if ($country==$countryother) {
			  $allowed==1;
			} else {
			   $allowed==$this->checkuserAccess($userid, $otheruser, $lid);
			}
			return	$allowed;		
		}
		function checkuserAccess($userid, $otheruser, $lid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__request_access ra');
			$query->where('ra.user_a=\''. $otheruser .'\'  AND ra.user_b=\''. $userid .'\' AND ra.property_id=\''. $lid .'\'');
			$db->setQuery($query);
			$permission = $db->loadObject()->permission;
			if ($permission==1) {
			  $allowed==1;
			} else if ($permission==0){
			  $allowed==0;
			} else {
				$allowed==0;
			}
			return	$allowed;		
		}
		function checkpermissionValue($userid, $otheruser, $lid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__request_access ra');
			$query->where('ra.user_a=\''. $otheruser .'\'  AND ra.user_b=\''. $userid .'\' AND ra.property_id=\''. $lid .'\'');
			$db->setQuery($query);
			$status = $db->loadObject()->permission;
			return $status;	
		}
		function CheckIfClosed($refid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__referral r');
			$query->where('(status = 4 OR status = 5 OR status = 9) AND referral_id='.$refid);
			$db->setQuery($query);
			$closed = $db->loadObjectList();
			return $closed;
		}
		function getR1Details($refid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('r.client_id, r.referral_id,b.buyer_id, b.name as client, u.id, u.name, u.email, ua.a_email, ua.cc_all,ur.gender,ur.firstname,ur.lastname,u.currLanguage');
			$query->from('#__referral r');
			$query->where('r.referral_id='.$refid);
			$query->leftJoin('#__users u on u.id = r.agent_a' );
			$query->leftjoin('#__user_registration ur ON ur.email = u.email ');
			$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
			$query->leftJoin('#__buyer b on b.buyer_id = r.client_id');
			$db->setQuery($query);
			$r1 = $db->loadObjectList();
			return $r1;
		}
		function getR2Details($refid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__referral r');
			$query->where('referral_id='.$refid);
			$query->leftJoin('#__users u on u.id = r.agent_b' );
			$db->setQuery($query);
			$r2 = $db->loadObjectList();
			return $r2;
		}
		function getReferralsSigned(){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__activities');
			$query->where('activity_type = 21');
			$db->setQuery($query);
			$signedbyuser1 = $db->loadObjectList();
			return $signedbyuser1;
		}
		function Checkr2Sign($refid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('activity_id');
			$query->from('#__activities');
			$query->where('activity_type = 20 AND activity_id ='.$refid);
			$db->setQuery($query);
			$signedbyuser2 = $db->loadObjectList();
			return $signedbyuser2;
		}

	function getSqMeasureByCountry($country){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('sqftMeasurement')->from('#__country_validations')->where(
			"country = ".$db->quote($country)
		);
		$db->setQuery($query);
		$sqmeasure = $db->loadResult();

		if($sqmeasure=="sq. ft."){
			$getColumn = "feet,feet";
		} else {
			$getColumn = "meter_round,feet";
		}

		$returnMeasure = array();

		$returnMeasure['sqmeasure'] = $sqmeasure;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_bldg');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['bldg'] = $db->loadRowList();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_lot');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['lot'] = $db->loadRowList();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_bldg_wide');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['bldg_wide'] = $db->loadRowList();

		return $returnMeasure;

	}

	function check_existing_pops( $listing_id, $buyer_id, $active ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__buyer_saved_listing')->where(
			"
			buyer_id = ".$buyer_id."
			AND agent_id = ".JFactory::getUser()->id."
			AND listing_id = ".$listing_id."
			AND active = ".$active 
		);
		$db->setQuery($query);
		$pops_count = $db->loadObject()->count;
		return $pops_count;
	}
	function getMCcities(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('cities');
		$query->from('#__country_monaco_cities');
		$query->where('country_id = 141');
		$db->setQuery($query);
		$mccities = $db->loadColumn();
		return $mccities;
	}
	function getCCities($country_id, $postcode){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('cities');
		$query->from('#__country_postcode_cities');
		$query->where('country = '.$country_id.' AND postcode = '.$postcode);
		$db->setQuery($query);
		$mccities = $db->loadColumn();
		return $mccities;
	}
	function expiredPOPDb($listing_id){
			$db = JFactory::getDbo();
			$update = new JObject();
			$update->listing_id = $listing_id;
			$update->closed = 1;
			$update->date_expired = date("Y-m-d");
			$upd = $db->updateObject('#__pocket_listing', $update, 'listing_id', false);
			return $upd;
	}	
	function soldPOPDb($listing_id){
			$db = JFactory::getDbo();
			$update = new JObject();
			$update->listing_id = $listing_id;
			$update->sold = 1;
			$upd = $db->updateObject('#__pocket_listing', $update, 'listing_id', false);
			return $upd;
	}
	function unSoldPOPDb($listing_id){
			$db = JFactory::getDbo();
			$update = new JObject();
			$update->listing_id = $listing_id;
			$update->sold = 0;
			$upd = $db->updateObject('#__pocket_listing', $update, 'listing_id', false);
			return $upd;
	}
	function inclosed_count( $user_id ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__referral')->where(
			"
				(status = 4 AND agent_b = $user_id) OR (status = 9 AND agent_a = $user_id) OR (status = 9 AND agent_b = $user_id)
			"
		);
		// echo $query
		// die();
		$db->setQuery($query);
		$refin_count = $db->loadObject()->count;
	    return $refin_count;
	}
	public function activate($token)
	{
		$config	= JFactory::getConfig();
		$userParams	= JComponentHelper::getParams('com_users');
		$db		= $this->getDbo();
		// Get the user id based on the token.
		$db->setQuery(
				'SELECT '.$db->quoteName('id').' FROM '.$db->quoteName('#__users') .
				' WHERE '.$db->quoteName('activation').' = '.$db->Quote($token) .
				' AND '.$db->quoteName('block').' = 1' .
				' AND '.$db->quoteName('lastvisitDate').' = '.$db->Quote($db->getNullDate())
		);
		$userId = (int) $db->loadResult();
		// Check for a valid user id.
		if (!$userId)
		{
			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
			return false;
		}
		// Load the users plugin group.
		JPluginHelper::importPlugin('user');
		// Activate the user.
		$user = JFactory::getUser($userId);
		// Admin activation is on and user is verifying their email
		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))
		{
			$uri = JURI::getInstance();
			// Compile the admin notification mail values.
			$data = $user->getProperties();
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$user->set('activation', $data['activation']);
			$data['siteurl']	= JUri::base();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$user->setParam('activate', 1);
			$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',
					$data['name'],
					$data['sitename']
			);
			$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',
					$data['sitename'],
					$data['name'],
					$data['email'],
					$data['username'],
					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation']
			);
			// get all admin users
			$query = 'SELECT name, email, sendEmail, id' .
					' FROM #__users' .
					' WHERE sendEmail=1';
			$db->setQuery($query);
			$rows = $db->loadObjectList();
			// Send mail to all users with users creating permissions and receiving system emails
			foreach ($rows as $row)
			{
				$usercreator = JFactory::getUser($row->id);
				if ($usercreator->authorise('core.create', 'com_users'))
				{
					$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);
					// Check for an error.
					if ($return !== true)
					{
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
						return false;
					}
				}
			}
		}
		//Admin activation is on and admin is activating the account
		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))
		{
			$user->set('activation', '');
			$user->set('block', '0');
			$uri = JURI::getInstance();
			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			$user->setParam('activate', 0);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl']	= JUri::base();
			$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',
					$data['name'],
					$data['sitename']
			);
			$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['siteurl'],
					$data['username']
			);
			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);
			// Check for an error.
			if ($return !== true)
			{
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}
		}
		else
		{
			$user->set('activation', '');
			$user->set('block', '0');
		}
		// Store the user object.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));
			return false;
		}
		return $user;
	}
	/**
	 * Method to get the registration form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return  mixed  	Data object on success, false on failure.
	 * @since   1.6
	 */
	public function getData()
	{
		if ($this->data === null)
		{
			$this->data	= new stdClass;
			$app	= JFactory::getApplication();
			$params	= JComponentHelper::getParams('com_users');
			// Override the base user data with any data in the session.
			$temp = (array) $app->getUserState('com_users.registration.data', array());
			foreach ($temp as $k => $v)
			{
				$this->data->$k = $v;
			}
			// Get the groups the user should be added to after registration.
			$this->data->groups = array();
			// Get the default new user group, Registered if not specified.
			$system	= $params->get('new_usertype', 2);
			$this->data->groups[] = $system;
			// Unset the passwords.
			unset($this->data->password1);
			unset($this->data->password2);
			// Get the dispatcher and load the users plugins.
			$dispatcher	= JEventDispatcher::getInstance();
			JPluginHelper::importPlugin('user');
			// Trigger the data preparation event.
			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));
			// Check for errors encountered while preparing the data.
			if (count($results) && in_array(false, $results, true))
			{
				$this->setError($dispatcher->getError());
				$this->data = false;
			}
		}
		return $this->data;
	}
	/**
	 * Method to get the registration form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}
		return $form;
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		return $this->getData();
	}
	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param   object	A form object.
	 * @param   mixed	The data expected for the form.
	 * @throws	Exception if there is an error in the form event.
	 * @since   1.6
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams	= JComponentHelper::getParams('com_users');
		//Add the choice for site language at registration time
		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)
		{
			$form->loadFile('sitelang', false);
		}
		parent::preprocessForm($form, $data, $group);
	}
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$app	= JFactory::getApplication();
		$params	= $app->getParams('com_users');
		// Load the parameters.
		$this->setState('params', $params);
	}
	/**
	 * Method to save the form data.
	 *
	 * @param   array  The form data.
	 * @return  mixed  	The user id on success, false on failure.
	 * @since   1.6
	 */
	public function register($temp)
	{
		$config = JFactory::getConfig();
		$db		= $this->getDbo();
		$params = JComponentHelper::getParams('com_setpass');
		// Initialise the table with JUser.
		$user = new JUser;
		$data = (array) $this->getData();
		// Merge in the registration data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}
		// Prepare the data for the user object.
		$data['email']		= $data['email1'];
		$data['password']	= $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);
		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2))
		{
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}
		// Bind the data.
		if (!$user->bind($data))
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}
		// Load the users plugin group.
		JPluginHelper::importPlugin('user');
		// Store the data.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
			return false;
		}
		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname']	= $config->get('fromname');
		$data['mailfrom']	= $config->get('mailfrom');
		$data['sitename']	= $config->get('sitename');
		$data['siteurl']	= JUri::root();
		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);
			$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
			);
			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username'],
						$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username']
				);
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JURI::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);
			$emailSubject	= JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
			);
			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username'],
						$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
						$data['name'],
						$data['sitename'],
						$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],
						$data['siteurl'],
						$data['username']
				);
			}
		}
		else
		{
			$emailSubject	= JText::sprintf('Welcome to AgentBridge!');
			if ($sendpassword)
			{
				$emailBody = "Dear ".$data['name'].",<br /><br /> Congratulations.  You are now a member of AgentBridge, the most powerful network of real estate professionals in the world.<br /><br />
						Click this link to access your account and take advantage of your membership privileges.<br />
						<a href='https://www.agentbridge.com/index.php/component/users/?view=login'>AgentBridge Log in</a>
						<br /><br /> Sincerely, <br/><br/>The AgentBridge Team<br/><a href='mailto:service@agentbridge.com'>service@AgentBridge.com</a>";
			}
			else
			{
				$emailBody = JText::sprintf(
						'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',
						$data['name'],
						$data['sitename'],
						$data['siteurl']
				);
			}
		}
		// Send the registration email.
		$sendmail = JFactory::getMailer();
		$sendmail->isHTML( true );
		if($sendmail->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody)){
			$return = true;
		}
		//Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
					'COM_USERS_EMAIL_ACCOUNT_DETAILS',
					$data['name'],
					$data['sitename']
			);
			$emailBodyAdmin = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
					$data['name'],
					$data['username'],
					$data['siteurl']
			);
			// get all admin users
			$query = 'SELECT name, email, sendEmail' .
					' FROM #__users' .
					' WHERE sendEmail=1';
			$db->setQuery($query);
			$rows = $db->loadObjectList();
			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);
				// Check for an error.
				if ($return !== true)
				{
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}
		// Check for an error.
		if ($return !== true)
		{
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));
			// Send a system message to administrators receiving system mails
			$db = JFactory::getDBO();
			$q = "SELECT id
					FROM #__users
					WHERE block = 0
					AND sendEmail = 1";
			$db->setQuery($q);
			$sendEmail = $db->loadColumn();
			if (count($sendEmail) > 0)
			{
				$jdate = new JDate;
				// Build the query to add the messages
				$q = "INSERT INTO ".$db->quoteName('#__messages')." (".$db->quoteName('user_id_from').
				", ".$db->quoteName('user_id_to').", ".$db->quoteName('date_time').
				", ".$db->quoteName('subject').", ".$db->quoteName('message').") VALUES ";
				$messages = array();
				foreach ($sendEmail as $userid)
				{
					$messages[] = "(".$userid.", ".$userid.", '".$jdate->toSql()."', '".JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')."', '".JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])."')";
				}
				$q .= implode(',', $messages);
				$db->setQuery($q);
				$db->execute();
			}
			return false;
		}
		if ($useractivation == 1)
		{
			return "useractivate";
		}
		elseif ($useractivation == 2)
		{
			return "adminactivate";
		}
		else
		{
			return $user->id;
		}
	}
	public function get_property_image($lID){
		$db		= $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__pocket_images');
		$query->where('listing_id LIKE \''. $lID .'\' 
			ORDER BY CASE 
		    WHEN order_image = 0 THEN image_id
		    ELSE order_image
		END ASC');
		//$query->order('order_image ASC');
		$db->setQuery($query);
		$images = $db->loadObjectList();
		return $images;
	}
	//count the number of buyer/s of logged in user
	public function count_user_buyers() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__buyer')->where(array_merge(array('agent_id = '.JFactory::getUser()->id)));
		$buyers = $db->setQuery($query)->loadObjectList();
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		$buyerctr = 0;
		foreach($buyers as $buyer){
			if(!empty($buyer->needs)){
				$buyerctr++;
			}
		}
		return $buyerctr;
	}	
	public function getViews($lid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(distinct(user_id)) as views');
		$query->from('#__views');
		$query->where('property_id='.$lid);
		$query->order('property_id desc');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_listing($filters){
		$conditions = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('p.*,ps.*,pp.*,pa.*,pi.*,v.*,re.*,viewers.*,pt.*,pst.*, conc.symbol, conc.currency, cvs.*, pst.name AS subtype_name , pt.type_name AS proptype_name,
			p.listing_id as listing_id,
			con.countries_iso_code_2 AS conIso,
			con.countries_id AS conCode,
			p.property_name as property_name
		');
		$query->from('#__pocket_listing p');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." > ".$value;
				else{
					$explode = explode('-', $value);
					if(count($explode)>1){
						$conditions[] = $key."1 >= ".$explode[0]." AND ".$key."2 =< ".$explode[1];
					}
					else
						$conditions[] = $key."1 > ".$value;
				}
			}
		}
		if(count($conditions)>0)
			$query->where($conditions);
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image ORDER BY image_id ASC) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed
							FROM #__request_access racs
							WHERE permission = 1
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');
		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = p.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = p.sub_type');
		$query->order('p.setting desc');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	//EXPANDSEARCH
	public function get_pocket_listing_indi_filter2($filters, $buyers){
		$conditions = array();
		#echo "<pre>"; print_r($buyers[0]->needs[0]->bedroom); die();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("*, p.listing_id, CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) AS LowestSqft, CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER), CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER), IFNULL(pp.price2,'exact_price') as abcd");
		$query->from('#__pocket_listing p');
		$query->where('expiry!=0');
		$searchzip = substr($buyers[0]->needs[0]->zip, 0, 4);
		foreach ($filters as $key => $value){
			#print_r($filters);
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id") {
					$conditions[] = $key." = ".$value;
				} else if($key!="pp.price") {
					$conditions[] = $key." > ".$value;
				} else {
					$property_type = $buyers[0]->needs[0]->property_type;
					$sub_type = $buyers[0]->needs[0]->sub_type;
					$conditions[] = "(p.zip != '" . $buyers[0]->needs[0]->zip . "' AND p.zip LIKE '".$searchzip."%') AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$buyers[0]->needs[0]->country."'";
						switch($property_type){
						case 1:
						if($sub_type == 1){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		
						} else if($sub_type == 2){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			
						} else if($sub_type == 3){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";				
						} else if($sub_type == 4){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		
						}
						break;
						case 2:
						if($sub_type == 5){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.pet >='".$buyers[0]->needs[0]->pet."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."' AND p.possession >='".$buyers[0]->needs[0]->possession."'";
						} else if($sub_type == 6){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."'  AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 7){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 8){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->needs[0]->view."'";
						}
						break;
						case 3:
						if($sub_type == 9){
							$unitsrange = $buyers[0]->needs[0]->units;
							$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
							$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 10){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 11){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 12){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 13){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 14){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 15){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$conditions[] = "p.type='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";
						}
						break;
					case 4:
						if($sub_type == 16){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 17){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.type_lease >='".$buyers[0]->needs[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 18){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						}
						break;
					}
					//echo $key; die();
					$explode = explode('-', $value);
	         		$conditions[] = $key."1 <= ".$explode[1];
				}
			}
		}
		if(count($conditions)>0) {
			$query->where($conditions);
		}
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							WHERE racs.user_b = ' . $buyers[0]->agent_id . '
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->order('p.setting asc');
		$db->setQuery($query);
		$original_array=$db->loadObjectList();
		$i=0;
		foreach ($original_array as $key => $value) {
				if($value->price1 < $explode[0] && ($value->price2 == null || $value->price2 == "")){
					unset($original_array[$i]);
				}
				$i++;
		}
		return $original_array;
		die();
	}
	//MAINSEARCH OPTI
	public function get_pocket_listing_indi_filter_opti($filters, $buyers){
		$conditions = array();
		$db = JFactory::getDbo();
		$today = date ('Y-m-d');
		//get listing details
		$query = $db->getQuery(true);
		$query->select("p.*, pp.price_type, p.listing_id, currcon.*, IFNULL(pp.price2,'exact_price') as abcd, cvs.*, u.name AS username, ps.*");
		$query->from('#__pocket_listing p');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');
		$query->where('DATE(p.date_expired) > \''. $today .'\' AND p.closed = 0');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id") {
					if($key=="p.user_id") {
						$user_id = $value;	
					}					
					$conditions[] = $key." = ".$value;
				} else if($key!="pp.price") {
					$conditions[] = $key." > ".$value;
				} else{
					$property_type = $buyers[0]->property_type;
					$sub_type = $buyers[0]->sub_type;		
					$country = 	$buyers[0]->country;			
					// zip extract
					$zip = explode(",",$buyers[0]->zip);
					$zip_cnt=0;
					$zip_condition = "";
					foreach($zip as $row){
						if($zip_cnt<count($zip)){
							if($country==222){
								$zip_cnt++;
								$zip_condition .= "p.zip LIKE '".trim($row)."%'";
							} else if($country==73){
								$zip_cnt++;
								$zip_condition .= "(p.zip LIKE '".trim($row)."%' AND p.country = 73)";
							} else {
								$zip_cnt++;
								$zip_condition .= "p.zip ='".$row."'";
							}
						}
						if($zip_cnt<count($zip)){
							$zip_condition .=" OR ";
						}
					}
					//if(count($zip)>=2){
						$conditions[] = "(".$zip_condition.") AND p.property_type='".$buyers[0]->property_type."' AND p.sub_type='".$buyers[0]->sub_type."' AND p.country ='".$country."'";
					//} else {
					//	$conditions[] = "p.zip='".$buyers[0]->zip."' AND p.property_type='".$buyers[0]->property_type."' AND p.sub_type='".$buyers[0]->sub_type."' AND p.country ='".$country."'";
					//}
					switch($property_type){
						case 1:
						if($sub_type == 1){
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		
						} else if($sub_type == 2){
							$unitrange = $buyers[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			
						} else if($sub_type == 3){
							$unitrange = $buyers[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";				
						} else if($sub_type == 4){
							$zrange = $buyers[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		
						}
						break;
						case 2:
						if($sub_type == 5){
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->term."' AND p.pet >='".$buyers[0]->pet."' AND p.furnished >='".$buyers[0]->furnished."' AND p.possession >='".$buyers[0]->possession."'";
						} else if($sub_type == 6){
							$unitrange = $buyers[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->term."'  AND p.furnished >='".$buyers[0]->furnished."'  AND p.pet >='".$buyers[0]->pet."'";
						} else if($sub_type == 7){
							$unitrange = $buyers[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->bedroom."' AND p.bathroom >='".$buyers[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->term."' AND p.furnished >='".$buyers[0]->furnished."'  AND p.pet >='".$buyers[0]->pet."'";
						} else if($sub_type == 8){
							$zrange = $buyers[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->view."'";
						}
						break;
						case 3:
						if($sub_type == 9){
							$unitsrange = $buyers[0]->units;
							$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
							$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 10){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->type."' AND p.listing_class >='".$buyers[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 11){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 12){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 13){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 14){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 15){
							$caprange = $buyers[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$conditions[] = "p.type='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";
						}
						break;
					case 4:
						if($sub_type == 16){
							$avlbrange = $buyers[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->type."' AND p.listing_class >='".$buyers[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 17){
							$avlbrange = $buyers[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->type."' AND p.type_lease >='".$buyers[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 18){
							$bldgrange = $buyers[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						}
						break;
					}
					$explode = explode('-', $value);
					//	if($value->currency != $buyers[0]->currency){
					//		$explode[1] = $this->getExchangeRates($explode[1],$buyers[0]->currency,$value->currency);
					//}
					//	$conditions[]= $key."1 <= ".$explode[1];
				}
			} 
		}
		if(count($conditions)>0) {
			$query->where($conditions);
		}
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('#__users u on u.id = p.user_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		/*$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							WHERE racs.user_b = ' . $buyers[0]->agent_id . '
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');*/ 
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->group('p.listing_id');
		$db->setQuery($query);
		$original_array=$db->loadObjectList();
		$i=0;
		$ex_rates = $this->getExchangeRates_indi();
		foreach ($original_array as $key => $value) {
				if($value->user_id != JFactory::getUser()->id){
					if($value->currency != $buyers[0]->currency){
						$ex_rates_con = $ex_rates->rates->{$value->currency};
						if(!$buyers[0]->currency){
							$buyers[0]->currency ="USD";
						}
						$ex_rates_can = $ex_rates->rates->{$buyers[0]->currency};
						if($value->currency=="USD"){					
							$value->price1=$value->price1 * $ex_rates_can;
						} else {
							$value->price1=($value->price1 / $ex_rates_con) * $ex_rates_can;							
						}
						if(($value->price2 != null || $value->price2 != "")){
							if($value->currency=="USD"){					
								$value->price2=$value->price2 * $ex_rates_can;
							} else {
								$value->price2=($value->price2 / $ex_rates_con) * $ex_rates_can;							
							}
						}
						if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
					}
				} else {
					if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
				}
				if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
					unset($original_array[$i]);
				}
				$i++;
		}
		return $original_array;
		die();
	}

	public function get_columns_by_subtype($subtype){

		$initial_features = array();

			if($subtype==1){ //residential purchase + sfr
				$initial_features = array( 
									"bedroom",
									"bathroom",								
									"bldg_sqft"																
									);

			} else if($subtype==2 || $subtype==3) { //residential purchase + condo / townhouse
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"unit_sqft",															
									);

			} else if($subtype==4) { //residential purchase + land
				$initial_features = array( 
									"lot_size",
									"view",
									"zoned"							
									);
				
			} else if($subtype==5) { //residential lease + sfr
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"bldg_sqft",
									"term",
									"possession",
									"pet",
									"furnished"								
									);
				
			} else if($subtype==6 || $subtype==7) { //residential lease + condo / townhouse
				$initial_features = array( 
									"bedroom",
									"bathroom",
									"unit_sqft",
									"term",
									"furnished",
									"pet"								
									);
				
			} else if($subtype==8) { //residential lease + land
				$initial_features = array( 
									"lot_size",
									"zoned",
									"view"							
									);

			} else if($subtype==9) { //comm purchase + multi family
				$initial_features = array( 
									"units",
									"cap_rate",
									"bldg_sqft"							
									);
				
			} else if($subtype==10) { //comm purchase + office
				$initial_features = array( 
									"type",
									"listing_class",
									"cap_rate",
									"bldg_sqft"						
									);
				
			} else if($subtype==11 || $subtype==12) { //comm purchase + industrial / retail
				$initial_features = array( 
									"type",
									"cap_rate",
									"bldg_sqft"						
									);
				
			} else if($subtype==13 || $subtype==14) { //comm purchase + hotel / assisted care
				$initial_features = array( 
									"type",
									"cap_rate",
									"room_count"						
									);
				
			} else if($subtype==15) { //comm purchase + special purpose
				$initial_features = array( 
									"type",
									"cap_rate"						
									);
				
			} else if($subtype==16) { //comm lease + office
				$initial_features = array( 
									"type",
									"listing_class",
									"available_sqft"						
									);
				
			} else if($subtype==17) { //comm lease + industrial
				$initial_features = array( 
									"type",
									"type_lease",
									"available_sqft"						
									);
				
			} else if($subtype==18) {
				$initial_features = array( 
									"type",
									"bldg_sqft"						
									);
				
			} else if(!$subtype){
				$initial_features = array();
			}

			return $initial_features;

	}

	public function match_buyer_to_pops($buyer,$columns_query){

		$country = 	$buyer->country;			
		// zip extract
		$zip = explode(",",$buyer->zip);
		$zip_cnt=0;
		$zip_condition = "";
		foreach($zip as $row){
			if($zip_cnt<count($zip)){
				if($country==222){
					$zip_cnt++;
					$zip_condition .= "p.zip LIKE '".trim($row)."%'";
				} else if($country==73){
					$zip_cnt++;
					$zip_condition .= "(p.zip LIKE '".trim($row)."%' AND p.country = 73)";
				} else {
					$zip_cnt++;
					$zip_condition .= "p.zip ='".$row."'";
				}
			}
			if($zip_cnt<count($zip)){
				$zip_condition .=" OR ";
			}
		}


		$conditions = array();
		$db = JFactory::getDbo();
		$today = date ('Y-m-d');
		//get listing details
		$query = $db->getQuery(true);

		$query->select($columns_query);
		$query->from('#__pocket_listing p');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = p.currency');
		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(device_token) as device_tokens, user_id from #__user_device_token where device_os = "android" group by user_id) ud on p.user_id =  ud.user_id');
		$query->where('DATE(p.date_expired) > \''. $today .'\' AND p.closed = 0 AND p.sold = 0 AND p.sub_type='.$buyer->sub_type." AND (".$zip_condition.")");
		// $query->setLimit(30,0);
		$db->setQuery($query);




		// $mailSender =& JFactory::getMailer();
		// $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
		// $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		// $mailSender ->setSubject( "edit buyer  raw query used" );
		// $mailSender ->isHTML(  true );
		// $mailSender ->setBody( print_r($query->__toString(),true));
		// $mailSender ->Send(); 

		$original_array=$db->loadObjectList();


		
		$ex_rates = $this->getExchangeRates_indi();
		$i=0;
		foreach ($original_array as $key => $value) {
				$explode = explode('-', $buyer->price_value);
				if($value->user_id != JFactory::getUser()->id){
					if($value->currency != $buyer->currency){
						$ex_rates_con = $ex_rates->rates->{$value->currency};
						if(!$buyer->currency){
							$buyer->currency ="USD";
						}
						$ex_rates_can = $ex_rates->rates->{$buyer->currency};
						if($value->currency=="USD"){					
							$value->price1=$value->price1 * $ex_rates_can;
						} else {
							$value->price1=($value->price1 / $ex_rates_con) * $ex_rates_can;							
						}
						if(($value->price2 != null || $value->price2 != "")){
							if($value->currency=="USD"){					
								$value->price2=$value->price2 * $ex_rates_can;
							} else {
								$value->price2=($value->price2 / $ex_rates_con) * $ex_rates_can;							
							}
						}
						if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
					}
				} else {
					if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
				}
				if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
					unset($original_array[$i]);
				}
				$i++;
		}

		$sub_type=$buyer->sub_type;
		$t=1;
		
		$pre_filter = print_r($original_array, true);
		$arr_match = array();
	//	var_dump($columns_query);
		foreach ($original_array as $key => $value) {
			# code...
			if($sub_type == 1){
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				// $conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		
				if(!(
					intval($value->bedroom) >= intval($buyer->bedroom) && 
					intval($value->bathroom) >= intval($buyer->bathroom) && 
					((!is_numeric($bldglow) && $bldglow == $value_bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 2 || $sub_type == 3){
				$unitrange = $buyer->unit_sqft;
				$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
				//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
				$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
				
				$value_unitexplode = explode("-", $value->unit_sqft);
				//$value_unitlow = str_replace(",", "", $value_unitexplode[0]);
				$value_unitlow = preg_replace('/[^\d.]/', '', $value_unitexplode[0]);
				//$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			
				if(!(
					intval($value->bedroom) >= intval($buyer->bedroom) && 
					intval($value->bathroom) >= intval($buyer->bathroom) && 
					((!is_numeric($unitlow) && $unitlow == $value_unitlow) || (is_numeric($unitlow) && intval($value_unitlow) >= intval($unitlow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 4){
				$zrange = $buyer->zoned;
				$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
				$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
				$lotszrange = $buyer->lot_size;
				$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
				//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
				$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
				
				$value_zexplode = explode("-", $value->zoned);
				$value_zlow = str_replace(",", "", $value_zexplode[0]);
				
				$value_lotszexplode = explode("-", $value->lot_size);
				//$value_lotszlow = str_replace(",", "", $value_lotszexplode[0]);
				$value_lotszlow = preg_replace('/[^\d.]/', '', $value_lotszexplode[0]);
				
				//$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		
				if(!(
					intval($value_zlow) >= intval($zlow) && 
					((!is_numeric($lotszlow) && $lotszlow == $value_lotszlow) || (is_numeric($lotszlow) && intval($value_lotszlow) >= intval($lotszlow)))					
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 5){
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				//$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.pet >='".$buyers[0]->needs[0]->pet."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."' AND p.possession >='".$buyers[0]->needs[0]->possession."'";
				if(!(
					intval($value->bedroom) >= intval($buyer->bedroom) && 
					intval($value->bathroom) >= intval($buyer->bathroom) && 
					((!is_numeric($bldglow) && $bldglow == $value_bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow))) && 
					$value->term == $buyer->term && 
					intval($value->pet) == intval($buyer->pet) && 
					intval($value->furnished) == intval($buyer->furnished) && 
					$value->possession == $buyer->possession
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 6 || $sub_type == 7){
				$unitrange = $buyer->unit_sqft;
				$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
				//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
				$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
				
				$value_unitexplode = explode("-", $value->unit_sqft); // split the ranged value into two, returning an array
				//$value_unitlow = str_replace(",", "", $value_unitexplode[0]); //lower range for buyer
				$value_unitlow = preg_replace('/[^\d.]/', '', $value_unitexplode[0]);
				
				// $conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."'  AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
				
				if(!(
					intval($value->bedroom) >= intval($buyer->bedroom) && 
					intval($value->bathroom) >= intval($buyer->bathroom) && 
					((!is_numeric($unitlow) && $unitlow == $value_unitlow) || (is_numeric($unitlow) && intval($value_unitlow) >= intval($unitlow))) && 
					$value->term == $buyer->term && 
					intval($value->pet) == intval($buyer->pet) && 
					intval($value->furnished) == intval($buyer->furnished)
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 8){
				$zrange = $buyer->zoned;
				$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
				$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
				
				$lotszrange = $buyers->lot_size;
				$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
				//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
				$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
				
				$value_zexplode = explode("-", $value->zoned);
				$value_zlow = str_replace(",", "", $value_zexplode[0]);
				
				$value_lotszexplode = explode("-", $value->lot_size);
				//$value_lotszlow = str_replace(",", "", $value_lotszexplode[0]);
				$value_lotszlow = preg_replace('/[^\d.]/', '', $value_lotszexplode[0]);
				
				// $conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->needs[0]->view."'";
				if(!(
					intval($value_zlow) >= intval($zlow) && 
					((!is_numeric($lotszlow) && $lotszlow == $value_lotszlow) || (is_numeric($lotszlow) && intval($value_lotszlow) >= intval($lotszlow))) && 
					$value->view == $buyer->view
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 9){
				$unitsrange = $buyer->units;
				$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
				//$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
				$unitslow = preg_replace('/[^\d.]/', '', $unitsexplode[0]);
				
				$caprange = $buyer->cap_rate;
				$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
				$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
				
				
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				
				$value_unitsexplode = explode("-", $value->units);
				//$value_unitslow = str_replace(",", "", $value_unitsexplode[0]);
				$value_unitslow = preg_replace('/[^\d.]/', '', $value_unitsexplode[0]);
				
				$value_capexplode = explode("-", $value->cap_rate);
				$value_caplow = preg_replace('/[^\d.]/', '', $value_capexplode[0]);
				
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				
				//$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
				if(!(
					((!is_numeric($unitslow) && $value_unitslow == $unitslow) || (is_numeric($unitslow) && intval($value_unitslow) >= intval($unitslow))) && 
					((!is_numeric($caplow) && $value_caplow == $caplow) || (is_numeric($caplow) && $value_caplow >= $caplow)) &&
					((!is_numeric($bldglow) && $bldglow == $value_bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 10){
				$caprange = $buyer->cap_rate;
				$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
				$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
				
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				
				$value_capexplode = explode("-", $value->cap_rate);
				$value_caplow = preg_replace('/[^\d.]/', '', $value_capexplode[0]);
				
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				//$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
				if(!(
					$value->type == $buyer->type &&
					((!is_numeric($buyer->listing_class) && $value->listing_class == $buyer->listing_class) || (is_numeric($buyer->listing_class) && $value->listing_class >= $buyer->listing_class)) &&
					((!is_numeric($caplow) && $value_caplow == $caplow) || (is_numeric($caplow) && $value_caplow >= $caplow)) &&
					((!is_numeric($bldglow) && $bldglow == $value_bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 11 || $sub_type == 12){
				$caprange = $buyer->cap_rate;
				$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
				$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
				
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				
				$value_capexplode = explode("-", $value->cap_rate);
				$value_caplow = preg_replace('/[^\d.]/', '', $value_capexplode[0]);
				
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				// $conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
				if(!(
					$value->type == $buyer->type && 
					((!is_numeric($caplow) && $value_caplow == $caplow) || (is_numeric($caplow) && $value_caplow >= $caplow)) &&
					((!is_numeric($bldglow) && $bldglow == $value_bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 13 || $sub_type == 14){
				$caprange = $buyer->cap_rate;
				$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
				$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
				
				$roomrange = $buyer->room_count;
				$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
				$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
				
				$value_capexplode = explode("-", $value->cap_rate);
				$value_caplow = preg_replace('/[^\d.]/', '', $value_capexplode[0]);
				
				$value_roomexplode = explode("-", $value->room_count);
				$value_roomlow = str_replace("-", "", $value_roomexplode[0]);
				
				// $conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
				if(!(
					$value->type == $buyer->type && 
					((!is_numeric($caplow) && $value_caplow == $caplow) || (is_numeric($caplow) && $value_caplow >= $caplow)) && 
					intval($value_roomlow) >= intval($roomlow)
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 15){
				$caprange = $buyer->cap_rate;
				$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
				$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
				
				$value_capexplode = explode("-", $value->cap_rate);
				$value_caplow = preg_replace('/[^\d.]/', '', $value_capexplode[0]);
				
				// $conditions[] = "p.type='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";
				if(!(
					$value->type == $buyer->type && 
					((!is_numeric($caplow) && $value_caplow == $caplow) || (is_numeric($caplow) && $value_caplow >= $caplow))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 16){
				$avlbrange = $buyer->available_sqft;
				$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
				//$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
				$avlblow = preg_replace('/[^\d.]/', '', $avlbexplode[0]);
				
				$value_avlbexplode = explode("-", $value->available_sqft);
				//$value_avlblow = str_replace(",", "", $value_avlbexplode[0]);
				$value_avlblow = preg_replace('/[^\d.]/', '', $value_avlbexplode[0]);
				
				// $conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
				if(!(
					$value->type == $buyer->type && 
					((!is_numeric($buyer->listing_class) && $value->listing_class == $buyer->listing_class) || (is_numeric($buyer->listing_class) && $value->listing_class >= $buyer->listing_class)) &&
					((!is_numeric($avlblow) && $value_avlblow == $avlblow) || (is_numeric($avlblow) && intval($value_avlblow) >= intval($avlblow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 17){
				$avlbrange = $buyer->available_sqft;
				$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
				//$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
				$avlblow = preg_replace('/[^\d.]/', '', $avlbexplode[0]);
				
				$value_avlbexplode = explode("-", $value->available_sqft);
				//$value_avlblow = str_replace(",", "", $value_avlbexplode[0]);
				$value_avlblow = preg_replace('/[^\d.]/', '', $value_avlbexplode[0]);
				
				// $conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.type_lease >='".$buyers[0]->needs[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
				if(!(
					$value->type == $buyer->type && 
					$value->type_lease == $buyer->type_lease && 
					((!is_numeric($avlblow) && $value_avlblow == $avlblow) || (is_numeric($avlblow) && intval($value_avlblow) >= intval($avlblow)))
				)){
					unset($original_array[$key]);
				}
			} else if($sub_type == 18){
				$bldgrange = $buyer->bldg_sqft;
				$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
				//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
				$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
				
				$value_bldgexplode = explode("-", $value->bldg_sqft);
				//$value_bldglow = str_replace(",", "", $value_bldgexplode[0]);
				$value_bldglow = preg_replace('/[^\d.]/', '', $value_bldgexplode[0]);
				
				//$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
				if(!(
					$value->type == $buyer->type && 
					((!is_numeric($bldglow) && $value_bldglow == $bldglow) || (is_numeric($bldglow) && intval($value_bldglow) >= intval($bldglow)))
				)){
					unset($original_array[$key]);
				}
			}
			$t++;
					
		}
		
		$post_filter = print_r($original_array, true);
		
		$mailSender =& JFactory::getMailer();
		//$mailSender ->addRecipient( "rroque@agentbridge.com" );
		$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "Buyer Matching results" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( print_r($buyer, true) . "<br />==========================<br />" . $post_filter . "<br />==========================<br />" . $data_orig_array );
		$mailSender ->Send(); 
	
		return $original_array;

	}

	public function match_pop_to_buyers($propertydata,$listing_id,$user_android_token,$transaction="add"){

		$property = new stdClass();
		
		$model = $this;


		$property->listing_id = $listing_id;
		$property->property_name = $propertydata['property_name'];
		$property->price_value = $propertydata['price1']."-".$propertydata['price2'];
		$property->price1 = $propertydata['price1'];
		$property->price2 = $propertydata['price2'];
		$property->price_type = $propertydata['price2'] ? 1 : 2;
		$property->city = $propertydata['city'];
		$property->zip = $propertydata['zip'];
		$property->state = $propertydata['state'];
		$property->country = $propertydata['country'];
		$property->currency = $propertydata['currency'];
		$property->property_type = $propertydata['ptype'];
		$property->sub_type = $propertydata['stype'];
		$property->bedroom = $propertydata['bedroom'];
		$property->bathroom = $propertydata['bathroom'];
		$property->unit_sqft = $propertydata['unitsqft'];
		$property->view = $propertydata['view'];
		$property->style = $propertydata['style'];
		$property->year_built = $propertydata['yearbuilt'];
		$property->pool_spa = $propertydata['poolspa'];
		$property->condition = $propertydata['condition'];
		$property->garage = $propertydata['garage'];
		$property->units = $propertydata['units'];
		$property->cap_rate = $propertydata['cap'];
		$property->grm = $propertydata['grm'];
		$property->occupancy = $propertydata['occupancy'];
		$property->type = $propertydata['type'];
		$property->listing_class = $propertydata['class'];
		$property->parking_ratio = $propertydata['parking'];
		$property->ceiling_height = $propertydata['ceiling'];
		$property->stories = $propertydata['stories'];
		$property->room_count = $propertydata['roomcount'];
		$property->type_lease = $propertydata['typelease'];
		$property->type_lease2 = $propertydata['typelease2'];
		$property->available_sqft = $propertydata['available'];
		$property->lot_sqft = $propertydata['lotsqft'];
		$property->lot_size = $propertydata['lotsize'];
		$property->bldg_sqft = $propertydata['bldgsqft'];
		$property->term = $propertydata['term'];
		$property->furnished = $propertydata['furnished'];
		$property->pet = $propertydata['pet'];
		$property->zoned = $propertydata['zoned'];
		$property->possession = $propertydata['possession'];
		$property->bldg_type = $propertydata['bldgtype'];
		$property->features1 = $propertydata['features1'];
		$property->features2 = $propertydata['features2'];
		$property->features3 = $propertydata['features3'];
		$property->setting = $propertydata['settings'];

		foreach ($propertydata as $key => $value)
		{
		    $property->$key = $value;
		}

		 
		
		$buyers = $this->getBuyerAll_simple($property->sub_type);


		$activity_inserts = array();
		$buyer_ids_array = array();
		$buyer_names_array = array();
		$array_buyer_message = array();
		$array_user_buyers = array();


		$ex_rates = $this->getExchangeRates_indi();
	
		foreach($buyers as $buyer){

			$abuyerzip = explode(",",$buyer->zip);
			$price = explode('-', $buyer->price_value);
			$buyertype = $buyer->buyer_type;
			$property_type = $property->property_type;
			$sub_type = $property->sub_type;

			

			if($this->array_search_partial($abuyerzip,$property->zip) && $property->country == $buyer->country && $buyer->property_type == $property_type && $buyer->sub_type == $sub_type){
				if($property->price_type == 1){
					$pkvalid = 0;
					$pkvalid2 = 0;
					if ($property->currency != $buyer->currency) {
						if(!$buyer->currency){
							$buyer->currency = "USD";
						}
						$ex_rates_con = $ex_rates->rates->{$property->currency};
						$ex_rates_can = $ex_rates->rates->{$buyer->currency};
						if($property->currency=="USD"){					
							$pop_price1=$property->price1 * $ex_rates_can;
							$pop_price2=$property->price2 * $ex_rates_can;
						} else {
							$pop_price1=($property->price1 / $ex_rates_con) * $ex_rates_can;
							$pop_price2=($property->price2 / $ex_rates_con) * $ex_rates_can;
						}
					} else {
						$pop_price1=$property->price1;
						$pop_price2=$property->price2;
					}
					/*if($property->price1 <= $price[1] && $property->price1 >= $price[0]) {
						$pkvalid = 1;
					}*/
					if($pop_price2 >= $price[0] && $pop_price1 <= $price[1]) {
						$pkvalid = 1;
					}
				switch($property_type){
						case 1:
							if($sub_type == 1){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}			
	
							} else if($sub_type == 2){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow)))
								){
									$pkvalid2 = 1;
								}				
							} else if($sub_type == 3){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow)))
								){
									$pkvalid2 = 1;
								}					
							} else if($sub_type == 4){
								$zrange = $buyer->zoned;
								$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
								$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
								
								$pkzonedrange = $property->zoned;
								$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
								$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
								
								$lotszrange = $buyer->lot_size;
								$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
								//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
								$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
								
								$pklotszrange = $property->lot_size;
								$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
								//$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
								$pklotszlow = preg_replace('/[^\d.]/', '', $pklotszexplode[0]);
								
								if(
									((!is_numeric($pklotszlow) && $pklotszlow == $lotszlow) || (is_numeric($pklotszlow) && intval($pklotszlow) >= intval($lotszlow))) && 									
									intval($pkzonedlow) >= intval($zlow)
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 2:
							if($sub_type == 5){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);								
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow))) && 
									$property->term == $buyer->term && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished) && 
									$property->possession == $buyer->possession){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 6){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow))) && 
									$property->term == $buyer->term  && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 7){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow))) && 
									$property->term == $buyer->term  && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 8){
								$zrange = $buyer->zoned;
								$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
								$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
								
								$pkzonedrange = $property->zoned;
								$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
								$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
								
								$lotszrange = $buyer->lot_size;
								$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
								//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
								$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
								
								$pklotszrange = $property->lot_size;
								$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
								//$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
								$pklotszlow = preg_replace('/[^\d.]/', '', $pklotszexplode[0]);
								
								if(
									((!is_numeric($pklotszlow) && $pklotszlow == $lotszlow) || (is_numeric($pklotszlow) && intval($pklotszlow) >= intval($lotszlow))) &&
									intval($pkzonedlow) >= intval($zlow) && 
									$property->view == $buyer->view
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 3:
							if($sub_type == 9){
								$unitsrange = $buyer->units;
								$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
								//$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
								$unitslow = preg_replace('/[^\d.]/', '', $unitsexplode[0]);
								
								$pkunitsrange = $property->units;
								$pkunitsexplode = explode("-", $pkunitsrange); // split the ranged value into two, returning an array
								//$pkunitslow = str_replace(",", "", $pkunitsexplode[0]); //lower range for buyer
								$pkunitslow = preg_replace('/[^\d.]/', '', $pkunitsexplode[0]);
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									((!is_numeric($pkunitslow) && $pkunitslow == $unitslow) || (is_numeric($pkunitslow) && intval($pkunitslow) >= intval($unitslow))) && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))									
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 10){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($property->listing_class) && $property->listing_class == $buyer->listing_class) || (is_numeric($property->listing_class) && $property->listing_class >= $buyer->listing_class)) &&
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 11){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 12){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 13){
								$roomrange = $buyer->room_count;
								$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
								$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
								
								$pkroomrange = $buyer->room_count;
								$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
								$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									intval($pkroomlow) >= intval($roomlow)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 14){
								$roomrange = $buyer->room_count;
								$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
								$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
								
								$pkroomrange = $buyer->room_count;
								$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
								$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									intval($pkroomlow) >= intval($roomlow)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 15){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow)))
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 4:
							if($sub_type == 16){
								$availablerange = $buyer->available_sqft;
								$availableexplode = explode("-", $availablerange); // split the ranged value into two, returning an array
								//$availablelow = str_replace(",", "", $availableexplode[0]); //lower range for buyer
								$availablelow = preg_replace('/[^\d.]/', '', $availableexplode[0]);
								
								$pkavailablerange = $property->available_sqft;
								$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
								//$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
								$pkavailablelow = preg_replace('/[^\d.]/', '', $pkavailableexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($property->listing_class) && $property->listing_class == $buyer->listing_class) || (is_numeric($property->listing_class) && $property->listing_class >= $buyer->listing_class)) &&
									((!is_numeric($pkavailablelow) && $pkavailablelow == $availablelow) || (is_numeric($pkavailablelow) && intval($pkavailablelow) >= intval($availablelow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 17){
								$availablerange = $buyer->available_sqft;
								$availableexplode = explode("-", $availablerange); // split the ranged value into two, returning an array
								//$availablelow = str_replace(",", "", $availableexplode[0]); //lower range for buyer
								$availablelow = preg_replace('/[^\d.]/', '', $availableexplode[0]);
								
								$pkavailablerange = $property->available_sqft;
								$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
								$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
								$pkavailablelow = preg_replace('/[^\d.]/', '', $pkavailableexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									$property->type_lease == $buyer->type_lease && 
									((!is_numeric($pkavailablelow) && $pkavailablelow == $availablelow) || (is_numeric($pkavailablelow) && intval($pkavailablelow) >= intval($availablelow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 18){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							}
							break;
					}
					if($pkvalid == 1 && $pkvalid2 == 1 && $buyertype!='Inactive'){
						//$totalcount = $propertylisting_model->countlisting($buyer->buyer_id);
						
						$activity = array(
							'user_id'		=> $buyer->agent_id,
							'buyer_id'=> $buyer->buyer_id,
							'other_user_id'		=> JFactory::getUser()->id,
							'activity_type' => (int)25,
							'date'			=> date('Y-m-d H:i:s', time())
						);
						//$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);
						

						$activity_inserts[] = $property->listing_id.", ".$buyer->agent_id.", ".$buyer->buyer_id.", ".JFactory::getUser()->id.", 25, '".date('Y-m-d H:i:s', time())."'";
						$buyer_ids_array[] = $buyer->buyer_id;
						$buyer_names_array[] = $buyer->name;
						$array_user_buyers[] = $buyer->agent_id."%%%".$buyer->buyer_id."%%%".$buyer->name;
						/*
						if($user_android_token)
						foreach ($user_android_token as $key => $value) {
							# code...
							$array_user_tokens[] = $value;
							//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
						}
						*/
						//$b_user_android_token = $propertylisting_model->getAppUserToken($buyer->agent_id,'android');
						if($buyer->device_tokens){
							$dev_tokens_arr = explode(",", $buyer->device_tokens);							
							foreach ($dev_tokens_arr as $key => $value) {
								# code...
								$array_user_tokens[] = trim($value);
								$array_buyer_message[$buyer->agent_id][] = trim($value);
								//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
							}
						}
						
						
						
						print_r($activity);
					}
				} else if($property->price_type == 2) {
					$pkvalid = 0;
					$pkvalid2 = 0;
					if ($property->currency != $buyer->currency) {
						if(!$buyer->currency){
							$buyer->currency = "USD";
						}
						$ex_rates_con = $ex_rates->rates->{$property->currency};
						$ex_rates_can = $ex_rates->rates->{$buyer->currency};
						if($property->currency=="USD"){					
							$pop_price1=$property->price1 * $ex_rates_can;
						} else {
							$pop_price1=($property->price1 / $ex_rates_con) * $ex_rates_can;
						}
					} else {
						$pop_price1=$property->price1;
					}
					/*if($property->price1 <= $price[1] && $property->price1 >= $price[0]) {
						$pkvalid = 1;
					}*/
					if($pop_price1 >= $price[0] && $pop_price1 <= $price[1]) {
						$pkvalid = 1;
					}
					switch($property_type){
						case 1:
							if($sub_type == 1){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}			
	
							} else if($sub_type == 2){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow)))
								){
									$pkvalid2 = 1;
								}				
							} else if($sub_type == 3){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow)))
								){
									$pkvalid2 = 1;
								}					
							} else if($sub_type == 4){
								$zrange = $buyer->zoned;
								$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
								$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
								
								$pkzonedrange = $property->zoned;
								$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
								$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
								
								$lotszrange = $buyer->lot_size;
								$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
								//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
								$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
								
								$pklotszrange = $property->lot_size;
								$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
								//$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
								$pklotszlow = preg_replace('/[^\d.]/', '', $pklotszexplode[0]);
								
								if(
									((!is_numeric($pklotszlow) && $pklotszlow == $lotszlow) || (is_numeric($pklotszlow) && intval($pklotszlow) >= intval($lotszlow))) && 									
									intval($pkzonedlow) >= intval($zlow)
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 2:
							if($sub_type == 5){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);								
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow))) && 
									$property->term == $buyer->term && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished) && 
									$property->possession == $buyer->possession){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 6){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow))) && 
									$property->term == $buyer->term  && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 7){
								$unitrange = $buyer->unit_sqft;
								$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
								//$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
								$unitlow = preg_replace('/[^\d.]/', '', $unitexplode[0]);
								
								$pkunitrange = $property->unit_sqft;
								$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
								//$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
								$pkunitlow = preg_replace('/[^\d.]/', '', $pkunitexplode[0]);
								
								if(
									intval($property->bedroom) >= intval($buyer->bedroom) && 
									intval($property->bathroom) >= intval($buyer->bathroom) && 
									((!is_numeric($pkunitlow) && $pkunitlow == $bldglow) || (is_numeric($pkunitlow) && intval($pkunitlow) >= intval($unitlow))) && 
									$property->term == $buyer->term  && 
									intval($property->pet) == intval($buyer->pet) && 
									intval($property->furnished) == intval($buyer->furnished)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 8){
								$zrange = $buyer->zoned;
								$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
								$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
								
								$pkzonedrange = $property->zoned;
								$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
								$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
								
								$lotszrange = $buyer->lot_size;
								$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
								//$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
								$lotszlow = preg_replace('/[^\d.]/', '', $lotszexplode[0]);
								
								$pklotszrange = $property->lot_size;
								$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
								//$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
								$pklotszlow = preg_replace('/[^\d.]/', '', $pklotszexplode[0]);
								
								if(
									((!is_numeric($pklotszlow) && $pklotszlow == $lotszlow) || (is_numeric($pklotszlow) && intval($pklotszlow) >= intval($lotszlow))) &&
									intval($pkzonedlow) >= intval($zlow) && 
									$property->view == $buyer->view
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 3:
							if($sub_type == 9){
								$unitsrange = $buyer->units;
								$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
								//$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
								$unitslow = preg_replace('/[^\d.]/', '', $unitsexplode[0]);
								
								$pkunitsrange = $property->units;
								$pkunitsexplode = explode("-", $pkunitsrange); // split the ranged value into two, returning an array
								//$pkunitslow = str_replace(",", "", $pkunitsexplode[0]); //lower range for buyer
								$pkunitslow = preg_replace('/[^\d.]/', '', $pkunitsexplode[0]);
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									((!is_numeric($pkunitslow) && $pkunitslow == $unitslow) || (is_numeric($pkunitslow) && intval($pkunitslow) >= intval($unitslow))) && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))									
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 10){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($property->listing_class) && $property->listing_class == $buyer->listing_class) || (is_numeric($property->listing_class) && $property->listing_class >= $buyer->listing_class)) &&
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 11){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 12){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 13){
								$roomrange = $buyer->room_count;
								$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
								$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
								
								$pkroomrange = $buyer->room_count;
								$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
								$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									intval($pkroomlow) >= intval($roomlow)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 14){
								$roomrange = $buyer->room_count;
								$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
								$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
								
								$pkroomrange = $buyer->room_count;
								$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
								$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
								
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow))) && 
									intval($pkroomlow) >= intval($roomlow)
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 15){
								$caprange = $buyer->cap_rate;
								$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
								//$caplow = str_replace(",", "", $capexplode[0]); //lower range for buyer
								$caplow = preg_replace('/[^\d.]/', '', $capexplode[0]);
								
								$pkcaprange = $property->cap_rate;
								$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
								//$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
								$pkcaplow = preg_replace('/[^\d.]/', '', $pkcapexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkcaplow) && $pkcaplow == $caplow) || (is_numeric($pkcaplow) && intval($pkcaplow) >= intval($caplow)))
								){
									$pkvalid2 = 1;
								}
							}
							break;
						case 4:
							if($sub_type == 16){
								$availablerange = $buyer->available_sqft;
								$availableexplode = explode("-", $availablerange); // split the ranged value into two, returning an array
								//$availablelow = str_replace(",", "", $availableexplode[0]); //lower range for buyer
								$availablelow = preg_replace('/[^\d.]/', '', $availableexplode[0]);
								
								$pkavailablerange = $property->available_sqft;
								$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
								//$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
								$pkavailablelow = preg_replace('/[^\d.]/', '', $pkavailableexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($property->listing_class) && $property->listing_class == $buyer->listing_class) || (is_numeric($property->listing_class) && $property->listing_class >= $buyer->listing_class)) &&
									((!is_numeric($pkavailablelow) && $pkavailablelow == $availablelow) || (is_numeric($pkavailablelow) && intval($pkavailablelow) >= intval($availablelow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 17){
								$availablerange = $buyer->available_sqft;
								$availableexplode = explode("-", $availablerange); // split the ranged value into two, returning an array
								//$availablelow = str_replace(",", "", $availableexplode[0]); //lower range for buyer
								$availablelow = preg_replace('/[^\d.]/', '', $availableexplode[0]);
								
								$pkavailablerange = $property->available_sqft;
								$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
								$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
								$pkavailablelow = preg_replace('/[^\d.]/', '', $pkavailableexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									$property->type_lease == $buyer->type_lease && 
									((!is_numeric($pkavailablelow) && $pkavailablelow == $availablelow) || (is_numeric($pkavailablelow) && intval($pkavailablelow) >= intval($availablelow)))
								){
									$pkvalid2 = 1;
								}
							} else if($sub_type == 18){
								$bldgrange = $buyer->bldg_sqft;
								$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
								//$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
								$bldglow = preg_replace('/[^\d.]/', '', $bldgexplode[0]);
								
								$pkbldgrange = $property->bldg_sqft;
								$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
								//$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
								$pkbldglow = preg_replace('/[^\d.]/', '', $pkbldgexplode[0]);
								
								if(
									$property->type == $buyer->type && 
									((!is_numeric($pkbldglow) && $pkbldglow == $bldglow) || (is_numeric($pkbldglow) && intval($pkbldglow) >= intval($bldglow)))
								){
									$pkvalid2 = 1;
								}
							}
							break;
					}
					if($pkvalid == 1 && $pkvalid2 == 1 && $buyertype!='Inactive'){
						//$totalcount = $propertylisting_model->countlisting($buyer->buyer_id);
						//$insertToBuyerListings = $propertylisting_model->insertToBuyerListings($buyer->buyer_id , $listing_id, $totalcount, 1);
						$activity = array(
							'user_id'		=> $buyer->agent_id,
							'buyer_id'=> $buyer->buyer_id,
							'other_user_id'		=> JFactory::getUser()->id,
							'activity_type' => (int)25,
							'date'			=> date('Y-m-d H:i:s', time())
						);
						//$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);

						
						$activity_inserts[] = $property->listing_id.", ".$buyer->agent_id.", ".$buyer->buyer_id.", ".JFactory::getUser()->id.", 25, '".date('Y-m-d H:i:s', time())."'";
						$buyer_ids_array[] = $buyer->buyer_id;
						$buyer_names_array[] = $buyer->name;
						$array_user_buyers[] = $buyer->agent_id."%%%".$buyer->buyer_id."%%%".$buyer->name;
						/*
						if($user_android_token)
						foreach ($user_android_token as $key => $value) {
							# code...
							$array_user_tokens[] = $value;
							//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
						}
						*/
						//$b_user_android_token = $propertylisting_model->getAppUserToken($buyer->agent_id,'android');
						if($buyer->device_tokens){
							$dev_tokens_arr = explode(",", $buyer->device_tokens);							
							foreach ($dev_tokens_arr as $key => $value) {
								# code...
								$array_user_tokens[] = trim($value);
								$array_buyer_message[$buyer->agent_id][] = trim($value);
								//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
							}
						}
						//foreach ($buyer->device_token as $key => $value) {
							# code...
							
							//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
						//}
						
						print_r($activity);
					}
				}
			}					
		}

		//$abuyerzip = $this->array_search_partial($abuyerzip_s,$property->zip) ? "true" : "false";		 	


	    if($buyer_ids_array)
			$insertToBuyerListings = $this->insertToBuyerListings_pops($buyer_ids_array, $listing_id);
		if($activity_inserts)
			$ret = $this->insertPropertyActivity_Multi($activity_inserts);
		if($array_user_tokens){

			$users_tosent = array();
			$message_tosent = array();
			$users_ids = array();
			$mess = array();
			$processed_users = array();
			foreach ($array_user_buyers as $key => $value) {
				# code...
				$temp_array = explode("%%%",$value);
				$users_ids[] = $temp_array[0];
				$users_tosent[$temp_array[0]][]=$temp_array[1]."%%%".$temp_array[2];
			}

			foreach ($users_ids as $key => $value) {
				# code...
				if(!in_array($value, $processed_users)){
					foreach ($users_tosent as $inside_key => $inside_value) {
						# code...
					
							$count_buyer = count($users_tosent[$inside_key]);
							$inside_v_array=explode("%%%",$inside_value[0]);

							if($count_buyer==1){
								$mess[] = "A New POPs™ in AgentBridge matches to ".$inside_v_array[1]."_idPOPs~".$inside_v_array[0]."_idUsers~".$inside_key;
							} else {
								$mess[] = "A New POPs™ in AgentBridge matches to ".$count_buyer." Buyers_idUsers~".$inside_key;
							}
							
										
					}

					$processed_users[]=$value;	
				}
			}

			$generated_message = implode("%%%%%%", array_unique($mess));
		
			$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( "array user token count" );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody( print_r($generated_message,true)."=============".print_r($users_tosent,true));
			$mailSender ->Send(); 
		


		//message generation

			$buyer_ids_array =array_unique($buyer_ids_array);

			$this->android_send_notification($array_user_tokens,"Buyer and POPs™ match",$generated_message, "BuyerMatch");
		/*	if(count($buyer_ids_array)==1){
				$this->android_send_notification($array_user_tokens,"Buyer and POPs™ match","A New POPs™ in AgentBridge matches to ".$buyer_names_array[0]."_idPOPs~".$buyer_ids_array[0], "BuyerMatch");
			} else {
				$this->android_send_notification($array_user_tokens,"Buyer and POPs™ match","A New POPs™ in AgentBridge matches to ".count($buyer_ids_array)." Buyers", "BuyerMatch");
			}*/

			
		}
			
	}
	
	public function get_pocket_listing_indi_filter_optimized($filters, $buyers){
		$conditions = array();
		$db = JFactory::getDbo();
		$today = date ('Y-m-d');
		//get listing details
		$query = $db->getQuery(true);
		$query->select("*, pp.price_type, p.listing_id, currcon.*, IFNULL(pp.price2,'exact_price') as abcd, cvs.*");
		$query->from('#__pocket_listing p');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');
		//$query->leftJoin('#__request_access ra ON ra.property_id = p.listing_id AND ra.user_b = "' . JFactory::getUser()->id . '"');
		$query->where('DATE(p.date_expired) > \''. $today .'\' AND p.closed = 0 AND p.sold = 0');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id") {
					if($key=="p.user_id") {
						$user_id = $value;	
					}					
					$conditions[] = $key." = ".$value;
				} else if($key!="pp.price") {
					$conditions[] = $key." > ".$value;
				} else{
					$property_type = $buyers[0]->needs[0]->property_type;
					$sub_type = $buyers[0]->needs[0]->sub_type;		
					$country = 	$buyers[0]->needs[0]->country;			
					// zip extract
					$zip = explode(",",$buyers[0]->needs[0]->zip);
					$zip_cnt=0;
					$zip_condition = "";
					foreach($zip as $row){
						if($zip_cnt<count($zip)){
							if($country==222){
								$zip_cnt++;
								$zip_condition .= "p.zip LIKE '".trim($row)."%'";
							} else if($country==73){
								$zip_cnt++;
								$zip_condition .= "(p.zip LIKE '".trim($row)."%' AND p.country = 73)";
							} else {
								$zip_cnt++;
								$zip_condition .= "p.zip ='".$row."'";
							}
						}
						if($zip_cnt<count($zip)){
							$zip_condition .=" OR ";
						}
					}
					//if(count($zip)>=2){
						$conditions[] = "(".$zip_condition.") AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";
					//} else {
					//	$conditions[] = "p.zip='".$buyers[0]->needs[0]->zip."' AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";
					//}
					switch($property_type){
						case 1:
						if($sub_type == 1){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		
						} else if($sub_type == 2){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			
						} else if($sub_type == 3){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";				
						} else if($sub_type == 4){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		
						}
						break;
						case 2:
						if($sub_type == 5){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.pet >='".$buyers[0]->needs[0]->pet."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."' AND p.possession >='".$buyers[0]->needs[0]->possession."'";
						} else if($sub_type == 6){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."'  AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 7){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 8){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->needs[0]->view."'";
						}
						break;
						case 3:
						if($sub_type == 9){
							$unitsrange = $buyers[0]->needs[0]->units;
							$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
							$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 10){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 11){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 12){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 13){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 14){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 15){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$conditions[] = "p.type='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";
						}
						break;
					case 4:
						if($sub_type == 16){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 17){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.type_lease >='".$buyers[0]->needs[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 18){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						}
						break;
					}
					$explode = explode('-', $value);
					//	if($value->currency != $buyers[0]->needs[0]->currency){
					//		$explode[1] = $this->getExchangeRates($explode[1],$buyers[0]->needs[0]->currency,$value->currency);
					//}
					//	$conditions[]= $key."1 <= ".$explode[1];
				}
			} 
		}
		if(count($conditions)>0) {
			$query->where($conditions);
		}
		//$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		//$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		/*$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							WHERE racs.user_b = ' . $buyers[0]->agent_id . '
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');*/ 
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->group('p.listing_id');
		$db->setQuery($query);
		$original_array=$db->loadObjectList();
		$i=0;
		$ex_rates = $this->getExchangeRates_indi();
		foreach ($original_array as $key => $value) {
				if($value->user_id != JFactory::getUser()->id){
					if($value->currency != $buyers[0]->needs[0]->currency){
						$ex_rates_con = $ex_rates->rates->{$value->currency};
						if(!$buyers[0]->needs[0]->currency){
							$buyers[0]->needs[0]->currency ="USD";
						}
						$ex_rates_can = $ex_rates->rates->{$buyers[0]->needs[0]->currency};
						if($value->currency=="USD"){					
							$value->price1=$value->price1 * $ex_rates_can;
						} else {
							$value->price1=($value->price1 / $ex_rates_con) * $ex_rates_can;							
						}
						if(($value->price2 != null || $value->price2 != "")){
							if($value->currency=="USD"){					
								$value->price2=$value->price2 * $ex_rates_can;
							} else {
								$value->price2=($value->price2 / $ex_rates_con) * $ex_rates_can;							
							}
						}
						if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
					}
				} else {
					if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
				}
				if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
					unset($original_array[$i]);
				}
				$i++;
		}
		return $original_array;
		die();
	}
	
    //MAINSEARCH
	public function get_pocket_listing_indi_filter($filters, $buyers){
		$conditions = array();
		$db = JFactory::getDbo();
		$today = date ('Y-m-d');
		//get listing details
		$query = $db->getQuery(true);
		$query->select("*, pp.price_type, p.listing_id, currcon.*, IFNULL(pp.price2,'exact_price') as abcd, cvs.*, ra.permission as req_acc_status, u.name AS username,ps.*");
		$query->from('#__pocket_listing p');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');
		$query->leftJoin('#__request_access ra ON ra.property_id = p.listing_id AND ra.user_b = "' . JFactory::getUser()->id . '"');
		$query->where('DATE(p.date_expired) > \''. $today .'\' AND p.closed = 0 AND p.sold = 0');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id") {
					if($key=="p.user_id") {
						$user_id = $value;	
					}					
					$conditions[] = $key." = ".$value;
				} else if($key!="pp.price") {
					$conditions[] = $key." > ".$value;
				} else{
					$property_type = $buyers[0]->needs[0]->property_type;
					$sub_type = $buyers[0]->needs[0]->sub_type;		
					$country = 	$buyers[0]->needs[0]->country;			
					// zip extract
					$zip = explode(",",$buyers[0]->needs[0]->zip);
					$zip_cnt=0;
					$zip_condition = "";
					foreach($zip as $row){
						if($zip_cnt<count($zip)){
							if($country==222){
								$zip_cnt++;
								$zip_condition .= "p.zip LIKE '".trim($row)."%'";
							} else if($country==73){
								$zip_cnt++;
								$zip_condition .= "(p.zip LIKE '".trim($row)."%' AND p.country = 73)";
							} else {
								$zip_cnt++;
								$zip_condition .= "p.zip ='".$row."'";
							}
						}
						if($zip_cnt<count($zip)){
							$zip_condition .=" OR ";
						}
					}
					//if(count($zip)>=2){
						$conditions[] = "(".$zip_condition.") AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";
					//} else {
					//	$conditions[] = "p.zip='".$buyers[0]->needs[0]->zip."' AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";
					//}
					switch($property_type){
						case 1:
						if($sub_type == 1){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		
						} else if($sub_type == 2){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			
						} else if($sub_type == 3){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";				
						} else if($sub_type == 4){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		
						}
						break;
						case 2:
						if($sub_type == 5){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.pet >='".$buyers[0]->needs[0]->pet."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."' AND p.possession >='".$buyers[0]->needs[0]->possession."'";
						} else if($sub_type == 6){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."'  AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 7){
							$unitrange = $buyers[0]->needs[0]->unit_sqft;
							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array
							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer
							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";
						} else if($sub_type == 8){
							$zrange = $buyers[0]->needs[0]->zoned;
							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array
							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer
							$lotszrange = $buyers[0]->needs[0]->lot_size;
							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array
							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->needs[0]->view."'";
						}
						break;
						case 3:
						if($sub_type == 9){
							$unitsrange = $buyers[0]->needs[0]->units;
							$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array
							$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 10){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 11){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 12){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						} else if($sub_type == 13){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 14){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$roomrange = $buyers[0]->needs[0]->room_count;
							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array
							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer
							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";
						} else if($sub_type == 15){
							$caprange = $buyers[0]->needs[0]->cap_rate;
							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array
							$conditions[] = "p.type='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";
						}
						break;
					case 4:
						if($sub_type == 16){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 17){
							$avlbrange = $buyers[0]->needs[0]->available_sqft;
							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array
							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.type_lease >='".$buyers[0]->needs[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";
						} else if($sub_type == 18){
							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;
							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array
							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer
							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";
						}
						break;
					}
					$explode = explode('-', $value);
					//	if($value->currency != $buyers[0]->needs[0]->currency){
					//		$explode[1] = $this->getExchangeRates($explode[1],$buyers[0]->needs[0]->currency,$value->currency);
					//}
					//	$conditions[]= $key."1 <= ".$explode[1];
				}
			} 
		}
		if(count($conditions)>0) {
			$query->where($conditions);
		}
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		//$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		/*$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							WHERE racs.user_b = ' . $buyers[0]->agent_id . '
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');*/ 
		$query->leftJoin('#__users u on u.id = p.user_id');
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->group('p.listing_id');
		$db->setQuery($query);
		$original_array=$db->loadObjectList();
		$i=0;
		$ex_rates = $this->getExchangeRates_indi();
		foreach ($original_array as $key => $value) {
				if($value->user_id != JFactory::getUser()->id){
					if($value->currency != $buyers[0]->needs[0]->currency){
						$ex_rates_con = $ex_rates->rates->{$value->currency};
						if(!$buyers[0]->needs[0]->currency){
							$buyers[0]->needs[0]->currency ="USD";
						}
						$ex_rates_can = $ex_rates->rates->{$buyers[0]->needs[0]->currency};
						if($value->currency=="USD"){					
							$value->price1=$value->price1 * $ex_rates_can;
						} else {
							$value->price1=($value->price1 / $ex_rates_con) * $ex_rates_can;							
						}
						if(($value->price2 != null || $value->price2 != "")){
							if($value->currency=="USD"){					
								$value->price2=$value->price2 * $ex_rates_can;
							} else {
								$value->price2=($value->price2 / $ex_rates_con) * $ex_rates_can;							
							}
						}
						if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
					}
				} else {
					if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
						unset($original_array[$i]);
						}
				}
				if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){
					unset($original_array[$i]);
				}
				$i++;
		}
		return $original_array;
		die();
	}
	public function get_pocket_listing_indi($filters){
		$conditions = array();
		#echo ""; print_r($buyers);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__pocket_listing p');
		$filters["p.sold"] = 0;
		foreach ($filters as $key => $value){
			if($key=="p.sold"){
				$conditions[] = $key." = ".$value;
			}
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id" || $key=="p.sold")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." > ".$value;
				else{
					//$explode = explode('-', $value);
					//if(count($explode)>1){
						//$conditions[] = $key."1 <= ".$explode[1] ." AND ". $key."2 >= ".$explode[0];
					//}
					//else
					//	$conditions[] = $key."1 >= ".$value;
				}
			}
		}
		if(count($conditions)>0)
			$query->where($conditions);
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->order('p.setting desc');
		$query->order('p.listing_id desc');
		//$query->order('racs.permission desc');
		$db->setQuery($query);
		return $db->loadObjectList();
		die();
	}
	public function getPOPPrice($popsId){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__property_price');
		$query->where('pocket_id = '.$popsId); 
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_saved_pocket_listing_indi($filters){
		$conditions = array();
		#echo ""; print_r($buyers);



		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__pocket_listing p');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id" || $key=="p.closed")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." > ".$value;
				else if ($key=="p.date_expired"){
					$conditions[] = "DATE(".$key.") > '".$value."'";
				}
				else{
					//$explode = explode('-', $value);
					//if(count($explode)>1){
						//$conditions[] = $key."1 <= ".$explode[1] ." AND ". $key."2 >= ".$explode[0];
					//}
					//else
					//	$conditions[] = $key."1 >= ".$value;
				}
			}
		}

		if(count($conditions)>0)
			$query->where($conditions);

		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
	//	$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
	//	$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
	/*	$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid
							FROM #__request_access racs
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');*/
		$query->leftJoin('#__property_type ON type_id = p.property_type');
		$query->leftJoin('#__country_currency concurr ON concurr.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
		$query->order('p.setting desc');
		$query->order('p.listing_id desc');
		//$query->order('racs.permission desc');
		$db->setQuery($query);
		return $db->loadObjectList();
		die();
	}
	public function getviewers($pid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "SELECT DISTINCT (user_id), property_id FROM tbl_views WHERE property_id = '".$pid."'";
		/*$query->from('#__user_registration');
		$query->where('email = \''.JFactory::getUser($uid)->email.'\''); */
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function getviewersimages($uid){
		//echo $uid;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('u.email AS vemail, u.id AS vid, ur.image AS img'); 
		$query->from('#__users u');
		$query->where('u.id = \''.$uid.'\'');
		$query->leftJoin('#__user_registration ur on ur.email =  u.email');
		$db->setQuery($query);
		return $db->loadObjectList();
		/*$query->select('email');
		$query->from('#__user_registration');
		$query->where('email = \''.$aa->email.'\'');
		$db->setQuery($query); */
		//echo "<pre>"; print_r($aa); die();
	}
	public function checkRequestAccess(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__request_access');
		$query->order('permission desc');
		$db->setQuery($query);
		$raccess = $db->loadObjectList();
		return $raccess;
		die();
	}
	public function getAllPropertyTypes() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_type');
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getAllSubPropTypes($ptype_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type');
		$query->where('property_id ='.$ptype_id);
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getAllStates() {
		$db = JFactory::getDbo();
		// GET ALL STATE
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__zones');
		$query->where('zone_country_id LIKE \'223\'');
		$db->setQuery($query);
		$zone = $db->loadObjectList();
		return $zone;
	}
	public function getAllListings($user_id) {
		$db = JFactory::getDbo();
		// GET ALL LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'currcon.symbol', 'currcon.currency', 'pl.*', 'pp.*', 'ps.*', 'pt.*', 'pst.*', 'con.*', 'viewers.allowed', 'request.request', 'declined.declined', 'cvs.*'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = pl.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = pl.country');
		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = pl.sub_type');
		$query->leftJoin('#__countries con ON con.countries_id = pl.country');
		$query->leftJoin('#__permission_setting ps ON ps.listing_id = pl.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed FROM #__request_access racs WHERE permission = 1 GROUP BY racs.property_id) as viewers ON viewers.id = pp.pocket_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as request FROM #__request_access racs WHERE permission = 0 GROUP BY racs.property_id) as request ON request.id = pp.pocket_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as declined FROM #__request_access racs WHERE permission = 2 GROUP BY racs.property_id) as declined ON declined.id = pp.pocket_id');
		//$query->join('LEFT', '#__pocket_images as pi ON (pl.listing_id = pi.listing_id)');
		$query->where('pl.user_id LIKE \''.$user_id.'\' GROUP by lid');
		$query->order('pl.sold asc, pl.closed asc, pl.date_created desc');
		$db->setQuery($query);
		$listing = $db->loadObjectList();
		return $listing;
	}
	public function getAllListings2($user_id) {
		$db = JFactory::getDbo();
		// GET ALL LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'currcon.symbol', 'pl.*', 'pp.*', 'pt.*', 'pst.*', 'viewers.allowed', 'request.request', 'declined.declined'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		$query->leftJoin('#__country_currency currcon ON currcon.currency = pl.currency');
		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = pl.sub_type');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed FROM #__request_access racs WHERE permission = 1 GROUP BY racs.property_id) as viewers ON viewers.id = pp.pocket_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as request FROM #__request_access racs WHERE permission = 0 GROUP BY racs.property_id) as request ON request.id = pp.pocket_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as declined FROM #__request_access racs WHERE permission = 2 GROUP BY racs.property_id) as declined ON declined.id = pp.pocket_id');
		//$query->join('LEFT', '#__pocket_images as pi ON (pl.listing_id = pi.listing_id)');
		$query->where('pl.closed !=1 AND pl.user_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$listing = $db->loadObjectList();
		return $listing;
	}
	public function getClosedListings($user_id) {
		$db = JFactory::getDbo();
		//GET CLOSED LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'pl.*', 'pp.*'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		//$query->join('LEFT', '#__pocket_images as pi ON (pl.listing_id = pi.listing_id)');
		$query->where(array('pl.user_id LIKE \''.$user_id.'\'', 'pl.closed = 1'));
		$db->setQuery($query);
		$closedlisting = $db->loadObjectList();
		return $closedlisting;
	}
	public function getInProgress($user_id) {
		$db = JFactory::getDbo();
		//GET IN PROGRESS
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'pl.*', 'pp.*'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		//$query->join('LEFT', '#__pocket_images as pi ON (pl.listing_id = pi.listing_id)');
		$query->where(array('pl.user_id LIKE \''.$user_id.'\'', 'pl.closed = 0'));
		$db->setQuery($query);
		$inprogresslisting = $db->loadObjectList();
		return $inprogresslisting;
	}
	public function getPermissionChoices($listing_id ) {
		$db = JFactory::getDbo();
		// GET PERMISSION CHOICES
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__permission_setting');
		$query->where('listing_id = \''. $listing_id .'\'');
		$db->setQuery($query);
		$setting = $db->loadObjectList();
		return $setting;
	}
	public function getSelectedListing($listing_id, $fallback = false) {
		if(!$fallback)
			return $this->get_listing(array('p.listing_id'=>$listing_id));
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid',  'pl.*', 'pp.*', 'ps.*', 'con.*', 'v.*','conc.country as concode','conc.*', 'pa.*', 'pst.*', 'pt.*','cvs.*', 'pst.name AS subtype_name', 'pt.type_name AS proptype_name'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		$query->join('LEFT', '(select pocket_id, group_concat(address separator "-----") as address from #__pocket_address group by pocket_id) as pa ON (pl.listing_id = pa.pocket_id)');
		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = pl.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = pl.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = pl.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = pl.sub_type');
		$query->leftJoin('#__permission_setting ps ON ps.listing_id = pl.listing_id');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = pl.listing_id');
		$query->where('pl.listing_id = \''.$listing_id.'\'');
		$query->order('pl.listing_id DESC LIMIT 1');
		$db->setQuery($query);
		$individual = $db->loadObjectList();
		return $individual;
	}

	public function getPOPsById_model($listing_id,$user_id){

		$conditions = array();
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query = "SET SESSION group_concat_max_len = 1000000;";
	
		$db->setQuery($query);			 
		$result = $db->execute();


		$query = $db->getQuery(true);
		$query->select('p.*,ps.*,pp.*,pa.*,pi.*,viewers.*, conc.symbol, conc.currency, pst.name AS subtype_name , pt.type_name AS proptype_name,
			p.listing_id as listing_id,u.name AS username,z.zone_name AS state_name, c.countries_iso_code_3 AS country_name, p.description AS `desc`,
			us.verified_2013 as v2013,us.verified_2014 as v2014
		');
		$query->from('#__pocket_listing p');
		$query->where("p.listing_id = ".$listing_id);
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image ORDER BY image_id ASC) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('#__request_access viewers ON viewers.property_id = p.listing_id AND viewers.user_b = '.$user_id);
		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');
		$query->leftJoin('#__countries c ON c.countries_id = p.country');
		$query->leftJoin('#__zones z ON p.state = z.zone_id');
		$query->leftJoin('#__country_currency conc ON conc.currency = p.currency');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = p.sub_type');
		$query->leftJoin('#__users u ON u.id = p.user_id');
		$query->leftJoin('#__user_sales us ON us.agent_id = p.user_id');
		$query->order('p.setting desc');

		//var_dump($query->__toString());
		$db->setQuery($query);
		return $db->loadAssocList();

	}

	public function printPropertyState($state_id) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where('zone_id = \''.$state_id.'\'');
		$db->setQuery($query);
		$state = $db->loadObject();
		return $state;
	}
	public function getStateIdByName($state_name) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('zone_id');
		$query->from('#__zones');
		$query->where('zone_name = \''.$state_name.'\'');
		$db->setQuery($query);
		$state = $db->loadResult();
		return $state;
	}
	public function getPtypeByName($ptype_name) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('type_id');
		$query->from('#__property_type');
		$query->where('type_name = \''.$ptype_name.'\'');
		$db->setQuery($query);
		$ptype_name = $db->loadResult();
		return $ptype_name;
	}
	public function getSubTypeByName($ptype_id,$subtype_name) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('sub_id');
		$query->from('#__property_sub_type');
		$query->where('name = \''.$subtype_name.'\' AND property_id='.$ptype_id);
		$db->setQuery($query);
		$subtype_name = $db->loadResult();
		return $subtype_name;
	}

	public function getRandViewPOPs($listing_id){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('rand_view_count');
		$query->from('#__pocket_listing');
		$query->where('listing_id='.$listing_id);
		$db->setQuery($query);
		$rand_view_save =  $db->loadResult();

		return $rand_view_save;

	}

	public function logActivity($individual) {

		//$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('DISTINCT (user_id) ');
		$query->from('#__views');
		$query->where('property_id='.$individual[0]->listing_id);
		$db->setQuery($query);
		$before_view =  count($db->loadColumn());

		$query = $db->getQuery(true);
		$query->select('rand_view_count');
		$query->from('#__pocket_listing');
		$query->where('listing_id='.$individual[0]->listing_id);
		$db->setQuery($query);
		$rand_view_save =  $db->loadResult();

		if($before_view==1 && !$rand_view_save){

			$rand_inser = new JObject();
			$rand_inser->listing_id = $individual[0]->listing_id;
			$rand_inser->rand_view_count = mt_rand(7, 33);
			$rand_insert_view = $db->updateObject('#__pocket_listing', $rand_inser, 'listing_id');

		}

		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$insert = new JObject();
		$insert->user_id = $user->id;
		$insert->property_id = $individual[0]->listing_id;
		$insert->date = date('Y-m-d H:i:s',time());
		$ret = $db->insertObject('#__views', $insert);
		$insert2 = new JObject();
		$insert2->user_id = $individual[0]->user_id;
		$insert2->activity_type = 5;
		$insert2->activity_id = $db->getConnection()->insert_id;
		$insert2->other_user_id = $user->id;
		$insert2->date = date('Y-m-d H:i:s',time());
		$re2t = $db->insertObject('#__activities', $insert2);
	}
	public function getSubTypes($selected_type) {
		$db = JFactory::getDbo();
		// GET ALL SUBTYPE
		$query = $db->getQuery(true);
		$query->select(array('sub_id', 'name'));
		$query->from('#__property_sub_type');
		$query->where('property_id LIKE \''.$selected_ptype.'\'');
		$db->setQuery($query);
		$subtypes = $db->loadObjectList();
		return $subtypes;
	}
	public function get_AB_ref_fee($amount){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('tbl_referral_fee')->where('tier1 <= '.$amount.' and tier2 >= '.$amount);
		//echo $query;
		return $db->setQuery($query)->loadObject();
	}
	public function get_referral($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*, (SELECT group_concat(other_user_id) as signatures FROM `#__activities` WHERE activity_type = 16 and activity_id = '.$id.') as signatories, (SELECT count(*) as signatures FROM `#__activities` WHERE activity_type = 16 and activity_id = '.$id.') as signatures')
		->from('#__referral r')
		->leftJoin('#__client_intention cl on r.client_intention=cl.id')
		->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id')
		->leftJoin('#__country_currency coun on ba.currency = coun.currency')
		->where('referral_id = '.$id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function get_referral_fee_dropdown(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__dropdown_referral_fee');
		return $db->setQuery($query)->loadObjectList();
	}
	public	function getAllPopsBuyers_min(){
		$db = JFactory::getDbo();
		$model = $this;
		$counts_a=0;
		$counts_b=0;
		$counts_c=0;
		$abuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"A Buyer"'));
		foreach($abuyers as $key =>$abuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($abuyer->buyer_id);
			$update->buyer_id = $abuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$bbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"B Buyer"'));
		foreach($bbuyers as $key =>$bbuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($bbuyer->buyer_id);
			$update->buyer_id = $bbuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$cbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"C Buyer"'));
		foreach($cbuyers as $key =>$cbuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($cbuyer->buyer_id);
			$update->buyer_id = $cbuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$ibuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"Inactive"'));
		foreach($ibuyers as $key =>$ibuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($ibuyer->buyer_id);
			$update->buyer_id = $ibuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		return true;
	}
	public	function getAllPopsBuyers(){
		$db = JFactory::getDbo();
		$model = $this;
		$counts_a=0;
		$counts_b=0;
		$counts_c=0;
		$abuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"A Buyer"'));
		foreach($abuyers as $key =>$abuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($abuyer->buyer_id);
			$update->buyer_id = $abuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$bbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"B Buyer"'));
		foreach($bbuyers as $key =>$bbuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($bbuyer->buyer_id);
			$update->buyer_id = $bbuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$cbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"C Buyer"'));
		foreach($cbuyers as $key =>$cbuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($cbuyer->buyer_id);
			$update->buyer_id = $cbuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		$ibuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"Inactive"'));
		foreach($ibuyers as $key =>$ibuyer) {
			$update = new JObject();
			$update->listingcount = $this->countlisting($ibuyer->buyer_id);
			$update->buyer_id = $ibuyer->buyer_id;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
		return true;
	}
	public function countlisting($buyer_id) {
		$model = $this;
		$buyers = $model->getBuyer($buyer_id);
		$search_key = $buyers[0]->needs[0]->zip;
		$searchreturn = $model->searchZip($search_key);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		if($price[1] == ''){$usethis = 0;}
		#print_r($listing); die();
		foreach($listing as $search){	
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
				 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					 if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
				if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->ceiling_height == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
				 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		#die();
		usort($pocketlistings_0, function($a, $b){
			if($a->abcd == $b->abcd){
				if($a->price1 > $b->price1){
					return 1;
				} else {
					return -1;
				}
				if($a->price1 == $b->price1){
					if($a->listing_id > $b->listing_id){
						return 1;
					} else {
						return -1;
					}
				}
			}
			if(($a->abcd < $b->abcd)){
				return 1;
			} else {
				return -1;
			}
		});
		foreach($pocketlistings_0 as $indi){
				array_push($pocketlistings, $indi);
		}
		return count($pocketlistings);
	}
	public function get_client($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__referral')->leftJoin('#__buyer on buyer_id = client_id')->where('referral_id='.$id);
		return $db->setQuery($query)->loadObject();
	}
	function getCountryCurrencyData($cID){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->setQuery("
				SELECT cur.* FROM #__country_currency AS cur WHERE country=".$cID);
			$db->setQuery($query);		
			return $db->loadObjectList();
	}
	public function getCountry($user_id) {
		$db = JFactory::getDbo();
		//GET COUNTRY
		$query = $db->getQuery(true);
		$query->select('email');
		$query->from('#__users');
		$query->where('id = \''.$user_id.'\'');
		$db->setQuery($query);
		$email = $db->loadObjectList();
		$query = $db->getQuery(true);
		$query->select('ur.country, countries_iso_code_2, curr.currency, curr.symbol');
		$query->from('#__user_registration ur');
		$query->leftJoin('#__countries ON countries_id = ur.country');
		$query->leftJoin('#__country_currency curr ON curr.country = ur.country');
		$query->where('email = \''.$email[0]->email.'\'');
		$db->setQuery($query);
		$country = $db->loadObjectList();
		return $country;
	}
	public function update_referral_object($id, $counter, $countered, $status = null){
		$referral_object = new JObject();
		if($status!=null)
			$referral_object->status = $status;
		$referral_object->referral_id = $id;
		$referral_object->counter = $counter;
		$referral_object->countered = $countered;
		$result = JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
	}
	public function update_referral_object_generic($params){
		$referral_object = new JObject();
		foreach($params as $key => $value){
			$referral_object->$key = $value;
		}
		$result = JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
	}
	public function insert_referral_status_object_generic($params){
		$referral_status_object = new JObject();
		foreach($params as $key => $value){
			$referral_status_object->$key = $value;
		}
		$referral_status_object->created_by = JFactory::getUser()->id;
		JFactory::getDbo()->insertObject('#__referral_status_update', $referral_status_object);
		return JFactory::getDbo()->insertid();
	}
	public function insert_activity($user_id, $activity_type, $activity_id, $other_user_id, $date){
		$db = JFactory::getDbo();
		$insert2 = new JObject();
		$insert2->user_id = $user_id;
		$insert2->activity_type = $activity_type;
		$insert2->activity_id = $activity_id;
		$insert2->other_user_id = $other_user_id;
		$insert2->date = $date;
		$re2t = $db->insertObject('#__activities', $insert2);
	}
	public function insert_referral_value_object($id, $value){
		$db = JFactory::getDbo();
		$referral_object = new JObject();
		$referral_object->referral_id = $id;
		$referral_object->referral_value = $value;
		$result = JFactory::getDbo()->insertObject('#__referral_value', $referral_object);
		return JFactory::getDbo()->insertid();
	}
	public function enableSavedListing($pid, $aid, $bid){
		$db = JFactory::getDbo();
		$query_0 = $db->getQuery(true);
		$query_0->select("*");
		$query_0->from('#__buyer_saved_listing');
		$query_0->where('agent_id = \''.$aid.'\'');
		$query_0->where('listing_id = \''.$pid.'\'');
		$query_0->where('buyer_id = \''.$bid.'\'');
		//$query_0->where('active = 1');
		#echo $query_0; die();
		$db->setQuery($query_0);
		$existing = $db->loadObjectList();
		$ctr = 0;
		foreach($existing as $ex){ $ctr++; }
		if($ctr == 0){
			$insert = new JObject();
			$insert->buyer_id = $bid;
			$insert->agent_id = $aid;
			$insert->listing_id = $pid;
			$ret = $db->insertObject('#__buyer_saved_listing', $insert);
			return $ret;
		} else {
			$query = $db->getQuery(true);
			$fields = array($db->quoteName('active') . '=1');
			$conditions = array(
				$db->quoteName('agent_id') . '=\''.$aid.'\'', 
				$db->quoteName('listing_id') . '=\''.$pid.'\'', 
				$db->quoteName('buyer_id') . '=\''.$bid.'\''
			);
			$query->update($db->quoteName('#__buyer_saved_listing'))->set($fields)->where($conditions);
			#echo $query; die();
			$db->setQuery($query);			 
			$result = $db->execute();
		}
	}
	public function disableSavedListing($pid, $aid, $bid){
		$db = JFactory::getDbo();
		$query_0 = $db->getQuery(true);
		$query_0->select("*");
		$query_0->from('#__buyer_saved_listing');
		$query_0->where('agent_id = \''.$aid.'\'');
		$query_0->where('listing_id = \''.$pid.'\'');
		$query_0->where('buyer_id = \''.$bid.'\'');
		$db->setQuery($query_0);
		$existing = $db->loadObjectList();
		$ctr = 0;
		foreach($existing as $ex){ $ctr++; }
		if($ctr == 0){
			$insert = new JObject();
			$insert->buyer_id = $bid;
			$insert->agent_id = $aid;
			$insert->listing_id = $pid;
			$insert->active = 0;
			$ret = $db->insertObject('#__buyer_saved_listing', $insert);
			return $ret;
		} else {
			$query = $db->getQuery(true);
			$fields = array($db->quoteName('active') . '=0');
			$conditions = array(
				$db->quoteName('agent_id') . '=\''.$aid.'\'', 
				$db->quoteName('listing_id') . '=\''.$pid.'\'', 
				$db->quoteName('buyer_id') . '=\''.$bid.'\''
			);
			$query->update($db->quoteName('#__buyer_saved_listing'))->set($fields)->where($conditions);
			$db->setQuery($query);			 
			$result = $db->execute();
			#echo $query."----------";
		}
	}
	public function getSavedListing($aid, $bid){
		$db = JFactory::getDbo();
		$query_0 = $db->getQuery(true);
		$query_0->select("bs.*");
		$query_0->from('#__buyer_saved_listing bs');
		$query_0->leftJoin('#__pocket_listing pl ON pl.listing_id = bs.listing_id');
		$query_0->where('bs.agent_id = \''.$aid.'\'');
		$query_0->where('bs.buyer_id = \''.$bid.'\'');
		$query_0->where('pl.closed = 0');
		$query_0->where('bs.active = \'1\'');
		$query_0->order('pkId DESC');
		$db->setQuery($query_0);
		#echo "<pre>".$query_0; die();
		return $db->loadObjectList();
	}
	public function getSavedBuyerListing($aid, $bid){
		$db = JFactory::getDbo();
		$query_0 = $db->getQuery(true);
		$query_0->select("*");
		$query_0->from('#__buyer_saved_listing');
		$query_0->where('agent_id = \''.$aid.'\'');
		$query_0->where('listing_id = \''.$bid.'\'');
		$query_0->where('active = \'1\'');
		$db->setQuery($query_0);
		#echo "<pre>".$query_0; die();
		return $db->loadObjectList();
	}
	public function insertPocket($user_id, $data) {
		$db = JFactory::getDbo();
		$insert = new JObject();
		$insert->user_id = $user_id;
		$insert->property_name = $db->escape($data['property_name']);
		$insert->city = $data['city'];
		$insert->zip = $data['zip'];
		$insert->state = $data['state'];
		$insert->country = $data['country'];
		$insert->currency = $data['currency'];
		$insert->property_type = $data['ptype'];
		$insert->sub_type = $data['stype'];
		$insert->bedroom = $data['bedroom'];
		$insert->bathroom = $data['bathroom'];
		$insert->unit_sqft = $data['unitsqft'];
		$insert->view = $data['view'];
		$insert->style = $data['style'];
		$insert->year_built = $data['yearbuilt'];
		$insert->pool_spa = $data['poolspa'];
		$insert->condition = $data['condition'];
		$insert->garage = $data['garage'];
		$insert->units = $data['units'];
		$insert->cap_rate = $data['cap'];
		$insert->grm = $data['grm'];
		$insert->occupancy = $data['occupancy'];
		$insert->type = $data['type'];
		$insert->listing_class = $data['class'];
		$insert->parking_ratio = $data['parking'];
		$insert->ceiling_height = $data['ceiling'];
		$insert->stories = $data['stories'];
		$insert->room_count = $data['roomcount'];
		$insert->type_lease = $data['typelease'];
		$insert->type_lease2 = $data['typelease2'];
		$insert->available_sqft = $data['available'];
		$insert->lot_sqft = $data['lotsqft'];
		$insert->lot_size = $data['lotsize'];
		$insert->bldg_sqft = $data['bldgsqft'];
		$insert->term = $data['term'];
		$insert->furnished = $data['furnished'];
		$insert->pet = $data['pet'];
		$insert->zoned = $data['zoned'];
		$insert->possession = $data['possession'];
		$insert->bldg_type = $data['bldgtype'];
		$insert->features1 = $data['features1'];
		$insert->features2 = $data['features2'];
		$insert->features3 = $data['features3'];
		$insert->setting = $data['setting'];
		$insert->description = $data['desc'];
		$insert->date_created = date("Y-m-d");
		$insert->date_expired = date('Y-m-d', strtotime(date('Y-m-d'). ' + 90 days'));
		$ret = $db->insertObject('#__pocket_listing', $insert);
		$ret = $db->insertid();
		return $ret;
	}
	public function searchZip($searchval){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		#$query = "SELECT * FROM #__pocket_listing WHERE user_id='".$user."' AND zip LIKE '".$searchval."%'";
		$query = "SELECT * FROM #__pocket_listing WHERE zip LIKE '".$searchval."%'";
		$db->setQuery($query);
		//echo "<pre>"; print_r($db->loadObjectList()); die();
		return $db->loadObjectList();
	}
	public function searchAll(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		#$query = "SELECT * FROM #__pocket_listing WHERE user_id='".$user."' AND zip LIKE '".$searchval."%'";
		$query = "SELECT * FROM #__pocket_listing";
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function getBuyerPageDetails($userid, $filters = array()){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array();
		foreach($filters as $filter => $value){
			$conditions[] = $filter." = ".$value;
		}
	//	array_push($conditions, "bn.buyer_id NOT NULL ");
	//	var_dump($conditions);
		//$query->select('*')->from('#__buyer')->where(array_merge($conditions, array('agent_id = '.$userid)));
		//$buyers = $db->setQuery($query)->loadObjectList();
		//foreach ($buyers as $buyer) {
		//	$this->resetNewBuyerCount($buyer->buyer_id,$buyer->listingcount,$buyer->reset_new);
		//}
		$query = $db->getQuery(true);
		$query->select('*')->from('#__buyer b')->where(array_merge($conditions, array('agent_id = '.$userid)));
		$buyers = $db->setQuery($query)->loadObjectList();
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('bn.*,con.*,currcon.symbol,pst.name AS sub_name,pt.type_name AS ptype_name, cvs.*')->from('#__buyer_needs bn')
			->leftJoin('#__country_currency currcon ON bn.currency = currcon.currency')
			->leftJoin('#__country_validations cvs ON bn.country = cvs.country')
			->leftJoin('#__countries con ON con.countries_id = bn.country')
			->leftJoin('#__property_sub_type pst ON pst.sub_id = bn.sub_type')
			->leftJoin('#__property_type pt ON pt.type_id = bn.property_type')
			->where('bn.buyer_id = '.$buyer->buyer_id);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		$today = date ('Y-m-d');
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('bs.*')->from('#__buyer_saved_listing bs')->leftJoin('#__pocket_listing pl ON pl.listing_id = bs.listing_id')->where("pl.closed=0 AND DATE(pl.date_expired) > '".$today."' AND buyer_id = ".$buyer->buyer_id);
			$buyer->hassaved = $db->setQuery($query)->loadObjectList();
		}			
		#echo "<pre>"; print_r($buyers);
		return $buyers;
	}

	public function getBuyerPageDetails_opti($userid,$limit_one = ""){
		$db = JFactory::getDbo();
		$today = date ('Y-m-d');
		$query = $db->getQuery(true);
		$query->select('b.*,bn.*,con.*,currcon.symbol,pst.name AS sub_name,pt.type_name AS ptype_name, cvs.* , COUNT(Distinct bs.listing_id) AS hassaved')
		->innerJoin('#__buyer_needs bn ON bn.buyer_id = b.buyer_id')
		->leftJoin('#__buyer_saved_listing bs ON bs.buyer_id = b.buyer_id')
		->leftJoin('#__pocket_listing pl ON pl.listing_id = bs.listing_id AND pl.closed=0 AND DATE(pl.date_expired) > '.$today)
	    ->leftJoin('#__country_currency currcon ON bn.currency = currcon.currency')
		->leftJoin('#__country_validations cvs ON bn.country = cvs.country')
		->leftJoin('#__countries con ON con.countries_id = bn.country')
		->leftJoin('#__property_sub_type pst ON pst.sub_id = bn.sub_type')
		->leftJoin('#__property_type pt ON pt.type_id = bn.property_type')
		->from('#__buyer b')
		->where('b.agent_id = '.$userid)
		->group("b.buyer_id")
		->order('b.buyer_type,b.buyer_id DESC');
		if($limit_one){
			$query->setLimit(3, $limit_one);
		}


		//var_dump($query->__toString());
		
		$buyers = $db->setQuery($query)->loadAssocList();		
	
		#echo "<pre>"; print_r($buyers);
		return $buyers;
	}

	public function getBuyerCount_opti($userid){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('b.buyer_id')
		->innerJoin('#__buyer_needs bn ON bn.buyer_id = b.buyer_id')
		->from('#__buyer b')->group("b.buyer_id")
		->where('b.agent_id = '.$userid);

		$buyers = $db->setQuery($query)->loadAssocList();		
	
		#echo "<pre>"; print_r($buyers);
		return $buyers;
	}

	public function getPOPs_simple($listingId){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);	
		$query->select('*')
		->from('#__pocket_listing p')
		->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id')
		->leftJoin('#__country_currency conc_c ON conc_c.currency = p.currency')
		->where(array('p.listing_id = '.$listingId));
		$buyer = $db->setQuery($query)->loadAssoc();
		#print_r($buyers); die();
		
		return $buyer;
	}

	public function getBuyer($buyerid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);	
		$query->select('*')
		->from('#__buyer')		
		->where(array('buyer_id = '.$buyerid));
		$buyers = $db->setQuery($query)->loadObjectList();
		#print_r($buyers); die();
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('bn.*,conc.country,  cvs.*, conc_c.currency, conc_c.symbol, ps.name AS subtype_name, pt.type_name AS proptype_name')->from('#__buyer_needs bn')
			->leftJoin('#__country_currency conc_c ON conc_c.currency = bn.currency')
			->leftJoin('#__country_validations cvs ON bn.country = cvs.country')
			->leftJoin('#__country_currency conc ON conc.country = bn.country')
			->leftJoin('#__property_type pt ON pt.type_id = bn.property_type')
		    ->leftJoin('#__property_sub_type ps ON ps.sub_id = bn.sub_type')
			->where('bn.buyer_id = '.$buyerid);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		return $buyers;
	}

	public function getBuyer_simple($buyerid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);	
		$query->select('*')
		->from('#__buyer b')
		->leftJoin('#__buyer_needs bn ON b.buyer_id = bn.buyer_id')
		->leftJoin('#__country_currency conc_c ON conc_c.currency = bn.currency')
		->where(array('b.buyer_id = '.$buyerid));
		$buyer = $db->setQuery($query)->loadObject();
		#print_r($buyers); die();
		
		return $buyer;
	}

	public function getBuyerAll_simple($subtype){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);	
		$query->select('*')
		->from('#__buyer b')
		->leftJoin('#__buyer_needs bn ON b.buyer_id = bn.buyer_id')
		->leftJoin('#__country_currency conc_c ON conc_c.currency = bn.currency')
		// ->leftJoin('#__user_device_token ud ON ud.user_id = b.agent_id')
		->leftJoin('(SELECT GROUP_CONCAT(device_token) as device_tokens, user_id from #__user_device_token where device_os = "android" group by user_id) ud on b.agent_id =  ud.user_id')
		->group('b.buyer_id')
		->where('bn.sub_type = '.$subtype);
		$buyer = $db->setQuery($query)->loadObjectList();
		#print_r($buyers); die();
		
		return $buyer;
	}

	public function array_search_partial($arr,$keyword) {
	     for ($i = 0; $i < count($arr); $i++) {
	     	$z_check_len = strlen(trim($arr[$i]));
	     	if(substr($keyword, 0, $z_check_len) == trim($arr[$i])){
	     		return true;
	     	}
			//if(strpos(strtolower($keyword),strtolower(trim($arr[$i]))) !== FALSE){
	        //    return true;
			//} 
		 }
		return false;
	}

	public function getBuyerAll(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__buyer');
		$buyers = $db->setQuery($query)->loadObjectList();
		//
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		//echo "<pre>"; print_r($buyers); die();
		#echo "<pre>";  print_r($buyers); die();
		return $buyers;
	}
	
	
	public function getBuyerSameType($type, $subtype, $zip) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('b.*')->from('#__buyer as b');
		$query->leftJoin('#__buyer_needs bn on bn.buyer_id = b.buyer_id');
		$query->where("bn.property_type = $type AND bn.sub_type = $subtype AND zip LIKE '%" . $zip . "%'");
		$buyers = $db->setQuery($query)->loadObjectList();
		//
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		//echo "<pre>"; print_r($buyers); die();
		//echo "<pre>";  print_r($buyers); die();
		return $buyers;
	}
	
	public function getBuyerFiltered($type, $subtype, $zip, $data) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('b.*, bn.currency, bn.zip, bn.country, bn.property_type, bn.sub_type')->from('#__buyer as b');
		$query->leftJoin('#__buyer_needs bn on bn.buyer_id = b.buyer_id');
		
		switch($type) {
			case 1:
				switch($subtype) {
					case 1:
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow;
						break;
					case 2:
						$pkunitrange = $data->unit_sqft;
						$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
						$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " AND CAST(REPLACE(SUBSTRING_INDEX(bn.unit_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkunitlow;
						break;
					case 3:
						$pkunitrange = $data->unit_sqft;
						$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
						$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " AND CAST(REPLACE(SUBSTRING_INDEX(bn.unit_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkunitlow;
						break;
					case 4:
						$pkzonedrange = $data->zoned;
						$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
						$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
						$pklotszrange = $data->lot_size;
						$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
						$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
						$condition = " CAST(REPLACE(SUBSTRING_INDEX(bn.zoned,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkzonedlow . " AND CAST(REPLACE(SUBSTRING_INDEX(bn.lot_size,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pklotszlow;
						break;
				}
				break;
			case 2:
				switch($subtype) {
					case 5:
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " 
									AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " 
									AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow . " 
									AND bn.term <= '" . $data->term . "' 
									AND bn.pet <= '" . $data->pet . "' 
									AND bn.furnished <= '" . $data->furnished . "'
									AND bn.posession <= '" . $data->posession . "'";
						break;
					case 6:
						$pkunitrange = $data->unit_sqft;
						$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
						$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " 
									AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " 
									AND CAST(REPLACE(SUBSTRING_INDEX(bn.unit_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkunitlow . " 
									AND bn.term <= '" . $data->term . "' 
									AND bn.pet <= '" . $data->pet . "' 
									AND bn.furnished <= '" . $data->furnished . "'";
						break;
					case 7:
						$pkunitrange = $data->unit_sqft;
						$pkunitexplode = explode("-", $pkunitrange); // split the ranged value into two, returning an array
						$pkunitlow = str_replace(",", "", $pkunitexplode[0]); //lower range for buyer
						$condition = " CAST(bn.bedroom as UNSIGNED INTEGER) <= " . (int)$data->bedroom . " 
									AND CAST(bn.bathroom as UNSIGNED INTEGER) <= " . (int)$data->bathroom . " 
									AND CAST(REPLACE(SUBSTRING_INDEX(bn.unit_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkunitlow . " 
									AND bn.term <= '" . $data->term . "' 
									AND bn.pet <= '" . $data->pet . "' 
									AND bn.furnished <= '" . $data->furnished . "'";
						break;
					case 8:
						$pkzonedrange = $data->zoned;
						$pkzonedexplode = explode("-", $pkzonedrange); // split the ranged value into two, returning an array
						$pkzonedlow = str_replace(",", "", $pkzonedexplode[0]); //lower range for buyer
						$pklotszrange = $data->lot_size;
						$pklotszexplode = explode("-", $pklotszrange); // split the ranged value into two, returning an array
						$pklotszlow = str_replace(",", "", $pklotszexplode[0]); //lower range for buyer
						$condition = " CAST(REPLACE(SUBSTRING_INDEX(bn.zoned,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkzonedlow . " 
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.lot_size,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pklotszlow . "
							AND bn.view <= '" . $data->view . "'";
						break;
				}
				break;
			case 3:
				switch($subtype) {
					case 9:
						$pkunitsrange = $data->units;
						$pkunitsexplode = explode("-", $pkunitsrange); // split the ranged value into two, returning an array
						$pkunitslow = str_replace(",", "", $pkunitsexplode[0]); //lower range for buyer
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " CAST(REPLACE(SUBSTRING_INDEX(bn.units,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkunitslow . " 
								AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
								AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow;
						break;
					case 10:
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "' AND bn.listing_class <= '" . $data->listing_class . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow;
						break;
					case 11:
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow;
						break;
					case 12:
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.bldg_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkbldglow;
						break;
					case 13:
						$pkroomrange = $data->room_count;
						$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
						$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.room_count,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkroomlow;
						break;
					case 14:
						$pkroomrange = $data->room_count;
						$pkroomexplode = explode("-", $pkroomrange); // split the ranged value into two, returning an array
						$pkroomlow = str_replace(",", "", $pkroomexplode[0]); //lower range for buyer
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow . "
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.room_count,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkroomlow;
						break;
					case 15:
						$pkcapgrange = $data->cap_rate;
						$pkcapexplode = explode("-", $pkcaprange); // split the ranged value into two, returning an array
						$pkcaplow = str_replace(",", "", $pkcapexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.cap_rate,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkcaplow;
						break;
				}
				break;
			case 4:
				switch($subtype) {
					case 16:
						$pkavailablerange = $data->available_sqft;
						$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
						$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "' AND bn.listing_class <= '" . $data->listing_class . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.available_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkavailablelow;
						break;
					case 17:
						$pkavailablerange = $data->available_sqft;
						$pkavailableexplode = explode("-", $pkavailablerange); // split the ranged value into two
						$pkavailablelow = str_replace(",", "", $pkavailableexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "' AND bn.type_lease <= '" . $data->type_lease . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.available_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkavailablelow;
						break;
					case 18:
						$pkbldgrange = $data->bldg_sqft;
						$pkbldgexplode = explode("-", $pkbldgrange); // split the ranged value into two, returning an array
						$pkbldglow = str_replace(",", "", $pkbldgexplode[0]); //lower range for buyer
						$condition = " bn.type <= '" . $data->type . "'
							AND CAST(REPLACE(SUBSTRING_INDEX(bn.available_sqft,'-',1),',','') AS UNSIGNED INTEGER) <= " . $pkavailablelow;
						break;
				}
				break;
		}
		
		
		$query->where("bn.property_type = $type AND bn.sub_type = $subtype AND zip LIKE '%" . $zip . "%' AND $condition");
		
		$buyers = $db->setQuery($query)->loadObjectList();
		//
		/*
		foreach ($buyers as $buyer){
			$query = $db->getQuery(true);
			$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id);
			$buyer->needs = $db->setQuery($query)->loadObjectList();
		}
		*/
		//echo "<pre>"; print_r($buyers); die();
		//echo "<pre>";  print_r($buyers); die();
		return $buyers;
	}
	
	public function resetBuyerListings($bid, $type){
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->buyer_id = $bid;
		$update->reset_new = 1;
		$update->listingcount_new = 0;
		if($type == 0){	
			$update->hasnew = 0;
			$update->hasnew_2 = 0;
		} else if($type == 1){
			$update->hasnew = 0;
		} else if($type == 2){
			$update->hasnew_2 = 0;
		}		
		$db->updateObject('#__buyer', $update, 'buyer_id', false);
	}
	public function resetBuyerListings2($bid, $type){
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->buyer_id = $bid;
		$update->listingcount_new = 0;
		$update->reset_new = 1;
		if($type == 0){	
			$update->hasnew = 0;
			$update->hasnew_2 = 0;
		} else if($type == 1){
			$update->hasnew = 0;
		} else if($type == 2){
			$update->hasnew_2 = 0;
		}		
		// $db->setQuery('delete from #__buyer_new_listing where buyer_id = '.$bid);
		// $db->execute();
		$db->updateObject('#__buyer', $update, 'buyer_id', false);
	}
	public function resetNewBuyerCount($bid,$listingcount,$reset_value){
		if($reset_value==0){
			$update = new JObject();
			if($listingcount){
				$update->reset_new = 1;
				$update->buyer_id = $bid;
				$update->listingcount_new = $listingcount;
				$update->hasnew = 1;
				$update->hasnew_2 = 1;
			} else {
				$update->reset_new = 1;
				$update->buyer_id = $bid;
			}
			$db = JFactory::getDbo();
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
	}
	public function resetListingCount($bid,$listingcount,$reset_value){
		if($reset_value==0){
			$update = new JObject();		
			$update->reset_listing = 1;
			$update->buyer_id = $bid;
			$update->listingcount = $listingcount;			
			$db = JFactory::getDbo();
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}
	}

	function removeDuplicates($array) {
	   $valueCount = array();
	   $valueCount=array_count_values($array);
	   $return = array();
	   foreach ($valueCount as $value => $count) {
	      if ( $count == 1 ) {
	         $return[] = $value;
	      }
	   }

	   return $return;
	}

	public function insertToBuyerListings_pops($buyer_ids, $listing_id, $transaction){

		if($transaction=="edit"){ //edit pops
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select('buyer_id')
			->from('#__buyer_new_listing')
			->where('listing_id='.$listing_id);
			$db->setQuery($query);
			$buyerIds_array = $db->loadColumn();

			$oldcount= count($buyerIds_array); // number of buyers previous match
			$newcount= count($buyer_ids); //number of buyers now matched with listingid

			$new_buyers= $buyer_ids;

			$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( "POPs edit" );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody( print_r($buyerIds_array,true)."-----".print_r($buyer_ids,true));
			$mailSender ->Send(); 

			if($oldcount){

				
				$remove_this = array_diff($buyerIds_array, $buyer_ids);
				$removed_count= count($remove_this);
				$add_this = array_diff($buyer_ids,$buyerIds_array);
				$add_count= count($add_this);

				

				if($removed_count){

					$db = JFactory::getDbo(); 
					$query = $db->getQuery(true);
					$conditions = "listing_id = ".$listing_id." AND buyer_id IN (".implode(",", $remove_this).")";
					$query->delete($db->quoteName('#__buyer_new_listing'));
					$query->where($conditions);		 
					$db->setQuery($query);		 
					$result = $db->execute();

 
					//Remove Count to New and All
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					// Fields to update.
					/*$fields = array(
						$db->quoteName('listingcount_new') . ' = ' . $db->quoteName('listingcount_new')-1,
					    $db->quoteName('listingcount') . ' = ' . $db->quoteName('listingcount')-1,
					);*/

					$fields = "listingcount_new = listingcount_new-1,listingcount = listingcount-1";
					$conditions = "buyer_id IN (".implode(",", $removed_buyers).") AND listingcount > 0 AND listingcount_new > 0";
					$query->update($db->quoteName('#__buyer'))->set($fields)->where($conditions);
					$db->setQuery($query);
					$result = $db->execute();

				} 

				if($add_count){

					$new_buyers= $add_this;

					$buyerId_inserts = array();
					foreach ($new_buyers as $key => $value) {
						# code...
						$buyerId_inserts[] = $value.", ".$listing_id;
					}

					// add new listing id to buyers
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$columns = array('buyer_id','listing_id');
					$query->insert($db->quoteName('#__buyer_new_listing'));
					$query->columns($columns);
					$query->values($buyerId_inserts);
					$db->setQuery($query);
					$ret =$db->execute();

					//Add Count to New
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					/*$fields = array(
						$db->quoteName('listingcount_new') . ' = ' . $db->quoteName('listingcount_new')+1,
					    $db->quoteName('listingcount') . ' = ' . $db->quoteName('listingcount')+1,
					    $db->quoteName('hasnew') . ' = ' . 1,
					    $db->quoteName('hasnew_2') . ' = ' . 1,
					);*/
					$fields = "listingcount_new = listingcount_new+1,listingcountw = listingcount+1,hasnew=1,hasnew_2=1";
					$conditions = "buyer_id IN (".implode(",", $buyer_ids).")";
					$query->update($db->quoteName('#__buyer'))->set($fields)->where($conditions);
					$db->setQuery($query);
					$result = $db->execute();

				}
			
			} else {
				$new_buyers= $buyer_ids;

				$buyerId_inserts = array();
				foreach ($new_buyers as $key => $value) {
					# code...
					$buyerId_inserts[] = $value.", ".$listing_id;
				}

				// add new listing id to buyers
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$columns = array('buyer_id','listing_id');
				$query->insert($db->quoteName('#__buyer_new_listing'));
				$query->columns($columns);
				$query->values($buyerId_inserts);
				$db->setQuery($query);
				$ret =$db->execute();

				//Add Count to New
				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				/*$fields = array(
					$db->quoteName('listingcount_new') . ' = ' . $db->quoteName('listingcount_new')+1,
				    $db->quoteName('listingcount') . ' = ' . $db->quoteName('listingcount')+1,
				    $db->quoteName('hasnew') . ' = ' . 1,
				    $db->quoteName('hasnew_2') . ' = ' . 1,
				);*/
				$fields = "listingcount_new = listingcount_new+1,listingcount = listingcount+1,hasnew=1,hasnew_2=1";
				$conditions = "buyer_id IN (".implode(",", $buyer_ids).")";
				$query->update($db->quoteName('#__buyer'))->set($fields)->where($conditions);
				$db->setQuery($query);
				$result = $db->execute();
			}

			
	
		} else { // new pops

			$new_buyers= $buyer_ids;

			$buyerId_inserts = array();
			foreach ($new_buyers as $key => $value) {
				# code...
				$buyerId_inserts[] = $value.", ".$listing_id;
			}

			// add new listing id to buyers
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$columns = array('buyer_id','listing_id');
			$query->insert($db->quoteName('#__buyer_new_listing'));
			$query->columns($columns);
			$query->values($buyerId_inserts);
			$db->setQuery($query);
			$ret =$db->execute();


			//Add Count to New
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			// Fields to update.
			/*$fields = array(
				$db->quoteName('listingcount_new') . ' = ' . $db->quoteName('listingcount_new')+1,
			    $db->quoteName('listingcount') . ' = ' . $db->quoteName('listingcount')+1,
			    $db->quoteName('hasnew') . ' = ' . 1,
			    $db->quoteName('hasnew_2') . ' = ' . 1,
			);*/
			$fields = "listingcount_new = listingcount_new+1, listingcount = listingcount+1,hasnew=1,hasnew_2=1";
			$conditions = "buyer_id IN (".implode(",", $buyer_ids).")";
			$query->update($db->quoteName('#__buyer'))->set($fields)->where($conditions);
			$db->setQuery($query);
			$result = $db->execute();

		}

		$_SESSION['viewed_buyerspage'] = 0;
		$_SESSION['viewed_buyerspage_2'] = 0;
		return $ret;
	}

	public function insertToBuyerListings_optimized($bid, $listingAll, $transaction, $pops_trans=0){

		if($transaction=='edit'){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select('listing_id')
			->from('#__buyer_new_listing')
			->where('buyer_id='.$bid);
			$db->setQuery($query);
			$listingIds_array = $db->loadColumn();

			$oldcount= count($listingIds_array);
			$newcount= count($listingAll);


		
			if(count($listingAll)){

				if($newcount > $oldcount && $oldcount!=0){
					var_dump($newcount);
					var_dump($oldcount);
					var_dump($listingIds_array);
					var_dump($listingAll);
					var_dump($listingAll);

					/*for ($j = 0; $j < count($listingAll); $j++) {
				        if(!in_array($listingAll[$j],$listingIds_array)){ 
				            $new_listing[] = $listingAll[$j]; 
				        } 
				    } */

				    $remove_this = array_diff($listingIds_array, $listingAll);
					$removed_count= count($remove_this);
					$add_this = array_diff($listingAll,$listingIds_array);
					$add_count= count($add_this);

				//	$new_listing = array_diff($listingIds_array,$listingAll);

					 if($removed_count){
					 	$new_listing = $remove_this;
					 } else if($add_count){
					 	$new_listing = $add_this;
					 }

					 $all_new_count = count($listingAll);
					 $new_count = count($new_listing);	
				} else {
					$new_listing = $listingAll;
					$all_new_count = count($new_listing);
					$new_count = count($new_listing);	
				}
	
			} else {
				$new_listing = $listingAll;
				$new_count = count($new_listing);
				$all_new_count = $new_count;
			}


			$listingId_inserts = array();
			foreach ($new_listing as $key => $value) {
				# code...
				$listingId_inserts[] = $bid.", ".$value;
			}

			$query1 = $db->getQuery(true);
			$query1 = "SELECT listing_id FROM #__buyer_saved_listing WHERE buyer_id='".$bid."'";
			$db->setQuery($query1);
			$bl = $db->loadObjectList();
			$bnl_count = count($bl) + $all_new_count;

		} else {
			foreach ($listingAll as $key => $value) {
				# code...
				$listingId_inserts[] = $bid.", ".$value;
			}
			//$listingId_inserts = $listingAll;
			$new_count = count($listingAll);
			$bnl_count = $new_count;
		}

		

		//delete all new listing from buyer
		if($transaction=="edit"){
			$db = JFactory::getDbo(); 
			$query = $db->getQuery(true);
			$conditions = array($db->quoteName('buyer_id') . ' = '.$bid);		 
			$query->delete($db->quoteName('#__buyer_new_listing'));
			$query->where($conditions);		 
			$db->setQuery($query);		 
			$result = $db->execute();
		}

		//if($listingId_inserts){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$columns = array('buyer_id','listing_id');
			$values = array();
			// Proper escaping/quotes should be done here, and probably in a loop, but cluttered the answer, so omitted it
			$query->insert($db->quoteName('#__buyer_new_listing'));
			$query->columns($columns);
			$query->values($listingId_inserts);
			$db->setQuery($query);
			$ret =$db->execute();
		//}




		//if($new_count){	
			$update = new JObject();
			$update->listingcount = $bnl_count;
			$update->buyer_id = $bid;
			$update->hasnew = 1;
			$update->listingcount_new = $new_count;
			$update->hasnew_2 = 1;

			$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( "Edit Buyer" );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody( print_r($update,true));
			$mailSender ->Send(); 


			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
			echo $bnl_count." > ".$bl_count;		
		//}
		$_SESSION['viewed_buyerspage'] = 0;
		$_SESSION['viewed_buyerspage_2'] = 0;
		return $ret;
	}

	public function insertToBuyerListings($bid, $pid, $listingAll, $pops_trans=0){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select('*')
		->from('#__buyer_new_listing')
		->where('listing_id = ' . $pid. ' AND buyer_id='.$bid);
		$db->setQuery($query);
		$objects = $db->loadRow();
		if(count($objects)==0){
			$insert = new JObject();
			$insert->buyer_id = $bid;
			$insert->listing_id = $pid;
			$ret = $db->insertObject('#__buyer_new_listing', $insert);
			$refid = $db->insertid();
			$query0 = $db->getQuery(true);
			//$query0 = "SELECT COUNT(DISTINCT listing_id) as 'bnl_count' FROM #__buyer_new_listing WHERE buyer_id='".$bid."'";''
			$query0 = "SELECT COUNT( listing_id ) AS 'bnl_count', COUNT( * ) - COUNT( DISTINCT listing_id ) AS  'dup_list' FROM tbl_buyer_new_listing WHERE buyer_id =".$bid." GROUP BY listing_id HAVING dup_list =0";
			$db->setQuery($query0);
			$bnl = $db->loadObjectList();
			$bnl_count = $listingAll;
			$bnl_new = count($bnl);
			$query1 = $db->getQuery(true);
			$query1 = "SELECT listingcount FROM #__buyer WHERE buyer_id='".$bid."'";
			$db->setQuery($query1);
			$bl = $db->loadObjectList();
			$bl_count = $bl[0]->listingcount;
			if($bnl_new ){
				$query3 = $db->getQuery(true);
				$query3 = "SELECT listingcount_new FROM #__buyer WHERE buyer_id='".$bid."' AND hasnew_2=1";
				$db->setQuery($query3);
				$listing_news = $db->loadObjectList();
				$listing_news = $listing_news[0]->listingcount_new;
				if($pops_trans==1){
					$bnl_new = $listing_news+1;
				} 
				$update = new JObject();
				$update->listingcount = $bnl_count;
				$update->buyer_id = $bid;
				$update->hasnew = 1;
				$update->listingcount_new = $bnl_new;
				$update->hasnew_2 = 1;
				$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
				echo $bnl_count." > ".$bl_count;
			} else {
				echo "none";
			}
		}
		$_SESSION['viewed_buyerspage'] = 0;
		$_SESSION['viewed_buyerspage_2'] = 0;
		return $ret;
	}
	public function insertBuyerNeeds($data){
		$db = JFactory::getDbo();
		$insert = new JObject();
		$insert->buyer_id = $data['buyer_id'];
		$insert->property_name = $data['property_name'];
		$insert->city = $data['city'];
		$insert->zip = $data['zip'];
		$insert->state = $data['state'];
		$insert->country = $data['country'];
		$insert->currency = $data['currency'];
		$insert->property_type = $data['ptype'];
		$insert->sub_type = $data['stype'];
		$insert->bedroom = $data['bedroom'];
		$insert->bathroom = $data['bathroom'];
		$insert->unit_sqft = $data['unitsqft'];
		$insert->view = $data['view'];
		$insert->style = $data['style'];
		$insert->year_built = $data['yearbuilt'];
		$insert->pool_spa = $data['poolspa'];
		$insert->condition = $data['condition'];
		$insert->garage = $data['garage'];
		$insert->units = $data['units'];
		$insert->cap_rate = $data['cap'];
		$insert->grm = $data['grm'];
		$insert->occupancy = $data['occupancy'];
		$insert->type = $data['type'];
		$insert->listing_class = $data['class'];
		$insert->parking_ratio = $data['parking'];
		$insert->ceiling_height = $data['ceiling'];
		$insert->stories = $data['stories'];
		$insert->room_count = $data['roomcount'];
		$insert->type_lease = $data['typelease'];
		$insert->type_lease2 = $data['typelease2'];
		$insert->available_sqft = $data['available'];
		$insert->lot_sqft = $data['lotsqft'];
		$insert->lot_size = $data['lotsize'];
		$insert->bldg_sqft = $data['bldgsqft'];
		$insert->term = $data['term'];
		$insert->furnished = $data['furnished'];
		$insert->pet = $data['pet'];
		$insert->zoned = $data['zoned'];
		$insert->possession = $data['possession'];
		$insert->bldg_type = $data['bldgtype'];
		$insert->features1 = $data['features1'];
		$insert->features2 = $data['features2'];
		$insert->features3 = $data['features3'];
		$insert->setting = $data['settings'];
		$property_type = $insert->property_type;
		$sub_type = $insert->sub_type;
		//print_r($insert);
		$ret = $db->insertObject('#__buyer_needs', $insert);
		$ret = $db->insertid();
		return $ret;
	}
	public function deleteExistingNeeds($data, $buyer_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "
			UPDATE tbl_buyer_needs 
			SET bedroom = 0, bathroom = 0, unit_sqft = NULL, view = NULL, style = NULL, year_built = NULL, pool_spa = NULL, garage = NULL, units = NULL, cap_rate = NULL, grm = NULL, type = NULL, occupancy = NULL, listing_class = NULL, parking_ratio = NULL, ceiling_height = NULL, stories = NULL, room_count = NULL, type_lease = NULL, type_lease2 = NULL, available_sqft = NULL, lot_size = NULL, lot_sqft = NULL, bldg_sqft = NULL, term = NULL, furnished = 0, pet = 0, possession = NULL, zoned = NULL, bldg_type = NULL, features1 = NULL, features2 = NULL, features3 = NULL
			WHERE buyer_id = $buyer_id
		";
		$db->setQuery($query);
		$db->execute();
	}
	public function updateBuyerNeeds($data, $buyer_id){
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->buyer_id = $buyer_id;
		$update->property_name = $data['property_name'];
		$update->city = $data['city'];
		$buyerzip = explode(", ",$data['zip']);
		$buyerzip = implode(",", $buyerzip);
		$update->zip = $buyerzip;
		$update->state = $data['state'];
		$update->country = $data['country'];
		$update->currency = $data['currency'];
		$update->property_type = $data['ptype'];
		$update->sub_type = $data['stype'];
		$update->bedroom = $data['bedroom'];
		$update->bathroom = $data['bathroom'];
		$update->unit_sqft = $data['unitsqft'];
		$update->view = $data['view'];
		$update->style = $data['style'];
		$update->year_built = $data['yearbuilt'];
		$update->pool_spa = $data['poolspa'];
		$update->condition = $data['condition'];
		$update->garage = $data['garage'];
		$update->units = $data['units'];
		$update->cap_rate = $data['cap'];
		$update->grm = $data['grm'];
		$update->occupancy = $data['occupancy'];
		$update->type = $data['type'];
		$update->listing_class = $data['class'];
		$update->parking_ratio = $data['parking'];
		$update->ceiling_height = $data['ceiling'];
		$update->stories = $data['stories'];
		$update->room_count = $data['roomcount'];
		$update->type_lease = $data['typelease'];
		$update->type_lease2 = $data['typelease2'];
		$update->available_sqft = $data['available'];
		$update->lot_sqft = $data['lotsqft'];
		$update->lot_size = $data['lotsize'];
		$update->bldg_sqft = $data['bldgsqft'];
		$update->term = $data['term'];
		$update->furnished = $data['furnished'];
		$update->pet = $data['pet'];
		$update->zoned = $data['zoned'];
		$update->possession = $data['possession'];
		$update->bldg_type = $data['bldgtype'];
		$update->features1 = $data['features1'];
		$update->features2 = $data['features2'];
		$update->features3 = $data['features3'];
		$update->setting = $data['settings'];
		//print_r($update);
		//$ret = $db->insertObject('#__buyer_needs', $update);
		$ret = $db->updateObject('#__buyer_needs', $update, 'buyer_id', false);
		
		return $ret;
	}

	public function updateBuyerNeeds_raw($data, $buyer_id){
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->buyer_id = $buyer_id;
		if($data['property_name'])
		$update->property_name = $data['property_name'];
		
		if($data['city'])
		$update->city = $data['city'];

		$buyerzip = explode(", ",$data['zip']);
		$buyerzip = implode(",", $buyerzip);
		
		if($buyerzip)
		$update->zip = $buyerzip;
		
		if($data['state'])
		$update->state = $data['state'];
		
		if($data['country'])
		$update->country = $data['country'];
		
		if($data['currency'])
		$update->currency = $data['currency'];
		
		if($data['ptype'])
		$update->property_type = $data['ptype'];
		
		if($data['stype'])
		$update->sub_type = $data['stype'];
		
		if($data['bedroom'])
		$update->bedroom = $data['bedroom'];
		
		if($data['bathroom'])
		$update->bathroom = $data['bathroom'];
		
		if($data['unitsqft'])
		$update->unit_sqft = $data['unitsqft'];
		
		if($data['view'])
		$update->view = $data['view'];
		
		if($data['style'])
		$update->style = $data['style'];
		
		if($data['yearbuilt'])
		$update->year_built = $data['yearbuilt'];
		
		if($data['poolspa'])
		$update->pool_spa = $data['poolspa'];
		
		if($data['condition'])
		$update->condition = $data['condition'];
		
		if($data['garage'])
		$update->garage = $data['garage'];
		
		if($data['units'])
		$update->units = $data['units'];
		
		if($data['cap'])
		$update->cap_rate = $data['cap'];
		
		if($data['grm'])
		$update->grm = $data['grm'];
		
		if($data['occupancy'])
		$update->occupancy = $data['occupancy'];
		
		if($data['type'])
		$update->type = $data['type'];
		
		if($data['class'])
		$update->listing_class = $data['class'];
		
		if($data['parking'])
		$update->parking_ratio = $data['parking'];
		
		if($data['ceiling'])
		$update->ceiling_height = $data['ceiling'];
		
		if($data['stories'])
		$update->stories = $data['stories'];
		
		if($data['roomcount'])
		$update->room_count = $data['roomcount'];
		
		if($data['typelease'])
		$update->type_lease = $data['typelease'];
		
		if($data['typelease2'])
		$update->type_lease2 = $data['typelease2'];
		
		if($data['available'])
		$update->available_sqft = $data['available'];
		
		if($data['lotsqft'])
		$update->lot_sqft = $data['lotsqft'];
		
		if($data['lotsize'])
		$update->lot_size = $data['lotsize'];
		
		if($data['bldgsqft'])
		$update->bldg_sqft = $data['bldgsqft'];
		
		if($data['term'])
		$update->term = $data['term'];
		
		if(isset($data['furnished']))
		$update->furnished = $data['furnished'];
		
		if(isset($data['pet']))
		$update->pet = $data['pet'];
		
		if($data['zoned'])
		$update->zoned = $data['zoned'];
		
		if($data['possession'])
		$update->possession = $data['possession'];
		
		if($data['bldgtype'])
		$update->bldg_type = $data['bldgtype'];
		
		if($data['features1'])
		$update->features1 = $data['features1'];
		
		if($data['features2'])
		$update->features2 = $data['features2'];
		
		if($data['features3'])
		$update->features3 = $data['features3'];
		
		if($data['settings'])
		$update->setting = $data['settings'];

		//print_r($update);
		//$ret = $db->insertObject('#__buyer_needs', $update);
		$ret = $db->updateObject('#__buyer_needs', $update, 'buyer_id', false);
		return $ret;
	}

	public function getListing($user_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('listing_id')
		->from('#__pocket_listing')
		->where('user_id = '.$user_id)
		->order('listing_id DESC LIMIT 1');
		$db->setQuery($query);
		$listing = $db->loadObjectList();
		return $listing;
	}
	public function insertPropertyPrice($listing_id, $data) {
		$db = JFactory::getDbo();
		$insert2 = new JObject();
		$insert2->price_type = $data['pricerange'];
		$insert2->price1 = $data['price1'];
		$insert2->price2 = $data['price2'];
		$insert2->disclose = $data['disclose'];
		$insert2->pocket_id = $listing_id;
		$ret2 = $db->insertObject('#__property_price', $insert2);
		return $ret2;
	}
	public function insertPocketAddress($listing_id, $addr) {
		$db = JFactory::getDbo();
		$insert3 = new JObject();
		$insert3->address = $addr;
		$insert3->pocket_id = $listing_id;
		$ret3 = $db->insertObject('#__pocket_address', $insert3);
		return $ret3;
	}
	public function insertPocketImages($listing_id, $v, $k) {
		$db = JFactory::getDbo();
		$insert4 = new JObject();
		if (strpos($v, 'uploads/') === FALSE) {
			$insert4->image = JUri::base(). "uploads/" . $v;
		} else {
			$insert4->image = $v;
		}
		$insert4->order_image = $k;
		$insert4->listing_id = $listing_id;
		$ret4 = $db->insertObject('#__pocket_images', $insert4);
		return $ret4;
	}
	public function insertPropertyActivity($listing_id, $activity) {
		$db = JFactory::getDbo();
		#print_r($activity); die();
		#print_r($activity); exit;
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)$activity['activity_type'];
		$activity_type = (int)$activity['activity_type'];
		$insert_activity->activity_id = (int)$listing_id;
		$insert_activity->user_id = (int)$activity['user_id'];
		$insert_activity->date = $activity['date'];
		#$insert_activity->other_user_id = (int)$activity['user_id'];
		$insert_activity->other_user_id = (isset($activity['other_user_id'])) ? (int)$activity['other_user_id'] : (int)$activity['user_id'];
		if($activity_type == 23 || $activity_type == 24 || $activity_type == 25 || $activity_type == 26){
			$insert_activity->buyer_id = (int)$activity['buyer_id'];
		}	
		$ret = $db->insertObject('#__activities', $insert_activity);
		return $ret;
	}

	public function insertPropertyActivity_Multi($activity_inserts) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$columns = array('activity_id','user_id','buyer_id','other_user_id','activity_type','date');
		$values = array();
		// Proper escaping/quotes should be done here, and probably in a loop, but cluttered the answer, so omitted it
		$query->insert($db->quoteName('#__activities'));
		$query->columns($columns);
		$query->values($activity_inserts);
		$db->setQuery($query);
		$ret =$db->execute();
		return $ret;
	}
	public function deleteExistingFeatures($data, $listing_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "
			UPDATE tbl_pocket_listing
			SET bedroom = 0, bathroom = 0, unit_sqft = NULL, view = NULL, style = NULL, year_built = NULL, pool_spa = NULL, garage = NULL, units = NULL, cap_rate = NULL, grm = NULL, type = NULL, occupancy = NULL, listing_class = NULL, parking_ratio = NULL, ceiling_height = NULL, stories = NULL, room_count = NULL, type_lease = NULL, type_lease2 = NULL, available_sqft = NULL, lot_size = NULL, lot_sqft = NULL, bldg_sqft = NULL, term = NULL, furnished = 0, pet = 0, possession = NULL, zoned = NULL, bldg_type = NULL, features1 = NULL, features2 = NULL, features3 = NULL
			WHERE listing_id = $listing_id
		";
		$db->setQuery($query);
		$db->execute();
	}
	/*public function updatePocket($listing_id, $user_id, $data) {
		$db = JFactory::getDbo();
		$edit = new JObject();
		$edit->listing_id = (int)$listing_id;
		$edit->user_id = $user_id;

		if($data['city'])
		$edit->city = $data['city'];

		if($data['property_name'])
		$edit->property_name = $db->escape($data['property_name']);

		if($data['zip'])
		$edit->zip = $data['zip'];

		if($data['state'])
		$edit->state = $data['state'];

		if($data['ptype'])
		$edit->property_type = $data['ptype'];	
			
		if($data['stype'])
		$edit->sub_type = $data['stype'];
				
		if($data['currency'])
		$edit->currency = $data['currency'];
		
		if($data['bedroom'])
		$edit->bedroom = $data['bedroom'];
		
		if($data['bathroom'])
		$edit->bathroom = $data['bathroom'];
		
		if($data['unitsqft'])
		$edit->unit_sqft = $data['unitsqft'];
		
		if($data['view'])
		$edit->view = $data['view'];
		
		if($data['style'])
		$edit->style = $data['style'];
		
		if($data['yearbuilt'])
		$edit->year_built = $data['yearbuilt'];
		
		if($data['poolspa'])
		$edit->pool_spa = $data['poolspa'];
		
		if($data['condition'])
		$edit->condition = $data['condition'];
		
		if($data['garage'])
		$edit->garage = $data['garage'];
		
		if($data['units'])
		$edit->units = $data['units'];
		
		if($data['cap'])
		$edit->cap_rate = $data['cap'];
		
		if($data['grm'])
		$edit->grm = $data['grm'];
		
		if($data['occupancy'])
		$edit->occupancy = $data['occupancy'];
		
		if($data['type'])
		$edit->type = $data['type'];
		
		if($data['class'])
		$edit->listing_class = $data['class'];
		
		if($data['parking'])
		$edit->parking_ratio = $data['parking'];
		
		if($data['ceiling'])
		$edit->ceiling_height = $data['ceiling'];
		
		if($data['stories'])
		$edit->stories = $data['stories'];
		
		if($data['roomcount'])
		$edit->room_count = $data['roomcount'];
		
		if($data['typelease'])
		$edit->type_lease = $data['typelease'];
		
		if($data['typelease2'])
		$edit->type_lease2 = $data['typelease2'];
		
		if($data['available'])
		$edit->available_sqft = $data['available'];
		
		if($data['lotsqft'])
		$edit->lot_sqft = $data['lotsqft'];
		
		if($data['lotsize'])
		$edit->lot_size = $data['lotsize'];
		
		if($data['bldgsqft'])
		$edit->bldg_sqft = $data['bldgsqft'];
		
		if($data['term'])
		$edit->term = $data['term'];
		
		if(isset($data['furnished']))
		$edit->furnished = $data['furnished'];
		
		if(isset($data['pet']))
		$edit->pet = $data['pet'];
		
		if($data['zoned'])
		$edit->zoned = $data['zoned'];
		
		if($data['possession'])
		$edit->possession = $data['possession'];
		
		if($data['bldgtype'])
		$edit->bldg_type = $data['bldgtype'];
		
		if($data['features1'])
		$edit->features1 = $data['features1'];
		
		if($data['features2'])
		$edit->features2 = $data['features2'];
		
		if($data['features3'])
		$edit->features3 = $data['features3'];
		
		if($data['setting'])
		$edit->setting = $data['setting'];
	
		if($data['desc'])
		$edit->desc = $data['desc'];


		$result = JFactory::getDbo()->updateObject('#__pocket_listing', $edit, 'listing_id');
		return $result;
	}*/
	
	public function updatePocket($listing_id, $user_id, $data) {
		$db = JFactory::getDbo();
		$edit = new JObject();
		$edit->listing_id = (int)$listing_id;
		$edit->user_id = $user_id;
		$edit->city = $data['city'];
		$edit->property_name = $db->escape($data['property_name']);
		$edit->zip = $data['zip'];
		$edit->state = $data['state'];
		$edit->property_type = $data['ptype'];
		$edit->sub_type = $data['stype'];
		$edit->currency = $data['currency'];
		$edit->bedroom = $data['bedroom'];
		$edit->bathroom = $data['bathroom'];
		$edit->unit_sqft = $data['unitsqft'];
		$edit->view = $data['view'];
		$edit->style = $data['style'];
		$edit->year_built = $data['yearbuilt'];
		$edit->pool_spa = $data['poolspa'];
		$edit->condition = $data['condition'];
		$edit->garage = $data['garage'];
		$edit->units = $data['units'];
		$edit->cap_rate = $data['cap'];
		$edit->grm = $data['grm'];
		$edit->occupancy = $data['occupancy'];
		$edit->type = $data['type'];
		$edit->listing_class = $data['class'];
		$edit->parking_ratio = $data['parking'];
		$edit->ceiling_height = $data['ceiling'];
		$edit->stories = $data['stories'];
		$edit->room_count = $data['roomcount'];
		$edit->type_lease = $data['typelease'];
		$edit->type_lease2 = $data['typelease2'];
		$edit->available_sqft = $data['available'];
		$edit->lot_sqft = $data['lotsqft'];
		$edit->lot_size = $data['lotsize'];
		$edit->bldg_sqft = $data['bldgsqft'];
		$edit->term = $data['term'];
		$edit->furnished = $data['furnished'];
		$edit->pet = $data['pet'];
		$edit->zoned = $data['zoned'];
		$edit->possession = $data['possession'];
		$edit->bldg_type = $data['bldgtype'];
		$edit->features1 = $data['features1'];
		$edit->features2 = $data['features2'];
		$edit->features3 = $data['features3'];
		$edit->setting = $data['setting'];
		$edit->description = $data['desc'];
		$edit->date_expired = date('Y-m-d', strtotime(date('Y-m-d'). ' + 90 days'));
		$result = JFactory::getDbo()->updateObject('#__pocket_listing', $edit, 'listing_id');
		return $result;
	}
	
	public function updatePropertyPrice($listing_id, $data) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "
			UPDATE tbl_property_price
			SET price1 = NULL, price2 = NULL
			WHERE pocket_id = $listing_id
		";
		$db->setQuery($query);
		$db->execute();
		$edit2 = new JObject();
		$edit2->price_type = $data['pricerange'];
		$edit2->price1 = $data['price1'];
		$edit2->price2 = $data['price2'];
		$edit2->disclose = $data['disclose'];
		$edit2->pocket_id = (int)$listing_id;
		$result2 = JFactory::getDbo()->updateObject('#__property_price', $edit2, 'pocket_id');
		return $result2;
	}
	public function deletePocketAddress($listing_id) {
		$db=JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->delete('#__pocket_address');
		$query->where('pocket_id = '.$listing_id);
		$db->setQuery($query);
		$db->query();
	}
	public function deletePocketImages($listing_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array('listing_id=' . (int)$listing_id);
		$query->delete($db->quoteName('#__pocket_images'));
		$query->where($conditions);
		$db->setQuery($query);
		$deleteresult = $db->query();
	}
	public function logpayment($refid, $paidby){
		$referral = $this->get_referral($refid);
		$other = ($referral->agent_a==$paidby) ? $referral->agent_b:$referral->agent_a;
		$this->insert_activity($paidby, 22, $refid, $other, date('Y-m-d H:i:s',time()));
	}
	public function getPermissionSetting($listing_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__permission_setting')->where("listing_id = ".$listing_id);
		return $db->setQuery($query)->loadObjectList();
	}
	public function insertNewPermissionSetting($listing_id, $data) {
		$db = JFactory::getDbo();
		$insert = new JObject();
		$insert->selected_permission = (int)$data['setting'];
			if( (int)$data['setting']==3 ) {
				$psetting1 = str_replace(",", "", $data['psetting1']);
				$insert->values =  (int)(str_replace("$", "", $psetting1));
			} else if ( (int)$data['setting']==4 ) {
				$psetting2 = str_replace(",", "", $data['psetting2']);
				$insert->values =  (int)(str_replace("$", "", $psetting2));
			} else if ( (int)$data['setting']==5 ) {
				$insert->values =  (int)($data['psetting3']);
			} else {
				$insert->values = "";
			}
			$insert->listing_id = $listing_id;
			$db->insertObject('#__permission_setting', $insert);
	}
	public function updatePermissionSetting($listing_id, $data) {
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->selected_permission = (int)$data['setting'];
			if( (int)$data['setting']==3 ) {
				$update->values =  (int)(str_replace(array("$",","), "", $data['psetting1']));
			} else if ( (int)$data['setting']==4 ) {
				$update->values =  (int)(str_replace(array("$",","), "", $data['psetting2']));
			} else if ( (int)$data['setting']==5 ) {
				$update->values =  (int)($data['psetting3']);
			} else {
				$update->values = "";
			}
			$update->listing_id = $listing_id;
			$db->updateObject('#__permission_setting', $update, 'listing_id');
	}
	public function insertIfnotExists($object, $primary, $table){
		//QUERY
		$result = 0;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("count(*) as count");
		$query->from($table);
		if(isset($object->$primary) || $object->$primary!=""){
			$query->where($primary.' = '.$object->$primary);
			$result = $db->setQuery($query)->loadObject();
		}
		if($result && $result->count){
			//EDIT
			JFactory::getDbo()->updateObject($table, $object, $primary);
			return $primary;
		}
		else{
			JFactory::getDbo()->insertObject($table, $object, $primary);
			return $db->insertid();
		}
	}
	//close referral 
	public function updatereferraltoclose( $referral_id ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "UPDATE tbl_referral SET status = 4 WHERE referral_id = $referral_id";
		$db->setQuery($query);
		$db->execute();
	}
	public function get_contact_my_client(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__contact_client');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_client_intention($id=0){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__client_intention');
		if($id)
			$query->where('id='.$id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_refferals( $ref_in ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__referral r');
		$query->where("referral_id = $ref_in");
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_referral_in($user, $filter = 0){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__referral r');
		$query->leftJoin('#__client_intention ci on ci.id = r.client_intention');
		$query->leftJoin('#__referral_status rs on rs.status_id = r.status');
		$query->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id');
		$query->leftJoin('#__country_currency coun on ba.currency = coun.currency');
		$query->order('referral_id DESC');
		$query->where('agent_b = '.$user.' AND ba.buyer_id != ""');
		if($filter)
			$query->where('status = '.$filter);
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_referral_out($user, $filter = 0 ){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__referral r');
		$query->leftJoin('#__client_intention ci on ci.id = r.client_intention');
		$query->leftJoin('#__referral_status rs on rs.status_id = r.status');
		$query->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id');
		$query->leftJoin('#__country_currency coun on ba.currency = coun.currency');
		$query->order('referral_id DESC');
		$query->where('agent_a = '.$user.' AND ba.buyer_id != ""');
		if($filter)
			$query->where('status = '.$filter);
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_referral_history($refid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('rsu.* , rv.referral_value, rv.countered, u.name');
		$query->from('#__referral_status_update rsu');
		$query->where('rsu.referral_id = '.$refid);
		$query->leftJoin('#__referral_value rv on rsu.value_id = rv.value_id');
		$query->leftJoin('#__users u on u.id = rsu.created_by');
		$query->order('rsu.created_date DESC');
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_referral_info($refid){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model2 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$object = $this->get_referral($refid);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		return $model2->activity_getReferral($object, $query, $db);
	}
	public function get_latest_referral($id){
		$db=JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__referral_value')->where('referral_id = '.$id)->order('value_id DESC limit 1');
		$db->setQuery($query);
		return  $db->loadObject();
	}
	public function counterReferral($id, $user, $value){
		$db=JFactory::getDbo();
		$referral = $this->get_referral($id);
		if(($referral->agent_a == $user || $referral->agent_b == $user) && $referral->counter!=$user){
			$old_referral_value = $this->get_latest_referral($id);
			$old_referral_value->countered = 1;
			JFactory::getDbo()->updateObject('#__referral_value', $old_referral_value, 'value_id');
			$this->insert_referral_value_object($id, $value);
			$other_user_id = $user;
			$user_id = ($referral->agent_a == $user) ? $referral->agent_b:$referral->agent_a;
			$this->insert_activity($user_id, 13, $db->insertid(), $other_user_id, date('Y-m-d H:i:s',time()));
			$this->update_referral_object($id, $other_user_id, 1);
		}
		else
			return 'You cannot counter this referral because it doesn\'t belong to you or you have just countered it.';
	}
	public function declineReferral($id, $user, $reason = 1){
		$db=JFactory::getDbo();
		$referral = $this->get_referral($id);
		if($referral->agent_a == $user || $referral->agent_b == $user){
			$this->update_referral_object($id, $user, 0, 5);
			echo 'Fully declined the referral';
			$other_user_id = $user;
			$user_id = ($referral->agent_a == $user) ? $referral->agent_b:$referral->agent_a;
			$this->insert_activity($user_id, 12, $id, $other_user_id, date('Y-m-d H:i:s',time()));
		}
		else
			return 'You cannot decline this referral because it doesn\'t belong to you';
	}
	function printmessage($mailsubject, $body, $idie = true){
		echo "<div style='width:400px'>";
		echo $mailsubject."<br/>";
		echo $body;
		echo "</div>";
		if($idie)
			die();
	}
	public function send_statuschange_mail($other_user, $user, $newstatus, $refid){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('Activitylog', 'ActivitylogModel');
		$referral = $this->get_referral($refid);
		$mail_to = $model2->get_mail_options($user);
		$other_options = $model2->get_mail_options($other_user);
		$user_assist_details = $model2->getUserAssistant($other_user);
		$other_user_info = $model2->get_user_registration($other_user);	
		$user_info = $model2->get_user_registration($user);
		//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    //language loading
		if($other_user_info->gender == "Female"){
			$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
		} else {
			$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
		}
		if($user_info->gender == "Female"){
			$salut_u = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($user_info->lastname);
		} else {
			$salut_u = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($user_info->lastname);
		}
		if($user_assist_details->cc_all && $user_assist_details->cc_all==1){
			$cc_email=$user_assist_details->a_email;
		}

		$notif_client_status = "";
		if($newstatus == 6){
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $other_user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    	//language loading
			if(!count($other_options) || strpos($other_options->contact_options, "6")) {
			$mailer = JFactory::getMailer();
			$client = $model2->get_client($referral->client_id)->name;
			$r2 = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".stripslashes_all(JFactory::getUser($user)->name)."</a>";
			$email = "<a href='mailto:".JFactory::getUser($user)->email."'>".JFactory::getUser($user)->email."</a>" ;
			$mailsubject = JText::sprintf('COM_REFERRALS_HELP', $client);
			//$body = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".JFactory::getUser($user)->name."</a> needs your help with <a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_activitylog'),true,-1)."'>referral</a> (".$model2->get_client($referral->client_id)->name.").";
			$numbers = $model2->get_mobile_numbers(JFactory::getUser($user)->id);
			$phone="";
				if(count($numbers)){
					$phone = "phone ".implode(", ",array_map(function($no, $countryCode){ return $countryCode." ".$no->value; }, $numbers, array($model2->get_client($referral->client_id)->address[0]->countryCode)))." or ";
					//$phone = "phone ".implode(", ",array_map(function($no){ return $no->value; }, $numbers))." or "; 
				}

			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$body = "<p style='text-align: justify'>".$salut.",</p><br/>";
			$body .= JText::sprintf('COM_REFERRALS_HELP_P1', $client, $r2, $client, $r2, $phone, $email, $client );
			//$this->printmessage($mailsubject, $body);
			$this->clearmail();
			//R2 First Name R2 Last Name, hyperlink to R2 profile] is actively working your referral [Client First Name Client Last Name].
			$mailer->IsHTML(true);
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($other_user)->email, $mailsubject, $body, true, $cc_email);
			$this->clearmail();
			$notif_client_status="Need Help";
			}
		}
		else if($newstatus == 1){
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $other_user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    	//language loading
			if(!count($other_options) || strpos($other_options->contact_options, "6"))  {
			$client = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>".stripslashes_all($model2->get_client($referral->client_id)->name)."</a>";
			$r2 = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".stripslashes_all(JFactory::getUser($user)->name)."</a>";
			$agentbridge = "<a href='".JRoute::_('index.php?option=com_userprofile&task=edit', true, -1)."'>AgentBridge</a> ";
			$mailer = JFactory::getMailer();
			//$mailsubject = JFactory::getUser($user)->name.' is under contract';
			//$body = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".JFactory::getUser($user)->name."</a> is under contract with <a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_activitylog'),true,-1)."'>referral</a> (".$model2->get_client($referral->client_id)->name.").";
			
			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$mailsubject = JText::_('COM_REFERRALS_UNDERCONTRACT');
			$body = "<p style='text-align: justify; padding-top:20px'>".$salut.",<br/><br/>";
			$body .= JText::sprintf('COM_REFERRALS_UNDERCONTRACT_P1', $client, $r2, $agentbridge);
			//$this->printmessage($mailsubject, $body);
			$this->clearmail();
			//R2 First Name R2 Last Name, hyperlink to R2 profile] is actively working your referral [Client First Name Client Last Name].
			$mailer->IsHTML(true);
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($other_user)->email, $mailsubject, $body, true, $cc_email);
			$this->clearmail();
			$notif_client_status="Under Contract";
			}
		}
		else if($newstatus == 5){
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $other_user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    	//language loading
			if(!count($other_options) || strpos($other_options->contact_options, "6"))  {
			$client = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals')."'>".$model2->get_client($referral->client_id)->name."</a>";
			$mailer = JFactory::getMailer();
			$mailsubject = JText::sprintf('COM_REFERRALS_DECLINE', $client);
			//$body = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".JFactory::getUser($user)->name."</a> has updated your referral ".$model2->get_client($referral->client_id)->name." to NoGo.";
			$numbers = $model2->get_mobile_numbers(JFactory::getUser($user)->id);
			$r2 = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".stripslashes_all(JFactory::getUser($user)->name)."</a>";
			$email = "<a href='mailto:".JFactory::getUser($user)->email."'>".JFactory::getUser($user)->email."</a>";
			$phone="";
			if(count($numbers)){
				$phone = implode(", ",array_map(function($no, $countryCode){ return $countryCode." ".$no->value; }, $numbers,array($model2->get_client($referral->client_id)->address[0]->countryCode)))." or ";
			} 
			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$body = "<p style='text-align: justify'>".$salut.",</p><br/>";
			$body .= JText::sprintf('COM_REFERRALS_DECLINE_P1', $client, $r2, $r2, $phone, $email);
			$this->clearmail();
			//R2 First Name R2 Last Name, hyperlink to R2 profile] is actively working your referral [Client First Name Client Last Name].
			$mailer->IsHTML(true);
			 $mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($other_user)->email, $mailsubject, $body, true, $cc_email);
			$this->clearmail();
			$notif_client_status="Declined";
			}
		}
		else if($newstatus == 4){
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $other_user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    	//language loading
			if(!count($other_options) || strpos($other_options->contact_options, "6"))  {
			$client = stripslashes_all($model2->get_client($referral->client_id)->name);
			$r2 = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".stripslashes_all(JFactory::getUser($user)->name)."</a>";
			$mailer = JFactory::getMailer();
			//$mailsubject = JFactory::getUser($user)->name.' closed a referral referral';
			//$body = "<a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_userprofile&task=profile&uid='.JFactory::getUser($user)->id),true,-1)."'>".JFactory::getUser($user)->name."</a> has closed <a href='".JRoute::_('index.php?option=com_nrds&returnurl='.base64_encode('index.php?option=com_activitylog'),true,-1)."'>referral</a> ".$model2->get_client($referral->client_id)->name.". Final numbers are being verified.";

			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$mailsubject = JText::_('COM_REFERRALS_CLOSED');
			$body = "<p style='text-align: justify; padding-top:20px'>".$salut.",<br/><br/>";
			$body .= JText::sprintf('COM_REFERRALS_CLOSED_P1', $client, $r2, $r2);
			//$this->printmessage($mailsubject, $body);
			$this->clearmail();
			//R2 First Name R2 Last Name, hyperlink to R2 profile] is actively working your referral [Client First Name Client Last Name].
			$mailer->IsHTML(true);
			 $mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($other_user)->email, $mailsubject, $body, true, $cc_email);
			$this->clearmail();
			$notif_client_status="Closed";
			}
		}
		else if($newstatus == 8){
			if(!count($other_options) || strpos($other_options->contact_options, "6")) {
			$mailer = JFactory::getMailer();
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $other_user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    	//language loading
			$client = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>".stripslashes_all($model2->get_client($referral->client_id)->name)."</a>";
			$refpage = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>Referral page.</a>";
			$notifpage = "<a href='".JRoute::_('index.php?option=com_userprofile&task=emailsnotif', true, -1)."'>Notification Settings</a>";
			$tempuser = $other_user;
			$other_user = $user;
			$user = $tempuser;
			$other_user_info = $model2->get_user_registration($tempuser);	
			$mailsubject = JText::_('COM_REFERRALS_ACTIVE');
			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$other_name_linked = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$other_user."'>".stripslashes_all(JFactory::getUser($other_user)->name)."</a>";
			$body = "<p  style='text-align: justify; padding-top:20px'>".$salut.",<br/><br/>";
			$body .= JText::sprintf('COM_REFERRALS_ACTIVE_P1', $client, $other_name_linked, $other_name_linked,$other_name_linked, $refpage, $other_name_linked, $client, $other_name_linked, $notifpage);
			//$this->printmessage($mailsubject, $body);
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($user)->email, $mailsubject, $body, true, $cc_email);
			$notif_client_status="Active";
			}
		}
		if($newstatus == 9 AND ( !count($mail_to) || strpos($mail_to->contact_options, "6"))){
			$mailer = JFactory::getMailer();
			//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
			//language loading
			$closedref = $model3->get_closed_referral_by_refid($referral->referral_id);
			$client = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>".$model2->get_client($referral->client_id)->name."</a>";
			$fee = explode('%',$closedref['referral']->referral_fee);
			$broker = "<a href='#'>".$model2->get_broker($model2->get_user($user)->brokerage)."</a>";
			$other_user = $referral->agent_a;
			$r2 = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$user."'>".stripslashes_all(JFactory::getUser($user)->name)."</a>";
			$user = $referral->agent_b;
			$computed = $closedref['clrfobject']->price_paid * (".".$fee[0]);
			$formatcomputed = format_currency_global($computed);	
			if($other_user_info->gender == "Female"){
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$mailsubject = JText::sprintf('COM_REFERRALS_COMPLETE', $client);
			$body = "<p  style='text-align: justify'>".$salut.",</p>";
			$body .= JText::sprintf('COM_REFERRALS_COMPLETE_P1', $formatcomputed, $client, $broker, $r2);
			$this->clearmail();
			$mailer->IsHTML(true);
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($other_user)->email, $mailsubject, $body, true, $cc_email);
			$this->clearmail();
			$notif_client_status="Complete";
		}

		$client_name = $model2->get_client($referral->client_id)->name;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		$r1_user_android_token = $this->getAppUserToken($other_user,'android');
		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$this->android_send_notification($value,"Referral Update", $r1_name." has changed the status of ".$client_name." to ".$notif_client_status, "Referrals");
			}

	}
	function send_accepted_referral($referrer, $acceptedby, $client, $refid=0){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$user_assist_details = $model2->getUserAssistant($acceptedby);
		if($user_assist_details->cc_all && $user_assist_details->cc_all==1){
			$cc_email=$user_assist_details->a_email;
		}
		$user_info = $model2->get_user_registration($acceptedby);
		//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    //language loading
		if($user_info->gender == "Female"){
			$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($user_info->lastname);
		} else {
			$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($user_info->lastname);
		}
		$referral = $this->get_referral($refid);
		$client = $model2->get_client($client);
		$mailer = JFactory::getMailer();
		$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$referrer."'>".stripslashes_all(JFactory::getUser($referrer)->name)."</a>";
		$mail_to = $model2->get_mail_options($acceptedby);
		$formatclient = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=agreement', true, -1)."&ref_id=".$refid."'>".$this->format_name($client->name)."</a>";
		$account = "<a href='".JRoute::_('index.php?option=com_propertylisting&task=agreement', true, -1)."&ref_id=".$refid."'>".JText::_('COM_REFERRALS_ACCOUNT')."</a>";
		$other_options = $model2->get_mail_options($referrer);
		if(!count($mail_to) || strpos($mail_to->contact_options, "6")) {
			$mailsubject = JText::_('COM_REFERRALS_SIGN_R1');
			$body = "<p style='text-align:justify; padding-top:20px'>".$salut.",<br/><br/>"; 
			$body .= JText::sprintf('COM_REFERRALS_SIGN_R2_P1', $other_linked_name, $formatclient, $referral->referral_fee, $account);
		//$this->printmessage($mailsubject, $body);
			$this->clearmail();
			$mailer->IsHTML(true);
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($acceptedby)->email, $mailsubject, $body, true, $cc_email);
		}


		$client_name = $model2->get_client($referral->client_id)->name;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		$r1_user_android_token = $this->getAppUserToken($referrer,'android');
		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$this->android_send_notification($value,"Referral Update", $r1_name." has accepted your referral", "Referrals");
			}


	}
	function clearmail(){
		$mailer2 = JFactory::getMailer();
		$mailer2->ClearAddresses();
		$mailer2->ClearAllRecipients();
		$mailer2->ClearAttachments();
	}
	function insert_authorize_transaction($data){
		$db = JFactory::getDbo();
		$object = new JObject();
		foreach ($data as $key => $value){
			if($key=="card_number" || $key=="security_no"|| $key=="crlfid")
				continue;
			$object->$key = $value;
		}
		$db->insertObject('#__authorize_transactions', $object);
	}
	function addBuyer($buyer){
		$db = JFactory::getDbo();
		$db->insertObject('#__buyer', $buyer);
		$lastinsert = $db->insertid();
	}
	function insertReferral($data, $otheruser = null){
		$db = JFactory::getDbo();
			$buyerobject = new JObject();
			$buyerobject->name = $db->escape($data['name']);
			$buyerobject->contact_method = $data['contact_method'];
			$buyerobject->contact_type = $data['contact_type'];
			$buyerobject->price_type = $data['price_type'];
			$buyerobject->price_value = $data['price'];
			$buyerobject->agent_id = JFactory::getUser()->id;
			$db->insertObject('#__buyer', $buyerobject);
			$lastinsert = $db->insertid();
			/*if(!isset($data['buyerid']) || $data['buyerid']==""){
				$db->insertObject('#__buyer', $buyerobject);
				$lastinsert = $db->insertid();
			}
			else{
				$buyerobject->buyer_id = $data['buyerid'];
				$db->updateObject('#__buyer', $buyerobject, 'buyer_id');
				$lastinsert = $data['buyerid'];
			}*/
			$mobile_object = array();
			$i=0;
			/*$db->setQuery('delete from #__buyer_mobile where buyer_id = '.$lastinsert);
			$db->execute(); */
			foreach($data['mobile'] as $mobile){
				$mobile_object[$i] = new JObject();
				$mobile_object[$i]->buyer_id = $lastinsert;
				$mobile_object[$i]->number = $mobile;
				$db->insertObject('#__buyer_mobile', $mobile_object[$i]);
				$i++;
			}
			$email_object = array();
			$i=0;
			/*$db->setQuery('delete from #__buyer_email where buyer_id = '.$lastinsert);
			$db->execute();*/
			foreach($data['email'] as $email){
				$email_object[$i] = new JObject();
				$email_object[$i]->buyer_id = $lastinsert;
				$email_object[$i]->email = $email;
				$db->insertObject('#__buyer_email', $email_object[$i]);
				$i++;
			}
			/*$db->setQuery('delete from #__buyer_address where buyer_id = '.$lastinsert);
			$db->execute();*/
			$buyer_address_object = new JObject();
			$buyer_address_object->buyer_id = $lastinsert;
			$buyer_address_object->country = $data['country'];
			$buyer_address_object->currency = $data['currency'];
			$buyer_address_object->address_1 = $data['address1'];
			$buyer_address_object->address_2 =$data['address2'];
			$buyer_address_object->city = $data['city'];
			$buyer_address_object->state = $data['state'];
			$buyer_address_object->zip = $data['zip'];
			$db->insertObject('#__buyer_address', $buyer_address_object);
		//$agentb = $this->get_listing(array('p.listing_id'=>$data['propertyid']));
		$other_user = ($otheruser!=null) ? $otheruser : $agentb[0]->user_id;
		echo $data['agentb'];
		$referral_object = new JObject();
		$referral_object->agent_a = JFactory::getUser()->id;
		$referral_object->agent_b = $data['agentb'];
		$referral_object->client_id = $lastinsert;
		$sprice1 = str_replace(",","",$data['price1']);
		$referral_object->price_1 = ($data['price1']) ? str_replace("$","",$sprice1): "";
		$referral_object->price_2 = ($data['price2']) ? str_replace(",","",$data['price2']): str_replace("$","",$sprice1);
		$referral_object->referral_fee = $data['referral_fee'];
		$referral_object->status = 7;
		$referral_object->counter = JFactory::getUser()->id;
		$referral_object->client_intention = $data['intention'];
		$referral_object->property_id = $data['propertyid'];
		$db->insertObject('#__referral', $referral_object);
		$referral_id = $db->insertid();
		$referral_value = new JObject();
		$referral_value->referral_id = $referral_id;
		$referral_value->referral_value = $data['referral_fee'];
		$db->insertObject('#__referral_value', $referral_value);
		$referral_value_id = $db->insertid();
		$referral_status_id = $this->insert_referral_status_object_generic(array('value_id'=>$referral_value_id,'referral_id'=>$referral_id,'status'=>7,'created_date'=>date('Y-m-d H:i:s',time()),'note'=>$data['note']));
		$this->sendreferral_mail($data['agentb'], JFactory::getUser()->id, $lastinsert, $referral_id);
		$this->insert_activity($data['agentb'], 11, $referral_status_id, JFactory::getUser()->id, date('Y-m-d H:i:s',time()));
		$this->insert_activity(JFactory::getUser()->id, 15, $referral_status_id, $data['agentb'], date('Y-m-d H:i:s',time()));

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		$r1_user_android_token = $this->getAppUserToken($data['agentb'],'android');
		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$this->android_send_notification($value,"New Referral", $r1_name." has sent you a referral", "Referrals");
			}

		return $agentb[0]->user_id;
	}


	public function android_send_notification($token,$title,$message,$topic){

		
		if(!is_array($token)){
			$token = array($token);
		} else{
			$token = array_unique($token);
		}

		sort($token);
	

		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			// "to" => "/topics/".$topic,
			 'registration_ids' => $token,
			 'data' =>  array("title" => $title,"message" => $message)
			);

		$headers = array(
			'Authorization:key = AIzaSyB-6XSF_wghYrGTL5NlKk3MPBwHgrmpujI ',
			'Content-Type: application/json'
			);

	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);

        $mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "array user token count" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( print_r($result,true));
		$mailSender ->Send(); 
       

	}

	function get_valueId($refid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value_id')->from('#__referral_value')->where('referral_id = '.$refid)->order('value_id DESC');
		return $db->setQuery($query)->loadObject()->value_id;
	}
	function update_ref_status($value_id, $refid, $newstatus, $reason = null, $note = false){
		$db = JFactory::getDbo();
		if(!$user)
			$user = JFactory::getUser()->id;
		$referral = new JObject();
		if(!$value_id || $value_id==NULL){
			$value_id = $this->get_valueId($refid);
		}
		$this->update_referral_object_generic(
				array(	'referral_id' => $refid,
						'status' => $newstatus,
						'reason' => $reason,
						'counter' => $user,
						'countered' => 0)
		);
		$note = ($note!="") ? $note: $reason;
		$id = $this->insert_referral_status_object_generic(
				array(	'referral_id' => $refid,
						'status' => $newstatus,
						'value_id' => $value_id,
						'note' => $note,
						'created_date' => date('Y-m-d H:i:s',time())
				)
		);
		$listing = $this->get_referral($refid);
		$other_user = ($user == $listing->agent_a) ? $listing->agent_b : $listing->agent_a;
		$this->insert_activity($other_user, 17, $id, $user, date('Y-m-d H:i:s',time()));
		$this->insert_activity($user, 17, $id, $other_user, date('Y-m-d H:i:s',time()));

		$this->send_statuschange_mail($other_user, $user, $newstatus, $refid);
		return 'successfully updated status';
	}
	function sendHelloSignMail($agent_a, $agent_b, $referral_fee){
		$username = 'adinbautista@yahoo.com';
		$password = 'password1';
		#echo "<br/>https://$username:$password@api.hellosign.com/v3/account";
		$curl = curl_init();
		$url = 'https://api.hellosign.com/v3/signature_request/send_with_reusable_form';
		curl_setopt($curl, CURLOPT_URL, $url);
		$params = array(
				'title'                          => 'Referral Contract',
				'subject'                        => 'Please Sign this Referral Contract',
				'signers[Agent1][email_address]' => JFactory::getUser($agent_a)->email,
				'signers[Agent1][name]'          => stripslashes_all(JFactory::getUser($agent_a)->name),
				'signers[Agent2][email_address]' => JFactory::getUser($agent_b)->email,
				'signers[Agent2][name]'          => stripslashes_all(JFactory::getUser($agent_b)->name),
				'custom_fields[agent1_name]'     => stripslashes_all(JFactory::getUser($agent_a)->name),
				'custom_fields[agent2_name]'     => stripslashes_all(JFactory::getUser($agent_b)->name),
				'custom_fields[referral_fee]'    => "$referral_fee",
				'reusable_form_id'               => '5cfbff1ba1088196251f509d826b405a1da13c88',
		);
		$paramsformat = http_build_query($params);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $paramsformat);
		$ret = curl_exec($curl);
		curl_close($curl);
		#echo "<br/><pre>";
		#print_r(json_decode($ret));
		#echo "</pre><br/>a";
		return json_encode($ret);
	}
	function sendDocuSignMail($usera, $userb, $referral_fee){
	}
	function sendreferral_mail($mailto, $other_id, $client=null, $refid = null){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');		
		$user_assist_details = $model2->getUserAssistant($mailto);
		if($user_assist_details->cc_all && $user_assist_details->cc_all==1){
			$cc_email=$user_assist_details->a_email;
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($mailto)->email.'\'');
		$db->setQuery($query);
		$recipient = $db->loadObject();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($other_id)->email.'\'');
		$db->setQuery($query);
		$other = $db->loadObject();
		if($client){
			$client = $model2->get_client($client);
		}
		if($refid){
			$referral = $this->get_referral($refid);
		}
		$mail_to = $model2->get_mail_options($mailto);
		$other_options = $model2->get_mail_options($other_id);
		if(!count($mail_to) || strpos($mail_to->contact_options, "6")){
			//$oldlist = $listing;
			//$listing = $this->get_listing(array('p.listing_id' => $listing));
			//$listing = $listing[0];
			$mailer = JFactory::getMailer();
			$mailer2 = JFactory::getMailer();
			$intention = $this->get_client_intention($referral->client_intention);
			//$listingname=(!empty($listing->property_name)) ? $listing->property_name : 'unnamed';
			//$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
			//JModelLegacy::addIncludePath( $userprofilemodel );
			//$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
			$ex_rates = $this->getExchangeRates_indi();
			$mailto_details = $model2->get_user_registration($mailto);
			/*
				$query->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id');
		$query->leftJoin('#__country_currency coun on ba.currency = coun.currency');
			
			*/
			if($referral->price_1){
				if(!$referral->currency){
					$referral->currency = "USD";
				}
				$ex_rates_con = $ex_rates->rates->{$referral->currency};
				$ex_rates_can = $ex_rates->rates->{$mailto_details->currency};
				//$ref->price_1=$model->getExchangeRates($ref->price_1,$userDetails->currency,$ref->currency);
				if($referral->currency=="USD"){									
					$referral->price_1=$referral->price_1 * $ex_rates_can;
				} else {
					$referral->price_1=($referral->price_1 / $ex_rates_con) * $ex_rates_can;
				}
				if($referral->price_2){
					//$ref->price_2=$model->getExchangeRates($ref->price_2,$userDetails->currency,$ref->currency);
					if($ref->currency=="USD"){								
						$referral->price_2=$referral->price_2 * $ex_rates_can;
					} else {
						$referral->price_2=($referral->price_2 / $ex_rates_con) * $ex_rates_can;
					}
				} 
				$referral->correctcurr = $mailto_details->currency;
				$referral->symbol = $mailto_details->symbol;

				$price = format_currency_global($referral->price_1,$referral->symbol,$referral->correctcurr).(($referral->price_1==$referral->price_2)?"": "-".format_currency_global($referral->price_2,$referral->symbol,$referral->correctcurr));
			}
			else{
				$price = '(unknown)';
			}
			$user_info = $model2->get_user_registration($mailto);
			//language loading
			  	$language = JFactory::getLanguage();
		      	$extension = 'com_nrds';
		      	$base_dir = JPATH_SITE;
		      	$language_tag = $user_info->currLanguage;
		      	$language->load($extension, $base_dir, $language_tag, true);
		    //language loading
			if($user_info->gender == "Female"){
				$salut_u = JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($user_info->lastname);
			} else {
				$salut_u = JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($user_info->lastname);
			}
			$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$other_id."'>".stripslashes_all(JFactory::getUser($other_id)->name)."</a>";
			$reflog = "<a href='".JRoute::_('index.php?option=com_activitylog', true, -1)."'>".JText::_('COM_REFERRALS_HERE')."</a>";
			$mailsubject = JText::_('COM_REFERRALS_NEW_R2');
			$r1broker = $model2->get_broker_info(JFactory::getUser()->email)->broker_name;
			$intention = JText::_($intention[0]->intention) ;
			$fee = $referral->referral_fee;
			$body = "<p style='text-align: justify; padding-top:20px'>".JText::_('COM_USERACTIV_HEADER')." ".$salut_u.",<br/><br/>";
			$body .= JText::sprintf('COM_REFERRALS_NEW_R2_P1', $other_linked_name, $r1broker,$this->format_name($client->name));
			$body .= JText::sprintf('COM_REFERRALS_NEW_R2_P2', $intention, $price,$other_linked_name,$fee);
			$body .= JText::sprintf('COM_REFERRALS_NEW_R2_P3', $reflog)."</p>";
			$mailer->sendMail('socon.agentbridge@gmail.com', 'AgentBridge', JFactory::getUser($mailto)->email, $mailsubject, $body, true, $cc_email);
		}
	}
	function format_name($name){
		$first_2 = substr($name, 0, 2);
		$middle = str_repeat('X', (strlen($name)-4));
		$last_2 =  substr($name, -2, 2);
		return $first_2.$middle.$last_2;
	}
	function retract_referral($referral, $userid, $reason = 5){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$referral_status_object = $model->get_referral_status_object($referral);
		$referral_object = $this->get_referral($referral_status_object->referral_id);
		unset($referral_status_object->update_id);
		$referral_status_object->note = $reason;
		$referral_status_object->status = 5;
		$referral_status_object->created_date = date('Y-m-d H:i:s',time());
		$id = $this->insert_referral_status_object_generic($referral_status_object);
		$referral_status_object_original = $model->get_referral_status_object($referral);
		$referral_status_object_original->response = $id;
		$referral_status_object_original->edited_by = $userid;
		JFactory::getDbo()->updateObject('#__referral_status_update', $referral_status_object_original, 'update_id');
		$referral_object = new stdClass();
		$referral_object->referral_id = $referral_status_object_original->referral_id;
		$referral_object->status = 5;
		JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
		if($userid == JFactory::getUser()->id)
			return 'You have successfully retracted your referral';
		else
			return 'You have successfully declined the referral';
	}
	public function counter_referral($id, $user, $value){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$db=JFactory::getDbo();
		$referral_status_object = $model->get_referral_status_object($id);
		$referral = $this->get_referral($referral_status_object->referral_id);
		if(($referral->agent_a == $user || $referral->agent_b == $user)){
			$ref_value_id = $this->insert_referral_value_object($referral_status_object->referral_id, $value);
			$referral_status_object = $model->get_referral_status_object($id);
			$referral_object = $this->get_referral($referral_status_object->referral_id);
			unset($referral_status_object->update_id);
			unset($referral_status_object->note);
			$referral_status_object->status = 7;
			$referral_status_object->value_id = $ref_value_id;
			$referral_status_object->created_date = date('Y-m-d H:i:s',time());
			$status_object_id = $this->insert_referral_status_object_generic($referral_status_object);
			$referral_status_object_original = $model->get_referral_status_object($id);
			$referral_status_object_original->response = $status_object_id;
			$referral_status_object_original->edited_by = $user;
			JFactory::getDbo()->updateObject('#__referral_status_update', $referral_status_object_original, 'update_id');
			$referral_object = new stdClass();
			$referral_object->referral_id = $referral_status_object_original->referral_id;
			$referral_object->referral_fee = $value;
			JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
			$other_user_id = $user;
			$user_id = ($referral->agent_a == $user) ? $referral->agent_b:$referral->agent_a;
			$this->insert_activity($user_id, 13, $status_object_id, $other_user_id, date('Y-m-d H:i:s',time()));
			return 'You have successfully countered the request';
		}
		else
			return 'You cannot counter this referral because it doesn\'t belong to you or you have just countered it.';
	}
	public function accept_referral($id, $user, $filename){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$db=JFactory::getDbo();
		$referral_status_object = $model->get_referral_status_object($id);
		$referral = $this->get_referral($referral_status_object->referral_id);
		if(($referral->agent_a == $user || $referral->agent_b == $user)){
			$referral_status_object = $model->get_referral_status_object($id);
			$referral_object = $this->get_referral($referral_status_object->referral_id);
			unset($referral_object->address_id);
			unset($referral_object->buyer_id);
			unset($referral_object->pkID);
			unset($referral_object->symbol);
			unset($referral_object->zip);
			unset($referral_object->country);
			unset($referral_object->currency);
			unset($referral_object->address_1);
			unset($referral_object->address_2);
			unset($referral_object->city);
			unset($referral_object->state);
			unset($referral_status_object->update_id);
			unset($referral_status_object->note);
			$referral_status_object->created_date = date('Y-m-d H:i:s',time());
			$status_object_id = $this->insert_referral_status_object_generic($referral_status_object);
			$referral_status_object_original = $model->get_referral_status_object($id);
			$referral_status_object_original->response = $status_object_id;
			$referral_status_object_original->edited_by = $user;
			JFactory::getDbo()->updateObject('#__referral_status_update', $referral_status_object_original, 'update_id');
			/*$referral_object = new stdClass();
			$referral_object->referral_id = $referral_status_object_original->referral_id;
			$referral_object->status = 8;
			JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');*/
			$other_user_id = $user;
			$user_id = ($referral->agent_a == $user) ? $referral->agent_b:$referral->agent_a;
			$this->insert_activity($user_id, 14, $status_object_id, $other_user_id, date('Y-m-d H:i:s',time()));
			$this->insert_activity($other_user_id, 14, $status_object_id, $user_id, date('Y-m-d H:i:s',time()));
			$referrals = "";
			$referral_object = $this->get_referral($referral_object->referral_id);
			unset($referral_object->address_id);
			unset($referral_object->buyer_id);
			unset($referral_object->pkID);
			unset($referral_object->symbol);
			unset($referral_object->zip);
			unset($referral_object->country);
			unset($referral_object->currency);
			unset($referral_object->address_1);
			unset($referral_object->address_2);
			unset($referral_object->city);
			unset($referral_object->state);

			$signer = new HelloSign();
			$referrals.="<br/>".$signer->login();
			$referrals.="<br/>".$signer->createEnvelope(JFactory::getUser($referral->agent_a),JFactory::getUser($referral->agent_b), $filename);
			//$referrals.="<br/>".$signer->modifyRFee($referral_object->referral_fee);
			//$referrals.="<br/>".$signer->sendEmail();
			//echo $referrals;
			$referral_status_object = $model->get_referral_status_object($id);
			$referral_object = $this->get_referral($referral_status_object->referral_id);
			unset($referral_object->address_id);
			unset($referral_object->buyer_id);
			unset($referral_object->pkID);
			unset($referral_object->symbol);
			unset($referral_object->zip);
			unset($referral_object->country);
			unset($referral_object->currency);
			unset($referral_object->address_1);
			unset($referral_object->address_2);
			unset($referral_object->city);
			unset($referral_object->state);

			$referral_object->docusign_envelope = $signer->getEnvelope();
			unset($referral_object->signatures);
			unset($referral_object->intention);
			unset($referral_object->id);

			$result = JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
			$this->send_accepted_referral($user_id, $other_user_id, $referral_object->client_id, $referral_status_object->referral_id);
			//return 'this is to confirm your referral.';
			return 1;
		}
		else
			return 'You cannot accept this referral because it doesn\'t belong';
	}
	public function decline_referral($id, $user){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$db=JFactory::getDbo();
		$referral_status_object = $model->get_referral_status_object($id);
		$referral = $this->get_referral($referral_status_object->referral_id);
		if(($referral->agent_a == $user || $referral->agent_b == $user)){
			if($referral->agent_a == $user)
				$action = "retracted";
			else
				$action = "declined";
			$referral_status_object = $model->get_referral_status_object($id);
			$referral_object = $this->get_referral($referral_status_object->referral_id);
			unset($referral_status_object->update_id);
			unset($referral_status_object->note);
			$referral_status_object->created_date = date('Y-m-d H:i:s',time());
			$referral_status_object->status=5;
			$status_object_id = $this->insert_referral_status_object_generic($referral_status_object);
			$referral_status_object_original = $model->get_referral_status_object($id);
			$referral_status_object_original->response = $status_object_id;
			$referral_status_object_original->edited_by = $user;
			JFactory::getDbo()->updateObject('#__referral_status_update', $referral_status_object_original, 'update_id');
			$referral_object = new stdClass();
			$referral_object->referral_id = $referral_status_object_original->referral_id;
			$referral_object->status = 5;
			JFactory::getDbo()->updateObject('#__referral', $referral_object, 'referral_id');
			$other_user_id = $user;
			$user_id = ($referral->agent_a == $user) ? $referral->agent_b:$referral->agent_a;
			$this->insert_activity($user_id, 12, $status_object_id, $other_user_id, date('Y-m-d H:i:s',time()));
			$this->insert_activity($other_user_id, 12, $status_object_id, $user_id, date('Y-m-d H:i:s',time()));
			return 'You have successfully '.$action." the referral";
		}
		else
			return 'You cannot accept this referral because it doesn\'t belong';
	}
	public function get_referral_status_list(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__referral_status')->order('sorted ASC');
		return $db->setQuery($query)->loadObjectList();
	}
	public function close_listing($refid, $amount, $files){
		$object = new JObject();
		$object->referral_id = $refid;
		$object->price_paid = $amount;
		$object->r1_paid = 0;
		$object->r2_paid = 1;
		$referral = $this->get_referral($refid);
		$date =  date('Y-m-d H:i:s',time());
		$temp = $this->insertIfnotExists($object, 'clrf_id', '#__closed_referrals');
		$this->insert_activity($referral->agent_b, 19, $temp, $referral->agent_a, $date);
		$this->insert_activity($referral->agent_a, 19, $temp, $referral->agent_b, $date);
	}
	public function send_closed_referral($refid, $ammount, $files, $payer){
		//TODO: refactor this
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('Activitylog', 'ActivitylogModel');
		$referral = $this->get_referral($refid);
		$signer = new HelloSign();
		$signer->login();
		$signer->setEnvelope($referral->docusign_envelope);
		$signer->getEnvelope();
		$agreement = $signer->getDocument();
		if($referral->agent_b==$payer) {
			$fee = explode('%',$referral->referral_fee);
			$broker = $model2->get_broker_info(JFactory::getUser($referral->agent_a)->email);
			$topay = $ammount * ('.'.$fee[0]);
			$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$referral->agent_a."'>".stripslashes_all(JFactory::getUser($referral->agent_a)->name)."</a>";
			$user_info = $model2->get_user_registration($referral->agent_b);
			//language loading
			  	$language = JFactory::getLanguage();
		      	$extension = 'com_nrds';
		      	$base_dir = JPATH_SITE;
		      	$language_tag = $user_info->currLanguage;
		      	$language->load($extension, $base_dir, $language_tag, true);
		    //language loading
			if($user_info->gender == "Female"){
				$salut_u = JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($user_info->lastname);
			} else {
				$salut_u = JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($user_info->lastname);
			}
			$subject = "Your AgentBridge Referral has Closed!";
			$body = "<p style='text-align:justify; padding-top:20px '>Hello ".$salut_u.",<br/><br/>";
			$body .= "Congratulations on the closing of ".stripslashes_all($model2->get_client($referral->client_id)->name).", referral from $other_linked_name.  <br/><br/>";
			$body .= "Please disburse to ".$broker->broker_name." the referral fee of ".format_currency_global($topay)." as agreed to in the referral contract with $other_linked_name.<br/><br/>";
			$body .= "Attached you will find copy of your referral agreement with final closing numbers as well as an invoice of the AgentBridge transaction fee for your records.<br/><br/>";
			$body .= "Thank you for allowing AgentBridge to facilitate this referral for you.<br/>";
			$body .= "The AgentBridge Team</p>";
			$email = JFactory::getUser($referral->agent_b)->email;
			//$email = 'adinbautista@gmail.com';
			$mailer = JFactory::getMailer();
			$mailer->AddAttachment('referrals/'.$files['invoice'].'.pdf');
			$mailer->AddAttachment('referrals/'.$files['payment_method'].'.pdf');
			$mailer->AddAttachment('referrals/'.$agreement.'.pdf');
			$mailer->setBody($body);
			$mailer->setSubject($subject);
			$mailer->addRecipient($email);
			$mailer->IsHTML(true);
			$mailer->SetFrom('AgentBridge');
			$mailer->Send();
		}
		else{
			$fee = explode('%',$referral->referral_fee);
			$topay = $ammount * ('.'.$fee[0]);
			$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$referral->agent_b."'>".stripslashes_all(JFactory::getUser($referral->agent_b)->name)."</a>";
			$user_info = $model2->get_user_registration($referral->agent_a);
			//language loading
			  	$language = JFactory::getLanguage();
		      	$extension = 'com_nrds';
		      	$base_dir = JPATH_SITE;
		      	$language_tag = $other_user_info->currLanguage;
		      	$language->load($extension, $base_dir, $language_tag, true);
		    //language loading
			if($user_info->gender == "Female"){
				$salut_u = JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($other_user_info->lastname);
			} else {
				$salut_u = JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($other_user_info->lastname);
			}
			$subject = "Your Referral fee for ".stripslashes_all($model2->get_client($referral->client_id)->name)." is Closed";
			$body = "<p style='text-align:justify; '>Hello ".$salut_u.",</p><br/>";
			$body .= "<p style='text-align:justify; '>Congratulations! Your referral of ".$model2->get_client($referral->client_id)->name." has closed and your referral fee of ".format_currency_global($topay)." is being disbursed.</p>";
			$body .= "<p style='text-align:justify; '>Attached you will find a copy of your referral agreement with final closing numbers, as well as an AgentBridge transaction fee invoice for your records.</p><br/>";
			$body .= "<p style='text-align:justify; '>Upon receipt of your referral fee, please update AgentBridge. The system will then complete the referral and add it to your AgentBridge rating. </p><br/><br/>";
			$body .= "<p style='text-align:justify; '>Thank you for allowing AgentBridge to facilitate this referral for you.</p>";
			$body .= "<p style='text-align:justify; '>The AgentBridge Team</p>";
			$email = JFactory::getUser($referral->agent_a)->email;
			//$email = 'adinbautista@gmail.com';
			$mailer = JFactory::getMailer();
			$mailer->AddAttachment('referrals/'.$files['invoice'].'.pdf');
			$mailer->AddAttachment('referrals/'.$agreement.'.pdf');
			$mailer->setBody($body);
			$mailer->setSubject($subject);
			$mailer->addRecipient($email);
			$mailer->IsHTML(true);
			$mailer->SetFrom('AgentBridge');
			$mailer->Send();
		}
	}
	public function create_authorize_profile($user_id, $email){
		$config = new JConfig();
		$authnet = new AuthorizeNet($config->authnet_loginid, $config->authnet_transkey);
		$response = $authnet->profile_create($user_id, $email);
		if($response->messages->resultCode=="Error"){
			return array('status'=> '-1', 'message'=>(string)$response->messages->message->text);
		}
		else if($response->messages->resultCode=="Ok"){
			return array('status'=> '1', 'message'=>(string)$response->messages->message->text, 'profile_id'=>(string)$response->customerProfileId);
		}
		/*$mywallet = new JObject();
		$mywallet -> 
		$this->insertIfnotExists($object, $primary, $table);*/
	}
	public function save_payment_profile($uid, $profile_id, $payment_profile_id){
		$paymentid = new JObject();
		$paymentid->user_id = $uid;
		$paymentid->au_profile_id = $profile_id;
		$paymentid->au_payment_profile_id = $payment_profile_id;
		$this->insertIfnotExists($paymentid, 'profile_id', '#__payment_profiles');
	}
	public function create_payment_profile($cs_profile_id, $fname, $lname, $address, $city, $state, $zip, $country, $cardno, $cardexp){
		$config = new JConfig();
		$authnet = new AuthorizeNet($config->authnet_loginid, $config->authnet_transkey);
		$response = $authnet->create_payment_profile($cs_profile_id, $fname, $lname, $address, $city, $state, $zip, $country, $cardno, $cardexp, $config->authorize_test_mode);
		//print_r($response);
		if($response->messages->resultCode=="Error"){
			return array('status'=> '-1', 'message'=>(string)$response->messages->message->text);
		}
		else if($response->messages->resultCode=="Ok"){
			return array('status'=> '1', 'message'=>(string)$response->messages->message->text, 'payment_profile_id'=>(string)$response->customerPaymentProfileId);
		}
	}
	public function debugModel(){
	}
	public function create_transaction($user_id, $ammount, $invoice){
		$config = new JConfig();
		$authnet = new AuthorizeNet($config->authnet_loginid, $config->authnet_transkey);
		$paymentprofile = $this->get_payment_profile_db($user_id);
		$cs_profile_id = $paymentprofile->au_profile_id;
		$cs_payment_id = $paymentprofile->au_payment_profile_id;
		$response = $authnet->transaction_create($ammount, $cs_profile_id, $cs_payment_id, $invoice);
		if($response->messages->resultCode=="Error"){
			return array('status'=> '-1', 'message'=>(string)$response->messages->message->text);
		}
		else if($response->messages->resultCode=="Ok"){
			return array('status'=> '1', 'message'=>(string)$response->messages->message->text, 'directResponse'=>(string)$response->directResponse);
		}
	}
	public function get_payment_profile_db($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__payment_profiles')->where('user_id = '.$id);
		return $db->setQuery($query)->loadObject();
	}
	public function get_payment_profile($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__payment_profiles')->where('user_id = '.$id);
		$raw = $db->setQuery($query)->loadObject();
		if($raw){
			$config = new JConfig();
			$authnet = new AuthorizeNet($config->authnet_loginid, $config->authnet_transkey);
			return $authnet->get_payment_profile($raw->au_profile_id, $raw->au_payment_profile_id);
		}
		return null;
		//return $raw;
	}
	public function get_next_invoice(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$config = new JConfig();
		$query = "SELECT AUTO_INCREMENT AS id FROM information_schema.tables WHERE table_schema = '".$config->db."' AND table_name = 'tbl_payment_profiles'";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$nextid = $rows[0]->id;
		return $nextid;
	}
	public function get_buyer_image($property_type, $sub_type, $bw = false) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$config = new JConfig();
		$query->select('type_name')->from('#__property_type')->where('type_id = '.$property_type);
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		#print_r($rows);
		#echo "<br />";
		//$query = $db->getQuery(true);
		$query->select('name')->from('#__property_sub_type')->where('type_id = '.$property_type.' AND sub_id='.$sub_type);
		$db->setQuery($query);
		$rows2 = $db->loadObjectList();
		$property_type = strtolower(str_replace(" ", "-", $rows2[0]->type_name));
		$sub_type = strtolower($rows2[0]->name);
		switch($sub_type) {
			case "motel/hotel":
				$sub_type = "motel";
				break;
			case "townhouse/row house":
				$sub_type = "townhouse";
				break;
			case "assisted care":
				$sub_type = "assist";
				break;
			case "special purpose":
				$sub_type = "special";
				break;
			case "multi family":
				$sub_type = "multi-family";
				break;
		}
		$img_filename = ($bw) ? $property_type . "-" . $sub_type . "-bw.jpg" : $property_type . "-" . $sub_type . ".jpg";
		return $img_filename;
	}

	//Form Fields

	function getFieldsBedrooms($reqfield = ""){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_BEDS").'</label>
				<select
					id="jform_bedroom"
					name="jform[bedroom]" class="'.$reqfield.'">
					<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				for($i=1; $i<=5; $i++){
					//	if($i==$data->bedroom) $selected = "selected";
					//	else $selected = "";
						$fieldBody.= "<option value=\"$i\"".$selected.">".$i."</option>";
					}

		$fieldBody.='<option value="6+">6+</option>
			</select>
			<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsBathrooms($reqfield = ""){

		
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_BATHS").'</label>
				<select
					id="jform_bathroom"
					name="jform[bathroom]" class="'.$reqfield.'">
					<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				for($i=1; $i<=5; $i++){
					//	if($i==$data->bathroom) $selected = "selected";
					//	else $selected = "";
						$fieldBody.= "<option value=\"$i\"".$selected.">".$i."</option>";
					}

		$fieldBody.='<option value="6+">6+</option>
			</select>
			<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsBldgSqft($getMeasurement,$defsqft,$bldgsuffix="",$reqfield=""){

		
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft.'</label>
				<select id="jform_bldgsqft"
			name="jform[bldgsqft]" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['bldg'.$bldgsuffix] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsView($reqfield=""){

		
		$fieldBody='
		 <div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_VIEW").'</label> <select id="jform_view" name="jform[view]" class="'.$reqfield.'">
				<option value="">--- '.JText::_("COM_POPS_TERMS_DEFAULT_SELECT").' ---</option>
				<option value="None">'.JText::_("COM_POPS_TERMS_NONE").'</option>
				<option value="Panoramic">'.JText::_("COM_POPS_TERMS_PANORAMIC").'</option>
				<option value="City">'.JText::_("COM_POPS_TERMS_CITY").'</option>
				<option value="Mountains/Hills">'.JText::_("COM_POPS_TERMS_MOUNT").'</option>
				<option value="Coastline">'.JText::_("COM_POPS_TERMS_COAST").'</option>
				<option value="Water">'.JText::_("COM_POPS_TERMS_WATER").'</option>
				<option value="Ocean">'.JText::_("COM_POPS_TERMS_OCEAN").'</option>
				<option value="Lake/River">'.JText::_("COM_POPS_TERMS_LAKE").'</option>
				<option value="Landmark">'.JText::_("COM_POPS_TERMS_LANDMARK").'</option>
				<option value="Desert">'.JText::_("COM_POPS_TERMS_DESERT").'</option>
				<option value="Bay">'.JText::_("COM_POPS_TERMS_BAY").'</option>
				<option value="Vineyard">'.JText::_("COM_POPS_TERMS_VINE").'</option>
				<option value="Golf">'.JText::_("COM_POPS_TERMS_GOLF").'</option>
				<option value="Other">'.JText::_("COM_POPS_TERMS_OTHER").'</option>
			</select>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">'.JText::_("COM_NRDS_FORMERROR").'</p>
		 </div>';

		return $fieldBody;

	}

	function getFieldsLotSqFt($getMeasurement,$defsqft_l,$reqfield=''){
		
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft_l.'</label>
				<select id="jform_lotsqft"
			name="jform[lotsqft]" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['lot'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_lotsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsCondition($reqfield=''){

		$fieldBody='
		 <div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CONDITION').'</label> <select id="jform_condition"
				name="jform[condition]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>
				<option value="Fixer">'.JText::_('COM_POPS_TERMS_FIX').'</option>
				<option value="Good">'.JText::_('COM_POPS_TERMS_GOOD').'</option>
				<option value="Excellent">'.JText::_('COM_POPS_TERMS_EXCELLENT').'</option>
				<option value="Remodeled">'.JText::_('COM_POPS_TERMS_REMODEL').'</option>
				<option value="New Construction">'.JText::_('COM_POPS_TERMS_NEWCONSTRUCT').'</option>
				<option value="Under Construction">'.JText::_('COM_POPS_TERMS_UNDERCONSTRUCT').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsStyle($reqfield=''){

		$fieldBody='
		 <div class="left ys push-top2" style="margin-top: 24x;">
			<label>'.JText::_('COM_POPS_TERMS_STYLE').'</label> 
			<select id="jform_style" style="width:200px" name="jform[style]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="American Farmhouse">'.JText::_('COM_POPS_TERMS_AMERICANFARM').'</option>
				<option value="Art Deco">'.JText::_('COM_POPS_TERMS_ARTDECO').'</option>
				<option value="Art Modern/Mid Century">'.JText::_('COM_POPS_TERMS_MIDCENT').'</option>
				<option value="Cape Cod">'.JText::_('COM_POPS_TERMS_CAPECOD').'</option>
				<option value="Colonial Revival">'.JText::_('COM_POPS_TERMS_COLONIAL').'</option>
				<option value="Contemporary">'.JText::_('COM_POPS_TERMS_CONTEMPORARY').'</option>
				<option value="Craftsman">'.JText::_('COM_POPS_TERMS_CRAFTSMAN').'</option>
				<option value="French">'.JText::_('COM_POPS_TERMS_FRENCH').'</option>
				<option value="Italian/Tuscan">'.JText::_('COM_POPS_TERMS_ITALIAN').'</option>
				<option value="Prairie Style">'.JText::_('COM_POPS_TERMS_PRAIRIE').'</option>
				<option value="Pueblo Revival">'.JText::_('COM_POPS_TERMS_PUEBLO').'</option>
				<option value="Ranch">'.JText::_('COM_POPS_TERMS_RANCH').'</option>
				<option value="Spanish/Mediterranean">'.JText::_('COM_POPS_TERMS_SPANISH').'</option>
				<option value="Swiss Cottage">'.JText::_('COM_POPS_TERMS_SWISS').'</option>
				<option value="Tudor">'.JText::_('COM_POPS_TERMS_TUDOR').'</option>
				<option value="Victorian">'.JText::_('COM_POPS_TERMS_VICTORIAN').'</option>
				<option value="Historic">'.JText::_('COM_POPS_TERMS_HISTORIC').'</option>
				<option value="Architecturally Significant">'.JText::_('COM_POPS_TERMS_ARCHITECTURE').'</option>
				<option value="Green">'.JText::_('COM_POPS_TERMS_GREEN').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsGarage($reqfield=''){
		
		$fieldBody='
		<div class="left ys push-top2" style="margin-top: 24px;">
		<label>'.JText::_('COM_POPS_TERMS_GARAGE').'</label> <select id="jform_garage" name="jform[garage]" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=1;$i<=7;$i++){
					$fieldBody.="<option value=\"$i\">$i</option>";
				}

		$fieldBody.='
			<option value="8+">8+</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsYearBuilt($reqfield=''){
		
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_YEAR_BUILT').'</label> 
			<select id="jform_yearbuilt"
				name="jform[yearbuilt]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="2016">2016</option>
				<option value="2015">2015</option>
				<option value="2014">2014</option>
				<option value="2013">2013</option>
				<option value="2012">2012</option>
				<option value="2011">2011</option>
				<option value="2010">2010</option>
				<option value="2009-2000">2009-2000</option>
				<option value="1999-1990">1999-1990</option>
				<option value="1989-1980">1989-1980</option>
				<option value="1979-1970">1979-1970</option>
				<option value="1969-1960">1969-1960</option>
				<option value="1959-1950">1959-1950</option>
				<option value="1949-1940">1949-1940</option>
				<option value="1939-1930">1939-1930</option>
				<option value="1929-1920">1929-1920</option>
				<option value="< 1919">< 1919</option>
				<option value="COM_POPS_TERMS_UNDISC">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}


	function getFieldsPoolSpa($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_POOL_SPA').'</label> <select id="jform_poolspa" class="'.$reqfield.'"
			name="jform[poolspa]">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="None">'.JText::_('COM_POPS_TERMS_NONE').'</option>
			<option value="Pool">'.JText::_('COM_POPS_TERMS_POOL').'</option>
			<option value="Pool/Spa">'.JText::_('COM_POPS_TERMS_POOL_SPA').'</option>
			<option value="Spa">'.JText::_('COM_POPS_TERMS_OTHER').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select id="jform_'.$id.'"
			name="jform['.$id.']" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="One Story">'.JText::_('COM_POPS_TERMS_ONESTORY').'</option>
			<option value="Two Story">'.JText::_('COM_POPS_TERMS_TWOSTORY').'</option>
			<option value="Three Story">'.JText::_('COM_POPS_TERMS_THREESTORY').'</option>
			<option value="Water Access">'.JText::_('COM_POPS_TERMS_WATERACCESS').'</option>
			<option value="Horse Property">'.JText::_('COM_POPS_TERMS_HORSE').'</option>
			<option value="Golf Course">'.JText::_('COM_POPS_TERMS_GOLFCOURSE').'</option>
			<option value="Walkstreet">'.JText::_('COM_POPS_TERMS_WALKSTREET').'</option>
			<option value="Media Room">'.JText::_('COM_POPS_TERMS_MEDIA').'</option>
			<option value="Guest House">'.JText::_('COM_POPS_TERMS_GUESTHOUSE').'</option>
			<option value="Wine Cellar">'.JText::_('COM_POPS_TERMS_WINECELLAR').'</option>
			<option value="Tennis Court">'.JText::_('COM_POPS_TERMS_TENNISCOURT').'</option>
			<option value="Den/Library">'.JText::_('COM_POPS_TERMS_DENLIB').'</option>
			<option value="Green Const.">'.JText::_('COM_POPS_TERMS_GREENCONSTRUCT').'</option>
			<option value="Basement">'.JText::_('COM_POPS_TERMS_BASEMENT').'</option>
			<option value="RV/Boat Parking">'.JText::_('COM_POPS_TERMS_RV').'</option>
			<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
		</select>
		</div>';

		return $fieldBody;

	}


	function getFieldsCondoFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
			name="jform['.$id.']">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="Gym" >'. JText::_('COM_POPS_TERMS_GYM').'</option>
			<option value="Security">'.JText::_('COM_POPS_TERMS_SECURITY').'</option>
			<option value="Tennis Court">'.JText::_('COM_POPS_TERMS_TENNISCOURT').'</option>
			<option value="Doorman" >'.JText::_('COM_POPS_TERMS_DOORMAN').'</option>
			<option value="Penthouse" >'.JText::_('COM_POPS_TERMS_PENTHOUSE').'</option>
			<option value="One Story" >'.JText::_('COM_POPS_TERMS_ONESTORY').'</option>
			<option value="Two Story" >'.JText::_('COM_POPS_TERMS_TWOSTORY').'</option>
			<option value="Three Story">'.JText::_('COM_POPS_TERMS_THREESTORY').'</option>
			<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsLandFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
			name="jform['.$id.']">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="Sidewalks">'.JText::_('COM_POPS_TERMS_SIDEWALKS').'</option>
			<option value="Utilities">'.JText::_('COM_POPS_TERMS_UTILITIES').'</option>
			<option value="Curbs">'.JText::_('COM_POPS_TERMS_CURBS').'</option>
			<option value="Horse Trails">'.JText::_('COM_POPS_TERMS_HORSETRAILS').'</option>
			<option value="Rural">'.JText::_('COM_POPS_TERMS_RURAL').'</option>
			<option value="Urban">'.JText::_('COM_POPS_TERMS_URBAN').'</option>
			<option value="Suburban">'.JText::_('COM_POPS_TERMS_SUBURBAN').'</option>
			<option value="Permits">'.JText::_('COM_POPS_TERMS_PERMITS').'</option>
			<option value="HOA">'.JText::_('COM_POPS_TERMS_HOA').'</option>
			<option value="Sewer">'.JText::_('COM_POPS_TERMS_SEWER').'</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">'.JText::_('COM_POPS_TERMS_COASTAL').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsMultiFamFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
				name="jform['.$id.']">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Rent Control">'.JText::_('COM_POPS_TERMS_RENTCONTROL').'</option>
				<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
				<option value="Assoc-Pool">Assoc-'.JText::_('COM_POPS_TERMS_POOL').'</option>
				<option value="Assoc-Spa">'.JText::_('COM_POPS_TERMS_ASSOCSPA').'</option>
				<option value="Assoc-Tennis">'.JText::_('COM_POPS_TERMS_ASSOCTENNIS').'</option>
				<option value="Assoc-Other">'.JText::_('COM_POPS_TERMS_ASSOCOTHER').'</option>
				<option value="Section 8">'.JText::_('COM_POPS_TERMS_SECTION8').'</option>
				<option value="25% Occupied">'.JText::_('COM_POPS_TERMS_25OCCUPIED').'</option>
				<option value="50% Occupied">'.JText::_('COM_POPS_TERMS_50OCCUPIED').'</option>
				<option value="75% Occupied">'.JText::_('COM_POPS_TERMS_75OCCUPIED').'</option>
				<option value="100% Occupied">'.JText::_('COM_POPS_TERMS_100OCCUPIED').'</option>
				<option value="Cash Cow">'.JText::_('COM_POPS_TERMS_CASHCOW').'</option>
				<option value="Value Add">'.JText::_('COM_POPS_TERMS_VALUEADD').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsOfficeFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
				name="jform['.$id.']">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Mixed use">'.JText::_('COM_POPS_TERMS_MIXEDUSE').'</option>
				<option value="Single Tenant">'.JText::_('COM_POPS_TERMS_SINGLETENANT').'</option>
				<option value="Multiple Tenant">'.JText::_('COM_POPS_TERMS_MULTIPLETENANT').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
				<option value="Net-Leased">'.JText::_('COM_POPS_TERMS_NETLEASED').'</option>
				<option value="Owner User">'.JText::_('COM_POPS_TERMS_OWNERUSER').'</option>
				<option value="Vacant">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsIndusFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
				name="jform['.$id.']">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Mixed use">'.JText::_('COM_POPS_TERMS_MIXEDUSE').'</option>
				<option value="Single Tenant">'.JText::_('COM_POPS_TERMS_SINGLETENANT').'</option>
				<option value="Multiple Tenant">'.JText::_('COM_POPS_TERMS_MULTIPLETENANT').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
				<option value="Net-Leased">'.JText::_('COM_POPS_TERMS_NETLEASED').'</option>
				<option value="Owner User">'.JText::_('COM_POPS_TERMS_OWNERUSER').'</option>
				<option value="Vacant">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsHotelFeatures($id,$reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="jform_'.$id.'"
				name="jform['.$id.']">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT') .' ---</option>
				<option value="Restaurant">'.JText::_('COM_POPS_TERMS_RESTAURANT').'</option>
				<option value="Bar">'.JText::_('COM_POPS_TERMS_BAR').'</option>
				<option value="Pool">'.JText::_('COM_POPS_TERMS_POOL').'</option>
				<option value="Banquet Room">'.JText::_('COM_POPS_TERMS_BANQUETROOM').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsUnitSqft($getMeasurement,$defsqft_u,$reqfield=''){

		
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft_u.'</label>
				<select id="jform_unitsqft"
			name="jform[unitsqft]" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['bldg'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_unitsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsBldgType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_BLDG_TYPE').'</label> <select class="'.$reqfield.'" id="jform_bldgtype"
			name="jform[bldgtype]">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="North Facing">'.JText::_('COM_POPS_TERMS_NORTH').'</option>
			<option value="South Facing">'.JText::_('COM_POPS_TERMS_SOUTH').'</option>
			<option value="East Facing">'.JText::_('COM_POPS_TERMS_EAST').'</option>
			<option value="West Facing">'.JText::_('COM_POPS_TERMS_WEST').'</option>
			<option value="Low Rise">'.JText::_('COM_POPS_TERMS_LOWRISE').'</option>
			<option value="Mid Rise">'.JText::_('COM_POPS_TERMS_MIDRISE').'</option>
			<option value="High Rise">'.JText::_('COM_POPS_TERMS_HIGHRISE').'</option>
			<option value="Co-Op">'.JText::_('COM_POPS_TERMS_COOP').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldLotSize($getMeasurement,$defsqft_ls,$reqfield=''){


		$fieldBody='
		<div class="left ys push-top2">
		<label>'.$defsqft_ls.'</label> <select id="jform_lotsize" 
			name="jform[lotsize]" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				foreach ($getMeasurement['lot'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_lotsize error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;
	}


	function getFieldsZoned($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_ZONED').'</label> <select
			class="'.$reqfield.'"
			id="jform_zoned"
			name="jform[zoned]">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1">1 '.JText::_('COM_POPS_TERMS_UNITONE').'</option>
				<option value="2">2 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="3-4">3-4 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="5-20">5-20 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="20">20+ '.JText::_('COM_POPS_TERMS_UNIT').'</option>
			</select>
			<p class="jform_zoned error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsTerm($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TERM').'</label> <select id="jform_term" name="jform[term]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Short Term">'.JText::_('COM_POPS_TERMS_SHORTTERM').'</option>
				<option value="M to M">'.JText::_('COM_POPS_TERMS_M2M').'</option>
				<option value="Year Lease">'.JText::_('COM_POPS_TERMS_YEARLEASE').'</option>
				<option value="Multi Year Lease">'.JText::_('COM_POPS_TERMS_MULTIYEARLEASE').'</option>
				<option value="Lease Option">'.JText::_('COM_POPS_TERMS_LEASEOPTION').'</option>
			</select>
			<div>
				<p class="jform_term error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
			</div>
		</div>';

		return $fieldBody;

	}

	function getFieldsTermPossesion($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_POSSESSION').'</label> <select id="jform_possession"
				name="jform[possession]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Immediately">'.JText::_('COM_POPS_TERMS_IMMEDIATELY').'</option>
				<option value="Within 30 days">'.JText::_('COM_POPS_TERMS_30DAYS').'</option>
				<option value="Within 60 days">'.JText::_('COM_POPS_TERMS_60DAYS').'</option>
				<option value="Within 90 days">'.JText::_('COM_POPS_TERMS_90DAYS').'</option>
				<option value="Within 180 days">'.JText::_('COM_POPS_TERMS_180DAYS').'</option>
			</select>
			<p class="jform_possession error_msg" style="display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsPet(){

		$fieldBody='
		<div class="left ys" style="margin-top: 30px;width:96px">
			<label>'.JText::_('COM_POPS_TERMS_PET').'</label> 
			<a id="pyes" href="javascript: void(0)" style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1">'.JText::_('COM_POPS_TERMS_PETYES').'</a> 
			<a id="pno" href="javascript: void(0)" class="left gradient-gray no pet" rel="0">'.JText::_('COM_POPS_TERMS_PETNO').'</a>
			<input type="hidden" value="1" id="jform_pet" name="jform[pet]" class="text-input reqfield" />
		</div>';

		return $fieldBody;

	}

	function getFieldsFurnished(){

		$fieldBody='
		<div class="left ys" style="margin-top: 30px;width:96px">
			<label>'.JText::_('COM_POPS_TERMS_FURNISHED').'</label> 
			<a id="fyes" href="javascript: void(0)" style="margin-right:1px" class="left gradient-gray no furnished" rel="1" value="1">'.JText::_('COM_POPS_TERMS_PETYES').'</a> 
			<a id="fno" href="javascript: void(0)" class="left gradient-blue-toggle yes furnished" rel="0" value="0">'.JText::_('COM_POPS_TERMS_PETNO').'</a>
			<input type="hidden" value="0" id="jform_furnished" name="jform[furnished]" class="text-input reqfield" />
		</div>';

		return $fieldBody;

	}

	function getFieldsTermUnits($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_UNIT').'</label> <select id="jform_units" name="jform[units]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="2">'.JText::_('COM_POPS_TERMS_DUPLEX').'</option>
				<option value="3">'.JText::_('COM_POPS_TERMS_TRIPLEX').'</option>
				<option value="4">'.JText::_('COM_POPS_TERMS_QUAD').'</option>
				<option value="5-9">5-9</option>
				<option value="10-15">10-15</option>
				<option value="16-29">16-29</option>
				<option value="30-50">30-50</option>
				<option value="50-100">50-100</option>
				<option value="101-150">101-150</option>
				<option value="151-250">151-250</option>
				<option value="251">251+</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_units error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsTermCAP($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CAP').'</label>
				<select id="jform_cap" name="jform[cap]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="0-.9">0-.9</option>
				<option value="1-1.9">1-1.9</option>
				<option value="2-2.9">2-2.9</option>
				<option value="3-3.9">3-3.9</option>
				<option value="4-4.9">4-4.9</option>
				<option value="5-5.9">5-5.9</option>
				<option value="6-6.9">6-6.9</option>
				<option value="7-7.9">7-7.9</option>
				<option value="8-8.9">8-8.9</option>
				<option value="9-9.9">9-9.9</option>
				<option value="10-10.9">10-10.9</option>
				<option value="11-11.9">11-11.9</option>
				<option value="12-12.9">12-12.9</option>
				<option value="13-13.9">13-13.9</option>
				<option value="14-14.9">14-14.9</option>
				<option value="15">15+</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_cap error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsTermGRM($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_GRM').'</label> <select id="jform_grm" name="jform[grm]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="0">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
				<option value="1-2">1-2</option>
				<option value="3-4">3-4</option>
				<option value="4-5">4-5</option>
				<option value="5-6">5-6</option>
				<option value="6-7">6-7</option>
				<option value="7-8">7-8</option>
				<option value="8-9">8-9</option>
				<option value="9-10">9-10</option>
				<option value="10-11">10-11</option>
				<option value="11-12">11-12</option>
				<option value="12-13">12-13</option>
				<option value="13-14">13-14</option>
				<option value="14-15">14-15</option>
				<option value="15-16">15-16</option>
				<option value="16-17">16-17</option>
				<option value="17-18">17-18</option>
				<option value="18-19">18-19</option>
				<option value="19-20">19-20</option>
				<option value="20">20+</option>
			</select>	
			<p class="jform_grm error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldOccupancy($reqfield=''){


		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_OCCUPANCY').'</label> <select id="jform_occupancy" class="'.$reqfield.'"
			name="jform[occupancy]">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=100; $i>=0; $i=$i-5){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldOfficeType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label>
				<select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Office">'.JText::_('COM_POPS_TERMS_OFFICE').'</option>
				<option value="Institutional">'.JText::_('COM_POPS_TERMS_INSTITUTIONAL').'</option>
				<option value="Medical">'.JText::_('COM_POPS_TERMS_MEDICAL').'</option>
				<option value="Warehouse">'.JText::_('COM_POPS_TERMS_WAREHOUSE').'</option>
				<option value="Condo">'.JText::_('COM_POPS_TERMS_CONDO').'</option>
				<option value="R&D">R&D</option>
				<option value="Business Park">'.JText::_('COM_POPS_TERMS_BUSINESSPARK').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldOfficeClass($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CLASS').'</label>
				<select id="jform_class" name="jform[class]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="4">A</option>
				<option value="3">B</option>
				<option value="2">C</option>
				<option value="1">D</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_class error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsParkingRatio($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_PARKING_RATIO').'</label> <select id="jform_parking"
				name="jform[parking]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1/1000">1/1000</option>
				<option value="1.5/1000">1.5/1000</option>
				<option value="2/1000">2/1000</option>
				<option value="2.5/1000">2.5/1000</option>
				<option value="3/1000">3/1000</option>
				<option value="3.5/1000">3.5/1000</option>
				<option value="4/1000">4/1000</option>
				<option value="4.5/1000">4.5/1000</option>
				<option value="5/1000">5/1000</option>
				<option value="other">'.JText::_('COM_POPS_TERMS_OTHER').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsIndusType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Flex Space">Flex Space</option>
				<option value="Business Park">'.JText::_('COM_POPS_TERMS_BUSINESSPARK').'</option>
				<option value="Condo">'.JText::_('COM_POPS_TERMS_CONDO').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
				<option value="Manufacturing">'.JText::_('COM_POPS_TERMS_MANUFACTURING').'</option>
				<option value="Office Showroom">'.JText::_('COM_POPS_TERMS_OFFICESHOWROOM').'</option>
				<option value="R&D">R&D</option>
				<option value="Self/Mini Storage">'.JText::_('COM_POPS_TERMS_SELFSTORAGE').'</option>
				<option value="Truck Terminal/Hub">'.JText::_('COM_POPS_TERMS_TRUCK').'</option>
				<option value="Warehouse">'.JText::_('COM_POPS_TERMS_WAREHOUSE').'</option>
				<option value="Distribution">'.JText::_('COM_POPS_TERMS_DISTRIBUTION').'</option>
				<option value="Cold Storage">'.JText::_('COM_POPS_TERMS_COLDSTORAGE').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldCeiling($reqfield=''){


		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_CEILING_HEIGHT').'</label> <select id="jform_ceiling"
			name="jform[ceiling]" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=12; $i<=34; $i=$i+2){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			<option value="36+">36+</option>
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldStories($reqfield=''){


		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_STORIES').'</label> <select id="jform_stories"
			name="jform[stories]" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=1; $i<=5; $i++){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			<option value="6+">6+</option>
			<option value="COM_POPS_TERMS_VACANT">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldsRetailType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Community Center">'.JText::_('COM_POPS_TERMS_COMMUNITYCENTER').'</option>
				<option value="Strip Center">'.JText::_('COM_POPS_TERMS_STRIPCENTER').'</option>
				<option value="Outlet Center">'.JText::_('COM_POPS_TERMS_OUTLETCENTER').'</option>
				<option value="Power Center">'.JText::_('COM_POPS_TERMS_POWERCENTER').'</option>
				<option value="Anchor">'.JText::_('COM_POPS_TERMS_ANCHOR').'</option>
				<option value="Restaurant">'.JText::_('COM_POPS_TERMS_RESTAURANT').'</option>
				<option value="Service Station">'.JText::_('COM_POPS_TERMS_SERVICESTATION').'</option>
				<option value="Retail Pad">'.JText::_('COM_POPS_TERMS_RETAILPAD').'</option>
				<option value="Free Standing">'.JText::_('COM_POPS_TERMS_FREESTANDING').'</option>
				<option value="Day Care/Nursery">'.JText::_('COM_POPS_TERMS_DAYCARE').'</option>
				<option value="Post Office">'.JText::_('COM_POPS_TERMS_POSTOFFICE').'</option>
				<option value="Vehicle">'.JText::_('COM_POPS_TERMS_VEHICLE').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsHotelType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Economy">'.JText::_('COM_POPS_TERMS_ECONOMY').'</option>
				<option value="Full Service">'.JText::_('COM_POPS_TERMS_FULLSERVICE').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsRoomCount($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_ROOM_COUNT').'</label> <select id="jform_roomcount" name="jform[roomcount]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1-9">1-9</option>
				<option value="10-19">10-19</option>
				<option value="20-29">20-29</option>
				<option value="30-39">30-39</option>
				<option value="40-49">40-49</option>
				<option value="50-99">50-99</option>
				<option value="100-149">100-149</option>
				<option value="150-199">150-199</option>
				<option value="200">200+</option>
			</select>
			<p class="jform_roomcount error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsAssistType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'--- </option>
				<option value="Assisted">'.JText::_('COM_POPS_TERMS_ASSISTED').'</option>
				<option value="Acute Care">'.JText::_('COM_POPS_TERMS_ACUTECARE').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p style="display:none" class="jform_type error_msg"> '. JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsSpecialType($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2') .'</label> <select id="jform_type" name="jform[type]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Golf">'. JText::_('COM_POPS_TERMS_GOLF').'</option>
				<option value="Marina">'.JText::_('COM_POPS_TERMS_MARINA').'</option>
				<option value="Theater">'.JText::_('COM_POPS_TERMS_THEATER').'</option>
				<option value="Religious">'.JText::_('COM_POPS_TERMS_RELIGIOUS').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="display:none;">'.JText::_('COM_NRDS_FORMERROR').' </p>
		</div>';

		return $fieldBody;

	}

	function getFieldsAvailSqFt($getMeasurement,$defsqft_a,$reqfield=''){


		$fieldBody='
		<div class="left ys push-top2">
		<label class="changebycountry">'.$defsqft_a.'</label> <select id="jform_available" name="jform[available]" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				foreach ($getMeasurement['bldg'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_available error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;
	}


	function getFieldsOfficeTypeLease($reqfield=''){

		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE_LEASE').'</label> <select id="jform_typelease" name="jform[typelease]" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="NNN">'.JText::_('COM_POPS_TERMS_NNN').'</option>
				<option value="FSG">'.JText::_('COM_POPS_TERMS_FSG').'</option>
				<option value="MG">'.JText::_('COM_POPS_TERMS_MG').'</option>
				<option value="Modified Net">'.JText::_('COM_POPS_TERMS_MODIFIEDNET').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_typelease error_msg" style="display:none;">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}
	
	function post_request($url, $data, $referer='') {
	 
		// Convert the data array into URL Parameters like a=b&foo=bar etc.
		$data = http_build_query($data);
		// parse the given URL
		$url = parse_url($url);
		
		// extract host and path:
		$host = $url['host'];
		$path = $url['path'];
		
		// open a socket connection on port 80 - timeout: 30 sec
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
	 
		if ($fp){
			// send the request headers:
			$out = "GET $path?$data HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/json\r\n";
			$out .= "Content-length: ". strlen($data) ."\r\n";
			$out .= "Connection: close\r\n\r\n";
		
			fwrite($fp, $out);
			fwrite($fp, urlencode($data));
	 
			$result = ''; 
			while(!feof($fp)) {
				// receive the results of the request
				$result .= fgets($fp, strlen($data));
			}
		} else { 
			return array(
				'status' => 'err', 
				'error' => "$errstr ($errno)"
			);
		}
	 
		// close the socket connection:
		fclose($fp);
	 
		// split the result header from the content
		$result = explode("\r\n\r\n", $result, 2);
	 
		$header = isset($result[0]) ? $result[0] : '';
		$content = isset($result[1]) ? $result[1] : '';
	 
		// return as structured array:
		return array(
			'status' => 'ok',
			'header' => $header,
			'content' => $content
		);
	}

	//Form Fields
	

}
?>
<?php 
class AuthorizeNet{
	private $login_name;
	private $transaction_key;
	private $apihost = "api.authorize.net";
	private $apipath = "/xml/v1/request.api";
	public function __construct($login_name, $transaction_key){
		$this->login_name = $login_name;
		$this->transaction_key = $transaction_key;
	}
	public function get_payment_profile($cs_profile_id, $cs_pp_id){
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<getCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>".$cs_profile_id."</customerProfileId>".
 		"<customerPaymentProfileId>".$cs_pp_id."</customerPaymentProfileId>".
		"</getCustomerPaymentProfileRequest>";
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		return $parsedresponse;
	}
	public function create_payment_profile($cs_profile_id, $fname, $lname, $address, $city, $state, $zip, $country, $cardno, $cardexp, $testmode = 1){
		if($testmode)
			$mode = 'testMode';
		else
			$model = 'liveMode';
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".
		"<paymentProfile>".
		"<billTo>".
		"<firstName>". $fname ."</firstName>".
		"<lastName>". $lname ."</lastName>".
		"<address>". $address ."</address>".
		"<city>". $city ."</city>".
		"<state>". $state ."</state>".
		"<zip>". $zip ."</zip>".
		"<country>". $country ."</country>".
		"</billTo>".
		"<payment>".
		"<creditCard>".
		"<cardNumber>". $cardno ."</cardNumber>".
		"<expirationDate>". $cardexp ."</expirationDate>". // required format for API is YYYY-MM
		"</creditCard>".
		"</payment>".
		"</paymentProfile>".
		"<validationMode>". $mode ."</validationMode>". // or testMode
		"</createCustomerPaymentProfileRequest>";
		//echo $content;
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		return $parsedresponse;
	}
	public function profile_create($cs_id,$email){
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
		"<profile>".
		"<merchantCustomerId>".$cs_id."</merchantCustomerId>". // Your own identifier for the customer.
		"<description></description>".
		"<email>" . $email . "</email>".
		"</profile>".
		"</createCustomerProfileRequest>";
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		return $parsedresponse;
	}
	public function transaction_create($ammount, $cs_profile_id, $cs_payment_id, $invoice){
		$content =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
			"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
			"<transaction>".
			"<profileTransAuthOnly>".
			"<amount>" . str_replace('$', '', $ammount). "</amount>". // should include tax, shipping, and everything.
			"<lineItems>".
				"<itemId>000000001</itemId>".
				"<name>Service Fee</name>".
				"<description>Service Fee collected by AgentBridge for Referral {clientname}</description>".
				"<quantity>1</quantity>".
				"<unitPrice>".str_replace('$', '', $ammount)."</unitPrice>".
				"<taxable>false</taxable>".
			"</lineItems>".
			"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".
			"<customerPaymentProfileId>" . $cs_payment_id . "</customerPaymentProfileId>".
			"<customerShippingAddressId>" . 0 . "</customerShippingAddressId>".
			"<order>".
				"<invoiceNumber>".$invoice."</invoiceNumber>".
				"<description>Service Fee collected by AgentBridge for Referral {clientname}</description>".
			"</order>".
			"</profileTransAuthOnly>".
			"</transaction>".
			"</createCustomerProfileTransactionRequest>";
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		return $parsedresponse;
	}
	private function send_xml_request($content){
		return $this->send_request_via_fsockopen($content);
	}
	private function send_request_via_fsockopen($content){
		$posturl = "ssl://" . $this->apihost;
		$header = "Host: $this->host\r\n";
		$header .= "User-Agent: PHP Script\r\n";
		$header .= "Content-Type: text/xml\r\n";
		$header .= "Content-Length: ".strlen($content)."\r\n";
		$header .= "Connection: close\r\n\r\n";
		$fp = fsockopen($posturl, 443, $errno, $errstr, 30);
		if (!$fp)
		{
			$body = false;
		}
		else
		{
			//error_reporting(E_ERROR);
			fputs($fp, "POST $this->apipath  HTTP/1.1\r\n");
			fputs($fp, $header.$content);
			fwrite($fp, $out);
			$response = "";
			while (!feof($fp))
			{
				$response = $response . fgets($fp, 128);
			}
			fclose($fp);
			//error_reporting(E_ALL ^ E_NOTICE);
			$len = strlen($response);
			$bodypos = strpos($response, "\r\n\r\n");
			if ($bodypos <= 0)
			{
				$bodypos = strpos($response, "\n\n");
			}
			while ($bodypos < $len && $response[$bodypos] != '<')
			{
				$bodypos++;
			}
			$body = substr($response, $bodypos);
		}
		return $body;
	}
	private function parse_api_response($content){
		$parsedresponse = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOWARNING);
		return $parsedresponse;
	}
	private function MerchantAuthenticationBlock() {
		return
		"<merchantAuthentication>".
		"<name>" . $this->login_name . "</name>".
		"<transactionKey>" . $this->transaction_key . "</transactionKey>".
		"</merchantAuthentication>";
	}
	
}
?>
<?php
class HelloSign{
		public $baseUrl;
		public $accountId;
		public $header;
		public $templateId;
		public $emailSubject;
		public $emailBlurb;
		public $envelopeId;
		function login(){
			// Input your info here:
			$integratorKey = 'KEYD-657a870f-4405-4d93-bf80-0af455ff6b7e';
			$email = 'redler@agentbridge.com';
			$password = '&c%#lBVeT^r9hr';
			$name = "AgentBridge LLC";
			// Note: Login to the Console and create a template first, then copy its template Id into the following string.  Make sure
			// your template has at least one template role defined, which you will reference below in the body of your request.
			$this->templateId = "04894A0B-5B33-4BD8-BC37-6E592ECE94F7";
			// construct the authentication header:
			$this->header = "<DocuSignCredentials><Username>" . $email . "</Username><Password>" . $password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";
			/////////////////////////////////////////////////////////////////////////////////////////////////
			// STEP 1 - Login (to retrieve baseUrl and accountId)
			/////////////////////////////////////////////////////////////////////////////////////////////////
			$url = "https://na2.docusign.net/restapi/v2/login_information";
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $this->header"));
			$json_response = curl_exec($curl);
			$response = json_decode($json_response, true);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$this->accountId = $response["loginAccounts"][0]["accountId"];
			$this->baseUrl = $response["loginAccounts"][0]["baseUrl"];
			curl_close($curl);
			$this->emailSubject = "Please sign this form";
			$this->emailBlurb   = "You can sign the form through the button below";
			return $status;
		}
		function createEnvelope($user1, $user2, $filename){
			$data = array(
					"accountId"     => $this->accountId,
					"emailSubject"  => $this->emailSubject,
					"emailBlurb"    => $this->emailBlurb,
					"templateId"    => $this->templateId,
					"enableWetSign" => false,
					"templateRoles"	=> array(
											array(
												"email"        => $user1->email,
												"name"         => stripslashes_all($user1->name),
												"clientUserId" => $user1->id,
												"roleName"     => "agent_a"
											),
											array(
												"email"        => $user2->email,
												"name"         => stripslashes_all($user2->name),
												"clientUserId" => $user2->id,
												"roleName"     => "agent_b"
											)
										),
					"notification"		=> array(
							"expirations" => array(
												"expirationEnabled"	=> true,
												"expirationAfter"	=> "30",
												"expirationWarn"	=> "5"
											)
										),
					"documents"		=> array(
											array(
												"documentId"	=> "1",
												"name"			=> "Referral Agreement"
											)
										),
					"status"		=> "sent"
				);
	//	print_r($data); die();
			$file_contents = file_get_contents("referrals/$filename.pdf");
			$data_string = json_encode($data);
			$requestBody = "\r\n"
			."\r\n"
			."--myboundary\r\n"
			."Content-Type: application/json\r\n"
			."Content-Disposition: form-data\r\n"
			."\r\n"
			."$data_string\r\n"
			."--myboundary\r\n"
			."Content-Type:application/pdf\r\n"
			."Content-Disposition: file; filename=\”document.pdf\"; documentid=1 \r\n"
			."\r\n"
			."$file_contents\r\n"
			."--myboundary--\r\n"
			."\r\n";
			$curl = curl_init($this->baseUrl . "/envelopes/" );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: multipart/form-data;boundary=myboundary',
			'Content-Length: ' . strlen($requestBody),
			"X-DocuSign-Authentication: $this->header" )
			);
			$json_response = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$response = json_decode($json_response, true);
		//	print_r($response); die();
			$this->envelopeId = $response["envelopeId"];
			curl_close($curl);
			return $status;
		}
		function modifyRFee($val1){
			$dbug = '';
			$curl = curl_init($this->baseUrl . "/envelopes/".$this->envelopeId."/recipients?include_tabs=true");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"X-DocuSign-Authentication: $this->header" )
			);
			$temp = $json_response = curl_exec($curl);
			$dbug.= $temp;
			$decoded = json_decode($json_response);
			$tab = $decoded->signers[0]->tabs->textTabs[0];
			$tab->value= $val1;
			$tab->locked = 'true';
			$data = array("textTabs" => array($tab));
			$data_string = json_encode($data);
			curl_close($curl);
			$curl = curl_init($this->baseUrl . "/envelopes/".$this->envelopeId."/recipients/".$decoded->signers[0]->tabs->textTabs[0]->recipientId."/tabs/" );
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			"X-DocuSign-Authentication: $this->header" )
			);
			curl_exec($curl);
			curl_close($curl);
			$tab = $decoded->signers[1]->tabs->textTabs[0];
			$tab->value= $val1;
			$tab->locked = 'true';
			$data = array("textTabs" => array($tab));
			$data_string = json_encode($data);
			$curl = curl_init($this->baseUrl . "/envelopes/".$this->envelopeId."/recipients/".$decoded->signers[1]->tabs->textTabs[0]->recipientId."/tabs/" );
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			"X-DocuSign-Authentication: $this->header" )
			);
			$json_response = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			return $status.'/'.$json_response.'/'.$val1.'/'.$data_string.'/'.$dbug;
		}
		function sendEmail(){
			$data = array(
					"status"		=> "sent"
				);
			$data_string = json_encode($data);
			$curl = curl_init($this->baseUrl . "/envelopes/".$this->envelopeId );
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			"X-DocuSign-Authentication: $this->header" )
			);
			$json_response = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			return $status;
		}
		function getDocument(){
			$curl = curl_init($this->baseUrl . "/envelopes/" . $this->envelopeId . "/documents" );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $this->header" ));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			$json_response = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			if ( $status != 200 ) {
				echo "error calling webservice, status is:" . $status;
				exit(-1);
			}
			$response = json_decode($json_response, true);
			curl_close($curl);
			/////////////////////////////////////////////////////////////////////////////////////////////////
			// STEP 3 - Download the envelope's documents
			/////////////////////////////////////////////////////////////////////////////////////////////////
			$document = $response["envelopeDocuments"][0];
				$docUri = $document["uri"];
				$curl = curl_init($this->baseUrl . $docUri );
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array( "X-DocuSign-Authentication: $this->header" ));
				$data = curl_exec($curl);
				$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
				if ( $status != 200 ) {
					echo "error calling webservice, status is:" . $status;
					exit(-1);
				}
				$filename = JFactory::getUser()->id . "-" . $document["name"] .'-' .microtime(true);
				file_put_contents("referrals/".$filename.".pdf", $data);
				curl_close($curl);
			return $filename;
		}
		function getEmbeddedSigningLink($refid, $otheruser, $user){
			$data = array(
				"authenticationMethod" => 'None',
				"email"                => trim(JFactory::getUser()->email),
				"returnUrl"            => JUri::base()."index.php?option=com_activitylog&task=signedbyuser&id=$refid&other=$otheruser&signed=$user",
				"userName"             => stripslashes_all(trim(JFactory::getUser()->name)),
				"clientUserId"		   => JFactory::getUser()->id
			);
			$data_string = json_encode($data);
			$curl = curl_init($this->baseUrl . "/envelopes/".$this->envelopeId."/views/recipient" );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			"X-DocuSign-Authentication: $this->header" )
			);
			$json_response = curl_exec($curl);
			$url = json_decode($json_response)->url;
			//print_r($json_response); die();
			//return $json_response;
			#echo $data_string;
			if(isset($url))
				return $url;
			else
				#console.log(url);
				return 'https://docusign.com';
				echo "<pre>";
				return json_decode($json_response)->url;
		}
		function getDocuments(){
			$curl = curl_init($this->baseUrl . "/envelopes/" . $this->envelopeId . "/documents" );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"X-DocuSign-Authentication: $this->header" )
			);
			$json_response = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			/*if ( $status != 200 ) {
				echo "error calling webservice, status is:" . $status;
				exit(-1);
			}*/
			$response = json_decode($json_response, true);
			curl_close($curl);
			return $json_response;
		}
		function getEnvelope(){
			return $this->envelopeId;
		}
		function setEnvelope($val){
			$this->envelopeId = $val;
		}
	}
?>