<?php  include('_dbconn.php');
    
   $con = $mysqli = new mysqli("localhost", $username, $password, $database);

   $mysqli->set_charset('utf8');

	

	$user_id = $_GET["user_id"];
	$limit_one = $_GET["limit_one"];

	$query = "SELECT email FROM tbl_users WHERE id='$user_id'";

	$result = $mysqli->query($query) or die($mysqli->error);

	$num = $result->num_rows;

	while ($r = $result->fetch_assoc())
		{
			$user_email = $r['email'];
		}

		$query = "SELECT at.*, 
						aat.label, 
						other.name, 
						images.image AS other_image, 
						images2.image,  
						tbl_referral.refcontract_filename, 
						b.name AS buyer_name, 
						client.name AS client_name,  
						req_pocket.listing_id AS req_pocket_id,
						req_pocket.zip AS req_pocket_zip,
						req_pocket.user_id AS req_pocket_userId,
						req_pocket.setting AS req_pocket_setting, 
						req_pocket.property_name AS req_pocket_name, 
						req_pocket.closed AS req_pocket_closed, 					
						pocket.listing_id AS pocket_id,
						pocket.user_id AS pocket_userId,
						pocket.zip AS pocket_zip,
						pocket.setting AS pops_setting, 
						pocket.property_name AS pocket_name, 
						pocket.closed AS pocket_closed, 

						req_access.permission AS req_net_status, 
						req_access_match.permission AS req_acc_status,						
						refstat_up.edited_by, 
						perm_sett.selected_permission, 
						`referral_table`.`referral_id`, 
						tbl_referral.referral_fee 
						FROM tbl_activities at ";
		$query .= "LEFT JOIN tbl_users other on other.id = at.other_user_id ";
		$query .= "LEFT JOIN tbl_user_registration images on images.email = other.email ";
		$query .= "LEFT JOIN tbl_user_registration images2 on images2.email ='$user_email' ";
		$query .= "LEFT JOIN tbl_activity_type aat on aat.pkId = at.activity_type ";
		$query .= "LEFT JOIN tbl_buyer b on b.buyer_id = at.buyer_id ";
		$query .= "LEFT JOIN `tbl_referral_status_update` AS `referral_table` ON `referral_table`.`update_id` = at.`activity_id`
		    	   LEFT JOIN `tbl_referral` ON `tbl_referral`.`referral_id` = `referral_table`.`referral_id` ";
		$query .= "LEFT JOIN tbl_buyer client ON client.buyer_id = `tbl_referral`.`client_id` ";
		$query .= "LEFT JOIN tbl_referral_status_update refstat_up ON refstat_up.referral_id = `tbl_referral`.`referral_id` AND at.activity_id = refstat_up.update_id ";
		$query .= "LEFT JOIN tbl_request_access req_access ON req_access.pkId = at.activity_id ";	
		$query .= "LEFT JOIN tbl_request_access req_access_match ON req_access_match.property_id = at.activity_id AND req_access_match.user_b =".$user_id." ";		
		$query .= "LEFT JOIN tbl_pocket_listing req_pocket ON req_pocket.listing_id = req_access.property_id "; // for act 30 and 6
		$query .= "LEFT JOIN tbl_pocket_listing pocket ON pocket.listing_id = at.activity_id ";
		//$query .= "LEFT JOIN tbl_pocket_listing req_access ON req_access.property_id = pocket.`listing_id` ";
		$query .= "LEFT JOIN tbl_request_network req_net ON req_net.pk_id = at.`activity_id` ";
		$query .= "LEFT JOIN tbl_permission_setting perm_sett ON req_pocket.listing_id = perm_sett.`listing_id` ";
		//$query .="LEFT JOIN `tbl_referral_status_update` AS `status_referral_table` ON `status_referral_table`.`referral_id` = `referral_table`.`referral_id` ";
		$query .= "WHERE at.user_id='$user_id'";
		$query .= "AND ((at.activity_type = 23 AND b.home_buyer = 1 ) OR at.activity_type = 8 OR at.activity_type = 30 OR at.activity_type = 25 OR at.activity_type = 15 OR at.activity_type = 11 OR at.activity_type = 6 ) ";
		$query .= "ORDER BY at.date DESC limit ".$limit_one.",6";


$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Activities Found", 'data'=>array());
}
else {
	$rows = array();
	$rows_u = array();
	while ($r = $result->fetch_assoc())
	{
		if(key($r)=='date'){
			
			
		}	

		$rows[] = $r;
	}
	date_default_timezone_set("America/Los_Angeles");
	$pl_userId="";
	$pl_setting="";
	$pl_id="";
	foreach ($rows as $key => $value) {
		$this_line=$value;
		foreach ($value as $key2 => $value2) {
			if($key2=='activity_type'){
				if($value2=="25"){
					$please = "popmatch";
				}
			}
			if($key2=='date'){
				$this_line['date']=date("m/d/Y h:i:s A",strtotime($value2))." PST";
			//	$this_line['client_name']="Sample";
			//	$this_line['referral_status']="Sample";
			//	$this_line['ref_status']="Sample";
			}
			if($key2=='name'){
				$this_line['name']=stripslashes($value2);
			//	$this_line['client_name']="Sample";
			//	$this_line['referral_status']="Sample";
			//	$this_line['ref_status']="Sample";
			}
			if($key2=='client_name'){
				$this_line['client_name']=stripslashes($value2);
			//	$this_line['client_name']="Sample";
			//	$this_line['referral_status']="Sample";
			//	$this_line['ref_status']="Sample";
			}
			if($key2=="pocket_id"){
				$pl_id=$value2;
			}
			if($key2=="pocket_userId"){
				$pl_userId=$value2;
			}
			if($key2=="other_user_id"){
				$other_user_id=$value2;
			}
			if($key2=="pops_setting"){
				$pl_setting=$value2;
			}
			if($key2 == 'selected_permission'){
                $this_line['req_access_per']= "0";
                    if($user_id == $pl_userId){
                        $this_line['selected_permission'] = "1";
                    } else {
                        if($pl_userId && $this_line['pops_setting'] != 1){
                               
                                if( $this_line['setting'] == 1) {
                                    $this_line['selected_permission'] = "1";    
                                    $this_line['req_access_per'] = "1";                                                             
                                } else {                                    
                                    if ($this_line['req_acc_status']=="1"){
                                        $this_line['selected_permission'] = "1";
                                        $this_line['req_access_per'] = "1";
                                    } else if ($this_line['req_acc_status']=="0"){
                                        $this_line['req_access_per'] = "pending_private";
                                    } else if ($this_line['req_acc_status']=="2"){
                                        $this_line['req_access_per'] = "denied_private";
                                    } else {
                                        $this_line['req_access_per'] = "request_private";
                                    }
                                }

                        } else if($this_line['pops_setting'] == 1) {
                          $this_line['req_access_per'] = "1";  
                          $this_line['selected_permission'] = "1";
                        }

                    }
            }
			/*if($key2 == 'selected_permission' ){
				$this_line['req_access_per']= "0";
				$this_line['selected_permission'] = "0";
				if(!$this_line['req_net_status']){
					
					if($pl_userId == $user_id){
						$this_line['selected_permission'] = "1";
						$this_line['req_access_per']= "1";

					} else {
						if($pl_id){


							
							$query_allowed = "SELECT user_b FROM tbl_request_access WHERE property_id =".$pl_id." AND permission = 1";

                            $result_allowed = $mysqli->query($query_allowed) or die($mysqli->error);

                            $allowed_array = array();

                            while( $row = $result_allowed->fetch_assoc()){
                                $allowed_array[] = $row['user_b']; // Inside while loop
                            }

                            $query_requests = "SELECT user_b FROM tbl_request_access WHERE property_id =".$pl_id." AND permission = 0";

                            $result_requests = $mysqli->query($query_requests) or die($mysqli->error);

                            $requests_array = array();

                            while( $row = $result_requests->fetch_assoc()){
                                $requests_array[] = $row['user_b']; // Inside while loop
                            }


                            $query_declined = "SELECT user_b FROM tbl_request_access WHERE property_id =".$pl_id." AND permission = 2";

                            $result_declined = $mysqli->query($query_declined) or die($mysqli->error);

                            $declined_array = array();

                            while( $row = $result_declined->fetch_assoc()){
                                $declined_array[] = $row['user_b']; // Inside while loop
                            }
                            

                            if( $pl_setting == 1) {
                                $this_line['selected_permission'] = "1";    
                                $this_line['req_access_per'] = "1";                                                             
                            } else {                                    
                                if (in_array($user_id, $allowed_array)){
                                    $this_line['selected_permission'] = "1";
                                    $this_line['req_access_per'] = "1";
                                } else if (in_array($user_id, $requests_array)){
                                    $this_line['req_access_per'] = "pending_private";
                                } else if (in_array($user_id, $declined_array)){
                                    $this_line['req_access_per'] = "denied_private";
                                } else {
                                    $this_line['req_access_per'] = "request_private";
                                }

                            }

                           // if($other_user_id=="2885"){
                            $mailSender =& JFactory::getMailer();
						    $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
						    $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
						    $mailSender ->setSubject( "Allowed" );
						    $mailSender ->isHTML(  true );
						    $mailSender ->setBody( print_r($allowed_array,true));
						    $mailSender ->Send(); 
                           // }

						} else {
							$this_line['selected_permission'] = "0";
						}

					}
				}
					
			}*/

			if($key2=='referral_id'){

				$this_line['ref_status'] = "none";

				$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
				JModelLegacy::addIncludePath( $propmodel );
				$modelPropertyListing =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

				/*$model = &$this->getModel('PropertyListing');
				$db    =  &JFactory::getDbo();

				$query = "SELECT status FROM `tbl_referral_status_update`
				          WHERE `tbl_referral_status_update`.`referral_id` = '".$id."' 
				          AND `tbl_referral_status_update`.`referral_id`='".$user_id."'";

				$db->setQuery((string)$query);
				$refsstat = $db->loadObjectList();*/

				$query3 = "SELECT status
			    FROM `tbl_referral_status_update`			    
			    WHERE `tbl_referral_status_update`.`referral_id` = '".$value2."' AND `tbl_referral_status_update`.status != 0 ORDER BY `tbl_referral_status_update`.created_date DESC
				LIMIT 1;";

				$result3 = $mysqli->query($query3) or die($mysqli->error);
				
				while ($r = $result3->fetch_assoc())
				{							
					$this_line['ref_status'] = $r['status'];
				}

				/*$query2 = "SELECT `tbl_activities`.`pkId` AS `activities_id`,
			    `tbl_activities`.`activity_type`,
			    `tbl_activities`.`user_id`,
			    `tbl_activities`.`date`,
			    `tbl_activities`.`buyer_id`,
			    `tbl_activities`.`other_user_id`,
			    `tbl_activities`.`activity_id`
			    FROM `tbl_activities`
			    
			    WHERE `tbl_activities`.`activity_id` = '".$value2."' AND `tbl_activities`.`user_id` = '$user_id' AND `tbl_activities`.`activity_type` = '21'

			  	ORDER BY tbl_activities.date DESC

			  	$result2 = $mysqli->query($query2) or die($mysqli->error);

				$this_line['referral_status'] = $result2->num_rows;
			    ";
			    $query3 = "SELECT o_signed, r_signed
			    FROM `tbl_referral_status_update`			    
			    WHERE `tbl_referral_status_update`.`referral_id` = '".$value2."' AND `tbl_activities`.`user_id` = '$user_id' AND `tbl_referral_status_update`.status = 0 ORDER BY `tbl_referral_status_update`.created_date DESC
				LIMIT 1;";

				$result3 = $mysqli->query($query3) or die($mysqli->error);
				
				while ($r = $result3->fetch_assoc())
				{							
					$this_line['ref_status'] = $r['status'];
				}*/


				$db    =  &JFactory::getDbo();

				$query = "SELECT o_signed, r_signed
			    FROM `tbl_referral_status_update`			    
			    WHERE `tbl_referral_status_update`.`referral_id` = '".$value2."' AND `tbl_referral_status_update`.`created_by` = '".$user_id."' AND `tbl_referral_status_update`.status = 0 ORDER BY `tbl_referral_status_update`.created_date DESC
				LIMIT 1;";

				$db->setQuery((string)$query);
				$refsstat = $db->loadObjectList();
				$isSigned=0;
				if($refsstat[0]->o_signed==1 || $refsstat[0]->o_signed==1){
					$isSigned=1;
				}

				$this_line['referral_status'] = $isSigned;
			}
		}
		$rows_u[]=$this_line;
	}
	$response = array('status'=>1, 'message'=>"Found Activities", 'data'=>$rows_u);
}
$mysqli->close();

echo json_encode($response);

?>