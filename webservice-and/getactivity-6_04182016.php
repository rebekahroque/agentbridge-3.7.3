<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $user_id = $_GET["user_id"];
    $activities_id = $_GET["activities_id"];

    $query = "SELECT `tbl_activities`.`pkId` AS `activities_id`,
    `tbl_activities`.`activity_type`,
    `tbl_activities`.`user_id`,
    `tbl_activities`.`date`,
    `tbl_activities`.`buyer_id`,
    `tbl_activities`.`other_user_id`,
    `activity_user`.`name` AS `user_name`,
    `tbl_user_registration`.`image`,
    `tbl_request_access`.`permission`,
    `tbl_pocket_listing`.`listing_id`,
    `tbl_pocket_listing`.`property_name`,
    `tbl_request_access`.`pkId` AS `access_id`
    FROM `tbl_activities`
    
    LEFT JOIN `tbl_users` AS `activity_user` ON
    CASE `tbl_activities`.`user_id`
    WHEN '$user_id'
    THEN
    `activity_user`.`id` = `tbl_activities`.`other_user_id`
    ELSE
    `activity_user`.`id` = `tbl_activities`.`user_id`
    END
    LEFT JOIN `tbl_user_registration` ON `tbl_user_registration`.`email` = `activity_user`.`email`
    
    
    LEFT JOIN `tbl_request_access` ON `tbl_request_access`.`pkId` = `tbl_activities`.`activity_id` AND `tbl_request_access`.`user_a` = `tbl_activities`.`user_id` AND `tbl_request_access`.`user_b` = `tbl_activities`.`other_user_id`
    
    LEFT JOIN `tbl_pocket_listing` ON `tbl_pocket_listing`.`listing_id` = `tbl_request_access`.`property_id`
    
    WHERE `tbl_activities`.`pkid` = '$activities_id' AND (`tbl_activities`.`user_id` = '$user_id' OR `tbl_activities`.`other_user_id` = '$user_id') AND `tbl_activities`.`activity_type` = '6'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Activity Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Activity", 'data'=>$rows);
}

echo json_encode($response);

?>