<?php
    require 'PHPMailer-master/PHPMailerAutoload.php';
    
    $emailTo = $_POST["email"];
    $subject = $_POST["subject"];
    $bodyMessage = $_POST["message"];
    
    $uploaddir = 'pdf_uploads/';
    $file = basename($_FILES['userfile']['name']);
    $uploadfile = $uploaddir . $file;
    
    
    $response = array();
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        $file = basename($_FILES['paymentfile']['name']);
        $uploadfilePayment = $uploaddir . $file;
        
        if (move_uploaded_file($_FILES['paymentfile']['tmp_name'], $uploadfilePayment)) {
            $mail = new PHPMailer;
            
            $mail->isSendmail();
            
            $mail->From = 'no-reply@agentbridge.com';
            $mail->FromName = 'AgentBridge';
            $mail->addAddress($emailTo);  // Add a recipient
            $mail->addBCC('rebekah.roque@keydiscoveryinc.com');
            
            $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
            $mail->addAttachment($uploadfile);         // Add attachments
            $mail->addAttachment($uploadfilePayment);         // Add attachments
            
            $mail->Subject = $subject;
            $mail->Body    = $bodyMessage;
            
            if(!$mail->send()) {
                $response = array('status'=>0, 'message'=>"Error Occurred in Sending Email".$mail->ErrorInfo);
                exit;
            }
            
            $response = array('status'=>1, 'message'=>"Successful in Sending email");
        }
        else {
            $response = array('status'=>0, 'message'=>"Error Occurred in Uploading Payment PDF");
        }
       
    }
    else {
        $response = array('status'=>0, 'message'=>"Error Occurred in Uploading PDF");
    }
    
    
    echo json_encode($response);
   