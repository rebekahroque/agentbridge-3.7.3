<?php include('_dbconn.php');
    


//$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);

$con = $mysqli = new mysqli("localhost", $username, $password, $database);

$mysqli->set_charset('utf8');



$user_id = $_GET["user_id"];

$langValues = $propertylist_model->checkLangFiles();
$userDetails = $userprof_model->get_user_registration($user_id);

$now_date = date("Y-m-d",time());

$query = "SELECT `tbl_pocket_listing`.`sold`, 
                 `tbl_pocket_listing`.`listing_id`, 
                 `tbl_pocket_listing`.`property_type` AS `type_property_type`, 
                 `tbl_pocket_listing`.`property_name`, 
                 `tbl_pocket_listing`.`currency`, 
                 `tbl_country_currency`.`symbol`,
                 `tbl_property_type`.`type_name`, 
                 `tbl_pocket_listing`.`sub_type`, 
                 `tbl_property_sub_type`.`name` AS `sub_type_name`, 
                 `tbl_property_price`.`price_type`, 
                 `tbl_property_price`.`price1`, 
                 `tbl_property_price`.`price2`, 
                 `tbl_property_price`.`disclose`, 
                 `tbl_pocket_listing`.`expiry`, 
                 `tbl_pocket_listing`.`user_id`, 
                 `tbl_users`.`name`, 
                 `tbl_pocket_listing`.`city`, 
                 `tbl_pocket_listing`.`zip`, 
                 `tbl_pocket_listing`.`state`, 
                 `tbl_pocket_listing`.`bedroom`, 
                 `tbl_pocket_listing`.`bathroom`,
                 `tbl_pocket_listing`.`unit_sqft`, 
                 `tbl_pocket_listing`.`view`, 
                 `tbl_pocket_listing`.`style`, 
                 `tbl_pocket_listing`.`year_built`, 
                 `tbl_pocket_listing`.`pool_spa`, 
                 `tbl_pocket_listing`.`condition`, 
                 `tbl_pocket_listing`.`garage`, 
                 `tbl_pocket_listing`.`units`, 
                 `tbl_pocket_listing`.`cap_rate`, 
                 `tbl_pocket_listing`.`grm`, 
                 `tbl_pocket_listing`.`occupancy`, 
                 `tbl_pocket_listing`.`type`, 
                 `tbl_pocket_listing`.`listing_class`, 
                 `tbl_pocket_listing`.`parking_ratio`, 
                 `tbl_pocket_listing`.`ceiling_height`, 
                 `tbl_pocket_listing`.`stories`, 
                 `tbl_pocket_listing`.`room_count`, 
                 `tbl_pocket_listing`.`type_lease`, 
                 `tbl_pocket_listing`.`type_lease2`, 
                 `tbl_pocket_listing`.`available_sqft`, 
                 `tbl_pocket_listing`.`lot_sqft`, 
                 `tbl_pocket_listing`.`lot_size`, 
                 `tbl_pocket_listing`.`bldg_sqft`,
                 `tbl_pocket_listing`.`term`, 
                 `tbl_pocket_listing`.`furnished`, 
                 `tbl_pocket_listing`.`pet`, 
                 `tbl_pocket_listing`.`possession`, 
                 `tbl_pocket_listing`.`zoned`, 
                 `tbl_pocket_listing`.`bldg_type`, 
                 `tbl_pocket_listing`.`features1`, 
                 `tbl_pocket_listing`.`features2`, 
                 `tbl_pocket_listing`.`features3`, 
                 `tbl_pocket_listing`.`setting`, 
                 `tbl_pocket_listing`.`description` AS `desc`, 
                 `tbl_pocket_listing`.`closed`, 
                 `tbl_pocket_listing`.`date_created`, 
                 `tbl_pocket_listing`.`date_expired`,
                 `tbl_property_price`.`pocket_id`,
                 `pocket_images`.`images`

FROM `tbl_pocket_listing`
LEFT JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
LEFT JOIN `tbl_country_currency` ON `tbl_country_currency`.`currency` = `tbl_pocket_listing`.`currency`
LEFT JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
LEFT JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`
LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
           FROM `tbl_pocket_images`
           GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
WHERE `tbl_pocket_listing`.`user_id`='$user_id' GROUP BY `tbl_pocket_listing`.`listing_id`
ORDER BY `tbl_pocket_listing`.`sold` DESC,`tbl_pocket_listing`.`date_created` ASC 
 ";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;


$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Properties Found", 'data'=>array());
}
else {
	$rows = array();
	$rows_u = array();
    while ($r = $result->fetch_assoc())
    {
        if(key($r)=='date'){
            
            
        }   

        $rows[] = $r;
    }
    foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {

               if($key2=='listing_id'){
                 $query2 = "SELECT `tbl_buyer`.`buyer_id`, `tbl_buyer_saved_listing`.`listing_id`, `tbl_buyer`.`name`
                  FROM `tbl_buyer`
                  LEFT JOIN `tbl_buyer_needs` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_needs`.`buyer_id`
                  LEFT JOIN `tbl_buyer_saved_listing` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_saved_listing`.`buyer_id`
                  LEFT JOIN (SELECT `tbl_buyer_saved_listing`.`buyer_id`, COUNT(*) AS 'saved_pops' FROM `tbl_buyer_saved_listing`  GROUP BY `tbl_buyer_saved_listing`.`buyer_id`) AS `saved_list` ON `saved_list`.`buyer_id` = `tbl_buyer`.`buyer_id`
                  LEFT JOIN (SELECT `tbl_buyer_new_listing`.`buyer_id`, COUNT(*) AS 'new_pops' FROM `tbl_buyer_new_listing` GROUP BY `tbl_buyer_new_listing`.`buyer_id`) AS `new_list` ON `new_list`.`buyer_id` = `tbl_buyer`.`buyer_id`";

              //    $query.=" WHERE `tbl_buyer_saved_listing`.`listing_id`!='$listing_id'";
                  $query2.=" WHERE `tbl_buyer`.`agent_id`='$user_id' AND `tbl_buyer`.`buyer_type` <> '' AND `tbl_buyer_saved_listing`.`listing_id`='$value2'";

                  $query2.=" GROUP BY `tbl_buyer`.`buyer_id`";

                $result2 = $mysqli->query($query2) or die($mysqli->error);

                $num2 = $result2->num_rows;


                
                if($num2 == 0) {
                    $this_line['show_button'] = "true";
                    $this_line['buyer_name'] = "none";
                }
                else {                  
                    $this_line['show_button'] = "true";
                    $this_line['buyer_name'] = "false";                    
                }

            }

            if($key2=='name'){
                $this_line['name']=stripslashes($value2);
            }
            if($key2=='date_expired'){
                $now = time();
                $timedifference=strtotime($value2)-$now;
                $this_line['expiry']=strval(round($timedifference/(60*60*24)));
            }
           
        }



       // $ex_rates_con = $ex_rates->rates->{$this_line['currency']};
       // $ex_rates_can = $ex_rates->rates->USD;
        
       // if($this_line['currency']!="USD"){            
        //    $this_line['price1']=($this_line['price1'] / $ex_rates_con)*$ex_rates_can;
       //     if($this_line['price2']){                
       //          $this_line['price2']=($this_line['price2'] / $ex_rates_con)*$ex_rates_can;
       //     }
       // }

        //$items->currency = $userDetails->currency;
        //$items->symbol = $userDetails->symbol;

         if($user_id != $this_line['user_id']){
            $country_used = $userDetails->country;
            $sqftMeasurement_used= $userDetails->sqftMeasurement;
        } else {
            $country_used = $this_line['country'];
            $sqftMeasurement_used= $this_line['sqftMeasurement'];
        }


        $sqM = "";
        
        if($sqftMeasurement_used=="sq. meters"){
            $getMeasurement = $propertylist_model->getSqMeasureByCountry($this_line['country']);
            foreach ($getMeasurement['bldg'] as $key => $value) {
                if($this_line['bldg_sqft']){
                    if($this_line['bldg_sqft'] == $value[1]){
                        $this_line['bldg_sqft']=$value[0];
                    }
                }
                if($this_line['available_sqft']){
                    if($this_line['available_sqft'] == $value[1]){
                        $this_line['available_sqft']=$value[0];
                    }
                    
                }
                if($this_line['unit_sqft']){
                    if($this_line['unit_sqft'] == $value[1]){
                        $this_line['unit_sqft']=$value[0];
                    }
                    
                }
            }

            foreach ($getMeasurement['lot'] as $key => $value) {
                # code...
                if($this_line['lot_sqft']){
                    if($this_line['lot_sqft'] == $value[1]){
                        $this_line['lot_sqft']=$value[0];
                    }
                    
                }
                if($this_line['lot_size']){
                    if($this_line['lot_size'] == $value[1]){
                        $this_line['lot_size']=$value[0];
                    }
                    
                }
            }

            $sqM = "_M";
        }

        $pops_feats = $propertylist_model->getPOPsDetails($this_line,$sqM,$this_line['sub_type'],$langValues);

         foreach ($pops_feats['feats_array'] as $key => $value) {
            # code...
            $arr_value= explode(": ", $value);
            if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type" && $arr_value[0]!="Term" && $arr_value[0]!="Possession"){
                $pops_feats['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
            } else {
                $pops_feats['feats_array_edited'][]=$value;
            }
            
        }

        $this_line['initial_feats'] =$pops_feats['initial_feats'];
        $this_line['feats_array'] =implode(", ", $pops_feats['feats_array_edited']);

        $this_line['type_name'] = JText::_($this_line['type_name']);
        $this_line['sub_type_name'] = JText::_($this_line['sub_type_name']);
        $rows_r[]=$this_line;
        $rows_u = array_reverse($rows_r);
    }
    $response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$rows_u);
    $mysqli->close();
}

echo json_encode($response);

?>