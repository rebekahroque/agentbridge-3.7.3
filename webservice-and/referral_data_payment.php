<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $referral_id = $_GET["referral_id"];

    $query = "SELECT `tbl_referral`.`referral_id`, `tbl_referral`.`agent_a` AS `referring_user_id`, `A`.`name` AS `referring_name`, `A`.`email` AS `referring_email`, `A_P`.`street_address`, `A_P`.`city`, `A_P`.`zip`, `tbl_zones`.`zone_name` AS `state`, `tbl_countries`.`countries_name` AS `country`, `A_P`.`licence` AS `referring_agent_license`, `A_P`.`brokerage_license` AS `referring_brokerage_license`, `A_P`.`tax_id_num` AS `receiving_tax_id`, `A_B`.`broker_name` AS `referring_broker_name`, `A_M`.`value` AS `referring_mobile_number`, `tbl_referral`.`agent_b` AS `receiving_user_id`, `B`.`name` AS `receiving_name`, `B`.`email` AS `receiving_email`, `B_P`.`licence` AS `receiving_agent_license`, `B_P`.`brokerage_license` AS `receiving_brokerage_license`, `B_P`.`tax_id_num` AS `referring_tax_id`, `B_B`.`broker_name` AS `receiving_broker_name`, `B_M`.`value` AS `receiving_mobile_number`
    FROM `tbl_referral`
    LEFT JOIN `tbl_users` A ON `A`.`id` = `tbl_referral`.`agent_a`
    LEFT JOIN `tbl_user_registration` A_P ON `A_P`.`email` = `A`.`email`
    LEFT JOIN `tbl_zones` ON `tbl_zones`.`zone_id` = `A_P`.`state`
    LEFT JOIN `tbl_countries` ON `tbl_countries`.`countries_id` = `A_P`.`country`
    LEFT JOIN `tbl_broker` A_B ON `A_B`.`broker_id` = `A_P`.`brokerage`
    LEFT JOIN `tbl_user_mobile_numbers` A_M ON `A_M`.`user_id` = `A_P`.`user_id`
    LEFT JOIN `tbl_users` B ON `B`.`id` = `tbl_referral`.`agent_b`
    LEFT JOIN `tbl_user_registration` B_P ON `B_P`.`email` = `B`.`email`
    LEFT JOIN `tbl_broker` B_B ON `B_B`.`broker_id` = `B_P`.`brokerage`
    LEFT JOIN `tbl_user_mobile_numbers` B_M ON `B_M`.`user_id` = `B_P`.`user_id`
    WHERE `tbl_referral`.`referral_id` = '$referral_id'
    GROUP BY `tbl_referral`.`referral_id`";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Data Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows["values"] = $r;
        $rows["referring_agent_license"] = decrypt($r["referring_agent_license"]);
        $rows["referring_brokerage_license"] = decrypt($r["referring_brokerage_license"]);
        $rows["referring_tax_id"] = decrypt($r["referring_tax_id"]);
        $rows["receiving_agent_license"] = decrypt($r["receiving_agent_license"]);
        $rows["receiving_brokerage_license"] = decrypt($r["receiving_brokerage_license"]);
        
        $rows["receiving_tax_id"] = decrypt($r["receiving_tax_id"]);
	}
    
	
	$response = array('status'=>1, 'message'=>"Found Data", 'data'=>$rows);
}

echo json_encode($response);

    
    function decrypt($encrypted_text)
    {
        
        $key = 'password to (en/de)crypt';
        
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
        
        return $decrypted;
    }
?>