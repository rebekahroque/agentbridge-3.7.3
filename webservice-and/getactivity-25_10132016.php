<?php include('_dbconn.php');
    
    $con = $mysqli = new mysqli("localhost", $username, $password, $database);

    $mysqli->set_charset('utf8');



    $user_id = $_GET["user_id"];
    $activities_id = $_GET["activities_id"];

    $query = "SELECT `tbl_activities`.`pkId` AS `activities_id`, `tbl_activities`.`activity_type`, `tbl_activities`.`user_id`, `tbl_activities`.`activity_type`, `tbl_activities`.`date`, `tbl_activities`.`buyer_id`, `tbl_activities`.`other_user_id`, `tbl_pocket_listing`.`listing_id`, `tbl_pocket_listing`.`property_name`, `tbl_pocket_listing`.`setting`, `tbl_pocket_listing`.`user_id` AS `pops_user_id`, `user_data`.`name` AS `pops_user_name`, `tbl_buyer`.`name` AS `client_name`, `tbl_user_registration`.`image`, `other_user_data`.`name` AS `other_user_name`
    FROM `tbl_activities`
    LEFT JOIN `tbl_pocket_listing` ON `tbl_activities`.`activity_id` = `tbl_pocket_listing`.`listing_id`
    LEFT JOIN `tbl_users` AS `user_data` ON `user_data`.`id` = `tbl_pocket_listing`.`user_id`
    LEFT JOIN `tbl_buyer` ON `tbl_buyer`.`buyer_id` = `tbl_activities`.`buyer_id`
    LEFT JOIN `tbl_user_registration` ON `tbl_user_registration`.`email` = `user_data`.`email`
    LEFT JOIN `tbl_users` AS `other_user_data` ON `other_user_data`.`id` = `tbl_activities`.`other_user_id`
    WHERE `tbl_activities`.`pkid` = '$activities_id' AND `tbl_activities`.`user_id` = '$user_id' AND `tbl_activities`.`activity_type` = '25'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Activity Found", 'data'=>array());
}
else {
	$rows = array();    
    $rows_u = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='date'){
                $this_line['date']=date("m/d/Y h:i:s A",strtotime($value2))." PDT";
            }
            if($key2=='other_user_name'){
                $this_line['other_user_name']=stripslashes($value2);
            }
        }
        $rows_u[]=$this_line;
    }
	$response = array('status'=>1, 'message'=>"Found Activity", 'data'=>$rows_u);
}

echo json_encode($response);

?>