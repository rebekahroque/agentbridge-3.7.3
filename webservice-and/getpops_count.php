<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



$user_id = $_GET["user_id"];

$now_date = date("Y-m-d",time());

$query = "SELECT `tbl_pocket_listing`.*

FROM `tbl_pocket_listing`
LEFT JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
LEFT JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
LEFT JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`
LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
           FROM `tbl_pocket_images`
           GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
WHERE `tbl_pocket_listing`.`user_id`='$user_id'
ORDER BY `tbl_pocket_listing`.`listing_id` ASC 
 ";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;


$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Properties Found", 'data'=>array());
}
else {
	$rows = array();
	$rows_u = array();
    while ($r = $result->fetch_assoc())
    {
        if(key($r)=='date'){
            
            
        }   

        $rows[] = $r;
    }

    $response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>count($rows));
    $mysqli->close();
}

echo json_encode($response);

?>