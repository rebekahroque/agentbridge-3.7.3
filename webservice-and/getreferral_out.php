<?php  include('_dbconn.php');
    

    //$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);

  $con =  $mysqli = new mysqli("localhost", $username, $password, $database);

  $mysqli->set_charset('utf8');



$user_id = $_GET["user_id"];
$limit_one = $_GET["limit_one"];

$query = "SELECT `tbl_referral`.`referral_id`, `tbl_referral`.`agent_a`,`buyer_data`.`currency`, `buyer_data`.`symbol`, `tbl_referral`.`agent_b`, `user_data`.`email`, `tbl_users`.`name` AS `agent_name`, `user_data`.`city`, `user_data`.`state_code`, `user_data`.`state_name`, `user_data`.`countries_name`, `user_data`.`countries_iso_code_3`, `tbl_referral`.`client_id`, `tbl_referral`.`price_1`, `tbl_referral`.`price_2`, `tbl_referral`.`referral_fee`, `tbl_referral`.`status`, `tbl_referral`.`property_id`, `tbl_referral`.`date`, `tbl_referral`.`client_intention`, `tbl_referral`.`counter`, `tbl_referral`.`docusign_envelope`, `tbl_referral`.`reason`, `tbl_referral`.`countered`,`user_data`.`image`, `buyer_data`.`client_name`, `buyer_data`.`email` AS `client_email`, `buyer_data`.`number` AS `client_number`, `buyer_data`.`address_1` AS `client_address_1`, `buyer_data`.`address_2` AS `client_address_2`, `buyer_data`.`city` AS `client_city`, `buyer_data`.`zip` AS `client_zip`, `buyer_data`.`countries_name` AS `client_country_name`, `buyer_data`.`countries_iso_code_3` AS `client_country_code`, `buyer_data`.`zone_code` AS `client_state_code`, `buyer_data`.`zone_name` AS `client_state_name`
FROM `tbl_referral`
LEFT JOIN `tbl_users` ag_a ON `tbl_referral`.`agent_a` = ag_a.`id`
LEFT JOIN `tbl_users` ON `tbl_referral`.`agent_b` = `tbl_users`.`id`
    LEFT JOIN (
               SELECT `tbl_buyer`.`buyer_id`, `tbl_country_currency`.`symbol`, `tbl_buyer_address`.`currency`, `tbl_buyer`.`name` AS `client_name`, `tbl_buyer_email`.`email`, `tbl_buyer_mobile`.`number`, `tbl_buyer_address`.`address_1`, `tbl_buyer_address`.`address_2`, `tbl_buyer_address`.`city`, `tbl_buyer_address`.`zip`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_zones`.`zone_code`, `tbl_zones`.`zone_name`
               FROM `tbl_buyer`
               LEFT JOIN `tbl_buyer_email` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_email`.`buyer_id`
               LEFT JOIN `tbl_buyer_mobile` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_mobile`.`buyer_id`
               LEFT JOIN `tbl_buyer_address` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_address`.`buyer_id`
               LEFT JOIN `tbl_countries` ON `tbl_buyer_address`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_country_currency` ON `tbl_country_currency`.`currency` = `tbl_buyer_address`.`currency`
               LEFT JOIN `tbl_zones` ON `tbl_buyer_address`.`state` = `tbl_zones`.`zone_id` AND `tbl_buyer_address`.`country` = `tbl_zones`.`zone_country_id`
              ) AS `buyer_data` ON `tbl_referral`.`client_id` = `buyer_data`.`buyer_id`
LEFT JOIN (
			SELECT `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`email` , `tbl_user_registration`.`city`, `tbl_zones`.`zone_code` AS `state_code`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_user_registration`.`country` AS `country_id`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`image`
			FROM `tbl_user_registration`
			INNER JOIN `tbl_users` ON `tbl_user_registration`.`email` = `tbl_users`.`email`
			LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
           LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
			) AS `user_data` ON `tbl_referral`.`agent_b` = `user_data`.`user_id`
WHERE `tbl_referral`.`agent_a`='$user_id' AND `buyer_data`.`client_name` != ''";

if($limit_one) {
  $query .= " LIMIT ".$limit_one.",3";
}

$query .= " ORDER BY `tbl_referral`.referral_id DESC  ";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;



$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Referrals Found", 'data'=>array());
}
else {
	$rows = array();
	 $rows_u = array();

  while ($r = $result->fetch_assoc())
  {
    $rows[] = $r;
  }

  foreach ($rows as $key => $value) {
    $this_line=$value;
    foreach ($value as $key2 => $value2) {
        if($key2=='agent_name'){
          $this_line['agent_name']=stripslashes($value2);
        } 
        if($key2=='client_name'){
          $this_line['client_name']=stripslashes($value2);
        }  
        if($key2=='email'){
            $query4 = "SELECT image
              FROM `tbl_user_registration`
              
              WHERE `tbl_user_registration`.`email` = '".$value2."'";

            $result4 = $mysqli->query($query4) or die($mysqli->error);
            
            while ($r = $result4->fetch_assoc())
              {             
                $this_line['image'] = strpos($r['image'], JURI::base()) !== false ? str_replace("loads/", "loads/", $r['image']).'?'.microtime(true) :  JURI::base().'uploads/'.$r['image'].'?'.microtime(true);
              }
        }     
        if($key2=='referral_id'){

        $query3 = "SELECT status
          FROM `tbl_referral_status_update`
          
          WHERE `tbl_referral_status_update`.`referral_id` = '".$value2."'";

        $result3 = $mysqli->query($query3) or die($mysqli->error);
        
        while ($r = $result3->fetch_assoc())
          {             
            $this_line['ref_status'] = $r['status'];
          }

        $query2 = "SELECT `tbl_activities`.`pkId` AS `activities_id`,
          `tbl_activities`.`activity_type`,
          `tbl_activities`.`user_id`,
          `tbl_activities`.`date`,
          `tbl_activities`.`buyer_id`,
          `tbl_activities`.`other_user_id`,
          `tbl_activities`.`activity_id`
          FROM `tbl_activities`
          
          WHERE `tbl_activities`.`activity_id` = '".$value2."' AND `tbl_activities`.`user_id` = '$user_id' AND `tbl_activities`.`activity_type` = '21'

          ORDER BY tbl_activities.date DESC
          ";

        $result2 = $mysqli->query($query2) or die($mysqli->error);

        $this_line['referral_status'] = $result2->num_rows;
      }
      }
      $rows_u[]=$this_line;
    }

	$response = array('status'=>1, 'message'=>"Found Referrals", 'data'=>$rows_u);
  // $response = array('status'=>1, 'message'=>"Found Referrals", 'data'=>array());
}

$mysqli->close();
echo json_encode($response);

?>