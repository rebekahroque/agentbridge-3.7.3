<?php

// No direct access

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');

$session =& JFactory::getSession(); 
$user = & JFactory::getUser();
//$thisuserid=$session->get( 'userid' );

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select(array('*'));
$query->from('#__user_registration');
$query->where('email = "'.$user->email.'"');
$db->setQuery($query);
$users_tb = $db->loadObject();

$thisuserid=$users_tb->user_id;

$start_activated=date("m-d-Y, h:i:s A");
$session->set( 'start_activated', $start_activated );

$questions = array();

foreach($this->results as $result){

	$questions[$result->qid]['set1'] = $result->set1;
	$questions[$result->qid]['set2'] = $result->set2;
	$questions[$result->qid]['set3'] = $result->set3;

}

?>

<script type="text/javascript">
mixpanel.track("Set Security Questions");
</script>
						<form style="display:none"
							id="user_logout" 
							class="form-horizontal jqtransformdone"
							action="<?php echo JRoute::_("index.php?option=com_users&task=user.logout"); ?>"
							method="post">
							<a id='logout_link' href="javascript: void(0);" onclick="document.getElementById('user_logout').submit();">Logout</a>
							<?php echo JHtml::_('form.token');?></form>
<div class="banner-row" style="padding-bottom:40px; min-height:800px">
<div class="full-radius wide-container security-question" style="display: block; line-height:16px; color:#000000; text-align:left"><!-- start security question -->

	<div class="forms-head">
		<h3 style="font-size:24px; font-weight:bold; padding-bottom:30px">AgentBridge Account Setup</h3>
        <div class="user clear">
		<p style="font-size:16px">AgentBridge provides bank level security for your information. You, the agent, are always in control. <br/><br/>
      
		Please provide answers to a few security questions. This will ensure that only you can access your AgentBridge account.</p>
	</div>

<!--
	<form id="security_question" action="<?php echo $_SERVER['PHP_SELF']; ?>?option=com_securityquestion&uid=<?php echo $_SESSION['uid'] ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
-->
	<form id="security_question"  class="form-validate form-horizontal" enctype="multipart/form-data">
	
		<input type="hidden" value="<?php echo $_SESSION['uid'] ?>" id="uid" name="uid" />
		<input type="hidden" value="security_question" id="jform_form" name="jform[form]" />

		<div class="question">
			<div>
				<select style="width:400px" class="validate-question1 required" id="jform_question1" name="jform[question1]" aria-required="true" required="required">
					<?php
						foreach($questions as $key=>$question){
					?>
					<option value="<?php echo $key; ?>"><?php echo $question['set1'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
			
			<div>
				<p class="jform_question1 error_msg" style="display:none; font-size:11px">This field is required  <br /> <br /></p> 
			</div>		
		
			<div style="margin-top:5px;">
				<input 
					type="text" 
					size="30" 
					class="validate-answer1 required text-input" 
					autocomplete="off" 
					value="" 
					id="jform_answer1" 
					name="jform[answer1]"
				>
			</div>
			
			<div>
				<p class="jform_answer1 error_msg" style="display:none; font-size:11px"><br /> This field is required  <br /></p> 
			</div>
		</div>

		<div class="question">
			<div>
				<select style="width:400px"  class="validate-question1 required" id="jform_question2" name="jform[question2]" aria-required="true" required="required">
					<?php
						foreach($questions as $key=>$question){
					?>
					<option value="<?php echo $key; ?>"><?php echo $question['set2'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
			
			<div>
				<p class="jform_question2 error_msg" style="display:none; font-size:11px">This field is required  <br /> <br /></p> 
			</div>
			
			<div style="margin-top:5px;">
				<input 
					type="text" 
					size="30" 
					class="validate-answer2 required text-input" 
					autocomplete="off" 
					value="" 
					id="jform_answer2" 
					name="jform[answer2]" 
				>
			</div>
			<div>
				<p class="jform_answer2 error_msg" style="display:none; font-size:11px"><br /> This field is required  <br /></p> 
			</div>
		</div>

		<div class="question">
			<div>
				<select style="width:400px"  class="validate-question3 required" id="jform_question3" name="jform[question3]" aria-required="true" required="required">
					<?php
						foreach($questions as $key=>$question){
					?>
					<option value="<?php echo $key; ?>"><?php echo $question['set3'] ?></option>
					<?php
						}
					?>
				</select>
			</div>
			
			<div>
				<p class="jform_question3 error_msg" style="display:none; font-size:11px">This field is required  <br /> <br /></p> 
			</div>
			
			<div style="margin-top:5px;">
				<input 
					type="text" 
					size="30" 
					class="validate-answer3 required text-input" 
					autocomplete="off" 
					value="" 
					id="jform_answer3" 
					name="jform[answer3]" 
				>
				<div>
					<p class="jform_answer3 error_msg" style="display:none; font-size:11px"><br /> This field is required  <br /></p> 
				</div>
			</div>
		</div>

		<div class="login-form">
			<input id="continue" name="" value="Continue" style="padding-top:5px" type="submit" class="submit-btn"/>
			<input type="hidden" name="option" value="com_securityquestion" />
			<input type="hidden" name="task" value="set_security" />
			<!--input type="hidden" name="task" value="registration.register" /-->
			<?php echo JHtml::_('form.token');?>
		</div>

	</form>
    </div>

</div>
</div>

<script>
//Your timing variables in number of seconds
	 
	//total length of session in seconds
	var sessionLength = (2*60);
	//var sessionLength = 10;
	//time warning shown (10 = warning box shown 10 seconds before session starts)
	var warning = 10; 
	//time redirect forced (10 = redirect forced 10 seconds after session ends)    
	var forceRedirect = 0;
	 
	//time session started
	var pageRequestTime = new Date();
	 
	//session timeout length
	var timeoutLength = sessionLength*1000;
	 
	//set time for first warning, ten seconds before session expires
	var warningTime = timeoutLength - (warning*1000);
	 
	//force redirect to log in page length (session timeout plus 10 seconds)
	var forceRedirectLength = timeoutLength + (forceRedirect*1000);
	 
	//set number of seconds to count down from for countdown ticker
	var countdownTime = warning;
	 
	//warning dialog open; countdown underway
	var warningStarted = false;

	var teset_seconds=1;

	function CheckForSession() {

		console.log(teset_seconds++);
		//get time now
	    var timeNow = new Date();
	     
	    //event create countdown ticker variable declaration
	    var countdownTickerEvent;  
	     
	    //difference between time now and time session started variable declartion
	    var timeDifference = 0;
	     
	    timeDifference = timeNow - pageRequestTime;
	          
	    if (timeDifference > forceRedirectLength)
	        {   
	           //clear (stop) checksession event
	         
	           clearInterval(checkSessionTimeEvent);
	          jQuery.ajax({
	          	url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=emailProgressAbandon&format=raw",
	          	type: "POST",
	          	data: {useregid: "<?php echo $thisuserid?>", page: "com_securityquestion", start:"<?php echo $start_activated?>",abandoned:timeDifference},
	          	success: function(data){
	          		//window.location="<?php echo $this->baseurl?>/index.php?option=com_login&task=logout";
	          		//window.location="<?php echo $this->baseurl?>";
	          		jQuery("#continue").data('clicked', true);
          			jQuery("#logout_link").click();
	          	}
	          });
	 			//$jquery("#logout_link").click();
	            //force relocation
	           // window.location="<?php echo $this->baseurl?>";
	        }

	}

	var checkSessionTimeEvent;
jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("ul").hide();
	checkSessionTimeEvent = setInterval("CheckForSession()", 1200000);
	
	jQuery("#continue").click(function(){
		jQuery(this).data('clicked', true);
	});

		jQuery(window).bind('beforeunload', function(){
						var timeNow = new Date();
			     
					    //event create countdown ticker variable declaration
					    var countdownTickerEvent;  
					     
					    //difference between time now and time session started variable declartion
					    var timeDifference = 0;
					     
					    timeDifference = timeNow - pageRequestTime;
					    if(!jQuery("#continue").data('clicked')){

			    		   jQuery.ajax({
				          	url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=emailProgressAbandon&format=raw",
				          	type: "POST",
				          	async:false,
				          	data: {useregid: "<?php echo $thisuserid?>", page: "com_securityquestion", start:"<?php echo $start_activated?>",abandoned:timeDifference},
				          	success: function(data){
				          		//window.location="<?php echo $this->baseurl?>";
				          		jQuery("#logout_link").click();
				          	}
				          });
			    		}
			});

});
</script>
