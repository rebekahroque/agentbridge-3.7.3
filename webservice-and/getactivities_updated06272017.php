<?php  include('_dbconn.php');
    
	

	$user_id = $_GET["user_id"];
	$limit_one = $_GET["limit_one"];

	$db = JFactory::getDbo();
	$query = $db->getQuery(true);	
	$query->select('at.*, b.name as buyer_name, u.name as user_name, uo.name as other_user_name')
	->from('#__activities at')
	->leftJoin('#__buyer b ON b.buyer_id = at.buyer_id')
	->leftJoin('#__users u ON u.id = at.user_id')
	->leftJoin('#__users uo ON uo.id = at.other_user_id')
	->where("at.user_id=".$user_id." AND ((at.activity_type = 23 AND b.home_buyer = 1 ) OR at.activity_type = 8 OR at.activity_type = 30 OR at.activity_type = 25 OR at.activity_type = 15 OR at.activity_type = 11 OR at.activity_type = 6 )")
	//->where("at.user_id=".$user_id." AND (at.activity_type = 6)")
	->order('at.date DESC')
	->setLimit(5,$limit_one);
	$activities = $db->setQuery($query)->loadObjectList();
	// print_r($activities); die();

	$activitylog=array();
	foreach ($activities as $key => $value) {	
	
		if($value->activity_type == 23){ //Home Buyer Market

			$activitylog[]=array(
				"bluetitle"=>0,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>"",
				"userId"=>$user_id,
				"other_username"=>"",
				"other_userId"=>"",
				"popname"=>"",
				"popId"=>"",
				"buyername"=>$value->buyer_name,
				"buyerId"=>$value->buyer_id,
				"title"=>"Home Market Buyer",
				"message"=>"My Home Market Buyer has been created just for you"
			   );


		} else if($value->activity_type == 25){ //POPs Buyer Matches

			$other_username = stripslashes_all($value->other_user_name);


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$value->activity_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('permset.selected_permission')
			->from('#__permission_setting permset')
			->where("permset.listing_id=".$value->activity_id);			
			$permset_permission = $db->setQuery($query)->loadResult();

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('reqa.permission')
			->from('#__request_access reqa')
			->where("reqa.property_id=".$value->activity_id." AND reqa.user_b = ".$user_id);			
			$reqa_permission = $db->setQuery($query)->loadResult();

			$ou_name = "";

			if($user_id!=$value->other_user_id){
				$ou_name = $other_username;
				if($permset_permission==1){
					$message = $other_username."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
				} else {
					if($popsetting==1){
						$message = $other_username."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
					} else {
						$popname=$popzip;
						if($reqa_permission==1){
							$message = $other_username."'s POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
							$popname=$popname;
						} else if($reqa_permission==0){
							$message = $other_username."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						} else if($reqa_permission==2){
							$message = $other_username."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						} else {
							$message = $other_username."'s POPs™ in ".$popzip." is a match to your buyer, ".$value->buyer_name;
						}						
					}
				}				
			} else {
				$message = "Your POPs™, ".$popname." is a match to your buyer, ".$value->buyer_name;
			}
		

			$activitylog[]=array(
					"bluetitle"=>1,
					"actlogId"=>$value->pkId,
					"actlogtype"=>$value->activity_type,
					"username"=>$value->user_name,
					"userId"=>$user_id,
					"other_username"=>$ou_name,
					"other_userId"=>$value->other_user_id,
					"popname"=>$popname,
					"popId"=>$value->activity_id,
					"buyername"=>$value->buyer_name,
					"buyerId"=>$value->buyer_id,
					"title"=>"Buyer Match",
					"message"=>$message
				   );

		} else if($value->activity_type == 6){ //R2 Request POPs Access

			$other_username = stripslashes_all($value->other_user_name);
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select("property_id,permission")
			->from('#__request_access')
			->where('pkId = '.$value->activity_id);
			$db->setQuery($query);
			$listing_id = $db->loadObject()->property_id;
			$permset_permission = $db->loadObject()->permission;
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$listing_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			if($permset_permission==1){
				$title = "POPs™ Access Granted";
				$message = $other_username." can now view your POPs™, ".$popname;
			} else {				
				$title = "Request To View Your POPs™";
				$message = $other_username." is requesting to view your private POPs™, ".$popname;
			}

			$activitylog[]=array(
				"bluetitle"=>1,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>$value->user_name,
				"userId"=>$user_id,
				"other_username"=>$other_username,
				"other_userId"=>$value->other_user_id,
				"popname"=>$popname,
				"popId"=>$listing_id,
				"buyername"=>"",
				"buyerId"=>"",
				"title"=>$title,
				"message"=>$message
			   );

		} else if($value->activity_type == 30){ //R1 Request POPs Access

			$other_username = stripslashes_all($value->other_user_name);

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query
			->select("property_id,permission")
			->from('#__request_access')
			->where('pkId = '.$value->activity_id);
			$db->setQuery($query);
			$listing_id = $db->loadObject()->property_id;
			$permset_permission = $db->loadObject()->permission;

			if($listing_id==""){
				continue;
			}

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$listing_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			if($permset_permission==1){
				$title = "POPs™ Access Granted";
				$message = "You may now view ".$other_username."'s POPs™, ".$popname;
			} else {				
				$popname = $popzip;
				$listing_id="";
				$title = "Request POPs™ Access";
				$message = "Your request to access ".$other_username."'s POPs™ in ".$popzip." is pending";
			}

			$activitylog[]=array(
				"bluetitle"=>1,
				"actlogId"=>$value->pkId,
				"actlogtype"=>$value->activity_type,
				"username"=>$value->user_name,
				"userId"=>$user_id,
				"other_username"=>$other_username,
				"other_userId"=>$value->other_user_id,
				"popname"=>$popname,
				"popId"=>$listing_id,
				"buyername"=>"",
				"buyerId"=>"",
				"title"=>$title,
				"message"=>$message
			   );

		} else if($value->activity_type == 15){

			$other_username = stripslashes_all($value->other_user_name);

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')->from('#__referral_status_update rsu')
			->leftJoin('#__referral r ON r.referral_id = rsu.referral_id')
			->where('rsu.update_id = '.$value->activity_id);
			$db->setQuery($query);
			$referral_value = $db->loadObject();
			//$value->value = $this->get_referral_value($value->value_id);		

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('b.name')
			->from('#__buyer b')
			->where("b.buyer_id=".$referral_value->client_id);			
			$cliresults = $db->setQuery($query)->loadObjectList();
			$clientname = $cliresults[0]->name;
			
			$title = "";
			$message = "";


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')->from('#__referral_status_update rsu')
			->where('rsu.status != 0 AND rsu.referral_id = '.$referral_value->referral_id)
			->order('rsu.created_date DESC LIMIT 1');	
			$db->setQuery($query);
			$current_referral_status = $db->loadObject();

			//var_dump($current_referral_status);

			if($current_referral_status->status == 7){

				$db    =  &JFactory::getDbo();
				$query = "SELECT o_signed, r_signed, created_by
			    FROM `tbl_referral_status_update`			    
			    WHERE `tbl_referral_status_update`.`referral_id` = '".$referral_value->referral_id."' AND `tbl_referral_status_update`.status = 0 ORDER BY `tbl_referral_status_update`.created_date ";
				$db->setQuery((string)$query);
				$refsstat = $db->loadObjectList();

				$isSigned=0;
				$signed_user=array();

				if($refsstat){
					foreach ($refsstat as $key3 => $value3) {
					# code...
					if($value3->o_signed==1 || $value3->r_signed==1){
							$signed_user[]=$value3->created_by;
							$isSigned++;
						}
					}

					if($isSigned==1){
						if(in_array($user_id, $signed_user)){
							$isSigned=3;
						}
					}
					//var_dump($isSigned);

					if($isSigned==1){
						$title="Sign Referral Contract";
						$other_username="";
						$message ="Referral ".$clientname." has been signed. The ".$clientname."'s contact information has been released.";
					} else if($isSigned==3){

						$title="Sign Referral Contract";						
						$message ="You have signed the referral contract for ".$clientname.". ".$clientname."'s contact details have been sent to ".$other_username.".";

					} else {

						$title="Referral Update";
						$message ="You and ".$other_username." have both signed referral ".$clientname.". ".$clientname." is waiting to be contacted.";
					}	

				} else {

					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query->select('activity_type')->from('#__activities act')
					->where("act.user_id=".$user_id.' AND act.activity_id = '.$current_referral_status->update_id)
					->order('act.date DESC LIMIT 1');	
					$db->setQuery($query);
					$isAccepted = $db->loadObject();

					if($isAccepted->activity_type==14){
						$title="Referral Update";
						$message =$other_username." has accepted your referral, ".$clientname.", for client 25%";
					} else {
						$title="Referral Update";
						$message ="You have sent referral ".$clientname." to ".$other_username;
					}

				}
				

				
			} else if($current_referral_status->status == 8){

				$title="Referral Update";
				$message = $other_username." is actively working on your referral, ".$clientname.".";

			} else if($current_referral_status->status == 6){

				$title="Referral Update";
				$message = $other_username." needs your help with referral ".$clientname.".";

			} else if($current_referral_status->status == 1){

				$title="Referral Update";
				$message = $other_username." is Under Contract with referred client ".$clientname.".";

			} else if($current_referral_status->status == 5){

				$title="Referral Update";
				$message = $other_username." changed the status of referral, ".$clientname.", to No Go.";

			} else if($current_referral_status->status == 4){

				$title="Referral Update";
				$message = $other_username." has closed referral, ".$clientname.". Final numbers are being verified";

			} else if($current_referral_status->status == 9){

				$title="Referral Completed";
				$other_username="";
				$message = "Funds have been disbursed. Referral ".$clientname." is now Complete.";

			} else {

				$title="Referral Update";
				$message ="You have sent referral ".$clientname." to ".$other_username;

			}

			


			$activitylog[]=array(
							"bluetitle"=>1,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>$value->user_name,
							"userId"=>$user_id,
							"other_username"=>$other_username,
							"other_userId"=>$value->other_user_id,
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>$title,
							"message"=>$message
						   );

		} else if($value->activity_type == 11){

			$other_username = stripslashes_all($value->other_user_name);

			//var_dump($other_username);

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')->from('#__referral_status_update rsu')
			->leftJoin('#__referral r ON r.referral_id = rsu.referral_id')
			->where('rsu.update_id = '.$value->activity_id);
			$db->setQuery($query);
			$referral_value = $db->loadObject();
			//$value->value = $this->get_referral_value($value->value_id);		

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('b.name')
			->from('#__buyer b')
			->where("b.buyer_id=".$referral_value->client_id);			
			$cliresults = $db->setQuery($query)->loadObjectList();
			$clientname = $cliresults[0]->name;
			
			$title = "";
			$message = "";


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')->from('#__referral_status_update rsu')
			->where('rsu.status != 0 AND rsu.referral_id = '.$referral_value->referral_id)
			->order('rsu.created_date DESC LIMIT 1');	
			$db->setQuery($query);
			$current_referral_status = $db->loadObject();

			//var_dump($current_referral_status);

			if($current_referral_status->status == 7){

				$db    =  &JFactory::getDbo();
				$query = "SELECT o_signed, r_signed, created_by
			    FROM `tbl_referral_status_update`			    
			    WHERE `tbl_referral_status_update`.`referral_id` = '".$referral_value->referral_id."' AND `tbl_referral_status_update`.status = 0 ORDER BY `tbl_referral_status_update`.created_date ";
				$db->setQuery((string)$query);
				$refsstat = $db->loadObjectList();

				$isSigned=0;
				$signed_user=array();

				if($refsstat){
					foreach ($refsstat as $key3 => $value3) {
					# code...
					if($value3->o_signed==1 || $value3->r_signed==1){
							$signed_user[]=$value3->created_by;
							$isSigned++;
						}
					}

					if($isSigned==1){
						if(in_array($user_id, $signed_user)){
							$isSigned=3;
						}
					}
					//var_dump($isSigned);

					if($isSigned==1){
						$title="Sign Referral Contract";
						$other_username="";
						$message ="Referral ".$clientname." has been signed. The ".$clientname."'s contact information has been released.";
					} else if($isSigned==3){

						$title="Sign Referral Contract";	
						$other_username="";					
						$message ="You have signed the referral contract for ".$clientname.". ".$clientname."'s contact details have been sent to your email address.";

					} else {

						$title="Referral Update";
						$message ="You and ".$other_username." have both signed referral ".$clientname.". ".$clientname." is waiting to be contacted.";
					}	

				} else {

					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query->select('activity_type')->from('#__activities act')
					->where("act.user_id=".$user_id.' AND act.activity_id = '.$current_referral_status->update_id)
					->order('act.date DESC LIMIT 1');	
					$db->setQuery($query);
					$isAccepted = $db->loadObject();

					if($isAccepted->activity_type==14){
						$title="Referral Update";
						$clientname = encryptClientName($clientname);
						$message ="You have now accepted ".$other_username."'s 25% referral fee for client ".$clientname.". Sign the referral contract.	";
					} else {
						$title="New Referral";
						$clientname = encryptClientName($clientname);
						$message =$other_username." has sent you referral, ".$clientname.", requesting a 25% referral fee.";
					}

				}
				

				
			} else if($current_referral_status->status == 8){ //Active

				$title="Referral Update";
				$other_username="";	
				$message = "You have updated your referral ".$clientname." to Active.";

			} else if($current_referral_status->status == 6){// Need Help

				$title="Referral Update";
				$message = "You requested help from ".$other_username." for referral ".$clientname.".";

			} else if($current_referral_status->status == 1){

				$title="Referral Update";
				$other_username="";
				$message = "You are now Under Contract with referred client, ".$clientname.".";

			} else if($current_referral_status->status == 5){

				$title="Referral Update";
				$other_username="";
				$message = "You changed the status of referral, ".$clientname.", to No Go.";

			} else if($current_referral_status->status == 4){

				$title="Referral Update";
				$other_username="";
				$message = "Your referral,".$clientname." is closed.";

			} else if($current_referral_status->status == 9){

				$title="Referral Completed";
				$other_username="";
				$message = "Funds have been disbursed. Referral ".$clientname." is now Complete.";

			} else {

				$title="NONE ACTIVE REF VALUE";
				$message = $other_username . " You have sent referral ".$clientname." to ".$other_username;

			}

			


			$activitylog[]=array(
							"bluetitle"=>1,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>$value->user_name,
							"userId"=>$user_id,
							"other_username"=>$other_username,
							"other_userId"=>$value->other_user_id,
							"popname"=>"",
							"popId"=>"",
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>$title,
							"message"=>$message
						   );

		}
		 /*else if($value->activity_type == 7){

		 	$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('ra.*')
			->from('#__request_access ra')
			->where("ra.pkId=".$value->activity_id);			
			$requestAccessResults = $db->setQuery($query)->loadObject();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$requestAccessResults->property_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			$other_username = stripslashes_all($value->other_user_name);


		 	$title="POPs™ Access Granted";
		    $message = $other_username." has accepted your request to view this POPs™, ".$popname.".";

			$activitylog[]=array(
							"bluetitle"=>0,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>"",
							"userId"=>$user_id,
							"other_username"=>$other_username,
							"other_userId"=>$value->other_user_id,
							"popname"=>$popname,
							"popId"=>$requestAccessResults->property_id,
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>$title,
							"message"=>$message
						   );

		} else if($value->activity_type == 6){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('ra.*')
			->from('#__request_access ra')
			->where("ra.pkId=".$value->activity_id);			
			$requestAccessResults = $db->setQuery($query)->loadObject();


			$db = JFactory::getDbo();
			$query = $db->getQuery(true);	
			$query->select('p.property_name,p.zip,p.setting')
			->from('#__pocket_listing p')
			->where("p.listing_id=".$requestAccessResults->property_id);			
			$popresults = $db->setQuery($query)->loadObjectList();
			$popname = $popresults[0]->property_name;
			$popzip = $popresults[0]->zip;
			$popsetting = $popresults[0]->setting;

			$other_username = stripslashes_all($value->other_user_name);


		 	$title="Approve POPs™ Access";
		    $message = $other_username." requested to view your POPs™, ".$popname.".";

			$activitylog[]=array(
							"bluetitle"=>0,
							"actlogId"=>$value->pkId,
							"actlogtype"=>$value->activity_type,
							"username"=>"",
							"userId"=>$user_id,
							"other_username"=>$other_username,
							"other_userId"=>$value->other_user_id,
							"popname"=>$popname,
							"popId"=>$requestAccessResults->property_id,
							"buyername"=>"",
							"buyerId"=>"",
							"title"=>$title,
							"message"=>$message
						   );

		}*/ 
	}

	$response = array('status'=>1, 'message'=>"Found Activities", 'data'=>$activitylog);

	echo json_encode($response);

?>