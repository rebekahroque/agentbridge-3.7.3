<?php include('_dbconn.php');
    
    $con = $mysqli = new mysqli("localhost", $username, $password, $database);

    $mysqli->set_charset('utf8');



    $user_id = $_GET["user_id"];
    $activities_id = $_GET["activities_id"];
   // $listing_id = $_GET["listing_id"];

    $query = "SELECT 
            at.`pkId` AS `activities_id`, 
            at.`activity_id`,
            at.`activity_type`, 
            at.`user_id`, 
            at.`activity_type`, 
            at.`date`, 
            at.`buyer_id` AS at_buyerId, 
            at.`other_user_id`, 
            pl.`listing_id`, 
            pl.`property_name`, 
            pl.`setting`, 
            pl.`zip`,
            pl.`user_id` AS `pops_user_id`,
            `user_data`.`name` AS `pops_user_name`, 
            b.`name` AS `client_name`, 
            ur.`image`, 
            `other_user_data`.`name` AS `other_user_name`,
            
            bsl.*,`bsl`.`buyer_id` AS saved_buyer,rq.*
    FROM `tbl_activities` at
    LEFT JOIN `tbl_pocket_listing` pl ON at.`activity_id` = pl.`listing_id`
    LEFT JOIN `tbl_users` AS `user_data` ON `user_data`.`id` = pl.`user_id`
    LEFT JOIN `tbl_buyer` b ON b.`buyer_id` = at.`buyer_id`
    LEFT JOIN `tbl_buyer_saved_listing` bsl ON bsl.`buyer_id` = at.`buyer_id` AND `bsl`.`listing_id` = at.`activity_id`
    LEFT JOIN `tbl_request_access` rq ON rq.`user_b` = at.`user_id` AND rq.`property_id` = at.`activity_id`
    LEFT JOIN `tbl_user_registration` ur ON ur.`email` = `user_data`.`email`
    LEFT JOIN `tbl_users` AS `other_user_data` ON `other_user_data`.`id` = at.`other_user_id`
    WHERE at.`pkId` = '$activities_id' AND at.`user_id` = '$user_id' AND at.`activity_type` = '25'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
    $response = array('status'=>0, 'message'=>"No Activity Found", 'data'=>array());
}
else {
    $rows = array();    
    $rows_u = array();
    while ($r = $result->fetch_assoc())
    {
        $rows[] = $r;
    }
    foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='date'){
                $this_line['date']=date("m/d/Y h:i:s A",strtotime($value2))." PDT";
            }
            if($key2=='other_user_name'){
                $this_line['other_user_name']=stripslashes($value2);
            }

            if($key2 == 'permission'){
                $this_line['req_access_per']= "0";
                $this_line['selected_permission'] = "0";
                    if($user_id == $this_line['other_user_id']){
                        $this_line['selected_permission'] = "1";
                    } else {
                        if($this_line['other_user_id']){
                               
                                if( $this_line['setting'] == 1) {
                                    $this_line['selected_permission'] = "1";    
                                    $this_line['req_access_per'] = "1";                                                             
                                } else {                                    
                                    if ($this_line['permission']=="1"){
                                        $this_line['selected_permission'] = "1";
                                        $this_line['req_access_per'] = "1";
                                    } else if ($this_line['permission']=="0"){
                                        $this_line['req_access_per'] = "pending_private";
                                    } else if ($this_line['permission']=="2"){
                                        $this_line['req_access_per'] = "denied_private";
                                    } else {
                                        $this_line['req_access_per'] = "request_private";
                                    }
                                }

                        } else {
                          //  $this_line['selected_permission'] = "1";
                        }

                    }
            }
        }
        $rows_u[]=$this_line;
    }
    $response = array('status'=>1, 'message'=>"Found Activity", 'data'=>$rows_u);
}

echo json_encode($response);

?>