<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $keyword = $_GET['keyword'];

	$query = "SELECT `tbl_user_registration`.*, `tbl_broker`.broker_name, `tbl_countries`.countries_name, `tbl_zones`.zone_name, `tbl_users`.id AS `unique_id`,((IFNULL(buyerActive.activeBUYcount,0))+(IFNULL(popActive.activePOPcount,0))) AS popbcounts 
    FROM `tbl_user_registration`
    LEFT JOIN `tbl_users` ON `tbl_users`.email = `tbl_user_registration`.email
    LEFT JOIN `tbl_countries` ON `tbl_countries`.countries_id = `tbl_user_registration`.country
    LEFT JOIN `tbl_zones` ON `tbl_zones`.zone_id = `tbl_user_registration`.state
	LEFT JOIN `tbl_broker` ON `tbl_broker`.broker_id = `tbl_user_registration`.brokerage
    LEFT JOIN tbl_user_usergroup_map ugmp ON ugmp.user_id = `tbl_users`.id 
    LEFT JOIN  tbl_buyer b ON  b.agent_id = `tbl_users`.id
    LEFT JOIN  tbl_buyer_needs bn ON  b.buyer_id = bn.buyer_id
    LEFT JOIN  tbl_pocket_listing pl ON  pl.user_id = `tbl_users`.id
    LEFT JOIN   tbl_user_zips_workaround uz
                    on  uz.user_id = tbl_user_registration.user_id
    LEFT JOIN ( SELECT 
                COUNT(pl.listing_id) AS activePOPcount, 
                pl.user_id FROM tbl_pocket_listing pl 
                WHERE pl.sold != 1 AND pl.closed != 1 GROUP BY pl.user_id) as popActive ON popActive.user_id = tbl_users.id 
    LEFT JOIN ( SELECT  
                COUNT(bu.buyer_id) AS activeBUYcount, 
                bu.agent_id FROM tbl_buyer bu WHERE bu.buyer_type != 'Inactive' AND bu.buyer_type != '' GROUP BY bu.agent_id) as buyerActive ON buyerActive.agent_id = tbl_users.id
    LEFT JOIN ( SELECT 
                 GROUP_CONCAT(DISTINCT  bn.zip) AS bnzipslist,b.agent_id FROM tbl_buyer_needs bn 
                 LEFT JOIN  tbl_buyer b on  b.buyer_id = bn.buyer_id
                 GROUP BY b.agent_id) as buyerZipLists ON buyerZipLists.agent_id = tbl_users.id
    LEFT JOIN ( SELECT 
                GROUP_CONCAT(DISTINCT pl.zip) AS plzipslist,pl.user_id FROM tbl_pocket_listing pl 
                 GROUP BY pl.user_id) as popLists ON popLists.user_id = tbl_users.id
    LEFT JOIN  tbl_country_sp_address csp
            on  (tbl_user_registration.zip LIKE CONCAT(csp.postcode, '%') OR 
                buyerZipLists.bnzipslist LIKE CONCAT(csp.postcode, '%') OR 
                popLists.plzipslist LIKE CONCAT(csp.postcode, '%'))
    WHERE (`firstname` LIKE '%$keyword%' OR 
           `lastname` LIKE '%$keyword%' OR 
           `tbl_user_registration`.`city` LIKE '%$keyword%' OR 
           `tbl_user_registration`.`zip` LIKE '%$keyword%' OR 
           `buyerZipLists`.`bnzipslist` LIKE '%$keyword%' OR 
           `popLists`.`plzipslist` LIKE '%$keyword%' OR
           `uz`.`zip_workaround` LIKE '%$keyword%' OR 
           `csp`.`county` LIKE '%$keyword%' OR 
           `csp`.`address1` LIKE '%$keyword%' OR 
           `csp`.`address2` LIKE '%$keyword%' OR 
           `csp`.`address3` LIKE '%$keyword%' OR 
           `csp`.`address4` LIKE '%$keyword%' OR 
           `countries_name` LIKE '%$keyword%' OR 
           `zone_name` LIKE '%$keyword%' OR
           `broker_name` LIKE '%$keyword%' ) 
            AND `is_term_accepted` = '1' AND ugmp.group_id!=8 
    GROUP BY tbl_users.id 
    ".'ORDER BY 
        CASE 
        WHEN popLists.plzipslist REGEXP "^'.$keyword.'[a-z]{1,}" THEN 1
        WHEN FIND_IN_SET("'.$keyword.'",uz.zip_workaround) THEN 2
        WHEN FIND_WILD_IN_SET("'.$keyword.'%",uz.zip_workaround) THEN 3                                                  
        WHEN popLists.plzipslist REGEXP "[[.,.]]'.$keyword.'[a-z]{1,}" THEN 5
        WHEN FIND_IN_SET("'.$keyword.'",buyerZipLists.bnzipslist) THEN 6
        WHEN buyerZipLists.bnzipslist REGEXP "^'.$keyword.'[a-z]{1,}" THEN 7                                                 
        WHEN buyerZipLists.bnzipslist REGEXP "[[.,.]]'.$keyword.'[a-z]{1,}" THEN 8 
        WHEN popLists.plzipslist REGEXP "[[.,.]]'.$keyword.'[0-9]{1,}" THEN 9 
        ELSE 10
        END,popbcounts DESC';

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Agents Found", 'data'=>array());
}
else {
	$rows = array();
    $rows_u = array();
    while ($r = $result->fetch_assoc())
    {
        if(key($r)=='date'){
            
            
        }   

        $rows[] = $r;
    }
    foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='firstname'){
                $this_line['firstname']=stripslashes($value2);
            }
            if($key2=='lastname'){
                $this_line['lastname']=stripslashes($value2);
            }
        }
        $rows_u[]=$this_line;
    }
    $response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$rows_u);
}

echo json_encode($response);

?>