<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



$user_id = $_GET["user_id"];

    $query = "SELECT `tbl_user_designations`.`pk_id` AS `designation_id`, `tbl_user_designations`.`user_id`, `tbl_user_designations`.`desig_id`, `tbl_designations`.`designations` AS `designation_name`
    FROM `tbl_user_designations`
    LEFT JOIN `tbl_designations` ON `tbl_user_designations`.`desig_id` = `tbl_designations`.`id`
WHERE `tbl_user_designations`.`user_id`='$user_id'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Designation Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found User Designation", 'data'=>$rows);
}

echo json_encode($response);

?>