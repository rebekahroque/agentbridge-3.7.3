<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);
    
    
    
    $user_id = $_GET["user_id"];
    
    $query = "SELECT `tbl_activities`.`pkId` AS `activities_id`, `tbl_activities`.`activity_id`, `tbl_activities`.`activity_type`, `tbl_activities`.`user_id`, `tbl_activities`.`date`, `tbl_activities`.`buyer_id`, `tbl_activities`.`other_user_id`
    FROM `tbl_activities`
    WHERE ((`tbl_activities`.`other_user_id` = '$user_id' OR `tbl_activities`.`user_id` = '$user_id') AND (`tbl_activities`.`activity_type` = '11' OR `tbl_activities`.`activity_type` = '6' OR `tbl_activities`.`activity_type` = '28' OR `tbl_activities`.`activity_type` = '8')) OR (`tbl_activities`.`user_id` = '$user_id' AND `tbl_activities`.`activity_type` = '25')
    ORDER BY `tbl_activities`.`date` DESC";
    
    $result = $mysqli->query($query) or die($mysqli->error);
    
    $num = $result->num_rows;
    
    $mysqli->close();
    
    $response = array();
    if($num == 0) {
        $response = array('status'=>0, 'message'=>"No Activity Found", 'data'=>array());
    }
    else {
        $rows = array();
        while ($r = $result->fetch_assoc())
        {
            $rows[] = $r;
        }
        
        $response = array('status'=>1, 'message'=>"Found Activity", 'data'=>$rows);
    }
    
    echo json_encode($response);
    
    ?>