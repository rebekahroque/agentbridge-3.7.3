<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);




    $query = "SELECT `tbl_designations`.`id` AS `designation_id`, `tbl_designations`.`designations`, `tbl_designations`.`description` AS `desc`, `tbl_designations`.`url`, `tbl_designations`.`country_available`
    
    FROM `tbl_designations`";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Designation Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Designation", 'data'=>$rows);
}

echo json_encode($response);

?>