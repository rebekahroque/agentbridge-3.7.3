<?php
    
    include('_dbconn.php');

      //$user = JFactory::getUser();
  ////$session = JFactory::getSession();
  //$session->set('user', new JUser($user->id));
  $language = JFactory::getLanguage();
  $extension = 'com_nrds';
  $base_dir = JPATH_SITE;
  // $language_tag = JFactory::getUser()->currLanguage;
  $language_tag = "english-US";
  $language->load($extension, $base_dir, $language_tag, true);


    $user_id = $_GET["user_id"];
    $listing_id = $_GET["listing_id"];
    $pl_id=$listing_id;


    $langValues = $propertylist_model->checkLangFiles();
    $individual = $propertylist_model->getPOPsById_model($listing_id,$user_id);
    //$userDetails = $userprof_model->get_user_registration($user_id);

    $num = count($individual);
    //echo "<pre>";
    //var_dump($individual);

    $response = array();
    if($num == 0) {
    	$response = array('status'=>0, 'message'=>"No User Properties Found", 'data'=>array());
    }
    else {
    	$rows = array();
    /*	while ($r = $result->fetch_assoc())
    	{
    		$rows[] = $r;
    	}*/

    	$rows = $individual;

         foreach ($rows as $key => $value) {
            $this_line=$value;
            foreach ($value as $key2 => $value2) {
                if($key2=='user_id'){
                     $pl_userId=$value2;
                }
                if($key2=='name'){
                    $this_line['name']=stripslashes($value2);
                }

                if($key2=='date_expired'){
                     $now = time();
                    $timedifference=strtotime($value2)-$now;
                    $this_line['expiry']=strval(round($timedifference/(60*60*24)));
                }

                if($key2 == 'selected_permission'){
                    $this_line['req_access_per']= "1";
                        if($user_id == $pl_userId){
                            $this_line['selected_permission'] = "1";
                        } else {
                            if($pl_userId && $this_line['selected_permission'] != 1){                                                               
                                    if( $this_line['setting'] == 1) {
                                        $this_line['selected_permission'] = "1";    
                                        $this_line['req_access_per'] = "1";                                                             
                                    } else {                                    
                                        if ($this_line['permission']=="1"){
                                            $this_line['selected_permission'] = "1";
                                            $this_line['req_access_per'] = "1";
                                        } else if ($this_line['permission']=="0"){
                                            $this_line['req_access_per'] = "pending_private";
                                        } else if ($this_line['permission']=="2"){
                                            $this_line['req_access_per'] = "denied_private";
                                        } else {
                                            $this_line['req_access_per'] = "request_private";
                                        }
                                    }
                            } else {
                              //$this_line['selected_permission'] = "1";
                            }

                        }
                }

            }

            $this_line['show_button']= "false";
            $this_line['type_name'] = JText::_($this_line['proptype_name']);
            $this_line['sub_type_name'] = JText::_($this_line['subtype_name']);

            $current_user_info_currency = "USD";
            $current_user_info_symbol = "$";

            if($user_id!=$this_line['user_id']){
                if($this_line['currency']!=$current_user_info_currency){
                  $ex_rates_con = $ex_rates->rates->{$this_line['currency']};
                  $ex_rates_can = $ex_rates->rates->{$current_user_info_currency};
                  if($this_line['currency']=="USD"){                  
                    //$user->price1=$listing->price1 * $ex_rates_CAD;
                 //   $user_details->ave_price_2012=$user_details->ave_price_2012 * $ex_rates_can;
                    $this_line['price1'] = $this_line['price1'] * $ex_rates_can;
                    if($this_line['price2']){
                        $this_line['price2'] = $this_line['price2'] * $ex_rates_can;
                    }

                  } else {
                    //$user->price1=$listing->price1 / $ex_rates_CAD;
                    //$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
                    //$user_details->ave_price_2012=($user_details->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
                    $this_line['price1'] = ($this_line['price1']  / $ex_rates_con) * $ex_rates_can;
                    if($this_line['price2']){
                        $this_line['price2'] = ($this_line['price2']  / $ex_rates_con) * $ex_rates_can;
                    }

                  }
                  $this_line['currency'] = $current_user_info_currency;
                  $this_line['symbol'] = $current_user_info_symbol;
                }
                $this_line['edit_button']=0;
            } else {
                $this_line['edit_button']=1;
            }


            if($user_id != $individual['user_id']){
                $country_used = $userDetails->country;
                $sqftMeasurement_used= $userDetails->sqftMeasurement;
                
            } else {
                $country_used = $individual['country'];
                $sqftMeasurement_used= $individual['sqftMeasurement'];
                
            }


            $sqM = "";
            
            if($sqftMeasurement_used=="sq. meters"){
                $getMeasurement = $propertylist_model->getSqMeasureByCountry($this_line['country']);
                foreach ($getMeasurement['bldg'] as $key => $value) {
                    if($individual['bldg_sqft']){
                        if($individual['bldg_sqft'] == $value[1]){
                            $individual['bldg_sqft']=$value[0];
                        }
                    }
                    if($individual['available_sqft']){
                        if($individual['available_sqft'] == $value[1]){
                            $individual['available_sqft']=$value[0];
                        }
                        
                    }
                    if($individual['unit_sqft']){
                        if($individual['unit_sqft'] == $value[1]){
                            $individual['unit_sqft']=$value[0];
                        }
                        
                    }
                }

                foreach ($getMeasurement['lot'] as $key => $value) {
                    # code...
                    if($individual['lot_sqft']){
                        if($individual['lot_sqft'] == $value[1]){
                            $individual['lot_sqft']=$value[0];
                        }
                        
                    }
                    if($individual['lot_size']){
                        if($individual['lot_size'] == $value[1]){
                            $individual['lot_size']=$value[0];
                        }
                        
                    }
                }

                $sqM = "_M";
            }

            $pops_feats = $propertylist_model->getPOPsDetails($individual[0],$sqM,$individual['sub_type'],$langValues);

            foreach ($pops_feats['feats_array'] as $key => $value) {
                # code...
                $arr_value= explode(": ", $value);
                if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type" && $arr_value[0]!="Term" && $arr_value[0]!="Possession"){
                    $pops_feats['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
                } else {
                    $pops_feats['feats_array_edited'][]=$value;
                }
                
            }

            $this_line['initial_feats'] =$pops_feats['initial_feats'];
            $this_line['feats_array'] =implode(", ", $pops_feats['feats_array_edited']);

            $rows_r[]=$this_line;
            $rows_u = array_reverse($rows_r);
        }
        
    	$response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$rows_u);
    }
 
    echo json_encode($response);

?>