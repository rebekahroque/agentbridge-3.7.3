<?php
    
    include('_dbconn.php');
    
    $mysqli = new mysqli("localhost", $username, $password, $database);



    $keyword = $_GET['keyword'];
    $user_id = $_GET['user_id'];

    $now_date = date("Y-m-d",time());

    /*$query = "SELECT `pocket_images`.images, `tbl_zones`.zone_name, `tbl_users`.name, `tbl_property_type`.`type_name`, `tbl_property_sub_type`.`name` AS `sub_type_name`, `tbl_pocket_listing`.*
    FROM `tbl_pocket_listing`
    LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
               FROM `tbl_pocket_images`
               GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
    LEFT JOIN `tbl_zones` ON `tbl_zones`.zone_id = `tbl_pocket_listing`.state
    LEFT JOIN `tbl_users` ON `tbl_users`.id = `tbl_pocket_listing`.user_id
    LEFT JOIN `tbl_permission_setting` ON `tbl_permission_setting`.`listing_id` = `tbl_pocket_listing`.`listing_id`
    INNER JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
    INNER JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
    WHERE `property_name` LIKE '%$keyword%' OR `city` LIKE '%$keyword%' OR `zip` LIKE '%$keyword%' OR `view` LIKE '%$keyword%' OR `pool_spa` LIKE '%$keyword%' OR `city` LIKE '%$keyword%' OR `type` LIKE '%$keyword%' OR `bldg_type` LIKE '%$keyword%' OR `features1` LIKE '%$keyword%' OR `features2` LIKE '%$keyword%' OR `features3` LIKE '%$keyword%' OR `description` LIKE '%$keyword%' OR `zone_name` LIKE '%$keyword%'
    ";*/

    $query = "SELECT `tbl_pocket_listing`.`listing_id`, `tbl_zones`.zone_name, `tbl_pocket_listing`.`property_type` AS `type_property_type`, `tbl_pocket_listing`.`property_name`, `tbl_property_type`.`type_name`, `tbl_pocket_listing`.`sub_type`, `tbl_property_sub_type`.`name` AS `sub_type_name`, `tbl_property_price`.`price_type`, `tbl_property_price`.`price1`, `tbl_property_price`.`price2`, `tbl_property_price`.`disclose`, `tbl_pocket_listing`.`expiry`, `tbl_pocket_listing`.`user_id`, `tbl_users`.`name`, `tbl_pocket_listing`.`city`, `tbl_pocket_listing`.`zip`, `tbl_pocket_listing`.`state`, `tbl_pocket_listing`.`bedroom`, `tbl_pocket_listing`.`bathroom`, `tbl_pocket_listing`.`unit_sqft`, `tbl_pocket_listing`.`view`, `tbl_pocket_listing`.`style`, `tbl_pocket_listing`.`year_built`, `tbl_pocket_listing`.`pool_spa`, `tbl_pocket_listing`.`condition`, `tbl_pocket_listing`.`garage`, `tbl_pocket_listing`.`units`, `tbl_pocket_listing`.`cap_rate`, `tbl_pocket_listing`.`grm`, `tbl_pocket_listing`.`occupancy`, `tbl_pocket_listing`.`type`, `tbl_pocket_listing`.`listing_class`, `tbl_pocket_listing`.`parking_ratio`, `tbl_pocket_listing`.`ceiling_height`, `tbl_pocket_listing`.`stories`, `tbl_pocket_listing`.`room_count`, `tbl_pocket_listing`.`type_lease`, `tbl_pocket_listing`.`type_lease2`, `tbl_pocket_listing`.`available_sqft`, `tbl_pocket_listing`.`lot_sqft`, `tbl_pocket_listing`.`lot_size`, `tbl_pocket_listing`.`bldg_sqft`, `tbl_pocket_listing`.`term`, `tbl_pocket_listing`.`furnished`, `tbl_pocket_listing`.`pet`, `tbl_pocket_listing`.`possession`, `tbl_pocket_listing`.`zoned`, `tbl_pocket_listing`.`bldg_type`, `tbl_pocket_listing`.`features1`, `tbl_pocket_listing`.`features2`, `tbl_pocket_listing`.`features3`, `tbl_pocket_listing`.`setting`, `tbl_pocket_listing`.`description` AS `desc`, `tbl_pocket_listing`.`closed`, `tbl_pocket_listing`.`date_created`, `tbl_pocket_listing`.`date_expired`, `tbl_property_price`.`pocket_id`, `pocket_images`.`images`
    
    FROM `tbl_pocket_listing`
    LEFT JOIN `tbl_zones` ON `tbl_zones`.zone_id = `tbl_pocket_listing`.state
    INNER JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
    INNER JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
    INNER JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
    LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`
    LEFT JOIN (SELECT `tbl_pocket_images`.`listing_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_images`.`image`) AS `images`, `tbl_pocket_images`.`image_id`
               FROM `tbl_pocket_images`
               GROUP BY `tbl_pocket_images`.`listing_id`) AS `pocket_images` ON `pocket_images`.`listing_id` = `tbl_pocket_listing`.`listing_id`
    WHERE (`property_name` LIKE '%$keyword%' OR  `zip` LIKE '%$keyword%' OR `view` LIKE '%$keyword%' OR `pool_spa` LIKE '%$keyword%' OR `tbl_pocket_listing`.`city` LIKE '%$keyword%' OR `type` LIKE '%$keyword%' OR `bldg_type` LIKE '%$keyword%' OR `features1` LIKE '%$keyword%' OR `features2` LIKE '%$keyword%' OR `features3` LIKE '%$keyword%' OR `description` LIKE '%$keyword%' OR `zone_name` LIKE '%$keyword%') AND `tbl_pocket_listing`.`date_expired` > '$now_date'
    ";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;
    
$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No POPs Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
        $pops_user_id = $r["user_id"];
        $pops_listing_id = $r["listing_id"];        
        
        $network_status = 0;
        
        $queryNetwork = "SELECT `tbl_request_network`.* FROM `tbl_request_network`
        WHERE `tbl_request_network`.`user_id` = $pops_user_id AND `tbl_request_network`.`other_user_id` = $user_id";
        
        
        $resultNetwork = $mysqli->query($queryNetwork) or die($mysqli->error);
        $r['req_net_status']="empty";
        while($rowN = $resultNetwork->fetch_assoc())
        {
            $network_status = $rowN["status"];
            $r['req_net_status']=$rowN["status"];
           
        }
        
        $r['req_access_per']="empty";
        $access_status = 0;
        if($network_status == 1) {
            
            $queryAccess = "SELECT `tbl_request_access`.`pkId` AS `access_id`, `tbl_request_access`.`user_a`, `tbl_request_access`.`user_b`, `tbl_request_access`.`property_id`, `tbl_request_access`.`permission` FROM `tbl_request_access`
            WHERE `tbl_request_access`.`user_a` = '$pops_user_id' AND `tbl_request_access`.`user_b` = '$user_id' AND `tbl_request_access`.`property_id` = '$pops_listing_id'";
            
            
            $resultAccess = $mysqli->query($queryAccess) or die($mysqli->error);
            
            while($rowA = $resultAccess->fetch_assoc())
            {
                $access_status = $rowA["permission"];
                $r['req_access_per'] = $rowA["permission"];
            }
        }
        
        array_push($r, $network_status, $access_status);
        
		    $rows[] = $r;
    }
    
    
    

    $rows_u = array();

    foreach ($rows as $key => $value) {
       $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='listing_id'){
               
                $query = "SELECT `tbl_permission_setting`.`selected_permission` 
                          FROM `tbl_pocket_listing` 
                          LEFT JOIN `tbl_permission_setting` ON `tbl_permission_setting`.`listing_id` = `tbl_pocket_listing`.`listing_id`
                          WHERE `tbl_pocket_listing`.`user_id`='$user_id' AND `tbl_pocket_listing`.`listing_id` = '$value2'";

                $result = $mysqli->query($query) or die($mysqli->error);
            
                while($row = $result->fetch_assoc())
                {
                    $this_line['selected_permission'] = $row["selected_permission"];
                }

                if($this_line['selected_permission'] == null){
                    $this_line['selected_permission'] = 0;
                }

                
              /*if($key2=='user_id'){
                   $poplist= $this_line['listing_id'];  
                   $poplist_userid= $this_line['user_id'];             
                  $query = "SELECT `tbl_request_network`.`pk_id` AS `network_id`, 
                           `tbl_request_network`.`user_id`, `tbl_request_network`.`other_user_id`, 
                           `tbl_request_network`.`status` 
                           FROM `tbl_request_network`
                           WHERE `tbl_request_network`.`user_id` = '$user_id' AND `tbl_request_network`.`other_user_id` = '$value2'";

                  $result = $mysqli->query($query) or die($mysqli->error);
              
                  while($row = $result->fetch_assoc())
                  {
                      $this_line['req_net_status'] = $row["status"];
                  }

                  if($this_line['req_net_status'] == null){
                      $this_line['req_net_status'] = "empty";
                  }

                   $query = "SELECT `tbl_request_access`.`pkId` AS `access_id`, 
                                    `tbl_request_access`.`user_a`, 
                                    `tbl_request_access`.`user_b`, 
                                    `tbl_request_access`.`property_id`, 
                                    `tbl_request_access`.`permission` 
                             FROM `tbl_request_access` WHERE `tbl_request_access`.`user_a` = '$user_id' AND `tbl_request_access`.`user_b` = '$poplist_userid' AND `tbl_request_access`.`property_id` = '$poplist'";

                  $result = $mysqli->query($query) or die($mysqli->error);
              
                  while($row = $result->fetch_assoc())
                  {
                      $this_line['req_access_per'] = $row["permission"];
                  }

                  if($this_line['req_access_per'] == null){
                      $this_line['req_access_per'] = "empty";
                  }
              }*/

            }



            /*if($key2=='date_expired'){
                if($value2!=0 || $value2!=null){
                      $now = time();
                      $timedifference=strtotime($value2)-$now;
                      $this_line['expiry']=strval(floor($timedifference/(60*60*24)));                   
                }
              
            }*/

        }
        
                      $now = time();
                      $timedifference=strtotime($this_line['date_expired'])-$now;
                      $this_line['expiry']=strval(round($timedifference/(60*60*24)));   
        $this_line['property_name']=stripslashes($this_line['property_name']);
        $this_line['name']=stripslashes($this_line['name']);
        $rows_u[]=$this_line;
    }
	
	$response = array('status'=>1, 'message'=>"Found POPs", 'data'=>$rows_u);
}
$mysqli->close();

echo json_encode($response);

?>