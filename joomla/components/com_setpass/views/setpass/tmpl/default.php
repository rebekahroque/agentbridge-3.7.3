<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');

?>

<jdoc:include type="message" />

<form id="set_password" action="index.php?option=com_setpass&regid=<?php echo $_GET['regid'] ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">

	<input type="hidden" value="set_password" id="jform_form" name="jform[form]" />		
	<input type="hidden" value="<?php echo $_GET['regid'] ?>" id="jform_regid" name="jform[regid]" />		

	<div class="control-group">
		<div class="control-label">
			<label title="Enter your desired password<br />Minimum of 4 characters" class="hasTip required" for="jform_password1" id="jform_password1-lbl">Password:<span class="star">&nbsp;*</span></label>				
		</div>
		<div class="controls">
			<input type="password" size="30" class="validate-password required" autocomplete="off" value="" id="jform_password1" name="jform[password1]" aria-required="true" required="required">					
		</div>
	</div>

	<div class="control-group">
		<div class="control-label">
			<label title="Confirm your password" class="hasTip required" for="jform_password2" id="jform_password2-lbl">Confirm Password:<span class="star">&nbsp;*</span></label>		
		</div>
		<div class="controls">
			<input type="password" size="30" class="validate-password required" autocomplete="off" value="" id="jform_password2" name="jform[password2]" aria-required="true" required="required">					
		</div>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary validate"><?php echo JText::_('Continue');?></button>
		<a class="btn" href="<?php echo JRoute::_('');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
		<input type="hidden" name="option" value="com_setpass" />
		<input type="hidden" name="task" value="set_pass" />
		<?php echo JHtml::_('form.token');?>
	</div>
	
</form>