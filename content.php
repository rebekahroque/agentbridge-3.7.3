<a href="#message" id="showmessage"></a>
<div style="display: none;">
	<div id="message" style="padding: 20px; font: 13px Century Gothic; line-height: 18px; width: 250px; text-align: left; color: #FF4040;"></div>
</div>
<div class="contenttop">
	<div class="contentleft">
		<div class="toptitle">
			<div class="number1">1</div>
			<div class="number1title">SELECT A CATEGORY AND CLICK ON YOUR MOSBEAUS&#39;S FAVORITE TO BEGIN ORDERING</div>
		</div>
		<div class="linetop"></div> <!-- LINE -->
		<div class="topnav">
			<ul>
				<input type="hidden" id="category" value="9" />
				<a href="javascript: void(loadProducts(9))"><li class="active">BEST SELLER |</li></a>
				<a href="javascript: void(loadProducts(7))"><li>WHITENING |</li></a>
				<a href="javascript: void(loadProducts(8))"><li>ANTI-AGING |</li></a>
				<a href="javascript: void(loadProducts(2))"><li>BODY |</li></a>
				<a href="javascript: void(loadProducts(1))"><li>FACE |</li></a>
				<a href="javascript: void(loadProducts(4))"><li>SUPPLEMENT |</li></a>
				<a href="javascript: void(loadProducts(3))"><li>HAIR |</li></a>
				<a href="javascript: void(loadProducts(5))"><li>BEAUTY SETS |</li></a>
				<a href="javascript: void(loadProducts(6))"><li>PROMO</li></a>
				<!--<a href="#"><li>PREMIUM COLLECTION</li></a>-->
			</ul>
		</div>
		<div class="linebottom"></div> <!-- LINE -->
		<div class="contentholder">
			<!--<div class="contentleftcontentcenter">
				<div class="placentaleftcontent">
					<img src="imgs/pcadvance.png" />
				</div>
				<div class="placentarightcontent">
					<div class="placentarightcontenttop">
						<img src="imgs/agedefy.png" />
					</div>
					<div class="placentarightcontentbottom">
						<img src="imgs/pcwhite.png" />
					</div>
				</div>
			</div>
			<div class="contentleftcontentbottom">
				<table>
					<tr>
						<td><img src="imgs/pcbodylotion.png" /></td>
						<td width="2"></td>
						<td><img src="imgs/pcbodyscrub.png" /></td>
						<td width="2"></td>
						<td><img src="imgs/pcpremiumcream.png" /></td>
						<td width="2"></td>
						<td><img src="imgs/pcbodysoap.png" /></td>
						<td width="2"></td>
						<td><img src="imgs/pcgreentea.png" /></td>
					</tr>
				</table>
			</div>-->
			<div id="product_list">
				
			</div>
		</div>
	</div>
	<div class="contentright">
		<div class="toptitleright">
			<div class="number2">2</div>
			<div class="number2title">CHECK YOUR ORDER AND TOTAL BILL</div>
		</div>
		<div class="tclinetop"></div> <!-- LINE -->
		<div id="cart_cont">
		<div class="totalcontent">
			<!--// CART CONTENTS //-->
				<table>
				<?php
					if(count($_SESSION['cart']->contents) > 0){
						foreach($_SESSION['cart']->contents as $pID => $p_arr){
				?>
						<tr>
							<td width="80%">
								<a href="javascript: deleteCart(<?php echo $pID ?>);"><img src="images/close.gif" border="0" /></a>
								<?php echo zen_get_products_name($pID); ?>
							</td>
							<td>
								<?php echo $p_arr['qty']; echo ($p_arr['qty'] > 1) ? 'pcs.' : 'pc'; ?><br/>
								<?php echo $currencies->format(zen_get_products_base_price($pID) * $p_arr['qty']); ?>
							</td>
						</tr>
				<?php	
							$total = $_SESSION['cart']->show_total() + $_SESSION['shipping_cost'];
						}
					}
				?>	
				</table>
		</div>
		<div class="tclinebottom"></div> <!-- LINE -->
		<div class="totalcontenttotal">
			<table>
				<tr>
					<td width="80%">SUB TOTAL</td>
					<td><?php echo $currencies->format($_SESSION['cart']->show_total()); ?></td>
				</tr>
				<tr>
					<td colspan="2" height="5"></td>
				</tr>
				<tr>
					<td width="80%">SHIPPING COST</td>
					<td><?php echo $currencies->format($_SESSION['shipping_cost']); ?></td>
				</tr>
				<tr>
					<td colspan="2" height="5"></td>
				</tr>
				<tr>
					<td width="80%">TOTAL</td>
					<td><?php echo $currencies->format($total); ?></td>
				</tr>
			</table>
		</div>
		</div>
	</div>
</div>
<div class="contentbottom">
	<div class="bottomtoptitle">
		<div class="number3">3</div>
		<div class="number3title">COMPLETE YOUR DELIVERY INFORMATION CLICK THE BUTTON WHEN YOURE DONE</div>
	</div>
	<div class="bottomtopline"></div> <!-- LINE -->
	<div class="bottombodycontent">
		<form method="post">
			<div class="leftcompanydetails">
				<table>
					<tr>
						<td class="tdtitle">COMPANY DETAILS*</td>
					</tr>
					<tr>
						<td>
							<table style="width:30%">
								<tr>
									<td><input type="radio" name="s_gender" <?php if($_SESSION['tmp_customer']['gender']=="m") echo "checked"; ?> value="m" onclick="set_gender(this.value);" /></td>
									<td>MR.</td>
									<td><input type="radio" name="s_gender" <?php if($_SESSION['tmp_customer']['gender']=="f") echo "checked"; ?> value="f" onclick="set_gender(this.value);" /></td>
									<td>MS.</td>
									<input type="hidden" id="gender" name="gender" value="<?php echo $_SESSION['tmp_customer']['gender']; ?>" />
								</tr>
							</table>
						</td>
					</tr>
					<tr><td>COMPANY NAME*</td></tr>
					<tr><td><input type="text" class="textbox" name="company" id="company" value="<?php echo $_SESSION['tmp_customer']['company'] ?>" /></td></tr>
					<tr><td>FIRST NAME*</td></tr>
					<tr><td><input type="text" class="textbox" name="fname" id="fname" value="<?php echo $_SESSION['tmp_customer']['fname']; ?>" /></td></tr>
					<tr><td>LAST NAME*</td></tr>
					<tr><td><input type="text" class="textbox"  name="lname" id="lname" value="<?php echo $_SESSION['tmp_customer']['lname']; ?>" /></td></tr>
					<tr><td>Address Line 1*</td></tr>
					<tr><td><input type="text" class="textbox" id="address" name="address" value="<?php echo $_SESSION['tmp_customer']['address']; ?>" /></td></tr>
					<tr><td>ADDRESS LINE 2*</td></tr>
					<tr><td><input type="text" class="textbox" id="suburb"  name="suburb" value="<?php echo $_SESSION['tmp_customer']['suburb']; ?>" /></td></tr>
					<tr><td>CITY*</td></tr>
					<tr><td><input type="text" class="textbox" id="city"  name="city" value="<?php echo $_SESSION['tmp_customer']['city']; ?>" /></td></tr>
					<tr>
						<td>
							<table>
								<tr><td>STATE/PROVINCE*</td><td>&nbsp;</td><td>POST/ZIP*</td></tr>
								<tr>
									<td>
										<div id="_state">
											<input type="text" class="textboxsmall" id="state" name="state" value="<?php echo $_SESSION['tmp_customer']['state']; ?>" />
										</div>
									</td>
									<td>&nbsp;</td>
									<td><input type="text" class="textboxsmall" id="zip" name="zip" value="<?php echo $_SESSION['tmp_customer']['zip']; ?>" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td>COUNTRY*</td></tr>
					<tr><td>
						<select name='countries' class="textbox" onchange="get_state(this.value, false, true); get_shipping();  loadProducts(document.getElementById('category').value, this.value)" id="countries">
							<option value=""> - Select a Country - </option>
						<?php
							foreach(zen_get_countries() as $country){
								if($_SESSION['tmp_customer']['country']==$country['countries_id']) $selected = "selected";
								else $selected = "";
								
								echo "<option value='".$country['countries_id']."' $selected>".$country['countries_name']."</option>";
							}
						?>
						</select>
					</td></tr>
				</table>
				
			</div>
			<div class="contentrightline"></div>
			<div class="leftadditionaldetails">
				<table>
					<tr>
						<td class="tdtitle">ADDITIONAL CONTACT DETAILS*</td>
					</tr>
					<tr>
						<td height="28">&nbsp;</td>
					</tr>
					<tr><td>TELEPHONE NO.*</td></tr>
					<tr><td><input type="text" class="textbox" name="phone" id="phone" value="<?php echo $_SESSION['tmp_customer']['phone']; ?>" /></td></tr>
					<tr><td>FAX NO.*</td></tr>
					<tr><td><input type="text" class="textbox" name="fax" id="fax" value="<?php echo $_SESSION['tmp_customer']['fax']; ?>" /></td></tr>
					<tr><td>SKYPE ACCOUNT*</td></tr>
					<tr><td><input type="text" class="textbox" id="skype" name="skype" value="<?php echo $_SESSION['tmp_customer']['skype']; ?>" /></td></tr>
					<tr><td>BEST TIME TO CALL*</td></tr>
					<tr><td><input type="text" class="textbox" id="best" name="best" value="<?php echo $_SESSION['tmp_customer']['best']; ?>" /></td></tr>
					<tr>
						<td>
							<table>
								<tr><td></td><td>DATE OF BIRTH*</td></tr>
								<tr>
									<td>VERIFY YOUR AGE&nbsp;</td>
									<td><input type="zip" name="dbirth" class="textboxmedium"  onblur="if (this.value == '') {this.value = '(eg. 05/21/1970)';}" onfocus="if (this.value == '(eg. 05/21/1970)') {this.value = '';}" id="dob" <?php if(empty($_SESSION['customer_id'])){ ?> value="(eg. 05/21/1970)" <?php }else{ ?> value="<?php echo $_SESSION['tmp_customer']['dob']; }?>" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="contentrightline"></div>
			<div class="leftlogindetails">
				<table>
					<tr>
						<td class="tdtitle">LOGIN DETAILS*</td>
					</tr>
					<tr>
						<td height="28">&nbsp;</td>
					</tr>
					<tr><td>USERNAME / EMAIL*</td></tr>
					<tr><td><input type="text" name="uname" id="email" class="textbox" value="<?php echo $_SESSION['tmp_customer']['email']; ?>" /></td></tr>
					<tr><td>PASSWORD*</td></tr>
					<tr><td><input class="textbox" type="password"  id="password"  name="password" value="<?php echo $_SESSION['tmp_customer']['password']; ?>" <?php echo $disabled_text; ?> /></td></tr>
					<tr><td>CONFIR MPASSWORD*</td></tr>
					<tr><td><input class="textbox" type="password" id="repassword" name="repassword" value="<?php echo $_SESSION['tmp_customer']['password']; ?>" <?php echo $disabled_text;?> /></td></tr>
					<tr>
						<td>
							<table>
								<tr>
									<td><input type="checkbox" value="true" name="set_same" <?php echo ($_SESSION['tmp_customer']['same']==true) ? "checked" : "" ?> id="set_same" onclick="set_billing()" checked="true" /></td>
									<td>Billing Address the same customer address.</td>
									<input type="hidden" value="<?php echo (empty($_SESSION['tmp_customer']['same']) ? true : $_SESSION['tmp_customer']['same']) ?>" id="same" name="same" />
								</tr>
							</table>
						</td>
					</tr>
					<tr><td height="28">&nbsp;</td></tr>
					<tr>
						<td align="right">
							<input type="reset" name="clear" value=" " class="clearbutton"/>
							<input type="button" name="next" id="next" value=" " class="submitbutton" onclick="return prepare();"/>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</div>