<?php
	$con = mysql_connect('localhost', 'kdci_agentbridge', 'BbJdo3ho7cBt') or die(mysql_error());
	$db = mysql_select_db('kdci_agentbridge', $con) or die(mysql_error());
	
	$countries = array();
	
	$sql = mysql_query("SELECT `countries_id`, `countries_name` FROM tbl_countries ORDER BY `countries_name`");
	while($r = mysql_fetch_assoc($sql)){
		$countries[$r['countries_id']] = $r['countries_name'];
	}
	
?>

<script>
	var $ajax = jQuery.noConflict();
	function get_state(cID){
		$ajax("#state").html('');
		$ajax.ajax({
			url: 'custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID },
			success: function(e){
				$ajax("#state").html(e);
			}
		});
	}
	
	function get_broker(b){
		var $country = $ajax("#jform_country").val();
		var $state = $ajax("#jform_state").val();
		var $city = $ajax("#jform_city").val();
		
		if(b.length > 0){ 
			$ajax("#broker").html("searching...");
			$ajax.ajax({
				url: 'custom/_get_broker.php',
				type: 'POST',
				data: { 'broker': b, 'cID': $country, 'state': $state, 'city': $city },
				success: function(e){
					$ajax("#broker").html(e);
				}
			});
		}
	}
	
</script>

<form id="register_nrds" action="" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
<input type="hidden" name="jform[form]" value="nrds_registration" />
<input type="hidden" name="jform[manual]" value="true" />	
	
	<div class="full-radius wide-container account-setup"><!-- start account setup -->
		<div class="forms-head">
			<h2>AgentBridge Account Setup</h2>
		</div>
		<div class="user clear">
			<div class="account-setup-bottom"> <br />
				<br />
				<br />
				<div class="acct_setup_holder">
					<label>First Name</label>
					<input type="text" id="jform_fname" class="required text-input" aria-required="true" required="required" name="jform[fname]" value="" />
					<br />
					
					<label>Last Name</label>
					<input type="text" id="jform_lname" class="required text-input" aria-required="true" required="required" name="jform[lname]" value="" />
					<br />
					
					<label>License</label>
					<input type="text" id="jform_licence" class="required text-input" aria-required="true" required="required" name="jform[licence]" value="" />
					<br />
					
					<label>Email</label>
					<input type="text" id="jform_email" class="required text-input" aria-required="true" required="required" name="jform[email]" value="" />
					<br />
					
					<label>Work Phone</label>
					<input type="text" id="jform_wphone" class="required text-input" aria-required="true" required="required" name="jform[wphone][]" value="" style="width:150px;" />
					<br />
					
					<label class="clear-float">Country</label>
					<select id="jform_country" class="required" aria-required="true" required="required" name="jform[country]" onchange="get_state(this.value)">
						<option value=""></option>
						<?php 
							foreach($countries as $cID=>$cName){
								echo "<option value=\"$cID\">$cName</option>";
							}
						?>
					</select>
					<br />
					
					<label class="clear-float">Address 1</label>
					<input type="text" id="jform_straddress" class="required text-input" aria-required="true" required="required" name="jform[straddress]" value="" />
					<br />
					
					<label>Address 2</label>
					<input type="text" id="jform_suburb" class="text-input" name="jform[suburb]" value="" />
					<br />

					<label  class="clear-float">City</label>
					<input type="text" id="jform_city" class="required text-input" aria-required="true" required="required" name="jform[city]" value="" />
					<br />
					
					<label  class="clear-float">State</label>
					<div id="state">
						<input type="text" id="jform_state" class="required text-input" aria-required="true" required="required" name="jform[state]" value="" />
					</div>
					<br />
					
					<label class="clear-float">Zip/ Postal Code</label>
					<input type="text" id="jform_zip" class="required text-input" aria-required="true" required="required" name="jform[zip]" value="" style="width:100px" />
					
					<label class="clear-float">Brokerage</label>
					<input type="text" class="text-input" id="jform_brokerage" name="jform[brokerage]" value="" onkeyup="get_broker(this.value);" autocomplete="off" />
					
					<br /><div style="clear: both;"></div>	
					<div id="broker" style="margin-top: 10px; margin-left: 118px; line-height: 27px;"></div>		
					<br />
				</div>
			</div>
		</div>
		<div class="account-setup-bottom clear-float clear"> 
		<button type="submit" class="btn btn-primary validate button gradient-blue left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Register&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>		
		<a href=" " class="text-link left"> Cancel</a> </div>
	</div>
	<!-- end account setup -->
	
</form>