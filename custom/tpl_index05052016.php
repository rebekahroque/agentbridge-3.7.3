<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.formvalidation');
	
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	$user =& JFactory::getUser($_SESSION['user_id']);

	$mainframe=JFactory::getApplication();
	
	if($user->id > 0){
		$mainframe->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
	}
	
	if(isset($_GET['notme'])){
		JFactory::getApplication()->enqueueMessage('Please click on the No NRDS# text link to manually register.');	
		$mainframe->redirect(JRoute::_('index.php'));
	}
	
	if(isset($_GET['illegal'])){
		//JError::raiseWarning( 100, 'Login Required.<br />Please set up an account with AgentBridge or simply log in to access the site listings.' );
		//$mainframe->redirect(JRoute::_('index.php'));
		$mainframe->redirect(JRoute::_('index.php'), "Please set up an account with AgentBridge or simply log in to access the site listings.", "Login Required", false);
	}
	
	// CONFIRM ACCOUNT SETUP BUTTON
	if($_POST['jform']['form']=="nrds_registration"){
		
		$data = array();
		
		// PREPARE POST VARIABLES
		foreach($_POST['jform'] as $key=>$value){
			if(is_array($value)){
				foreach($value as $k=>$v){
					$data[$key][$k] = trim(addslashes($v));
				}
			}
			else{
				$data[$key] = trim(addslashes($value));
			}
		}
		extract($data);
		
		//convert country to numeric
		if(!is_numeric($country)){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('countries_id')
			->from('#__countries')
			->where('countries_name = \''.$country.'\'');
			$db->setQuery($query);
			$object = $db->loadObject();
			$country = $object->countries_id;
		}
		$db = JFactory::getDbo();
		
		//try converting state too
		$query = $db->getQuery(true);
		$query->select('group_concat(distinct(zone_country_id)) as cntry')
		->from('#__zones');
		$db->setQuery($query);
		$object = $db->loadObject();
		$list = explode(',',$object->cntry);
		
		if(in_array($country, $list)){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('zone_id')
			->from('#__zones')
			->where('zone_name = \''.$state.'\'');
			$db->setQuery($query);
			$object = $db->loadObject();
			$state = $object->zone_id;
		}
		
		// INITIALIZE DATABASE CONNECTION		
		$db = JFactory::getDbo();				
		$query = $db->getQuery(true);
				
		// CHECK FOR DUPLICATE ENTRIES
		$query->select(array('*'));
		$query->from('#__user_registration');
		$query->where('licence LIKE \''.$licence.'\' OR email LIKE \''.$email.'\'');

		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		if(!empty($results[0]->reg_id)){
			JFactory::getApplication()->enqueueMessage('Click on Forget Password to retrieve your password.', "Duplicate Entry" );
			//JError::raiseWarning( 100, 
			
		}
		else{		
			
			if(!empty($licence)){
				
				$n_explode = explode(" ", $name);
				$n_lname = array();
				$fname = (empty($fname) ? $n_explode[0] : $fname);
				for($i=1; $i<count($n_explode); $i++){
					$n_lname[] = $n_explode[$i];
				}	
				$lname = (empty($lname) ? implode(" ", $n_lname) : $lname);
				$waddress = (empty($waddress) ? $straddress : $waddress);
			
				// SAVE TO USER REGISTRATION			
				$insert = new JObject();
				
				$insert->firstname = $fname;
				$insert->lastname = $lname;
				$insert->imagelink = $imagelink;
				$insert->licence = $licence;
				$insert->pending_date = $pending;
				$insert->wphone = serialize($wphone);
				$insert->wfax = serialize($wfax);
				$insert->email = $email;
				$insert->blog = $blog;
				$insert->street_address = $waddress;
				$insert->suburb = $suburb;
				$insert->city = $city;
				$insert->zip = $zip;
				$insert->state = $state;
				$insert->country = $country;
				$insert->socialsite = $socialsite;
				$insert->brokerage = $brokerage;
				$insert->registration_date = date('Y-m-d h:i:s');
				$insert->status = $status;
													
				//Insert new record into user_registration table.
				$ret = $db->insertObject('#__user_registration', $insert);
				
				$query2 = $db->getQuery(true);			
				$query2->select(array('reg_id'));
				$query2->from('#__user_registration');
				//$query->where('email LIKE \''.$email.'\'');
				$query2 ->order('reg_id DESC LIMIT 1');
				
				$db->setQuery($query2);
				$results = $db->loadObjectList();
				
				// NOTIFY THE AGENT
				if($ret){
				
					if(empty($manual)){
						
						$body = "";
						
						$body .= "Hi " . $name . ",<br /><br />";
						$body .= "Thank you for registering to AgentBridge. <br /><br />";
						$body .= "Start using AgentBridge now.<br /><br /><a href='http://keydiscoveryinc.com/agentbridge/index.php?option=com_setpass&action=activated&regid=".$results[0]->reg_id."'>http://keydiscoveryinc.com/agentbridge/index.php?option=com_setpass&action=activated&regid=".$results[0]->reg_id."</a>.<br />";
						$body .= "<br /><br />";
						$body .= "- AgentBridge Team";
					
						$email = $email;
						$subject = "AgentBridge Signup Confirmation!";
					}
					else{
						
						$body = "";
						
						$body .= "Hello Admin,<br /><br />";
						$body .= "You have a new application.<br /><br />";
						$body .= "
							<table>
								<tr>
									<td>First Name: </td>
									<td>$fname</td>
								</tr>
								<tr>
									<td>Last Name: </td>
									<td>$lname</td>
								</tr>
								<tr>
									<td>Licence: </td>
									<td>$licence</td>
								</tr>
								<tr>
									<td>Work Phone: </td>
									<td>".@implode('<br />', $wphone)."</td>
								</tr>
								<tr>
									<td>Work Fax: </td>
									<td>".@implode('<br />', $wfax)."</td>
								</tr>
								<tr>
									<td>Email: </td>
									<td>$email</td>
								</tr>
								<tr>
									<td>Address Line 1: </td>
									<td>$waddress</td>
								</tr>
								<tr>
									<td>Address Line 2: </td>
									<td>$suburb</td>
								</tr>
								<tr>
									<td>City: </td>
									<td>$city</td>
								</tr>
								<tr>
									<td>Zip/ Postal Code: </td>
									<td>$zip</td>
								</tr>
								<tr>
									<td>State: </td>
									<td>$state</td>
								</tr>
								<tr>
									<td>Country: </td>
									<td>$country</td>
								</tr>
								<tr>
									<td>Brokerage: </td>
									<td>$brokerage</td>
								</tr>
							</table>
						";
						$body .= "<br /><br />";
						$body .= "To approve the application please log in to <a href=\"http://keydiscoveryinc.com/agentbridge/administrator/index.php?option=com_useractivation\">http://keydiscoveryinc.com/agentbridge/administrator/index.php?option=com_useractivation</a>";
						
						$email = 'dyeysi09@yahoo.com.ph';
						$subject = 'New Inquiry - ' . $fname . ' ' . $lname;
					}
					
					
					
					$mailSender =& JFactory::getMailer();
					$mailSender ->addRecipient( $email );
					$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
					$mailSender ->setSubject( $subject );
					$mailSender ->isHTML(  true );
					$mailSender ->setBody(  $body );

					if ($mailSender ->Send())
					{
						if(empty($manual)){
							JFactory::getApplication()->enqueueMessage('An activation link has been sent to your email address. Please follow the instructions on the email to complete your registration.');
						}
						else{
							JFactory::getApplication()->enqueueMessage('Thank you for your interest to join AgentBridge.<br /> Please check your email for further instructions.');
						}
					}
					else{
						JError::raiseWarning( 100, 'Email not sent!' );
					}
				}
				else{
					JError::raiseWarning( 100, 'Cannot save to database!' );
				} 
			
			}
			else{
				JError::raiseWarning( 100, 'Please enter your licence #!' );
			}
		} 
	
	}
	
?>

<script>
	var $find = jQuery.noConflict();
	
	function find_me(){
		$find("#searching").html('<center>Searching. Please wait...<br /><img src="images/loading.gif" /></center>');
	
		$find.ajax({
			url: 'custom/tpl_get_agents.php',
			type: 'POST',
			data: { 'licence': $find("#jform_nrds").val() },
			success: function(msg){
				if(msg==""){
					$find("#find").append("<div id=\"system-message-container\">" +
												"<div id=\"system-message\">" +
												"<div class=\"alert alert-error\"><a data-dismiss=\"alert\" class=\"close\">&times;</a>" +
												"<h4 class=\"alert-heading\">Not Found</h4>" +
												"<div>" +
														"<p>No records found! Please click on the 'Don't have an NRDS#?' text link to register manually.</p>" +
												"</div>" +
												"</div>" +
												"</div>" +
											"</div>");
					$find("#searching").html('');						
				}
				else{
					$find("#searching").html('');
					$find("#find").html(msg);
				}
			}
		});
	}
	
	function register(){
		$find("#find").html('<center>Loading. Please wait...<br /><img src="images/loading.gif" /></center>');
		$find.ajax({
			url: 'custom/tpl_manual_nrds.php',
			type: 'POST',
			success: function(e){
				$find("#find").html(e);
			}
		});
	}
	
	function ndrs(e){
		e.select();
		e.style.color='#000'		
	}
	
	function no_ndrs(e){
		if(e.value==""){	
			e.style.color = "#ccc";
			e.value="Enter NRDS#";
		}
	}
	
</script>

<div id="searching"></div>
	
<div id="find">		
	<div class="home-form clear">	
		<div class="box1 full-radius">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-inline">
			
				<h2>Sign In</h2>
				<div>
					<input id="modlgn-username" type="text" name="username" class="input-small text-input" tabindex="0" size="18" required="required" placeholder="Enter Your Email" />
				</div>
				<div>
					<input id="modlgn-passwd" type="password" name="password" class="input-small text-input enter-pword"  required="required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
					<input name="" value="Sign In" type="submit" class="button sign-in-btn btn btn-primary validate "/>
				</div>
				<div class="clear" style="padding-bottom: 3px">
					<input type="checkbox" name="chbox" id=""> <label>Remember Me </label>
					<a href="index.php/component/users/?view=reset" class="text-link" style="float: right; margin-right: 20px; margin-top: 2px;"> &#8226;Forgot Your Password?</a>
				</div>
			
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.login" />
				<input type="hidden" name="return" value="<?php echo base64_encode('index.php?option=com_userprofile'); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
			</form> 	
		
		</div>
			
		<div class="box1 full-radius">
			<form id="find-nrds" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" class="form-validate form-horizontal"><!-- start form -->
		
				<h2><label title="Please enter your NRDS#" class="required" for="jform_nrds" id="jform_nrds-lbl">New to AgentBridge?:</label></h2>
				<div>
					<input onblur="no_ndrs(this)" onfocus="ndrs(this)" style="color: #ccc" type="text" size="30" class="required" value="Enter NRDS#" id="jform_nrds" name="jform[nrds]" aria-required="true" required="required" />
				</div>
				<div>
					<input name="" value="Find Me" type="button" onclick="find_me()" class="button find-me"/>
				</div>
				<div><a href="<?php echo JRoute::_('index.php?option=com_nrds'); ?>" class="text-link">Don’t have an NRDS#?</a></div>
				<?php echo JHtml::_('form.token');?>
			</form><!-- end form -->	
		</div>
	</div>
</div>