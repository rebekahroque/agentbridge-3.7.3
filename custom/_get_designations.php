<?php
	require_once '../configuration.php';
	
	$config = new JConfig;	$mysqli = new mysqli($config->host, $config->user, $config->password, $config->db);
	
	$zones = array();
	
	$sql = $mysqli->query("SELECT * FROM tbl_designations WHERE `country_available` = '".$_POST['country']."'");
	while($r = $sql->fetch_assoc()){
		$designations[$r['id']] = $r['designations'];
	}
	
	$designation = unserialize($_POST['designations']);
	
	if(count($designation) > 0){
		$designation = $designation;
	}
	else{
		$designation = array();
	}	
	
	echo "<div style=\"display: table; width: 330px;\">";	
	
	if(count($designations) > 0):
		foreach($designations as $key=>$value):
			if(@in_array($key, $designation)) $checked = "checked='true'";
			else $checked = ""; 
			
?>	
	<div style="display: table-row; width: 330px; margin-bottom: 10px;">
		<div style="display: table-cell; float: left; width: 40px; text-align: center; vertical-align: middle;">
			<input type="checkbox" name="jform[designation][]" value="<?php echo $key; ?>" <?php echo $checked; ?> /> 
		</div>
		<div style="display: table-cell; float: left; width: 280px; text-align: left; margin-right: 10px; vertical-align: middle; padding-top: 3px;">
			<?php echo $value; ?>
		</div>
	</div>
	<div style="clear: both;"></div>
<?php
		endforeach;
	else:

		echo "<br /><span style='color: red;'>No Designations & Certification found on this country...</span>";
		
	endif;
?>
	</div>