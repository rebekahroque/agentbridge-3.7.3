<?php
	require_once '../configuration.php';
	$config = new JConfig;
	$mysqli = new mysqli($config->host, $config->user, $config->password, $config->db);
	
	$zones = array();
	
	$sql = $mysqli->query("SELECT `zone_id`, `zone_name`, `zone_code` FROM tbl_zones WHERE `zone_country_id` = '".$_POST['cID']."'");
	while($r = $sql->fetch_assoc()){
		$zones[$r['zone_id']]['zone_name'] = $r['zone_name'];
		$zones[$r['zone_id']]['zone_code'] = $r['zone_code'];
	}
	
	$state = $_POST['state'];
	
	if(count($zones) > 0){
?>
		<select  id="jform_state2" name="jform[state]">
		
			<?php 
				foreach($zones as $zID => $zone){
					$selected = "";
			?>
			<option id="zone<?php echo $zone['zone_code'];?>" value="<?php echo $zID; ?>" <?php echo $selected; ?>><?php echo $zone['zone_name']; ?></option>
			<?php	
				}
			?>
		</select>
        
<?php
	}
	else{
?>	
	<select  id="jform_state2" name="jform[state]">
	</select>
<?php
	}
?>