<?php
	require_once '_utilities.php';
	$xml = simplexml_load_file("../agents.xml");	
	
	$result = array();
	
	foreach($xml->agent as $agent){
		if($agent->licence==$_POST['licence']){
			$result = $agent;
		}
		else{
			continue;
		} 

	}
	
	 if(count($result) > 0){

?>	
	<form id="register_nrds" action="" method="post" class="form-validate form-horizontal">
	<input type="hidden" name="jform[form]" value="nrds_registration" />
	
	<div class="full-radius wide-container account-setup"><!-- start account setup -->
		<div class="forms-head">
			<h2>AgentBridge Account Setup</h2>
			<p class="sub-head">Is this you?</p>
			<p>Please verify and update your information.</p>
		</div>

		<div class="user clear">
				<span class="user-thumb clear">
					<img src="<?php echo $result->imagelink; ?>" alt="<?php echo $result->name; ?>" />
					<input type="hidden" name="jform[imagelink]" value="<?php echo $result->imagelink; ?>" />
				</span>
				<span class="user-name">
					<?php echo $result->name; ?>
					<input type="hidden" name="jform[name]" value="<?php echo $result->name; ?>" />
				</span>
				<em class="license">
					<span>Licence:</span> 
					<?php echo $result->licence; ?> 
					<input type="hidden" name="jform[licence]" value="<?php echo $result->licence; ?>" /> 
				</em>
				<em class="pending">
					<span>Pending as of:</span> 
					<?php 
						preg_match('/(\d\d\d\d)-(\d\d)-(\d\d)/', $result->pending, $date);
						echo @date('F d, Y', mktime(0, 0, 0, $date[2], $date[3], $date[1]));
					?>
					<input type="hidden" name="jform[pending]" value="<?php echo $result->pending; ?>" />
				</em>
		</div>

		<div class="user-details">
			<div class="details-left left">Work Phone: </div>
			<div class="details-right left">
				<?php
					echo isValid("/\([0-9]{1,}\) ?[0-9]+-[0-9]+/", $result->wphone, "wphone");
				?>
			</div>

			<div class="details-left left">Work Fax:</div>
			<div class="details-right left">
				<?php
					echo isValid("/\([0-9]{1,}\) ?[0-9]+-[0-9]+/", $result->wfax, "wfax");
				?>
			</div>
			<div class="detail-full"><!--a href="#" class="text-link">Add Information</a--></div>

			<div class="details-left left">Email:</div>
			<div class="details-right left">
				<a href="<?php echo $result->email; ?>" class="text-link-dark"><?php echo $result->email; ?></a>
				<input type="hidden" name="jform[email]" value="<?php echo $result->email; ?>" />
			</div>

			<div class="details-left left">Blog:</div>
			<div class="details-right left">
				<a href="<?php echo $result->blog; ?>" class="text-link-dark"><?php echo $result->blog; ?></a>				
				<input type="hidden" name="jform[blog]" value="<?php echo $result->blog; ?>" />
			</div>

			<div class="details-left left">Address:</div>
			<div class="details-right left">
				<?php echo $result->waddress; ?>&nbsp;
				<input type="hidden" name="jform[waddress]" value="<?php echo $result->waddress; ?>" />
				
				<?php echo $result->suburb; ?>
				<input type="hidden" name="jform[suburb]" value="<?php echo $result->suburb; ?>" />
				
				<?php echo $result->city; ?>,&nbsp;
				<input type="hidden" name="jform[city]" value="<?php echo $result->city; ?>" />
				
				<?php echo $result->state; ?>&nbsp;
				<input type="hidden" name="jform[state]" value="<?php echo $result->state; ?>" />
				
				<?php echo $result->zip; ?>&nbsp;
				<input type="hidden" name="jform[zip]" value="<?php echo $result->zip; ?>" />
				
				<?php echo $result->country; ?>
				<input type="hidden" name="jform[country]" value="<?php echo $result->country; ?>" />
			</div>
			<div class="detail-full"><!--a href="#" class="text-link">Add New Address</a--></div>
			
			
			<div class="details-left left">Social Site:</div>
			<div class="details-right left">
				<a href="<?php echo $result->socialsite; ?>" class="text-link-dark"><?php echo $result->socialsite; ?></a>
				<input type="hidden" name="jform[socialsite]" value="<?php echo $result->socialsite; ?>" />
			</div>
			<div class="detail-full"><!--a href="#" class="text-link">Add Social Site</a--></div>
		</div>
		
		<input type="hidden" name="jform[status]" value="1" />

		<div class="account-setup-bottom">
			<a href="javascript: void(document.getElementById('register_nrds').submit())" class="button gradient-blue left">Confirm Account Setup</a>
			<a href="index.php?notme=1" class="text-link left">This is not me</a>
		</div>

	</div><!-- end account setup -->
	
	</form>
	
<?php

	}
	
?>
