<?php include('_dbconn.php');

//$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);


    
    
  $con = $mysqli = new mysqli("localhost", $username, $password, $database);

    $mysqli->set_charset('utf8');



$user_id = $_GET["user_id"];

    $query = "SELECT `tbl_country_currency`.*,`tbl_users`.*,`tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`firstname`, `tbl_user_registration`.`lastname`, `tbl_user_registration`.`image`, `tbl_broker`.`broker_name`, `tbl_user_registration`.`brokerage_license`, `tbl_user_registration`.`street_address`, `tbl_user_registration`.`suburb`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_zones`.`zone_code`  AS `state_code`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`zip`, `user_mobile_numbers`.`mobile_numbers` AS `mobile_number`, `user_mobile_numbers`.`main` AS `mobile_number_main`, `user_mobile_numbers`.`show` AS `mobile_number_show`, `tbl_user_registration`.`email`, `tbl_user_registration`.`about`, `tbl_user_registration`.`licence`,  `tbl_user_registration`.`status`, `tbl_user_registration`.`registration_date`, `tbl_user_registration`.`is_premium`, `tbl_user_registration`.`activation_status`, `tbl_user_registration`.`tax_id_num`, `pocket_listing`.`zipcodes`, `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`is_term_accepted`
    FROM `tbl_users`
    LEFT JOIN `tbl_user_registration` ON `tbl_user_registration`.`email` = `tbl_users`.`email`
    LEFT JOIN `tbl_broker` ON `tbl_user_registration`.`brokerage` = `tbl_broker`.`broker_id`
    LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
    LEFT JOIN `tbl_country_currency` ON `tbl_user_registration`.`country` = `tbl_country_currency`.`country`
    LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
    LEFT JOIN (SELECT `tbl_user_mobile_numbers`.`user_id`,`tbl_user_mobile_numbers`.`show`, `tbl_user_mobile_numbers`.`main`, GROUP_CONCAT(DISTINCT `tbl_user_mobile_numbers`.`value`) AS `mobile_numbers`
          FROM `tbl_user_mobile_numbers`
          GROUP BY `tbl_user_mobile_numbers`.`user_id`) AS `user_mobile_numbers` ON `tbl_user_registration`.`user_id` = `user_mobile_numbers`.`user_id`
    LEFT JOIN (SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
          FROM `tbl_pocket_listing`
          GROUP BY `tbl_pocket_listing`.`user_id`) AS `pocket_listing` ON `pocket_listing`.`user_id` = `tbl_users`.`id`

/*    LEFT JOIN (SELECT `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`firstname`, `tbl_user_registration`.`lastname`, `tbl_user_registration`.`image`, `tbl_broker`.`broker_name`, `tbl_user_registration`.`brokerage_license`, `tbl_user_registration`.`street_address`, `tbl_user_registration`.`suburb`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_zones`.`zone_code`  AS `state_code`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`zip`, `user_mobile_numbers`.`mobile_numbers` AS `mobile_number`, `user_mobile_numbers`.`main` AS `mobile_number_main`, `user_mobile_numbers`.`show` AS `mobile_number_show`, `tbl_user_registration`.`email`, `tbl_user_registration`.`about`, `tbl_user_registration`.`licence`,  `tbl_user_registration`.`status`, `tbl_user_registration`.`registration_date`, `tbl_user_registration`.`is_premium`, `tbl_user_registration`.`activation_status`, `tbl_user_registration`.`tax_id_num`, `tbl_user_sales`.`ave_price_2012`, `tbl_user_sales`.`volume_2012`, `tbl_user_sales`.`sides_2012`, `pocket_listing`.`zipcodes`, `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`is_term_accepted`
               FROM `tbl_user_registration`
               LEFT JOIN `tbl_user_sales` ON `tbl_user_registration`.`user_id` = `tbl_user_sales`.`agent_id`
         LEFT JOIN `tbl_broker` ON `tbl_user_registration`.`brokerage` = `tbl_broker`.`broker_id`
               LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
               LEFT JOIN (SELECT `tbl_user_mobile_numbers`.`user_id`,`tbl_user_mobile_numbers`.`show`, `tbl_user_mobile_numbers`.`main`, GROUP_CONCAT(DISTINCT `tbl_user_mobile_numbers`.`value`) AS `mobile_numbers`
                          FROM `tbl_user_mobile_numbers`
                          GROUP BY `tbl_user_mobile_numbers`.`user_id`) AS `user_mobile_numbers` ON `tbl_user_registration`.`user_id` = `user_mobile_numbers`.`user_id`
               LEFT JOIN `tbl_users` ON `tbl_users`.`email` = `tbl_user_registration`.`email`
               LEFT JOIN (SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
                          FROM `tbl_pocket_listing`
                          GROUP BY `tbl_pocket_listing`.`user_id`) AS `pocket_listing` ON `pocket_listing`.`user_id` = `tbl_users`.`id`) AS `user_info` ON  `user_info`.`email`=`tbl_users`.`email`
   */ 
    WHERE `tbl_users`.`id` = '$user_id'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Info Found", 'data'=>array());
}
else {
	 $rows = array();    
   $rows_u = array();
  while ($r = $result->fetch_assoc())
  {
    $rows[] = $r;
  }
  foreach ($rows as $key => $value) {
        $this_line=$value;
        foreach ($value as $key2 => $value2) {
            if($key2=='firstname'){
                $this_line['firstname']=stripslashes($value2);
            }
            if($key2=='lastname'){
                $this_line['lastname']=stripslashes($value2);
            }
            if($key2=='zipcodes'){
                $value2=explode(",",$value2);                
                $this_line['zipcodes']=implode(", ",$value2);
            }
        }

        $user_details = get_profile_info($user_id);


        $current_user_info_currency = "USD";
        $current_user_info_symbol = "$";

        if($user_details->currency!=$current_user_info_currency){
          $ex_rates_con = $ex_rates->rates->{$user_details->currency};
          $ex_rates_can = $ex_rates->rates->{$current_user_info_currency};
          if($user_info->currency=="USD"){                  
            //$user->price1=$listing->price1 * $ex_rates_CAD;
            $user_details->ave_price_2012=$user_details->ave_price_2012 * $ex_rates_can;
            $user_details->ave_price_2013=$user_details->ave_price_2013 * $ex_rates_can;
            $user_details->ave_price_2014=$user_details->ave_price_2014 * $ex_rates_can;
            $user_details->ave_price_2015=$user_details->ave_price_2015 * $ex_rates_can;
            $user_details->volume_2012=$user_details->volume_2012 * $ex_rates_can;
            $user_details->volume_2013=$user_details->volume_2013 * $ex_rates_can;
            $user_details->volume_2014=$user_details->volume_2014 * $ex_rates_can;
            $user_details->volume_2015=$user_details->volume_2015 * $ex_rates_can;
          } else {
            //$user->price1=$listing->price1 / $ex_rates_CAD;
            //$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
            $user_details->ave_price_2012=($user_details->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
            $user_details->ave_price_2013=($user_details->ave_price_2013 / $ex_rates_con)*$ex_rates_can;
            $user_details->ave_price_2014=($user_details->ave_price_2014 / $ex_rates_con)*$ex_rates_can;
            $user_details->ave_price_2015=($user_details->ave_price_2015 / $ex_rates_con)*$ex_rates_can;
            $user_details->volume_2012=($user_details->volume_2012 / $ex_rates_con)*$ex_rates_can;
            $user_details->volume_2013=($user_details->volume_2013 / $ex_rates_con)*$ex_rates_can;
            $user_details->volume_2014=($user_details->volume_2014 / $ex_rates_con)*$ex_rates_can;
            $user_details->volume_2015=($user_details->volume_2015 / $ex_rates_con)*$ex_rates_can;
          }
          $this_line['currency'] = $current_user_info_currency;
          $this_line['symbol'] = $current_user_info_symbol;
        }


        $this_line['ave_price_2012']=$user_details->ave_price_2012;
        $this_line['ave_price_2013']=$user_details->ave_price_2013;
        $this_line['ave_price_2014']=$user_details->ave_price_2014;
		$this_line['ave_price_2015']=$user_details->ave_price_2015;
        $this_line['volume_2012']=$user_details->volume_2012;
        $this_line['volume_2013']=$user_details->volume_2013;
        $this_line['volume_2014']=$user_details->volume_2014;
		$this_line['volume_2015']=$user_details->volume_2015;
        $this_line['sides_2012']=$user_details->sides_2012;
        $this_line['sides_2013']=$user_details->sides_2013;
        $this_line['sides_2014']=$user_details->sides_2014;
		$this_line['sides_2015']=$user_details->sides_2015;
        $this_line['verified_2012']=$user_details->verified_2012;
        $this_line['verified_2013']=$user_details->verified_2013;
        $this_line['verified_2014']=$user_details->verified_2014;
		$this_line['verified_2015']=$user_details->verified_2015;


        $this_line['image'] = strpos($this_line['image'], JURI::base()) !== false ? str_replace("loads/", "loads/", $this_line['image']).'?'.microtime(true) :  JURI::base().'uploads/'.$this_line['image'].'?'.microtime(true);


        $rows_u[]=$this_line;
    }

  $response = array('status'=>1, 'message'=>"Found User Info", 'data'=>$rows_u);
//	$response = array('status'=>1, 'message'=>"Found User Info", 'data'=>$rows);
}



  function get_profile_info($userid, $own = false){

    $db = JFactory::getDbo();

    $query = $db->getQuery(true);

    $query->select("u.*, conc.*,

      b.broker_name, 

      z.zone_code, 

      con.*, 

      b.zone_id, 

      b.broker_id, 

      ur.tax_id_num,

      us.*, ur.*,temptable.*");

    $query->from('#__users u');

    $query->where('id = '.$userid);

    $query->leftJoin('#__user_registration ur ON u.email = ur.email');

    $query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');

    $query->leftJoin('#__zones z ON ur.state = z.zone_id');

    $query->leftJoin('#__country_currency conc ON conc.country = z.zone_country_id');

    $query->leftJoin('#__countries con ON con.countries_id = z.zone_country_id');

    $query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
    

    //$query->leftJoin('(select temptable1.user_id, group_concat(temptable1.value) from (select user_id, value from #__user_work_numbers where user_id = '.$userid.' limit 3) temptable1 ) temptable ON temptable.user_id = '.$userid);

    $query->leftJoin('(

      SELECT 

        temptable1.newid,

        temptable1.user_id, 

        group_concat(temptable1.value)  as newcontact

      FROM 

        (

          SELECT 

            us.id as newid,

            ss.user_id, 

            ss.value 

          FROM 

            #__user_work_numbers ss

          LEFT JOIN 

            #__user_registration ur

          ON 

            ss.user_id = ur.user_id

          LEFT JOIN

            #__users us 

          ON 

            us.email = ur.email

          WHERE

            us.id = '.$userid.' 

          LIMIT 3

        ) temptable1 

      ) temptable 

      ON 

        temptable.newid = '.$userid

    );

  //  echo $query; die();

    $db->setQuery($query);


    $stripped_name= stripslashes($db->loadObject()->name);

    $stripped_firstname = stripslashes($db->loadObject()->firstname);

    $stripped_lastname = stripslashes($db->loadObject()->lastname);

    $db->loadObject()->lastname = $stripped_lastname ;


    $ret = $db->loadObject();

    $ret->name = $stripped_name;

    $ret->firstname = $stripped_firstname;

    $ret->lastname = $stripped_lastname;

    return $ret;

  }

echo json_encode($response);

?>