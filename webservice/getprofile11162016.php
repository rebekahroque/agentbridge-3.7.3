<?php
    
    $username = "agentb_dev";
    $password = "N)r@5ZT/nUe>YRqk4SD@zt/%";
    $database = "agentb_main";
    
	//mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);

$profile_image_prefix = "http://qa.agentbridge.com/uploads/";
$live_profile_image_prefix = "https://www.agentbridge.com/uploads/";


$user_id = $_GET["user_id"];

    $query = "SELECT `user_info`.*
    FROM `tbl_users`
    LEFT JOIN (SELECT `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`firstname`, `tbl_user_registration`.`lastname`, `tbl_user_registration`.`image`, `tbl_broker`.`broker_name`, `tbl_user_registration`.`brokerage_license`, `tbl_user_registration`.`street_address`, `tbl_user_registration`.`suburb`, `tbl_user_registration`.`city`, `tbl_user_sales`.`volume_2012`, `tbl_user_sales`.`sides_2012`, `tbl_user_sales`.`ave_price_2012`, `tbl_user_sales`.`verified_2012`, `tbl_user_sales`.`volume_2013`, `tbl_user_sales`.`sides_2013`, `tbl_user_sales`.`ave_price_2013`, `tbl_user_sales`.`verified_2013`, `tbl_user_sales`.`volume_2014`, `tbl_user_sales`.`sides_2014`, `tbl_user_sales`.`ave_price_2014`, `tbl_user_sales`.`verified_2014`, `tbl_user_sales`.`volume_2015`, `tbl_user_sales`.`sides_2015`, `tbl_user_sales`.`ave_price_2015`, `tbl_user_sales`.`verified_2015`,`tbl_zones`.`zone_name` AS `state_name`, `tbl_zones`.`zone_code`  AS `state_code`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`zip`, `user_mobile_numbers`.`mobile_numbers` AS `mobile_number`, `user_mobile_numbers`.`main` AS `mobile_number_main`, `user_mobile_numbers`.`show` AS `mobile_number_show`, `tbl_user_registration`.`email`,`tbl_user_registration`.`licence`, `tbl_user_registration`.`is_premium`, `tbl_user_registration`.`tax_id_num`,`pocket_listing`.`zipcodes`, `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`is_term_accepted`
               FROM `tbl_user_registration`
               LEFT JOIN `tbl_broker` ON `tbl_user_registration`.`brokerage` = `tbl_broker`.`broker_id`
			   LEFT JOIN `tbl_user_sales` ON `tbl_user_registration`.`user_id` = `tbl_user_sales`.`agent_id`
               LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
               LEFT JOIN (SELECT `tbl_user_mobile_numbers`.`user_id`,`tbl_user_mobile_numbers`.`show`, `tbl_user_mobile_numbers`.`main`, GROUP_CONCAT(DISTINCT `tbl_user_mobile_numbers`.`value`) AS `mobile_numbers`
                          FROM `tbl_user_mobile_numbers`
                          GROUP BY `tbl_user_mobile_numbers`.`user_id`) AS `user_mobile_numbers` ON `tbl_user_registration`.`user_id` = `user_mobile_numbers`.`user_id`
               LEFT JOIN `tbl_users` ON `tbl_users`.`email` = `tbl_user_registration`.`email`
               LEFT JOIN (SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
                          FROM `tbl_pocket_listing`
                          GROUP BY `tbl_pocket_listing`.`user_id`) AS `pocket_listing` ON `pocket_listing`.`user_id` = `tbl_users`.`id`) AS `user_info` ON  `user_info`.`email`=`tbl_users`.`email`
    WHERE `tbl_users`.`id` = '$user_id'";

$result = $mysqli->query($query) or die($mysqli->error);

$num = $result->num_rows;

$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No User Info Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		if(
			$r['image'] !== "" && 
			(
				strpos($r['image'], $profile_image_prefix) === false &&
				strpos($r['image'], $live_profile_image_prefix) === false
			)
		) {
			$r['image'] = $profile_image_prefix . $r['image'];
		}
		
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found User Info", 'data'=>$rows);
}

echo json_encode($response);

?>