<?php

	function encrypt($plain_text) {
	
		$key = 'password to (en/de)crypt';
			
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
		
	}

	function decrypt($encrypted_text) {
		$key = 'password to (en/de)crypt';
		
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		
		return $decrypted;
	}
	
	
	$licence = "OMG-WTF-BBQ";
	//echo "Plain Text: " . $licence . "<br />";
	$encrypted = encrypt($licence);
	//echo "Encrypted: " . $encrypted . "<br />";
	$decrypted = decrypt($encrypted);
	//echo "Decrypted: " . $decrypted . "<br />";
	$blank = "";
	$blank_decrypted = decrypt($blank);
	
	$arr_outputs = array();
	$arr_outputs['plaintext'] = $licence;
	$arr_outputs['encrypted'] = $encrypted;
	$arr_outputs['decrypted'] = $decrypted;
	$arr_outputs['blank'] = $blank;
	$arr_outputs['blank_decrypted'] = $blank_decrypted;
	echo json_encode($arr_outputs, JSON_UNESCAPED_UNICODE);
	echo json_last_error_msg();
	
	//echo "Blank Decrypted: " . decrypt($blank) . "<br />";
	