<?php
    
    include('_dbconn.php');
    
    //mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);

$user_id = $_GET["user_id"];

$query = "SELECT `tbl_referral`.`referral_id`, `tbl_referral`.`agent_a`, `tbl_referral`.`agent_b`, `tbl_users`.`name` AS `agent_name`, `user_data`.`city`, `user_data`.`state_code`, `user_data`.`state_name`, `user_data`.`countries_name`, `user_data`.`countries_iso_code_3`, `tbl_referral`.`client_id`, `tbl_referral`.`price_1`, `tbl_referral`.`price_2`, `tbl_referral`.`referral_fee`, `tbl_referral`.`status`, `tbl_referral`.`property_id`, `tbl_referral`.`date`, `tbl_referral`.`client_intention`, `tbl_referral`.`counter`, `tbl_referral`.`docusign_envelope`, `tbl_referral`.`reason`, `tbl_referral`.`countered`,`user_data`.`image`, `buyer_data`.`client_name`, `buyer_data`.`email` AS `client_email`, `buyer_data`.`number` AS `client_number`, `buyer_data`.`address_1` AS `client_address_1`, `buyer_data`.`address_2` AS `client_address_2`, `buyer_data`.`city` AS `client_city`, `buyer_data`.`zip` AS `client_zip`, `buyer_data`.`countries_name` AS `client_country_name`, `buyer_data`.`countries_iso_code_3` AS `client_country_code`, `buyer_data`.`zone_code` AS `client_state_code`, `buyer_data`.`zone_name` AS `client_state_name`
FROM `tbl_referral`
LEFT JOIN `tbl_users` ON `tbl_referral`.`agent_b` = `tbl_users`.`id`
    LEFT JOIN (
               SELECT `tbl_buyer`.`buyer_id`, `tbl_buyer`.`name` AS `client_name`, `tbl_buyer_email`.`email`, `tbl_buyer_mobile`.`number`, `tbl_buyer_address`.`address_1`, `tbl_buyer_address`.`address_2`, `tbl_buyer_address`.`city`, `tbl_buyer_address`.`zip`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_zones`.`zone_code`, `tbl_zones`.`zone_name`
               FROM `tbl_buyer`
               LEFT JOIN `tbl_buyer_email` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_email`.`buyer_id`
               LEFT JOIN `tbl_buyer_mobile` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_mobile`.`buyer_id`
               LEFT JOIN `tbl_buyer_address` ON `tbl_buyer`.`buyer_id` = `tbl_buyer_address`.`buyer_id`
               LEFT JOIN `tbl_countries` ON `tbl_buyer_address`.`country` = `tbl_countries`.`countries_id`
               LEFT JOIN `tbl_zones` ON `tbl_buyer_address`.`state` = `tbl_zones`.`zone_id` AND `tbl_buyer_address`.`country` = `tbl_zones`.`zone_country_id`
               ) `buyer_data` ON `tbl_referral`.`client_id` = `buyer_data`.`buyer_id`
LEFT JOIN (
			SELECT `tbl_users`.`id` AS `user_id`, `tbl_user_registration`.`user_id` AS `profile_id`, `tbl_user_registration`.`city`, `tbl_zones`.`zone_code` AS `state_code`, `tbl_zones`.`zone_name` AS `state_name`, `tbl_user_registration`.`country` AS `country_id`, `tbl_countries`.`countries_name`, `tbl_countries`.`countries_iso_code_3`, `tbl_user_registration`.`image`
			FROM `tbl_user_registration`
			INNER JOIN `tbl_users` ON `tbl_user_registration`.`email` = `tbl_users`.`email`
			LEFT JOIN `tbl_countries` ON `tbl_user_registration`.`country` = `tbl_countries`.`countries_id`
           LEFT JOIN `tbl_zones` ON `tbl_user_registration`.`state` = `tbl_zones`.`zone_id` AND `tbl_user_registration`.`country` = `tbl_zones`.`zone_country_id`
			) AS `user_data` ON `tbl_referral`.`agent_b` = `user_data`.`user_id`
WHERE `tbl_referral`.`agent_a`='$user_id'";

	$result = $mysqli->query($query) or die($mysqli->error);
	$num = $result->num_rows;
	//$result = mysql_query($query) or die(mysql_error());
	//$num = mysql_num_rows($result);

	//mysql_close();
	$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Referrals Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Referrals", 'data'=>$rows);
}

echo json_encode($response);

?>