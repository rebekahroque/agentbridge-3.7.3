<?php
    
    include('_dbconn.php');
    
	//mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);

    $id = $_GET["token_id"];

    $query = "DELETE FROM `tbl_user_device_token`
    WHERE `token_id` = $id
    LIMIT 1";

    $result = $mysqli->query($query) or die($mysqli->error);
    
    $num = $mysqli->affected_rows;
    
    $mysqli->close();
    
    $response = array();
    if($num == 0) {
        $response = array('status'=>0, 'message'=>"Failed to Remove Device Token", 'data'=>array());
    }
    else {
        
        $response = array('status'=>1, 'message'=>"Successfully Removed Device Token", 'data'=>array());
    }
    
    echo json_encode($response);

?>