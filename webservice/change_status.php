<?php
    
    include('_dbconn.php');
    
	//mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);
    
    date_default_timezone_set('US/Pacific');
    $user_id = $_GET["user_id"];
    $referral_id = $_GET["referral_id"];
    $value_id = $_GET["value_id"];
    $status = $_GET["status"];
    $activity_type = $_GET["activity_type"];
    $agent_a = $_GET["agent_a"];
    $note_text = $_GET["note"];
    $date_now = date("Y-m-d H:i:s");
    $buyer_id = $_GET['buyer_id'];

    $query1 = "INSERT INTO `tbl_referral_status_update` (`referral_id`, `status`, `value_id`, `created_date`, `note`, `response`, `edited_by`, `created_by`) VALUES ('$referral_id', '$status', '$value_id', '$date_now', '$note_text', 0, 0, '$user_id')";

$result1 = $mysqli->query($query1) or die($mysqli->error);
    
$num1 = $mysqli->affected_rows;
    
    
    $queryUpdateID = "SELECT `update_id` FROM `tbl_referral_status_update`
    WHERE `tbl_referral_status_update`.`referral_id` = '$referral_id' AND `tbl_referral_status_update`.`status` = '$status' AND `tbl_referral_status_update`.`value_id` = '$value_id' AND `tbl_referral_status_update`.`created_date` = '$date_now' AND `tbl_referral_status_update`.`created_by` = '$user_id'";
    
    $resultUpdateID = $mysqli->query($queryUpdateID) or die($mysqli->error);
    while($row = $resultUpdateID->fetch_assoc())
    {
        $update_id = $row["update_id"];
    }
    
    $query2 = "UPDATE `tbl_referral`
    SET `tbl_referral`.`status`= '$status', `tbl_referral`.`date`= '$date_now'
    WHERE `tbl_referral`.`referral_id`='$referral_id'";
    
$result2 = $mysqli->query($query2) or die($mysqli->error);
    
$num2 = $mysqli->affected_rows;
    
    $query3 = "INSERT INTO `tbl_activities` (`user_id`, `activity_type`, `date`, `other_user_id`, `activity_id`, `buyer_id`, `has_multiples`)
     VALUES ('$user_id', '$activity_type', '$date_now', '$agent_a', '$update_id','$buyer_id',0)";
    
$result3 = $mysqli->query($query3) or die($mysqli->error);
    
$num3 = $mysqli->affected_rows;

    
    $query4 = "INSERT INTO `tbl_activities` (`user_id`, `activity_type`, `date`, `other_user_id`, `activity_id`, `buyer_id`, `has_multiples`)
     VALUES ('$agent_a', '$activity_type', '$date_now', '$user_id', '$update_id','$buyer_id',0)";
    
$result4 = $mysqli->query($query4) or die($mysqli->error);
    
$num4 = $mysqli->affected_rows;
    
$mysqli->close();

if($num1 == 0 || $num2 == 0 || $num3 == 0 || $num4 == 0) {
	$response = array('status'=>0, 'message'=>"Failed in Changing Status");
}
else {
	
	$response = array('status'=>1, 'message'=>"Successful in Changing Status");
}

echo json_encode($response);

?>