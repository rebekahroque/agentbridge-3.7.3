<?php
    
    include('_dbconn.php');
    
    //mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);

	$mysqli->set_charset('utf8');

$buyer_id = $_GET["buyer_id"];

    $query = "SELECT `pops_data`.*
    FROM `tbl_buyer_saved_listing`
    LEFT JOIN (SELECT `tbl_pocket_listing`.`listing_id`, `tbl_pocket_listing`.`property_type` AS `type_property_type`, `tbl_pocket_listing`.`property_name`, `tbl_property_type`.`type_name`, `tbl_pocket_listing`.`sub_type`, `tbl_property_sub_type`.`name` AS `sub_type_name`, `tbl_property_price`.`price_type`, `tbl_property_price`.`price1`, `tbl_property_price`.`price2`, `tbl_property_price`.`disclose`, `tbl_pocket_listing`.`expiry`, `tbl_pocket_listing`.`user_id`, `tbl_users`.`name`, `tbl_pocket_listing`.`city`, `tbl_pocket_listing`.`zip`, `tbl_pocket_listing`.`state`, `tbl_pocket_listing`.`bedroom`, `tbl_pocket_listing`.`bathroom`, `tbl_pocket_listing`.`unit_sqft`, `tbl_pocket_listing`.`view`, `tbl_pocket_listing`.`style`, `tbl_pocket_listing`.`year_built`, `tbl_pocket_listing`.`pool_spa`, `tbl_pocket_listing`.`condition`, `tbl_pocket_listing`.`garage`, `tbl_pocket_listing`.`units`, `tbl_pocket_listing`.`cap_rate`, `tbl_pocket_listing`.`grm`, `tbl_pocket_listing`.`occupancy`, `tbl_pocket_listing`.`type`, `tbl_pocket_listing`.`listing_class`, `tbl_pocket_listing`.`parking_ratio`, `tbl_pocket_listing`.`ceiling_height`, `tbl_pocket_listing`.`stories`, `tbl_pocket_listing`.`room_count`, `tbl_pocket_listing`.`type_lease`, `tbl_pocket_listing`.`type_lease2`, `tbl_pocket_listing`.`available_sqft`, `tbl_pocket_listing`.`lot_sqft`, `tbl_pocket_listing`.`lot_size`, `tbl_pocket_listing`.`bldg_sqft`, `tbl_pocket_listing`.`term`, `tbl_pocket_listing`.`furnished`, `tbl_pocket_listing`.`pet`, `tbl_pocket_listing`.`possession`, `tbl_pocket_listing`.`zoned`, `tbl_pocket_listing`.`bldg_type`, `tbl_pocket_listing`.`features1`, `tbl_pocket_listing`.`features2`, `tbl_pocket_listing`.`features3`, `tbl_pocket_listing`.`setting`, `tbl_pocket_listing`.`description` AS `desc`, `tbl_pocket_listing`.`closed`, `tbl_pocket_listing`.`date_created`, `tbl_pocket_listing`.`date_expired`, `tbl_property_price`.`pocket_id`
               
               FROM `tbl_pocket_listing`
               INNER JOIN `tbl_property_type` ON `tbl_pocket_listing`.`property_type` = `tbl_property_type`.`type_id`
               INNER JOIN `tbl_property_sub_type` ON `tbl_pocket_listing`.`sub_type` = `tbl_property_sub_type`.`sub_id`
               INNER JOIN `tbl_property_price` ON `tbl_pocket_listing`.`listing_id` = `tbl_property_price`.`pocket_id`
               LEFT JOIN `tbl_users` ON `tbl_pocket_listing`.`user_id` = `tbl_users`.`id`) AS `pops_data` ON `tbl_buyer_saved_listing`.`listing_id` = `pops_data`.`listing_id`
WHERE `tbl_buyer_saved_listing`.`buyer_id`='$buyer_id'";

$result = $mysqli->query($query) or die($mysqli->error);
$num = $result->num_rows;
//$result = mysql_query($query) or die(mysql_error());
//$num = mysql_num_rows($result);

//mysql_close();
$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		switch($r['type_name']) {
			case "COM_POPS_PROPTYPE_RESP":
				$r['type_name'] = "Residential Purchase";
				break;
			case "COM_POPS_PROPTYPE_RESL":
				$r['type_name'] = "Residential Lease";
				break;
			case "COM_POPS_PROPTYPE_COMP":
				$r['type_name'] = "Commercial Purchase";
				break;
			case "COM_POPS_PROPTYPE_COML":
				$r['type_name'] = "Commercial Lease";
				break;
		}
		
		switch($r['sub_type_name']) {
			case "COM_POPS_PROPSTYPE_RESP_SFR":
				$r['sub_type_name'] = "SFR";
				break;
			case "COM_POPS_PROPSTYPE_RESP_CONDO":
				$r['sub_type_name'] = "Condo";
				break;
			case "COM_POPS_PROPSTYPE_RESP_TOWN":
				$r['sub_type_name'] = "Townhouse/Row House";
				break;
			case "COM_POPS_PROPSTYPE_RESP_LAND":
				$r['sub_type_name'] = "Land";
				break;
			case "COM_POPS_PROPSTYPE_RESL_SFR":
				$r['sub_type_name'] = "SFR";
				break;
			case "COM_POPS_PROPSTYPE_RESL_CONDO":
				$r['sub_type_name'] = "Condo";
				break;
			case "COM_POPS_PROPSTYPE_RESL_TOWN":
				$r['sub_type_name'] = "Townhouse/Row House";
				break;
			case "COM_POPS_PROPSTYPE_RESL_LAND":
				$r['sub_type_name'] = "Land";
				break;
			case "COM_POPS_PROPSTYPE_COMP_MULTI":
				$r['sub_type_name'] = "Multi-family";
				break;
			case "COM_POPS_PROPSTYPE_COMP_OFFICE":
				$r['sub_type_name'] = "Office";
				break;
			case "COM_POPS_PROPSTYPE_COMP_RETAIL":
				$r['sub_type_name'] = "Retail";
				break;
			case "COM_POPS_PROPSTYPE_COMP_IND":
				$r['sub_type_name'] = "Industrial";
				break;
			case "COM_POPS_PROPSTYPE_COMP_ASSIST":
				$r['sub_type_name'] = "Assisted Care";
				break;
			case "COM_POPS_PROPSTYPE_COMP_SPEC":
				$r['sub_type_name'] = "Special Purpose";
				break;
			case "COM_POPS_PROPSTYPE_COMP_MOTEL":
				$r['sub_type_name'] = "Motel/Hotel";
				break;
			case "COM_POPS_PROPSTYPE_COML_OFFICE":
				$r['sub_type_name'] = "Office";
				break;
			case "COM_POPS_PROPSTYPE_COML_IND":
				$r['sub_type_name'] = "Industrial";
				break;
			case "COM_POPS_PROPSTYPE_COML_RETAIL":
				$r['sub_type_name'] = "Retail";
				break;
		}
		
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
}

echo json_encode($response);

?>