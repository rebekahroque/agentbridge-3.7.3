<?php
  // Get Joomla! framework define( '_JEXEC', 1 ); 
  define( '_JEXEC', 1 );
  define( 'DS', DIRECTORY_SEPARATOR );
//define( 'JPATH_BASE', $_SERVER[ 'DOCUMENT_ROOT' ] );
  define( 'JPATH_BASE', str_replace("webservice", "", dirname(__FILE__)) );

//var_dump();

  require_once( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
  require_once( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
  require_once( JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'factory.php' );
  $mainframe =& JFactory::getApplication('site');
  $mainframe->initialise(); 


//$user = JFactory::getUser();
////$session = JFactory::getSession();
//$session->set('user', new JUser($user->id));
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
// $language_tag = JFactory::getUser()->currLanguage;
$language_tag = "english-US";
$language->load($extension, $base_dir, $language_tag, true);


$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
JModelLegacy::addIncludePath( $userprofilemodel );
$userprof_model =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

$popmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
JModelLegacy::addIncludePath( $popmodel );
$propertylist_model =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query = "SET SESSION group_concat_max_len = 1000000;";

$db->setQuery($query);           
$result = $db->execute();
        
$forqa = 0;


#ini_set('display_errors',1);
#ini_set('display_startup_errors',1);
#error_reporting(-1);

if($forqa){

	$username = "forqa_dev";
    $password = "82N[nw,p!hXA4)\\j^.%/z6u*";
    $database = "forqa_dev";

} else {

	$username = "agentb_dev";
    $password = "N)r@5ZT/nUe>YRqk4SD@zt/%";
    $database = "agentb_main";
    
}



function getExchangeRates_indi(){
    $file = 'latest.json';
    $appId = 'd73f8525552048a7a39aaac9977299fd';
    // Open CURL session:
  //  $ch = curl_init("https://openexchangerates.org/api/{$file}?app_id={$appId}");
  //  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // Get the data:
   // $json = curl_exec($ch);
   // curl_close($ch);
    // Decode JSON response:
    $json ='{
  "disclaimer": "Exchange rates provided for informational purposes only and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, no guarantees are made of accuracy, validity, availability, or fitness for any purpose. All usage subject to acceptance of Terms: https://openexchangerates.org/terms/",
  "license": "Data sourced from various providers; resale prohibited; no warranties given of any kind. All usage subject to License Agreement: https://openexchangerates.org/license/",
  "timestamp": 1464609615,
  "base": "USD",
  "rates": {
    "AED": 3.67299,
    "AFN": 68.860001,
    "ALL": 124.115901,
    "AMD": 477.822502,
    "ANG": 1.78875,
    "AOA": 165.785501,
    "ARS": 13.90769,
    "AUD": 1.391736,
    "AWG": 1.793333,
    "AZN": 1.4864,
    "BAM": 1.756776,
    "BBD": 2,
    "BDT": 78.59963,
    "BGN": 1.756991,
    "BHD": 0.377014,
    "BIF": 1565.5,
    "BMD": 1,
    "BND": 1.379538,
    "BOB": 6.88973,
    "BRL": 3.611666,
    "BSD": 1,
    "BTC": 0.001852476205,
    "BTN": 67.145965,
    "BWP": 11.237163,
    "BYR": 19746.5,
    "BZD": 2.009863,
    "CAD": 1.303547,
    "CDF": 926.5,
    "CHF": 0.993137,
    "CLF": 0.024598,
    "CLP": 687.513003,
    "CNY": 6.577207,
    "COP": 3070.916634,
    "CRC": 535.781,
    "CUC": 1,
    "CUP": 1,
    "CVE": 98.892233,
    "CZK": 24.26733,
    "DJF": 177.83625,
    "DKK": 6.678672,
    "DOP": 45.92526,
    "DZD": 110.495921,
    "EEK": 14.0601,
    "EGP": 8.878312,
    "ERN": 14.9985,
    "ETB": 21.76232,
    "EUR": 0.897533,
    "FJD": 2.1245,
    "FKP": 0.684386,
    "GBP": 0.684386,
    "GEL": 2.15044,
    "GGP": 0.684386,
    "GHS": 3.86553,
    "GIP": 0.684386,
    "GMD": 42.71224,
    "GNF": 7357.475098,
    "GTQ": 7.62942,
    "GYD": 206.525002,
    "HKD": 7.768253,
    "HNL": 22.63502,
    "HRK": 6.725502,
    "HTG": 62.462975,
    "HUF": 281.948201,
    "IDR": 13631.183333,
    "ILS": 3.846438,
    "IMP": 0.684386,
    "INR": 67.15196,
    "IQD": 1170.38295,
    "IRR": 30311.5,
    "ISK": 125.092,
    "JEP": 0.684386,
    "JMD": 124.794001,
    "JOD": 0.708872,
    "JPY": 110.953601,
    "KES": 100.78514,
    "KGS": 68.268999,
    "KHR": 4072.474976,
    "KMF": 437.224247,
    "KPW": 899.91,
    "KRW": 1189.759982,
    "KWD": 0.30233,
    "KYD": 0.824407,
    "KZT": 336.534992,
    "LAK": 8118.774903,
    "LBP": 1510.5,
    "LKR": 147.760999,
    "LRD": 90.49095,
    "LSL": 15.76086,
    "LTL": 3.047079,
    "LVL": 0.627876,
    "LYD": 1.357113,
    "MAD": 9.76111,
    "MDL": 19.96093,
    "MGA": 3237.633317,
    "MKD": 55.3005,
    "MMK": 1188.075025,
    "MNT": 1994.833333,
    "MOP": 8.00084,
    "MRO": 355.914998,
    "MTL": 0.683602,
    "MUR": 35.342262,
    "MVR": 15.243333,
    "MWK": 708.785018,
    "MXN": 18.47213,
    "MYR": 4.11119,
    "MZN": 56.66,
    "NAD": 15.76086,
    "NGN": 199.09,
    "NIO": 28.5151,
    "NOK": 8.347575,
    "NPR": 107.4816,
    "NZD": 1.492222,
    "OMR": 0.385008,
    "PAB": 1,
    "PEN": 3.35072,
    "PGK": 3.12305,
    "PHP": 46.7245,
    "PKR": 104.790999,
    "PLN": 3.944875,
    "PYG": 5660.433333,
    "QAR": 3.64061,
    "RON": 4.045271,
    "RSD": 110.66236,
    "RUB": 65.94726,
    "RWF": 746.5025,
    "SAR": 3.75025,
    "SBD": 7.806624,
    "SCR": 13.18769,
    "SDG": 6.08865,
    "SEK": 8.330552,
    "SGD": 1.380749,
    "SHP": 0.684386,
    "SLL": 3951.5,
    "SOS": 591.480003,
    "SRD": 6.425643,
    "STD": 21984.75,
    "SVC": 8.744138,
    "SYP": 219.255665,
    "SZL": 15.76396,
    "THB": 35.73678,
    "TJS": 7.869,
    "TMT": 3.5015,
    "TND": 2.10229,
    "TOP": 2.277098,
    "TRY": 2.956214,
    "TTD": 6.64671,
    "TWD": 32.62568,
    "TZS": 2190.93335,
    "UAH": 25.16878,
    "UGX": 3363.516667,
    "USD": 1,
    "UYU": 31.1839,
    "UZS": 2936.814942,
    "VEF": 9.9735,
    "VND": 22410,
    "VUV": 111.987501,
    "WST": 2.556476,
    "XAF": 589.888084,
    "XAG": 0.061977,
    "XAU": 0.0008285,
    "XCD": 2.70102,
    "XDR": 0.712238,
    "XOF": 592.426084,
    "XPD": 0.001853,
    "XPF": 107.287501,
    "XPT": 0.00102,
    "YER": 249.317001,
    "ZAR": 15.76171,
    "ZMK": 5252.024745,
    "ZMW": 10.398325,
    "ZWL": 322.387247
  }
}';
    $exchangeRates = json_decode($json);
    // You can now access the rates inside the parsed object, like so:
    /*printf(
        "1 %s in GBP: %s (as of %s)",
        $exchangeRates->base,
        $exchangeRates->rates->GBP,
        date('H:i jS F, Y', $exchangeRates->timestamp)
    );*/
    return $exchangeRates;
  }


 $ex_rates = getExchangeRates_indi();
