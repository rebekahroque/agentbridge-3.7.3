<?php
    
    include('_dbconn.php');
    
	//mysql_connect('localhost',$username,$password);
	//@mysql_select_db($database) or die("Error");
	
	$mysqli = new mysqli("localhost", $username, $password, $database);

    $query = "SELECT `tbl_pocket_listing`.`user_id`, GROUP_CONCAT(DISTINCT `tbl_pocket_listing`.`zip`) AS `zipcodes`
    FROM `tbl_pocket_listing`
    GROUP BY `tbl_pocket_listing`.`user_id`";

	$result = $mysqli->query($query) or die($mysqli->error);
	$num = $result->num_rows;
	//$result = mysql_query($query) or die(mysql_error());
	//$num = mysql_num_rows($result);

	//mysql_close();
	$mysqli->close();

$response = array();
if($num == 0) {
	$response = array('status'=>0, 'message'=>"No Zipcodes Found", 'data'=>array());
}
else {
	$rows = array();
	while ($r = $result->fetch_assoc())
	{
		$rows[] = $r;
	}
	
	$response = array('status'=>1, 'message'=>"Found Zipcodes", 'data'=>$rows);
}

echo json_encode($response);

?>