<?php

function pushCSSHead(){

	/*
	<script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
	<script src="/media/system/js/core.js" type="text/javascript"></script>
	<script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>
	<script src="/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
	<script src="/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
	<script src="/media/system/js/punycode.js" type="text/javascript"></script>
	<script src="/media/system/js/validate.js" type="text/javascript"></script>
	<script src="/media/jui/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/templates/agentbridge/scripts/jquery.html5-placeholder-shim.js" type="text/javascript"></script>



			header("Link: </media/system/js/mootools-core.js>; rel=preload; as=script;,
				  </media/system/js/core.js>; rel=preload; as=script;,
				  </media/jui/js/jquery.min.js>; rel=preload; as=script;,
				  </media/jui/js/jquery-noconflict.js>; rel=preload; as=script;,
				  </media/jui/js/jquery-migrate.min.js>; rel=preload; as=script;,
				  </media/system/js/punycode.js>; rel=preload; as=script;,
				  </media/system/js/validate.js>; rel=preload; as=script;,
				  </media/jui/js/bootstrap.min.js>; rel=preload; as=script;,
				  </templates/agentbridge/scripts/jquery.html5-placeholder-shim.js>; rel=preload; as=script;", false);
	*/




	header("Link: </media/system/js/mootools-core.js>; rel=preload; as=script;", false);
	header("Link: </media/system/js/core.js>; rel=preload; as=script;", false);
	header("Link: </media/jui/js/jquery.min.js>; rel=preload; as=script;", false);
	header("Link: </media/jui/js/jquery-noconflict.js>; rel=preload; as=script;", false);
	header("Link: </media/jui/js/jquery-migrate.min.js>; rel=preload; as=script;", false);
	header("Link: </media/system/js/punycode.js>; rel=preload; as=script;", false);
	header("Link: </media/system/js/validate.js>; rel=preload; as=script;", false);
	header("Link: </media/jui/js/bootstrap.min.js>; rel=preload; as=script;", false);
	header("Link: </templates/agentbridge/scripts/jquery.html5-placeholder-shim.js>; rel=preload; as=script;", false);




}

pushCSSHead();

function pushCSS($uri,$media="") {
header("Link: <{$uri}>; rel=preload; as=style;", false);
 $media_attr = "";
if($media){
  $media_attr = "media='".$media."'";
}
return '<link rel="stylesheet" type="text/css" href="'.$uri.'" '.$media_attr.'/>';
}


function pushScript($uri,$charset="") {
header("Link: <{$uri}>; rel=preload; as=script;", false);
$charset_attr="";
if($charset){
  $charset_attr = "charset='".$charset."'";
}

return '<script type="text/javascript" src="'.$uri.'" '.$charset_attr.' ></script>';
}


function pushImage($uri,$usemap="") {
header("Link: <{$uri}>; rel=preload; as=script;", false);
$usemap_attr="";
if($usemap){
  $usemap_attr = "usemap='".$usemap."'";
}

return '<img src="'.$uri.'" '.$usemap.' />';
}


function format_currency_global($val,$sym = "$",$currency=""){

	setlocale(LC_ALL, ''); // Locale will be different on each system.

	$locale = localeconv();

	if($val<1){
		$val=1;
	}

	if($currency){
		$currency=" ".$currency;
	}

	return  $sym. number_format($val, 0, '.', ',').$currency;

}



function format_currency_invoice_global($val){

	setlocale(LC_ALL, ''); // Locale will be different on each system.

	$locale = localeconv();

	return  '$'. number_format(((int)$value), 2, '.', ',');

}





function format_name_global($name){

	$first_2 = substr($name, 0, 2);

	$middle = str_repeat('X', (strlen($name)-4));

	$last_2 =  substr($name, -2, 2);



	return $first_2.$middle.$last_2;

}



function trim_desc_global($str){

	return substr($str, 0, 100)."...";

}

function stripslashes_all($str){
	
	$newStr = str_replace('\\', '', stripslashes($str));

 	return $newStr;

}

function convert_number_to_words_global($number) {



	$hyphen      = '-';

	$conjunction = ' and ';

	$separator   = ', ';

	$negative    = 'negative ';

	$decimal     = ' point ';

	$dictionary  = array(

			0                   => 'zero',

			1                   => 'one',

			2                   => 'two',

			3                   => 'three',

			4                   => 'four',

			5                   => 'five',

			6                   => 'six',

			7                   => 'seven',

			8                   => 'eight',

			9                   => 'nine',

			10                  => 'ten',

			11                  => 'eleven',

			12                  => 'twelve',

			13                  => 'thirteen',

			14                  => 'fourteen',

			15                  => 'fifteen',

			16                  => 'sixteen',

			17                  => 'seventeen',

			18                  => 'eighteen',

			19                  => 'nineteen',

			20                  => 'twenty',

			30                  => 'thirty',

			40                  => 'fourty',

			50                  => 'fifty',

			60                  => 'sixty',

			70                  => 'seventy',

			80                  => 'eighty',

			90                  => 'ninety',

			100                 => 'hundred',

			1000                => 'thousand',

			1000000             => 'million',

			1000000000          => 'billion',

			1000000000000       => 'trillion',

			1000000000000000    => 'quadrillion',

			1000000000000000000 => 'quintillion'

	);



	if (!is_numeric($number)) {

		return false;

	}



	if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {

		// overflow

		trigger_error(

		'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,

		E_USER_WARNING

		);

		return false;

	}



	if ($number < 0) {

		return $negative . convert_number_to_words(abs($number));

	}



	$string = $fraction = null;



	if (strpos($number, '.') !== false) {

		list($number, $fraction) = explode('.', $number);

	}



	switch (true) {

		case $number < 21:

			$string = $dictionary[$number];

			break;

		case $number < 100:

			$tens   = ((int) ($number / 10)) * 10;

			$units  = $number % 10;

			$string = $dictionary[$tens];

			if ($units) {

				$string .= $hyphen . $dictionary[$units];

			}

			break;

		case $number < 1000:

			$hundreds  = $number / 100;

			$remainder = $number % 100;

			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];

			if ($remainder) {

				$string .= $conjunction . convert_number_to_words($remainder);

			}

			break;

		default:

			$baseUnit = pow(1000, floor(log($number, 1000)));

			$numBaseUnits = (int) ($number / $baseUnit);

			$remainder = $number % $baseUnit;

			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];

			if ($remainder) {

				$string .= $remainder < 100 ? $conjunction : $separator;

				$string .= convert_number_to_words($remainder);

			}

			break;

	}



	if (null !== $fraction && is_numeric($fraction)) {

		$string .= $decimal;

		$words = array();

		foreach (str_split((string) $fraction) as $number) {

			$words[] = $dictionary[$number];

		}

		$string .= implode(' ', $words);

	}



	return $string;

}



?>