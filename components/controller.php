<?php

/**

 * @version     1.0.0

 * @package     com_customsearch

 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com */
 
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class CustomsearchController extends JControllerLegacy
{
	private $data;
	function display()
	{
		$view = $this->getView($this->getTask(), 'html');
		
		foreach ($this->data as $key => $value){
			$view->set($key, $value);
		}
		$view->display();
	}
	
	function searchlisting(){

		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		 
		$userprofile2model = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofile2model );

		$userPmodel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

		JModelLegacy::addIncludePath( $propmodel );

		$modelPropertyListing =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$temptext = $_REQUEST['refine'];



		$_REQUEST['refine'] = explode("|", $_REQUEST['refine']);

		$_REQUEST['searchword'] =explode(",",$_REQUEST['searchword'])[0];

		if (!(strpos($_REQUEST['searchword'],' ') !== false) && !$_REQUEST['refine'] && strlen($_REQUEST['searchword']) >= 6) {
		   $_REQUEST['searchword'] = substr($_REQUEST['searchword'], 0, 3) . ' ' . substr($_REQUEST['searchword'], 3);
		}

		$_REQUEST['searchword'] = strtolower($_REQUEST['searchword']);
		$_REQUEST['searchword'] = stripslashes_all($_REQUEST['searchword']);
		$_REQUEST['searchword'] = addslashes($_REQUEST['searchword']);

		#print_r($_REQUEST);


		if(isset($_REQUEST['sort_items'])){
			$order_by=$_REQUEST['sort_items'];
		} else {
			$order_by="DESC";
		}
		
		#echo "<pre>", print_r($_REQUEST), "</pre>";
		$query_string = "";
		foreach($_REQUEST as $key => $value) {
			$query_string .= ($query_string) ? "&" : "";
			if(is_array($value)) {
				if($key == "refine") {
					$query_string .= $key . "=" . str_replace(" ", "+", $value[0]);
				} else {
					foreach($value as $_key => $_value) {
						if(is_array($_value)) {
							foreach($_value as $__key => $__value) {
								$query_string .= $key . "[" . $_key . "][]=" . str_replace(" ", "+", $__value);
							}
						} else {
							$query_string .= $key . "[" . $_key . "]=" . str_replace(" ", "+", $_value);
						}					
					}
				}				
			} else {
				$query_string .= $key . "=" . str_replace(" ", "+", $value);
			}
		}
		
		#echo "<br />" . $query_string;


		$user =& JFactory::getUser($_SESSION['user_id']);
		$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];	
		
		$userDetails = $userPmodel->get_user_registration(JFactory::getUser()->id);
		
		
		if(empty($uid)) JFactory::getApplication()->redirect('index.php?illegal=1');
		
		$model = $this->getModel($this->getTask());
		$this->data = array();
		$this->data['page'] = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;	

		if(isset($_REQUEST['jform']) && $_REQUEST['jform']['name']=='refine_search' && !empty($_REQUEST['fields'])){
			$searchreturn_count = $model->searchDb($_REQUEST['searchword'], $this->data['page'], true, $_REQUEST['fields'], $order_by);
			$searchreturn = $model->searchDb($_REQUEST['searchword'], $this->data['page'], true, $_REQUEST['fields'], $order_by, 21);
			#echo "Page Number: " . $this->data['page'];
		} else {	
			if($_REQUEST['searchword'] !== "") {
				$searchreturn_count = $model->searchDb($_REQUEST['searchword'], $this->data['page'], null, null, $order_by);
				#print_r($searchreturn_count); exit;
				$searchreturn = $model->searchDb($_REQUEST['searchword'], $this->data['page'], null, null, $order_by, 21);
			} else {
				$searchreturn = NULL;
			}
			
		}
		
		if($searchreturn !== NULL) {
			$searchreturn = unserialize($searchreturn);
			
			$this->data['items'] = $searchreturn[0];		
			$this->data['keys'] = $searchreturn[1];
		} else {
			$this->data['items'] = array();		 
			$this->data['keys'] = array();
		}
		

		$this->data['userDetails'] = $userDetails;	
		$searchreturn_count = unserialize($searchreturn_count);
		$this->data['count'] = count($searchreturn_count[0]);
		
		$refine = array();
		$refine_keys = $this->refine_keys();

		$ex_rates = $modelPropertyListing->getExchangeRates_indi();
		
		$previous_year = date("Y", strtotime("-1 year"));
		echo $previous_year; exit; 

		while(intval($previous_year) >= 2012) {
			if($userDetails->{"verified_".$previous_year}) {
				$user_previous_year_volume = $userDetails->{"volume_".$previous_year};
				$user_previous_year_ave_price = $userDetails->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}
		
		if(count($this->data['items']))
		foreach($this->data['items'] as $_key => $item){
			$is_private = 0;
			$user_currency = $item->user_currency;
			if($item->selected_permission == 3) {
				if($previous_year == 2012 && !$userDetails->{"verified_".$previous_year}) {
					$is_private = 1;
				} else {
					$previous_year_volume_minimum = $item->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum * $item_user_currency_rate;
					} else {
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum;
					}
					
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_volume_converted = $user_previous_year_volume * $user_currency_rate;
					} else {
						$user_previous_year_volume_converted = $user_previous_year_volume;
					}
					#echo $user_previous_year_volume_converted . " " . $previous_year_volume_minimum_converted;
					if($user_previous_year_volume_converted <= $previous_year_volume_minimum_converted) {
						$is_private = 1;
					}
				}			
				
				$this->data['items'][$_key]->volume_private = $is_private;
				#echo $this->data['items'][$_key]->volume_private;
				#echo $item->listing_id;
			}
			
			if($item->selected_permission == 4) {
				if($previous_year == 2012 && !$userDetails->{"ave_price_".$previous_year}) {
					$is_private = 1;
				} else {
					$previous_year_ave_price_minimum = $item->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum * $item_user_currency_rate;
					} else {
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum;
					}
					
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price * $user_currency_rate;
					} else {
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price;
					}
					
					if($user_previous_year_ave_price_converted <= $previous_year_ave_price_minimum_converted) {
						$is_private = 1;
					}
				}			
				
				$this->data['items'][$_key]->ave_price_private = $is_private;
				#echo $this->data['items'][$_key]->volume_private;
				#echo $item->listing_id;
			}
			
			
		
			if($item->country!=0)
				$this->data['items'][$_key]->countryDetails =  $userPmodel->getCountryIso($item->country);
			else if($item->country==0)
				$this->data['items'][$_key]->countryDetails =  $userPmodel->getCountryIso("223");


			if($user->id != $item->user_id){
				if($item->currency!=$userDetails->currency){
					if($item->currency === "") {
						$item->currency = "USD";
					}
										
					$ex_rates_con = $ex_rates->rates->{$item->currency};
					$ex_rates_can = $ex_rates->rates->{$userDetails->currency};

					if($item->currency=="USD"){									
						$item->price1=$item->price1 * $ex_rates_can;
					} else {
						$item->price1=($item->price1 / $ex_rates_con)*$ex_rates_can;
					}
					if($item->price2){
						//$listing->price2=$modelPropertyListing->getExchangeRates($listing->price2,$userDetails->currency,$listing->currency);
						if($item->currency=="USD"){									
							$item->price2=$item->price2 * $ex_rates_can;
						} else {
							$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}

					$item->symbol=$userDetails->symbol;
					$item->currency=$userDetails->currency;				
				}
			}

			
			$this->data['items'][$_key]->user_network = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id));
			$this->data['items'][$_key]->user_network_pending = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 0));
			$this->data['items'][$_key]->user_network_declined = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 2));
			$arr_images = explode(",", $item->images);
			$this->data['items'][$_key]->images_bw = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type, true);
			if(!$arr_images[0]) {
				$this->data['items'][$_key]->images = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type);
			}
			
			/*
			foreach($item as $key => $value){

				if(isset($value) && !empty($value) && $value!="" && in_array($key, $refine_keys))
				    $refine[$key][] = $value;
					
			}
			*/
		}
		
		if(count($searchreturn_count[0]))
		foreach($searchreturn_count[0] as $_key => $item){

		
			#if($item->country!=0)
			#	$this->data['items'][$_key]->countryDetails =  $userPmodel->getCountryIso($item->country);
			#else if($item->country==0)
			#	$this->data['items'][$_key]->countryDetails =  $userPmodel->getCountryIso("223");


			if($user->id != $item->user_id){
				if($item->currency!=$userDetails->currency){
					if($item->currency === "") {
						$item->currency = "USD";
					}
										
					$ex_rates_con = $ex_rates->rates->{$item->currency};
					$ex_rates_can = $ex_rates->rates->{$userDetails->currency};

					if($item->currency=="USD"){									
						$item->price1=$item->price1 * $ex_rates_can;
					} else {
						$item->price1=($item->price1 / $ex_rates_con)*$ex_rates_can;
					}
					if($item->price2){
						//$listing->price2=$modelPropertyListing->getExchangeRates($listing->price2,$userDetails->currency,$listing->currency);
						if($item->currency=="USD"){									
							$item->price2=$item->price2 * $ex_rates_can;
						} else {
							$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}

					$item->symbol=$userDetails->symbol;
					$item->currency=$userDetails->currency;				
				}
			}

			
			#$this->data['items'][$_key]->user_network = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id));
			#$this->data['items'][$_key]->user_network_pending = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 0));
			#$this->data['items'][$_key]->user_network_declined = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 2));
			#$arr_images = explode(",", $item->images);
			#$this->data['items'][$_key]->images_bw = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type, true);
			#if(!$arr_images[0]) {
			#	$this->data['items'][$_key]->images = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type);
			#}
			
			foreach($item as $key => $value){

				if(isset($value) && !empty($value) && $value!="" && in_array($key, $refine_keys))
				    $refine[$key][] = $value;
					
			}
		}
		
		
		
		if(isset($_REQUEST['fields']))
			foreach($_REQUEST['fields']  as $key => $fields){
				$temp = array();
				foreach($fields as $r_item){
					if($r_item){
						$temp[] = $r_item;
					}
				}
				$this->data['fields'][$key] = $temp;
			}
		$this->data['andtext'] = $temptext;
		$this->data['originaltext'] = $_REQUEST['searchword'];
		$this->data['refine'] = $refine;
		$this->data['order_by'] = $order_by;
		$this->data['user_currency'] = $userDetails->currency;
		$this->data['user_state'] = $userDetails->state;
		$this->data['query_string'] = $query_string;
		
		$_SESSION['selected'] = "POPs";
		$this->display();
	}

	function searchlisting_raw(){

		$searchword = $_GET['searchword'];
		$user_id = $_GET["user_id"];

		$temptext = $_REQUEST['refine'];
		$_REQUEST['refine'] = explode("|", $_REQUEST['refine']);
		$_REQUEST['searchword'] = strtolower($searchword);
		$_REQUEST['searchword'] = stripslashes_all($_REQUEST['searchword']);
		$_REQUEST['searchword'] = addslashes($_REQUEST['searchword']);

		if(isset($_REQUEST['sort_items'])){
			$order_by=$_REQUEST['sort_items'];
		} else {
			$order_by="DESC";
		}

		$user =& JFactory::getUser($user_id);
		$uid =$_GET["user_id"];	
		if(empty($uid)) JFactory::getApplication()->redirect('index.php?illegal=1');
		
		$model = $this->getModel("searchlisting");
		$this->data = array();
		$this->data['page'] = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;		

		if(isset($_REQUEST['jform']) && $_REQUEST['jform']['name']=='refine_search' && !empty($_REQUEST['fields'])){
			$searchreturn = $model->searchDb($_REQUEST['searchword'], $this->data['page'], true, $_REQUEST['fields'], $order_by, 21);
		}else {
			$searchreturn_count = $model->searchDb($_REQUEST['searchword'], $this->data['page'], null, null, $order_by);
			$searchreturn = $model->searchDb($_REQUEST['searchword'], $this->data['page'], null, null, $order_by, 21);
		    $searchreturn = unserialize($searchreturn);
		}
			
		echo "Count" . count($searchreturn[0]);
		$this->data['items'] = $searchreturn[0];		
		$this->data['keys'] = $searchreturn[1];
		$this->data['count'] = count($searchreturn_count[0]);
		$refine = array();
		$refine_keys = $this->refine_keys();


		if(count($this->data['items']))
		foreach($this->data['items'] as $_key => $item){
			$this->data['items'][$_key]->req_net_status = "1";
			$this->data['items'][$_key]->req_access_per = "1";
			$this->data['items'][$_key]->sub_type_name =  $this->data['items'][$_key]->name;
			$this->data['items'][$_key]->desc = $this->data['items'][$_key]->description;
			$this->data['items'][$_key]->type_property_type = $this->data['items'][$_key]->property_type;
			$this->data['items'][$_key]->user_network = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id));
			$this->data['items'][$_key]->user_network_pending = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 0));
			$this->data['items'][$_key]->user_network_declined = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($item->user_id, 2));
			$arr_images = explode(",", $item->images);
			$this->data['items'][$_key]->images_bw = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type, true);
			if(!$arr_images[0]) {
				$this->data['items'][$_key]->images = "images/buyers/212x172/" . $model->get_default_listing_image($item->property_type, $item->sub_type);
			}
			
			foreach($item as $key => $value){

				if(isset($value) && !empty($value) && $value!="" && in_array($key, $refine_keys))
				    $refine[$key][] = $value;
					
			}

			//$this->data['items'][$_key]->selected_permission = "0";
			if($uid == $this->data['items'][$_key]->user_id){
				$this->data['items'][$_key]->selected_permission = "1";
			} else {
				if(in_array($uid, $item->user_network)){
					if( $this->data['items'][$_key]->setting == 1) {
						$this->data['items'][$_key]->selected_permission = "1";
					} else {
						if (in_array($uid, explode(",", $this->data['items'][$_key]->allowed))){
							$this->data['items'][$_key]->selected_permission = "1";
						} else if (in_array($uid, explode(",", $this->data['items'][$_key]->requests))){
							$this->data['items'][$_key]->req_access_per = "pending_private";
						} else if (in_array($uid, explode(",", $this->data['items'][$_key]->denied))){
							$this->data['items'][$_key]->req_access_per = "denied_private";
						} else {
							$this->data['items'][$_key]->req_access_per = "request_private";
						}
					}
				} else if(in_array($uid, $item->user_network_pending)){
					if($this->data['items'][$_key]->selected_permission!=1) {
						$this->data['items'][$_key]->selected_permission=0;
						$this->data['items'][$_key]->req_access_per = "pending_network";
					} 
				} else if(in_array($uid, $item->user_network_declined)){
					$this->data['items'][$_key]->selected_permission=0;
					$this->data['items'][$_key]->req_access_per = "denied_network";
				} else {
					$this->data['items'][$_key]->req_access_per = "request_network";
				}

			}

			$now = time();
            $timedifference=strtotime($this->data['items'][$_key]->date_expired)-$now;
            $this->data['items'][$_key]->expiry=strval(round($timedifference/(60*60*24)));

		}
		
		if(isset($_REQUEST['fields']))
			foreach($_REQUEST['fields']  as $key => $fields){
				$temp = array();
				foreach($fields as $r_item){
					if($r_item){
						$temp[] = $r_item;
					}
				}
				$this->data['fields'][$key] = $temp;
			}
		$this->data['andtext'] = $temptext;
		$this->data['originaltext'] = $_REQUEST['searchword'];
		$this->data['refine'] = $refine;
		$this->data['order_by'] = $order_by;
		
		$_SESSION['selected'] = "POPs™";
	//	$this->display();

		$response = array('status'=>1, 'message'=>"Found POPs™", 'data'=>$this->data['items']);
		echo json_encode($response);
	}
	
	function refine_keys(){
		$temp = array();
		$temp[] = 'new';
		$temp[] = 'property_type';
		$temp[] = 'sub_type';
		$temp[] = 'zone_name';
		$temp[] = 'city';
		$temp[] = 'zip';
		$temp[] = 'bedroom';
		$temp[] = 'bathroom';
		$temp[] = 'view';
		$temp[] = 'style';
		$temp[] = 'pool_spa';
		$temp[] = 'condition';
		$temp[] = 'garage';
		$temp[] = 'units';
		$temp[] = 'occupancy';
		$temp[] = 'type';
		$temp[] = 'listing_class';
		$temp[] = 'stories';
		$temp[] = 'room_count';
		$temp[] = 'type_lease';
		$temp[] = 'type_lease2';
		$temp[] = 'term';
		$temp[] = 'furnished';
		$temp[] = 'pet';
		$temp[] = 'zoned';
		$temp[] = 'bldg_type';
		$temp[] = 'features1';
		$temp[] = 'features3';
		$temp[] = 'features2';
		return $temp;
	}
	

	function getCountryDataByID($country){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id WHERE cl.countries_id = ".$country." ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObject();
	}

	function searchagent(){

		

		if(isset($_GET['webservice']) && $_GET['webservice']==1){
			$user =& JFactory::getUser($_GET['user_id']);
		} else {
			$user =& JFactory::getUser();
		}
		
		$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
		
		$arr_ugmp = $user->groups;

		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

		JModelLegacy::addIncludePath( $propmodel );

		$modelPropertyListing =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$userprofile2model = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofile2model );

		$userPmodel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');


		$model = $this->getModel($this->getTask());
		
		$query_string = "";
		foreach($_REQUEST as $key => $value) {
			$query_string .= ($query_string) ? "&" : "";
			if(is_array($value)) {
				if($key == "refine") {
					$query_string .= $key . "=" . str_replace(" ", "+", $value[0]);
				} else {
					foreach($value as $_key => $_value) {
						if(is_array($_value)) {
							foreach($_value as $__key => $__value) {
								$query_string .= $key . "[" . $_key . "][]=" . str_replace(" ", "+", $__value);
							}
						} else {
							$query_string .= $key . "[" . $_key . "]=" . str_replace(" ", "+", $_value);
						}					
					}
				}				
			} else {
				$query_string .= $key . "=" . str_replace(" ", "+", $value);
			}
		}
		
		$this->data['query_string'] = $query_string;
			
		$_REQUEST['searchword'] = trim($_REQUEST['searchword']);
		$_REQUEST['searchword'] = strtolower($_REQUEST['searchword']);
		$_REQUEST['searchword'] = stripslashes_all($_REQUEST['searchword']);
		$_REQUEST['searchword'] = addslashes($_REQUEST['searchword']);

		$refine = false;
		$refinements = array();
		if( isset($_REQUEST['refine_search'])){
			$refine = true;
			$refinements = (is_array($_REQUEST['fields']) && count($_REQUEST['fields']) > 0) ? $_REQUEST['fields'] : array();
			$remove_arr =explode(",", $_REQUEST['searchword']);
			$remove_arr =array_filter($remove_arr);
			//$this->data['fields'] = $_REQUEST['fields'];
			foreach ($refinements as $key => $value) {
				# code...

				$refinements[$key] = array_diff($refinements[$key], $remove_arr);
			}
			//$refinements["zip"] =array_diff($refinements["zip"], $remove_arr);
			
			if(empty($_REQUEST['searchword'])){

				foreach ($refinements as $key => $value) {
					# code...
					$_REQUEST['searchword'] .= implode(",", $value);
				}
				foreach ($refinements as $key => $value) {
					# code...
					$refinements[$key] = array_diff($refinements[$key], $remove_arr);
				}
				//$_REQUEST['searchword'] = implode(",", $refinements);ucfirst
				$_REQUEST['searchword'] = trim($_REQUEST['searchword']);
				$_REQUEST['searchword'] = strtolower($_REQUEST['searchword']);
				$_REQUEST['searchword'] = stripslashes_all($_REQUEST['searchword']);
				$_REQUEST['searchword'] = addslashes($_REQUEST['searchword']);
				$_REQUEST['searchword'] = ucwords($_REQUEST['searchword']);
				$refine = false;
				$refinements=array();

			}
			$this->data['fields'] = $refinements;
		}
		
		$refine = false;
		foreach ($refinements as $key => $value) {
			# code...
			if(empty($refinements[$key])){
				unset($refinements[$key]);
			}
			if(!empty($value)){
				$refine = true;
			}
		}
		



		//var_dump($_REQUEST['searchword']);
		//var_dump(array_diff($refinements, $remove_arr));
		//var_dump(array_filter($remove_arr));

		$userDetails = $userPmodel->get_user_registration($user->id);

		if($_REQUEST['searchword'] !== "" || ($_REQUEST['searchword'] == "" && in_array("8", $arr_ugmp))) {
			$countresults = $model->searchDb(explode(',',$_REQUEST['searchword']), $refine, $refinements);
			$this->data['users'] = $model->searchDb(explode(',',$_REQUEST['searchword']), $refine, $refinements, $page, 21);
		} else {
			$this->data['users'] = array();
		}
		
		

		
	
		$arr = array();

		$ex_rates = $modelPropertyListing->getExchangeRates_indi();

        
       // $ex_rates_USD = $ex_rates->rates->USD;

		foreach($this->data['users'] as $user){
			
			$arr_usr_zips = array();
			$user->zips = $userPmodel->get_pocket_zips($user->id);
		    $user->b_zips = $userPmodel->get_buyer_zips($user->id);
		    $user->w_zips = $userPmodel->get_w_zips($user->email)[0]->zip_workaround;

		    $user->unique_id = $user->id;
			$zip_arr= array();
			$zip_arr_val= array();
			$zip_arr_country= array();
			if(count($user->zips)>=1 || count($user->b_zips)>=1){
			  	foreach($user->zips as $zip){
			  		$zip_arr[] = $zip->zip;
			  		$zip_arr_val[$zip->zip]["pop"]+=$zip->count;
			  	}
			  	foreach($user->b_zips as $zip){
			  		if(strpos($zip->zip,',') !== false){
			  			$zip_ex=explode(",",$zip->zip);
			  			$zip_arr[] = $zip_ex[0];
			  			$zip_arr_val[$zip_ex[0]]["buyer"]+=$zip->count;
			  			$zip_arr[] = $zip_ex[1];
			  			$zip_arr_val[$zip_ex[1]]["buyer"]+=$zip->count;
			  		} 
			  		else{
			  			$zip_arr[] = $zip->zip;                        	  		
			  		    $zip_arr_val[$zip->zip]["buyer"]+=$zip->count;
			  		}
			  	}
			}


			$expl_w_zips = explode(",",$user->w_zips);
			if(!empty($expl_w_zips))
				foreach ($expl_w_zips as $key => $value) {
					# code...
					if($value){
						$zip_arr[] = $value;
						$zip_arr_val[$value]["pop"]+=0;                        	  		
				  		$zip_arr_val[$value]["buyer"]+=0;
			  		}
				}

			
			
			
			$zips_imploded="'(".implode("|", array_unique(array_filter($zip_arr))).")'";

			$zips_b_raw = $userPmodel->get_buyer_zip_country($user->id, $zips_imploded);
			$zips_p_raw = $userPmodel->get_pops_zip_country($user->id, $zips_imploded);
			$zips_w_raw = $userPmodel->get_w_zips_country($zips_imploded);

			$zips_b=array();
			$i=0;
			foreach ($zips_b_raw as $value) {
				$this_country=$value->country;
				$this_country_iso=$value->countries_iso_code_2;

				$exploded_zips = explode(",", $value->zip);
				foreach ($exploded_zips as $zip_e) {

					$zips_b[$i]['zip']=$zip_e;
					$zips_b[$i]['country']=$this_country;
					$zips_b[$i]['this_country_iso']=$this_country_iso;
					$i++;
				}
				
			}



			foreach ($zips_p_raw as $value) {
				$this_country=$value->country;
				$this_country_iso=$value->countries_iso_code_2;
				$exploded_zips = explode(",", $value->zip);
				foreach ($exploded_zips as $zip_e) {
					$zips_b[$i]['zip']=$zip_e;
					$zips_b[$i]['country']=$this_country;
					$zips_b[$i]['this_country_iso']=$this_country_iso;
					$i++;
				}				
			}



			if(count($expl_w_zips)>0)
				foreach ($expl_w_zips as $zip_e) {
					if($zip_e){


						$zips_b[$i]['zip']=$zip_e;
						$zips_b[$i]['country']=$zips_w_raw[0]->countries_id;
						$zips_b[$i]['this_country_iso']=$zips_w_raw[0]->countries_iso_code_2;

					}
					$i++;
				}				
			
			


			$zip_arr[] = $user->zip;
			$zip_arr_val[$user->zip]["pop"]+=0;                        	  		
	  		$zip_arr_val[$user->zip]["buyer"]+=0;
	  		$last_row = count($zips_b);
			$zips_b[$last_row]['zip']=$user->zip;
			$zips_b[$last_row]['country']=$user->country;
			$zips_b[$last_row]['this_country_iso']=$user->countries_iso_code_2;
				

			$t=0;
			$l=0;
			$up_zip_arr_val=array();
			foreach ($zip_arr_val as $key => $value) {
				foreach ($zips_b as $key2 => $value2) {
					if($key==$value2['zip']){
						if($value2['country']==222){

							if(strcspn($value2['zip'], '0123456789') != strlen($value2['zip'])){					
								preg_match('/\d/', $value2['zip'], $m, PREG_OFFSET_CAPTURE);
						    	if (sizeof($m))
						        	 $m[0][1]; // 24 in your example


						        $newkey=substr($value2['zip'], 0, ($m[0][1]+1));
						        $otherhalf_key = substr($value2['zip'], ($m[0][1]+1), strlen($value2['zip']));
						        if(is_numeric($otherhalf_key[0])){
						        	$newkey = $newkey.$otherhalf_key[0];
						        }
							} else {
								$newkey = $value2['zip'];
							}
							
								if($zip_arr_val[$value2['zip']]['pop'])
									$up_zip_arr_val[$newkey]["pop"] += $zip_arr_val[$value2['zip']]['pop'];
								if($zip_arr_val[$value2['zip']]['buyer'])
									$up_zip_arr_val[$newkey]["buyer"] += $zip_arr_val[$value2['zip']]['buyer'];

								if(!$zip_arr_val[$value2['zip']]['buyer'] && !$zip_arr_val[$value2['zip']]['pop'] ){
									$up_zip_arr_val[$newkey]=$zip_arr_val[$value2['zip']];
								}
						
							//unset($zip_arr_val[$value2['zip']]);
							//$zip_arr_val[$t]
							//var_dump($value2['zip']);
							//var_dump( $value['buyer']);
							//var_dump( $value['pop']);
						} else {
							$up_zip_arr_val[$value2['zip']]=$zip_arr_val[$value2['zip']];
						}	
						continue 2;			
					}
					$l++;
				}
				$t++;				
			}

		 	uasort( $up_zip_arr_val, function($a, $b) {
			    if( $a['pop'] == $b['pop'] )
			        return 0;
			    return ( $a['pop'] < $b['pop'] ) ? 1 : -1;
			} );


			$user->zips_b = $zips_b;
			$user->zip_arr_val = $up_zip_arr_val;



		    foreach ($user->zips as $zip) {
		    	$arr_usr_zips[] = $zip->zip;
		    }

		    foreach ($user->b_zips as $zip) {
		    	$e_zips = explode(",", $zip->zip);
		    	foreach ($e_zips as $value) {
		    		# code...
		    		$arr_usr_zips[] = $value;
		    	}
		    	
		    }
		    $arr_usr_zips[]= $user->info->zip;
		    $u_arr_usr_zips = array_unique($arr_usr_zips);
		    foreach ($u_arr_usr_zips as  $value) {
		    	# code...
		    	$refinement['zip'][] = $value;
		    }
			//$refinement['zip'][] = $user->info->zip;
			$refinement['ur.city'][] = $user->info->city;
			$refinement['zone_code'][] = $user->zone_code;
			$refinement['countries_name'][] = $user->countries_iso_code_3;
			$refinement['broker_name'][] = $user->brokerage;
			
			if(strpos($user->user_language, ',')!== FALSE) {
				$exp = explode(',', $user->user_language);
				foreach ($exp as $expb) {
				$refinement['newtblang.lang_result'][] = $expb;
				}
			} else {
				$refinement['newtblang.lang_result'][] = $user->user_language;
			}

			if($user->id != JFactory::getUser()->id){
				if($user->currency == "") {
					$user->currency = "USD";
				}
				
				if($user->currency!=$userDetails->currency){
					$ex_rates_con = $ex_rates->rates->{$user->currency};
					$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
					if($user->currency=="USD"){									
						//$user->price1=$listing->price1 * $ex_rates_CAD;
						$user->ave_price_2012=$user->ave_price_2012 * $ex_rates_can;
						$user->ave_price_2013=$user->ave_price_2013 * $ex_rates_can;
						$user->ave_price_2014=$user->ave_price_2014 * $ex_rates_can;
						$user->ave_price_2015=$user->ave_price_2015 * $ex_rates_can;
						$user->volume_2012=$user->volume_2012 * $ex_rates_can;
						$user->volume_2013=$user->volume_2013 * $ex_rates_can;
						$user->volume_2014=$user->volume_2014 * $ex_rates_can;
						$user->volume_2015=$user->volume_2015 * $ex_rates_can;
					} else {
						//$user->price1=$listing->price1 / $ex_rates_CAD;
						//$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
						$user->ave_price_2012=($user->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
						$user->ave_price_2013=($user->ave_price_2013 / $ex_rates_con)*$ex_rates_can;
						$user->ave_price_2014=($user->ave_price_2014 / $ex_rates_con)*$ex_rates_can;
						$user->ave_price_2015=($user->ave_price_2015 / $ex_rates_con)*$ex_rates_can;
						$user->volume_2012=($user->volume_2012 / $ex_rates_con)*$ex_rates_can;
						$user->volume_2013=($user->volume_2013 / $ex_rates_con)*$ex_rates_can;
						$user->volume_2014=($user->volume_2014 / $ex_rates_con)*$ex_rates_can;
						$user->volume_2015=($user->volume_2015 / $ex_rates_con)*$ex_rates_can;
					}
			
				}
			}

			

		}
		
		foreach($countresults as $user2){
			
			$arr_usr_zips = array();
			$user2->zips = $userPmodel->get_pocket_zips($user2->id);
		    $user2->b_zips = $userPmodel->get_buyer_zips($user2->id);
		    $user2->w_zips = $userPmodel->get_w_zips($user2->email)[0]->zip_workaround;

		    $user2->unique_id = $user2->id;
			$zip_arr= array();
			$zip_arr_val= array();
			$zip_arr_country= array();
			if(count($user2->zips)>=1 || count($user2->b_zips)>=1){
			  	foreach($user2->zips as $zip){
			  		$zip_arr[] = $zip->zip;
			  		$zip_arr_val[$zip->zip]["pop"]+=$zip->count;
			  	}
			  	foreach($user2->b_zips as $zip){
			  		if(strpos($zip->zip,',') !== false){
			  			$zip_ex=explode(",",$zip->zip);
			  			$zip_arr[] = $zip_ex[0];
			  			$zip_arr_val[$zip_ex[0]]["buyer"]+=$zip->count;
			  			$zip_arr[] = $zip_ex[1];
			  			$zip_arr_val[$zip_ex[1]]["buyer"]+=$zip->count;
			  		} 
			  		else{
			  			$zip_arr[] = $zip->zip;                        	  		
			  		    $zip_arr_val[$zip->zip]["buyer"]+=$zip->count;
			  		}
			  	}
			}


			$expl_w_zips = explode(",",$user2->w_zips);
			if(!empty($expl_w_zips))
				foreach ($expl_w_zips as $key => $value) {
					# code...
					if($value){
						$zip_arr[] = $value;
						$zip_arr_val[$value]["pop"]+=0;                        	  		
				  		$zip_arr_val[$value]["buyer"]+=0;
			  		}
				}

			
			
			
			$zips_imploded="'(".implode("|", array_unique(array_filter($zip_arr))).")'";

			$zips_b_raw = $userPmodel->get_buyer_zip_country($user2->id, $zips_imploded);
			$zips_p_raw = $userPmodel->get_pops_zip_country($user2->id, $zips_imploded);
			$zips_w_raw = $userPmodel->get_w_zips_country($zips_imploded);

			$zips_b=array();
			$i=0;
			foreach ($zips_b_raw as $value) {
				$this_country=$value->country;
				$this_country_iso=$value->countries_iso_code_2;

				$exploded_zips = explode(",", $value->zip);
				foreach ($exploded_zips as $zip_e) {

					$zips_b[$i]['zip']=$zip_e;
					$zips_b[$i]['country']=$this_country;
					$zips_b[$i]['this_country_iso']=$this_country_iso;
					$i++;
				}
				
			}



			foreach ($zips_p_raw as $value) {
				$this_country=$value->country;
				$this_country_iso=$value->countries_iso_code_2;
				$exploded_zips = explode(",", $value->zip);
				foreach ($exploded_zips as $zip_e) {
					$zips_b[$i]['zip']=$zip_e;
					$zips_b[$i]['country']=$this_country;
					$zips_b[$i]['this_country_iso']=$this_country_iso;
					$i++;
				}				
			}



			if(count($expl_w_zips)>0)
				foreach ($expl_w_zips as $zip_e) {
					if($zip_e){


						$zips_b[$i]['zip']=$zip_e;
						$zips_b[$i]['country']=$zips_w_raw[0]->countries_id;
						$zips_b[$i]['this_country_iso']=$zips_w_raw[0]->countries_iso_code_2;

					}
					$i++;
				}				
			
			


			$zip_arr[] = $user2->zip;
			$zip_arr_val[$user2->zip]["pop"]+=0;                        	  		
	  		$zip_arr_val[$user2->zip]["buyer"]+=0;
	  		$last_row = count($zips_b);
			$zips_b[$last_row]['zip']=$user2->zip;
			$zips_b[$last_row]['country']=$user2->country;
			$zips_b[$last_row]['this_country_iso']=$user2->countries_iso_code_2;
				

			$t=0;
			$l=0;
			$up_zip_arr_val=array();
			foreach ($zip_arr_val as $key => $value) {
				foreach ($zips_b as $key2 => $value2) {
					if($key==$value2['zip']){
						if($value2['country']==222){

							if(strcspn($value2['zip'], '0123456789') != strlen($value2['zip'])){					
								preg_match('/\d/', $value2['zip'], $m, PREG_OFFSET_CAPTURE);
						    	if (sizeof($m))
						        	 $m[0][1]; // 24 in your example


						        $newkey=substr($value2['zip'], 0, ($m[0][1]+1));
						        $otherhalf_key = substr($value2['zip'], ($m[0][1]+1), strlen($value2['zip']));
						        if(is_numeric($otherhalf_key[0])){
						        	$newkey = $newkey.$otherhalf_key[0];
						        }
							} else {
								$newkey = $value2['zip'];
							}
							
								if($zip_arr_val[$value2['zip']]['pop'])
									$up_zip_arr_val[$newkey]["pop"] += $zip_arr_val[$value2['zip']]['pop'];
								if($zip_arr_val[$value2['zip']]['buyer'])
									$up_zip_arr_val[$newkey]["buyer"] += $zip_arr_val[$value2['zip']]['buyer'];

								if(!$zip_arr_val[$value2['zip']]['buyer'] && !$zip_arr_val[$value2['zip']]['pop'] ){
									$up_zip_arr_val[$newkey]=$zip_arr_val[$value2['zip']];
								}
						
							//unset($zip_arr_val[$value2['zip']]);
							//$zip_arr_val[$t]
							//var_dump($value2['zip']);
							//var_dump( $value['buyer']);
							//var_dump( $value['pop']);
						} else {
							$up_zip_arr_val[$value2['zip']]=$zip_arr_val[$value2['zip']];
						}	
						continue 2;			
					}
					$l++;
				}
				$t++;				
			}

		 	uasort( $up_zip_arr_val, function($a, $b) {
			    if( $a['pop'] == $b['pop'] )
			        return 0;
			    return ( $a['pop'] < $b['pop'] ) ? 1 : -1;
			} );


			$user2->zips_b = $zips_b;
			$user2->zip_arr_val = $up_zip_arr_val;



		    foreach ($user2->zips as $zip) {
		    	$arr_usr_zips[] = $zip->zip;
		    }

		    foreach ($user2->b_zips as $zip) {
		    	$e_zips = explode(",", $zip->zip);
		    	foreach ($e_zips as $value) {
		    		# code...
		    		$arr_usr_zips[] = $value;
		    	}
		    	
		    }
		    $arr_usr_zips[]= $user2->info->zip;
		    $u_arr_usr_zips = array_unique($arr_usr_zips);
		    foreach ($u_arr_usr_zips as  $value) {
		    	# code...
		    	$refinement2['zip'][] = $value;
		    }
			//$refinement2['zip'][] = $user2->info->zip;
			$refinement2['ur.city'][] = $user2->info->city;
			$refinement2['zone_code'][] = $user2->zone_code;
			$refinement2['countries_name'][] = $user2->countries_iso_code_3;
			$refinement2['broker_name'][] = $user2->brokerage;
			
			if(strpos($user2->user_language, ',')!== FALSE) {
				$exp = explode(',', $user2->user_language);
				foreach ($exp as $expb) {
				$refinement2['newtblang.lang_result'][] = $expb;
				}
			} else {
				$refinement2['newtblang.lang_result'][] = $user2->user_language;
			}

			if($user2->id != JFactory::getUser()->id){
				if($user2->currency == "") {
					$user2->currency = "USD";
				}
				
				if($user2->currency!=$userDetails->currency){
					$ex_rates_con = $ex_rates->rates->{$user2->currency};
					$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
					if($user2->currency=="USD"){									
						//$user2->price1=$listing->price1 * $ex_rates_CAD;
						$user2->ave_price_2012=$user2->ave_price_2012 * $ex_rates_can;
						$user2->ave_price_2013=$user2->ave_price_2013 * $ex_rates_can;
						$user2->ave_price_2014=$user2->ave_price_2014 * $ex_rates_can;
						$user2->ave_price_2015=$user2->ave_price_2015 * $ex_rates_can;
						$user2->volume_2012=$user2->volume_2012 * $ex_rates_can;
						$user2->volume_2013=$user2->volume_2013 * $ex_rates_can;
						$user2->volume_2014=$user2->volume_2014 * $ex_rates_can;
						$user2->volume_2015=$user2->volume_2015 * $ex_rates_can;
					} else {
						//$user2->price1=$listing->price1 / $ex_rates_CAD;
						//$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
						$user2->ave_price_2012=($user2->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
						$user2->ave_price_2013=($user2->ave_price_2013 / $ex_rates_con)*$ex_rates_can;
						$user2->ave_price_2014=($user2->ave_price_2014 / $ex_rates_con)*$ex_rates_can;
						$user2->ave_price_2015=($user2->ave_price_2015 / $ex_rates_con)*$ex_rates_can;
						$user2->volume_2012=($user2->volume_2012 / $ex_rates_con)*$ex_rates_can;
						$user2->volume_2013=($user2->volume_2013 / $ex_rates_con)*$ex_rates_can;
						$user2->volume_2014=($user2->volume_2014 / $ex_rates_con)*$ex_rates_can;
						$user2->volume_2015=($user2->volume_2015 / $ex_rates_con)*$ex_rates_can;
					}
			
				}
			}

			

		}




		$this->data['userDetails'] = $userDetails;

		$this->data['result_count'] = count($countresults);

		$this->data['refine'] = $refinement2;
		$this->data['originaltext'] = $_REQUEST['searchword'];
		$this->data['keys'] = array_filter(explode(",", $_REQUEST['searchword']));

		$_SESSION['selected'] = "Agent";

		if(isset($_GET['webservice']) && $_GET['webservice']==1){
			$response = array('status'=>1, 'message'=>"Found User Properties", 'data'=>$this->data['users']);
			echo json_encode($response);
		}

		$this->display();
	}
}