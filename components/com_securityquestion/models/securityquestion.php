<?php

defined('_JEXEC') or die;

class SecurityQuestionModelSecurityQuestion extends JModelLegacy{
	function check_duplicate($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(*) as count');
		$query->from('#__answers');
		$query->where('uid = '.$id.'');
		$db->setQuery($query);
		return $db->loadObject()->count;
	}
	
	function insert_answers($data){
		$db = JFactory::getDbo();
		$insert = new JObject();
		foreach ($data as $key => $value){
			$insert->$key = $value;
		}
		return $db->insertObject('#__answers', $insert);
	}
	
	public function get_user_type($select, $uid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($select);
		$query->from('#__users u');
		$query->where('id = '.$uid);
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');
		$db->setQuery($query);
		return $db->loadObject();

	}
}	
?>