<?php
	
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class SecurityQuestionController extends JControllerLegacy
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {		
    	$model = &$this->getModel('SecurityQuestion');
		$_SESSION['user_id'] = JFactory::getUser()->id;
		$uid = JFactory::getUser()->id;
		$application = JFactory::getApplication();
		$result = $model->get_user_type(array('is_premium', 'user_type'), $uid);
		$usertype=$result->user_type;

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model3 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		
		$user_info = $model3->get_user_registration($uid);		

		//language loading
	  	$language = JFactory::getLanguage();
      	$extension = 'com_nrds';
      	$base_dir = JPATH_SITE;
      	$language_tag = $user_info->currLanguage;
      	$language->load($extension, $base_dir, $language_tag, true);
        //language loading	


		if(JFactory::getUser()->id==0){
			$application->redirect( "index.php" );	
		} else {
			$dup = $model->check_duplicate(JFactory::getUser()->id);
			if($dup){
				$application->redirect( "index.php" );
			}
		}


		if($_POST['jform']['form']=="security_question"){
			foreach($_POST['jform'] as $key=>$value){
				if(is_array($value)){
					foreach($value as $k=>$v){
						$data[$key][$k] = trim(addslashes($v));
					}
				}
				else{
					$data[$key] = trim(addslashes($value));
				}
			}
				
			extract($data);

				
			$dup = $model->check_duplicate(JFactory::getUser()->id);
				
			if($dup){
				JError::raiseWarning( 100, 'Duplicate entry found!' );
				$application->redirect( "index.php" );	
			}
			else{
				$details = array(
								'uid'	=> JFactory::getUser()->id,
								'set1'	=> $question1,
								'ans1'	=> $answer1,
								'set2'	=> $question2,
								'ans2'	=> $answer2
								
						);
				
				$ret = $model->insert_answers($details);
				if($ret && ($usertype==2 || $usertype==3 || $usertype==5)){
					echo 'redirect';
				} else if ($ret && ($usertype==1 || $usertype==4 )) {
					echo 'express';
				} else {
					JError::raiseWarning( 100, 'Error encountered while saving to database!' );		
				}
				die();
			}	
		}
        parent::display();
    }
}
	
?>