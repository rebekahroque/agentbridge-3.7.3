<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class SecurityQuestionViewSecurityQuestion extends JViewLegacy
{
    function display($tpl = null)
    {	
	
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		
        // GET ALL SECURITY QUESTIONS
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__security_questions')->where("qid BETWEEN 6 AND 8");
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
        $this->assignRef( 'results', $results );
 
        parent::display($tpl);
    }
}

?>