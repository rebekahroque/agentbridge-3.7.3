<?php

// No direct access

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');

$session =& JFactory::getSession(); 
$user = & JFactory::getUser();
//$thisuserid=$session->get( 'userid' );

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select(array('*'));
$query->from('#__user_registration');
$query->where('email = "'.$user->email.'"');
$db->setQuery($query);
$users_tb = $db->loadObject();

$thisuserid=$users_tb->user_id;

$start_activated=date("m-d-Y, h:i:s A");
$session->set( 'start_activated', $start_activated );

$questions = array();
$questions1 = array();
$questions2 = array();
$questions3 = array();

/*foreach($this->results as $result){

	$questions[$result->qid]['set1'] = $result->set1;
	$questions[$result->qid]['set2'] = $result->set2;
	$questions[$result->qid]['set3'] = $result->set3;

}*/

$questions1 = $this->results[0];

$t=0;

foreach($questions1 as $result){

	if($t==0){
		$id=$result;
	} else {
		$questions[$id]['set'.$t] = $result;
	}
	$t++;

}
$questions2 = $this->results[1];

$t=0;
foreach($questions2 as $result){

	if($t==0){
		$id=$result;
	} else {
		$questions[$id]['set'.$t] = $result;
	}
	$t++;
}


$questions3 = $this->results[2];
$t=0;
foreach($questions3 as $result){

	if($t==0){
		$id=$result;
	} else {
		$questions[$id]['set'.$t] = $result;
	}
	$t++;
}




?>
<style type="text/css">
#security_question #s2id_jform_question1, #security_question #s2id_jform_question2{
	width: 300px !important;
	min-height: 30px !important;
	height: inherit !important;
}

#security_question #s2id_jform_question1 a.select2-choice, #security_question #s2id_jform_question2 a.select2-choice {
	height: inherit !important;
	padding-bottom:6px;
}
#security_question #s2id_jform_question1 a.select2-choice span, #security_question #s2id_jform_question2 a.select2-choice span {
	word-wrap: break-word !important;
	text-overflow: inherit !important;
	white-space: normal !important;
}
</style>
<script type="text/javascript">
mixpanel.track("Set Security Questions");
</script>
						<form style="display:none"
							id="user_logout" 
							class="form-horizontal jqtransformdone"
							action="<?php echo JRoute::_("index.php?option=com_users&task=user.logout"); ?>"
							method="post">
							<a id='logout_link' href="javascript: void(0);" onclick="document.getElementById('user_logout').submit();">Logout</a>
							<?php echo JHtml::_('form.token');?></form>
<div style="padding-bottom:40px; min-height:800px">
<div style="display: block; line-height:16px; color:#000000"><!-- start security question -->
	<h3 class="pw-heading"><?php echo JText::_('COM_SEC_SETUP');?></h3>
	<div class="forms-head" style="text-align:center">
        <div class="clear">
		<p style="font-size:16px; line-height:22px"><?php echo JText::_('COM_SEC_SETUP_TEXT');?><br/><br/></p>
	</div>

<!--
	<form id="security_question" action="<?php echo $_SERVER['PHP_SELF']; ?>?option=com_securityquestion&uid=<?php echo $_SESSION['uid'] ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
-->
	<form id="security_question"  class="form-validate form-horizontal" enctype="multipart/form-data">
	
		<input type="hidden" value="<?php echo JFactory::getUser()->id ?>" id="uid" name="uid" />
		<input type="hidden" value="security_question" id="jform_form" name="jform[form]" />

		<div class="question">

			<select class="validate-question1 required" id="jform_question1" name="jform[question1]" aria-required="true" required="required" style="width:300px">
					<?php
						foreach($questions as $key=>$question){
					?>
					<option value="<?php echo $key; ?>"><?php echo JText::_($question['set1']) ?></option>
					<?php
						}
					?>
			</select>
				
			<span class="jform_question1 error_msg" style="display:none; font-size:11px"><?php echo JText::_('COM_SEC_ERROR');?> <br /> <br /></span> 
			
			<div class="clear-float"></div>
	
			<input 
					type="text" 
					class="validate-answer1 required text-input-sec" 
					autocomplete="off" 
					value="" 
					id="jform_answer1" 
					name="jform[answer1]"
				>
				
			<span class="jform_answer1 error_msg" style="display:none; font-size:11px"><br /> <?php echo JText::_('COM_SEC_ERROR');?><br /> <br /></span> 

				
		</div>

		<div class="question">
			<div>
				<select class="validate-question2 required" id="jform_question2" name="jform[question2]" aria-required="true" required="required">
					<?php
						foreach($questions as $key=>$question){
					?>
					<option value="<?php echo $key; ?>"><?php echo JText::_($question['set2']) ?></option>
					<?php
						}
					?>
				</select>
				
				<span class="jform_question2 error_msg" style="display:none; font-size:11px"><?php echo JText::_('COM_SEC_ERROR');?> <br /> <br /></span> 
				
			</div>
			
			
			<div style="margin-top:5px;">
				<input 
					type="text" 
					class="validate-answer2 required text-input-sec" 
					autocomplete="off" 
					value="" 
					id="jform_answer2" 
					name="jform[answer2]" 
				>
				
				<span class="jform_answer2 error_msg" style="display:none; font-size:11px"><br /> <?php echo JText::_('COM_SEC_ERROR');?>  <br /></span> 
				
			</div>
			
		</div>



		<div>
			<input id="continue" name="" value="<?php echo JText::_('COM_SEC_BTN') ?>" type="submit" class="submit-btn short-btn"/>
			<input type="hidden" name="option" value="com_securityquestion" />
			<input type="hidden" name="task" value="set_security" />
			<!--input type="hidden" name="task" value="registration.register" /-->
			<?php echo JHtml::_('form.token');?>
		</div>

	</form>
    </div>

</div>
</div>

<script>
//Your timing variables in number of seconds
	 
	//total length of session in seconds
	var sessionLength = (30*60);
	//var sessionLength = 10;
	//time warning shown (10 = warning box shown 10 seconds before session starts)
	var warning = 10; 
	//time redirect forced (10 = redirect forced 10 seconds after session ends)    
	var forceRedirect = 0;
	 
	//time session started
	var pageRequestTime = new Date();
	 
	//session timeout length
	var timeoutLength = sessionLength*1000;
	 
	//set time for first warning, ten seconds before session expires
	var warningTime = timeoutLength - (warning*1000);
	 
	//force redirect to log in page length (session timeout plus 10 seconds)
	var forceRedirectLength = timeoutLength + (forceRedirect*1000);
	 
	//set number of seconds to count down from for countdown ticker
	var countdownTime = warning;
	 
	//warning dialog open; countdown underway
	var warningStarted = false;

	var teset_seconds=1;

	function CheckForSession() {

		console.log(teset_seconds++);
		//get time now
	    var timeNow = new Date();
	     
	    //event create countdown ticker variable declaration
	    var countdownTickerEvent;  
	     
	    //difference between time now and time session started variable declartion
	    var timeDifference = 0;
	     
	    timeDifference = timeNow - pageRequestTime;
	          
	    if (timeDifference > forceRedirectLength)
	        {   
	           //clear (stop) checksession event
	         
	           clearInterval(checkSessionTimeEvent);
	          jQuery.ajax({
	          	url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=emailProgressAbandon&format=raw",
	          	type: "POST",
	          	data: {useregid: "<?php echo $thisuserid?>", page: "com_securityquestion", start:"<?php echo $start_activated?>",abandoned:timeDifference},
	          	success: function(data){
	          		jQuery("#continue").data('clicked', true);
          			jQuery("#logout_link").click();
	          	}
	          });
	 	
	        }

	}

	var checkSessionTimeEvent;
	
jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("ul").hide();
	checkSessionTimeEvent = setInterval("CheckForSession()", (30*60*1000));

	//jQuery("#jform_question1").select2({ width:"100%"});
	//jQuery("#jform_question2").select2({ width:"100%"});
	
	jQuery("#continue").click(function(){
		jQuery(this).data('clicked', true);
	});

		jQuery(window).bind('beforeunload', function(){
						var timeNow = new Date();
			     
					    //event create countdown ticker variable declaration
					    var countdownTickerEvent;  
					     
					    //difference between time now and time session started variable declartion
					    var timeDifference = 0;
					     
					    timeDifference = timeNow - pageRequestTime;
					    if(!jQuery("#continue").data('clicked')){

			    		   jQuery.ajax({
				          	url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=emailProgressAbandon&format=raw",
				          	type: "POST",
				          	async:false,
				          	data: {useregid: "<?php echo $thisuserid?>", page: "com_securityquestion", start:"<?php echo $start_activated?>",abandoned:timeDifference},
				          	success: function(data){
				          		//window.location="<?php echo $this->baseurl?>";
				          		jQuery("#logout_link").click();
				          	}
				          });
			    		}
			});

});
</script>
