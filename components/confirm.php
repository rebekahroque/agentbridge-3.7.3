<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery(document).ready(function(){
	jQuery('input[type="submit"]').live("click", function(){

		jQuery(".jform_field1.error_msg").hide();
		jQuery(".jform_field2.error_msg.error1").hide();
		jQuery("#username").removeClass("glow-required");

		var error=0;

		if(!jQuery("#jform_username").val()){
			jQuery("#jform_username").addClass("glow-required");
			jQuery(".jform_field1.error_msg.error1").show();
			error++;
		} else {
			if(IsEmail(jQuery("#jform_username").val())==false){
				jQuery("#jform_username").addClass("glow-required");
				jQuery(".jform_field1.error_msg.error1").hide();
				jQuery(".jform_field1.error_msg.error2").show();
				error++;
			}
		}

		if(!jQuery("#jform_token").val()){
			jQuery("#jform_token").addClass("glow-required");
			jQuery(".jform_field2.error_msg.error1").show();
			error++;
		}

		if(error>0){
			return false;
		}
 		
	});
});
	
</script>
<div class="banner-row" style="padding-bottom:40px; min-height:800px">
	<div class="short-container full-radius forgot-password" style="color:#000000; text-align:left"><!-- start forgot password -->

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm'); ?>" method="post" id="confirm-reset" class="form-validate">
		
		<?php //if ($this->params->get('show_page_heading')) : ?>
		<div class="forms-head">
			<h3 style="font-size:24px; font-weight:bold; padding-bottom:20px; border-bottom:1px dotted #bfbfbf">Password reset</h3>
			<p style="line-height:20px; font-size:16px; margin-top:10px">Before you can reset your password, we would like to verify your identity for security reasons.</p>
		</div>
		<?php //endif; ?>
		
		<?php  foreach ($this->form->getFieldsets() as $fieldset) : ?>
		
			<?php $i=1; foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<div>
					<?php echo $field->input; ?>
					<p class="jform_field<?php echo $i?> error_msg error1" style="margin-left:100px;display:none; font-size:11px"><br> This field is required</p> 
					<p class="jform_field<?php echo $i?> error_msg error2" style="margin-left:100px;display:none; font-size:11px"><br>Invalid <?php echo $i==1 ? "username" : "password";?> </p> 
					<?php $i++; ?>
				</div>
			<?php endforeach; ?>
		
		<?php endforeach; ?>

		<div class="login-form right" style="margin-right:20px">
			<input name="" value="Continue" type="submit" style="padding-top:8px" class="submit-btn"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
</div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("ul").hide();
});
</script>