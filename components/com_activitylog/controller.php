<?php

/**

 * @version     1.0.0

 * @package     com_activitylog

 * @copyright   Copyright (C) 2013. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com

 */



// No direct access

defined('_JEXEC') or die( 'Restricted access' );



jimport('joomla.application.component.controller');



if(!JFactory::getUser())

    JFactory::getApplication()->redirect('index.php?illegal=1');



class ActivitylogController extends JControllerLegacy

{

	function display()

	{

		// language loading
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));

		    $language =& JFactory::getLanguage();
		    $extension = 'com_nrds';
		    $base_dir = JPATH_SITE;
		    $language_tag = JFactory::getUser()->currLanguage;
		    $language->load($extension, $base_dir, $language_tag, true);
		// language loading
	

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$user = & JFactory::getUser();
		$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
		if(empty($uid)) JFactory::getApplication()->redirect('index.php?illegal=1');
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$application = JFactory::getApplication();
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		$model3 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$userreg = $model3->get_user_registration(JFactory::getUser()->id);
		$usertype = $userreg->user_type;
		$agentlicense = $userreg->licence;
		$security = $model3->get_security(JFactory::getUser()->id);
		$ispremium = $userreg->is_premium;
		$termsaccept = $userreg->is_term_accepted;

		$usersales = $model3->get_user_sales($userreg->user_id);
		

		//redirect rules
		if ($security->uid=='') {
			$application->redirect(JRoute::_('index.php?option=com_securityquestion'));
		} else {
			//$application->redirect(JRoute::_('index.php?option=com_activitylog'));
			if ($usertype==1 || $usertype==4 ) {
				if ($agentlicense=='') {
					$application->redirect(JRoute::_('index.php?option=com_userprofile&task=continue_express'));
				}  else {
					if ($ispremium==0) {
						if ($termsaccept==0) {
							$application->redirect(JRoute::_('index.php?option=com_userprofile&task=payments'));
						} 
					} 
				}
				
			} else if ($usertype==2 || $usertype==3 || $usertype==5) {
				if ($agentlicense=='') {
					$application->redirect(JRoute::_('index.php?option=com_userprofile&task=continue_complete'));
				} else {
					if ($ispremium==0) {
						if ($termsaccept==0) {
							$application->redirect(JRoute::_('index.php?option=com_userprofile&task=payments'));
						} 
					} 
				}
				
			}
			
		}
		
		//update all pops matches for buyers
			$updatedAll=$model3->checkUpdatedAll(JFactory::getUser()->email);

			if($updatedAll[0]->is_updated_all<1){
				$model2->getAllPopsBuyers();
				$model3->updateUpdatedAll(JFactory::getUser()->email);
			}

		//update all pops matches for buyers

		$def_tz =date_default_timezone_get();
		date_default_timezone_set($def_tz);
		//date_default_timezone_set("America/Los_Angeles");


		$date = strtotime(date("Y-m-d", time()));

		$date_3 = strtotime("+3 day", $date);



		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__pocket_listing pocket')
		->where('user_id = '.$user->id. " AND closed!=1 AND sold!=1");

		$user_pockets = $db->setQuery($query)->loadObjectList();

		//var_dump($user_pockets);

		foreach ($user_pockets as $pocket) {
			$timestamp_pockets = strtotime($pocket->date_expired);

			if($timestamp_pockets==$date_3){

				

				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select('listing_id')
				->from('#__buyer_saved_listing bsl')
				->where('listing_id = '.$pocket->listing_id);

				$counts_ =$db->setQuery($query)->loadColumn();
				$pops_saved = count($counts_);


				/*if($pops_saved){
					$origdate = strtotime($pocket->date_expired);
					$date_30  = strtotime("+90 day", $origdate);
					$date_expred_new = date("Y-m-d", $date_30);

					$fields = array($db->quoteName('date_expired') .'="'. $date_expred_new.'"');

					$db = JFactory::getDbo();
					$query = $db->getQuery(true);

					$conditions = array($db->quoteName('listing_id') . '=\''.$pocket->listing_id.'\'');
					$query->update($db->quoteName('#__pocket_listing'))->set($fields)->where($conditions);
					$db->setQuery($query);			 

					$result = $db->execute();

				} else {*/


					$numDays = abs($timestamp_pockets - $date_3)/60/60/24;
					$flr_numdays=round($numDays);
					$db = JFactory::getDbo();
					$query = $db->getQuery(true);
					$query->select('*')
					->from('#__activities act')
					->where('activity_id = '.$pocket->listing_id. " AND user_id=".$user->id." AND (activity_type=32 || activity_type=33)");
					$acts_32 = $db->setQuery($query)->loadObjectList();
					if(count($acts_32)<=0){
						$model2->insert_activity($user->id, 32, $pocket->listing_id, $user->id, date('Y-m-d H:i:s',time()));
					}				
					if(($flr_numdays>=1 && $flr_numdays<2) || ($flr_numdays>=2 && $flr_numdays<3) || $flr_numdays==3){

					}

				//}

			} else if($timestamp_pockets<$date_3){



					$origdate = strtotime($pocket->date_expired);
					$date_30  = strtotime("+90 day", $origdate);
					$date_expred_new = date("Y-m-d", $date_30);

					$fields = array($db->quoteName('date_expired') .'="'. $date_expred_new.'"');

					$db = JFactory::getDbo();
					$query = $db->getQuery(true);

					$conditions = array($db->quoteName('listing_id') . '=\''.$pocket->listing_id.'\'');
					$query->update($db->quoteName('#__pocket_listing'))->set($fields)->where($conditions);
					$db->setQuery($query);			 

					$result = $db->execute();

			} 
		}

		$view = &$this->getView($this->getName(), 'html');
		$model = $this->getModel($this->getName());
		$model->updateVisit(JFactory::getUser()->id);


		/*if(JFactory::getUser()->id==2090){
			$referral22 = new stdClass();
			$referral22->client_id = 645;
			$model2->send_statuschange_mail(JFactory::getUser()->id, JFactory::getUser()->id, 5, 157);
			//$model->send_contact_details($referral22,JFactory::getUser()->id,JFactory::getUser()->id);
		}*/

		if(isset($_GET['page'])){
			$offsets=($_GET['page']-1)*10;
			$datas = $model->getActivities($offsets, 10);
		} else {
			$datas = $model->getActivities(0, 10);
		}

		$ex_rates = $model2->getExchangeRates_indi();
		
		$previous_year = date("Y", strtotime("-1 year"));
		$user_previous_year_volume = 0;
		$user_previous_year_ave_price = 0;
		while(intval($previous_year) >= 2012) {
			if($userreg->{"verified_".$previous_year}) {
				$user_previous_year_volume = $userreg->{"volume_".$previous_year};
				$user_previous_year_ave_price = $userreg->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}
		
		
		/*if(isset($_GET['acttype'])){
			$datas = $model->getActivities(400, 500);
			foreach ($datas as $key => $value) {
				 if ($value->activity_type != $_GET['acttype'] ) {
				        unset($datas[$key]);
				    }
			}
		}*/
	//	echo "<pre>";
	//	var_dump($datas);
	//	echo "<pre>";

		$refout_closed_count = $model->outclosed_count(JFactory::getUser()->id);	
		$refin_closed_count  = $model2->inclosed_count(JFactory::getUser()->id);
		$this->data = array();
		


		$view->assignRef('refin_closed_count', $refin_closed_count);

		$d_format = $this->getCountryDataByID($userreg->country)->dateTimeFormat;
		
		switch($userreg->country) {
			case 223:
			case 38:
			case 13:			
				$objTimeZone = $this->getTimeZone($userreg->state);
				$timezone = $objTimeZone->timezone;
				break;
			case 222:
			case 103:
				$timezone = "Europe/London";
				break;
			case 204:
			case 81:
			case 141:
			case 73:
				$timezone = "Europe/Paris";
				break;
			default:
				$timezone = "America/Los_Angeles";
		}
		
		$view->set('timezone', $timezone);

		#echo "<pre>"; print_r($model->getActivities()); die();
		
		$ownerCountryDetails = $model2->getCountry($user->id);
		$view->set('user_currency', $ownerCountryDetails[0]->currency);
		
		$view->set('ex_rates', $ex_rates);
		$view->set('previous_year', $previous_year);
		$view->set('user_previous_year_volume', $user_previous_year_volume);
		$view->set('user_previous_year_ave_price', $user_previous_year_ave_price);
		
		$view->set('data', $datas);
		$view->set('user_info', $userreg);
		$view->set('d_format', $d_format);
		$view->set('refout_closed_count', $refout_closed_count);
		$view->set('dropdown_referral', $model2->get_referral_fee_dropdown());
		$view->set('country_list', $model3->get_countries());

		$view->set('buyers', $buyers);
		$view->set('buyers_a', $buyers_a);
		$view->set('buyers_b', $buyers_b);
		$view->set('buyers_c', $buyers_c);
		$view->set('buyers_i', $buyers_i);
		
		$view->set('volume_2016', (int)$usersales[0]->volume_2016);
		$view->set('sides_2016', (int)$usersales[0]->sides_2016);
		
		$view->display();

	}


	public function getCountryDataByID($country){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id WHERE cl.countries_id = ".$country." ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObject();
	}
	
	public function getTimeZone($zone_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT ct.timezone FROM #__country_timezones ct WHERE ct.zone_id = " . $zone_id);
		$db->setQuery($query);		
		return $db->loadObject();
	}
	


	// new rules for declining a network request as of 7:50 PM 10/9/2013

	function acceptContactRequest() {

		$model = $this->getModel($this->getName);

		$model->update_contact_request($_REQUEST['id'], 1);

		die();

	}

	

	function declineContactRequest() {

		$model = $this->getModel($this->getName);

		$model->update_contact_request($_REQUEST['id'], 0);

		die();	

	}

	// new rules end here!!!



	function acceptrequest(){

		$model = $this->getModel($this->getName);

		$model->update_request($_REQUEST['id'], 1);

		die();

	}

	function acceptrequest_raw(){

		$id=$_GET['id'];	

		$model = $this->getModel($this->getName);

		$model->update_request($id, 1);

		$response = array('status'=>1, 'message'=>"Request Accepted", 'data'=>array());
		
		echo json_encode($response);

	}

	

	function declinerequest(){

		$model = $this->getModel($this->getName);

		$model->update_request($_REQUEST['id'], 2);

		die();

	}



	function signedbyuser(){

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		

		$propertymodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

		JModelLegacy::addIncludePath( $propertymodel );

		$model3 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		

		

		$model = $this->getModel($this->getName);

		$referral = $model3->get_referral($_REQUEST['id']);

		if(trim($_REQUEST['event'])==trim("signing_complete")){

			$count = $model->logsignedActivity($_REQUEST['other'], $_REQUEST['signed'], $_REQUEST['id']);

			$model->logsignedActivity($_REQUEST['signed'], $_REQUEST['other'], $_REQUEST['id'], 0);

			//echo 'olabeforeif';

			

			$buyer = $model->broadcast_contact($_REQUEST['id']);

			

			$buyer_card = $this->get_buyer_card($buyer);

			if($referral->agent_b == JFactory::getUser()->id){

				$buyer_card = $this->get_buyer_card($buyer);

				$model->send_contact_details($referral, $referral->agent_b, $referral->agent_a);
	
				$model->logReferralSigning($_REQUEST['id'], true);
			} else {
				$model->logReferralSigning($_REQUEST['id']);
			}

			if($count->count>=2){

				$user_id = $referral->agent_b;

				$other_user_id = $referral->agent_a;

				$date = date('Y-m-d H:i:s',time());

				$model3->insert_activity($user_id, 20, $_REQUEST['id'], $other_user_id, $date);

				$model3->insert_activity($other_user_id, 20, $_REQUEST['id'], $user_id, $date);

			}

			if($referral->agent_b == JFactory::getUser()->id)

				echo "<script>window.top.location = '".JRoute::_('index.php?option=com_activitylog&signed=agent_b')."&clientname=".urlencode($buyer_card->name)."';</script>";

			else

				echo "<script>window.top.location = '".JRoute::_('index.php?option=com_activitylog&signed=agent_a')."&clientname=".urlencode($buyer_card->name)."';</script>";

		}

		die();

	}

	

	function json_encode_card(){

		echo json_encode($this->format_card($this->get_buyer_card($_POST['b_id'])));

		die();

	}

	

	function get_buyer_card($id){

		$model = $this->getModel($this->getName);

		

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		

		$propertymodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

		JModelLegacy::addIncludePath( $propertymodel );

		$model3 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		

		$buyer = $model->get_buyer($id);

		$buyer_card = new stdClass();

		$buyer_card->name = $buyer->name;

		$buyer_card->address = array();

		$buyer_card->mobile = array();

		$buyer_card->email = array();

		$buyer_card->note = $buyer->note;



		$buyer_card->address_breakdown = $buyer->address[0];

		foreach($buyer->address as $address){

			$state = ($address->state) ? $address->state : "";

			$con_Code = ($address->countryCode) ? $address->countryCode : "";

			$buyer_card->address[] = $address->address_1.', '.$address->address_2.', '.$address->city.' '.$state.' '.$model2->getCountry($address->country).', '.$address->zip;

		}

		foreach($buyer->mobile as $mobile){

			$buyer_card->mobile[] = $con_Code.' '.$mobile->number;

		}

		foreach($buyer->email as $email){

			$buyer_card->email[] = $email->email;

		}

		return $buyer_card;

	}



	function dlvcard(){

		$buyer_card = $this->get_buyer_card($_REQUEST['id']);

		$this->download_card($buyer_card);

		echo $buyer_card;

		die();

	}

	

	function format_card($data){

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		

		$card = new stdClass();

		$card->name = $data->name;

		$state = (is_numeric($data->address_breakdown->state)) ? $model2->getState($data->address_breakdown->state) : $data->address_breakdown->state;

		$card->mobile = implode(',',$data->mobile);

		$card->address = $data->address_breakdown->address_1." ".$data->address_breakdown->address_2.",".$data->address_breakdown->city.",".$state.",".$data->address_breakdown->zip.",".$model2->getCountry($data->address_breakdown->country);

		$card->email = implode(',', $data->email);

		$card->note = $data->note;
		

		return $card;

	}

	

	function download_card($data){

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		

		$state = (is_numeric($data->address_breakdown->state)) ? $model2->getState($data->address_breakdown->state) : $data->address_breakdown->state;

		

		header('Content-Type: text/x-vcard; charset=utf-8');

		echo "BEGIN:VCARD\n";

		echo "VERSION:2.1\n";

		echo "FN:".stripslashes_all($data->name)."\n";

		echo "TEL;PREF;HOME;VOICE:".$data->mobile[0]."\n";

		echo "ADR;HOME:;;".$data->address_breakdown->address_1." ".$data->address_breakdown->address_2.";".$data->address_breakdown->city.";".$state.";".$data->address_breakdown->zip.";".$model2->getCountry($data->address_breakdown->country)."\n";

		//echo "LABEL;HOME;ENCODING=QUOTED-PRINTABLE:3 Acacia Avenue=0D=0AHoemtown, MA 02222=0D=0AUSA\n";

		echo "EMAIL;PREF;INTERNET:".$data->email[0]."\n";

		foreach ($data->email as $key=>$email){

			if($email == $data->email[0])

				continue;

			echo "EMAIL;INTERNET:".$email."\n";

		}

		echo "END:VCARD\n";

	}
	
	
	public function savepops(){


		$db = JFactory::getDbo();


		$application = JFactory::getApplication();


		$model = $this->getModel($this->getName());


		$buyers_0 = $_REQUEST['buyer_arr'];


		$buyers_0_dis = $_REQUEST['buyer_arr_dis'];


		$buyers_dis = explode(",",$buyers_0_dis);


		$aid = $_POST['aida'];


		$pid = $_POST['pida'];


		$msg = "";


		$buyers = explode(",",$buyers_0);


		/*print_r($buyers_dis);


		print_r($buyers); */


		foreach($buyers as $b){


			if($b != 0 && $b != ""){


				$insert = $model->enableSavedListing($pid, $aid, $b);


				$activity = array(


					'user_id'		=> $aid,


					'buyer_id'=> $b,


					'other_user_id'		=> JFactory::getUser()->id,


					'activity_type' => (int)26,


					'date'			=> date('Y-m-d H:i:s', time())


				);


				$ret = $model->insertPropertyActivity($pid, $activity);


			}


		}


		


		foreach($buyers_dis as $b){


			if($b != 0 && $b != ""){


				$insert = $model->disableSavedListing($pid, $aid, $b);


				$msg = "Property Saved";


			}


		}


			


		echo $msg;


		//echo $aid." ".$pid;


		//


		die();


	}
	
	
	function check_saved_pops(){
		$model = $this->getModel($this->getName());
		
		$listing_id = JRequest::getVar('pida');
		$buyer_id 	= JRequest::getVar('buyer_arr');
		$active 	= JRequest::getVar('current_status');

		echo $model->check_existing_pops($listing_id, $buyer_id, $active);
		die();
	}


	function extend_expPops(){

		$listing_id=$_POST['listing_id'];
		$pid = $_POST['listing_id'];

		$user = & JFactory::getUser();

		$date = time();

		$date_30 = strtotime("+30 day", $date);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__pocket_listing pocket')
		->where('listing_id = '.$listing_id);

		$user_pockets = $db->setQuery($query)->loadObjectList();

		

		foreach ($user_pockets as $pocket) {

			$origdate = strtotime($pocket->date_expired);
			$date_30  = strtotime("+30 day", $origdate);
			$date_expred_new = date("Y-m-d", $date_30);

			$fields = array($db->quoteName('date_expired') .'="'. $date_expred_new.'"');

		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$conditions = array($db->quoteName('listing_id') . '=\''.$listing_id.'\'');

		$query->update($db->quoteName('#__pocket_listing'))->set($fields)->where($conditions);

		$db->setQuery($query);			 

		$result = $db->execute();

		if($result){

			$propertymodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

			JModelLegacy::addIncludePath( $propertymodel );

			$model3 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

				$activity = array(


					'user_id'		 => JFactory::getUser()->id,


					'buyer_id'		 => "",


					'other_user_id'	 => JFactory::getUser()->id,


					'activity_type'  => (int)34,


					'date'			 => date('Y-m-d H:i:s', time())


				);


				$ret = $model3->insertPropertyActivity($pid, $activity);

			echo $date_expred_new;

		} else {
		//s	die();
		}

	}

	function noExtend_expPops(){

		$pkId=$_POST['pkId'];
		$listing_id=$_POST['listing_id'];

		$user = & JFactory::getUser();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__pocket_listing pocket')
		->where('listing_id = '.$listing_id);

		$user_pockets = $db->setQuery($query)->loadObjectList();
		

		foreach ($user_pockets as $pocket) {

			$origdate = $pocket->date_expired;

		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$fields = array($db->quoteName('activity_type') .'=33');

		$conditions = array($db->quoteName('pkId') . '=\''.$pkId.'\'');

		$query->update($db->quoteName('#__activities'))->set($fields)->where($conditions);

		$db->setQuery($query);			 

		$result = $db->execute();

		if($result){
			echo $origdate;
		} else {
		//s	die();
		}

	}

}