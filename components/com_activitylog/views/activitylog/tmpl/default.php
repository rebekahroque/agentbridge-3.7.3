<?php
/**
 * @version     1.0.0
 * @package     com_activitylog
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com
 */
// no direct access
defined('_JEXEC') or die;
//Load admin language file
//$lang = JFactory::getLanguage();
//$lang->load('com_activitylog', JPATH_ADMINISTRATOR);


$model = &$this->getModel('ActivityLog');
$user_model = &$this->getModel('UserProfile');
$prev = 0;
$ctr = 0;
function in_network($r1,$r2){
			$r1 = intval($r1);
			$r2 = intval($r2);
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('status')->from('#__request_network')->where("user_id = ".$r1." AND other_user_id = ".$r2);
			$db->setQuery($query);
			return $db->loadObject()->status;
		}
preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
$os = current($matches);
if($os){
	$notMobile=0;
} else {
	$notMobile=1;
}
?>

<script type="text/javascript">mixpanel.track("Activity Log");</script>
<script type="text/javascript">
	var isDoing = false;
	var sel_c = "";
	var state_variable = "adminCode1";
	function declineFully(){
		if(jQuery("#referralrequest").hasClass('ui-dialog-content'))
		jQuery("#referralrequest").dialog('close');
		jQuery("#reason").dialog(
			{
				modal:true, 
				width: 'auto', 
				title: "Reason for Decline",
			}
		)
	}
/*	var options =  function(){
			var zip = jQuery("#zip").val();
			if(sel_c=="IE"){
				jQuery.ajax({
		                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
		                success: function(data){
		                	console.log(data[0]);
		                	if (data[0] == null) {
		                		alert("Eircode search reached limit. only 15 per day");
		               		 }
		                	jQuery("#jform_state option").filter(function() {
									if(this.text == data[0].county){
										jQuery("#jform_state").select2({ width: 'resolve' });
										jQuery("#jform_state").select2("val", this.value);
									}
								});		
		                	console.log("posttown = "+data[0].posttown);
		                	console.log("dependentlocality = "+data[0].dependentlocality);
							if(typeof data[0].posttown != 'undefined'){
								jQuery("#city").val(data[0].posttown);
							} else {
								jQuery("#city").val(data[0].dependentlocality);
							}
							jQuery("#s2id_state").remove();
		                }
		        }); 
			} else {
				jQuery.ajax({
			            url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+sel_c+'&username=damianwant33',
			            success: function(data){
			            	/*if(sel_c=='GB'){
								jQuery("#jform_state option").filter(function() {
									if(this.text == data.postalcodes[0].adminName3){
										jQuery("#jform_state").select2("val", this.value);
									}
								});
			            	} else {
			            		jQury("#jform_state").select2("val", jQuery("#zone"+data.postalcodes[0].adminCode1).val());
			            	}
			            	if(state_variable=="adminCode1"){
	                			console.log(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
	                			jQuery("#jform_state").select2("val", jQuery("#zone"+data.postalcodes[0].adminCode1).val());
	                		} else if(sel_c=='MC'){	
		                		jQuery("#jform_state").select2("val", jQuery("#zoneMC").val());
		                	} else {
		                		console.log(state_variable);
		                		jQuery("#jform_state > option").filter(function() {				                			
									if(this.text == data.postalcodes[0][state_variable]) {
										jQuery("#jform_state").select2({ width: 'resolve' });
										jQuery("#jform_state").select2("val", this.value);
									} 
								});
		                	}				
							jQuery("#city").val(data.postalcodes[0].placeName);
							//jQuery("#s2id_state").remove();
			            }
			     }); 
			}
		};*/
	function calculateSummary(clrf){
		jQuery("head").append("<style>#content{position: static !important;</style>");
		crlfid = clrf;
		isDoing = true;
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=calculatefee')?>",
			type: 'POST',
			data: 	{ 
				'crlfid'	: clrf
				},
			success: function(response){
				var values = jQuery.parseJSON(response);
				if(values.authorize!=null){
					jQuery(".savedcard").html(values.authorize.paymentProfile.payment.creditCard.cardNumber);
					jQuery(".authorizefields").hide();
					jQuery("#use").click();
				} else {
					jQuery(".radiobuttons, .usesavedsubmit").remove();
					usesaved = false;
					jQuery.placeholder.shim();
				}
			jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_userprofile&task=get_current_registered_user'); ?>",
					type: 'POST',
					dataType: "json",
					data: {  },
					title: "Change Referral Status",
					success: function(response) {
						jQuery("#alslno").val(response.licence);
						jQuery("#bslno").val(response.brokerage_license);
						jQuery("#btino").val(response.tax_id_num);
						jQuery("#authorizenet").dialog(
							{
								modal:true,
								width:'auto',
								closeOnEscape: false,
								title: "Calculate Commission Split‏",
								 appendTo: ".wrapper"
							}
						)
					//	jQuery("#authorizenet").dialog("option", "position", 'top')	;
					/*jQuery.ajax({
						url: "<?php echo $this->baseurl?>/index.php?option=com_propertylisting&task=changeCountryData&format=raw&webservice=1",
						type: "POST",
						data: {country:jQuery("#jform_country").val()},
						success: function (data){
							//jQuery("#pocketform").prepend(data);
							var datas = JSON.parse(data);
							jQuery("#zip").inputmask({mask:datas.zip_format.split(','),oncomplete:options});
							jQuery("#zip").blur(function(){
							  jQuery("#zip").val((jQuery("#zip").val()).toUpperCase());
							});
							jQuery(".countCode").text(datas.countryCode);
							jQuery("#zip").attr("maxlength",datas.zipMaxLength);
							jQuery("#city").attr("placeholder",datas.cityLabel);
							jQuery("#zip").attr("placeholder",datas.zipLabel);
						    jQuery(".jform_zip_2.error_msg").text(datas.zipErrorMess);
						    jQuery('.numbermask').inputmask("remove");
							jQuery('.numbermask2').inputmask("remove");
							jQuery(".numbermask").inputmask({mask:datas.phoneFormat.split(',')});
							jQuery(".numbermask2").inputmask({mask:datas.phoneFormat.split(',')});				
						}
					});
					jQuery.ajax({
						url: "<?php echo JRoute::_('index.php?option=com_userprofile&task=decrypt_taxid'); ?>",
						type: 'POST',
						data: { alslno: jQuery("#btino").val() },
						success: function(response) {
							jQuery("#btino").val(response);
						},
						error: function(){
						}
					});	
					jQuery.ajax({
						url: "<?php echo JRoute::_('index.php?option=com_userprofile&task=decrypt_license'); ?>",
						type: 'POST',
						data: { alslno: jQuery("#alslno").val() },
						success: function(response) {
							jQuery("#alslno").val(response);
						},
						error: function(){
						}
					});		
					jQuery.ajax({
						url: "<?php echo JRoute::_('index.php?option=com_userprofile&task=decrypt_license'); ?>",
						type: 'POST',
						data: { alslno: jQuery("#bslno").val() },
						success: function(response) {
							jQuery("#bslno").val(response);
						},
						error: function(){
						}
					});	*/						
					},
					error: function(){
						alert('error');
					}
				});	
				jQuery("#clientinput").hide();
				//jQuery('select#jform_country').select2('destroy').select2();
				jQuery('#ronecomm').html(values.ronecomm+" USD");
				jQuery("#rtwocomm").html(values.rtwocomm+" USD");
				jQuery(".amounttopay").html(values.amount+" USD");
				jQuery(".r2name").html(values.r2);
				jQuery(".total").html(values.amount+" USD");
				jQuery(".clientname").html(values.clientname);
					var refin_count = <?php echo $this->refin_closed_count; ?>;
				if( refin_count==0 ) {
						jQuery(".orig_total").html(values.amount+" USD");
						jQuery("#discount_item_label").html(""+
							"<div style='color:#007BAE;margin-left:37px;font-size:11px;'>- Discount (100%) "+"<span  style='margin-left:50px;'>"+values.amount+"</span>"
						);
						values.amount = "$0 USD";
						jQuery(".total").html(values.amount+" USD");
						jQuery(".amounttopay").html(values.amount+" USD");
						jQuery(".order_subx").html(values.amount+" USD");
						jQuery("#rtwocomm").html(values.rtwocomm+" USD");
						jQuery(".clientname").html(values.clientname);
					} else{
						jQuery(".amounttopay").html(values.amount+" USD");
						jQuery(".total").html(values.amount+" USD");
						jQuery(".orig_total").html(values.amount+" USD");		
						jQuery(".order_subx").html(values.amount+" USD");
						jQuery("#rtwocomm").html(values.rtwocomm+" USD");
						jQuery(".clientname").html(values.clientname);
					}
				jQuery("#summary")
					.show().parent().parent().center();
			}
		})
	}
	function jsonpCallback(data){
		console.log(data);
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		jQuery("#city").val(data.postalcodes[0].placeName);
		jQuery("#s2id_state").remove();
		try{
			jQuery("#jform_state").select2('val', jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		}catch(e){}
	}
	function setValidators(){
		jQuery(".validateme").blur(function(){
			var regex = Array();
			regex[0] = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
			regex[1] = /[0-9-()+]{3,20}/;
			console.log(regex[jQuery(this).attr('validator')]);
			jQuery(this).val(this.value.match(regex[jQuery(this).attr('validator')]));
		});
		jQuery('input').keydown(function(){
			jQuery(this).qtip('destroy');
		});
	}
	function zipblur(){
		setValidators();
	}
	function form_validate(name){
		if(!(jQuery("#nouse").length) || jQuery("#nouse").is(":checked")){
			jQuery("#"+name).find('input, select').each(function(){
				if(jQuery(this).attr('required')!=undefined){
					if(jQuery(this).val()==0||jQuery(this).val()==""){
						jQuery("#"+jQuery(this).attr('data-panel')).slideDown();
						jQuery(this).on('focusout', function(){jQuery(this).removeClass('focus');});
						jQuery(this).addClass('focus');
						jQuery(this).focus();
						return false;
					}
				}
			});
		} else {
			if(!jQuery('#agree_terms_saved').attr('checked')){
				jQuery("#agree_terms2").show();
				return false;
			}
		}
		return true;
	}
	function submitsavedPayment(){
		var res = jQuery("#refidtopost").data('item-id');
		console.log (res);
		var ctr = 0;
		if( !$jquery('#agree_terms_saved').is(':checked') ) {
				ctr++;
				$jquery(".agree_terms2").show();
			} else {
				$jquery(".agree_terms2").hide();
			}
		console.log( ctr );
		if(ctr==0){
		jQuery("#submit-usesaved").hide();
		jQuery("#loading-image-submit-usesaved").show();	
		jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=submitsavedpayment')?>",
					type: 'POST',
					data: 	{ 
						'ref_id'			: jQuery("#refidtopost").data('item-id'),
						'amount'			: jQuery("#rtwocomm").autoNumeric('get'),
						'usesaved'			: true,
						'crlfid'			: crlfid,
					},
					success: function(response){
						console.log(res);
						var res = jQuery.parseJSON(response);
						console.log(response);
						if(res[0]==1){
							location.reload();
						} else{
							jQuery(".card_message2").show();
							jQuery("#loading-image-submit-usesaved").hide();
							jQuery("#submit-usesaved").show();
							jQuery("#submit-nosaved").show();
							jQuery("#loading-image-submit-nosaved").hide();
						}
					}
			});
		}
	}
	function submitPayment(){
		var ctr = 0;
		if(form_validate("summary")) {
			if($jquery("#btino").val()==""){$jquery("#btino").addClass("glow-required");$jquery(".btino").show();
				$jquery("#btino").focus();
				ctr++;
			} else { $jquery("#btino").removeClass("glow-required");$jquery(".btino").hide(); }
			if($jquery("#zip").val()==""){$jquery("#zip").addClass("glow-required");$jquery(".zip").show();
				$jquery("#zip").focus();
				ctr++;
			} else { $jquery("#zip").removeClass("glow-required");$jquery(".zip").hide(); }
			if($jquery("#address1").val()==""){$jquery("#address1").addClass("glow-required");$jquery(".address1").show();
				$jquery("#address1").focus();
				ctr++;
			} else { $jquery("#address1").removeClass("glow-required");$jquery(".address1").hide(); }
			if($jquery("#state").val()==""){$jquery("#state").addClass("glow-required");$jquery(".state").show();
				$jquery("#state").focus();
				ctr++;
			} else { $jquery("#state").removeClass("glow-required");$jquery(".state").hide(); }			
			if($jquery("#city").val()==""){$jquery("#city").addClass("glow-required");$jquery(".city").show();
				$jquery("#city").focus();
				ctr++;
			} else { $jquery("#city").removeClass("glow-required");$jquery(".city").hide(); }
			if(
				$jquery("#card_number").val()=="" ||
				$jquery("#card_number").val()==""
			){$jquery("#card_number").addClass("glow-required");$jquery(".card_number").show();
				$jquery("#card_number").focus();
				ctr++;
			} else { $jquery("#card_number").removeClass("glow-required");$jquery(".card_number").hide(); }
			if(
				$jquery("#card_expiry").val()=="" ||
				$jquery("#card_expiry").val()=="__-____"
			){$jquery("#card_expiry").addClass("glow-required");$jquery(".card_expiry").show();
				$jquery("#card_expiry").focus();
				ctr++;
			} else { $jquery("#card_expiry").removeClass("glow-required");$jquery(".card_expiry").hide(); }
			if($jquery("#security_number").val()==""){$jquery("#security_number").addClass("glow-required");$jquery(".security_number").show();
				$jquery("#security_number").focus();
				ctr++;
			} else { $jquery("#security_number").removeClass("glow-required");$jquery(".security_number").hide(); }
			if($jquery("#phone").val()==""){$jquery("#phone").addClass("glow-required");$jquery(".phone").show();
				$jquery("#phone").focus();
				ctr++;
			} else { $jquery("#phone").removeClass("glow-required");$jquery(".phone").hide(); }
			if($jquery("#email").val()==""){$jquery("#email").addClass("glow-required");$jquery(".email").show();
				$jquery("#email").focus();
				ctr++;
			} else { $jquery("#email").removeClass("glow-required");$jquery(".email").hide(); }
			if($jquery("#lastname").val()==""){$jquery("#lastname").addClass("glow-required");$jquery(".lastname").show();
				$jquery("#lastname").focus();
				ctr++;
			} else { $jquery("#lastname").removeClass("glow-required");$jquery(".lastname").hide(); }			
			if($jquery("#firstname").val()==""){$jquery("#firstname").addClass("glow-required");$jquery(".firstname").show();
				$jquery("#firstname").focus();
				ctr++;
			} else { $jquery("#firstname").removeClass("glow-required");$jquery(".firstname").hide(); }
			if( !$jquery('#agree_terms').is(':checked') ) {
				ctr++;
				$jquery(".agree_terms").show();
			} else {
				$jquery(".agree_terms").hide();
			}
			console.log( ctr );
			if(ctr==0) {
				jQuery("#loading-image-submit-usesaved").show();
				jQuery("#submit-usesaved").hide();
				jQuery("#submit-nosaved").hide();
				jQuery("#loading-image-submit-nosaved").show();
				jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=submitpayment')?>",
					type: 'POST',
					data: 	{ 
						'card_number'	: jQuery("#card_number").val(),
						'card_expiry'	: jQuery("#card_expiry").val(),
						'security_no'	: jQuery("#security_number").val(),
						'ref_id'		: jQuery("#refidtopost").data('item-id'),
						'firstname'		: jQuery("#firstname").val(),
						'lastname'		: jQuery("#lastname").val(),
						'email'			: jQuery("#email").val(),
						'phone'			: jQuery("#phone").val(),
						'address'		: jQuery("#address1").val()+" "+jQuery("#address2").val(),
						'city'			: jQuery("#city").val(),
						'zip'			: jQuery("#zip").val(),
						'country'		: jQuery("#jform_country").val(),
						'save_trans'	: jQuery("#save_trans").attr('checked'),
						'rtwocomm'		: jQuery("#rtwocomm").val(),
						'ronecomm'		: jQuery("#ronecomm").val(),
						'state'			: jQuery("#jform_state").val(),
						'crlfid'		: crlfid,
						'usesaved'		: false,
						'btino'			: jQuery("#btino").val()
					},
					success: function(response) {
						console.log(response);
						var res = jQuery.parseJSON(response);
						console.log(res);
						if(res[0]==1){
							location.reload();
						} else {
							jQuery("#card_message").show();
							jQuery("#loading-image-submit-usesaved").hide();
							jQuery("#submit-usesaved").show();
							jQuery("#submit-nosaved").show();
							jQuery("#loading-image-submit-nosaved").hide();
						}
					}
				});
			}
		}
	}
	function submitStatusChange(){
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>",
			type: 'POST',
			data: { 'id': jQuery('#referral_id').val(), 'reason':jQuery('#selectreason').val() },
			success: function(response){
				jQuery('#changestatus').dialog('close');
				location.reload();
			}
		});
	}
	function counterReferral(){
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=counterreferral') ?>',
			data: {'id': jQuery("#referral_id").val(), 'value': jQuery("#refvalue").val()},
			success: function(data){
				location.reload();
			}
		});
	}
	function savedialogbroker(){
		if(jQuery("#bl_value_id").val()){
			jQuery(".broker.error_msg").hide();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?format=raw&option=com_propertylisting&task=saveDialogBroker') ?>',
				data: {'otheruserid': jQuery("#otheruserid_val").val(), 'brokerage_license': jQuery("#bl_value_id").val()},
				success: function(data){
					jQuery('#addbrokerlicense').dialog('close');
					var id =  jQuery("#activityid_val").val();
					jQuery("#loading-image-questions-"+id).show();
					jQuery("#buttons-"+id).hide();
					jQuery.ajax({
						type: "POST",
						url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=acceptreferral') ?>',
						data: {'id': id},
							success: function(data){
								jQuery("#loading-image-questions-"+id).hide();
								if(data==1){
									location.reload();
									return false;
								} else {
									jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">The Referral Agreement cannot be generated at the moment. Please retry in a few minutes. If the problem persists, please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
								}
							}, 
							error : function(){
								jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">An error occurred. Please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
							}
					});
				}
			});
		} else {
			jQuery(".broker.error_msg").show();
		}
	}
	function prompt_for_pay(crlfid){
		calculateSummary(crlfid);
	}
	function acceptReferral(id,otheruserid){
		if (typeof otheruserid === 'undefined') { otheruserid = ''; }
		jQuery("#loading-image-questions-"+id).show();
		jQuery("#buttons-"+id).hide();
		console.log(id);
		if(otheruserid!=""){
			jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?format=raw&option=com_propertylisting&task=checkIfBrokerExists') ?>',
				data: {'otheruserid': otheruserid},
				success: function(data){
					var brok_obj = JSON.parse(data);
					var bl_exist = brok_obj[0].brokerage_license;
					if(!bl_exist){
						jQuery("#addbrokerlicense").dialog (
							{
								modal:true, 
								width: 305, 
								open: function( event, ui ) {jQuery(".ui-dialog").center()},
								title: "Add Brokerage License",
								close: function( event, ui) {
									jQuery("#loading-image-questions-"+id).hide();
									jQuery("#buttons-"+id).show();
								}
							}
						);
						jQuery("#activityid_val").val(id);
						jQuery("#otheruserid_val").val(otheruserid);
					} else {
						jQuery.ajax({
							type: "POST",
							url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=acceptreferral') ?>',
							data: {'id': id},
								success: function(data){
									jQuery("#loading-image-questions-"+id).hide();
									if(data==1){
										location.reload();
										return false;
									} else {
										jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">The Referral Agreement cannot be generated at the moment. Please retry in a few minutes. If the problem persists, please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
									}
								}, 
								error : function(){
									jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">An error occurred. Please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
								}
							});
						}
				}, 
				error : function(){
					jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">An error occurred. Please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
				}
			});
		} else {
			jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=acceptreferral') ?>',
			data: {'id': id},
				success: function(data){
					jQuery("#loading-image-questions-"+id).hide();
					if(data==1){
						location.reload();
						return false;
					} else {
						jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">The Referral Agreement cannot be generated at the moment. Please retry in a few minutes. If the problem persists, please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
					}
				}, 
				error : function(){
					jQuery("#question-"+id).html('<div style="margin-top:-8px;line-height:16px">An error occurred. Please contact <a href="mailto:service@agentbridge.com" style="font-size:11px"> AgentBridge </a>.</div>');
				}
			});
		}
	}
	function declineReferral(id){
		jQuery("#referral_id").val(id);
		jQuery("#referralrequest").dialog (
				{
					modal:true, 
					width: 500, 
					open: function( event, ui ) {jQuery(".ui-dialog").center()},
					title: "Referral Fee Options",
				}
				);
	}
	function retractReferral(id){
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>",
			type: 'POST',
			data: { 'id': id , 'reason' : 5},
			success: function(response){
				jQuery('#changestatus').dialog('close');
				location.reload();
			}
		});
	}
	function acceptListing(id, other){
		jQuery("#loading-image-questions-"+id).show();
		jQuery("#buttons-"+id).hide();
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=acceptrequest') ?>',
			data: {'id': id, 'other': other},
			success: function(data){
				location.reload();
			}
		});
	}
	function acceptNetworkRequest(activity_id){
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=acceptrequest',
			data: {'id': activity_id},
				success: function(data){
				location.reload();
			}
		});
	}
	function acceptContactNetworkRequest(activity_id){
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=acceptContactRequest',
			data: {'id': activity_id},
				success: function(data){
				location.reload();
			}
		});
	}
	function declineContactNetworkRequest(activity_id){
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=declineContactRequest',
			data: {'id': activity_id},
			success: function(data){
				location.reload();
			}
		});
	}
	function declineNetworkRequest(activity_id){
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=declinerequest',
			data: {'id': activity_id},
			success: function(data){
				location.reload();
			}
		});
	}
	function declineUserReferral(id){
		jQuery("#referral_id").val(id);
		declineFully();
	}
	function set_masks(){
		jQuery(".numbermask").inputmask("(999) 999-9999");
	}
	jQuery(document).ready(function(){

		

		Landing.format();
		set_masks();
		zipblur();
		jQuery("#card_expiry").mask('99-9999');
		jQuery("#refvalue").keydown(function() {
			var numeric = this.value.replace('%', '');
			jQuery("#refvalue").val(numeric);
		});
		jQuery("#jform_country").select2();
 		jQuery("#jform_country").select2("val","<?php echo $this->user_info->country ?>");
 		get_state(jQuery("#jform_country").val());
 		sel_c = jQuery("#jform_country").find(':selected').attr('data');		
		var baseurl = "<?php echo $this->baseurl?>";
		applyAddressDataPP(baseurl,jQuery("#jform_country").find(':selected').attr('data'),jQuery("#jform_country").val(),"initial");
		jQuery("#city").val("<?php echo $this->user_info->city ?>");
		jQuery("#zip").val("<?php echo $this->user_info->zip ?>");
		jQuery("#jform_country").live("change",function(){
			get_state(jQuery(this).val());
			jQuery("#zip").unbind("keyup");
			jQuery("#zip").val("");
			jQuery('#zip').inputmask("remove");
			jQuery('#city').val("");
			sel_c = jQuery(this).find(':selected').attr('data');
			applyAddressDataPP(baseurl,sel_c,jQuery(this).val());

		});
		jQuery("#refvalue").keyup(function() {
			var numeric = this.value.replace('%', '');
			jQuery("#refvalue").val(numeric.match(/[0-9]{1,2}/));
			jQuery("#refvalue").val(this.value+'%');
		});
		jQuery("#security_number").keyup(function() {
			jQuery(this).val(this.value.match(/[0-9 \-\(\)]+/));
		});
		jQuery("#card_number").keyup(function() {
			jQuery(this).val(this.value.match(/[0-9 \-\(\)]+/));
		});
		jQuery(".change").click(function(){
			var status = jQuery(this).attr('data');
			jQuery("#changestatus").append("<input type='hidden' id='tochangeid' />");
			jQuery('select#changestatusselect option.status').hide();
			jQuery('select#changestatusselect option.status'+status).show();
			jQuery('select#changestatusselect').select2('destroy');
			jQuery('#tochangeid').val(jQuery(this).attr('data-id'));
			jQuery('.client').html(jQuery("#client-"+jQuery.trim(jQuery(this).attr('data-id'))).val());
			jQuery('select#reason').select2('destroy');
			jQuery("#reason").hide();
			jQuery('#changestatus').dialog(
				    {
					  title: "Change Referral Status",
					});
			jQuery("#changestatus").dialog({modal:true,open: function( event, ui ) {jQuery(".ui-dialog").center() }});
		});
		jQuery(".popupclient").live("click", function(){
			var buyer = jQuery.parseJSON(jQuery(this).attr('data'));
			var offest = jQuery(this).offset();
		    var height = jQuery(this).height();
		    var thisclicked = jQuery(this);
		    var origContainer = jQuery("#content").parent();

			jQuery.ajax({
				url: "<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=json_encode_card",
				type: 'POST',
				data: { 'b_id': buyer.buyer_id },
				success: function(e){
					var details = jQuery.parseJSON(e);
					jQuery(".cc_name").html(details.name);
					jQuery(".cc_no").html(details.mobile);
					jQuery(".cc_note").html(details.note);
					if(details.note == '');{
						jQuery(".cc_note_label").hide();
					}
					var email = details.email.split(',');
					var i=0;
					var formated="";
					for(i=0; i<email.length; i++){
						formated+='<a style="color:#009bd0" href="mailto:'+email[i]+'">'+email[i]+'</a> ';
					}
					jQuery(".cc_email").html(formated).parent().dialog(
						{
							modal:false,
							position: { my: "left top", at: "left-2 bottom+10", of: thisclicked, collision:"none", },
							open: function( event, ui ) {
								/*jQuery(".ui-dialog").center()*/
								jQuery(".ui-dialog").appendTo("#content");
								jQuery(".ui-dialog").css("z-index",0);
							/*	jQuery('.ui-dialog').dialog("option", "position", [offest.left, offest.top+height]);
								jQuery('.ui-dialog').dialog('open');*/
							},
							title: details.name,
						});
					console.log(details.name);
				}
			})
		});
	});
	var $ajax = jQuery.noConflict();
	function get_state(cID, state){
		$ajax("#state").html("loading...");
		$ajax("#jform_state").select2('destroy');
		$ajax.ajax({
			url: '<?php echo $this->baseurl ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID, 'state': state },
			success: function(e){
				var change="#state";
				if($ajax("#state").length < 1)
					change="#jform_state";
				$ajax(change).replaceWith(
					jQuery(e).addClass('mc_select')
						.addClass('left'))
						.promise()
						.done(
							function () {
								$ajax("#jform_state").removeAttr('required');
								$ajax("#jform_state").css('width', '259px');
								$ajax("#jform_state").val(<?php echo $this->user->state; ?>);
								$ajax("#jform_state").select2();
								$ajax("#jform_state").select2("val","<?php echo $this->user_info->state ?>");
								
							}
						);
			}
		});
	}
</script>
<style type="text/css">
body{background: #fff;}
.reftriangle{
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 0 10px 10px 10px;
	border-color: transparent transparent #595959 transparent;
	position: absolute;
    top: -51px;
    left: -5px;
}
#popupdetails{
	overflow:visible !important;
}
</style>
<?php 
	$selfimage = array(1,2,3,32,34,33,23,24,15);
?>
<div class="wrapper">
	<div class="wide left activity">
		<!-- start wide-content -->
		<input type="hidden" id="countbuyer" value="<?php echo $this->countbuyer ?>" onclick="removeBuyerButton()">
		<input type="hidden" id="usertype" value="<?php echo $this->user_info->user_type ?>">
		<input type="hidden" id="volume2015" value="<?php echo $this->user_info->volume_2015 ?>">
		<input type="hidden" id="sides2015" value="<?php echo $this->user_info->sides_2015 ?>">
		<h1><?php echo JText::_('COM_ACTIVITY_MY') ?></h1>
			<div id="nopops" class="right" style="margin: 10px auto">
			<a id="addbuyer" style="padding-top:8px; margin-right:10px; display:none" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>" class="button gradient-green right"> <span class="create-referral"><?php echo JText::_('COM_USERPROF_PROF_ADDB');?></span></a> 
            <a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>" class="button gradient-green addpopsact">+ <?php echo JText::_('COM_USERPROF_PROF_ADDP');?></a>
			<a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newreferral')?>" class="button gradient-green addrefact"><em class="refer-me-text" style="padding-left:20px; margin-left:0px; font-size:12px"><?php echo JText::_('COM_USERPROF_PROF_CREATER');?></em></a>
			</div>
			<div id="addsalesdiv" onclick="showsalesdiv()" class="clear-float premium-indicatior addbuyerdiv" style="display:none">
			<div class="left" style="font-size:12px;padding:12px">
				<?php echo JText::_('COM_USERPROF_COMPROF');?>
			</div>
			<div class="right" style="padding:4px">
				<div class="profile-btn right" style="width:auto"><a style="padding-top:8px" href="<?php echo JRoute::_('index.php?option=com_userprofile&task=edit') ?>" class='button gradient-green left'> <span class='create-referral'><?php echo JText::_('COM_USERPROF_EDITP')?></span></a> 
			   <span class="left text-link cursor-point" style="padding:8px"  id="hideaddsalesdiv"  onclick="hidesalesdiv()"><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div id="addbuyerdiv" onclick="showbuyerdiv()" class="clear-float premium-indicatior addbuyerdiv" style="display:none">
			<div class="left" style="font-size:12px;padding:12px">
				<?php echo JText::_('COM_USERPROF_PROF_ADDBT') ?>
			</div>
			<div class="right" style="padding:4px">
				<div class="profile-btn right" style="width:183px;"><a style="padding-top:8px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile') ?>" class='button gradient-green left'> <span class='create-referral'><?php echo JText::_('COM_USERPROF_PROF_ADDB')?></span></a> 
			   <span class="left text-link cursor-point" style="padding:8px"  id="hideaddbuyerdiv"  onclick="hidebuyerdiv()"><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div style='clear:both'></div>
			<div id='invitediv' class='premium-indicatior addbuyerdiv' style='display:none'>
			<div class='left' style='font-size:12px;padding:12px'>
				<?php echo JText::_('COM_ACTIVITY_SPONSOR')?>
			</div>
			<div class='right' style='padding:4px'>
				<div class='profile-btn right' style='width:auto'><a class='button gradient-green left' href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile#invitesection') ?>">&nbsp;  &nbsp; <span class='create-referral'>  <?php echo JText::_('COM_USERPROF_PROF_INV')?> &nbsp;  &nbsp; </span></a>
			    <span class='left text-link cursor-point' style='padding:8px'  id='not-now' onclick='hideinvitediv()'><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div style='clear:both'></div>
		<div class="activity-list">
			<?php
			$savepop_arr = array();
			$newpop_arr = array();
			$nb_elem_per_page = 10;
			$data = (array)$this->data;
			$number_of_pages = intval(count($data)/$nb_elem_per_page)+1;
			foreach($data as $activity){

				$message = "";
				$reality_desc = "";
				$reality_item = '';
				$buttons = null;
				$hasquestion = false;
				$question = '';
				$imageleft = "";
				if(in_array($activity->activity_type, $selfimage)){
					$imageleft = $activity->myimage;
				} else {
					$imageleft = $activity->image;
				}
				//add and edit pops
				if($activity->activity_type == 1 || $activity->activity_type == 2){
					$hasquestion = false;
					if($activity->activity_type == 1 || $activity->activity_type == 2) {
						$propertylink = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$activity->property_id);
					} else if($activity->activity_type == 5) {
						$propertylink = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$activity->property_id);
					}
					if($activity->activity_type == 1) {
						$message = JText::_('COM_ACTIVITY_POPSUPD');
					} else if($activity->activity_type == 2) {
						$message = JText::_('COM_ACTIVITY_POPSADD');
					} else {
						$message = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid=')."&uid=".$activity->other_user_id."'>".stripslashes_all($activity->name)."</a> has viewed your POPs&trade; in <a href='".$propertylink."' class='text-link'>".$activity->address."</a>";	
					}
					$userinfos = $this->userDetails;
					$flagicon="";
					/*if($this->user_info->country!=$activity->countries_id){
						if($activity->country == 0 && $this->user_info->country!=223){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
						} elseif($activity->country != 0) {
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->countries_iso_code_2).'-flag-lang.png">';
						}								
					}*/
			
					if($this->user_info->country!=$activity->country){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->countries_iso_code_2).'-flag-lang.png">';
					}	
					$reality_desc = '
						<a href="'.$propertylink.'" class="realty-small-thumb left">
							<img  src="'.trim($activity->propertyImage).'?'.microtime(true).'" height="46px"/>
						</a>
						<a href="'.$propertylink.'" class="item-number text-link">
							'.$flagicon.' '.$activity->zip.'
						</a>
						<span class="reality-description">
							'.stripslashes_all($activity->property_name).' 
						</span> <br>
						<span class="reality-description">
							'.(($activity->disclose == 1 || JFactory::getUser()->id == $activity->user_id) ? format_currency_global($activity->price1,$activity->symbol).((!empty($activity->price2)) ? ' - '.format_currency_global($activity->price2,$activity->symbol) : '')." ".$activity->currency : 'price undisclosed').'
						</span>
					';
				}
				//edit profile
				else if($activity->activity_type==3){
					$hasquestion = false;
					$message = JText::_('COM_ACTIVITY_PROFUPD');
					$reality_desc = stripslashes_all($activity->message); 
				}
				//update password
				else if($activity->activity_type==31){
					$hasquestion = false;
					$message = JText::_('COM_ACTIVITY_PWUPD');
					$reality_desc = $activity->message; 
				}
				//request to view private pops
				else if($activity->activity_type==6){
					$propertylink = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$activity->property_id);
					$hasquestion = true;
					$r1 = '<a href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'"class="text-link">'.stripslashes_all($activity->name).'</a>';
					$message = JText::sprintf('COM_ACTIVITY_R1REQPOP', $r1);
					$reality_desc = '
							<a href="'.$propertylink.'" class="realty-small-thumb left">
								<img  src="'.trim($activity->propertyImage).'?'.microtime(true).'" height="46px"/>
							</a>
							<a href="'.$propertylink.'" class="item-number text-link">
								'.$activity->zip.'
							</a>
						<span class="reality-description">
							'.stripslashes_all($activity->property_name).'
						</span>'; 
					if($activity->permission == 0){
						$question = JText::_('COM_ACTIVITY_REQ2VIEWQ');
						$buttons = array(
										array(
											'href'=>"javascript:void(0);",
											'label'=>JText::_('COM_ACTIVITY_BT_ACCEPT'),
											'class'=>'accept gradient-blue',
											'onclick'=>"acceptListing('".$activity->activity_id."', '".$activity->other_user_id."')",
											'spanclass'=>'accept-text'
										)
									);
					}
					else if($activity->permission == 1){
						$question = JText::_('COM_ACTIVITY_REQ2VIEWAPP');
					}
				}
				//request to view pops approved
				else if($activity->activity_type==7){
					$flagicon="";
					/*if($this->user_info->country!=$activity->countries_id){
						if($activity->country == 0 && $this->user_info->country!=223){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
						} elseif($activity->country != 0) {
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->countries_iso_code_2).'-flag-lang.png">';
						}								
					}*/
					if($this->user_info->country!=$activity->country){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->countries_iso_code_2).'-flag-lang.png">';
					}	
					$hasquestion = false;
					$propertylink = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$activity->property_id);
					$r2 = '<a href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'" class="text-link">'.stripslashes_all($activity->name).'</a>';
					$message = JText::sprintf('COM_ACTIVITY_REQ2VIEWAPP2', $r2);
					$reality_desc = '
						<a href="'.$propertylink.'" class="realty-small-thumb left">
							<img  src="'.trim($activity->propertyImage).'?'.microtime(true).'" height="46px"/>
						</a>
						<a href="'.$propertylink.'" class="item-number text-link">
							'.$flagicon.' '.$activity->zip.'
						</a>
						<span class="reality-description">
							'.stripslashes_all($activity->property_name).' 
						</span> <br>
						<span class="reality-description">
							'.(($activity->disclose == 1 || JFactory::getUser()->id == $activity->user_id) ? format_currency_global($activity->price1,$activity->symbol).((!empty($activity->price2)) ? ' - '.format_currency_global($activity->price2,$activity->symbol) : '')." ".$activity->currency : 'price undisclosed').'
						</span>
					';
				}
				// request to join network showing on requestee's feed
				else if( $activity->activity_type==8){
					$hasquestion = true;
					$message = '
						<a
							href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'"
							class="text-link"
						>'.stripslashes_all($activity->name).'
						</a> requested to join your Network and view your POPs&#8482.
						';
					if($activity->status==0){
						$question = "Would you like to accept this request?";
						$buttons = array(
										array(
												'href'=>"javascript:void(0);",
												'label'=>JText::_('COM_ACTIVITY_BT_ACCEPT'),
												'class'=>'accept gradient-blue',
												'onclick'=>"acceptNetworkRequest('".$activity->activity_id."')",
												'spanclass'=>'accept-text'
										),
										array(
												'href'=>"javascript:void(0);",
												'label'=>JText::_('COM_ACTIVITY_BT_DEC'),
												'class'=>'decline gradient-gray',
												'onclick'=>"declineNetworkRequest('".$activity->activity_id."')",
												'spanclass'=>'decline-text'
										)
								);
					}
					else if ($activity->status==1){
							$status2 = in_network( $activity->other_user_id, JFactory::getUser()->id);
							if($status2==null || $status2==2){
								$question = "You have accepted this request. Would you like to join ".stripslashes_all($activity->name)."'s Network? <a href='javascript:void(0)' id='questioninvite' onclick='requestNetworkPostAccept(" . $activity->other_user_id . ")' class='button accept gradient-blue' >Join</a>
								<a href='javascript:void(0)' id='questionaccept' style='display:none' class='button accept gradient-blue' >".JText::_('COM_NETWORK_TERMS_PEND')."</a>";
							} else if($status2==1 || $status2==0){
								$question = JText::_('COM_ACTIVITY_REQ2VIEWAPP');
							}
					}
					else if ($activity->status==2){
						$question = "You have declined this request.";
					} else {}
				}
				// response to request to join network
				else if ($activity->activity_type==30){
					$imageleft = $activity->myimage;
					$hasquestion = true;
					$r2 = '<a href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'" class="text-link">'.stripslashes_all($activity->name).'</a>';
					$message = JText::sprintf('COM_ACTIVITY_REQ2VIEW2', $r2);
					if($activity->status==0){
						$question = JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>";
					}
					else if ($activity->status==1){
						$question = '<a
							href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'"
							class="text-link" style="font-size:11px"
						>'.stripslashes_all($activity->name).'
						</a> has accepted this request.';
					}
					else if ($activity->status==2){
						$question = '<a
							href="'.JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'"
							class="text-link" style="font-size:11px"
						>'.stripslashes_all($activity->name).'
						</a> has declined this request.';
					} else {}
				}
				// premium activation
				else if( $activity->activity_type==27 ) {
					if(in_array($this->user_info->user_type, array(1,2,4))) //1, 2 and 4
						$message = JText::_('COM_ACTIVITY_MEMINVITES');
					else
						$message = JText::_('COM_ACTIVITY_REGMEMINVITES');
				}
				// invitation to view one's pops from registration
				else if( $activity->activity_type==28 ) {
					if($activity->status==0){
					$hasquestion = true;
					$message = "You have been invited to join <a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class=text-link>".stripslashes_all($activity->name)."'s</a> Network. If you accept this request, you will be able to view <a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class=text-link>".stripslashes_all($activity->name)."</a>'s private POPs&trade;.";
						$question = "Would you like to accept this request?";
						$buttons = array(
										array(
												'href'=>"javascript:void(0);",
												'label'=>JText::_('COM_ACTIVITY_BT_ACCEPT'),
												'class'=>'accept gradient-blue',
												'onclick'=>"acceptContactNetworkRequest('".$activity->activity_id."')",
												'spanclass'=>'accept-text'
										),
										array(
												'href'=>"javascript:void(0);",
												'label'=>JText::_('COM_ACTIVITY_BT_DEC'),
												'class'=>'decline gradient-gray',
												'onclick'=>"declineContactNetworkRequest('".$activity->activity_id."')",
												'spanclass'=>'decline-text'
										)
								);
					}
					else if ($activity->status==1){
						$hasquestion = true;
						$message = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class=text-link>".stripslashes_all($activity->name)."</a> is requesting to add you to his/her Network. If you accept this request,
								<br /> you will be able to view <a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class=text-link>".stripslashes_all($activity->name)."</a>'s private POPs&trade;.";
						$question = JText::_('COM_ACTIVITY_REQ2VIEWAPP');
					}
					else if ($activity->status==0){
						$message = "You have declined this request.";
					}
				}
				// network request accepted
				else if( $activity->activity_type==29 ) {
						$hasquestion = false;
						$message = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class=text-link>".stripslashes_all($activity->name)."</a> 
						is now added to your Network and will be able to view your private POPs&trade;.";
				}
				else if($activity->activity_type==9){
					$hasquestion = false;
					$message = "Your request to view <a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."'>".stripslashes_all($activity->name)."</a>'s private POPs&trade; has been ".(($activity->status == 1) ? "accepted" : "declined")." .";
				}
				// regular member activation
				else if($activity->activity_type==10){
					$hasquestion = false;
					$message = JText::_('COM_ACTIVITY_REGMEMINVITES');
				} 
				else if( $activity->activity_type==23 || $activity->activity_type==24 || $activity->activity_type==25 || $activity->activity_type==26 ){
					$hasquestion = false;
					$message = "";
					$listing_link = "";
					if($activity->activity_type==23){
						$message = JText::_('COM_ACTIVITY_BUYERADD');
					} else if($activity->activity_type==24){
						$message = JText::_('COM_ACTIVITY_BUYERADDMATCH');
					} else if($activity->activity_type==25){

						
						$user_id = JFactory::getUser()->id;
						$arr_allowed = explode(",", $activity->listing[0]->allowed);
						if($user_id == $activity->other_user_id) {
							
							
							$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
							$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
							$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
							//$message = "Your POPs&#8482;, <a href='".$listing_link."'>".stripslashes_all($activity->listing[0]->property_name)."</a>, is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";			
							$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_YOU', $buyerlink0,stripslashes_all ($activity->buyer[0]->name));
							$button_id = $activity->actid;
							if($activity->show_button){
								$show_this = "display:none;";
								$show_this2 = "";
							} else {
								$show_this2 = "display:none;";
								$show_this = "";
							}
							$question = "
								<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')." </div> 
								<div class='actbtn'>
									<div class='left margtop'><a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a></div>
									<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
								</div>
								<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
								<input type='hidden' class='aida' value='".$user_id."' />
								<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
								<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
								";
						} else {
							if($activity->listing[0]->setting == 1) {
								//pop setting is not private

								if($activity->listing[0]->selected_permission == 0 || $activity->listing[0]->selected_permission == 1) {

									$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
									$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
									$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
									//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "</a>'s POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
									$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
									$button_id = $activity->actid;
									if($activity->show_button){
										$show_this = "display:none;";
										$show_this2 = "";
									} else {
										$show_this2 = "display:none;";
										$show_this = "";
									}
									$question = "
										<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
										<div class='actbtn'>
											<div class='left margtop '> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
											<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
										</div>
										<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
										<input type='hidden' class='aida' value='".$user_id."' />
										<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
										<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
										";
								//if not private but restrited to agents within the user's state
								} else if($activity->listing[0]->selected_permission == 7) {
									if($activity->listing[0]->owner_state === $this->user_info->state) {
										if(!$activity->listing[0]->per) {
											if($activity->listing[0]->per === "0") {
												//request view is pending
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>";
											} else {
												//request view
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "".JText::_('COM_ACTIVITY_POPSDO')." <div class='actbtnreq'><a onclick='requestAccess(" . $activity->other_user_id . ", " . $activity->listing[0]->listing_id . ", " . $activity->buyer[0]->buyer_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
												<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
														<img height=\"26px\" id=\"loading-image_custom_question\"
															src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
															alt=\"Loading...\"
															class=\"right\"
														/>
													</div>";
											}
										} else {
											if($activity->listing[0]->per == 1) {
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												#$question = "".JText::_('COM_ACTIVITY_POPSDO')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> <a onclick='popTermsb(". $activity->listing[0]->listing_id .")' href='javascript:void(0);' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  ";
												$button_id = $activity->actid;
												if($activity->show_button){
													$show_this = "display:none;";
													$show_this2 = "";
												} else {
													$show_this2 = "display:none;";
													$show_this = "";
												}
												$question = "
													<div class='left'>".JText::_('COM_ACTIVITY_POPSDO')."</div>
													<div class='actbtn'>
														<a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> 
														<a id='" . $button_id . "' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' href='javascript:void(0);' class='button accept gradient-blue' style='".$show_this."' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  
													</div>
													<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
													<input type='hidden' class='aida' value='".$user_id."' />
													<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
													<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
												";
												
											} else if($activity->listing[0]->per == 2) {
												//request denied
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "Your request to view has been declined.";
											}
										}
									} else {
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "</a>'s POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$button_id = $activity->actid;
										if($activity->show_button){
											$show_this = "display:none;";
											$show_this2 = "";
										} else {
											$show_this2 = "display:none;";
											$show_this = "";
										}
										$question = "
											<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
											<div class='actbtn'>
												<div class='left margtop '> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
												<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
											</div>
											<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
											<input type='hidden' class='aida' value='".$user_id."' />
											<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
											<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
											";
									}
									
									/*
									echo "<pre>", print_r($activity), "</pre>";
									echo "Owner State: " . $activity->listing[0]->owner_state . "<br />";
									echo "Current User State: " . $this->user_info->state;
									*/
								
								//if not private but restrited to 
								} else if($activity->listing[0]->selected_permission == 3) {
									$is_private = 0;
									
									if($this->previous_year == 2012 && !$this->user_info->{"verified_".$previous_year}) {
										$is_private = 1;
									} else {
										$previous_year_volume_minimum = $activity->listing[0]->values;
										if($this->user_currency !== "USD") {
											$item_user_currency_rate = $this->ex_rates->rates->{$this->user_currency};
											$previous_year_volume_minimum_converted = $previous_year_volume_minimum * $item_user_currency_rate;
										} else {
											$previous_year_volume_minimum_converted = $previous_year_volume_minimum;
										}
										
										if($this->user_info->currency !== "USD") {
											$user_currency_rate = $this->ex_rates->rates->{$userDetails->currency};
											$user_previous_year_volume_converted = $this->user_previous_year_volume * $user_currency_rate;
										} else {
											$user_previous_year_volume_converted = $this->user_previous_year_volume;
										}
										#echo $user_previous_year_volume_converted . " " . $previous_year_volume_minimum_converted;
										if($user_previous_year_volume_converted <= $previous_year_volume_minimum_converted) {
											$is_private = 1;
										}
									}
									
									if($is_private) {
										if(!$activity->listing[0]->per) {
											if($activity->listing[0]->per === "0") {
												//request view is pending
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>";
											} else {
												//request view
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "".JText::_('COM_ACTIVITY_POPSDO')." <div class='actbtnreq'><a onclick='requestAccess(" . $activity->other_user_id . ", " . $activity->listing[0]->listing_id . ", " . $activity->buyer[0]->buyer_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
												<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
														<img height=\"26px\" id=\"loading-image_custom_question\"
															src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
															alt=\"Loading...\"
															class=\"right\"
														/>
													</div>";
											}
										} else {
											if($activity->listing[0]->per == 1) {
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												$button_id = $activity->actid;
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												#$question = "".JText::_('COM_ACTIVITY_POPSDO')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> <a onclick='popTermsb(". $activity->listing[0]->listing_id .")' href='javascript:void(0);' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  ";
												
											
												if($activity->show_button){
													$show_this = "display:none;";
													$show_this2 = "";
												} else {
													$show_this2 = "display:none;";
													$show_this = "";
												}
												$question = "
													<div class='left'>".JText::_('COM_ACTIVITY_POPSDO')."</div>
													<div class='actbtn'>
														<a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> 
														<a id='" . $button_id . "' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' href='javascript:void(0);' class='button accept gradient-blue' style='".$show_this."' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  
													</div>
													<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
													<input type='hidden' class='aida' value='".$user_id."' />
													<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
													<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
												";
											} else if($activity->listing[0]->per == 2) {
												//request denied
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "Your request to view has been declined.";
											}
										}	
									} else {
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "</a>'s POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$button_id = $activity->actid;
										if($activity->show_button){
											$show_this = "display:none;";
											$show_this2 = "";
										} else {
											$show_this2 = "display:none;";
											$show_this = "";
										}
										$question = "
											<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
											<div class='actbtn'>
												<div class='left margtop '> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
												<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
											</div>
											<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
											<input type='hidden' class='aida' value='".$user_id."' />
											<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
											<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
											";										
									}
									//echo "My Previous Year Volume: " . $this->user_previous_year_volume;
									//echo $activity->listing[0]->values;
									//echo "<pre>", print_r($activity), "</pre>";
									//echo "Owner State: " . $activity->listing[0]->owner_state . "<br />";
									//echo "Current User State: " . $this->user_info->state;
									
								} else if($activity->listing[0]->selected_permission == 4) {
									$is_private = 0;
									if($previous_year == 2012 && !$this->user_info->{"ave_price_".$previous_year}) {
										$is_private = 1;
									} else {
										$previous_year_ave_price_minimum = $activity->listing[0]->values;
										if($this->user_currency !== "USD") {
											$item_user_currency_rate = $this->ex_rates->rates->{$this->user_currency};
											$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum * $item_user_currency_rate;
										} else {
											$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum;
										}
										
										if($this->user_info->currency !== "USD") {
											$user_currency_rate = $this->ex_rates->rates->{$this->user_info->currency};
											$user_previous_year_ave_price_converted = $this->user_previous_year_ave_price * $user_currency_rate;
										} else {
											$user_previous_year_ave_price_converted = $this->user_previous_year_ave_price;
										}
										
										if($user_previous_year_ave_price_converted <= $previous_year_ave_price_minimum_converted) {
											$is_private = 1;
										}
									}
									
									if($is_private) {
										if(!$activity->listing[0]->per) {
											if($activity->listing[0]->per === "0") {
												//request view is pending
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>";
											} else {
												//request view
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "".JText::_('COM_ACTIVITY_POPSDO')." <div class='actbtnreq'><a onclick='requestAccess(" . $activity->other_user_id . ", " . $activity->listing[0]->listing_id . ", " . $activity->buyer[0]->buyer_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
												<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
														<img height=\"26px\" id=\"loading-image_custom_question\"
															src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
															alt=\"Loading...\"
															class=\"right\"
														/>
													</div>";
											}
										} else {
											if($activity->listing[0]->per == 1) {
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												$button_id = $activity->actid;
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												#$question = "".JText::_('COM_ACTIVITY_POPSDO')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> <a onclick='popTermsb(". $activity->listing[0]->listing_id .")' href='javascript:void(0);' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  ";
												if($activity->show_button){
													$show_this = "display:none;";
													$show_this2 = "";
												} else {
													$show_this2 = "display:none;";
													$show_this = "";
												}
												$question = "
													<div class='left'>".JText::_('COM_ACTIVITY_POPSDO')."</div>
													<div class='actbtn'>
														<a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> 
														<a id='" . $button_id . "' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' href='javascript:void(0);' class='button accept gradient-blue' style='".$show_this."' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  
													</div>
													<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
													<input type='hidden' class='aida' value='".$user_id."' />
													<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
													<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
												";
											} else if($activity->listing[0]->per == 2) {
												//request denied
												$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
												$listing_link = "javascript:void(0);";
												$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
												//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
												$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
												$question = "Your request to view has been declined.";
											}
										}	
									} else {
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "</a>'s POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$button_id = $activity->actid;
										if($activity->show_button){
											$show_this = "display:none;";
											$show_this2 = "";
										} else {
											$show_this2 = "display:none;";
											$show_this = "";
										}
										$question = "
											<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
											<div class='actbtn'>
												<div class='left margtop '> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
												<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
											</div>
											<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
											<input type='hidden' class='aida' value='".$user_id."' />
											<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
											<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
											";										
									}
								}
							} else if($activity->listing[0]->setting == 2) {
								//pop setting is private
								if(!$activity->listing[0]->per) {
									if($activity->listing[0]->per === "0") {
										//request view is pending
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$question = JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>";
									} else {
										//request view
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$question = "".JText::_('COM_ACTIVITY_POPSDO')." <div class='actbtnreq'><a onclick='requestAccess(" . $activity->other_user_id . ", " . $activity->listing[0]->listing_id . ", " . $activity->buyer[0]->buyer_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
										<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
												<img height=\"26px\" id=\"loading-image_custom_question\"
													src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
													alt=\"Loading...\"
													class=\"right\"
												/>
											</div>";
									}
								} else {
									if($activity->listing[0]->per == 1) {
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										$button_id = $activity->actid;
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										#$question = "".JText::_('COM_ACTIVITY_POPSDO')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> <a onclick='popTermsb(". $activity->listing[0]->listing_id .")' href='javascript:void(0);' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  ";
										if($activity->show_button){
											$show_this = "display:none;";
											$show_this2 = "";
										} else {
											$show_this2 = "display:none;";
											$show_this = "";
										}
										$question = "
											<div class='left'>".JText::_('COM_ACTIVITY_POPSDO')."</div>
											<div class='actbtn'>
												<a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> 
												<a id='" . $button_id . "' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' href='javascript:void(0);' class='button accept gradient-blue' style='".$show_this."' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  
											</div>
											<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
											<input type='hidden' class='aida' value='".$user_id."' />
											<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
											<div class='".$button_id." left margtop' style='".$show_this2." margin-left:5px;'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
										";
									} else if($activity->listing[0]->per == 2) {
										//request denied
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$question = "Your request to view has been declined.";
									}
								}
							}
						}
						
						/*
						if($user_id == $activity->other_user_id) {
							$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
							$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
							$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
							//$message = "Your POPs&#8482;, <a href='".$listing_link."'>".stripslashes_all($activity->listing[0]->property_name)."</a>, is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";			
							$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_YOU', $buyerlink0,stripslashes_all ($activity->buyer[0]->name));
							$button_id = $activity->actid;
							if($activity->show_button){
								$show_this = "display:none;";
								$show_this2 = "";
							} else {
								$show_this2 = "display:none;";
								$show_this = "";
							}
							$question = "
								<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')." </div> 
								<div class='actbtn'>
									<div class='left margtop'><a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a></div>
									<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
								</div>
								<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
								<input type='hidden' class='aida' value='".$user_id."' />
								<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
								<div class='".$button_id." left margtop' style='".$show_this2."'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
								";
						} else {
							if(in_array($user_id, $activity->user_network)) {
								//owner is already in network of viewer
								if($activity->listing[0]->setting == 1) {
									//pop setting is not private
									$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
									$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
									$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
									//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "</a>'s POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
									$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
									$button_id = $activity->actid;
									if($activity->show_button){
										$show_this = "display:none;";
										$show_this2 = "";
									} else {
										$show_this2 = "display:none;";
										$show_this = "";
									}
									$question = "
										<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
										<div class='actbtn'>
											<div class='left margtop '> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
											<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
										</div>
										<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
										<input type='hidden' class='aida' value='".$user_id."' />
										<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
										<div class='".$button_id." left margtop' style='".$show_this2."'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
										";
								} else if($activity->listing[0]->setting == 2) {
									//pop setting is private
									if(!$activity->listing[0]->per) {
										if($activity->listing[0]->per === "0") {
											//request view is pending
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = "javascript:void(0);";
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$question = JText::_('COM_ACTIVITY_REQSENT')." <a onclick='' class='button accept gradient-blue'>".JText::_('COM_NETWORK_TERMS_PEND')."</a>";
										} else {
											//request view
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = "javascript:void(0);";
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$question = "".JText::_('COM_ACTIVITY_POPSDO')." <div class='actbtnreq'><a onclick='requestAccess(" . $activity->other_user_id . ", " . $activity->listing[0]->listing_id . ", " . $activity->buyer[0]->buyer_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
											<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
													<img height=\"26px\" id=\"loading-image_custom_question\"
														src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
														alt=\"Loading...\"
														class=\"right\"
													/>
												</div>";
										}
									} else {
										if($activity->listing[0]->per == 1) {
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$question = "".JText::_('COM_ACTIVITY_POPSDO')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >View</a> <a onclick='popTermsb(". $activity->listing[0]->listing_id .")' href='javascript:void(0);' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a>  ";
										} else if($activity->listing[0]->per == 2) {
											//request denied
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = "javascript:void(0);";
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$question = "Your request to view has been declined.";
										}
									}
								} 
							} else {
								if(isset($activity->user_network_raw[$user_id])) {											
									if($activity->user_network_raw[$user_id] == 2) {
										//request network denied
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$question = "Your request to view has been declined.";
									} else if($activity->user_network_raw[$user_id] == 0) {
										//request network pending
										if ($activity->listing[0]->selected_permission == 1){
											//$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);	
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$button_id = $activity->actid;
											if($activity->show_button){
												$show_this = "display:none;";
												$show_this2 = "";
											} else {
												$show_this2 = "display:none;";
												$show_this = "";
											}
											$question = "
												<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
												<div class='actbtn'>
													<div class='left margtop'> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
													<div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
												</div>
												<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
												<input type='hidden' class='aida' value='".$user_id."' />
												<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
												<div class='".$button_id." left margtop' style='".$show_this2."'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
												";
										} else {
											$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
											$listing_link = "javascript:void(0);";
											$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
											//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
											$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
											$question = "Your request to view has been sent. <a onclick='' class='button accept gradient-blue'>".JText::_('COM_NETWORK_TERMS_PEND')."</a> pending";
										}
									}
								} else {
									if ($activity->listing[0]->selected_permission == 1){
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
									//	$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$button_id = $activity->actid;
									if($activity->show_button){
											$show_this = "display:none;";
											$show_this2 = "";
										} else {
											$show_this2 = "display:none;";
											$show_this = "";
										}					
									$question = "
										<div class='left' >".JText::_('COM_ACTIVITY_POPSDO')."</div> 
										<div class='actbtn'>
											<div class='left margtop'> <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a> </div>
										    <div class='left margtop'> <a style='".$show_this."' id='".$button_id."' href='javascript:void(0);' onclick='saveSingleBuyer(". $user_id .", ".$activity->listing[0]->listing_id.", ".$activity->buyer[0]->buyer_id.", ".$button_id.")' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_SAVE')."</a> </div>
										</div>
										<input type='hidden' class='pida' value='".$activity->listing[0]->listing_id."' />
										<input type='hidden' class='aida' value='".$user_id."' />
										<input type='hidden' class='buyer_arr' value='".$activity->buyer[0]->buyer_id."' />
										<div class='".$button_id." left margtop' style='".$show_this2."'>".JText::_('COM_ACTIVITY_BUYERSAVED')."</div>
										";
									} else {
									//not yet requested
										$property_owner_link = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
										$listing_link = "javascript:void(0);";
										$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
										//$message = "<a href='" . $property_owner_link . "'>" . stripslashes_all($activity->name) . "'s</a> POPs&#8482; is a match to your buyer, <a href='".$buyerlink0."'>".stripslashes_all ($activity->buyer[0]->name)."</a>.";
										$message = JText::sprintf('COM_ACTIVITY_MATCH_YOURPOPS_ALL', $property_owner_link, stripslashes_all($activity->name),$buyerlink0,stripslashes_all ($activity->buyer[0]->name));
										$question = "".JText::_('COM_ACTIVITY_POPSDO')."<div class='actbtnreq'><a onclick='requestNetwork(" . $activity->other_user_id . ", " . $activity->pkId . ");' href='".$listing_link."' class='button accept gradient-blue ra" . $activity->pkId . "' >".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a></div>
										<div class=\"right\" style=\"display:none; margin:-18px 0 0 5px;position:relative;float:right;\" id=\"loading" . $activity->pkId . "\">
											<img height=\"26px\" id=\"loading-image_custom_question\"
												src=\"https://www.agentbridge.com/images/ajax_loader.gif\"
												alt=\"Loading...\"
												class=\"right\"
											/>
										</div>";
									}
								}
							}
						}
						*/
					} else if($activity->activity_type==26){
						$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
						$buyerlink0 = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
						$message = JText::sprintf('COM_ACTIVITY_SAVEDPOPS', $buyerlink0, stripslashes_all ($activity->buyer[0]->name));
						$question = JText::_('COM_ACTIVITY_VIEWNOW')." <a onclick='' href='".$listing_link."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_BT_VIEW')."</a>";
					}
					$buyerlink = JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $activity->buyer[0]->buyer_id);
					$price = explode('-', $activity->buyer[0]->price_value); 					
					if($activity->activity_type==23 || $activity->activity_type==24){
						$buyerlink_edit = JRoute::_('index.php?option=com_propertylisting&task=editbuyerprofile&lID=' . $activity->buyer[0]->buyer_id);
					if($activity->buyer[0]->home_buyer && $activity->activity_type==23 ){
						$message = JText::_('COM_ACTIVITY_HOMEMARKET');
						$question = "<div class='left'>".JText::_('COM_ACTIVITY_HOMEMARKETQ')."</div><div class='actbtnreq'><a onclick='' href='".$buyerlink_edit."' class='button accept gradient-blue' >".JText::_('COM_ACTIVITY_HOMEMARKETEDIT')."</a></div>";
						if(!$activity->buyer[0]->needs[0]->currency){
							$activity->buyer[0]->needs[0]->currency=$this->user_info->currency;
							$activity->buyer[0]->needs[0]->symbol=$this->user_info->symbol;
						}
					}
					$flagicon="";
					if($this->user_info->country!=$activity->buyer[0]->needs[0]->country){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->buyer[0]->needs[0]->countries_iso_code_2).'-flag-lang.png">';					
					}
					$buyerzip = explode(",",$activity->buyer[0]->needs[0]->zip);
					$buyerzip = implode(", ", $buyerzip);
					$buyer_desc = '
						<a href="'.$buyerlink.'" class="realty-small-thumb left">
							<img  src="'.$activity->buyer[0]->propertyImage.'?'.microtime(true).'" height="46px"/>
						</a>
						<a href="'.$buyerlink.'" class="item-number text-link">
							'.stripslashes_all ($activity->buyer[0]->name).'
						</a>
						<span class="reality-description">
							'.$flagicon.' '.$buyerzip.'
						</span>
						<br>
						<span class="reality-description price_con">
							'.format_currency_global($price[0],$activity->buyer[0]->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$activity->buyer[0]->needs[0]->symbol) : '').' '.$activity->buyer[0]->needs[0]->currency.'
						</span>
						';
					} else if($activity->activity_type==25){
						$flagicon="";
						if($this->user_info->country!=$activity->listing[0]->countries_id){
								$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->listing[0]->countries_iso_code_2).'-flag-lang.png">';
						}
							if($user_id == $activity->other_user_id) {
								$buyer_desc = '
								<a href="'.$listing_link.'" class="realty-small-thumb left">
									<img  src="'.$activity->listing[0]->images.'" height="46px"/>
								</a>
								<a href="'.$listing_link.'" class="item-number text-link">
									'.$activity->listing[0]->property_desc.'
								</a>
								<a href="'.$listing_link.'" class="item-number text-link">
									'.$flagicon." ".$activity->listing[0]->zip.' 
								</a>
								<span class="reality-description">'.stripslashes_all($activity->listing[0]->property_name).' </span> <br/>
								<span class="reality-description">
									'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency : 'price undisclosed').'
								</span>
								';
							} else {
								if(in_array($user_id, $activity->user_network)) {
									//owner is already in network of viewer
									if($activity->listing[0]->setting == 1) {
										$buyer_desc = '
										<a href="'.$listing_link.'" class="realty-small-thumb left">
											<img  src="'.$activity->listing[0]->images.'" height="46px"/>
										</a>
										<a href="'.$listing_link.'" class="item-number text-link">
											'.$activity->listing[0]->property_desc.'
										</a>
										<a href="'.$listing_link.'" class="item-number text-link">
											'.$flagicon." ".$activity->listing[0]->zip.' 
										</a>
										<span class="reality-description">
											'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency  : 'price undisclosed').'
										</span>
										';
									} else if($activity->listing[0]->setting == 2) {
										//pop setting is private
										if(!$activity->listing[0]->per) {
											if($activity->listing[0]->per === "0") {
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" width="56px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
												</a>
												<span class="reality-description">
													Your request to view this POPs&trade; is pending approval.
												</span>
												';
											} else {
											    $r2 = '<a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$activity->listing[0]->user_id.'">'.JFactory::getUser($activity->listing[0]->user_id)->name.'</a>';
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" width="56px"/>
												</a>
												<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
												</span>
												<span class="reality-description">
													<!--This POPs&trade; is private and only viewable by the Owner.-->											
													'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
												</span>
												';
											}
										} else {
											if($activity->listing[0]->per == 1) {
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images.'" height="46px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$activity->listing[0]->property_desc.'
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
												</a>
												<span class="reality-description">
													'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency  : 'price undisclosed').'
												</span>
												';											
											} else if($activity->listing[0]->per == 2) {
												$r2 = '<a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$activity->listing[0]->user_id.'">'.JFactory::getUser($activity->listing[0]->user_id)->name.'</a>';
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" height="46px"/>
												</a>
												<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
												</span>
												<span class="reality-description">
													<!--This POPs&trade; is private and only viewable by the Owner.-->											
													'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
												</span>
												';
											}
										}
									}
								} else {
									if(isset($activity->user_network_raw[$user_id])) {
										if($activity->user_network_raw[$user_id] == 2) {
											if ($activity->listing[0]->selected_permission == 1) {
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyer_desc = '
											<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images.'" height="46px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$activity->listing[0]->property_desc.'
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
											</a>
											<span class="reality-description">
											'.stripslashes_all($activity->listing[0]->property_name).' 
											</span>
											';	
											} else { 
											$r2 = '<a style="font-size:11px" href="' . $property_owner_link . '">'. stripslashes_all($activity->name) .'</a>';
											$buyer_desc = '
											<a href="'.$listing_link.'" class="realty-small-thumb left">
												<img  src="'.$activity->listing[0]->images_bw.'" height="46px"/>
											</a>
											<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
											</span>
											<span class="reality-description">
												<!--This POPs&trade; is only viewable by the Owner&rsquo;s Network members-- >
												'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
											</span>
											';	}
										} else if($activity->user_network_raw[$user_id] == 0) {
											if ($activity->listing[0]->selected_permission == 1) {
											$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
											$buyer_desc = '
											<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images.'" height="46px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$activity->listing[0]->property_desc.'
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
											</a>
											<span class="reality-description">
											'.stripslashes_all($activity->listing[0]->property_name).' 
											</span>
											<br>
											<span class="reality-description">
												'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency : 'price undisclosed').'
											</span>
											';	
											} else {
											$r2 = '<a style="font-size:11px" href="' . $property_owner_link . '">'. stripslashes_all($activity->name) .'</a>';
											$buyer_desc = '
											<span class="realty-small-thumb left">
												<img  src="'.$activity->listing[0]->images_bw.'" height="46px"/>
											</span>
											<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
											</span>
											<span class="reality-description">
												<!--This POPs&trade; is only viewable by the Owner&rsquo;s Network members-->
												'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
											</span>
											';	
											}
										}
									} else {
										//new for public pops
										if ($activity->listing[0]->selected_permission == 1) {
										$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->listing[0]->listing_id);
										$buyer_desc = '
											<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images.'" height="46px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$activity->listing[0]->property_desc.'
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
											</a>
											<span class="reality-description">
											'.stripslashes_all($activity->listing[0]->property_name).' 
											</span> <br>
											<span class="reality-description">
												'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency : 'price undisclosed').'
											</span>
											';	
										} else {
											if(!$activity->listing[0]->per) {
											if($activity->listing[0]->per === "0") {
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" width="56px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
												</a>
												<span class="reality-description">
													Your request to view this POPs&trade; is pending approval.
												</span>
												';
											} else {
											    $r2 = '<a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$activity->listing[0]->user_id.'">'.JFactory::getUser($activity->listing[0]->user_id)->name.'</a>';
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" width="56px"/>
												</a>
												<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
												</span>
												<span class="reality-description">
													<!--This POPs&trade; is private and only viewable by the Owner.-->											
													'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
												</span>
												';
											}
										} else {
											if($activity->listing[0]->per == 1) {
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images.'" height="46px"/>
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$activity->listing[0]->property_desc.'
												</a>
												<a href="'.$listing_link.'" class="item-number text-link">
													'.$flagicon." ".$activity->listing[0]->zip.' 
												</a>
												<span class="reality-description">
													'.(($activity->listing[0]->disclose == 1 || JFactory::getUser()->id == $activity->listing[0]->user_id) ? format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '')." ".$activity->listing[0]->currency  : 'price undisclosed').'
												</span>
												';											
											} else if($activity->listing[0]->per == 2) {
												$r2 = '<a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$activity->listing[0]->user_id.'">'.JFactory::getUser($activity->listing[0]->user_id)->name.'</a>';
												$buyer_desc = '
												<a href="'.$listing_link.'" class="realty-small-thumb left">
													<img  src="'.$activity->listing[0]->images_bw.'" height="46px"/>
												</a>
												<span class="item-number" style="color: #999; font-size:13px">
												'.$flagicon." ".$activity->listing[0]->zip.' 
												</span>
												<span class="reality-description">
													<!--This POPs&trade; is private and only viewable by the Owner.-->											
													'.JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2).'
												</span>
												';
											}
										}
										}
									}
								}
							}
					} else if($activity->activity_type==26){
						$flagicon="";
						if($this->user_info->country!=$activity->listing[0]->countries_id){
								$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($activity->listing[0]->countries_iso_code_2).'-flag-lang.png">';						
						}
						$buyer_desc = '
							<a href="'.$listing_link.'" class="realty-small-thumb left">
								<img  src="'.$activity->listing[0]->images.'" height="46px"/>
							</a>
							<a href="'.$listing_link.'" class="item-number text-link">
								'.$activity->listing[0]->property_desc.'
							</a>
							<a href="'.$listing_link.'" class="item-number text-link">
								'.$flagicon." ".$activity->listing[0]->zip.' 
							</a>
							<span class="reality-description price_con">
								'.format_currency_global($activity->listing[0]->price1,$activity->listing[0]->symbol).((!empty($activity->listing[0]->price2)) ? ' - '.format_currency_global($activity->listing[0]->price2,$activity->listing[0]->symbol) : '').' '.$activity->listing[0]->currency.'
							</span>
							';
					} 
				}
				//incoming referral
				else if($activity->activity_type==11){
					$hasquestion = true;
					$r1 =JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
					$message = JText::sprintf('COM_ACTIVITY_REFSENT2', $r1, stripslashes_all($activity->name),$activity->status->value->referral_value);
					$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT').": ".format_name_global(stripslashes_all(stripslashes_all (stripslashes_all ($activity->referral->buyer->name))))."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." : ".JText::_($activity->referral->intention)."<br />";
					if($activity->referral->price_1 || $activity->referral->price_2) {
						$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." <span class='price_con'> ".(($activity->referral->price_1 == $activity->referral->price_2) ? format_currency_global($activity->referral->price_1,$activity->referral->symbol) : format_currency_global($activity->referral->price_1,$activity->referral->symbol)." - ".format_currency_global($activity->referral->price_2,$activity->referral->symbol))." ".$activity->thisuser_info->currency."</span>";
					}
					if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by == JFactory::getUser()->id){
						$question = "You have declined this referral.";
					}else if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by != JFactory::getUser()->id){
						$question =  stripslashes_all($activity->name)." has retracted this referral.";
					}else if(is_object($activity->status->response) && $activity->status->response->status==7 && $activity->status->edited_by == JFactory::getUser()->id && ($activity->status->value->referral_value!=$activity->status->response->value->referral_value)){
						$question = "You have countered this referral requesting ".$activity->status->response->value->referral_value;
					}else if(is_object($activity->status->response) && $activity->status->response->status==7 && $activity->status->edited_by == JFactory::getUser()->id){
						$reflink = JRoute::_('index.php?option=com_propertylisting&task=agreement_ab')."&ref_id=".$activity->referral->referral_id;
						$question = JText::sprintf('COM_ACTIVITY_REFACC', $reflink);
					}else{
						$question = JText::_('COM_ACTIVITY_REFDO');
						$buttons = array(
									array(
											'href'=>"javascript:void(0);",
											'label'=>JText::_('COM_ACTIVITY_BT_ACCEPT'),
											'class'=>'accept gradient-blue',
											'onclick'=>"acceptReferral('".$activity->activity_id."','".JFactory::getUser()->id."')",
											'spanclass'=>'accept-text'
									),
									array(
											'href'=>"javascript:void(0);",
											'label'=>JText::_('COM_ACTIVITY_BT_DEC'),
											'class'=>'decline gradient-gray',
											'onclick'=>"declineReferral('".$activity->activity_id."')",
											'spanclass'=>'decline-text'
									)
							);
					}
				}
				//response to request for referral
				else if($activity->activity_type==12){
					$hasquestion = false;
					if($activity->status->created_by == JFactory::getUser()->id){
						$imageleft = $activity->myimage;
						$message = "You have ";
						$clientname = format_name_global(stripslashes_all($activity->referral->client->name));
					}
					else{
						$imageleft = $activity->image;
						$message = "<a href = '".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."'>".stripslashes_all($activity->name)."</a> ";
						$clientname = stripslashes_all($activity->referral->client->name);
					}
					if($activity->referral->agent_a == JFactory::getUser()->id && $activity->referral->agent_a==$activity->status->created_by){
						$message .= "retracted ";
					}
					else{
						$message .= "declined ";
					}
					$message .= " the referral on client ".$clientname." on a ".$activity->status->value->referral_value." referral fee";
					$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT')." ".$clientname."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." ".JText::_($activity->referral->intention)."<br />";
					if($activity->referral->price_1 || $activity->referral->price_2) {
						$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." <span class='price_con'>".(($activity->referral->price_1 == $activity->referral->price_2) ? format_currency_global($activity->referral->price_1,$activity->referral->symbol) : format_currency_global($activity->referral->price_1,$activity->referral->symbol)." - ".format_currency_global($activity->referral->price_2,$activity->referral->symbol))."</span>";
					}
				}
				//referral countered
				else if($activity->activity_type==13){
					$hasquestion = true;
					if($activity->referral->agent_a == JFactory::getUSer()->id)
						$clientname = stripslashes_all (stripslashes_all (stripslashes_all ($activity->referral->buyer->name)));
					else
						$clientname = format_name_global(stripslashes_all (stripslashes_all ($activity->referral->buyer->name)));
					$message = "<a href = '".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."'>".stripslashes_all($activity->name)."</a> has countered your referral on client ".$clientname." with a ".$activity->status->value->referral_value." referral fee";
					$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT').": ".$clientname."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." ".JText::_($activity->referral->intention)."<br />";
					if($activity->referral->price_1 || $activity->referral->price_2) {
						$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." <span class='price_con'>".(($activity->referral->price_1 == $activity->referral->price_2) ? format_currency_global($activity->referral->price_1,$activity->referral->symbol) : format_currency_global($activity->referral->price_1,$activity->referral->symbol)." - ".format_currency_global($activity->referral->price_2,$activity->referral->symbol))."</span>";
					}
					if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by == JFactory::getUser()->id){
						$question = "You have declined this referral";
					}else if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by != JFactory::getUser()->id){
						$question = stripslashes_all($activity->name)." has retracted this referral";
					}else if(is_object($activity->status->response) && $activity->status->response->status==7 && $activity->status->edited_by == JFactory::getUser()->id){
						$question = "You have countered this referral requesting ".$activity->status->response->value->referral_value." referral fee.";
					}else if(is_object($activity->status->response) && $activity->status->response->status==7 && $activity->status->edited_by == JFactory::getUser()->id){
						$question = "You have accepted this <a  href='".JRoute::_('index.php?option=com_propertylisting&task=agreement_ab')."&ref_id=".$activity->referral->referral_id."'>referral.</a>";
					}else{
						$question = JText::_('COM_ACTIVITY_REFDO');
						$buttons = array(
									array(
											'href'=>"javascript:void(0);",
											'label'=>JText::_('COM_ACTIVITY_BT_ACCEPT'),
											'class'=>'accept gradient-blue',
											'onclick'=>"acceptReferral('".$activity->activity_id."')",
											'spanclass'=>'accept-text'
									),
									array(
											'href'=>"javascript:void(0);",
											'label'=>JText::_('COM_ACTIVITY_BT_DEC'),
											'class'=>'decline gradient-gray',
											'onclick'=>"declineReferral('".$activity->activity_id."')",
											'spanclass'=>'decline-text'
									)
							);
					}
				}
				//referral agreement
				else if($activity->activity_type==14){
					$sigs = explode(',',$activity->referral->signatories);
					if($sigs && in_array(JFactory::getUser()->id, $sigs)){
						$clientname = stripslashes_all($activity->referral->client->name);
					}
					else{
						$clientname = format_name_global(stripslashes_all($activity->referral->client->name));
					}
					$mine =  ($activity->status->created_by == JFactory::getUser()->id) ? true : false;
					$hasquestion = true;
					$question = JText::_('COM_ACTIVITY_REFSIGN');
					$other_linked_name = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
					if($mine){
						$message = JText::sprintf('COM_ACTIVITY_REF_ACCEPT', $other_linked_name, stripslashes_all($activity->name), $activity->status->value->referral_value,$clientname);
						$imageleft = $activity->myimage;
					}
					else{	
						$clientname = stripslashes_all($activity->referral->client->name);
						$message = JText::sprintf('COM_ACTIVITY_REF_ACCEPTR1', $other_linked_name, stripslashes_all($activity->name), $activity->status->value->referral_value,$clientname);
					}
					$buttons = array(
							array(
									'href'=>JRoute::_('index.php?option=com_propertylisting&task=agreement_ab')."&ref_id=".$activity->referral->referral_id,
									'label'=>JText::_('COM_ACTIVITY_BT_SIGN'),
									'class'=>'accept gradient-blue',
									'onclick'=>"",
									'spanclass'=>'accept-text'
							)
					);
//print_r($activity->referral);
					if($sigs && in_array(JFactory::getUser()->id, $sigs)){
						$buttons=null;
						$question = JText::_('COM_ACTIVITY_REFSIGN3');
					}
				}
				//outgoing referral
				else if($activity->activity_type==15){
					$hasquestion = false;
					$r2 = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
					$message = JText::sprintf('COM_ACTIVITY_REFSENT', $r2, stripslashes_all($activity->name),$activity->status->value->referral_value);
					$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT').": ".stripslashes_all ($activity->referral->buyer->name)."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." ".JText::_($activity->referral->intention)."<br />";
					if($activity->referral->price_1 || $activity->referral->price_2) {
						$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." ".(($activity->referral->price_1 == $activity->referral->price_2) ? format_currency_global($activity->referral->price_1,$activity->referral->symbol) : format_currency_global($activity->referral->price_1,$activity->referral->symbol)." - ".format_currency_global($activity->referral->price_2,$activity->referral->symbol))." ".$activity->referral->currency;
					}
					if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by == JFactory::getUser()->id){
						$question = "You have retracted this referral.";
					}else if(is_object($activity->status->response) && $activity->status->response->status==7 && ($activity->status->value->referral_value!=$activity->status->response->value->referral_value)){
						$question = "You have countered this referral requesting ".$activity->status->response->value->referral_value;
					}else if(is_object($activity->status->response) && $activity->status->response->status==7){
						$question = "<a style='font-size:11px' href = '".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."'>".stripslashes_all($activity->name).'</a> has accepted this <a style="font-size:11px" href="'.JRoute::_('index.php?option=com_propertylisting&task=agreement_ab')."&ref_id=".$activity->referral->referral_id.'">referral.</a>';
					}else if(is_object($activity->status->response) && $activity->status->response->status==5 && $activity->status->edited_by != JFactory::getUser()->id){
						$question = stripslashes_all($activity->name)." has declined this referral.";
					}else {
						$question = JText::_('COM_ACTIVITY_REFDO');
						$buttons = array(
										array(
												'href'=>"javascript:void(0);",
												"onmouseover"=>"ddrivetip('You can cancel this referral prior to other agent accepting it.');hover_dd()",
												"onmouseout"=>"hideddrivetip();hover_dd()",
												'label'=>'Cancel',
												'class'=>'decline gradient-gray',
												'onclick'=>"retractReferral('".$activity->activity_id."')",
												'spanclass'=>'decline-text'
										)
									);
					}
				}
				//referral update 
				else if($activity->activity_type==16){
					$hasquestion = false;
					$r1 = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
					$refcon = JRoute::_('index.php?option=com_propertylisting&task=agreement_ab')."&ref_id=".$activity->activity_id;
					$message = JText::sprintf('COM_ACTIVITY_REFSIGN5', $r1, stripslashes_all($activity->name),$refcon);
				}
				//more referral updates
				else if($activity->activity_type==17 && $activity->status->status != 4){
					$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'\' class="text-link">'.stripslashes_all($activity->name).'</a>';
					$mine =  ($activity->status->created_by == JFactory::getUser()->id) ? true : false;
					$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT')." ".stripslashes_all ($activity->referral->buyer->name)."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." ".JText::_($activity->referral->intention)."<br />";
					if($activity->referral->price_1 || $activity->referral->price_2) {
						if($activity->status->status == 9)
							$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." ".(($activity->closedref['referral']->price_1 == $activity->closedref['referral']->price_2) ? format_currency_global($activity->closedref['referral']->price_1,$activity->closedref['referral']->symbol) : format_currency_global($activity->closedref['referral']->price_1,$activity->closedref['referral']->symbol)." - ".format_currency_global($activity->closedref['referral']->price_2,$activity->closedref['referral']->symbol))." ".$activity->closedref['referral']->currency;
						else 
							$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." ".(($activity->referral->price_1 == $activity->referral->price_2) ? format_currency_global($activity->referral->price_1,$activity->referral->symbol) : format_currency_global($activity->referral->price_1,$activity->referral->symbol)." - ".format_currency_global($activity->referral->price_2,$activity->referral->symbol))." ".$activity->referral->currency;
					}
					if($activity->status_update->note) {
						$reality_desc .= "<br/><strong>NOTE: ".$activity->status_update->note."</strong>";
					}
					if($mine){
						$imageleft = $activity->myimage;
					}
					else
						$imageleft = $activity->image;

					$class1 = "class='1text-link 1change' data-id='".$activity->referral->referral_id."' data='".$activity->referral->status."'";
					 if($notMobile) {
						//<a class="client_download_grid <?php echo $linkable> </a>
						$client_popup = "<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
					 } else {
						$client_popup = "<a href='".JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$activity->referral->client->buyer_id."' >".stripslashes_all($activity->referral->client->name)."</a>";
					 }

					if($activity->status->status == 6){
						if(!$mine){
							$message = JText::sprintf('COM_ACTIVITY_REFUPD6', $other_linked_name, $client_popup );
						}
						else{
							$message = JText::sprintf('COM_ACTIVITY_REFUPD3', $class1, $other_linked_name, $client_popup );
						}
					}
					else if($activity->status->status == 1){
						//$client_popup ="<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
						if(!$mine){
							$message = JText::sprintf('COM_ACTIVITY_REFUPD5', $other_linked_name, $client_popup );
						}
						else{
							$message = JText::sprintf('COM_ACTIVITY_REFUPD4', $client_popup );
						}
					}
					else if($activity->status->status == 8){
						//$client_popup ="<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
						if(!$mine){
							$message = JText::sprintf('COM_ACTIVITY_REFUPD8', $other_linked_name, $client_popup);
						}
						else{
							$message = JText::sprintf('COM_ACTIVITY_REFUPD7', $client_popup);
						}
					}
					else if($activity->status->status == 9){
						if(!$mine){
							//$message = $other_linked_name." has changed the referral status of <a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a> to <span class='1text-link changein'> Completed. </span>.";
							$message = JText::sprintf('COM_ACTIVITY_REFUPD9', $other_linked_name, json_encode($activity->referral->client), stripslashes_all($activity->referral->client->name));
						}
					}else if($activity->status->status == 4){
						$message = "closed mahn";
					}
					else{
						if($mine){
							$message = $other_linked_name." ";
						}
						else{
							$message = "You ";
						}
						//$message .= "changed the status of a referral to <span $class1>".$activity->status_label."</span>";
						$message .= JText::sprintf('COM_ACTIVITY_REF_CHANGE_STATUS', $class1, JText::_($activity->status_label));
					}
				}
				//closed referral
				else if($activity->activity_type==19){
					$mine =  ($activity->closed_referral_object['referral']->agent_b == JFactory::getUser()->id) ? true : false;
					if($mine){
						$imageleft = $activity->myimage;
					}
					else
						$imageleft = $activity->image;
						$reality_desc = "
									".JText::_('COM_REFERRAL_TERMS_CLIENT')." ".$activity->closed_referral_object['referral']->client->name."<br />
									".JText::_('COM_REFERRAL_TERMS_INT')." ".JText::_($activity->closed_referral_object['referral']->intention)."<br />";
					if($activity->closed_referral_object['referral']->price_1 || $activity->closed_referral_object['referral']->price_2) {
						$reality_desc .= "".JText::_('COM_REFERRAL_TERMS_PRICE')." ".(($activity->closed_referral_object['referral']->price_1 == $activity->closed_referral_object['referral']->price_2) ? format_currency_global($activity->closed_referral_object['referral']->price_1,$activity->closed_referral_object['referral']->symbol) : format_currency_global($activity->closed_referral_object['referral']->price_1,$activity->closed_referral_object['referral']->symbol)." - ".format_currency_global($activity->closed_referral_object['referral']->price_2,$activity->closed_referral_object['referral']->symbol))." ".$activity->closed_referral_object['referral']->currency;
					}
					$topay = ($activity->closed_referral_object['referral']->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
					if($notMobile) {
						//<a class="client_download_grid <?php echo $linkable> </a>
						$client_popup = "<a class='popupclient' data='".json_encode($activity->closed_referral_object['client'])."' href='javascript:void(0)'>".stripslashes_all($activity->closed_referral_object['client']->name)."</a>";
					 } else {
						$client_popup = "<a href='".JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$activity->closed_referral_object['client']->buyer_id."' >".stripslashes_all($activity->closed_referral_object['client']->name)."</a>";
					 } 
					 //<a id='refidtopost' data-item-id='".$activity->closed_referral_object['referral']->referral_id."'  class='popupclient' data='".json_encode($activity->closed_referral_object['client'])."' href='javascript:void(0)'>".$activity->closed_referral_object['client']->name."</a>

					if($mine) {
						$hasquestion = true;
						$message = "Congratulations. You have closed ".$client_popup."</a>.";
						if((trim($activity->closed_referral_object['referral']->agent_b)==JFactory::getUser()->id && ($activity->closed_referral_object['clrfobject']->r2_paid))){
							$question = "AgentBridge has successfully completed this transaction";
						}
					}else{
						$message = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id)."' class='text-link'>".stripslashes_all($activity->name)."</a> has closed referral, <a id='refidtopost' data-item-id='".$activity->closed_referral_object['referral']->referral_id."'  class='popupclient' data='".json_encode($activity->closed_referral_object['client'])."' href='javascript:void(0)'>".$activity->closed_referral_object['client']->name."</a>. Final numbers are being verified.";
						$hasquestion = true;
						if((trim($activity->closed_referral_object['referral']->agent_a)==JFactory::getUser()->id && ($activity->closed_referral_object['clrfobject']->r1_paid))){
							$question = "AgentBridge has successfully completed this transaction";
						}else{
							$question = "You haven't paid the service fee for the referral yet.";
							$buttons = array(
											array(
												'href'=>"javascript:void(0);",
												'label'=>'Please Pay',
												'class'=>'accept gradient-blue',
												'onclick'=>"prompt_for_pay(".$activity->closed_referral_object['clrfobject']->clrf_id.")",
												'spanclass'=>'accept-text'
											)
									);
						}
					}
				}
				//referral reminder
				else if($activity->activity_type==20){

					if($notMobile) {
						//<a class="client_download_grid <?php echo $linkable> </a>
						$client_popup = "<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
					 } else {
						$client_popup = "<a href='".JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$activity->referral->client->buyer_id."' >".stripslashes_all($activity->referral->client->name)."</a>";
					 }

					$ireferred = ($activity->referral->agent_a == JFactory::getUser()->id) ? true: false;
					$other_linked_name = "<a href='".JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id).'\' class="text-link">'.stripslashes_all($activity->otheruser_info->name).'</a>';
					$imageleft = $activity->myimage;
					$refpage = JRoute::_('index.php?option=com_propertylisting&task=referrals');
					$client_popup = "<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
					if($ireferred)
						$message = JText::sprintf('COM_ACTIVITY_REFUPD1', $refpage, stripslashes_all($activity->referral->client->name),$other_linked_name );
					else
						$message = JText::sprintf('COM_ACTIVITY_REFUPD2', $client_popup, $client_popup );
				}
				//referral update after signing
				else if($activity->activity_type==21){

					if($notMobile) {
						//<a class="client_download_grid <?php echo $linkable> </a>
						$client_popup = "<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
					 } else {
						$client_popup = "<a href='".JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$activity->referral->client->buyer_id."' >".stripslashes_all($activity->referral->client->name)."</a>";
					 } 

					#echo "<pre>", print_r($activity), "</pre>";
					$ireferred = ($activity->referral->agent_a == JFactory::getUser()->id) ? true: false;
					$other_linked_name = JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$activity->other_user_id);
					//$client_popup = json_encode($activity->referral->client);
					//$client_popup = "<a class='popupclient' data='".json_encode($activity->referral->client)."' href='javascript:void(0)'>".stripslashes_all($activity->referral->client->name)."</a>";
					$imageleft = $activity->myimage;
					$refpage = JRoute::_('index.php?option=com_propertylisting&task=referrals');
					if($ireferred)
						$message = JText::sprintf('COM_ACTIVITY_REFSIGN4', $client_popup, $client_popup, $other_linked_name, stripslashes_all($activity->otheruser_info->name));
					else
						$message = JText::sprintf('COM_ACTIVITY_REFSIGN6', $client_popup, $client_popup, $refpage, $client_popup );
				}
				//closed referral
				else if($activity->activity_type==22){
					$mine = ($activity->referral->agent_a==JFactory::getUser()->id);
					$imageleft = $activity->myimage;
					if($mine){
						$reffee = explode('%',$activity->referral->referral_fee);
						$ammount = $activity->closed_referral_object['clrfobject']->price_paid * (".".$reffee[0]);
						$ammount = format_currency_global($ammount);
						$message = "You have earned a referral fee of $ammount from ".stripslashes_all($activity->name)." for referral ".stripslashes_all($activity->referral->client->name).". Please <a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals')."'>change your referral status to Completed </a> to close this transaction.";
					}
					else{
						continue;
					}
				} else if($activity->activity_type==32){

					$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
					JModelLegacy::addIncludePath( $userprofilemodel );
					$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
					$date = time();
					$date_3 = strtotime("+4 day", $date);
					$user_pockets = $activity->user_pockets;
					if(!$user_pockets){
						//continue;
					}
					$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->activity_id);
					
					foreach ($user_pockets as $pocket) {
						$timestamp_pockets = strtotime($pocket->date_expired);		
						if($timestamp_pockets<=$date_3){
							$numDays = abs($timestamp_pockets - $date_3)/60/60/24;
							$flr_numdays=round($numDays);
							$acts_32 = $user_pockets;
							if(count($acts_32)<=0){
								$model2->insert_activity($user->id, 32, $pocket->listing_id, $user->id, date('Y-m-d H:i:s',time()));
							}				
							/*if( ($flr_numdays>=1 && $flr_numdays<2) || ($flr_numdays>=2 && $flr_numdays<3) || $flr_numdays==3){
								$message = "Your POPs™, <a href=".$listing_link.">$pocket->property_name</a>, will expire on $pocket->date_expired";
								$question = 'Would you like to extend this POPs™ for 30 days? <a id="yesPops" class="'.$activity->activity_id.' button accept gradient-blue yesPops" style="font-size:11px;cursor:pointer" ><span class="accept-text">Yes</span></a>| <span id="'.$activity->activity_id.'" class="'.$activity->pkId.' '.$activity->activity_id.' text-link noPops" style="font-size:11px" > No Thanks </span>';
							}*/
							$message = JText::sprintf('COM_ACTIVITY_POPSEXP2', $listing_link, $pocket->property_name, $pocket->date_expired);
							$sold_text = $pocket->property_type == 1 || $pocket->property_type == 3 ? JText::_('COM_POPS_TERMS_SOLD') : JText::_('COM_POPS_TERMS_LEASED'); 
							if($pocket->sold){
							  	$question = $pocket->property_type == 1 || $pocket->property_type == 3 ? JText::_('COM_POPS_TERMS_WILLEXP_SOLD') : JText::_('COM_POPS_TERMS_WILLEXP_LEASED'); 
							} else if($pocket->closed){
							  $question = JText::_('COM_POPS_TERMS_WILLEXP_EXPIRED');
							} else {
							  $question = JText::_('COM_ACTIVITY_POPSEXT2').' <span class="margtopsend"><a id="popsSold" class="'.$activity->activity_id.' '.$pocket->property_type.' button accept gradient-blue" style="font-size:11px;cursor:pointer" >'.$sold_text.'</a> | <a id="popsExpired" class="'.$activity->activity_id.' button accept gradient-blue" style="font-size:11px;cursor:pointer" >'.JText::_('COM_POPS_TERMS_EXPIRED').'</a>';
							}
							
						} 
					}
				}  else if($activity->activity_type==34){
					$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
					JModelLegacy::addIncludePath( $userprofilemodel );
					$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
					$date = time();
					$date_3 = strtotime("+4 day", $date);
					$user_pockets = $activity->user_pockets;
					if(!$user_pockets){
						continue;
					}
					$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->activity_id);
					foreach ($user_pockets as $pocket) {
						$timestamp_pockets = strtotime($pocket->date_expired);		
							$past_exp =  date('Y-m-d', strtotime($pocket->date_expired." -30 days") );
								$message = JText::sprintf('COM_ACTIVITY_POPSEXP', $listing_link, $pocket->property_name, $past_exp);
								$question = 'You have extended this POPs™ expiration date to '.$pocket->date_expired;
					}
				} else if($activity->activity_type==33){
					$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
					JModelLegacy::addIncludePath( $userprofilemodel );
					$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
					$date = time();
					$date_3 = strtotime("+4 day", $date);
					$user_pockets = $activity->user_pockets;
					if(!$user_pockets){
						continue;
					}
					$listing_link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $activity->activity_id);
					foreach ($user_pockets as $pocket) {
						$timestamp_pockets = strtotime($pocket->date_expired);		
						if($timestamp_pockets<=$date_3){
							$numDays = abs($timestamp_pockets - $date_3)/60/60/24;
							$flr_numdays=floor($numDays);
							$acts_32 = $user_pockets;		
							//if($flr_numdays==1 || $flr_numdays==2 || $flr_numdays==3){
								$message = JText::sprintf('COM_ACTIVITY_POPSEXP', $listing_link, $pocket->property_name, $pocket->date_expired);
								$question = JText::sprintf('COM_ACTIVITY_POPSEXTNO2', $pocket->date_expired);
							//}
						}
					}
				}
				else{
					continue;
				} 
			$ctr++;
			?> 
			<?php if( $question =="" && $message=="" && $activity->activity_type == 32){ 
				continue;
			}?>
		<div class="border-dot-act log_<?php echo $ctr; ?> <?php echo 'acttype_'.$activity->activity_type?>" style="display:none;">
		<?php if($imageleft!="") { ?>
			<div class="border-dot-act-left"><a class="other-user-act left" style="overflow: hidden"> <img src="<?php echo strpos($imageleft, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $imageleft).'?'.microtime(true) :  JURI::base().'/uploads/thumb_'.$imageleft.'?'.microtime(true)?>" width="60px"/></a> </div>
		<?php } else { ?>
			<div class="border-dot-act-left"><a class="other-user-act left" style="overflow: hidden"> <img class=""src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" width="60px"/></a></div>
		<?php } ?>
			<div class="border-dot-act-right"><p class="left"><?php echo $message; ?>
			<br/>
			<span class="calendar">
			<?php
					$new_date = new DateTime($activity->date, new DateTimeZone($this->timezone));
					$userTimeZone = new DateTimeZone($this->timezone);
					$offset = $userTimeZone->getOffset($new_date);
					$gmdate = gmdate($this->d_format, strtotime($activity->date)+intval($offset));
					echo "$gmdate ";
			?> </span>			
				
			</p>
			
			</div>
		
			<?php if( $activity->activity_type == 23 || $activity->activity_type == 24 || $activity->activity_type == 25 || $activity->activity_type == 26 || $activity->activity_type == 32 || $activity->activity_type == 34){ ?>
				<div class="realty-item-activity">
				<?php if($activity->activity_type != 32 && $activity->activity_type != 34){?>
					<span class="reality-description"><?php echo $buyer_desc;?></span>
				<?php } ?>
						<?php if($question != ""){ ?>
							<div class="clear-both"></div>
							<div class="pattern-div-activity clear pattern-div-activity-pl-notif">	
								<div class="left"><span class="question" id="question-<?php echo $activity->pkId?>"><?php echo $question;?></span></div>
							</div>
						<?php } ?>
							<div class="clear-both"></div>
				</div>
			<?php } else if( $activity->activity_type==27 && in_array($this->user_info->user_type, array(1,2,4)) ) { ?>
				<div class="realty-item-activity">
					<div class="clear-both"></div>
					<div class="pattern-div-activity clear pattern-div-activity-pl-notif">	
						<div class="left">
							<div class="question"><?php echo JText::_('COM_ACTIVITY_SENDINVQ') ?>
							<span class="margtopsend"><a class="button accept gradient-blue" href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile#invitesection'); ?>"><?php echo JText::_('COM_ACTIVITY_SENDINV') ?></a></span></div>
						</div>
						</div>
						<div class="clear-both"></div>
					</div>
					<?php } ?>
					<?php if($reality_desc!="" || $reality_item!="" || $hasquestion){?>
						<div class="realty-item-activity">
							<?php echo $reality_item; ?>
							<span class="reality-description"><?php echo $reality_desc;?></span>
							<div class="clear-both"></div>
							<?php if($hasquestion){?>
							<div class="pattern-div-activity clear">
								<div class="left">
								<span class="question" id="question-<?php echo $activity->activity_id?>"><?php echo $question;?></span>
								<div class="actbtn" id="buttons-<?php echo $activity->activity_id?>">
									<?php if(count($buttons)) {?>
										<?php foreach($buttons as $button){?>
										<div class="left margtop2">
											<a href="<?php echo $button['href']?>" class="button <?php echo $button['class']?>" onclick="<?php echo $button['onclick']?>" onmouseover="<?php echo $button['onmouseover']?>" onmouseout="<?php echo $button['onmouseout']?>">
												<span class="<?php echo $button['spanclass']?>"><?php echo $button['label'];?></span></a></div>
										<?php }?>
									<?php }?>
								</div>
								</div>
								<div class="right" style="display:none" id="loading-image-questions-<?php echo $activity->activity_id?>">
									<img height="26px" id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" alt="Loading..."class="right"/>
								</div>
							</div>
							<div class="clear-both"></div>
							<?php }?>
						</div>
					<?php }?>
					<div class="clear-both"></div>
				</div>
			<?php }?>
		</div>
					<ul id='paginator' >
				<?php if(isset($_GET['page']) && $_GET['page']>1){?>
					<li><a class="page_prev" href='/index.php/component/activitylog/?page=<?php echo $_GET['page']-1?>'><< Prev </a>|</li>
				<?php }?>				
				<?php for($i=1;$i<$number_of_pages;$i++){?>
				    <li><a class="page_number" href='/index.php/component/activitylog/?page=<?php echo $i?>'><?php echo $i?></a></li>
				<?php }?>
				<?php if(isset($_GET['page']) && $_GET['page']<($number_of_pages-1)){?>
					<li> |<a class="page_next" href='/index.php/component/activitylog/?page=<?php echo $_GET['page']+1?>'> Next>></a></li>
				<?php } else if($number_of_pages>1) {?>
					<li> |<a class="page_next" href='/index.php/component/activitylog/?page=2'> Next>></a></li>
				<?php }?>
			</ul>
<!-- end wide-content -->
	</div>
	<?php
		include( 'includes/sidebar_left.php' );
	?>
</div>
<div style="display:none;overflow: visible;" id="popupdetails">
	<div class="reftriangle"></div>
	<br/>
	<!-- Name: <span class="cc_name"></span><br/> -->
	Contact Number: <span class="cc_no"></span><br/>
	Email: <span class="cc_email"></span><br/>
	<span class="cc_note_label">Note: <span class="cc_note"></span><br/></span>
</div>
<div style="display: none" id="addbrokerlicense">
	To continue, please enter your brokerage license # <br /> <br /> <strong>Brokerage License #:</strong><br />
	<input 
		type="text" 
		id="bl_value_id" 
		name="bl_value"
		class="mc_select left" 
		required="required"
		style="height:30px;  width: 186px;"/> 
	<input 
		type="hidden" 
		id="otheruserid_val" 
		class="right"
		name="otheruserid"/> 
	<input 
		type="hidden" 
		id="activityid_val" 
		class="right"
		name="activityid"/> 
	<a 
		href="javascript:void(0)"
		class="button accept gradient-blue left" 
		style="width: 80px;height:30px; padding-top:5px "
		onclick="savedialogbroker()">Save</a> 
	<p class="broker error_msg" style="padding:0;margin:0px;display:none"> This field is required</p>
</div>
<div style="display: none" id="referralrequest">
	If you wish to make a counter offer, please enter the percentage below.<br /> <br /> <strong>Counter on referral fee:</strong><br />
	<input 
		type="text" 
		id="refvalue" 
		name="jform[referral_fee]"
		class="mc_select left" 
		style="height:30px"/> 
	<input 
		type="hidden" 
		id="referral_id" 
		class="right"
		name="referral_id"/> 
	<a 
		href="javascript:void(0)"
		class="button accept gradient-blue left" 
		style="width:150px; height:30px; padding-top:5px "
		onclick="counterReferral()">Send Counter Offer</a> 
	<a 
		href="javascript:void(0)" 
		class="text-link left"
		style="padding: 9px 15px;" 
		onclick="declineFully()"><span style="margin-top:5px">I am not interested</span></a>
</div>
<div style="display: none;" id="reason">
	<select width="150px" id="selectreason" style="display: none">
		<option value="1">Client unresponsive</option>
		<option value="2">Client working with another agent</option>
		<option value="3">Client’s needs changed</option>
		<option value="4">Retracted</option>
	</select> <input type="hidden" id="tochangeid" />
	<div class="clear-float"></div>
	<a href="javascript:void(0)" class="button gradient-green right" onclick="submitStatusChange()" style="margin-top: 10px">Submit</a>
	<div class="clear-float"></div>
</div>
<div id="authorizenet" style="display: none">
	<div class="clear-float"></div>   
      <!-- Saved Card -->
    <div class="popsaved" style="display:none">
    		<div class="radiobuttons">
				<br/>
                <div id="summarysaved">
                <h1>Summary of Transaction</h1>
				<p style="font-size:12px">
				<br/>The <span class="clientname"></span> referral fee of <span class="yourcomm" ></span> is ready to to be disbursed.
				AgentBridge will now be collecting the service fee of <span class="total">a</span>. Confirm or enter broker info and CC#, expiry date, and
				security code.<br /> <br />
				<span>Gross Commission of <span class="r2name"></span>:</span> <span id="rtwocomm" >a</span><br/>
                <span>Your Commission:</span> <span id="ronecomm" >a</span><br/>
				<span id="transaction">Service Fee:</span> <span class="amounttopayx amounttopay">a</span><br/></p></div><br/>
				<input  type="radio" name="usesaved" value="1" id="use" /> <label style="margin-right:20px; font-size:14px"  for="use">Use saved card information <span class="savedcard"></span></label>
				<input  type="radio" name="usesaved" value="0" id="nouse"/> <label style="font-size:14px" for="nouse ">Use another card</label>
			</div>
			<br/>
			<div class="usesavedsubmit">
				<div style="line-height:18px;">
					<span> Payment Types Accepted <br /><img src="images/payment.jpg" width="148" height="12"/></span><br />
					<input name="agree_terms_saved" type="checkbox" value="1" id="agree_terms_saved" style="margin-right:5px"/>
					<label for="agree_terms_saved"  style="font: 11px 'OpenSansRegular',Arial,sans-serif; width:150px"> 
					I have read and agree to the <span style="font: 11px 'OpenSansRegular',Arial,sans-serif;" class="text-link" onclick="popTerms()">Terms and Conditions</span></label><br />
                    <div style="display:none; margin-top:10px" class="agree_terms2 error_msg">You need to agree to our Terms before you proceed</div>
                    <div style="display:none; margin-top:10px" class="card_message2 error_msg">Some of the credit card details are now invalid. Please use a new card.</div>
					<a style="margin-top: 30px; height:30px; padding-top:5px" onclick="submitsavedPayment()" id="submit-usesaved" class="alwaysshow button gradient-green left" href="javascript:void(0)">Submit</a>
					<div class="left" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-submit-usesaved">
						<img height="26px" id="loading-image_custom_question"
							src="https://www.agentbridge.com/images/ajax_loader.gif"
							alt="Loading..."
							class="right"/>
					</div>
				</div>
			</div>
        </div>
    <!-- New Card -->
	<form id="summary" style="display: none">
		<div class="pop">
			<div class="authorizefields">
				<div class="clear-float">
					<div class="pcolumn1">
                    	<h1>Summary of Transaction</h1>
			<p style="font-size:12px">
				<br/>The <span class="clientname"></span> referral fee of <span class="yourcomm" ></span> is ready to to be disbursed.
				AgentBridge will now be collecting the service fee of <span class="total">a</span>.<br/><br/>
				Confirm or enter broker info and CC#, expiry date, and
				security code.<br /> <br />
				<span>Gross Commission of <span class="r2name"></span>:</span> <span id="rtwocomm" >a</span><br/>
                <span>Your Commission:</span> <span id="ronecomm" >a</span><br/>
				<span id="transaction">Service Fee:</span> <span class="amounttopayx amounttopay">a</span><br/>
			</p><br/>
						<h2>Agent and Brokerage Details</h2>
						<h3>Broker Information</h3>
						<input 
							type="text" 
							id="bslno"
							data-panel="panel1"
                            onMouseover="ddrivetip('This field is autopopulated. Please enter your Broker State License number <br/>if field is showing blank.');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" 
							class="left text-input ptext_input"
							placeholder="Brokerage License Number"/> <br /> 
						<input type="text" id="alslno"
							required="required" data-panel="panel1"
							onMouseover="ddrivetip('This field is autopopulated. Please enter your Agent License number <br/>if field is showing blank.');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="Agent License Number"/> <br /> 
						<input 					
							type="text" 
							id="btino"
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your Brokerage TAX ID number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="Brokerage Tax ID"/>
						<p class="btino error_msg" style="margin-left:0px;display:none"> This field is required  <br /> <br /></p>
					</div>
					<div class="pcolumn2">
						<h2 style=""> Cardholder Details</h2>
						<h3 style="">Personal</h3>
						<input 
							type="text" 
							id="firstname"
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter the first name that appears on your card');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input" 
							placeholder="Cardholder First Name"
                            style=""/> <br/> 
						<p class="firstname error_msg" style="display:none; "> This field is required  <br /> <br /></p>
						<input 
							type="text" 
							id="lastname"
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter the last name that appears on your card');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input" 
							placeholder="Cardholder Last Name"
                            style=""/> <br/> 
						<p class="lastname error_msg" style="display:none; "> This field is required  <br /> <br /></p>
						<input 
							type="text" 
							id="email"
							validator="0" 
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your email address');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input validateme"
							placeholder="Email"
                            style=""/> <br/> 
						<p class="email error_msg" style="display:none; "> This field is required  <br /> <br /></p>
						<label class="countCode" style="width:28px"></label>
						<input 
							type="text" 
							id="phone"
							validator="1" 
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your phone number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input numbermask"
							placeholder="Phone Number"
							style="width: 228px;"
                            style="width:260px; "/> <br />
						<p class="phone error_msg" style="display:none; "> This field is required  <br /> <br /></p>
						<h3 style="" >Billing Address</h3>
						<select 
							id="jform_country"
                            onMouseover="ddrivetip('Country');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
                            required="required"
							data-panel="panel2" class="required " 
                            style="width: 145px; "
							name="jform[country]"
							>
							<option value=""></option>
							<?php
							foreach($this->country_list as $country_list):
							if($country_list->countries_id == $this->user_info->country) $selected = "selected";
							else $selected = "";
							echo "<option data=\"".$country_list->countries_iso_code_2."\" value=\"".$country_list->countries_id."\" ".$selected.">".$country_list->countries_name."</option>";
							endforeach;
							?>
						</select>
                        <input 
							style="width:107px" 
							type="text" 
							id="zip"
                            onMouseover="ddrivetip('Enter your Zip');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" 
						    data-panel="panel2" 
							class="text-input sptext_input"
							placeholder="Zip"
                            /> <br/>
							<p class="zip error_msg" style="display:none; "> This field is required  <br /> <br /></p>
                        <input type="text" id="address1"
							data-panel="panel2" 
                            onMouseover="ddrivetip('Enter your street address');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input" 
                            placeholder="Address 1"
                            style=""/>  <br/>
                        <span><p class="address1 error_msg" style="display:none; "> This field is required  <br /> <br /></p></span> 
						<input 
							type="text" 
							id="address2"
							onMouseover="ddrivetip('This is optional');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							style=""
							placeholder="Address 2"/> <br/> 
                         <div class="clear-float"></div> 
						 <input 
							type="text" 
							id="city" 
							data-panel="panel2" 
                            onMouseover="ddrivetip('Enter your City');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="City"
                            style="margin-right:10px; width:258px; "/>
                        <span><p class="city error_msg" style="display:none; "> This field is required </p></span>
                        <br/>
                        <div class="clear-float"></div> 
						<input 
							type="text"
							id="state"
							data-panel="panel2" 
                            onMouseover="ddrivetip('Enter your State');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input " 
                            style=""
							placeholder="State"/> 
						<span><p class="state error_msg" style="display:none; "> This field is required  <br /> <br /></p></span>
                       </div>
					<div class="pcolumn3">
                    	<h2>Order Summary</h2>
						<h3>&nbsp;</h3>
						<div class="order_box">
							<div id="item_label" class="order_service">
								<strong>Service</strong> <br /> <br /> AgentBridge <br /> Referral
								Service <br /> Fee
							</div>
							<div class="order_qty">
								<strong>QTY</strong> <br /> <br /> 1
							</div>
							<div class="order_sub">
								<strong>Subtotal</strong> <br /> <br /> <span class="orig_total"></span>
							</div>
							<div id="discount_item_label" style="padding-top:10px;margin-left:10px;clear:both"></div>
						</div>
						<div class="order_service">
							<strong>TOTAL </strong>
						</div>
						<div class="order_qty">&nbsp;</div>
						<div class="order_sub total">$xx</div> <br/><br/>
						<h2>Card Details</h2><br/>
						<input 
							type="text" 
							id="card_number"
                            onMouseover="ddrivetip('Enter your credit card number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							data-panel="panel3"
							class="ptext_input text-input" 
							value="" 
							placeholder="Card Number (99999999999)"/> <br/> 
						<p class="card_number error_msg" style="margin-left:0px;display:none"> This field is required  <br /> <br /></p>
						<input 
							type="text" 
							id="card_expiry"
                            onMouseover="ddrivetip('Enter card expiry');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							data-panel="panel3"
							class="sptext_input text-input" 
							placeholder="MM-YYYY" 
							value="" 
							style="margin-right:10px; width:125px"/> 
						<input 
							type="text" 
							id="security_number"
                            onMouseover="ddrivetip('Enter the security number located at the back of your credit card');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							data-panel="panel3"
							class="sptext_input text-input" 
							placeholder="Security Number" 
							value="" 
							style="width:125px"
						/>
						<div style="float:left;width:135px;margin-top:-17px;"> &nbsp; <p class="card_expiry error_msg" style="display:none"> This field is required </p></div>
						<div style="float:left"><p class="security_number error_msg" style="display:none"> This field is required </p></div>
						<div style="clear:both"></div>
						<div style="line-height:18px;">
							<span> Payment Type Accepted<br /> <img src="images/payment.jpg"width="148" height="12"/></span> <br /><br />
							<input name="agree_terms" type="checkbox" value="1" id="agree_terms" style="margin-right:5px"/><label for="agree_terms_saved"  style="font: 11px 'OpenSansRegular',Arial,sans-serif;"> I agree to the <span style="font: 11px 'OpenSansRegular',Arial,sans-serif;" class="text-link" onclick="popTerms()">Terms and Conditions</span></label> <br />
							<input name="save_trans" type="checkbox" value="1" id="save_trans"  style="margin-right:5px;"/><label for="save_trans" style="font: 11px 'OpenSansRegular',Arial,sans-serif;"> Save
								payment details for future transactions </label> <br /><br />
									<div style="float:left"><p style="display:none" class="agree_terms error_msg"> You need to agree to our Terms before you proceed </p></div>
                                      <div style="float:left"><p style="display:none" id="card_message" class="error_msg"> Some of the credit card details are invalid. <br/>Please check again. </p></div>
						<div style="clear:both"></div>
	                        <a style="margin-top: 10px; width:120px; padding-top:5px; height:30px" onclick="submitPayment()" id="submit-nosaved" class="alwaysshow button gradient-green right" href="javascript:void(0)">Submit</a>
							<div class="left" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-submit-nosaved">
							<img height="26px" id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" alt="Loading..." class="right" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
<div id="termsandconditions" style="display:none">
	<span style="font-size:12px"><br />AgentBridge Terms and Conditions</span><br/>
	Don't worry, your card details are never stored in our database.
</div>
<div style="display: none" id="savebuyers">
	<div class="buyers-modal" style="width:700px">
		<div id="buyer-modal-container">
		<?php 
			$buyers_arr = array();
			$buyers_arr_a = array();
			$buyers_arr_b = array();
			$buyers_arr_c = array();
			foreach($this->buyers as $buyer){ array_push($buyers_arr, $buyer); }
			$toralrows = round(count($buyers_arr));
			$count_half = ceil($total_rows/2);
			$x = 1;
			foreach($buyers_arr as $buyer){
				if($x <= 3) {
					$buyers_arr_a[] = $buyer;
				} else if($x > 3 && $x <= 6) {
					$buyers_arr_b[] = $buyer;
				} else {
					$buyers_arr_c[] = $buyer;
				} 
				$x++;
			}
		?>
		<div class="left" id="buyer-m-tbl" border="1" style="width:100%">
                <div class="col1"></div>
				<div class="col2" style="text-align:center"><?php if(count($this->buyers_a)) { ?>A <?php } ?></div>
                <div class="col3" style="text-align:center"><?php if(count($this->buyers_b)) { ?>B <?php } ?></div>
                <div class="col4" style="text-align:center"><?php if(count($this->buyers_c)) { ?>C <?php } ?></div>
                <div style="clear:both;"></div>   
            	<div style="border-style:solid; border-bottom:1px solid #bfbfbf;">
				<div class="col1">Saved to Buyers:</div>
				<div class="col2">
    				<?php if(count($this->buyers_a)) { ?>
						<?php foreach($this->buyers_a as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
               <div class="col3">
    				<?php if(count($this->buyers_b)) { ?>
						<?php foreach($this->buyers_b as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
				<div class="col4">
    				<?php if(count($this->buyers_c)) { ?>
						<?php foreach($this->buyers_c as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
		    <div style="clear:both;"></div>		
            </div>
            <!--Available Buyers-->
            <div style="margin-top:10px">
            <div class="col1">Available Buyers:</div>
				<div class="col2">
    				<?php if(count($this->buyers_a)) { ?>
						<?php foreach($this->buyers_a as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
               <div class="col3">
    				<?php if(count($this->buyers_b)) { ?>
						<?php foreach($this->buyers_b as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
				<div class="col4">
    				<?php if(count($this->buyers_c)) { ?>
						<?php foreach($this->buyers_c as $buyer) { ?>
						<div>
                          <div style="float:left">
                        	<div id="buyer-m">
							<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="right" style=" margin-top:-2px; padding-bottom:10px; width:130px"><?php echo stripslashes_all ($buyer->name); ?></span></div>
							</div>
                          </div>
                        </div>
               		  <div style="clear:both;"></div>
					<?php } ?>
				 <?php } ?>
 			   </div>
		    <div style="clear:both;"></div>
            </div>		
			</div>
		</div>
		</div>
		<input type="hidden" name="property_id" id="property_id" value="<?php echo $_REQUEST['lID']; ?>" />
		<input type="hidden" name="agent_id" id="agent_id" value="<?php echo JFactory::getUser()->id; ?>" />
	 <div id="result-message"></div>
		<div id="savebuyers-button-container" style="margin-top:5px;">
			<div id="save-savebuyer">
				<a onclick="savebuyers()" class="button gradient-green right" style="width:120px" href="javascript:void(0);"> Save </a>
			</div>
            <div id="cancel-savebuyer" style="margin-top:7px">
				<a onclick="customClose()" href="javascript:void(0);">Cancel</a>
			</div>
		</div>
	</div>
</div>
<?php if(!isset($_SESSION['update_sales']) && (!$this->volume_2016 || !$this->sides_2016)) { ?>
<div class="overlay"></div>
<div class="overlay-container">
	<div class="form-container">
		<div class="formlogin">
			<a id="fancybox-close" style="display:block"></a>
			<div class="login-logo"></div>
			<form id="frm_usersales" style="width:100%; background-color:#ffffff; border-radius:10px; padding:0 10px 10px; margin:0px auto; color:#000000;position:relative" class="form-validate form-horizontal well">
				<div class="login-field"><input type="text" name="volume_2016" id="volume_2016" value="" class="validate-password required big-field" tabindex="0" placeholder="Add 2016 Sales Volume" /></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field"><input type="text" name="sides_2016" id="sides_2016" value="" class="validate-password required big-field" tabindex="0" placeholder="Add 2016 Sides" /></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field" style="padding-bottom: 20px">
					<button type="button" class="button submit-btn big-submit">Update Profile</button>
				</div>
				<input type="hidden" id="profile_id" name="profile_id" value="<?php echo $this->user_info->user_id; ?>"/>
				<div class="notnow">Not now. Update Later</div>
			</form>
			<div class="bottom-signup">&nbsp;</div>
		</div>
		<div id="frm_usersales_msg">
			<div class="login-logo"></div>
			<p style="margin:20px auto;text-align:center;">Thank you, your numbers have been submitted for verification. </p>
			<div class="dismiss">Continue to site</div>
		</div>
	</div>
</div>
<?php } ?>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.infinitescroll.js"/></script>
<script>
	function getInvitationCount(){
		jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_userprofile&task=get_invitation_list&format=raw",
			    type: "POST",
			    success: function(data){
			    	var obj = jQuery.parseJSON(data);
			    	var count_inv = 10-obj.length;	
			    	console.log(count_inv);		    	
			    	if(count_inv>0)
			    		jQuery(".counter").text(count_inv);
			    	else{
			    		var parent = jQuery(".counter").parent();
			    		parent.text("You have invited 10 licensed real estate professionals to join us on AgentBridge.");
			    		parent.next().find(".profile-btn").css("width","200px");
			    		parent.next().find(".create-referral").html("Check Invites &nbsp; &nbsp;");
			    		parent.next().find("#not-now").hide();
			    	}
			    }
		});
	}
	function showsalesdiv(){
		var volume2015 = jQuery( "#volume2015" ).val();
		var sides2015 = jQuery( "#sides2015" ).val();
		if (volume2015 == 0 || sides2015== 0) {
			jQuery("#addsalesdiv").show(); }
		else {
			showbuyerdiv ();
		}
	}
	function hidesalesdiv(){
		jQuery("#addsalesdiv").remove();
		showbuyerdiv ();
	}
	function showbuyerdiv(){
		var buyercount = jQuery( "#countbuyer" ).val();
		if (buyercount == 0)
		jQuery("#addbuyerdiv").show();		
	}
	function hidebuyerdiv(){
		var utype = jQuery( "#usertype" ).val();
		jQuery("#addbuyerdiv").remove();
		jQuery( "#addbuyer" ).show();
		if (utype == 1 || utype == 4 )
		 jQuery("#invitediv").show();	
	}
	function hideinvitediv(){
		jQuery("#invite-button" ).show();
		jQuery("#invitediv").remove();	
	}
	function removeBuyerButton(){
		var buyercount = jQuery( "#countbuyer" ).val();
		var utype = jQuery( "#usertype" ).val();
		if (buyercount >= 1 && utype==3)
			jQuery("#addbuyer").show();
		if (buyercount >= 1 && utype!=3) {
			jQuery( "#addbuyer" ).show();
			jQuery("#invitediv").show();
		}
	}
	var usesaved=true;
	jQuery(document).ready(function(){

		jQuery("#popsSold").live("click", function(){
				var prop_type=jQuery(this).attr("class").split(" ")[1];
				var listing_id=jQuery(this).attr("class").split(" ")[0];
				var parent_elem = jQuery(this);
				//parent_elem.parent().parent().html("<span style='padding: 2px 10px 3px;'>You have marked this POPs as <?php echo JText::_('COM_POPS_TERMS_SOLD')?></span>");
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=soldPOPs&format=raw') ?>",
					data: {'listing_id': listing_id},
					success: function(data){
						if(prop_type==1 || prop_type==3){
							//parent_elem.parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_SOLD')?></span>");
							parent_elem.parent().parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_WILLEXP_SOLD')?></span>");						
						} else {
							//parent_elem.parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_LEASED')?></span>");
							parent_elem.parent().parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_WILLEXP_LEASED')?></span>");
						}
					}
				});
			});

		jQuery("#popsExpired").live("click", function(){
				var listing_id=jQuery(this).attr("class").split(" ")[0];
				var parent_elem = jQuery(this);
				//parent_elem.parent().parent().html("<span style='padding: 2px 10px 3px;'>This POPs is now <?php echo JText::_('COM_POPS_TERMS_EXPIRED')?></span>");
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=expiredPOPs&format=raw') ?>",
					data: {'listing_id': listing_id},
					success: function(data){
						parent_elem.parent().parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_WILLEXP_EXPIRED')?></span>");
						//parent_elem.parent().html("<span style='padding: 2px 10px 3px;'><?php echo JText::_('COM_POPS_TERMS_EXPIRED')?></span>");
					}
				});
			});


	getInvitationCount();
	showsalesdiv ();
	jQuery("#countbuyer").trigger("click");
	//jQuery("#addbuyerdiv").trigger("click");
		jQuery ("#summarysaved").show();
		var main_count = 0;
		var vctr = 0;
		jQuery(".log_"+vctr).show();
		while (main_count<15){
			main_count++;
			vctr++;
			jQuery(".log_"+vctr).show();
		}
		jQuery(window).scroll(function() {
			if (vctr != <?php echo $ctr; ?>) {
			   if(jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height()-100) {
					vctr++;
					jQuery(".main .main-content").css('height', 'auto');
					jQuery(".log_"+vctr).show();
			   }
			}
		});
		jQuery("input[name='usesaved']").click(function(){
			if(!(jQuery(this).val()>0)){
				usesaved=false;
				jQuery(".authorizefields").show();
				jQuery(".usesavedsubmit").hide();
				jQuery("#summarysaved").hide();
			}
			else{
				usesaved=true;
				jQuery(".authorizefields").hide();
				jQuery(".usesavedsubmit").show();
				jQuery(".popsaved").show();
				jQuery("#summarysaved").show();
			}
		});
		jQuery("#rtwocomm").autoNumeric('init', {mDec: '0'});   
		jQuery(".yesPops").click(function(){
			var parent =jQuery(this).parent().parent();
			var id = jQuery(this).attr("class");
			var id = id.split(" ");
			var id = id[0];
			jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_activitylog&task=extend_expPops&format=raw",
			    type: "POST",
			    data:{listing_id:id},
			    success: function(data){
			    	parent.text("You have extended your POPs™ expiration date to"+data+".");
			    }
			});
		});
		jQuery(".noPops").click(function(){
			var parent =jQuery(this).parent().parent();
			var id = jQuery(this).attr("class");
			var id = id.split(" ");
			var id = id[0];
			var listing_id = jQuery(this).attr("id");
			jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_activitylog&task=noExtend_expPops&format=raw",
			    type: "POST",
			    data:{pkId:id,listing_id:listing_id},
			    success: function(data){
			    	parent.text("This POPs™ will expire on "+data+".");
			    }
			});
		});
	});
	function saveSingleBuyer(aida, pida, buyer_arr, button_id){
		console.log(button_id);
		jQuery.ajax({        
			type: 'POST',
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=savepops'); ?>',
			data: {
				aida : aida, 
				pida : pida,
				buyer_arr: buyer_arr, 
				buyer_arr_dis: ''
			},
			beforeSend: function () {
	            jQuery("#loading").show();
	        },				
			success: function(data) {
				console.log(button_id);
				jQuery('#'+button_id).hide();
				jQuery("."+button_id).show();
				jQuery("#loading").hide();
			}
		});
	}
	function requestAccess(uid, pid, bid, aid){
		jQuery(".ra"+aid).hide();
		jQuery("#loading"+aid).show();
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=requestaccess') ?>',
			data: {'pid': pid, 'uid': uid},
			success: function(data){
				var hid = 1;
				jQuery.post('<?php echo htmlspecialchars_decode(JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=')); ?>'+bid, { hidden: hid}, function(data) {
				jQuery("#question-"+aid).html("<?php echo JText::_('COM_ACTIVITY_REQSENT')." <div class='act-req-pending'>".JText::_('COM_NETWORK_TERMS_PEND')."</div>"; ?>");
				});				
			}
		});
	}
	function requestNetwork(other_user_id, aid){
		jQuery(".ra"+aid).hide();
		jQuery.ajax({
			type: "GET",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=request_network'); ?>&id='+other_user_id,
			success: function(data){
				var hid = 1;
				jQuery("#question-"+aid).html("<?php echo JText::_('COM_ACTIVITY_REQSENT') ?> <div class='act-req-pending'><?php echo JText::_('COM_NETWORK_TERMS_PEND')?></div>");
			}				
		});
	}
	function requestNetworkPostAccept(other_user_id){
		jQuery(".ra").hide();
		jQuery.ajax({
			type: "GET",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=request_network'); ?>&id='+other_user_id,
			success: function(data){
				var hid = 1;
				jQuery("#questioninvite").hide();
				jQuery("#questionaccept").show();
			}				
		});
	}
	function popTerms(){
		jQuery("#termsandconditions").dialog(
			{
				modal:true, 
				width: 500,
				title: "Terms and Conditions",
				});
	}
	function popTermsb(listing_id){
		jQuery("#property_id").val(listing_id);
		jQuery("#savebuyers").dialog(
			{
				modal:true, 
				width: 'auto', 
				maxWidth: 1500, 
				minWidth: 500,
				title: "Save POPs&trade; to Buyers",
			}
		);
	}
	function savebuyers(){
		var pid = document.getElementById('property_id').value;
		var aid = document.getElementById('agent_id').value;
		var arr = [];
		var notchecked = [];
		var i = 0;
		jQuery('.savetobuyercheckbox:checked').each(function(){
			 arr[i++] = jQuery(this).val();
		});
		jQuery('.savetobuyercheckbox:not(:checked)').each(function(){
			 notchecked[i++] = jQuery(this).val();
		});
		jQuery.ajax({        
			type: "POST",
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=savepops'); ?>",
			data: {aida : ""+aid, pida : ""+pid, buyer_arr: ""+arr, buyer_arr_dis: ""+notchecked},
			success: function(data) {
				jQuery("#savebuyers").dialog('close');
				jQuery("#save-savebuyer a").attr('onclick', '');
				jQuery("#save-savebuyer a").removeClass('button');
				jQuery("#save-savebuyer a").toggleClass('button-save');
				jQuery('.savetobuyercheckbox').prop({disabled: true});
				jQuery("#save-savebuyer a").css('cursor', 'default');
				jQuery("#save-savebuyer a").toggleClass('gray');
				location = "<?php echo JRoute::_('index.php?option=com_activitylog'); ?>";
		   }
		});
	}
	function customClose(){
		jQuery("#savebuyers").dialog('close');
		jQuery("#save-savebuyer a").attr("onclick", 'savebuyers();');
		jQuery("#save-savebuyer a").removeClass('button-save');
		jQuery("#save-savebuyer a").toggleClass('button');
		jQuery('.savetobuyercheckbox').prop({disabled: false});
		jQuery("#save-savebuyer a").css('cursor', 'pointer');
		jQuery("#save-savebuyer a").toggleClass('gray');
	}
	jQuery("#viewmore").click(function() {
		jQuery(".activity-list").append("afsdfasdf");
	});
	jQuery('.activity-list').infinitescroll({
	    navSelector  : "#paginator",            
	    nextSelector : "#paginator a.page_next",    
	    itemSelector : ".activity-list" ,         
	    loading: {
	    	img   : "https://www.agentbridge.com/images/ajax_loader.gif",                
  			msgText  : "",      
	     }        
	  });

<?php if(!isset($_SESSION['update_sales']) && (!$this->volume_2016 || !$this->sides_2016)) { ?>
	jQuery(document).ready(function(){
		jQuery("#volume_2016").autoNumeric('init', {aSign:'<?php echo $this->user_info->symbol; ?>', mDec: '0'});
		jQuery("#volume_2016").val("");
		jQuery("#volume_2016").autoNumeric('update', {aSign:'<?php echo $this->user_info->symbol; ?>', mDec: '0'});
		
		removethis=" <?php echo $this->user_info->currency; ?>";
		addthis=" <?php echo $this->user_info->currency; ?>";
		
		jQuery("#volume_2016").blur(function(){
			if(jQuery(this).val().indexOf(addthis)==-1){
				console.log(addthis);
				jQuery(this).val(jQuery(this).val()+" "+addthis);
			}
		});
	
	
		
		jQuery("#sides_2016").autoNumeric('init', {mDec: '0', vMax: '9999'}); 
			jQuery(".overlay").css("display", "table");
			jQuery(".overlay-container").css("display", "table");
		
		jQuery('.formlogin button[type="button"]').live("click", function(){
			jQuery(".jform_field1.error_msg").hide();
			jQuery(".jform_field2.error_msg.error1").hide();
			jQuery("#username").removeClass("glow-required");

			var error=0;

			if(!jQuery("#volume_2016").val()){
				jQuery("#volume_2016").addClass("glow-required");
				jQuery(".jform_field1.error_msg.error1").show();
				error++;
			}

			if(!jQuery("#sides_2016").val()){
				jQuery("#sides_2016").addClass("glow-required");
				jQuery(".jform_field2.error_msg.error1").show();
				error++;
			}

			if(error>0){
				return false;
			} else {
				jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_nrds&task=actusersales')?>",
					type: 'POST',
					data: 	{ 
						'volume_2016'	: jQuery("#volume_2016").val(),
						'sides_2016'	: jQuery("#sides_2016").val(),
						'profile_id'	: jQuery("#profile_id").val()
					},
					success: function(response){
						jQuery(".formlogin").css("display", "none");
						jQuery("#frm_usersales_msg").css("display", "table");
					}
				});
			}
		});

		jQuery("#fancybox-close, .notnow").live("click",function(){
			jQuery.ajax({
				url: "<?php echo JRoute::_('index.php?option=com_nrds&task=setUpdateSalesSession')?>"
			});
			jQuery(".overlay").hide();
			jQuery(".overlay-container").hide();
		});
		
		jQuery(".dismiss").live("click", function(){
			jQuery("#frm_usersales_msg").css("display", "none");
			jQuery(".overlay").hide();
			jQuery(".overlay-container").hide();
		});
	});
<?php } ?>
</script>
<style>
	.notnow {
		cursor:pointer;
		font-size:16px;
		color:#007bae;
		font-weight:normal;
		text-decoration:underline;
		margin-left:40px;
	}
	
	
	.overlay {
		width:100%;
		height:100%;
		z-index:2000;
		opacity: 0.4;
		filter: alpha(opacity=40); /* msie */
		background-color: #000;
		position:fixed;
		top:0;
		left:0;
		display:none;
	}
	
	.overlay-container {
		display:none;
		position:fixed;
		width:100%;
		height:100%;
		z-index:2001;
		top:0;
	}
	
	.form-container {
		display:table-cell;
		vertical-align:middle;
	}
	
	.formlogin {
		margin: 0 auto;
		display:table;
		width:500px;
		height:370px;
		position:relative;
		background:#fff;
		border-radius:10px;
		box-sizing:border-box;
	}
	
	
	
	.login-logo {
		background:url("/templates/agentbridge/images/main-logo.png") no-repeat;
		width:208px;
		height:49px;
		display:table;
		margin:10px auto 20px;
	}
	
	.login-field {
		width:400px;
		display:table;
		margin:10px auto;
	}
	
	.big-field {
		width:100%;
		height:40px;
		border: 2px solid #ccc;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	.big-submit {
		width:100%;
		height:40px;
		font-size:16px;
		color:#fff;
		background:#339900;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	#remember-me-big {
		border:2px solid #ccc;
		width:15px;
		height:15px;
	}
	
	.chklabel {
		margin:0 0 0 5px;
		font-size:16px;
	}
	
	.bottom-signup {
		position:absolute;
		border-top:1px solid #ccc;
		text-align:right;
		padding:15px;
		font-size:16px;
		box-sizing:border-box;
		width:100%;
		bottom:0;
	}
	
	.bottom-signup a {
		font-size:16px;
	}
	
	.formlogin p {
		font-size:12px;width:400px;margin:0 auto;font-weight:normal!important;
	}
	
	.login-field a {
		color:#007bae;font-size:16px;
	}
	
	#frm_usersales_msg {
		display:none;
		margin: 0 auto;
		display:none;
		width:500px;
		height:150px;
		position:relative;
		background:#fff;
		border-radius:10px;
		box-sizing:border-box;
	}
	
	#frm_usersales_msg .dismiss {
		cursor:pointer;
		display:table;
		color:#007bae;
		font-weight:normal;
		text-align:center;
		position:relative;
		margin:20px auto;
	}
	
	@media screen and (max-width: 500px) {
		#frm_usersales_msg {
			width:300px;
		}
		
		.formlogin {
			margin: 0 auto;
			display:table;
			width:300px;
			height:330px;
			position:relative;
			background:#fff;
			border-radius:10px;
			box-sizing:border-box;
		}
		
		.login-logo {
			background:url("/templates/agentbridge/images/main-logo.png") no-repeat;
			width:208px;
			height:49px;
			display:table;
			margin:0 auto 20px;
		}
		
		.login-field {
			width:250px;
			display:table;
			margin:10px auto;
		}
		
		.big-field {
			width:100%;
			height:30px;
			border: 2px solid #ccc;
			border-radius:5px;
			padding:10px;
			box-sizing:border-box;
		}
		
		.big-submit {
			width:100%;
			height:30px;
			font-size:16px;
			color:#fff;
			background:#339900;
			border-radius:5px;
			padding:5px;
			box-sizing:border-box;
		}
		
		#remember-me-big {
			border:2px solid #ccc;
			width:15px;
			height:15px;
		}
		
		.chklabel {
			margin:0 0 0 5px;
			font-size:14px;
		}
		
		.bottom-signup {
			position:absolute;
			border-top:1px solid #ccc;
			text-align:right;
			padding:15px;
			font-size:14px;
			box-sizing:border-box;
			width:100%;
			bottom:0;
		}
		
		.bottom-signup a {
			font-size:14px;  
		}
		
		.formlogin p {
			width:250px;
			font-size:12px;
			font-weight:normal!important;
		}
		
		.login-field a {
			color:#007bae;font-size:14px;
		}
	}
</style>
