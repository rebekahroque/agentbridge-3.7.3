<?php
/**
 * @version     1.0.0
 * @package     com_activitylog
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * Activitylog model.
*/


class ActivitylogModelActivitylog extends JModelForm
{

	var $_item = null;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	 

	 function dbcall_ExpPOPs($activity_id){

	 	$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*')

		->from('#__pocket_listing pocket')

		->where('listing_id = '.$activity_id. " AND closed!=1");



		$user_pockets = $db->setQuery($query)->loadObjectList();

		return $user_pockets;

	 }

	function outclosed_count( $user_id ) {
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__referral')->where(
			"
				status = 4 AND (agent_b = $user_id OR agent_a = $user_id)
			"
		);
		

		
		// echo $query
		// die();
		
		$db->setQuery($query);
		$refin_count = $db->loadObject()->count;
		
	    return $refin_count;
	}	 
	
	function check_existing_pops( $listing_id, $buyer_id, $active ) {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__buyer_saved_listing')->where(
			"
			buyer_id = ".$buyer_id."
			AND agent_id = ".JFactory::getUser()->id."
			AND listing_id = ".$listing_id."
			AND active = ".$active 
			
		);
		$db->setQuery($query);
		$pops_count = $db->loadObject()->count;
	
		return $pops_count;
	}
	 
	 
	protected function populateState()
	{
		$app = JFactory::getApplication('com_activitylog');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit') {
			$id = JFactory::getApplication()->getUserState('com_activitylog.edit.activitylog.id');
		} else {
			$id = JFactory::getApplication()->input->get('id');
			JFactory::getApplication()->setUserState('com_activitylog.edit.activitylog.id', $id);
		}
		$this->setState('activitylog.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$params_array = $params->toArray();
		if(isset($params_array['item_id'])){
			$this->setState('activitylog.id', $params_array['item_id']);
		}
		$this->setState('params', $params);

	}


	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id)) {
				$id = $this->getState('activitylog.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}

	public function getTable($type = 'Activitylog', $prefix = 'ActivitylogTable', $config = array())
	{
		$this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
		return JTable::getInstance($type, $prefix, $config);
	}


	/**
	 * Method to check in an item.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int)$this->getState('activitylog.id');

		if ($id) {

			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin')) {
				if (!$table->checkin($id)) {
					$this->setError($table->getError());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int)$this->getState('activitylog.id');

		if ($id) {

			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout')) {
				if (!$table->checkout($user->get('id'), $id)) {
					$this->setError($table->getError());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_activitylog.activitylog', 'activitylog', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		$data = $this->getData();

		return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function save($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('activitylog.id');
		$state = (!empty($data['state'])) ? 1 : 0;
		$user = JFactory::getUser();

		if($id) {
			//Check the user can edit this item
			$authorised = $user->authorise('core.edit', 'com_activitylog') || $authorised = $user->authorise('core.edit.own', 'com_activitylog');
			if($user->authorise('core.edit.state', 'com_activitylog') !== true && $state == 1){ //The user cannot edit the state of the item.
				$data['state'] = 0;
			}
		} else {
			//Check the user can create new items in this section
			$authorised = $user->authorise('core.create', 'com_activitylog');
			if($user->authorise('core.edit.state', 'com_activitylog') !== true && $state == 1){ //The user cannot edit the state of the item.
				$data['state'] = 0;
			}
		}

		if ($authorised !== true) {
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

		$table = $this->getTable();
		if ($table->save($data) === true) {
			return $id;
		} else {
			return false;
		}

	}

	function delete($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('activitylog.id');
		if(JFactory::getUser()->authorise('core.delete', 'com_activitylog') !== true){
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}
		$table = $this->getTable();
		if ($table->delete($data['id']) === true) {
			return $id;
		} else {
			return false;
		}

		return true;
	}

	function getCategoryName($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select('title')
		->from('#__categories')
		->where('id = ' . $id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	public function updateVisit($user){
		$db = JFactory::getDbo();
		$object = new JObject();
		$object->id = JFactory::getUser()->id;
		$object->lastActivityLogVisit = date("Y-m-d H:i:s");
		$db->updateObject('#__users', $object, 'id');
	}

	
	public function getActivities($offset = 0, $limit = 10){
		$userprofile2model = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofile2model );

		$model_user =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$modelPropertyListing =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$ex_rates = $modelPropertyListing->getExchangeRates_indi();

        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;

		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query
		->select('at.*, at.pkId as actid, aat.label, other.name, images.image, images2.image as myimage')
		->from('#__activities at 
			STRAIGHT_JOIN #__users other on other.id = at.other_user_id 
			STRAIGHT_JOIN #__user_registration images on images.email = other.email 
			STRAIGHT_JOIN #__user_registration images2 on images2.email = "'.$user->email.'" 
			STRAIGHT_JOIN #__activity_type aat on aat.pkId = at.activity_type ')
		//->leftJoin('')
		//->leftJoin('')
		//->leftJoin('')
		//->leftJoin('')
		->order('date DESC')
		->where('aat.pkId != 5 AND at.user_id = ' . $user->id);

		if(!$limit) {
			$db->setQuery($query);	
		} else {
			$db->setQuery($query, $offset, $limit);

		} 
		#$db->setQuery($query);
		$objects = $db->loadObjectList();
		
		$user_info = $model_user->get_user_registration($user->id);

		#echo "<pre>".$user->id; print_r($objects); die();
		foreach ($objects as $key => $object){
			$objects[$key]->user_network = $this->get_network($object->other_user_id);
			$objects[$key]->user_network_raw = $this->get_network_raw($object->other_user_id);
			$query = $db->getQuery(true);
		#	if($object->activity_type == 1 || $object->activity_type == 2 || $object->activity_type == 4 || $object->activity_type == 6){
			if($object->activity_type == 1 || $object->activity_type == 2 || $object->activity_type == 4){
				$query
				->select("pt_price.*,con.countries_iso_code_2,pty.country,pty.currency,pty.listing_id as property_id, pty.property_name, pta.address, pcti.image, pty.description as property_desc, pty.zip as zip, pt.type_name as property_type, pst.name as sub_type")
				->from('#__pocket_listing pty')
				->leftJoin('(select group_concat(address) as address, pocket_id from #__pocket_address group by pocket_id) pta on pta.pocket_id = pty.listing_id')
				->leftJoin('#__pocket_images pcti on pcti.listing_id = pty.listing_id')
				->leftJoin('#__property_type pt ON pty.property_type = pt.type_id')
				->leftJoin('#__property_sub_type pst ON pty.sub_type = pst.sub_id')
				->leftJoin('#__property_price pt_price ON pt_price.pocket_id = pty.listing_id')
				->leftJoin('#__countries con ON con.countries_id = pty.country')
			//	->leftJoin('#__country_currency currcon ON con.countries_id = currcon.country')
				#->leftJoin('#__request_access perms on perms.pkId='.$object->activity_id)
				->where('pty.listing_id = '.$object->activity_id);
				$db->setQuery($query);
				$values = $db->loadObject();
				#echo "<pre>"; print_r($values); die();
				$object->address = $values->address;
				$object->zip = $values->zip;
				$object->price1 = $values->price1;
				$object->price2 = $values->price2;
				$object->countries_iso_code_2 = $values->countries_iso_code_2;
				$object->country = $values->country;
				
				$object->currency = $values->currency;

				#$object->permission = $values->permission;
				//$object->propertyImage = $values->image;
				$object->property_desc = $values->property_desc;
				
				$property_type = strtolower(str_replace(" ", "-", $values->property_type));
				$sub_type = strtolower($values->sub_type);
				switch($sub_type) {
					case "motel/hotel":
						$sub_type = "motel";
						break;
					case "townhouse/row house":
						$sub_type = "townhouse";
						break;
					case "assisted care":
						$sub_type = "assist";
						break;
					case "special purpose":
						$sub_type = "special";
						break;
					case "multi family":
						$sub_type = "multi-family";
						break;
				}
				
				$img_filename = $property_type . "-" . $sub_type . ".jpg";
				
				$object->propertyImage = ($values->image) ? $values->image : "images/buyers/56x46/" . $img_filename;
				$object->property_id = $values->property_id;
				$object->property_name = $values->property_name;
				

			}
			else if($object->activity_type == 5){
				$query
				->select("property_id")
				->from('#__views')
				->where('pkId = '.$object->activity_id);
				$db->setQuery($query);
				$id = $db->loadObject();
				$id = $id->property_id;
				#echo $id; die();
				$query = $db->getQuery(true);
				$query
				->select("pta.address, pcti.image, pty.description as property_desc, pty.zip as zip, perms.permission as permission")
				->from('#__pocket_listing pty')
				->leftJoin('(select group_concat(address) as address, pocket_id from #__pocket_address group by pocket_id) pta on pta.pocket_id = pty.listing_id')
				->leftJoin('#__pocket_images pcti on pcti.listing_id = pty.listing_id')
				->leftJoin('#__request_access perms on perms.pkId='.$object->activity_id)
				->where('pty.listing_id = '.$id);
				$db->setQuery($query);
				$values = $db->loadObject();
				$object->address = $values->address;
				$object->zip = $values->zip;
				$object->permission = $values->permission;
				$object->propertyImage = $values->image;
				$object->property_desc = $values->property_desc;
				$object->property_id = $id;
			}
			else if($object->activity_type == 6 || $object->activity_type == 7){
				#echo "pkid = ". $object->activity_id; die();
				$query
				->select("property_id")
				->from('#__request_access')
				->where('pkId = '.$object->activity_id);
				$db->setQuery($query);
				$id = $db->loadObject();
				$id = $id->property_id;

				$query = $db->getQuery(true);
				$query
				->select("pta.address, pcti.image, pty.description as property_desc, pty.zip as zip, perms.permission as permission, pty.property_type, pty.sub_type")
				->from('#__pocket_listing pty')
				->leftJoin('(select group_concat(address) as address, pocket_id from #__pocket_address group by pocket_id) pta on pta.pocket_id = pty.listing_id')
				->leftJoin('#__pocket_images pcti on pcti.listing_id = pty.listing_id')
				->leftJoin('#__request_access perms on perms.pkId='.$object->activity_id)
				->where('pty.listing_id = '.$id);
				$db->setQuery($query);
				$values = $db->loadObject();
				$address = $values->address;
				$object->address = $address;
				$object->zip = $values->zip;
				$object->permission = $values->permission;
				$images = explode(",", $values->image);
				
				if(!$images[0]) {
					$object->propertyImage = "images/buyers/56x46/" . $this->get_buyer_image($values->property_type, $values->sub_type);
					#echo "<pre>", print_r($object->listing[0]), "</pre>";
				} else {
					$object->propertyImage = $images[0];
				}
				
				
				#$object->propertyImage = $values->image;
				$object->property_desc = $values->property_desc;
				$object->property_id = $id;
			}
			else if($object->activity_type == 3){
				$query
				->select("message")
				->from('#__edit_activity')
				->where('pkId = '.$object->activity_id);
				$db->setQuery($query);
				$message = $db->loadObject();
				$object->message = $message->message;
			}
			else if($object->activity_type == 8 || $object->activity_type == 30 || $object->activity_type == 30 ){
				$query
				->select("status")
				->from('#__request_network')
				->where('pk_id = '.$object->activity_id);
				$db->setQuery($query);
				$status = $db->loadObject();
				$object->status = $status->status;
			}
			else if($object->activity_type == 28){
				$query
				->select("status")
				->from('#__request_network')
				->where('pk_id = '.$object->activity_id);
				$db->setQuery($query);
				$status = $db->loadObject();
				$object->status = $status->status;
			}

			else if($object->activity_type == 32 || $object->activity_type == 33 || $object->activity_type == 34 ){
				$user_pockets=$this->dbcall_ExpPOPs($object->activity_id);
				$object->user_pockets = $user_pockets;
			}

			/*else if($object->activity_type == 11){
				$query
			->select("*")
			->from('#__referral')
			->where('referral_id = '.$object->activity_id);
			$db->setQuery($query);
			$object->referral = $db->loadObject();
			}*/
			else if($object->activity_type == 11 || $object->activity_type == 17 || $object->activity_type == 14 || $object->activity_type == 15 || $object->activity_type == 13){
				$value =  $this->get_referral_status_object($object->activity_id);
				if($value->response){
					$value->response = $this->get_referral_status_object($value->response);
					if($value->response->response && ($value->referral_id != $value->response->referral_id) && !is_object($value->response)){
						$value->response = $this->get_referral_status_object($value->response);
					}

				}
				$object->status = $value;
				if(!$value->value_id){
					continue;
				}


				$object->otheruser_info = $model_user->get_user_registration($object->other_user_id);
				$object->thisuser_info = $model_user->get_user_registration(JFactory::getUser()->id);
				$object->referral = $this->get_referral($value->referral_id);
				$object->referral_value = $this->get_referral_value($value->value_id);

				$query = $db->getQuery(true);
				$object->referral =$this->activity_getReferral($object->referral, $query, $db);
				//$object->referral->referral_fee = $this->get_referral_value($object->value_id);
				if($object->activity_type == 11 ){
					if($object->referral->user_id != JFactory::getUser()->id){
						if($object->referral->currency!=$object->thisuser_info->currency){
							//$object->referral->price_1=$modelPropertyListing->getExchangeRates($object->referral->price_1,$object->thisuser_info->currency,$object->referral->currency);
							//$object->referral->price_2=$modelPropertyListing->getExchangeRates($object->referral->price_2,$object->thisuser_info->currency,$object->referral->currency);

								if($object->thisuser_info->currency!="USD"){									
									$object->referral->price_1=$object->referral->price_1 / (1/$ex_rates_CAD);
									$object->referral->price_2=$object->referral->price_2 / (1/$ex_rates_CAD);
								} else {
									$object->referral->price_1=$object->referral->price_1 / $ex_rates_CAD;
									$object->referral->price_2=$object->referral->price_2 / $ex_rates_CAD;
								}
						}
					}
				}
				if($object->activity_type == 17 ){
					$value =  $this->get_referral_status_object($object->activity_id);
					$id = $value->referral_id;
					$object->status_update = $value;
					//print_r($classes);
					if(is_numeric($value->note)){
						$note = $this->getNote($value->note);
						$object->status_update->note = $note;
					}
					$object->status_label = $this->getStatusLabel($value->status);
					if($value->status == 9){
						$object->closedref = $this->get_closed_referral_by_refid($id);

						if($object->closedref['referral']->agent_b == JFactory::getUser()->id){
							if($object->closedref['referral']->currency!=$object->thisuser_info->currency){
								
								//$object->closedref['referral']->price_1=$modelPropertyListing->getExchangeRates($object->closedref['referral']->price_1,$object->thisuser_info->currency,$object->closedref['referral']->currency);
									
									if($object->thisuser_info->currency!="USD"){
										$object->closedref['referral']->price_1=$object->closedref['referral']->price_1 / (1/$ex_rates_CAD);
									} else {
										$object->closedref['referral']->price_1=$object->closedref['referral']->price_1 / $ex_rates_CAD;
									}

								if($object->closedref['referral']->price_2){
									//$object->closedref['referral']->price_2=$modelPropertyListing->getExchangeRates($object->closedref['referral']->price_2,$object->thisuser_info->currency,$object->closedref['referral']->currency);
									if($object->thisuser_info->currency!="USD"){
										$object->closedref['referral']->price_2=$object->closedref['referral']->price_2 / (1/$ex_rates_CAD);
									} else {
										$object->closedref['referral']->price_2=$object->closedref['referral']->price_2 / $ex_rates_CAD;
									}
								}
							}

							$object->closedref['referral']->currency=$object->thisuser_info->currency;	

						}

					}

					if($object->status->created_by == JFactory::getUser()->id){

						if($object->referral->currency!=$object->thisuser_info->currency){
							//$object->referral->price_1=$modelPropertyListing->getExchangeRates($object->referral->price_1,$object->thisuser_info->currency,$object->referral->currency);
							if($object->thisuser_info->currency!="USD"){
								$object->referral->price_1=$object->referral->price_1 / (1/$ex_rates_CAD);
							} else {
								$object->referral->price_1=$object->referral->price_1 / $ex_rates_CAD;
							}
							if($object->referral->price_2){
							//	$object->referral->price_2=$modelPropertyListing->getExchangeRates($object->referral->price_2,$object->thisuser_info->currency,$object->referral->currency);
								if($object->thisuser_info->currency!="USD"){
									$object->referral->price_2=$object->referral->price_2 / (1/$ex_rates_CAD);
								} else {
									$object->referral->price_2=$object->referral->price_2 / $ex_rates_CAD;
								}
							}
						}

						$object->referral->currency=$user_info->currency;	

					}
					
				}
			}
		 if($object->activity_type == 12){
				$value =  $this->get_referral_status_object($object->activity_id);
				if($value->response){
					$value->response = $this->get_referral_status_object($value->response);
					if($value->response->response && ($value->referral_id != $value->response->referral_id))
						$value->response = $this->get_referral_status_object($value->response);
				}
				$object->status = $value;
				$object->referral = $this->get_referral($value->referral_id);
			}
			else if($object->activity_type == 19){
				$object->closed_referral_object = $this->get_closed_referral($object->activity_id);
				$thisuser_info = $model_user->get_user_registration(JFactory::getUser()->id);

				

				if($object->closed_referral_object['referral']->agent_b == JFactory::getUser()->id){
					if($object->closed_referral_object['referral']->currency!=$thisuser_info->currency){
					//	$object->closed_referral_object['referral']->price_1=$modelPropertyListing->getExchangeRates($object->closed_referral_object['referral']->price_1,$thisuser_info->currency,$object->closed_referral_object['referral']->currency);
						if($object->thisuser_info->currency!="USD"){
							$object->closed_referral_object['referral']->price_1=$object->closed_referral_object['referral']->price_1 / (1/$ex_rates_CAD);
						} else {
							$object->closed_referral_object['referral']->price_1=$object->closed_referral_object['referral']->price_1 * $ex_rates_CAD;
						}

						if($object->closed_referral_object['referral']->price_2){
							//$object->closed_referral_object['referral']->price_2=$modelPropertyListing->getExchangeRates($object->closed_referral_object['referral']->price_2,$thisuser_info->currency,$object->closed_referral_object['referral']->currency);
							if($object->thisuser_info->currency!="USD"){
								$object->closed_referral_object['referral']->price_2=$object->closed_referral_object['referral']->price_2 / (1/$ex_rates_CAD);
							} else {
								$object->closed_referral_object['referral']->price_2=$object->closed_referral_object['referral']->price_2 * $ex_rates_CAD;
							}
						}
					}

					$object->closed_referral_object['referral']->currency=$thisuser_info->currency;	

				}
			}
			else if($object->activity_type == 20 || $object->activity_type == 21){
				$object->referral = $this->get_referral($object->activity_id);
			}
			if($object->activity_type == 22){
				$object->referral = $this->get_referral($object->activity_id);
				$object->closed_referral_object = $this->get_closed_referral_by_refid($object->activity_id);
			}
			#added pocket
			if($object->activity_type == 25 || $object->activity_type == 26){
				$buyerid = $object->buyer_id;
				$query->select('*')->from('#__buyer')->where(array('buyer_id = '.$buyerid));
				$buyers = $db->setQuery($query)->loadObjectList();

				foreach ($buyers as $buyer){
					$query = $db->getQuery(true);
					$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyerid);
					$buyer->needs = $db->setQuery($query)->loadObjectList();
				}


				$db->setQuery($query);
				$values = $db->loadObject();
				$object->buyer = $buyers;
				$query0 = $db->getQuery(true);
				$query0->select("*, p.listing_id, p.user_id, pp.disclose, conutn.*");
				$query0->from('#__pocket_listing p');
				$conditions = "p.listing_id = ".$object->activity_id;
				$query0->where($conditions);
				$query0->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
				$query0->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
				$query0->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
				$query0->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
				$query0->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
				$query0->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
				$query0->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.pkId as pkid
									FROM #__request_access racs
									WHERE racs.permission = 1
									GROUP BY racs.property_id
									) as viewers on viewers.id = p.listing_id');
				$query0->leftJoin('(SELECT racs.property_id as id, racs.permission as per
									FROM #__request_access racs
									WHERE racs.user_b = ' . $user->id . '
									GROUP BY racs.property_id
									) as viewers2 on viewers2.id = p.listing_id');
				$query0->leftJoin('#__property_type ON type_id = p.property_type');
				$query0->leftJoin('#__property_sub_type ON sub_id = p.sub_type');
				$query0->leftJoin('#__countries conutn ON conutn.countries_id = p.country');
				$query0->order('p.setting desc');
				$query0->order('p.listing_id desc');
				//$query->order('racs.permission desc');
				$db->setQuery($query0);
				$object->listing = $db->loadObjectList();
				if(count($object->listing)) {
					$images = explode(",", $object->listing[0]->images);
					$object->listing[0]->images_bw = "images/buyers/56x46/" . $this->get_buyer_image($object->listing[0]->property_type, $object->listing[0]->sub_type, true);
					if(!$images[0]) {
						$object->listing[0]->images = "images/buyers/56x46/" . $this->get_buyer_image($object->listing[0]->property_type, $object->listing[0]->sub_type);
						#echo "<pre>", print_r($object->listing[0]), "</pre>";
					} else {
						$object->listing[0]->images = $images[0];
					}	
				}	

				if($object->activity_type == 26){
					foreach ($object->listing as $key => $value) {

						if($value->user_id != JFactory::getUser()->id){
							if($value->currency != $user_info->currency){
								//$value->price1 = $modelPropertyListing->getExchangeRates($value->price1,$user_info->currency,$value->currency);
								//var_dump($value->price1/$ex_rates_CAD);
									if($user_info->currency!="USD"){
										$value->price1=$value->price1 / (1/$ex_rates_CAD);
									} else {
										$value->price1=$value->price1 / $ex_rates_CAD;
									}

						
									
								if(($value->price2 != null || $value->price2 != "")){
									//$value->price2 = $modelPropertyListing->getExchangeRates($value->price2,$user_info->currency,$value->currency);
									if($user_info->currency!="USD"){
										$value->price2=$value->price2 / (1/$ex_rates_CAD);
									} else {
										$value->price2=$value->price2 / $ex_rates_CAD;
									}
								}
							}
							$value->currency=$user_info->currency;	
						}

					}
					
					

				} else {
					foreach ($object->listing as $key => $value) {

						$object->show_button = $this->check_existing_pops( $value->listing_id, $buyerid, 1 );
						if($value->user_id != JFactory::getUser()->id){
							if($value->currency != $user_info->currency){
								//$value->price1 = $modelPropertyListing->getExchangeRates($value->price1,$buyer->needs[0]->currency,$value->currency);
								if($user_info->currency!="USD"){
										$value->price1=$value->price1 / (1/$ex_rates_CAD);
									} else {
										$value->price1=$value->price1 / $ex_rates_CAD;
									}
								if(($value->price2 != null || $value->price2 != "")){
								//	$value->price2 = $modelPropertyListing->getExchangeRates($value->price2,$buyer->needs[0]->currency,$value->currency);
									if($user_info->currency!="USD"){
										$value->price2=$value->price2 / (1/$ex_rates_CAD);
									} else {
										$value->price2=$value->price2 / $ex_rates_CAD;
									}
								}
							}
							$value->currency=$user_info->currency;
						}	
					}				
				}

				
				#echo "<pre>", print_r($db->loadObjectList()), "</pre>";
			}
			#buyer
			if($object->activity_type == 23 || $object->activity_type == 24){
				//echo "<pre>"; print_r($object); die();
				$buyerid = $object->buyer_id;
				$query->select('*')->from('#__buyer')->where(array('buyer_id = '.$buyerid));
				$buyers = $db->setQuery($query)->loadObjectList();
				#print_r($buyers); die();
				foreach ($buyers as $key => $buyer){
					$query = $db->getQuery(true);
					$query->select('bn.*, pt.type_name as property_type, pst.name as sub_type,con.*')
						->from('#__buyer_needs bn')
						->leftJoin('#__property_type pt ON bn.property_type = pt.type_id')
						->leftJoin('#__property_sub_type pst ON bn.sub_type = pst.sub_id')
						->leftJoin('#__countries con ON con.countries_id = bn.country')
						->where('bn.buyer_id = '.$buyerid);
					$values = $db->setQuery($query)->loadObjectList();
					$property_type = strtolower(str_replace(" ", "-", $values[0]->property_type));
					$sub_type = strtolower($values[0]->sub_type);
					switch($sub_type) {
						case "motel/hotel":
							$sub_type = "motel";
							break;
						case "townhouse/row house":
							$sub_type = "townhouse";
							break;
						case "assisted care":
							$sub_type = "assist";
							break;
						case "special purpose":
							$sub_type = "special";
							break;
						case "multi family":
							$sub_type = "multi-family";
							break;
					}
					
					$img_filename = $property_type . "-" . $sub_type . ".jpg";
					
					$buyers[$key]->propertyImage = "images/buyers/56x46/" . $img_filename; 
					
					$buyers[$key]->needs = $db->setQuery($query)->loadObjectList();
					
				}

				$db->setQuery($query);
				$values = $db->loadObject();
				$object->buyer = $buyers;
				//$object->countries_iso_code_2 = $values->countries_iso_code_2;
				//echo "<pre>"; print_r($buyers); die();
			}
			/*else if($object->activity_type == 12 || $object->activity_type == 14){ //|| $object->activity_type == 15 || $object->activity_type == 14){
				$query
				->select("*")
				->from('#__referral r ')
				->leftJoin('#__client_intention ci ON ci.id = r.client_intention')
				->where('r.referral_id = '.$object->activity_id);
				$db->setQuery($query);
				$object->referral = $db->loadObject();

				$object->referral = $this->activity_getReferral($object->referral, $query, $db);
			}*/
		}
		
		#echo "<pre>", print_r($objects), "</pre>"; exit;
		
		return $objects;
	}
	
	function get_closed_referral_by_refid($id){
		$ppmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $ppmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__closed_referrals')
		->where('referral_id = '.$id)

		;
		
		$db->setQuery($query);
		$closedref = $db->loadObject();
		$temp = array();
		$temp['referral'] = $this->get_referral($closedref->referral_id);
		$temp['clrfobject'] = $closedref;
		$temp['payments'] = $model2->get_AB_ref_fee($closedref->price_paid);
		$temp['client'] = $this->get_client_info($temp['referral']->client_id);
		return $temp;
	}
	
	public function getNote($id){
		$reasons = array('Client unresponsive','Client working with another agent','Client\'s needs changed','Retracted');
		return $reasons[$id];
	}
	
	public function getStatusLabels(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('label')->from('#__referral_status');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	public function getStatusLabel($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('label')->from('#__referral_status')->where('status_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->label;
	}
	
	public function get_closed_referral($id){
		$ppmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $ppmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__closed_referrals')->where('clrf_id = '.$id);
		$db->setQuery($query);
		$closedref = $db->loadObject();
		$temp = array();
		$temp['referral'] = $this->get_referral($closedref->referral_id);
		$temp['clrfobject'] = $closedref;
		$temp['payments'] = $model2->get_AB_ref_fee($closedref->price_paid);
		$temp['client'] = $this->get_client_info($temp['referral']->client_id);
		return $temp;
	}
	
	public function get_referral_status_object($status){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__referral_status_update')
		->where('update_id = '.$status);
		$db->setQuery($query);
		$value = $db->loadObject();
		$value->value = $this->get_referral_value($value->value_id);
		return $value;
	}
	
	public function get_referral_value($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__referral_value')
		->where('value_id = '.$id)
		->order('value_id DESC limit 1');
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	public function get_referral($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select("*, (SELECT group_concat(other_user_id) as signatures FROM `#__activities` WHERE activity_type = 16 and activity_id = ".$id.") as signatories, (SELECT count(*) as signatures FROM `#__activities` WHERE activity_type = 16 and activity_id = ".$id.') as signatures')
		->from('#__referral r')
		->where('referral_id = '.$id)
		->leftJoin('#__client_intention ci on ci.id = r.client_intention')
		->leftJoin('#__buyer_address ba on ba.buyer_id = r.client_id')
		//->leftJoin('#__country_currency countrycurr on countrycurr.country = ba.country')
		;
		$db->setQuery($query);
		//echo $query; die();
		$ref = $db->loadObject();
		$ref->client = $this->get_client_info($ref->client_id);
		return $ref;
	}

	public function activity_getReferral($object, $query, $db){
//print_r($object); die();
		$query = $db->getQuery(true);
		$query
		->select("pty.property_name as name, pta.address, pcti.image, pty.description as property_desc, pty.zip as zip")
		->from('#__pocket_listing pty')
		->leftJoin('(select group_concat(address) as address, pocket_id from #__pocket_address group by pocket_id) pta on pta.pocket_id = pty.listing_id')
		->leftJoin('#__pocket_images pcti on pcti.listing_id = pty.listing_id')
		->where('pty.listing_id = '.$object->property_id);
		$db->setQuery($query);

		$values = $db->loadObject();
		$object->property = new stdClass();
		$object->property->name = $values->name;
		$object->property->address = $values->address;
		$object->property->zip = $values->zip;
		$object->property->propertyImage = $values->image;
		$object->property->property_desc = $values->property_desc;

		

		$object->buyer = new stdClass();
		$object->buyer = $this->get_client_info($object->client_id);

		return $object;
	}
	
	public function get_client_info($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__buyer')
		->where('buyer_id = '.$id);
		return $db->setQuery($query)->loadObject();
	}

	public function getCountry($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('countries_name');
		$query->from('#__countries');
		$query->where('countries_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->countries_name;
	}

	public function getState($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where('zone_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->zone_name;
	}

	public function update_request($activity_id, $status){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$object = new JObject();
		$object->pk_id = $activity_id;
		$object->status = $status;
		$db->updateObject('#__request_network', $object, 'pk_id');

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__request_network');
		$query->where('pk_id = '.$activity_id);
		$activity = $db->setQuery($query)->loadObject();

	}
	
	public function update_contact_request($activity_id, $status){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$object = new JObject();
		$object->pk_id = $activity_id;
		$object->status = $status;
		$db->updateObject('#__request_network', $object, 'pk_id');

		//this instruction were discarded
		//swap values 
		// $query->setQuery("
			// UPDATE
				// #__request_network s1,
				// #__request_network s2
			// SET 
				// s1.user_id= s1.other_user_id,
				// s1.other_user_id = s2.user_id
			// WHERE
				// s1.pk_id = s2.pk_id and s1.pk_id = $activity_id");
		// $db->setQuery($query);
		// $db->execute();
		

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__request_network');
		$query->where('pk_id = '.$activity_id);
		$activity = $db->setQuery($query)->loadObject();

		// notifying the adder that added user accepts the request
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)29;
		$insert_activity->activity_id = (int)$activity_id;
		$insert_activity->user_id = $activity->user_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = $activity->other_user_id;
		$ret = $db->insertObject('#__activities', $insert_activity);
	}

	function logsignedActivity($agenta, $agentb, $activityid, $mode = 1){
		$db = JFactory::getDbo();
		if($mode){
			$act_type=16;
		}
		else{
			$act_type=21;
		}
		
		$insert_activity = new JObject();
		$insert_activity->activity_type = $act_type;
		$insert_activity->activity_id = (int)$activityid;
		$insert_activity->user_id = $agenta;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = $agentb;
		$ret = $db->insertObject('#__activities', $insert_activity);
		
		$this->send_signed_mail($agenta, $agentb, $activityid);
		$db->setQuery('SELECT count(*) as count FROM `tbl_activities` WHERE activity_type = 16 and activity_id = '.$activityid);
		
		return $db->loadObject();
	}
	
	function send_signed_mail($agenta, $agentb, $activityid){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		$mailer = JFactory::getMailer();
		$listingname=(!empty($listing->property_name)) ? $listing->property_name : 'unnamed';
		$mailsubject = JFactory::getUser($agentb)->name.' signed your referral agreement';
		$body = JFactory::getUser($agentb)->name.' signed the referral agreement. Please login to AgentBridge to further process the <a href = '.JRoute::_('index.php?option=com_nrds&return='.base64_encode('index.php?option=com_propertylisting&task=agreement&ref_id='.$activityid),true,-1).'>referral</a>.';
		$mailer->IsHTML(true);
		//$mailer->sendMail('AgentBridge', 'AgentBridge', JFactory::getUser($agenta)->email, $mailsubject, $body);
	}
	
	function clearmail(){
		$mailer2 = JFactory::getMailer();
		$mailer2->ClearAddresses();
		$mailer2->ClearAllRecipients();
		$mailer2->ClearAttachments();
	}
	
	function send_contact_details($referral, $user, $other){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		
		$mailer = JFactory::getMailer();
		$client->name = stripslashes($client->name);
		$name = stripslashes(JFactory::getUser($user)->name);
		$othername = stripslashes(JFactory::getUser($other)->name);
		$othername2 = stripslashes(JFactory::getUser($other_user)->name);
		
		$client = $model2->get_client($referral->client_id);
		$note= $model2->get_buyer_note($referral->client_id);
		$mailsubject = $name." has signed your AgentBridge Referral contract";
		$body = "<p style='text-align:justify; padding-top:20px'>Hello ".$othername.",<br/><br/>";
		$body .= "Good news! <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=$user'>".$name."</a> has accepted your referral and has received ".$client->name."'s contact information. Once <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=$user'>".$name."</a> has contacted your client and established a working relationship, <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=$user'>".$name."</a> will update the referral in the AgentBridge system to \"Active.\"<br/><br/>";
		$body .= "AgentBridge will send you updates throughout the transaction and you can also monitor the process by checking your account at <a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>AgentBridge</a>.<br/><br/>";
		$body .= "AgentBridge recognizes that your reputation is connected to the handling of this referral. As such, AgentBridge has implemented a Need Help feature that allows <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=$user'>".$name."</a> to request your assistance with <a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>".$client->name."</a> if needed. If <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=$user'>".$name."</a> feels your assistance would be beneficial, you will be notified.<br/><br/>";
		$body .= "We recommend that you take a moment to make sure your <a href='".JRoute::_('index.php?option=com_userprofile&task=emailsnotif', true, -1)."'>Notification Settings</a> are correct in your AgentBridge profile. This will ensure prompt communication as needed.<br/><br/>";
		$body .= "Thank you for allowing AgentBridge to facilitate this referral for you.<br/>";
		$body .= "The AgentBridge Team</p>";
		$this->clearmail();
		$mailer->IsHTML(true);
		$mailer->sendMail('no-reply@agentbridge.com', 'AgentBridge', JFactory::getUser($other)->email, $mailsubject, $body);
		$this->clearmail();
		$mailer2 = JFactory::getMailer();

		$mobile = implode(',',array_map(function($o){ return $o->number; }, $client->mobile));
		$email = implode(',',array_map(function($o){ return $o->email; }, $client->email));
		$contactmethod = $model2->get_contact_method_label($client->contact_method);
		$mailsubject = "Here's the client info for your AgentBridge Referral!";
		$body = "<p style='text-align:justify; padding-top:20px '>Hello ".$name.",<br/><br/>";
		$body .= "Thank you for accepting <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$other."'>".$othername."</a>'s referral of <a href='".JRoute::_('index.php?option=com_propertylisting&task=referrals', true, -1)."'>".$client->name."</a>. You have committed to contact the client as soon as possible.<br/><br/>After connecting with ".$client->name." and determining needs, please update the AgentBridge referral status to &ldquo;Active.&rdquo; <br/><br/>";
		$body .= "Contact Information for ".$client->name.":<br/>";
		$body .= "Preferred method of contact: $contactmethod";
		$body .= "Mobile: ".$mobile."<br/>";
		$body .= "Email Address: $email<br/>";
		if ($note) {
			$body .= "Note: $note<br/><br/>";
		} else {
			$body .= "Note: <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$other."'>".$othername."</a> has not entered notes for this client.<br/><br/>";
		}
		/*$body .= $client->address[0]->address_1." ".$client->address[0]->address_2."<br/>";
		$state = ($client->address[0]->state) ? is_numeric($client->address[0]->state) ? $model2->getState($client->address[0]->state): $client->address[0]->state : "";
		$body .= $client->address[0]->city.", ".$state." ".$client->address[0]->zip."<br/>";
		$body .= $model2->getCountry($client->address[0]->country)."<br/>";*/
		$body .= "If you need assistance from <a href='".JRoute::_('index.php?option=com_userprofile&task=profile', true, -1)."&uid=".$other."'>".$othername."</a>, change the status for this referral to &lsquo;Need Help&lsquo; and we will quickly respond.<br/><br/>";
		$body .= "Thank you for trusting AgentBridge to facilitate this transaction.<br/>";
		$body .= "The AgentBridge Team</p>";
			
		$mailer2->sendMail('no-reply@agentbridge.com', 'AgentBridge', JFactory::getUser($user)->email, $mailsubject, $body);
	}
	
	function didsign($user, $form){
		$db = JFactory::getDbo();
		$db->setQuery('SELECT count(*) as count FROM `tbl_activities` WHERE activity_type = 16 and activity_id = '.$form.' AND other_user_id = '.$user);
		return $db->loadObject()->count;
	}

	function broadcast_contact($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('client_id')->from('#__referral')->where('referral_id = '.$id);
		$db->setQuery($query);
		$id = $db->loadObject()->client_id;
		return $id;
	}

	function get_buyer($id){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =	$this->getInstance('UserProfile', 'UserProfileModel');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
		->from('#__buyer b')
		->where('buyer_id = '.$id);

		$buyer = $db->setQuery($query)->loadObject();
		$buyer->email=$model2->get_buyer_email($buyer->buyer_id);
		$buyer->mobile=$model2->get_buyer_mobile($buyer->buyer_id);
		$buyer->address=$model2->get_buyer_address($buyer->buyer_id);
		$buyer->note=$model2->get_buyer_note($buyer->buyer_id);

		return $buyer;
	}
	
	public function get_buyer_image($property_type, $sub_type, $bw = false) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$config = new JConfig();
		$query->select('type_name')->from('#__property_type')->where('type_id = '.$property_type);
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		#print_r($rows);
		
		#echo "<br />";
		
		$query->select('name')->from('#__property_sub_type')->where('type_id = '.$property_type.' AND sub_id='.$sub_type);
		$db->setQuery($query);
		$rows2 = $db->loadObjectList();
		
		$property_type = strtolower(str_replace(" ", "-", $rows2[0]->type_name));
		$sub_type = strtolower($rows2[0]->name);
		switch($sub_type) {
			case "motel/hotel":
				$sub_type = "motel";
				break;
			case "townhouse/row house":
				$sub_type = "townhouse";
				break;
			case "assisted care":
				$sub_type = "assist";
				break;
			case "special purpose":
				$sub_type = "special";
				break;
			case "multi family":
				$sub_type = "multi-family";
				break;
		}
		
		$img_filename = ($bw) ? $property_type . "-" . $sub_type . "-bw.jpg" : $property_type . "-" . $sub_type . ".jpg";
		return $img_filename;
	}
	
	public function get_network_raw($userid) {
		$db = JFactory::getDbo();
		$conditions = array();
		$conditions[] = 'user_id = '.$userid;
		$innerquery = $db->getQuery(true);
		$query = $db->getQuery(true);
		/*$add = ($broker_id) ? "
								 UNION	(
										SELECT	u.id
										  FROM	#__user_registration r,
												#__users u
										 WHERE	r.email = u.email
										   AND	r.brokerage = ".$broker_id."
										)" : "";*/
		$add = "";
		$query->setQuery("
				SELECT	other_user_id, status
				  FROM	#__request_network
				 WHERE	user_id =".$userid
				);
		$db->setQuery($query);
		
		$result = $db->loadObjectList();
		$network = array();
		foreach($result as $contact) {
			$network[$contact->other_user_id] = $contact->status;
		}
		return $network;
	}
	
	public function get_network($userid, $status = 1){
		$db = JFactory::getDbo();
		$conditions = array();
		$conditions[] = 'user_id = '.$userid;
		$conditions[] = 'status = '.$status;
		$innerquery = $db->getQuery(true);
		$query = $db->getQuery(true);
		/*$add = ($broker_id) ? "
								 UNION	(
										SELECT	u.id
										  FROM	#__user_registration r,
												#__users u
										 WHERE	r.email = u.email
										   AND	r.brokerage = ".$broker_id."
										)" : "";*/
		$add = "";
		$query->setQuery("
				SELECT	other_user_id
				  FROM	#__request_network
				 WHERE	user_id =".$userid."
				   AND	status = ".$status.$add
				);
		$db->setQuery($query);
		
		$result = $db->loadObjectList();
		$network = array();
		foreach($result as $contact) {
			$network[] = $contact->other_user_id;
		}
		return $network;
	}
}