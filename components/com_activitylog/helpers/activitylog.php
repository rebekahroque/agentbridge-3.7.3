<?php
/**
 * @version     1.0.0
 * @package     com_activitylog
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com
 */

defined('_JEXEC') or die;

abstract class ActivitylogHelper
{
	public static function myFunction()
	{
		$result = 'Something';
		return $result;
	}

}

