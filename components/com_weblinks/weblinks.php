<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_weblinks
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/helpers/route.php';

if(JFactory::getUser()->id){
	JFactory::getApplication()->redirect('index.php');
 } else {
    JFactory::getApplication()->redirect('index.php?illegal=1&returnurl='.base64_encode(JURI::current().$_SERVER['REQUEST_URI']));
 }


/*
$controller	= JControllerLegacy::getInstance('Weblinks');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
*/