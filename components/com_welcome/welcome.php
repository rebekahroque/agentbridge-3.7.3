<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT . '/controller.php' );
 

 if(JFactory::getUser()->id){
	JFactory::getApplication()->redirect('index.php');
 	
 } else {
    JFactory::getApplication()->redirect('index.php?illegal=1&returnurl='.base64_encode(JURI::current().$_SERVER['REQUEST_URI']));
 }


/*// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'WelcomeController'.$controller;
$controller   = new $classname( );
  
// Perform the Request task
$controller->execute( JRequest::getWord( 'task' ) );
  
// Redirect if set by the controller
$controller->redirect();
*/
?>

