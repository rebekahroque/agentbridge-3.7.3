<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Base controller class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.5
 */
class UsersController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean			If true, the view output will be cached
	 * @param   array  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		// Get the document object.
		$document	= JFactory::getDocument();

		// Set the default view name and format from the Request.
		$vName   = $this->input->getCmd('view', 'login');
		$vFormat = $document->getType();
		$lName   = $this->input->getCmd('layout', 'default');


		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$userModel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		if ($view = $this->getView($vName, $vFormat))
		{		
			// Do any specific processing by view.
			switch ($vName)
			{
				case 'registration':
					// If the user is already logged in, redirect to the profile page.
					$user = JFactory::getUser();
					if ($user->get('guest') != 1)
					{
						// Redirect to profile page.
						$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
						return;
					}

					// Check if user registration is enabled
					if (JComponentHelper::getParams('com_users')->get('allowUserRegistration') == 0)
					{
						// Registration is disabled - Redirect to login page.
						$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
						return;
					}

					// The user is a guest, load the registration model and show the registration page.
					$model = $this->getModel('Registration');
					break;

				// Handle view specific models.
				case 'profile':

					// If the user is a guest, redirect to the login page.
					$user = JFactory::getUser();
					if ($user->get('guest') == 1)
					{
						// Redirect to login page.
						$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
						return;
					}
					$model = $this->getModel($vName);
					break;

				// Handle the default views.
				/*case 'login':
					$user = JFactory::getUser();
					if ($user->get('guest') != 1 )
					{
						if(isset($_GET['return'])){
						// Redirect to profile page.
						$app = JFactory::getApplication();
						$redirect_def=base64_decode($_GET['return']);
		  				$this->setRedirect(JRoute::_($redirect_def, false));
						//$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
						return;
						} else {
							$this->setRedirect(JRoute::_('index.php?option=com_userprofile', false));
							return;
						}
					} else {
						$model = $this->getModel($vName);
					}
					
					break*/

				case 'reset':
					// If the user is already logged in, redirect to the profile page.
					$user = JFactory::getUser();
					if ($user->get('guest') != 1)
					{
						// Redirect to profile page.
						$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
						return;
					}


					$model = $this->getModel($vName);
					break;

				case 'remind':
					// If the user is already logged in, redirect to the profile page.
					$user = JFactory::getUser();
					if ($user->get('guest') != 1)
					{
						// Redirect to profile page.
						$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
						return;
					}

					$model = $this->getModel($vName);
					break;
				
				case 'securityquestion':
					$model = $this->getModel('Reset', 'UsersModel');
					$regid = $model->get_regid_frm_email($_REQUEST['jform']['email']);
					$questions = $model->get_questions($regid);
					$index = rand(1, 3);
					$question = "set".$index;
					$answer = "ans".$index;
					
					$user_question = $model->getQuestion($question, $questions->$question);
					$user_answer = $questions->$answer;
					die();
					break;
				default:
					$user = JFactory::getUser();
					$app    = JFactory::getApplication();

					if($user->get('guest')){
						if(isset($_GET['h321']) && $_GET['h321']!=""){
							$regid= base64_decode($_GET['h321']);
							$userdets = $userModel->get_user_details("*",$regid);
							$username = $userdets->firstname." ".$userdets->lastname;
							$user_email = $userdets->email;
							$view = &$this->getView($vName, 'html');
							$view->assignRef('username', $username);
							$view->assignRef('user_email', $user_email);
						}				
							$model = $this->getModel('Login');
					} else {
						if(isset($_GET['returnurl'])){
							$returnurl = urldecode(base64_decode($_GET['returnurl']));
							$app->redirect(JRoute::_(''.$returnurl.'', false));
						} else {
							$app->redirect(JRoute::_('index.php?option=com_activitylog', false));
						}

					}


					break;
			}


			$task = JRequest::getVar('task');

			switch ($task):

				case "login":
					if ($user->get('guest') != 1 )
					{
						if(isset($_GET['return'])){
						// Redirect to profile page.
							$app = JFactory::getApplication();
							$redirect_def=base64_decode($_GET['return']);
		  					$this->setRedirect(JRoute::_($redirect_def, false));
						//$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile', false));
						return;
						} else {
							$this->setRedirect(JRoute::_('index.php?option=com_userprofile', false));
							return;
						}
					} else {
						$model = $this->getModel($vName);
					}
				break;
			
			endswitch;

			// Push the model into the view (as default).
			$view->setModel($model, true);
			$view->setLayout($lName);
			
			// Push document object into the view.
			$view->document = $document;

			$view->display();
		}
	}

	function setLanguage($compName,$langId){

        $compName = $_POST['compName'];
        $langId = $_POST['langId'];
        $user = JFactory::getUser();

        // Create an object for the record we are going to update.
		$object = new stdClass();
		 
		// Must be a valid primary key value.
		$object->id = $user->id;
		$object->currLanguage = $langId;
		 
		// Update their details in the users table using id as the primary key.
		$result = JFactory::getDbo()->updateObject('#__users', $object, 'id');


        $language =& JFactory::getLanguage();
        $extension = $compName;
        $base_dir = JPATH_SITE;
        $language_tag = $langId;
        $language->load($extension, $base_dir, $language_tag, true);

    }
	
	function checkAnswers(){

		$answer = $_POST['answer'];
		$qset = $_POST['qset'];
		$userId = $_POST['userId'];


	    $db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'))
		->from('#__answers')
		->where('uid = '.$userId.' AND set1='.$qset.'');
		$db->setQuery($query);
		$object = $db->loadObject();


		if(strtoupper($answer) == strtoupper($object->ans1)){
			echo "success";
		} else {
			echo "false";
		}




	}
	
}
