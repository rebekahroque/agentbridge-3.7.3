<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Reset controller class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersControllerReset extends UsersController
{
	/**
	 * Method to request a password reset.
	 *
	 * @since   1.6
	 */
	
	public function securityquestion(){
		$model = $this->getModel('Reset', 'UsersModel');
		$regid = $model->get_regid_frm_email($_REQUEST['jform']['email']);
		$questions = $model->get_questions($regid);
		$index = rand(1, 3);
		$question = "set".$index;
		$answer = "ans".$index;
		
		$user_question = $model->getQuestion($question, $questions->$question);
		$user_answer = $questions->$answer;
		echo "dsafasdf";
		die();
	}
	
	public function request()
	{
		// Check the request token.
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('Reset', 'UsersModel');
		$data  = $this->input->post->get('jform', array(), 'array');


		if(isset($_GET['lang']) && $_GET['lang']!=""){
            $language =& JFactory::getLanguage();
            $extension = 'com_nrds';
            $base_dir = JPATH_SITE;
            $language_tag = $_GET['lang'];
            $language->load($extension, $base_dir, $language_tag, true);
	    } else {

	          $language = JFactory::getLanguage();
	          $extension = 'com_nrds';
	          $base_dir = JPATH_SITE;
	          $language_tag = "english-US";
	          $language->load($extension, $base_dir, $language_tag, true);

        }

		// Submit the password reset request.
		

		$db = JFactory::getDbo();
		$query_reg = $db->getQuery(true)
			->select('user_type')
			->from($db->quoteName('#__user_registration'))
			->where($db->quoteName('email') . ' = ' . $db->quote($data['email']));

		// Get the user object.
		$user_type = $db->setQuery($query_reg)->loadResult();

		

		if ($user_type == 86 )
		{		
			$return	= false;
		} else {
			$return	= $model->processResetRequest($data);
		}

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->getCfg('error_reporting'))
			{
				$message = $return->getMessage();
			} else {
				$message = JText::_('COM_USERS_RESET_REQUEST_ERROR');
			}

			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset'.$itemid;

			// Go back to the request form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');
			return false;
		} elseif ($return === false || $user_type == 86 )
		{
			// The request failed.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			
			if($user_type==86){
				$route	= 'index.php?option=com_users&view=reset&by=usertype'.$itemid;
				// Go back to the request form.
				//$message = JText::sprintf('COM_USERS_RESET_REQUEST_FAILED', $model->getError());
				$message ="This account has been deactivated. Please contact support@agentbridge.com for inquiries.";
				$this->setRedirect(JRoute::_($route, false));
			} else {
				$route	= 'index.php?option=com_users&view=reset'.$itemid;
				// Go back to the request form.
				$message = JText::sprintf('COM_USERS_RESET_REQUEST_FAILED', $model->getError());
				$this->setRedirect(JRoute::_($route, false), $message, 'notice');
			}
			
			return false;
		}
		else
		{
			// The request succeeded.
			// Get the route to the next page.
			/*$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=confirm'.$itemid;
			
			// Proceed to step two.
			$this->setRedirect(JRoute::_($route, false));*/
			
			if(isset($_GET['request'])){
				$message = "Thank you. Please check your email for further instructions on how to reset your password.";
				$route	= 'index.php?option=com_users&view=reset'.$itemid.'&success';
				// Go back to the request form.
				$this->setRedirect(JRoute::_('index.php?option=com_nrds'), $message, 'success');
				return false;
			}
			
			
			return true;
		}
	}

	/**
	 * Method to confirm the password request.
	 *
	 * @access	public
	 * @since   1.6
	 */
	public function confirm()
	{
		// Check the request token.
		JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('Reset', 'UsersModel');
		$data  = $this->input->get('jform', array(), 'array');

		// Confirm the password reset request.
		$return	= $model->processResetConfirm($data);

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->getCfg('error_reporting'))
			{
				$message = $return->getMessage();
			} else {
				$message = JText::_('COM_USERS_RESET_CONFIRM_ERROR');
			}

			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=confirm'.$itemid;

			// Go back to the confirm form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');
			return false;
		} elseif ($return === false)
		{
			// Confirm failed.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=confirm'.$itemid;

			// Go back to the confirm form.
			$message = JText::sprintf('COM_USERS_RESET_CONFIRM_FAILED', $model->getError());
			$this->setRedirect(JRoute::_($route, false), $message, 'notice');
			return false;
		}
		else
		{
			// Confirm succeeded.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=secQuestion'.$itemid;

			// Proceed to step three.
			$this->setRedirect(JRoute::_($route, false));
			return true;
		}
	}

	public function secQuestion()
	{
		// Check the request token.
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('Reset', 'UsersModel');
		$data  = $this->input->get('jform', array(), 'array');

		$userId	= $app->getUserState('com_users.reset.user', null);

		// Confirm the password reset request.
		/*$return	= $model->processResetSecQuestion($data);

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->getCfg('error_reporting'))
			{
				$message = $return->getMessage();
			} else {
				$message = JText::_('COM_USERS_RESET_SECQUESTION_ERROR');
			}

			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=secQuestion'.$itemid;

			// Go back to the confirm form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');
			return false;
		} elseif ($return === false) 
		{
			// Confirm failed.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=secQuestion'.$itemid;

			// Go back to the confirm form.
			$message = JText::sprintf('COM_USERS_RESET_SECQUESTION_FAILED', $model->getError());
			$this->setRedirect(JRoute::_($route, false), $message, 'notice');
			return false;
		}
		else
		{*/
			// Confirm succeeded.
			// Get the route to the next page.
			//var_dump("Sadsadsad");
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=complete'.$itemid;

			// Proceed to step three.
			$this->setRedirect(JRoute::_($route, false));
			return true;
		//}
	}

	/**
	 * Method to complete the password reset process.
	 *
	 * @since   1.6
	 */
	public function complete()
	{
		// Check for request forgeries
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('Reset', 'UsersModel');
		$data  = $this->input->post->get('jform', array(), 'array');

		// Complete the password reset request.
		$return	= $model->processResetComplete($data);

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->getCfg('error_reporting'))
			{
				$message = $return->getMessage();
			} else {
				$message = JText::_('COM_USERS_RESET_COMPLETE_ERROR');
			}

			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=complete'.$itemid;

			// Go back to the complete form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');
			return false;
		} elseif ($return === false)
		{
			// Complete failed.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getResetRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_users&view=reset&layout=complete'.$itemid;

			// Go back to the complete form.
			$message = JText::sprintf('COM_USERS_RESET_COMPLETE_FAILED', $model->getError());
			$this->setRedirect(JRoute::_($route, false), $message, 'notice');
			return false;
		}
		else
		{
			// Complete succeeded.
			// Get the route to the next page.
			$itemid = UsersHelperRoute::getLoginRoute();
			$itemid = $itemid !== null ? '&Itemid='.$itemid : '';
			$route	= 'index.php?option=com_nrds';

			// Proceed to the login form.
			$message = JText::_('COM_USERS_RESET_COMPLETE_SUCCESS');
			//$this->setRedirect(JRoute::_($route, false), $message);
			$this->setRedirect(JRoute::_(JURI::base()), $message);
			return true;
		}
	}
}
