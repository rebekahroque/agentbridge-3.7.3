<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

?>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">
	<div class="short-container full-radius new-password" style="color:#000000; text-align:left"><!-- start new password-->
	<input type="hidden" value="<?php echo $_GET['useregid'] ?>" name="regid" />	
	<?php //if ($this->params->get('show_page_heading')) : ?>
	<div class="forms-head">
		<h3 style="font-size:24px; font-weight:bold; padding-bottom:20px; border-bottom:1px dotted #bfbfbf">Your New Password</h3>
		<div class="user"></div>
		<p style="line-height:20px; font-size:16px; margin-top:10px">Password must be at least 8 characters and must contain uppercase, lowercase, and numeric characters. </p><br/>
	</div>
	<?php //endif; ?>

	<form id="reset" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete'); ?>" method="post" class="form-validate">

		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
		
			<?php /* foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<div>
					<?php //echo $field->label; ?>
					<?php echo $field->input; ?>
				</div>
			<?php endforeach; */ ?>
			
			<input type="hidden" value="reset_password" id="jform_form" name="jform[form]" />	
			<div>
				<label>Enter New Password</label>
				<input type="password" size="30" class="validate-password text-input required invalid" autocomplete="off" value="" id="jform_password1" name="jform[password1]" aria-required="true" required="required" aria-invalid="true" />				
			</div>
			<div>
				<p class="jform_password1 error_msg" style="display:none; line-height:16px; font-size:11px"> This field is required   <br /></p> 
				<p id="pw_min" class="jform_password1_min error_msg" style="display:none; line-height:16px; font-size:11px"> Password must be at least 8 characters  <br /></p> 
				<p id="pw_req" class="jform_password1_req error_msg" style="display:none; line-height:16px; font-size:11px"> Password must contain uppercase, lowercase and numeric characters  <br /></p> 
			</div>
			<div>
				<label>Confirm New Password</label>
				<input type="password" size="30" class="validate-password text-input required" autocomplete="off" value="" id="jform_password2" name="jform[password2]" aria-required="true" required="required"  oncopy="return false" onpaste="return false" oncut="return false" placeholder="" />				
			</div>
			<div>
				<p class="jform_password2 error_msg" style="display:none; line-height:16px; font-size:11px"> This field is required   <br /></p> 
			</div>
			

		<?php endforeach; ?>

		<div class="login-form right" style="margin-right:20px">
			<input id="submitreset" type="submit" style="padding-top:8px; width:120px; outline:0; text-align:center" class="submit-btn" value="Reset Password"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
	</div>
</div>
<script>
jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("ul").hide();
	resetPass();

});
function resetPass(){

	jQuery("#submitreset").click( function( event ){
	
	var ctr = 0;
		
	var pw = jQuery("#jform_password1").val();
	
	var has_upletters = (/[a-z]/).test(pw);
    
	var has_lwletters = (/[A-Z]/).test(pw);
	
	var has_numbers = (/[0-9]/).test(pw);
		
		if (pw.length < 8) {
		
			jQuery("#jform_password1").addClass("glow-required");
				
			jQuery(".jform_password1_min").show();
			
			ctr++;
			
		} else {
		
			jQuery("#jform_password1").removeClass("glow-required");
			
			jQuery(".jform_password1_min").remove();
			
			}
		
		if (has_upletters + has_lwletters + has_numbers < 3) {
			
			jQuery("#jform_password1").addClass("glow-required");
			
			jQuery(".jform_password1_req").show();
			
			ctr++;
			
		} else {
			
			jQuery("#jform_password1").removeClass("glow-required");
			
			jQuery(".jform_password1_req").remove();
		
		}

		if(jQuery("#jform_password2").val()==""){jQuery("#jform_password2").addClass("glow-required");jQuery(".jform_password2").show();

			jQuery("#jform_password2").focus();
			
			ctr++;

		} else { jQuery("#jform_password2").removeClass("glow-required");jQuery(".jform_password2").hide(); }	

		if(jQuery("#jform_password1").val()==""){jQuery("#jform_password1").addClass("glow-required");jQuery(".jform_password1").show();

			jQuery("#jform_password1").focus();
			
			ctr++;
			
		} else { jQuery("#jform_password1").removeClass("glow-required");jQuery(".jform_password1").hide(); }	

	

		if(jQuery("#jform_password1").val()!=jQuery("#jform_password2").val()){

			jQuery(".jform_password1").html("Passwords do not match <br /> <br />");

			jQuery(".jform_password1").show();

			jQuery("#jform_password1").focus();

			jQuery("#jform_password1").addClass("glow-required");

		}
			

		event.preventDefault();

		if(

			$jquery("#jform_password2").val()!="" &&

			$jquery("#jform_password1").val()!="" &&
			
			!(has_upletters + has_lwletters + has_numbers < 3) &&
			
			!(pw.length < 8) &&

			($jquery("#jform_password1").val()==$jquery("#jform_password2").val())

		){ 		
			jQuery("#reset").submit();
		}
		
	});		
}
	
</script>