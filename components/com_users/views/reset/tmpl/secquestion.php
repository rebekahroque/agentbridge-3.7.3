<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');

$app   = JFactory::getApplication();
$userId	= $app->getUserState('com_users.reset.user', null);

    $db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select(array('*'))
	->from('#__answers')
	->where('uid = '.$userId.'');
	$db->setQuery($query);
	$object = $db->loadObject();

	$ansq=$object->ans1;
	$setq=$object->set1;

	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select("set1")
	->from('#__security_questions')
	->where('qid = '.$setq);
	$db->setQuery($query);
	$object = $db->loadObject();


	 $db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select(array('*'))
	->from('#__users')
	->where('id = '.$userId.'');
	$db->setQuery($query);
	$object2 = $db->loadObject();

	$language = JFactory::getLanguage();
    $extension = 'com_nrds';
    $base_dir = JPATH_SITE;
    $language_tag = $object2->currLanguage;
    $language->load($extension, $base_dir, $language_tag, true);
?>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">
	<div class="short-container full-radius forgot-password" style="color:#000000; text-align:left"><!-- start forgot password -->

	<form id="secanswerform" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.secQuestion'); ?>" method="post" class="form-validate">
		
		<?php //if ($this->params->get('show_page_heading')) : ?>
		<div class="forms-head">
			<p style="line-height:25px; line-height:28px; font-size:23px; margin:10px 0px 0px 0px; padding-bottom:20px; border-bottom:1px dotted #bfbfbf">Security Question For New Password</p><br>
			<p style="line-height:20px; line-height:24px; font-size:16px; margin-top:10px">Please match answer entered at registration.</p><br>
		</div>
		<?php //endif; ?>
		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
		 <?php $i=0;?>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<?php if($i==0){?>
				<div>
					<label><?php echo JText::_($object->set1); ?>?</label>
					<input type="text" name="jform[email]" id="jform_email" value="" class="validate-username text-field required invalid" size="20" placeholder="" required="required" aria-required="true" aria-invalid="true">
				</div>
				<?php $i++;?>
				<?php }?>
				<?php endforeach; ?>
			
		<?php endforeach; ?>

		<div class="login-form right" style="margin-right:20px;">
			<input id="submithis" name="" value="Reset Password" type="submit" style="padding-top:8px;outline:0;" class="submit-btn"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
		<div id="wrongans" class="error_msg" style="clear:both; line-height:16px; display:none; font-size:11px">
			Answer is not an exact match. Please try again or email <a style="text-decoration:none; font-weight:bold; line-height:16px; font-size:11px; color:red" href="mailto:support@agentbridge.com?subject=Account&nbsp&nbsp;Locked">support@agentbridge.com</a> for help.
		</div>
	</form>
</div>
</div>

<script>
jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("ul").hide();

	jQuery("#submithis").click(function(e){
		 e.preventDefault();
		 var self = this;
		if(jQuery("#jform_email").val()){
			jQuery("#wrongans").hide();
			jQuery.ajax({
				url: '<?php echo $this->baseurl ?>/index.php?option=com_users&task=checkAnswers&format=raw',
				type: 'POST',
				cache: false,
				data: {	qset: '<?php echo $setq;?>' , answer : jQuery("#jform_email").val(), userId : '<?php echo $userId;?>' },
				success: function( data ) {
					if(data=="false"){
						jQuery("#wrongans").show();
						return false;
					} else {
						jQuery( "#secanswerform" ).submit();
					}	
				},
			});
					
		} else {
			return false;
		}

	});
});
</script>