<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');


?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl;?>/templates/agentbridge/plugins/fancybox/jquery.fancybox-1.3.4.css">
<script type="text/javascript">
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery(document).ready(function(){
	jQuery('input[type="submit"]').live("click", function(){

		if(!jQuery("#jform_email").val()){
			jQuery("#jform_email").addClass("glow-required");
			jQuery(".jform_email.error_msg.error1").show();
			return false;
		} else {
			if(IsEmail(jQuery("#jform_email").val())==false){
				jQuery("#jform_email").addClass("glow-required");
				jQuery(".jform_email.error_msg.error1").hide();
				jQuery(".jform_email.error_msg.error2").show();
				return false;
			}
		}

 		jQuery(".jform_email.error_msg").hide();
		jQuery("#jform_email").removeClass("glow-required");

	});
	jQuery("#fancybox-close").live("click",function(){
		window.location = "<?php echo JURI::root()?>";
	});
});
	
</script>
<script type="text/javascript">
mixpanel.track("Reset Password");
</script>
<style>
	.login-form {
	    padding: 2px;
	    float: right;
	    margin-bottom: 10px;
	}
</style>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">

<?php if(!isset($_GET['success'])){ ?>
<div  style="width:90%; margin:0 auto; padding-top:20px"><!-- start forgot password -->
	<form style="width:250px; padding:20px; background-color:#ffffff; border-radius:2px; margin:0px auto; color:#000000;position:relative" id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request&request'); ?>" method="post" class="form-validate form-horizontal">
		<a id="fancybox-close" style="display:block"></a>
		<h3 class="pw-heading">Reset Password</h3>
		<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="forms-head">
			<h2>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h2>
		</div>
		<?php endif; ?>

		<?php
			foreach ($this->form->getFieldsets() as $fieldset) : ?>
			<div>
            	<p style="font-size:14px; text-align:center">Enter your email address. An email with a reset link will be sent to this address.</p>
			</div>
			<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<div>
					<?php echo $field->input; ?> 
				</div>
			<?php endforeach; ?>
			<br/>
			
		
		<?php endforeach; ?>

		<p class="jform_email error_msg error1" style="display:none; font-size:11px"> This field is required</p> 
		<p class="jform_email error_msg error2" style="display:none; font-size:11px"> Invalid email </p>
		<?php if(isset($_GET['by']) && $_GET['by']=="usertype") {?> 
		<p class="jform_email error_msg error3" style="font-size:11px"> This account has been deactivated. Please contact support@agentbridge.com for inquiries </p> 
		<?php }?> 
		<div>
			<input name="" value="Reset Password" type="submit" style="padding-top:7px;margin-left: 11px;" class="button gradient-green submit-btn short-btn"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</form>
	
	</div>
</div>
<?php } ?>

<script>

jQuery(document).ready(function(){
	jQuery(".hpform").hide();
});

</script>