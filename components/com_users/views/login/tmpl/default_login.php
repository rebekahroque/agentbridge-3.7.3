<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');



?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl;?>/templates/agentbridge/plugins/fancybox/jquery.fancybox-1.3.4.css">
<style type="text/css">
ul.nav.nav-tabs.nav-stacked a {
  color: white;
  /* margin: 10px 3px 3px 3px; */
}

.login-form {
    /* padding-top: 12px; */
    float: none;
    margin-bottom: 0px;
}
</style>
<script type="text/javascript">
<?php if($this->username){ ?>
mixpanel.track("Weekly Email Log In");
<?php } else { ?>
mixpanel.track("Simplified Log In Page");
<?php } ?>
</script>
<script type="text/javascript">
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery(document).ready(function(){
	jQuery('button[type="submit"]').live("click", function(){

		jQuery(".jform_field1.error_msg").hide();
		jQuery(".jform_field2.error_msg.error1").hide();
		jQuery("#username").removeClass("glow-required");

		var error=0;

		if(!jQuery("#username").val()){
			jQuery("#username").addClass("glow-required");
			jQuery(".jform_field1.error_msg.error1").show();
			error++;
		} else {
			if(IsEmail(jQuery("#username").val())==false){
				jQuery("#username").addClass("glow-required");
				jQuery(".jform_field1.error_msg.error1").hide();
				jQuery(".jform_field1.error_msg.error2").show();
				error++;
			}
		}

		if(!jQuery("#password").val()){
			jQuery("#password").addClass("glow-required");
			jQuery(".jform_field2.error_msg.error1").show();
			error++;
		}

		if(error>0){
			return false;
		}
 		
	});

	jQuery("#fancybox-close").live("click",function(){
		window.location = "/";
	});
});
	
</script>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">
<div style="display:none;">
<div style="width:90%; margin:0 auto; padding-top:40px" class="login<?php echo $this->pageclass_sfx?>">

	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif; ?>

		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if (($this->params->get('login_image') != '')) :?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT')?>"/>
		<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif; ?>
</div>
<!--<div>
	<ul class="nav nav-tabs nav-stacked">
		<li>
			<a style="color:#000" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
		</li>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
		</li>
		<?php endif; ?>
	</ul>
</div>-->
</div>
</div>

<div class="overlay"></div>
<div class="overlay-container">
	<div class="form-container">
		<div class="formlogin">
			<a id="fancybox-close" style="display:block"></a>
			<form style="width:100%; background-color:#ffffff; border-radius:10px; padding:10px; margin:0px auto; color:#000000;position:relative" action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate form-horizontal well">
				<div class="login-logo"></div>
				<?php $default_cookie =  base64_decode($_COOKIE['rememberEmailsc']); ?>
				<div class="login-field"><input type="text" name="username" id="username" class="validate-password required big-field" tabindex="0" placeholder="Email" value="<?php echo $default_cookie ? $default_cookie : '' ?>"/></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field"><input type="password" name="password" id="password" value="" class="validate-password required big-field" tabindex="0" placeholder="Password" /></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field">
					<input type="checkbox" name="remember" id="remember-me-big" value="1" /><label for="remember-me-big" class="chklabel">Remember Me</label> 
					<span style="font-size:16px;">&nbsp;&#0183;&nbsp;</span><span><a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">Forgot Password?</a></span>
				</div>
				<div class="login-field" style="padding-bottom: 20px">
					<button type="submit" class="button submit-btn big-submit">Log in</button>
				</div>
				<input type="hidden" name="return" value="<?php echo $_GET['returnurl']; ?>"/>
				<input type="hidden" name="h321" value="<?php echo $_GET['h321']; ?>"/>
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<div class="bottom-signup">Don't have an account? <a href="<?php echo JRoute::_('index.php?option=com_nrds&task=manual'); ?>">Sign Up</a></div>
		</div>
	</div>
</div>

<script>

jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	<?php //  var_dump($this->username);?>

	<?php if($this->username){?>
		jQuery("#username").val("<?php echo $this->user_email; ?>");
		jQuery("#username").hide();
		jQuery("#username").after("<div id='def_user'><?php echo $this->username; ?> <span id='close_def' >x</span></div>");
	<?php } ?>

	jQuery("#close_def").attr("onmouseover","ddrivetip('Not you?');hover_dd()");
	jQuery("#close_def").attr("onmouseout","hideddrivetip();hover_dd()");

	jQuery("#close_def").live("click", function(){
		jQuery("#def_user").remove();
		jQuery("#username").val("");
		jQuery("#username").show();
	});


});

</script>
<style>
	.overlay {
		width:100%;
		height:100%;
		z-index:2000;
		opacity: 0.4;
		filter: alpha(opacity=40); /* msie */
		background-color: #000;
		position:fixed;
		top:0;
		left:0;
	}
	
	.overlay-container {
		display:table;
		position:fixed;
		width:100%;
		height:100%;
		z-index:2001;
		top:0;
	}
	
	.form-container {
		display:table-cell;
		vertical-align:middle;
	}
	
	.formlogin {
		margin: 0 auto;
		display:table;
		width:500px;
		height:370px;
		position:relative;
		background:#fff;
		border-radius:10px;
		box-sizing:border-box;
	}
	
	.login-logo {
		background:url("/templates/agentbridge/images/main-logo.png") no-repeat;
		width:208px;
		height:49px;
		display:table;
		margin:0 auto 20px;
	}
	
	.login-field {
		width:400px;
		display:table;
		margin:10px auto;
	}
	
	.big-field {
		width:100%;
		height:40px;
		border: 2px solid #ccc;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	.big-submit {
		width:100%;
		height:40px;
		font-size:16px;
		color:#fff;
		background:#339900;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	#remember-me-big {
		border:2px solid #ccc;
		width:15px;
		height:15px;
	}
	
	.chklabel {
		margin:0 0 0 5px;
		font-size:16px;
	}
	
	.bottom-signup {
		position:absolute;
		border-top:1px solid #ccc;
		text-align:right;
		padding:15px;
		font-size:16px;
		box-sizing:border-box;
		width:100%;
		bottom:0;
	}
	
	.bottom-signup a {
		font-size:16px;
	}
	
	.formlogin p {
		font-size:16px;width:400px;margin:0 auto;
	}
	
	.login-field a {
		color:#007bae;font-size:16px;
	}
	
	@media screen and (max-width: 500px) {
		.formlogin {
			margin: 0 auto;
			display:table;
			width:300px;
			height:330px;
			position:relative;
			background:#fff;
			border-radius:10px;
			box-sizing:border-box;
		}
		
		.login-logo {
			background:url("/templates/agentbridge/images/main-logo.png") no-repeat;
			width:208px;
			height:49px;
			display:table;
			margin:0 auto 20px;
		}
		
		.login-field {
			width:250px;
			display:table;
			margin:10px auto;
		}
		
		.big-field {
			width:100%;
			height:30px;
			border: 2px solid #ccc;
			border-radius:5px;
			padding:10px;
			box-sizing:border-box;
		}
		
		.big-submit {
			width:100%;
			height:30px;
			font-size:16px;
			color:#fff;
			background:#339900;
			border-radius:5px;
			padding:5px;
			box-sizing:border-box;
		}
		
		#remember-me-big {
			border:2px solid #ccc;
			width:15px;
			height:15px;
		}
		
		.chklabel {
			margin:0 0 0 5px;
			font-size:14px;
		}
		
		.bottom-signup {
			position:absolute;
			border-top:1px solid #ccc;
			text-align:right;
			padding:15px;
			font-size:14px;
			box-sizing:border-box;
			width:100%;
			bottom:0;
		}
		
		.bottom-signup a {
			font-size:14px;  
		}
		
		.formlogin p {
			width:250px;
			font-size:14px;
		}
		
		.login-field a {
			color:#007bae;font-size:14px;
		}
	}
</style>
