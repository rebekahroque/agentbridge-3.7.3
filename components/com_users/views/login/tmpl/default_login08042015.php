<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');



?>
<style type="text/css">
ul.nav.nav-tabs.nav-stacked a {
  color: white;
  /* margin: 10px 3px 3px 3px; */
}
</style>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">
<div style="width:90%; margin:0 auto; padding-top:40px" class="login<?php echo $this->pageclass_sfx?>">

	<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	</div>
	<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif; ?>

		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if (($this->params->get('login_image') != '')) :?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT')?>"/>
		<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif; ?>

	<form style="width:250px; background-color:#ffffff; border-radius:2px; margin:0px auto; color:#000000" action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate form-horizontal well">
		<h3 class="pw-heading">Log In</h3>
		<fieldset>
			<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
				<?php if (!$field->hidden) : ?>

					<div class="control-group" style="margin-bottom:10px">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
						</div>
						
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
			
			<?php if ($this->tfa): ?>
				<div class="control-group">
					<div class="control-label">
						<?php echo $this->form->getField('secretkey')->label; ?>
					</div>
					<div class="controls">
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
				</div>
			<?php endif; ?>

			<!--<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			<div  class="control-group">
				<div class="control-label"><label><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label></div>
				<div class="controls"><input id="remember" type="checkbox" name="remember" class="inputbox" value="yes"/></div>
			</div>
			<?php endif; ?>-->
			
			<span><a style="color:#007bae;font-size:11px; " href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a></span>
			<div class="control-group" style="margin-top:10px">
				<div class="controls" style="padding-bottom: 20px">
					<button type="submit" style="padding-top:7px; width:225px" class="button gradient-green submit-btn">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>
			</div>

			<!--<input type="hidden" name="returnurl" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />-->
			<input type="hidden" name="returnurl" value="<?php echo $_GET['returnurl']; ?>"/>
			<input type="hidden" name="h321" value="<?php echo $_GET['h321']; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</form>
</div>
<!--<div>
	<ul class="nav nav-tabs nav-stacked">
		<li>
			<a style="color:#000" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
		</li>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
		</li>
		<?php endif; ?>
	</ul>
</div>-->
</div>

<script>

jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	<?php //  var_dump($this->username);?>

	<?php if($this->username){?>
		jQuery("#username").val("<?php echo $this->user_email; ?>");
		jQuery("#username").hide();
		jQuery("#username").after("<div id='def_user'><?php echo $this->username; ?> <span id='close_def' >x</span></div>");
	<?php } ?>

	jQuery("#close_def").attr("onmouseover","ddrivetip('Not you?');hover_dd()");
	jQuery("#close_def").attr("onmouseout","hideddrivetip();hover_dd()");

	jQuery("#close_def").live("click", function(){
		jQuery("#def_user").remove();
		jQuery("#username").val("");
		jQuery("#username").show();
	});


});

</script>
