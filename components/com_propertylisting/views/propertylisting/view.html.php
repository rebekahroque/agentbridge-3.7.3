<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class PropertyListingViewPropertyListing extends JViewLegacy
{
    function display($tpl = null)
    {				
		parent::display($tpl);
    }
}

?>