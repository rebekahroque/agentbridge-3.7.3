<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.formvalidation');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');

$application = JFactory::getApplication();
$db = JFactory::getDbo();
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
$user =& JFactory::getUser($_SESSION['user_id']);

function getCountryData($countryId){


		$db = JFactory::getDbo();

		//GET COUNTRY

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__countries');

		$query->where('countries_id = '.$countryId);

		$db->setQuery($query);

		$country_data = $db->loadObjectList();

		return 	$country_data;


}

?>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.css" type="text/css" media="screen" charset="utf-8"/>


<script>
	var ptype = 0;
	var stype = 0;
	function redirect(){
		window.location = "<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyer'); ?>";
	}
	
	var hidden_fields;
	function set_selects(v){
		var ptype_s = jQuery("#jform_ptype").val();
		var stype_s = jQuery("#jform_stype").val();
		var stype_v=v;
		switch(ptype_s){
			case "1":
				if(stype_s==1){
					hidden_fields = ['jform_view','jform_lotsqft','jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_ceiling'];
				} else if(stype_s==2){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_parking','jform_view','jform_bldgtype'];
				} else if(stype_s==3){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_yearbuilt','jform_parking','jform_view', 'jform_bldgtype'];
				} else {
					hidden_fields = ['jform_features1','jform_features2','jform_view'];
				}
				break;
			case "2":
				if(stype_s==5){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_view'];
				} else if(stype_s==6){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype'];
				} else if(stype_s==7){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype','jform_yearbuilt'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "3":
				if(stype_s==9){
					hidden_fields = ['jform_features1','jform_features2','jform_style','jform_yearbuilt','jform_occupancy','jform_view','jform_lotsqft', 'jform_grm'];
				} else if(stype_s==10){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking'];
				} else if(stype_s==11){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_ceiling','jform_stories'];
				} else if(stype_s==12){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_stories'];
				} else if(stype_s==13){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_stories'];
				} else if(stype_s==14){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_yearbuilt','jform_stories'];
				} else {
					hidden_fields = [];
				}
				break;
			case "4":
				if(stype_s==16){
					hidden_fields = ['jform_typelease','jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking'];
				} else if(stype_s==17){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_ceiling','jform_stories'];
				} else {
					hidden_fields = ['jform_typelease','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_stories'];
				}
				break;
				
			default:
				hidden_fields = ['jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_possesion','jform_ceiling','jform_stories','jform_occupancy','jform_parking', 'jform_bldgtype','jform_view'];

		}

		load_form(stype_v);
	}
	
	function get_pet (val){			
			if (jQuery("#jform_pet").val()==0) {
				jQuery("#pyes").removeClass("gradient-blue-toggle").removeClass("yes");
				jQuery("#pyes").addClass("gradient-gray").addClass("no");
				jQuery("#pno").removeClass("gradient-gray").removeClass("no");
				jQuery("#pno").addClass("gradient-blue-toggle").addClass("yes");
			} else {
				jQuery("#pno").removeClass("gradient-blue-toggle").removeClass("yes");
				jQuery("#pno").addClass("gradient-gray").addClass("no");
				jQuery("#pyes").removeClass("gradient-gray").removeClass("no");
				jQuery("#pyes").addClass("gradient-blue-toggle").addClass("yes");
			}
		}
		
		function get_furnished (val){			
			if (jQuery("#jform_furnished").val()==0) {
				jQuery("#fyes").removeClass("gradient-blue-toggle").removeClass("yes");
				jQuery("#fyes").addClass("gradient-gray").addClass("no");
				jQuery("#fno").removeClass("gradient-gray").removeClass("no");
				jQuery("#fno").addClass("gradient-blue-toggle").addClass("yes");
			} else {
				jQuery("#fno").removeClass("gradient-blue-toggle").removeClass("no");
				jQuery("#fno").addClass("gradient-gray").addClass("no");
				jQuery("#fyes").removeClass("gradient-gray").removeClass("no");
				jQuery("#fyes").addClass("gradient-blue-toggle").addClass("yes");
			}
		}
	
	
	function setButtons(){
		jQuery(".pet").click(function(){
				jQuery(".pet").removeClass("yes");
				jQuery(".pet").removeClass("gradient-blue-toggle");
				jQuery(".pet").removeClass("gradient-gray");
				jQuery(".pet").addClass("no");
				jQuery(".pet").addClass("gradient-gray");

				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");

				jQuery("#jform_pet").val(jQuery(this).attr("rel"));
			})

			jQuery(".furnished").click(function(){
				jQuery(".furnished").removeClass("yes");
				jQuery(".furnished").removeClass("gradient-blue-toggle");
				jQuery(".furnished").removeClass("gradient-gray");
				jQuery(".furnished").addClass("no");
				jQuery(".furnished").addClass("gradient-gray");

				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");

				jQuery("#jform_furnished").val(jQuery(this).attr("rel"));
			})
	}

	<?php
		$buyerzip = explode(",",$this->buyer[0]->needs[0]->zip);
		//$buyerzip = implode(",", $buyerzip);

		echo "var zips_array = ".json_encode($buyerzip).";";
		echo "var zip_count = ".count($buyerzip).";";
	?>

	function removeItem(id){
		jQuery("#jform_zip").val("");
		jQuery("#"+id).remove();

		if(id.indexOf("-")){
			id = id.replace(/-/g, ' ');
		}

		var index = zips_array.indexOf(id);

		if(index>=0){

		   zips_array.splice(index, 1);
		   zip_count--;
		}
	}

	function logArrayElements(element, index, array) {
	  console.log('a[' + index + '] = ' + element);
	  jQuery("#zips_list").append("<div id='"+element.replace(/\s/g , "-")+"' class='textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable' style='float:none;display: inline-block;'>"+element+'<a href="javascript:void(0)" onclick="removeItem(\''+element.replace(/\s/g , "-")+'\')" class="textboxlist-bit-box-deletebutton"></a></div>');		
	}

	jQuery(document).ready(function(){

	//	zips_array.forEach(logArrayElements);

		var clicked=0;
		var invalid;
		var baseurl = "<?php echo $this->baseurl?>";

		if (jQuery(window).width() <= 600) {
			jQuery("#jform_propertyname").removeAttr('onmouseover');
			jQuery("#p_range").removeAttr('onmouseover');
			jQuery("#jform_price1").removeAttr('onmouseover');
			jQuery("#jform_price2").removeAttr('onmouseover');
			jQuery("#p_rangedesc").removeAttr('onmouseover');
			jQuery("#b_notes").removeAttr('onmouseover');
			jQuery("#b_status").removeAttr('onmouseover');
			jQuery("#formdiv").removeAttr('onmouseover');			
			}
		
		jQuery("#add_zips").live("click",function(){
			invalid=0;
			jQuery(".error_msg.invalid_zip").hide();
			clickZipValid(baseurl);			
		});



	    <?php
			$buyerzip = explode(",",$this->buyer[0]->needs[0]->zip);
		//$buyerzip = implode(",", $buyerzip);
			echo "var zips_array_init = ".json_encode($buyerzip).";";

		?>
		
		applyAddressDataAB(baseurl,"<?php echo $this->getCountryLangsInitial->countries_iso_code_2?>","<?php echo $this->getCountryLangsInitial->countries_id?>",zips_array_init);

		jQuery("#jform_zip").blur(function(){

		  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

		});

		jQuery("#formdiv").hide();
		jQuery(".hide").fadeOut();
		jQuery(".disclose").click(function(){
			jQuery(".disclose").removeClass("yes");
			jQuery(".disclose").removeClass("gradient-blue-toggle");
			jQuery(".disclose").removeClass("gradient-gray");
			jQuery(".disclose").addClass("no");
			jQuery(".disclose").addClass("gradient-gray");

			jQuery(this).removeClass("gradient-gray");
			jQuery(this).removeClass("no");
			jQuery(this).addClass("yes");
			jQuery(this).addClass("gradient-blue-toggle");

			jQuery("#jform_disclose").val(jQuery(this).attr("rel"));
		})
		

		<?php 
		
			if($this->buyer[0]->needs[0]->country=="223"){ ?>
				jQuery("#jform_zip").keyup(function() {
					jQuery("#jform_zip").val(this.value.match(/[0-9]*/));
				});
				jQuery("#jform_zip").attr("maxlength","5");
		<?php } else if($this->buyer[0]->needs[0]->country=="38") {?>

				jQuery("#jform_zip").mask('a9a 9a9');

				jQuery("#jform_zip").blur(function(){

				  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

				});
				
				
				jQuery("#jform_zip").attr("maxlength","7");
		<?php } ?>


		jQuery("#jform_ptype").change();
		jQuery("#jform_pet").change();
		jQuery("#jform_furnished").change();

	
		jQuery('#clickchangecurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[3].replace("cho_","")+this_class[2]+". Revert to "+this_class[1].replace("def_","")+this_class[0]+".</a>");
			jQuery("#jform_price1").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
			jQuery("#jform_price2").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);

			jQuery("#jform_currency").val(this_class[2]);

			jQuery(this).attr('id','clickrevertcurrency');

			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			});


		});

		jQuery('#clickrevertcurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[1].replace("def_","")+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].replace("cho_","")+this_class[2]+"</span>.</a></a>");
			jQuery("#jform_price1").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);
			jQuery("#jform_price2").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);

			jQuery("#jform_currency").val(this_class[0]);

			jQuery(this).attr('id','clickchangecurrency');

			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			});
		});		

		jQuery('#submitformbuttonx').click(function(){
			var zips = zips_array.join(",");
			if (jQuery(".glow-required").length){
			    // Do something if class exists
			} else {
			    jQuery("#jform_zip").inputmask("remove");
			 	var zips = zips_array.join(",");
			 	jQuery("#jform_zip").val(zips);
			} 
			jQuery("#jform_price1").val(jQuery("#jform_price1").autoNumeric('get'));
			jQuery("#jform_price2").val(jQuery("#jform_price2").autoNumeric('get'));

			 
			
		});
	});

	function get_sub(f){
		jQuery("#loading-image_custom_question").show();
		ptype=f;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=subtype',
			type: 'get',
			data: { 'ptype': f },
			success: function(msg){
				jQuery("#loading-image_custom_question").hide();
				var selectedsubtype = "<?php $sub_type = $this->buyer[0]->needs[0]->sub_type; echo $sub_type;?>";
				console.log(selectedsubtype);
				var html="<option value=\"\">--- Select ---</option>";
				var x = $nocon.parseJSON(msg);
				for(var i=0; i<x.length; i++){
					if(x[i].sub_id == selectedsubtype){
						html+="<option selected='selected' value=\""+x[i].sub_id+"\">"+x[i].name+"</option>";
						console.log("generated - "+x[i].sub_id+" selectedsubtype - "+selectedsubtype);
					} else {
						html+="<option value=\""+x[i].sub_id+"\">"+x[i].name+"</option>";
					}					
				}
				console.log('<?php echo $sub_type; ?>');
				jQuery("#jform_stype").html(html).promise().done(function(){
					jQuery("#jform_stype").val(<?php echo $sub_type; ?>);
					jQuery("#jform_stype").change();
				});
			}
		});
	}

	function load_form(v){
		jQuery("#loading-image_custom_question").show();
		stype=v;
		<?php
		$js_array = json_encode($this->buyer);
		echo "var buyer_array = ". $js_array . ";\n";
		?>
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=form&country=<?php echo $this->getCountryLangsInitial->country?>',
			type: 'POST',
			data: { 'ptype': ptype, 'stype': stype,'buyerdata':buyer_array },
			success: function(msg){
				jQuery("#loading-image_custom_question").hide();
				var temp = $nocon.trim(msg.replace('\n',''));
				temp = temp.replace('<div class="pocket-form">', '');
				temp = jQuery('<div/>').html(temp);
				if($nocon.trim(temp)!="&lt;/div&gt;"){
					jQuery("#formdiv").html(msg);
					jQuery("#formdiv").show();
					jQuery("#buyer_features").show();
					if (jQuery(window).width() <= 600) {
								jQuery("#buyer_features").removeAttr('onmouseover');	
					}
					jQuery("#formdiv").css('border-bottom', '1px solid #dedede');
					jQuery("section.main-content").css('min-height', '700px');
					jQuery("section.main-content").height(jQuery(document).height()-jQuery("section.header").height()-jQuery("section.footer").height());
					setButtons();
					//get_pet();
					//get_furnished();
					if(array!=null)
						jQuery(formnames).each(function(index){
							if(formnames[index] != "ptype" && formnames[index] != "stype") {
								jQuery("select[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
							}
							
							if(formnames[index] == "pet") {
								jQuery("input[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
								get_pet();
							}
							if(formnames[index] == "furnished") {
								jQuery("input[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
								get_furnished();
							}
						})	
					jQuery(jQuery("#formdiv").children()[0]).append('<a id="showhidefield" href="javascript:void(0)" onclick="toggleHiddenFields()"><?php echo JText::_('COM_BUYERS_MORE_FIELDS') ?></a>');
					jQuery('#jform_features1').change(function(){
						jQuery('#jform_features2 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
						jQuery('#jform_features3 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
					});

					jQuery('#jform_features2').change(function(){
						jQuery('#jform_features1 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
						jQuery('#jform_features3 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
					});

					jQuery('#jform_features3').change(function(){
						jQuery('#jform_features1 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
						jQuery('#jform_features2 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
					});
					
					var ptype_s = jQuery("#jform_ptype").val();
					var stype_s = jQuery("#jform_stype").val();
					var stype_v=v;
					switch(ptype_s){
					case "4":
					  if(stype_s==16){
						  jQuery("#jform_typelease").removeClass("reqfield")
						} else if (stype_s==17) {
						  jQuery("#jform_bldgsqft").removeClass("reqfield")
						} else if (stype_s==18) {
						  jQuery("#jform_typelease").removeClass("reqfield")
						} else {
						}
					break; }
					
					jQuery("#jform_view").removeClass("reqfield");
					
					jQuery("#jform_grm").removeClass("reqfield");
					
					jQuery("#jform_lotsqft").removeClass("reqfield");
					
					jQuery('select').select2().promise().done(function(){
						jQuery(hidden_fields).each(function(index){ 
						jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');
					})});
				}
			}
		});
	}

	function toggleHiddenFields(){
		if(jQuery('#showhidefield').html()=='<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_LESS_FIELDS') ?>')
		else
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			console.log(jQuery('#showhidefield').html()=='<?php echo JText::_('COM_POPS_TERMS_LESS_FIELDS') ?>'+"|"+jQuery('#showhidefield').html())
		jQuery(hidden_fields).each(function(index){
			jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');
		});
		
	}
	
</script>
<link
	rel="stylesheet" type="text/css"
	href="<?php echo $this->baseurl ?>/templates/agentbridge/css/styles.css" />
<style>
body {
	background: #fff;
}
</style>
<script type="text/javascript">
	var $nocon = jQuery.noConflict();
	<?php $timestamp = time();?>

	function validateFormLocal(){
		if(jQuery('.add-photo').length>1)
			return true;
		else{
			document.getElementById('vpb_upload_button').scrollIntoView();
			jQuery('.vpb_main_demo_wrapper').show()
			jQuery('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Please upload an image</div>');
			return false;
		}
	}
</script>
<!-- start main content -->
<div class="wrapper">
	<div class="wide left buyersform">
		<?php #echo "<pre>"; print_r($this->buyer); die();?>
		<!-- start wide-content -->
		<div id="pocketform">
			<h1><?php echo JText::_('COM_SIDEBAR_BUY') ?></h1>
<form id="pocket-listingx"
		 class="oooooform-validateooooo form-horizontal"
		 enctype="multipart/form-data" >
				<input  id="buyer_form_type" type="hidden" name="jform[form]" value="edit_buyer" />
				<input type="hidden" name="jform[bID]" value="<?php echo $this->buyer[0]->buyer_id; ?>" />
				<div class="pocket-form buyersnamesection" >
					<div class="left" >
						<div>
							<img class="left" src="<?php echo $this->baseurl ?>/images/security-icon.png" /> 
							<h2><?php echo JText::_('COM_BUYERS_NAME') ?> <span style="font-size:12px; font-weight:normal"><?php echo JText::_('COM_BUYERS_FOR_YOUR_EYES_ONLY') ?></span></h2>
						</div>
						<div class="left c200 buyernameinput" >
							<input id="jform_propertyname" value="<?php echo stripslashes_all(htmlspecialchars($this->buyer[0]->name)); ?>" name="jform[buyer_name]"  
                            onMouseover="ddrivetip('For your use only. Other agents do not see buyer name');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" class="text-input-buyername" type="text" />
						</div>
						<div class="clear-float"></div>
					</div>

					<div class="buyerstatus">
						<h2><?php echo JText::_('COM_BUYERS_STATUS') ?></h2>
						<div class="left c200" id="b_status"
                        onMouseover="ddrivetip('Choose buyer status. To remove buyer from matches, select Inactive.');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()" >
							<select id="buyer_type" name="jform[buyer_type]" style="width: 70%">
								<option value="A Buyer" <?php echo (!empty($this->buyer[0]->buyer_type) && strtolower($this->buyer[0]->buyer_type)=="a buyer")? "selected":""; ?>><?php echo JText::_('COM_BUYERS_STATUS_OPT_ABUYER') ?></option>
								<option value="B Buyer" <?php echo (!empty($this->buyer[0]->buyer_type) && strtolower($this->buyer[0]->buyer_type)=="b buyer")? "selected":""; ?>><?php echo JText::_('COM_BUYERS_STATUS_OPT_BBUYER') ?></option>
								<option value="C Buyer" <?php echo (!empty($this->buyer[0]->buyer_type) && strtolower($this->buyer[0]->buyer_type)=="c buyer")? "selected":""; ?>><?php echo JText::_('COM_BUYERS_STATUS_OPT_CBUYER') ?></option>
								<option value="Inactive" <?php echo (!empty($this->buyer[0]->buyer_type) && strtolower($this->buyer[0]->buyer_type)=="inactive")? "selected":""; ?>><?php echo JText::_('COM_BUYERS_STATUS_OPT_INACTIVE') ?></option>
							</select>
						</div>
						<div class="clear-float"></div>
					</div>
					<div class="clear-float"></div>
				</div>
				<div>
					<p class="jform_propertyname error_msg" style="margin-top:-0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
				</div>
				<div class="pocket-form buyerzipsection" >
					<h2><?php echo JText::_('COM_BUYERS_LOOKING_FOR') ?></h2>
					<?php $countryData = getCountryData($this->buyer[0]->needs[0]->country);?>
					<div style="font-size: 13px;margin-bottom: 15px;"> <?php echo JText::_('COM_BUYERS_SET_TO_COUNTRY') ?> <?php echo $countryData[0]->countries_iso_code_2;?>. </div>					
					<div id="zips_list" style="  font-size: 12px;text-decoration: none;color: #006699;margin-bottom: 10px; "><span style="color:black;margin-right: 5px;"></span><img id="loading-image_custom_question_0" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:10px;" /></div>					
					<div class="left" >
						<?php
							$buyerzip = explode(",",$this->buyer[0]->needs[0]->zip);
							$buyerzip = implode(", ", $buyerzip);
						?>
						<input 
							style="width:110px;margin-right: 10px;" 
							id="jform_zip" 
							name="jform[zip]"
							class="text-input" 
                            onMouseover="ddrivetip('Enter desired zip code(s). Separate multiple zip codes with commas');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							type="text"
							value="" />
							<span id="add_zips" class="button gradient-blue" style="font-size: 13px;text-decoration: none;cursor:pointer;padding:5px;">+ <?php echo JText::_('COM_BUYERS_ADD_ZIP_CODE') ?></span>
							<p class="jform_zip error_msg invalid_zip" style="margin-left:0px;display:none"><span style="display: inline-block;margin-top: 5px;"><?php echo JText::_('COM_BUYERS_ZIP_ERROR2') ?></span> <br /></p> 
							<p class="jform_zip error_msg" style="margin-left:0px;display:none"> <br /> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
							<div>
								
								<p class="jform_zip_2 error_msg" style="margin-top:10px;margin-left:2px;display:none; line-height:14px"> <?php echo JText::_('COM_BUYERS_ZIP_ERROR') ?> <br /></p>
							</div>
					</div>
					<div class="clear-float" style="margin-top:10px"></div>
					<div class="left c200">
						<?php $property_type = $this->buyer[0]->needs[0]->property_type; ?>
						<select style="width:195px" id="jform_ptype" name="jform[ptype]" onchange="get_sub(this.value)">
							<option value=""><?php echo JText::_('COM_POPS_TERMS_POP_TYPE') ?></option>							
							<?php
							foreach($this->ptype as $ptype){
								if($ptype->type_id == $property_type){
									$selected = "selected='selected'";								
								} else {
									$selected = "";
								}
								echo "<option value=\"".$ptype->type_id."\" ". $selected .">".JText::_($ptype->type_name)."</option>";
							}
							?>
						</select>
						<div>
							<p class="jform_ptype error_msg" style="margin-top:-4px;margin-left:0px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /><br /></p> 
						</div>
					</div>
					<div class="left c200">
						<div id="subtype">
							<?php $sub_type = $this->buyer[0]->needs[0]->sub_type; ?>
							<select id="jform_stype" name="jform[stype]"
								onchange="set_selects(this.value);">
								<option value=""><?php echo JText::_('COM_BUYERS_POP_SUBTYPE') ?></option>
								<?php
								foreach($this->sub_types as $stype){
									if($stype->sub_id == $sub_type){
											$selected = "selected='selected'";
									} else {
										$selected = "";
									}
									echo "<option value=\"".$stype->sub_id."\" ".$selected.">".JText::_($stype->name)."</option>";
								}
								?>
							</select>
                            <img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
							<div>
								<p class="jform_stype error_msg" style="margin-top:-4px;margin-left:0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
							</div>							
						</div>
					</div>
					<div class="clear-float"></div>
				</div>

				<div class="pocket-form clear-float buyerpricesection">
					<h2><?php echo JText::_('COM_BUYERS_PROPERTY_PRICE') ?> <!--<br/><span style="font-size:12px; font-weight:100">Enter monthly price for Lease or price per sq. ft. for Purchases</span>--></h2>
					<div class="rangeError" style='font-size:10px;margin-top:-10px;margin-left:160px;margin-bottom:15px;font-size:10px;display:none' ></div>
                    <?php 

                    if($countryData[0]->countries_id!=$this->country[0]->countries_id){ ?>
						<?php 
						$currency_def = $this->buyer[0]->needs[0]->currency;
						$currency_sym = $this->buyer[0]->needs[0]->symbol;
						$option_def = $this->user_info->currency;
						$option_sym = $this->user_info->symbol;

					?>


						<?php if($this->user_info->country!=$this->buyer[0]->needs[0]->country){?>
							<?php if($currency_def==$option_def){ ?>
								<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?> <?php echo $this->currency_data->currency;?> cho_<?php echo $this->currency_data->symbol;?>" style="font-size: 13px;margin-bottom: 25px;cursor: pointer; "><a href='#' onclick='return false;'> <?php echo JText::_('COM_BUYERS_SET_TO_CURRENCY') ?> <?php echo $currency_sym; ?><?php echo $currency_def; ?>. <?php echo JText::_('COM_BUYERS_CHANGE_TO_CURRENCY') ?> <span id='chosenCurr'><?php echo $this->currency_data->symbol;?><?php echo $this->currency_data->currency;?></span>.</a></div>						
							<?php } else { ?>
								<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?> <?php echo $option_def; ?> cho_<?php echo $option_sym; ?>" style="font-size: 13px;margin-bottom: 25px;cursor: pointer;"><a href="#" onclick="return false;"> <?php echo JText::_('COM_BUYERS_SET_TO_CURRENCY') ?> <?php echo $currency_sym; ?><?php echo $currency_def; ?>. <?php echo JText::_('COM_BUYERS_CHANGE_TO_CURRENCY') ?> <span id="chosenCurr"><?php echo $option_sym;?><?php echo $option_def;?></span>.</a> </div>		
							<?php } ?>
						<?php } ?>
					<?php } ?>

                    <div class="left c200 buyerprange" id="p_range" onMouseover="ddrivetip('Choose price range for buyer');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
						<input type="hidden" onchange="select_price_type(this.value)" value="1" style="width: 150px" id="jform_pricerange" name="jform[pricerange]"/>
						<div class="select-disabled"><span style="font-size:12px; color:#939393"><?php echo JText::_('COM_BUYERS_PRICE_RANGE') ?></span></div>
					</div>
					<?php 
						$currentprice = $this->buyer[0]->price_value; 
						$price = explode("-", $currentprice);
					?>

					<div class="left ys buyerpinput" id="p_rangedesc" onMouseover="ddrivetip('The wider the range, the more potential for matches.');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
                   	 	<label><?php echo JText::_('COM_BUYERS_PRICE_LOW') ?></label>
						<input 
							id="jform_price1" 
							name="jform[price1]" 
							class="required text-input pricevalue numberperiodcomma"
                            onMouseover="ddrivetip('Enter minimum price for buyer');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" 
							value="<?php echo $price[0]; ?>" type="text" />
							<div>
								<p class="jform_price1 error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
							</div>
					</div>
					<div class="left ys buyerpinput">
						<label><?php echo JText::_('COM_BUYERS_PRICE_HIGH') ?></label>
                        <input 
							id="jform_price2"
							value="<?php echo $price[1]; ?>" 
							name="jform[price2]"
							class="text-input numberperiodcomma pricevalue" 
                            onMouseover="ddrivetip('Enter maximum price for buyer');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" 
							type="text" />
							<div>
								<p class="jform_price2 error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
							</div>
					</div>
					<div class="clear-float"></div>
				</div>
				<div id="formdiv" onMouseover="ddrivetip('Select minimum buyer needs')" onMouseout="hideddrivetip();hover_dd();hover_dd()"></div>
				<input type="hidden" name="" id="country" value="<?php echo $this->getCountryLangsInitial->countries_iso_code_2?>"/>
				<input type="hidden" name="" id="jform_country" value="<?php echo $this->getCountryLangsInitial->countries_id; ?>"/>
				<div class="buyersnotessection">
					<div class="c480" id="b_notes" onMouseover="ddrivetip('Your notes about your buyer. All buyer information is not shared with other agents.')" onMouseout="hideddrivetip();hover_dd()">
						<h2><?php echo JText::_('COM_BUYERS_NOTES') ?></h2>
						<textarea  id="jform_desc" name="jform[desc]"
							class="required text-input text-area" <?php echo ($this->buyer[0]->home_buyer == 1) ? "readonly" : ""; ?> ><?php echo ($this->buyer[0]->home_buyer == 1) ? JText::_('COM_BUYERS_HOMEMARKET_DESC') : trim(stripslashes_all($this->buyer[0]->desc)); ?></textarea>
					</div>
				</div>
				<div class="pocket-form">
				<input type="hidden" name="jform[currency]" id="jform_currency" value="<?php echo $this->buyer[0]->needs[0]->currency; ?>"/>
					<div id="savebuttons" style="clear: both; padding-top: 20px"
						class="<?php echo (isset($_GET['saved']))? "gray": ""?>">
										
						<input name="" id="submitformbuttonx" value="<?php echo JText::_('COM_BUYERS_SAVE') ?>" type="button"
							class="button gradient-blue validate" />

						<input name="" value="<?php echo JText::_('COM_BUYERS_CANCEL') ?>" type="button"  onClick="javascript:location.href = '<?php echo JRoute::_("index.php?option=com_userprofile&task=profile"); ?>';"  class="button gradient-gray cancel" />
                          <img id="loading-image_custom_question2" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
					</div>
					<?php echo JHtml::_('form.token');?>
					<div class="clear-float"
						style="margin-top: 20px; margin-bottom: 40px;"></div>
				</div>
			</form>
		</div>
		
	</div>
	<!-- end wide-content -->

	<?php
	include( 'includes/sidebar_left.php' );
	?>

</div>
<div id="loremipsum" style="display:none">lorem ipsum dolor ismet</div>

<div style="width:1px; height:1px overflow:hidden">
<input type="file" name="newimagefile" id="inputfile"
						style="opacity: 0" />
</div>

<script>
	function select_price_type(e){
		if(e==2){
			document.getElementById('jform_price2').disabled=true;
			jQuery("#jform_price2").hide();
		}
		else{
			document.getElementById('jform_price2').disabled=false;
			jQuery("#jform_price2").show();
		}
	}

	jQuery(".watdis").click(function(){
		console.log(jQuery(this));
	});

	jQuery(window).load(function(){
		//jQuery(hidden_fields).each(function(index){ jQuery("#"+hidden_fields[index]).toggleClass('hidden'); jQuery("#"+hidden_fields[index]).prev().toggleClass('hidden'); })
	});
	jQuery(document).ready(function(){

		jQuery(".pricevalue").autoNumeric('init', {aSign:'<?php echo $currency_sym; ?>', mDec: '0'});



	});


	var tablecols = ['units','bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','property_type','sub_type','unit_sqft','year_built','pool_spa','cap_rate','listing_class','parking_ratio','ceiling_height','room_count','type_lease','type_lease2','available_sqft','lot_sqft','lot_size','bldg_sqft','bldg_type','description'];
	var formnames = ['units','bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','ptype','stype','unitsqft','yearbuilt','poolspa','cap','class','parking','ceili','roomcount','typelease','typelease2','available','lotsqft','lotsize','bldgsqft','bldgtype','desc'];
	var array = <?php echo json_encode($this->data)?>;
</script>