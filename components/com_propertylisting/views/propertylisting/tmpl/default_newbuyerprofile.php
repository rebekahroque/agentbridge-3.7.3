<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
 
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
$user =& JFactory::getUser($_SESSION['user_id']);
?>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.css" type="text/css" media="screen" charset="utf-8"/>

<script>
	var ptype = 0;
	var stype = 0;
	function redirect(){
		window.location = "<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>";
	}
	
	var hidden_fields;
	function set_selects(v){
		var ptype_s = jQuery("#jform_ptype").val();
		var stype_s = jQuery("#jform_stype").val();
		var stype_v=v;
		switch(ptype_s){
			case "1":
				if(stype_s==1){
					hidden_fields = ['jform_view','jform_lotsqft','jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_ceiling'];
				} else if(stype_s==2){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_parking','jform_view','jform_bldgtype'];
				} else if(stype_s==3){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_yearbuilt','jform_parking','jform_view', 'jform_bldgtype'];
				} else {
					hidden_fields = ['jform_features1','jform_features2','jform_view'];
				}
				break;
			case "2":
				if(stype_s==5){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_view'];
				} else if(stype_s==6){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype'];
				} else if(stype_s==7){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype','jform_yearbuilt'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "3":
				if(stype_s==9){
					hidden_fields = ['jform_features1','jform_features2','jform_style','jform_yearbuilt','jform_occupancy','jform_view','jform_lotsqft', 'jform_grm'];
				} else if(stype_s==10){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking'];
				} else if(stype_s==11){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_ceiling','jform_stories'];
				} else if(stype_s==12){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_stories'];
				} else if(stype_s==13){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_stories'];
				} else if(stype_s==14){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_yearbuilt','jform_stories'];
				} else {
					hidden_fields = [];
				}
				break;
			case "4":
				if(stype_s==16){
					hidden_fields = ['jform_typelease','jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking'];
				} else if(stype_s==17){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_ceiling','jform_stories'];
				} else {
					hidden_fields = ['jform_typelease','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_stories'];
				}
				break;
				
		}
		load_form(stype_v);
	}
	
	function setButtons(){
		jQuery(".pet").click(function(){
				jQuery(".pet").removeClass("yes");
				jQuery(".pet").removeClass("gradient-blue-toggle");
				jQuery(".pet").removeClass("gradient-gray");
				jQuery(".pet").addClass("no");
				jQuery(".pet").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_pet").val(jQuery(this).attr("rel"));
			})
			jQuery(".furnished").click(function(){
				jQuery(".furnished").removeClass("yes");
				jQuery(".furnished").removeClass("gradient-blue-toggle");
				jQuery(".furnished").removeClass("gradient-gray");
				jQuery(".furnished").addClass("no");
				jQuery(".furnished").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_furnished").val(jQuery(this).attr("rel"));
			})
	}
	
	jQuery(document).ready(function(){
		if (jQuery(window).width() <= 600) {
			jQuery("#jform_zip").removeAttr('onmouseover');
			jQuery("#jform_propertyname").removeAttr('onmouseover');
			jQuery("#p_range").removeAttr('onmouseover');
			jQuery("#p_range2").removeAttr('onmouseover');
			jQuery("#jform_price1").removeAttr('onmouseover');
			jQuery("#jform_price2").removeAttr('onmouseover');
			jQuery("#p_rangedesc").removeAttr('onmouseover');
			jQuery("#b_notes").removeAttr('onmouseover');
			jQuery("#b_status").removeAttr('onmouseover');
			jQuery("#formdiv").removeAttr('onmouseover');			
			}
		jQuery("#formdiv").hide();
		$nocon.ajax({
			url: '<?php echo $this->baseurl; ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': <?php echo (!empty($this->countryId)) ? $this->countryId: '""'; ?>, 'state': <?php echo (!empty($this->listing[0]->state)) ? $this->listing[0]->state: '""'; ?> },
			success: function(e){
				jQuery("#stateDiv").html(e);
				jQuery("#stateDiv").select2();
			}
		});
		jQuery(".hide").fadeOut();
		jQuery("#show_address").click(function(){
			jQuery(this).toggleClass("hideChild");
			if(jQuery(this).hasClass("hideChild")){
				jQuery(".hide").fadeOut();
				jQuery(this).html('+ Add Complete Address');
			}
			else{
				jQuery(".hide").fadeIn();
				jQuery(this).html('- Add Complete Address');
			}
		});
		
		//jQuery("#jform_zip").keyup(function() {
		//	jQuery("#jform_zip").val(this.value.match(/[0-9,]*/));
		//});
		
	});
	
	function get_sub(f){
		jQuery("#loading-image_custom_question").show();
		ptype=f;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=subtype',
			type: 'get',
			data: { 'ptype': f },
			success: function(msg){
				jQuery("#subtype").show();
				jQuery("#loading-image_custom_question").hide();
				var html="<option value=\"\">--- Select ---</option>";
				var x = $nocon.parseJSON(msg);
				for(var i=0; i<x.length; i++){
					html+="<option value=\""+x[i].sub_id+"\">"+x[i].name+"</option>";
				}
				jQuery("#jform_stype").html(html).promise().done(function(){
					jQuery("#jform_stype").select2("val", "");
					jQuery("#formdiv").html("");
				});
			}
		});
	}
	function load_form(v){
		jQuery("#loading-image_custom_question_2").show();
		stype=v;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=form&country='+jQuery("#jform_country").val(),
			type: 'POST',
			data: { 'ptype': ptype, 'stype': stype, 'data': <?php echo json_encode($this->listing) ?> },
			success: function(msg){
				jQuery("#loading-image_custom_question_2").hide();
				var temp = $nocon.trim(msg.replace('\n',''));
				temp = temp.replace('<div class="pocket-form">', '');
				temp = jQuery('<div/>').html(temp);
				if($nocon.trim(temp)!="&lt;/div&gt;"){
					jQuery("#formdiv").html(msg);
					jQuery("#formdiv").show();
					jQuery("#buyer_features").show();
					if (jQuery(window).width() <= 600) {
								jQuery("#buyer_features").removeAttr('onmouseover');	
					}
					jQuery("#formdiv").css('border-bottom', '1px solid #dedede');
					jQuery("section.main-content").css('min-height', '700px');
					jQuery("section.main-content").height(jQuery(document).height()-jQuery("section.header").height()-jQuery("section.footer").height());
					setButtons();
					if(array!=null)
						jQuery(formnames).each(function(index){
							jQuery("select[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
						})
						
					jQuery(jQuery("#formdiv").children()[0]).append('<a id="showhidefield" href="javascript:void(0)" onclick="toggleHiddenFields()"><?php echo JText::_('COM_BUYERS_MORE_FIELDS') ?></a>');
					jQuery('#jform_features1').change(function(){
						jQuery('#jform_features2 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
						jQuery('#jform_features3 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
					});
					jQuery('#jform_features2').change(function(){
						jQuery('#jform_features1 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
						jQuery('#jform_features3 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
					});
					
					var ptype_s = jQuery("#jform_ptype").val();
					var stype_s = jQuery("#jform_stype").val();
					var stype_v=v;
					switch(ptype_s){
					case "4":
					  if(stype_s==16){
						  jQuery("#jform_typelease").removeClass("reqfield")
						} else if (stype_s==17) {
						  jQuery("#jform_bldgsqft").removeClass("reqfield")
						} else if (stype_s==18) {
						  jQuery("#jform_typelease").removeClass("reqfield")
						} else {
						}
					};
					
					jQuery("#jform_view").removeClass("reqfield");
					
					jQuery("#jform_grm").removeClass("reqfield");
					
					jQuery("#jform_lotsqft").removeClass("reqfield");
					
					jQuery('#jform_features3').change(function(){
						jQuery('#jform_features1 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
						jQuery('#jform_features2 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
					});
					
					jQuery('select').select2().promise().done(function(){jQuery(hidden_fields).each(function(index){ jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');})});
				}
			}
		});
	}
	
	function toggleHiddenFields(){
		if(jQuery('#showhidefield').html()=='<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_LESS_FIELDS') ?>')
		else
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			console.log(jQuery('#showhidefield').html()=='<?php echo JText::_('COM_POPS_TERMS_LESS_FIELDS') ?>'+"|"+jQuery('#showhidefield').html())
		jQuery(hidden_fields).each(function(index){
			jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');
		});
		
	}
	
	/*function showSettings(){
		jQuery("#settingsform").dialog({
				modal: true,
				width: '600px',
				minHeight: '300px',
				open: function(){
					jQuery('input[type=\'radio\']').click(function(){
						jQuery("#submitsettings").click();
					});
				}
		});
	}*/
</script>
<style>
body {
	background: #fff;
}

</style>
<script type="text/javascript">
	var $nocon = jQuery.noConflict();
	<?php $timestamp = time();?>
	function validateFormLocal(){
		if(jQuery('.add-photo').length>1)
			return true;
		else{
			document.getElementById('vpb_upload_button').scrollIntoView();
			jQuery('.vpb_main_demo_wrapper').show()
			jQuery('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Please upload an image</div>');
			return false;
		}
	}
</script>
<!-- start main content -->
<div class="wrapper">
	<div class="wide left buyersform">
		<!-- start wide-content -->
		<div id="pocketform">
			<h1><?php echo JText::_('COM_SIDEBAR_BUY') ?></h1>
		
<form id="pocket-listingx"
		 class="oooooform-validateooooo form-horizontal"
		 enctype="multipart/form-data" >
			<?php if( !empty($_GET['buyer']) ) { ?>
				<input type="hidden" name="buyer" value="<?php echo $_GET['buyer']; ?>" />
			<?php } ?>
		 
				<input type="hidden" id="buyer_form_type" name="jform[form]" value="add_buyer" />
				<div class="pocket-form buyersnamesection" >
					<div class="left">
						<div>
							<img class="left" src="<?php echo $this->baseurl ?>/images/security-icon.png" /> 
							<h2><?php echo JText::_('COM_BUYERS_NAME') ?> <span style="font-size:12px; font-weight:normal"><?php echo JText::_('COM_BUYERS_FOR_YOUR_EYES_ONLY') ?></span></h2>
						</div>
						<div class="left c200 buyernameinput">
							<input 
								id="jform_propertyname"
								name="jform[buyer_name]"
								class="text-input"
                                onMouseover="ddrivetip('For your use only. Other agents do not see buyer name');hover_dd()"
								onMouseout="hideddrivetip();hover_dd()"
								type="text"
								placeholder="<?php echo JText::_('COM_BUYERS_NAME_PLACEHOLDER') ?>" 
							/>
						
						<div><p class="jform_propertyname error_msg" style="margin-left:0px; margin-top:10px; display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p></div>
						</div>
						<div style="clear: both"></div>
					</div>
					<div class="buyerstatus">
						<h2><?php echo JText::_('COM_BUYERS_STATUS') ?></h2>
						<div id="b_status" class="left c200" onMouseover="ddrivetip('Choose buyer status. To remove buyer from matches, select Inactive.');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
							<select id="buyer_type" name="jform[buyer_type]" style="width: 70%">
								<option value="A Buyer"><?php echo JText::_('COM_BUYERS_STATUS_OPT_ABUYER') ?></option>
								<option value="B Buyer"><?php echo JText::_('COM_BUYERS_STATUS_OPT_BBUYER') ?></option>
								<option value="C Buyer"><?php echo JText::_('COM_BUYERS_STATUS_OPT_CBUYER') ?></option>
								<option value="Inactive"><?php echo JText::_('COM_BUYERS_STATUS_OPT_INACTIVE') ?></option>
							</select>
						</div>
						<div class="clear-float"></div>
					</div>
					<div class="clear-float"></div>
				</div>
			
				<div class="pocket-form buyerzipsection" >
					<h2><?php echo JText::_('COM_BUYERS_LOOKING_FOR') ?></h2>
					<div id="zips_list" style="  font-size: 12px;text-decoration: none;color: #006699;margin-bottom: 10px;position:relative "><span style="color:black;margin-right: 5px;"></span><img id="loading-image_custom_question_0" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:10px;" /></div>
					<div class="left">
						<input 
							id="jform_zip" 
							style="width:110px;margin-right: 10px;" 
							name="jform[zip]"
							class="text-input" 
                            onMouseover="ddrivetip('Enter desired <?php echo $ziptext_php; ?> code(s). Separate multiple <?php echo $ziptext_php; ?> codes with commas');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							type="text"
							placeholder="Postal Code" 
						/>

						<span id="add_zips" class="button gradient-blue" style="font-size: 13px;text-decoration: none;cursor:pointer;padding:5px;">+ <?php echo JText::_('COM_BUYERS_ADD_ZIP_CODE') ?> </span>
						<p class="jform_zip error_msg invalid_zip" style="margin-left:0px;display:none"><span style="display: inline-block;margin-top: 5px;"><?php echo JText::_('COM_BUYERS_ZIP_ERROR2') ?></span> <br /></p> 
						<p class="jform_zip error_msg missfield" style="margin-left:0px;display:none"> <br /> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
						<div id="clickchangecountry" class="change_country"><a href="#" onclick="return false;"> <?php echo JText::_('COM_BUYERS_CHANGE_COUNTRY') ?> </a></div>
						<div>							
							<p class="jform_zip_2 error_msg" style="margin-top:10px;margin-left:2px;display:none; line-height:14px"> <?php echo JText::_('COM_BUYERS_ZIP_ERROR') ?> <br /></p>
						</div>
					</div>
					<div class="clear-float" style="margin-top:10px"></div>
					<div class="left c200">
						<select 
							style="width:195px" 
							id="jform_ptype" 
							name="jform[ptype]" 
							onchange="get_sub(this.value)">
							<option value=""><?php echo JText::_('COM_POPS_TERMS_POP_TYPE') ?></option>
							<?php
							foreach($this->ptype as $ptype){
								if($ptype->type_id==$this->property_type) $selected = "selected='selected'";
								else $selected = "";
								echo "<option value=\"".$ptype->type_id."\" ". $selected .">".JText::_($ptype->type_name)."</option>";
								echo "<script>console.log('typeid= ".$ptype->type_id." ||| typename= ".$ptype->type_name."');</script>";
							}
							?>
						</select>
						<img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
						<div>
							<p class="jform_ptype error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /></p> 
						</div>
					</div>
					
					<div class="left c200">
						<div id="subtype" style="display:none">
							<select 	
								id="jform_stype" 
								style="min-width:160px"
								name="jform[stype]"
								onchange="set_selects(this.value)">
								<option><?php echo JText::_('COM_POPS_TERMS_POP_SUBTYPE') ?></option>
								<?php
								foreach($this->sub_types as $stype){
									if($stype->sub_id==$this->sub_type) $selected = "selected='selected'";
									else $selected = "";
									echo "<option value=\"".$stype->sub_id."\" ".$selected.">".JText::_($stype->name)."</option>";
									echo "<script>console.log('subtypeid= ".$stype->sub_id." ||| subtypename= ".$stype->name."');</script>";
								}
								?>
							</select>
                             <img id="loading-image_custom_question_2" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
							 <p class="jform_stype error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /></p> 
						</div>
					</div>
					<div class="clear-float"></div>
				</div>
				<div class="pocket-form clear-float buyerpricesection">
					<h2><?php echo JText::_('COM_BUYERS_PROPERTY_PRICE') ?></h2>
					<div class="rangeError" style='font-size:10px;margin-top:-10px;margin-left:160px;margin-bottom:15px;font-size:10px;display:none' ></div>
					<?php 
						$currency_def = "USD";
						$currency_sym = "$";
						
						if($this->country[0]->currency){
							//$curreData = getCountryCurrencyData($this->data->country);
							$currency_def = $this->country[0]->currency;
							$currency_sym = $this->country[0]->symbol;
						}

					?>
					<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?>" style="display:none;font-size: 13px;margin-bottom: 15px;cursor: pointer;"><a href="#" onclick="return false;"> <?php echo JText::_('COM_BUYERS_SET_TO_CURRENCY') ?> <?php echo $currency_sym; ?><?php echo $currency_def; ?>. <?php echo JText::_('COM_BUYERS_CHANGE_TO_CURRENCY') ?> <span id="chosenCurr"></span>.</a> </div>	
	
					<div class="left c200 buyerprange" id="p_range" onMouseover="ddrivetip('Choose price range for buyer');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
						<select 
							style="width:150px" 
							id="jform_pricerange"
							onchange="select_price_type(this.value)" 
							name="jform[pricerange]"
							class="required">
							<option value="1"
							<?php echo (($this->data->price_type==1) ? "selected" : ""); ?>><?php echo JText::_('COM_BUYERS_PRICE_RANGE') ?></option>
							<!--option value="2"
							<?php echo (($this->data->price_type==2) ? "selected" : ""); ?>>Exact</option-->
						</select>
					</div>
					<div id="p_range2" class="left ys buyerpinput" onMouseover="ddrivetip('The wider the range, the more potential for matches.');hover_dd()" onMouseout="hideddrivetip();hover_dd();hover_dd()">
						<label><?php echo JText::_('COM_BUYERS_PRICE_LOW') ?></label>
						<input 
							id="jform_price1" 
							name="jform[price1]"  
							class="required text-input pricevalue numberperiodcomma"
                            onMouseover="ddrivetip('Enter minimum price for buyer');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" 
						/>
						<div>
							<p class="jform_price1 error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
						</div>
					</div>
					<div class="left ys buyerpinput">
						<label><?php echo JText::_('COM_BUYERS_PRICE_HIGH') ?></label>
						<input 
							id="jform_price2"
							name="jform[price2]"
                            onMouseover="ddrivetip('Enter maximum price for buyer');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input numberperiodcomma pricevalue" 
							type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" />
						<div>
							<p class="jform_price2 error_msg" style="margin-top:5px;margin-left:0px;display:none"> <?php echo JText::_('COM_BUYERS_FORMERROR') ?> <br /></p> 
						</div>
					</div>
					<div class="clear-float"></div>
				</div>
				<div id="formdiv" style="display:none"></div>
				
				<div class="buyersnotessection">
					<div id="b_notes" class="c480" onMouseover="ddrivetip('Your notes about your buyer. All buyer information is not shared with other agents.');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()">
						<h2><?php echo JText::_('COM_BUYERS_NOTES') ?></h2>
						<textarea id="jform_desc" name="jform[desc]" class="required text-input text-area"><?php echo trim(stripslashes_all($this->data->description)); ?></textarea>
					</div>
				</div>
				
				<div class="pocket-form">
					<div id="savebuttons" style="clear: both; padding-top: 20px"
						class="<?php echo (isset($_GET['saved']))? "gray": ""?>">
						<input 
							name="" 
							id="submitformbuttonx" 
							value="<?php echo JText::_('COM_BUYERS_SAVE') ?>" 
							type="button"
							class="sub_button button gradient-blue validate" 
						/> 
							
						<input name="" value="<?php echo JText::_('COM_BUYERS_CANCEL') ?>" type="button"  onClick="javascript:location.href = '<?php echo JRoute::_("index.php?option=com_userprofile&task=profile"); ?>';"  class="button gradient-gray cancel" />
                         <img id="loading-image_custom_question3" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
					</div>
					<?php echo JHtml::_('form.token');?>
					<div class="clear-float"
						style="margin-top: 20px; margin-bottom: 40px;"></div>
						<input type="hidden" name="jform[currency]" id="jform_currency" value="<?php echo $this->country[0]->currency; ?>"/>
					<input type="hidden" name="jform[country]" id="jform_country" value="<?php echo $this->country[0]->country; ?>"/>
					<input type="hidden" name="" id="country" value="<?php echo $this->country[0]->countries_iso_code_2; ?>"/>
				</div>
			</form>
		</div>
		
	</div>
	<!-- end wide-content -->
	<?php
	include( 'includes/sidebar_left.php' );
	?>
</div>
<div id="loremipsum" style="display:none">lorem ipsum dolor ismet</div>
<div style="width:1px; height:1px overflow:hidden">
<input type="file" name="newimagefile" id="inputfile"
						style="opacity: 0" />
</div>
<script>
	function select_price_type(e){
		if(e==2){
			document.getElementById('jform_price2').disabled=true;
			jQuery("#jform_price2").hide();
		}
		else{
			document.getElementById('jform_price2').disabled=false;
			jQuery("#jform_price2").show();
		}
	}


	
	jQuery(window).load(function(){
	});
	jQuery("#jform_price1").focusout(function(){
		//console.log(jQuery(this).val());
		var thisprice = jQuery(this).val();
		if(thisprice != "" && jQuery("#jform_price2").val() != ""){
			if(thisprice > jQuery("#jform_price2").val()){
				console.log(jQuery(this).val());
			}
		}		
	});
	
	jQuery("#jform_price2").focusout(function(){
		var thisprice = jQuery(this).val();
		if(thisprice != ""){
			if(thisprice < jQuery("#jform_price1").val()){
				//alert('Invalid Price');
				console.log(jQuery(this).val());
			}
		}
	});

	/*function removeItem(id){

		jQuery("#jform_zip").val("");
		jQuery("#"+id).remove();

		if(id.indexOf("-")){
			id = id.replace(/-/g, ' ');
		}

		var index = zips_array.indexOf(id);

		if(index>=0){

		   zips_array.splice(index, 1);
		   zip_count--;
		}

		console.log(zip_count);
		if(zip_count==1){
			jQuery("#clickchangecountry").show();
		}		
	

	}

	var zips_array = [];
	var zip_count = 1;

			var options =  function(){ 
			var zip = jQuery("#jform_zip").val();
			if(jQuery("#country").val()=="IE"){

				jQuery.ajax({
		                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
		                success: function(data){
		                	if(data[0]){
			                	console.log(data[0]);
			                	jQuery("#jform_state option").filter(function() {

										if(this.text == data[0].county){
											jQuery("#jform_state").select2({ width: 'resolve' });
											jQuery("#jform_state").select2("val", this.value);
										}
									});				
								if(typeof data[0].posttown != 'undefined'){
									jQuery("#jform_city").val(data[0].posttown);
								} else {
									jQuery("#jform_city").val(data[0].dependentlocality);
								}
								jQuery("#s2id_state").remove();
							} else {
								invalid=1;
								jQuery("#jform_zip").val("");
							}
		                }
		        }); 


			} else {
				jQuery.ajax({
		                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+jQuery("#country").val()+'&username=damianwant33',
		                success: function(data){
		                	if(data.postalcodes[0]){
			                	if(jQuery("#country").val()=='GB'){
									jQuery("#jform_state option").filter(function() {

										if(this.text == data.postalcodes[0].adminName3){
											jQuery("#jform_state").select2({ width: 'resolve' });
											jQuery("#jform_state").select2("val", this.value);
										}
									});
			                	} else {
			                		jQuery("#jform_state").select2("val", jQuery("#zone"+data.postalcodes[0].adminCode1).val());
			                	}
						
								jQuery("#jform_city").val(data.postalcodes[0].placeName);
								jQuery("#s2id_state").remove();
							} else {
								invalid=1;
								jQuery("#jform_zip").val("");
							}
		                }
		        }); 
			}
		};*/

	jQuery(document).ready(function(){
		jQuery(".pricevalue").autoNumeric('init', {aSign:'<?php echo strtolower($this->getCountryLangsInitial->symbol); ?>', mDec: '0'});
		var baseurl = "<?php echo $this->baseurl?>";
		var clicked=0;
		//var invalid;
		
		jQuery("#add_zips").live("click",function(){
			jQuery(".error_msg.invalid_zip").hide();
			clickZipValid();
			
		});

		jQuery('#clickchangecountry').click(function(){
		
			var status = jQuery(this).attr('data');
		
			//jQuery("#changecountry").html("");
			jQuery('#changecountry').dialog(
					{
					  title: "<?php echo JText::_('COM_BUYERS_CHANGE_COUNTRY') ?>",
					  width:'auto',
					});
		//}
		});






		jQuery("#jform_zip").blur(function(){

		  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

		});
		
	//	jQuery("#jform_zip").attr("maxlength","<?php echo $this->getCountryLangsInitial->zipMaxLength; ?>");
	//	jQuery("#add_zips").html("+ Add <?php echo $this->getCountryLangsInitial->zipLabel; ?>");
	//	jQuery(".invalid_zip span").html("Invalid <?php echo $this->getCountryLangsInitial->zipLabel; ?> Entered");
	//	jQuery("#zips_list span").text("<?php echo $this->getCountryLangsInitial->zipLabel; ?>:");
	//	jQuery("#jform_zip").attr("placeholder","<?php echo $this->getCountryLangsInitial->zipLabel; ?>");
	//    jQuery(".jform_zip_2.error_msg").text("<?php echo $this->getCountryLangsInitial->zipErrorMess; ?>");
	//    jQuery("#jform_zip").attr("onMouseover","ddrivetip('<?php echo $this->getCountryLangsInitial->zipHoverMess; ?>');hover_dd()");

		jQuery(".choose-country.<?php echo $this->country[0]->country;?>").css("display","none");
		
		applyAddressDataAB(baseurl,"<?php echo $this->getCountryLangsInitial->countries_iso_code_2?>","<?php echo $this->country[0]->country;?>");

		jQuery(".choose-country").click(function(){
			jQuery(".error_msg.invalid_zip").hide();
			var this_id = jQuery(this).attr("id");
			var this_class=jQuery(this).attr('class').split(' ');
			clicked=0;
			zips_array.length = 0;

			jQuery("#clickchangecountry").html("<a href='#' onclick='return false;'> <img class='ctry-flag' style='margin-right: 6px;' src='<?php echo $this->baseurl;?>/templates/agentbridge/images/"+this_class[0].toLowerCase()+"-flag-lang.png'>Change country</a>");
			//jQuery("#country").val(this_class[1]);
			jQuery("#jform_country").val(this_id);

			if(jQuery.trim( jQuery('#formdiv').html() ).length){
				console.log(jQuery("#jform_stype").val());
				jQuery('#formdiv').html("")
				set_selects(jQuery("#jform_stype").val());
			}


			jQuery("#jform_zip").unbind("keyup");
			jQuery("#jform_zip").val("");
			jQuery('#jform_zip').unmask();

			applyAddressDataAB(baseurl,this_class[0],this_id);

			/*jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_propertylisting&task=changeCountryData&format=raw",
				type: "POST",
				data: {country:this_id},
				success: function (data){
					//jQuery("#pocketform").prepend(data);

					var datas = JSON.parse(data);

					jQuery("#jform_zip").inputmask({mask:datas.zip_format.split(',')});
					jQuery("#country").val(datas.countries_iso_code_2);
					if(datas.country==222){
						jQuery("#jform_zip").inputmask({placeholder:"","clearIncomplete": false});
					} else {
						jQuery("#jform_zip").inputmask({"clearIncomplete": true});
					}

					jQuery(".sqftbycountry").html(datas.sqftMeasurement);

					jQuery("#jform_zip").blur(function(){

					  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

					});
					
					jQuery("#jform_zip").attr("maxlength",datas.zipMaxLength);
					jQuery("#cityLabel").text(datas.cityLabel);
					jQuery("#jform_city").attr("placeholder",datas.cityLabel);
					jQuery("#stateLabel").text(datas.stateLabel);
					jQuery("#jform_zip").attr("placeholder",datas.zipLabel);
				    jQuery(".jform_zip_2.error_msg").text(datas.zipErrorMess);
				    jQuery("#jform_zip").attr("onMouseover","ddrivetip('"+datas.zipHoverMess+"');hover_dd()");
					jQuery("#add_zips").html("+ Add "+datas.zipLabel);
					jQuery(".invalid_zip span").html("Invalid "+datas.zipLabel+" Entered");
					jQuery("#zips_list span").text(datas.zipLabel+":");
					
				}

			});*/

			


			jQuery("#clickrevertcurrency").attr('id','clickchangecurrency');

			//get_state(this_id);
			if(jQuery(this).find(".this_currency").val() != "<?php echo $this->country[0]->currency;?>"){
				jQuery('#clickchangecurrency').css("display","block");

				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
				if(count_Ar.length > 2){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}


				jQuery('#clickchangecurrency').addClass(jQuery(this).find(".this_currency").val()+" cho_"+jQuery(this).find(".this_symbol").val());
				var this_class=jQuery("#clickchangecurrency").attr('class').split(' ');
				jQuery("#clickchangecurrency").html("<a href='#' onclick='return false;'> Currency is "+this_class[1].split('_')[1]+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].split('_')[1]+this_class[2]+"</span>.</a></a>");
				
				jQuery("#chosenCurr").html(jQuery(this).find(".this_symbol").val()+jQuery(this).find(".this_currency").val());
				//jQuery("#jform_price1").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");
				//jQuery("#jform_price2").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");

				jQuery("#jform_price1").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
			    jQuery("#jform_price2").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);

		
				jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				});
			
			} else {
				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');

				

				jQuery("#jform_price1").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
			    jQuery("#jform_price2").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
				jQuery(".pricevalue").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				});
				if(count_Ar.length == 4){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				} else if(count_Ar.length == 3){					
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}

				jQuery('#clickchangecurrency').css("display","none");
			}

			jQuery(this).css("display","none");

			jQuery('.choose-country').not(this).each(function(){
		         jQuery(this).css("display","block");
		     });


			jQuery(".choose-country."+jQuery(this).attr("class").split(' ')[0]).css("display","none");

			jQuery('#changecountry').dialog('close');

		});




		jQuery('#clickchangecurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[3].replace("cho_","")+this_class[2]+". Revert to "+this_class[1].replace("def_","")+this_class[0]+".</a>");
			jQuery("#jform_price1").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
			jQuery("#jform_price2").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
 
 			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			});

			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});

			jQuery("#jform_currency").val(this_class[2]);

			jQuery(this).attr('id','clickrevertcurrency');


		});

		jQuery('#clickrevertcurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[1].replace("def_","")+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].replace("cho_","")+this_class[2]+"</span>.</a></a>");
			jQuery("#jform_price1").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);
			jQuery("#jform_price2").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);

			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			});

			jQuery("#jform_currency").val(this_class[0]);

			jQuery(this).attr('id','clickchangecurrency');

			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});


		});	


		jQuery(".sub_button").live("click",function(){
			
			jQuery("#jform_price1").val(jQuery("#jform_price1").autoNumeric('get'));
			jQuery("#jform_price2").val(jQuery("#jform_price2").autoNumeric('get'));

		});


		 jQuery('.tabs .tab-links a').live('click', function(e)  {
		        var currentAttrValue = jQuery(this).attr('href');

		        // Show/Hide Tabs
		        jQuery(currentAttrValue).css("display","block");
			 	jQuery(currentAttrValue).siblings().hide();
		        // Change/remove current tab to active
		        jQuery(this).parent('li').addClass('country_activel').siblings().removeClass('country_activel');
		 
		        e.preventDefault();
		    });

		 jQuery("#submitformbuttonx").click(function(){
		 	//var zips = jQuery("#zips_list").html().split("</span>")[1];

		 	if (jQuery(".glow-required").length){
			    // Do something if class exists
			} else {
			    jQuery("#jform_zip").inputmask("remove");
			 	var zips = zips_array.join(",");
			 	jQuery("#jform_zip").val(zips);
			}  

		 });
		 
	
	});


	var tablecols = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','property_type','sub_type','unit_sqft','year_built','pool_spa','cap_rate','listing_class','parking_ratio','ceiling_height','room_count','type_lease','type_lease2','available_sqft','lot_sqft','lot_size','bldg_sqft','bldg_type','description'];
	var formnames = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','ptype','stype','unitsqft','yearbuilt','poolspa','cap','class','parking','ceili','roomcount','typelease','typelease2','available','lotsqft','lotsize','bldgsqft','bldgtype','desc'];
	var array = <?php echo json_encode($this->data)?>;
</script>

<div id="changecountry" style="display: none; padding:10px">
	<?php
		$cl_list = $this->getCountryLangs;
		$fav_countries = array(
		       					"Canada",
		       					"United States",
		       					"United Kingdom",
		       					"France",
		       					"China",
		       					"Germany",
		       					"Ireland",
		       					"Australia",
		       					"Monaco",
		       					"Switzerland",
		       					"Spain");
	?>
		<div id="country_modal_left">
	
			<?php 
			$i=0;
				foreach($cl_list as $value){	
			?>
				<?php if(in_array($value->countries_name,$fav_countries)){?>
						<div class="<?php echo $value->countries_iso_code_2;?> choose-country <?php echo $value->countries_id?>"  id="<?php echo $value->countries_id?>">
				    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
				    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">				    
		    				<img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">  		    		    
				    		<?php echo $value->countries_name;?>
		           		</div>
				<?php //unset($cl_list[$i]); 
			}  ?>	
				<?php $i++?>
			<?php } ?>
		</div>
		<div id="country_modal_right">	
			<div class="tabs">
			    <ul class="tab-links">
			        <?php 
							    $capital_letter="A";			    
							    $once=1;
							    $x=1;
			        ?>
			        <?php foreach($cl_list as $value){	?>
			        			<?php 	if($capital_letter!=(substr($value->countries_name,0,1))){
				        					$capital_letter=substr($value->countries_name, 0, 1);
				        					$once=1;
				        				} else {
				        					$once=0;
				        				}

				        				if($once==1 || $x==1) { ?>

				        			<li><a href="#country_<?php echo $capital_letter; ?>" class="country_<?php echo $capital_letter; ?>" ><?php echo $capital_letter; ?></a></li>

				        		<?php $x++; }	?>
			        <?php }?>
			    </ul>
			</div>
			 <div class="tab-content" >
			 		<?php 
						    $capital_letter="A";			    
						    $once=1;
						    $x=1;
						    $def_Active="country_active";
						    $t=1;
			        ?>
			        <?php foreach($cl_list as $key=>$value){	?>


			        <?php if($capital_letter!=(substr($value->countries_name,0,1))){ echo $t!=1 ? "</div>":"";$once=1;$t=1;}  ?>  
			        <?php $capital_letter=substr($value->countries_name, 0, 1);?>
			        <?php if($once==1){ $once=0;?>
			        	<?php echo $x==1 ? "":"</div>";?>
			        	<div id="country_<?php echo $capital_letter; ?>" class="tab <?php echo $def_Active; ?>">
			        <?php  } $def_Active=""; ?>


		
	        			<?php if($t==1 || $t==10 || $t==20){ ?>
	        					<?php echo $t==1 ? "":"</div>";?>
				        		<div style="display:inline-block;vertical-align:top;width:150px">
				        <?php   } 	?>
				        	<?php if($capital_letter==(substr($value->countries_name,0,1))){?>
			            		<div class="<?php echo $value->countries_iso_code_2;?> choose-country <?php echo $value->countries_id?>"  id="<?php echo $value->countries_id?>">
						    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
						    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">						    	
				    				<img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">  				    		     
						    		<?php echo $value->countries_name;?>
				           		</div>
			           		<?php  } ?>
			       		 <?php $t++; ?>
				    
				    <?php $x++; } ?>
			 </div>				
	</div>
</div>
<!-- Updated Dialog Country -->