<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$user =& JFactory::getUser($_SESSION['user_id']);
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
$buyerprice = "";
?>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/fancybox/jquery.fancybox-1.3.4.css"/>
<style>body{background: #fff;}
div.items-inner-container:nth-child(2) {
	position: relative;
}
div.folded h2{
	background: #390;
    background-image: radial-gradient(transparent 30%, rgba(0, 0, 0, 0));
    border: 0 solid rgba(0,0,0,0.2);
	border-radius: 2px;
    color: #fff;
    font-size: 9px;
    font-weight: normal;
    position: relative;
    text-shadow: -1px -1px 1px rgba(0,0,0,0.2);
}
div.folded:before, div.folded:after {
    bottom: 26px;
    box-shadow: 0 28px 8px rgba(0,0,0,0.5);
    content: "";
    height: 28px;
    position: absolute;
    width: 48%;
    z-index: -1;
}
div.folded:before {
    left: 2%;
    transform: rotate(-3deg);
}
div.folded:after {
    right: 2%;
    transform: rotate(3deg);
}
div.folded h2 {
    box-shadow: 0 0px 1px rgba(0,0,0,0);
    margin: 0;
    padding: 4px 4px 4px 3px;
    position: absolute;
    right: 287px;
    left: 69px;
    top: 3px;
	text-align:center;
}
div.folded h2:after {
    border-width: 3px;
    border-style: solid;
    border-color: #390 transparent transparent #390;
    bottom: -4px;
    content: "";
    position: absolute;
    right: 0px;
}
</style>
<div class="wrapper buyersindividual">
	<div class="wide left"><!-- start wide-content -->
		<h1><?php echo JText::_('COM_BUYERS_VIEW_MY') ?></h1>
		<div class="border-dot" style="padding:25px 0px">
			<div class="left" style="margin-top:-15px"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=buyers'); ?>" class="button gradient-gray return-listing" style="margin:0px"><span class="return-listing-text"><?php echo JText::_('COM_BUYERS_RETURN') ?></span></a></div>
			<div class="left" style="margin-top:-15px"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>"	class="button gradient-blue" style="margin-left: 5px; display: block; font-size: 13px; width: auto; padding-top:8px">+ <?php echo JText::_('COM_USERPROF_PROF_ADDB') ?></a></div>
		</div>
	<?php if(JFactory::getUser()->id==$this->data->user_id){?>
	<?php }else { ?>
	<?php } ?>
		<div id="top-def-individual" class="clear-float">
			<div class="large-item-details left">
			<?php ?>
				<div style="height:230px;">
					<div style="width:100%;">
						<div class="left" style="width:100%">
								<img class="resize2" src="<?php echo ($this->buyer[0]->buyer_image) ? "images/buyers/500x500/" . $this->buyer[0]->buyer_image : "https://www.agentbridge.com/images/house-234.png"; #echo trim($images[0]->image); ?>" style="width:100%;position:relative;" alt="" />
						</div>
					</div>
				</div>
			</div>
			<div class="item-details left individual-buyer-details">
				<a class="item-number" style="font-weight:bold"><?php echo stripslashes_all($this->buyer[0]->name); ?></a>
				<?php 
					$buyer_type = $this->buyer[0]->buyer_type;
					$buyertype = "";
					$buyerclass = "";
					if($buyer_type == "A Buyer"){
						$buyertype = "A";
						$buyerclass = "buyer_a";
					} else if($buyer_type == "B Buyer"){
						$buyertype = "B";
						$buyerclass = "buyer_b";
					} else if($buyer_type == "C Buyer"){
						$buyertype = "C";
						$buyerclass = "buyer_c";
					} else if($buyer_type == "Inactive"){
						$buyertype = "Inactive";
						$buyerclass = "buyer_inactive";
					}
				?>
				<div id="buyer-type" class="<?php echo $buyerclass; ?>">
					<?php echo $buyertype; ?>
				</div>
				<span class="reality-description">
					<?php 
						echo JText::_($this->buyer[0]->needs[0]->proptype_name)." - ".JText::_($this->buyer[0]->needs[0]->subtype_name);					
					?>
					<br/>
					<?php
						$buyerzip = explode(",",$this->buyer[0]->needs[0]->zip);
						$buyerzip = implode(", ", $buyerzip);
					?>
					<?php echo $buyerzip; ?>
					<br/>
					<?php echo stripslashes_all($this->buyer[0]->needs[0]->property_name); ?><br/>
				</span>
					<span class="price">
						<?php $buyerprice = explode('-', $this->buyer[0]->price_value); ?>
						<?php echo format_currency_global($buyerprice[0],$this->buyer[0]->needs[0]->symbol).((!empty($buyerprice[1])) ? ' - '.format_currency_global($buyerprice[1],$this->buyer[0]->needs[0]->symbol) : '')." ".$this->buyer[0]->needs[0]->currency; ?>
					</span>
					<div id="buyer-needs" class="buyer-needs-individual">
						<?php
							echo $this->buyer[0]->buyer_feats['initial_feats'];
							
						?>
					</div>
                 <div class="edit-buyer"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=editbuyerprofile&lID=' . $_REQUEST['lID']); ?>" class="button gradient-green edit-pocket"><em class="edit-pocket-text"><?php echo JText::_('COM_ACTIVITY_HOMEMARKETEDIT')?></em></a></div>
			</div>
			<div class="clear" style="clear:both"></div>
		</div>
			<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=setting'); ?>" class="edit-view-setting text-link <?php echo (trim($this->data->user_id) != JFactory::getUser()->id) ? "hidden": ""; ?>">Edit View Setting</a>
			<div style="clear:both"></div>
		<div id="tabs">
			<ul id="menu2" class="pops_menu"><!-- start tab -->
				<li class="ui_border" style="margin-top: 10px;">
					<a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#general-info" class="panel_info"><?php echo JText::_('COM_POPS_VIEW_GENERALINFO') ?></a>
				</li>
			</ul>
			<br/>
			<div id="general-info" class="content">
				<div class="padded">
					<!-- <p id="buyernotes"><?php echo stripslashes_all(nl2br($this->buyer[0]->desc)) ? stripslashes_all(nl2br($this->buyer[0]->desc)) : "".JText::_('COM_USERPROF_NONE')."";  ?></p> -->
					<p id="buyernotes"><?php echo ($this->buyer[0]->home_buyer == 1) ? JText::_('COM_BUYERS_HOMEMARKET_DESC') : stripslashes_all(nl2br($this->buyer[0]->desc)); ?></p>
				</div>
				<div class="property-features">
					<h2 style="margin-bottom:10px;"><?php echo JText::_('COM_BUYERS_PROPERTY_INFO') ?></h2>
					<ul class="left first-feature">
						<?php 
	                       	$it=1;
	                       	foreach ($this->buyer[0]->buyer_feats['feats_array'] as $key => $value) {
	                       		echo "<li>".$value. "</li>";
	                       		if($it==6){
	                       			echo '</ul><ul  class="left" style="margin-left: 30px;">';
	                       			$it=0;
	                       		}
	                       		$it++;
	                       	}                     
						?>	
					</ul>
				</div>  
			</div>
		</div>
		<?php #echo "<pre>"; print_r($this->pocket_listings); die(); ?>
        <div id="divmatchpop" class="clear-float">
            <h1><?php echo JText::_('COM_BUYERS_MATCHPOPS_HEADING') ?></h1>
            <br>
			<div id="ifall"><br /><br /></div>
			<?php if($this->buyer[0]->hasnew_2 > 0 && $this->buyer[0]->listingcount_new > 0 ){ ?>
				<div class="items-container list-style "><!-- start items -->
                  <ul class="items">
					<a href="javascript:void(0);" id="show_hidenew" class="text-link show_hidenew" ><span id="dropdownindicatorNew">+ </span><?php echo JText::_('COM_BUYERS_NEWPOPS') ?> (<span class="allctr-new"></span>)</strong></h4></a>
					<div class="hiddenDivNew" style="margin-top:10px">
					<?php foreach($this->new_pocket_listings as $pk){ ?>
								<?php 
							  			$flagicon="";
										if($this->user_info->country!=$pk->country){
											if($pk->country == 0 && $this->user_info->country!=223){
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
											} elseif($pk->country != 0) {
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($pk->countryIso).'-flag-lang.png">';
											}								
										}
							  ?>
					<?php
					if(JFactory::getUser()->id == $pk->user_id) {
						$display = "full";
					} else {
						if($pk->setting == 1) {
							if($pk->is_private == 1) {
								if(!$pk->req_acc_status) {
									if($pk->req_acc_status === "0") {
										$display = "pending";
									} else {
										$display = "no";
									}
								} else {
									if($pk->req_acc_status == 1) {
										$display = "inside";
									} else if($pk->req_acc_status == 2) {
										$display = "no";
									}
								}
							} else {
								$display = "inside";
							}
							
						} else if($pk->setting == 0 || $pk->setting == 2) {
							if(!$pk->req_acc_status) {
								if($pk->req_acc_status === "0") {
									$display = "pending";
								} else {
									$display = "no";
								}
							} else {
								if($pk->req_acc_status == 1) {
									$display = "inside";
								} else if($pk->req_acc_status == 2) {
									$display = "no";
								}
							}
						}
						
						/*
						if(in_array(JFactory::getUser()->id, $pk->network)) {
							if($pk->setting == 1) {
								$display = "inside";
							} else if($pk->setting == 2) {
								if(!$pk->req_acc_status) {
									if($pk->req_acc_status === "0") {
										$display = "pending";
									} else {
										$display = "no";
									}
								} else {
									if($pk->req_acc_status == 1) {
										$display = "inside";
									} else if($pk->req_acc_status == 2) {
										$display = "no";
									}
								}
							}
						} else if(in_array(JFactory::getUser()->id, $pk->network_pending)) {
							$display = "ask network pending";
						} else if(in_array(JFactory::getUser()->id, $pk->network_denied)) {
							$display = "ask network denied";
						} else {
							$display = "ask network";
						}
						*/
					}
					?>
					<div class="clear-float"><br /></div>
                    <li>				
					  <div class="items-inner-container">
						  <div class="image-placeholder">
							<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
							<?php if($display == "full" ||$display == "inside" ) { ?>
								<blockquote>
									<p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>"><img  src="<?php echo $pk->images; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
							<?php } else { ?>
								<?php if ($pk->selected_permission==1) { ?>
								<blockquote>
									<p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>"><img  src="<?php echo $pk->images; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
								<?php } else { ?>
								<blockquote>
									<p><a href="javascript:void(0);" ><img  src="<?php echo $pk->images_bw; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
								<?php } ?>
							<?php } ?>
						  </div>
						  <div style="position:relative;">
							<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
							<?php if($display == "full" || $display == "inside") { ?>
							  <blockquote>
								  <p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>" class="item-number text-link"><?php echo $pk->zip; ?></a>
								  </p>
								   <p id="property_name" style="margin-top:10px"><?php echo stripslashes_all($pk->property_name); ?></p>
									<p class="reality-description">
									<?php
									 echo $pk->pop_feats['initial_feats'];
									?>
									</p>
									</blockquote>
							<?php } else if($display == "ask network" || $display == "ask network pending" || $display == "ask network denied") { ?>
								<?php if($pk->selected_permission==1) { ?>
								<blockquote>
									<p class="reality-description">
									<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>" class="item-number text-link"><?php echo $pk->zip; ?></a></p>
								    <p id="property_name" style="margin-top:10px"><?php echo stripslashes_all($pk->property_name); ?></p>
									<p class="reality-description">
										<?php
								echo $pk->pop_feats['initial_feats'];		
								?>
									</p>
								</blockquote>
								<?php } else { ?>
								<blockquote>
									<p class="item-number private-black"><?php echo $pk->zip; ?></p>
									<p class="reality-description"><?php echo "<a href= ".JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$pk->user_id."><span class='text-link'>".stripslashes_all(JFactory::getUser($pk->user_id)->name)."</span></a> ".JText::_('COM_NETWORK_TERMS_PRIV2').""; ?></p>
								</blockquote>
								<?php } ?>
							<?php } else { ?>
								<blockquote>
									<p class="item-number private-black"><?php echo $pk->zip; ?></a></p>
									<p class="reality-description"><?php echo "<a href= ".JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$pk->user_id."><span class='text-link'>".stripslashes_all(JFactory::getUser($pk->user_id)->name)."</span></a> ".JText::_('COM_NETWORK_TERMS_PRIV2').""; ?></p>
								</blockquote>
							<?php } ?>
						  </div>
					  </div>
					  <div class="other-description-placeholder">
						<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
						<?php if($display == "full") { ?>
						<blockquote>
							  <p>
								<span class="price">
									<?php
										if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
											echo format_currency_global($pk->price1,$pk->symbol);
											echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
										} else {
											echo "price undisclosed";
										}	
										$ts1 = strtotime(date('Y-m-d'));
										$ts2 = strtotime($pk->date_expired);
										$seconds_diff = $ts2 - $ts1;
										$days = floor($seconds_diff/3600/24);
									?>
								</span>
							  </p>
						</blockquote>
						<div class="seen-expire">
							  <blockquote>
								<p>
									<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
									</a>
								</p>
								<p>	
								 <span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()"><?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?> 	
										<a 
                                        	id="item<?php echo $pk->listing_id?>"
											class="text-link expiry"
											href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#edittext"
											onclick="update(<?php echo $pk->listing_id?>)"> <?php echo (($days<1) ? "" : $days) ?>
										</a> 
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</span>
								</p>
							  </blockquote>
						  </div>
						  <?php } else { ?>
							<?php if($display == "no"){ ?>
								<div style="margin-top: 8px;" >
									<a id="bigbutton" href="javascript:void(0)"
									onclick="requestAccess('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
								</div>
							<?php } else if($display == "ask network") { ?>
								<div style="margin-top: 8px;" >
									<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<a id="bigbutton" href="javascript:void(0);"
									onclick="requestNetwork('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
									<?php } ?>
								</div>		
							<?php } else if($display == "pending" || $display == "ask network pending") { ?>
								<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<div style="margin-top: 8px;" >
									<div class="button buymatch-req-pending" style="margin: 6px auto; display:block; font-size:12px; padding-top:7px; height:30px" ><?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?></div>
									</div>
									<?php } ?>
							<?php } else if($display == "ask network denied") {?>
								<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<div style="margin-top: 8px;" >
									<a id="bigbutton" href="javascript:void(0);"
									onclick="requestNetwork('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
									</div>
									<?php } ?>
							<?php } else if($display == "inside") {?>
									<blockquote>
									<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
									</p>
									</blockquote>
									<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
							<?php } ?>
						  <?php } ?>
					  </div>
					</li>
					<?php } ?>
					</div>
				  </ul>
				</div>
			<?php } ?>
            <div class="clear-float"></div>
			<div class="items-container items-container-saved list-style"><!-- start items -->
              <ul class="items">
                 <a href="javascript:void(0);" id="show_hidesaved" class="text-link show_hideSave"><h4 class="allctr-saved" style="margin-bottom:10px"><span id="dropdownindicatorSaved">+ </span><?php echo JText::_('COM_BUYERS_SAVEDPOPS') ?> (<span class="savedctr"></span>)</h4></a>
                <div class="hiddenDivSaved" id="hiddenDivSaved">
				<?php $saved = 0; ?>
				<?php foreach($this->saved_listings as $key => $pk){ 
					$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($pk[0]->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);

									if ($days<1) {
									   continue;
									}


					?>
				<?php 	$saved++;
										$flagicon="";
										if($this->user_info->country!=$pk->country){
											if($pk->country == 0 && $this->user_info->country!=223){
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
											} elseif($pk->country != 0) {
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($pk->countryIso).'-flag-lang.png">';
											}								
										}
				?>
                <li>				
					  <div class="items-inner-container">
						  <div class="image-placeholder">
							<blockquote>
								<p>
								<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $this->buyers_list[$key]->listing_id); ?>">
								<img src="<?php echo $pk[0]->images; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a>
								</p> 
							</blockquote>
						  </div>
						  <div style="/*width:500px*/position:relative">
						  	<?php if($pk[0]->sold){ ?>
						  		<div class="folded"><h2>
								<?php if ($pk[0]->type_id==1 || $pk[0]->type_id==3) {?>
										<?php echo JText::_('COM_POPS_TERMS_SOLD') ?>
								<?php } else {?> <?php echo JText::_('COM_POPS_TERMS_LEASED') ?><?php }?></h2></div>
							<?php } ?>
							<blockquote>
							<p>
						    <a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $this->buyers_list[$key]->listing_id); ?>" class="item-number text-link"><?php echo $pk[0]->zip; ?></a>
							</p>
							<p id="property_name" style="margin-top:10px"><?php echo stripslashes_all($pk[0]->property_name); ?></p>
							<p class="reality-description">
							<?php
							echo $pk[0]->save_pop_feats['initial_feats'];							
							?>
									</p>
							</blockquote>
						  </div>
					  </div>
					  <div class="other-description-placeholder">
						<blockquote>
							  <p>
								<span class="price">
									<?php 
										if($pk[0]->disclose == 1 || JFactory::getUser()->id == $pk[0]->user_id) {
											echo format_currency_global($pk[0]->price1,$pk[0]->symbol);
											echo (!empty($pk[0]->price2) ? " - " . format_currency_global($pk[0]->price2,$pk[0]->symbol) : "")." ".$pk[0]->currency; 
										} else {
											echo "price undisclosed";
										}										
									?>
                                    <?php 
									
									?>
								</span>
							  </p>
						</blockquote>
						<div class="seen-expire">
									<a class="left <?php echo ($pk[0]->user_id != JFactory::getUser()->id) ? "hidden": "";?>">
                                    	<?php echo (($pk[0]->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk[0]->views.'</span>' : ""; ?></a> 
                            <?php if($pk[0]->sold){	
                            		$sleast = "";
                            		if($pk[0]->property_type=="2" || $pk[0]->property_type=="4"){
                            			$sleast = JText::_('COM_POPS_TERMS_LEASED');
                            		} else {
                            			$sleast = JText::_('COM_POPS_TERMS_SOLD');
                            		}
                            	?>
                            	   <span class="right" onMouseover="ddrivetip('<?php echo $sleast?>');hover_dd()" onMouseout="hideddrivetip();hover_dd()">							
										<?php echo $sleast?>
									</span>
                            <?php } else if($pk[0]->sold==0) { ?>        	
                            <span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
								<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?> 
	                            <?php echo (($days<1) ? "" : $days) ?>
								<?php if ($days<1) {
									echo "Expired" ;
									} else if ($days==1) {
									echo "".JText::_('COM_POPS_TERMS_DAY').""; 
									} else {
									echo "".JText::_('COM_POPS_TERMS_DAYS')."";
									} ?>
							</span>
							<?php } ?>
						</div>
					  </div>
				</li>
				<?php } ?>
              </ul>
         		 </div>
            </div>
             <div class="clear-float"></div>
             <div class="items-container list-style items-container-all"><!-- start items -->
             <a href="javascript:void(0);" id="show_hide" class="text-link show_hide"><h4 class="allctr-main"><span id="dropdownindicator">- </span><?php echo JText::_('COM_BUYERS_ALLPOPS') ?> (<span class="allctr"></span>)</h4></a>
				<div class="hiddenDiv" id="hiddenDiv">
				  <ul class="items">
				<?php $ctr = 0; ?>
				<?php foreach($this->pocket_listings as $pk){ ?>
				<?php 
							  			#print_r($pk); echo "<br />";
										$flagicon="";
										if($this->user_info->country!=$pk->country){
											if($pk->country == 0 && $this->user_info->country!=223){
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
											} elseif($pk->country != 0) {
												$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($pk->countryIso).'-flag-lang.png">';
											}								
										}
							  ?>
					<?php
					if(JFactory::getUser()->id == $pk->user_id) {
						$display = "full";
					} else {
						if($pk->setting == 1) {
							if($pk->is_private == 1) {
								if(!$pk->req_acc_status) {
									if($pk->req_acc_status === "0") {
										$display = "pending";
									} else {
										$display = "no";
									}
								} else {
									if($pk->req_acc_status == 1) {
										$display = "inside";
									} else if($pk->req_acc_status == 2) {
										$display = "no";
									}
								}
							} else {
								$display = "inside";
							}
							
						} else if($pk->setting == 0 || $pk->setting == 2) {
							if(!$pk->req_acc_status) {
								if($pk->req_acc_status === "0") {
									$display = "pending";
								} else {
									$display = "no";
								}
							} else {
								if($pk->req_acc_status == 1) {
									$display = "inside";
								} else if($pk->req_acc_status == 2) {
									$display = "no";
								}
							}
						}
						
						/*
						if(in_array(JFactory::getUser()->id, $pk->network)) {
							if($pk->setting == 1) {
								$display = "inside";
							} else if($pk->setting == 2) {
								if(!$pk->per) {
									if($pk->per === "0") {
										$display = "pending";
									} else {
										$display = "no";
									}
								} else {
									if($pk->per == 1) {
										$display = "inside";
									} else if($pk->per == 2) {
										$display = "no";
									}
								}
							}
						} else if(in_array(JFactory::getUser()->id, $pk->network_pending)) {
							$display = "ask network pending";
						} else if(in_array(JFactory::getUser()->id, $pk->network_denied)) {
							$display = "ask network denied";
						} else {
							$display = "ask network";
						}
						*/
					}
					?>
					<div class="clear-float"><br /></div>
                    <li>				
					  <div class="items-inner-container">
						  <div class="image-placeholder">
							<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
							<?php if($display == "full" ||$display == "inside" ) { ?>
								<blockquote>
									<p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>"><img  src="<?php echo $pk->images; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
							<?php } else { ?>
								<?php if ($pk->selected_permission==1) { ?>
								<blockquote>
									<p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>"><img  src="<?php echo $pk->images; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
								<?php } else { ?>
								<blockquote>
									<p><a href="javascript:void(0);" ><img  src="<?php echo $pk->images_bw; ?>?true" style="width: 100px; margin-top: -5px; margin-left: 0px;"></a></p>
								</blockquote>
								<?php } ?>
							<?php } ?>
						  </div>
						  <div style="position:relative;">

						  	<?php if($pk->sold){ ?>
						  		<div class="folded"><h2>
								<?php if ($pk->type_id==1 || $pk->type_id==3) {?>
										<?php echo JText::_('COM_POPS_TERMS_SOLD') ?>
								<?php } else {?> <?php echo JText::_('COM_POPS_TERMS_LEASED') ?><?php }?></h2></div>
							<?php } ?>

							<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
							<?php if($display == "full" || $display == "inside") { ?>
							  <blockquote>
								  <p><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>" class="item-number text-link"><?php echo $pk->zip; ?></a>
								  </p>
								   <p id="property_name" style="margin-top:10px"><?php echo stripslashes_all($pk->property_name); ?></p>
									<p class="reality-description">
										<?php
										echo $pk->pop_feats['initial_feats'];							
										?>
									</p>
									</blockquote>
							<?php } else if($display == "ask network" || $display == "ask network pending" || $display == "ask network denied") { ?>
								<?php if($pk->selected_permission==1) { ?>
								<blockquote>
									<p class="reality-description">
									<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $pk->listing_id); ?>" class="item-number text-link"><?php echo $pk->zip; ?></a></p>
								    <p id="property_name" style="margin-top:10px"><?php echo stripslashes_all($pk->property_name); ?></p>
									<p class="reality-description">
										<?php
										echo $pk->pop_feats['initial_feats'];		
									?>
									</p>
								</blockquote>
								<?php } else { ?>
								<blockquote>
									<p class="item-number private-black"><?php echo $pk->zip; ?></p>
									<p class="reality-description"><?php echo "<a href= ".JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$pk->user_id."><span class='text-link'>".stripslashes_all(JFactory::getUser($pk->user_id)->name)."</span></a> ".JText::_('COM_NETWORK_TERMS_PRIV2').""; ?></p>
								</blockquote>
								<?php } ?>
							<?php } else { ?>
								<blockquote>
									<p class="item-number private-black"><?php echo $pk->zip; ?></p>
									<p class="reality-description"><?php echo "<a href= ".JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$pk->user_id."><span class='text-link'>".stripslashes_all(JFactory::getUser($pk->user_id)->name)."</span></a> ".JText::_('COM_NETWORK_TERMS_PRIV2').""; ?></p>
								</blockquote>
							<?php } ?>
						  </div>
					  </div>
					  <div class="other-description-placeholder">
						<?php $arr_allowed = explode(",", $activity->listing[0]->allowed); ?>
						<?php if($display == "full") { ?>
						<blockquote>
							  <p>
								<span class="price">
									<?php
										if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
											echo format_currency_global($pk->price1,$pk->symbol);
											echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
										} else {
											echo "price undisclosed";
										}	
										$ts1 = strtotime(date('Y-m-d'));
										$ts2 = strtotime($pk->date_expired);
										$seconds_diff = $ts2 - $ts1;
										$days = floor($seconds_diff/3600/24);
									?>
								</span>
							  </p>
						</blockquote>
						<div class="seen-expire">
							  <blockquote>
								<p>
									<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
									</a>
								</p>
								<p>	
								 <span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()"><?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?> 	
										<a 
                                        	id="item<?php echo $pk->listing_id?>"
											class="text-link expiry"
											href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#edittext"
											onclick="update(<?php echo $pk->listing_id?>)"> <?php echo (($days<1) ? "" : $days) ?>
										</a> 
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</span>
								</p>
							  </blockquote>
						  </div>
						  <?php } else { ?>
							<?php if($display == "no"){ ?>
								<div style="margin-top: 8px;" >
									<a id="bigbutton" href="javascript:void(0)"
									onclick="requestAccess('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
								</div>
							<?php } else if($display == "ask network") { ?>
								<div style="margin-top: 8px;" >
									<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<a id="bigbutton" href="javascript:void(0);"
									onclick="requestNetwork('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
									<?php } ?>
								</div>		
							<?php } else if($display == "pending" || $display == "ask network pending") { ?>
								<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<div style="margin-top: 8px;" >
									<div class="button buymatch-req-pending" style="margin: 6px auto; display:block; font-size:12px; padding-top:7px; height:30px" ><?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?></div>
									</div>
									<?php } ?>
							<?php } else if($display == "ask network denied") {?>
								<?php if ($pk->selected_permission==1) { ?>
										<blockquote>
										<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
										</p>
										</blockquote>
										<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
									<?php } else { ?>
									<div style="margin-top: 8px;" >
									<a id="bigbutton" href="javascript:void(0);"
									onclick="requestNetwork('<?php echo $pk->user_id; ?>', '<?php echo $pk->listing_id; ?>')"
									style="margin: 6px auto; display:block; font-size:12px; padding-top:7px"  class="button gradient-blue ra<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>"><?php echo JText::_('COM_NETWORK_TERMS_REQ2VIEW') ?></a>
									<img height="26px" class="right loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question">
									</div>
									<?php } ?>
							<?php } else if($display == "inside") {?>
									<blockquote>
									<p>
										<span class="price">
										<?php
											if($pk->disclose == 1 || JFactory::getUser()->id == $pk->user_id){
												echo format_currency_global($pk->price1,$pk->symbol);
												echo (!empty($pk->price2) ? " - " . format_currency_global($pk->price2,$pk->symbol) : "")." ".$pk->currency; 
											} else {
												echo "price undisclosed";
											}	
											$ts1 = strtotime(date('Y-m-d'));
											$ts2 = strtotime($pk->date_expired);
											$seconds_diff = $ts2 - $ts1;
											$days = floor($seconds_diff/3600/24);
										?>
										</span>
									</p>
									</blockquote>
									<div class="seen-expire">
										<blockquote>
										<p>
											<a class="seen-by left <?php echo ($pk->user_id != JFactory::getUser()->id) ? "hidden": "";?>"><?php echo (($pk->views>0)) ? JText::_('COM_POPS_VIEW_SEENBY').'<span class="hover-item viewers" data=\''.$viewHTML.'\'>'.$pk->views.'</span>' : ""; ?>
											</a>
										</p>
										<p>	
										<span class="right" onMouseover="ddrivetip('Days until this POPs&trade; expires');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
										<?php echo (($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN').""); ?>
										<?php echo (($days<1) ? "" : $days) ?>
										<?php if ($days<1) {
											echo "Expired" ;
											} else if ($days==1) {
											echo "".JText::_('COM_POPS_TERMS_DAY').""; 
											} else {
											echo "".JText::_('COM_POPS_TERMS_DAYS')."";
											} ?>
										</p>
										</blockquote>				
										</div>
							<?php } ?>
						  <?php } ?>
					  </div>
					</li>
					<?php $ctr++; ?>
					<?php } ?>
					<?php  if($this->buyer[0]->listingcount <= 8){ # if($ctr <= 8){?>
						<?php if($this->buyer[0]->listingcount == 0) { ?>
						<div class="pattern-div" id="pattern-div" style="width:100%;position:relative;float:left;height:100px;text-align:center;">
						<p style="margin-top:40px;"> <?php echo JText::_('COM_BUYERS_NOMATCHINGPOPS') ?>
						 </p>
						</div>
						<?php } ?>
						<div class="list-style items-container-all"><!-- start items -->
							<div class="clear-float"><br /></div>
							<a href="javascript:void(0);" onclick="searchmore(<?php echo $this->buyer[0]->buyer_id; ?>);" id="search_more" class="text-link"><h4 class="allctr-main3"><?php echo JText::_('COM_BUYERS_EXPANDSEARCH')?></h4></a>
							<div class="clear-float" id="search-clearfloat"><br /><br /></div>
							<div class="hiddenDivsearchmore" id="hiddenDivsearchmore">
								<ul class="items-searchmore">
									<div id="loader-container"><img style="margin-bottom: 10px;" height="26px" class="loader loading<?php echo $pk->user_id; ?><?php echo $pk->listing_id; ?>" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading_search"></div>
								</ul>
							</div>
						</div>
					<?php } ?>
				<?php if($ctr == 0){ $noproperty = 1; } ?>
              </ul>
             <div class="clear-float">	 <br /><br /><br /></div>
             </div>
            <input type="hidden" value="<?php echo $this->hidden; ?>" id="hiddenval" />
			<?php #echo $ispending; ?>
      </div>
	</div>
	</div><!-- end wide-content -->
	<div id="edittext" style="display: none">
			 <span>Please select new date of expiration</span><br /> 
             <div style="width: 220px">
             <input style="margin-top:10px; height:30px" type="text" id="userinput" class="textbox-up left" style="width:180px" /> 
             <input type="hidden" id="editid" /> 
             <input class="button gradient-green right" style="margin-top:10px; padding-top:7px; height:30px; width:80px" type="button" value="Submit" id="changevalue"/>
			</div>
			<div id="expmsg" class="error_msg" style="display:none; padding-top:50px">You have successfully updated this POPs&trade; expiry.</div>
			<div id="experr" class="error_msg" style="display:none; padding-top:50px">You cannot set the expiry to today's date or a past date.</div>
	</div>
	<?php
		include( 'includes/sidebar_left.php' );
	?>
<script>
function update(lid){
		jQuery( "#editid" ).val(lid);
		jQuery( "#edittext" ).dialog(
			{
				modal: true,
				title: "Change Expiration Date",
				}
			);
	}
jQuery(document).ready(function(){
	//getExcRates();
	jQuery(".loader").hide();
	jQuery("#loader-container").hide();
	if(jQuery('.property-features ul').children().length < 1){	
		jQuery('.property-features').html('<h2>Property Features</h2><br/>none provided');
	}
	Landing.format();
	jQuery("#larger").click(function() {
		jQuery("div a.grouped_elements").click();
	});
	jQuery("#userinput").datepicker();
	var aa = jQuery("#hiddenval").attr('value');
	var checker = '<?php echo $noproperty; ?>';
	var ctr = <?php echo $this->buyer[0]->listingcount; ?>;
	var ctrNew = <?php echo $this->buyer[0]->listingcount_new ?>;
	var saved = <?php echo $saved; ?>;
	var clicked_th = "<?php echo $_GET['def_open'];?>";
	//var sessionedNew = '<?php if(isset($_SESSION['newctr'])){echo $_SESSION['newctr'];} else {echo "aa";} ?>';
	jQuery("#ifall").hide();
	if(saved > 0){
		jQuery(".savedctr").text(saved);
		//jQuery("#hiddenDivSaved").css("display","block");
		jQuery(".savedctr").show();
	//	alert("saved pops");
	} else {
		jQuery(".items-container-saved").hide();
	}
	if(ctr > 0){
		jQuery(".allctr").html(ctr);
		//jQuery(".items-container-all").show();
	} else{
		jQuery(".allctr-main").hide();
		//jQuery(".items-container-all").hide();
	}
	if(ctrNew > 0){
		jQuery(".allctr-new").html(ctrNew);
		//jQuery(".items-container-all").show();
	} else{
		//jQuery(".allctr-main").hide();
		//jQuery(".items-container-all").hide();
	}
    jQuery(".show_hide").show();
	jQuery(".show_hidenew").show();
	jQuery(".show_hideSaved").show();
	//if(aa == '1'){	
	//	jQuery(".hiddenDiv").show();
	//}else{
	//	jQuery(".hiddenDiv").hide();
	//}
if(clicked_th=="New"){
		jQuery("#dropdownindicator").html("+ ");
		jQuery("#dropdownindicatorNew").html("- ");
		jQuery("#dropdownindicatorSaved").html("+ ");
		jQuery(".hiddenDiv").hide();
		jQuery(".hiddenDivNew").show();
		jQuery(".hiddenDivSaved").hide();
	} else if(clicked_th=="Saved") {
		jQuery("#dropdownindicator").html("+ ");
		jQuery("#dropdownindicatorNew").html("- ");
		jQuery("#dropdownindicatorSaved").html("- ");
		jQuery(".hiddenDiv").hide();
		jQuery(".hiddenDivNew").show();
		jQuery(".hiddenDivSaved").show();
	} else if(clicked_th=="All"){
		jQuery("#dropdownindicator").html("- ");
		jQuery("#dropdownindicatorNew").html("- ");
		jQuery("#dropdownindicatorSaved").html("+ ");
		jQuery(".hiddenDiv").show();
		jQuery(".hiddenDivNew").show();
		jQuery(".hiddenDivSaved").hide();
	} else {
		if(ctrNew){
			jQuery("#dropdownindicator").html("+ ");
			jQuery("#dropdownindicatorNew").html("- ");
			jQuery("#dropdownindicatorSaved").html("+ ");
			jQuery(".hiddenDiv").hide();
			jQuery(".hiddenDivNew").show();
			jQuery(".hiddenDivSaved").hide();
		} else {			
			jQuery("#dropdownindicator").html("- ");
			jQuery("#dropdownindicatorNew").html("+ ");
			jQuery("#dropdownindicatorSaved").html("- ");
			jQuery(".hiddenDiv").show();
			jQuery(".hiddenDivNew").hide();
			jQuery(".hiddenDivSaved").show();
		}
	}
	jQuery('.show_hide').click(function(){
		jQuery(".main-content").css('height', 'auto ');
		var dropdownflag = jQuery("#dropdownindicator").html();	
		jQuery(".hiddenDiv").slideToggle("slow");
		if(dropdownflag == "+ "){
			jQuery("#dropdownindicator").html("- ");
		} else {
			jQuery("#dropdownindicator").html("+ ");
		}
		jQuery("html,body").animate({scrollTop: jQuery(".hiddenDiv").offset().top},"slow");
    });
	jQuery('.show_hidenew').click(function(){
		jQuery(".main-content").css('height', 'auto ');
		var dropdownflagNew = jQuery("#dropdownindicatorNew").html();
		jQuery(".hiddenDivNew").slideToggle("slow");
		if(dropdownflagNew == "+ "){
			jQuery("#dropdownindicatorNew").html("- ");
		} else {
			jQuery("#dropdownindicatorNew").html("+ ");
		}
		jQuery("html,body").animate({scrollTop: jQuery(".hiddenDivNew").offset().top},"slow");
	});
	jQuery('#show_hidesaved').click(function(){
		jQuery(".main-content").css('height', 'auto ');
		var dropdownflagNew = jQuery("#dropdownindicatorSaved").html();
		jQuery(".hiddenDivSaved").slideToggle("slow");
		if(dropdownflagNew == "+ "){
			jQuery("#dropdownindicatorSaved").html("- ");
		} else {
			jQuery("#dropdownindicatorSaved").html("+ ");
		}
		jQuery("html,body").animate({scrollTop: jQuery(".hiddenDivSaved").offset().top},"slow");
	});
});
jQuery(document).ready(function(){
		jQuery("div a.grouped_elements").fancybox();
			          window.onunload = window.onbeforeunload = function() { /* do stuff */ 
            //this will work only for Chrome
              jQuery.ajax({
                url: "<?php echo JRoute::_('index.php?option=com_propertylisting') ?>?format=raw&task=resetBuyerList",
                type: 'POST',
                data:{buyerId:"<?php echo $_GET['lID']?>"},
                async: false,
                success: function(e){
                }
              });
          };
	});
jQuery("#changevalue").click(function(){
	var lid = jQuery( "#editid" ).val();
	var userinput = jQuery( "#userinput" ).val();
	jQuery.post("<?php echo JRoute::_('index.php?option=com_userprofile&task=updatelisting'); ?>&id="+lid+"&value="+userinput,
			function(data){
				var result = jQuery.parseJSON(data);
				if(result.code == 1){
					//alert(result.value);
					jQuery( "#item"+lid ).html(result.days);
					jQuery( "#expmsg" ).show();
					jQuery( "#experr" ).hide();
				}
				else
					jQuery( "#experr" ).show();
			});
});
function searchmore(lid){
	jQuery("#loader-container").show();
	jQuery("#loading_search").show();
	jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=expandsearch') ?>',
			data: {'lID': lid},
			success: function(data){
				console.log(data);
				var test = data.split('<div id="remove-this">');
				var test0 = test[1];
				if(test0){
					jQuery("#loader-container").hide();
					jQuery("#loading_search").hide();
					var test2 = test0.split('<div id="remove-this2">');
					if(test2[0]) {
						console.log(test2[0]);
						jQuery(".items-searchmore").html(test2[0]);
						jQuery("#search_more").attr("onclick","");
						jQuery(".allctr-main3 #dropdownindicator").html("- ");
					} else {
						jQuery("#loader-container").hide();
						jQuery("#loading_search").hide();
						jQuery("#search-clearfloat").hide();
						jQuery(".items-searchmore").append("<div style='margin-top:10px; margin-bottom: 10px;'>No POPs&trade; Matched</div>");
						jQuery("#search_more").attr("onclick","");
						jQuery(".allctr-main3 #dropdownindicator").html("- ");			
					}
				} else {
					jQuery("#loader-container").hide();
					jQuery("#loading_search").hide();
					jQuery("#search-clearfloat").hide();
					jQuery(".items-searchmore").append("<div style='margin-top:10px; margin-bottom: 10px;'>No POPs&trade; Matched</div>");
					jQuery("#search_more").attr("onclick","");
					jQuery(".allctr-main3 #dropdownindicator").html("- ");
				}
				jQuery("#search_more").addClass("show_hide_3");
			}
		});		
}
function requestAccess(uid, pid){
		//console.log(uid +" "+ pid);
		jQuery(".ra"+uid+""+pid).hide();
		jQuery(".loading"+uid+""+pid).show();
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=requestaccess') ?>',
			data: {'pid': pid, 'uid': uid},
			success: function(data){
				var hid = 1;
				jQuery(".loading"+uid+""+pid).hide();	
				jQuery(".ra"+uid+""+pid).after("<div class=\"button buymatch-req-pending\" style=\"display:block;font-size:12px;height:30px;margin:6px auto;padding-top:7px;\"><?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?></div>");
			}
		});
	}
function requestAccess2(uid, pid){
		//console.log(uid +" "+ pid);
		jQuery(".ra"+uid+""+pid).hide();
		jQuery(".loading"+uid+""+pid).show();
		jQuery.ajax({
			type: "POST",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=requestaccess') ?>',
			data: {'pid': pid, 'uid': uid},
			success: function(data){
				var hid = 1;
				jQuery(".loading"+uid+""+pid).hide();	
				jQuery(".ra"+uid+""+pid).after("<div class=\"button buymatch-req-pending\" style=\"display:block;font-size:12px;height:30px;margin:6px auto;padding-top:7px;\"><?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?></div>");
			}
		});
	}
function requestNetwork(other_user_id, aid){
		//alert('<?php echo htmlspecialchars_decode(JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=')); ?>'+bid);
		//console.log(uid +" "+ pid);
		jQuery(".ra"+other_user_id+""+aid).hide();
		jQuery(".loading"+other_user_id+""+aid).show();
		jQuery.ajax({
			type: "GET",
			url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=request_network'); ?>&id='+other_user_id,
			success: function(data){
				var hid = 1;
				//jQuery.post('<?php echo htmlspecialchars_decode(JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=')); ?>'+bid, { hidden: hid}, function(data) {
				jQuery(".loading"+other_user_id+""+aid).hide();
				jQuery(".ra"+other_user_id+""+aid).after("<div class=\"button buymatch-req-pending\" style=\"display:block;font-size:12px;height:30px;margin:6px auto;padding-top:7px;\"><?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?></div>");
			}				
		});
	}
function viewers(){
		//alert('qtip');
		$(".viewers").each(function(){
			$(this).qtip({
				content: $(this).attr('data'),
				events: {
					show: function(event, api) {
						$(this).find('img.resize3').each(function() {
							$(this).resizeonload(jQuery(this).parent(), false);
						});
					},
					hide: function(event, api) {
							$(this).qtip("destroy");
							viewers();
					}
				}
			});
		});
	}
</script>
<?php 
	if(!isset($_SESSION['viewed_buyerspage_2']) || $_SESSION['viewed_buyerspage_2'] == 0){
		$_SESSION['viewed_buyerspage_2'] = 1;	
	}
?>