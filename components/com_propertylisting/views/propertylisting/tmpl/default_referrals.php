<?php
$classes = array('u_contract','connected','working','closed','no_go','need_help','pending','active','comres');
$printdesig = "";
function decrypt($encrypted_text) {
	$key = 'password to (en/de)crypt';
	
	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	
	return $decrypted;
}

preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
$os = current($matches);

if($os){
	$notMobile=0;
} else {
	$notMobile=1;
}

?>
<style>
.referral_transaction_details_text.price_h {
height: 30px;
}
.countCode{
	width: 28px;
    display: inline-block;
    text-align: center;
}
.reftriangle{
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 0 10px 10px 10px;
	border-color: transparent transparent #595959 transparent;
	position: absolute;
    top: -51px;
    left: -5px;
}
.client_download_grid.opennext.text-link {
height: 23px;
}
#popupdetails{
	overflow:visible !important;
}
</style>
<script>
jQuery(document).ready(function(){

	jQuery("#select_fil").select2({ width: "100%" });

	jQuery("#select_fil").change(function(){

	//	jQuery(this).closest("li");

		var v_style ="";
		if(jQuery(".list-view").hasClass("bg-bottom")){
			v_style='list';
		} else {
			v_style='grid';
		}

		var id = jQuery(this).val();
		if(id=="all"){
			jQuery('.refs_all').show();
			jQuery(".noresultsfilter").hide();	
		} else {
			jQuery("."+id+"_items").show();	
			/*if(jQuery("."+id+"_items").length){
				jQuery("."+id+"_items").show();	
				//jQuery(".noresultsfilter").hide();		
			} else {
			//	jQuery(".noresultsfilter").show();
			}*/


/*			alert(jQuery(".ref_in_li").filter(function() {
  return jQuery(this).css('display') !== 'none';
}).length);
*/

        
			
			jQuery('.refs_all').not("."+id+"_items").hide();


			var numrelated=jQuery('.ref_in_li:visible').length;
       		var numrelated_ro=jQuery('.ref_out_li:visible').length;

			if(numrelated==0){
				jQuery(".ref_in_div .noresultsfilter").show();
			} else {
				jQuery(".ref_in_div .noresultsfilter").hide();
			}
			
			if(numrelated_ro==0){
				jQuery(".ref_out_div .noresultsfilter").show();
			}  
			else {
				jQuery(".ref_out_div .noresultsfilter").hide();
			}
		}
		
		/*<?php $newRequest = preg_replace('/[?&]filter=[a-z]{6}/', '', $_SERVER['REQUEST_URI']);
			  $newRequest = preg_replace('/[?&]vstyle=[a-z]{4}/', '', $newRequest);
		?>
		if(jQuery(this).val()=='nofilter')
			window.location = "//<?php echo $_SERVER['HTTP_HOST'].$newRequest.(isset($_GET) ? '&' : '?');?>vstyle="+v_style;
		else
			window.location = "//<?php echo $_SERVER['HTTP_HOST'].$newRequest.(isset($_GET) ? '&' : '?'); ?>filter="+jQuery(this).val()+"&vstyle="+v_style;*/


	});
	
	if (jQuery(window).width() <= 600) {
			jQuery("#updatestatus").removeAttr('onmouseover');
			jQuery("#updatestatus2").removeAttr('onmouseover');
			jQuery("#updatestatus3").removeAttr('onmouseover');
			jQuery("#updatestatus4").removeAttr('onmouseover');
			jQuery("#firstname").removeAttr('onmouseover');
			jQuery("#lastname").removeAttr('onmouseover');
			jQuery("#phone").removeAttr('onmouseover');
			jQuery("#email").removeAttr('onmouseover');
			jQuery("#jform_country").removeAttr('onmouseover');	
			jQuery("#zip").removeAttr('onmouseover');
			jQuery("#bslno").removeAttr('onmouseover');
			jQuery("#btino").removeAttr('onmouseover');
			jQuery("#alslno").removeAttr('onmouseover');
			jQuery("#address1").removeAttr('onmouseover');	
			jQuery("#card_number").removeAttr('onmouseover');
			jQuery("#card_security").removeAttr('onmouseover');
			jQuery("#card_expiry").removeAttr('onmouseover');			
	}
			
     <?php if( $_POST['v_style']=="list"){ ?>
		jQuery(".referral_row ").addClass('l_rbox');
		jQuery(".tile-view").addClass('bg-bottom');
		jQuery(".list-view").addClass('bg-bottom');		
	<?php }?>
		
	
		<?php if(isset($_POST['filter'])){ ?>
					if(!jQuery(".rbox").length){
				//		jQuery("div.referral_row").css("display","none");
				//		jQuery("div.referral_row").html("<br/>no results found").css("display","block");
					}
					jQuery("select[name='filter']").select2("data", "7");
				
					console.log("<?php echo $_POST['v_style']?>");
					console.log("sa");
		<?php } ?>
});
</script>
<div class="wrapper">
	<div class="wide left referrals">
		<!-- start wide-content -->
		<h1><?php echo JText::_('COM_REFERRAL_MY') ?></h1>
		<?php if(((count($this->refin))+(count($this->refout))) || isset($_POST['filter'])){ ?>
					<?php $numrefs = (count($this->refin))+(count($this->refout)) ?>
		<div class="items-filter">
			<div class="left" id="changeview" style="margin-right: 10px;">
				<a class="tile-view left view"></a> 
                <a class="list-view left view"></a>
			</div>
			<span class="tile-number">
				<?php echo JText::_('COM_POPS_TERMS_YOUHAVEACTIVEPOPS')?>
				<?php echo ($numrefs).(($numrefs>1) ? " ".JText::_('COM_REFERRAL_REFERRALS')."":" ".JText::_('COM_REFERRAL_REFERRAL')."")?>
				<?php echo (isset($_POST['filter']) ? '': '')?>
			</span> <a
						href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newreferral'); ?>"
						class="button gradient-green right addrefbtn"
						style="display: block; padding-top:8px"> <span class="refer-me-text" style="margin-left:-5px; padding-left:20px;"><?php echo JText::_('COM_USERPROF_PROF_CREATER') ?></span>
					</a>
			<div class="right" style="width:140px">
				<form id="filter" method="POST">
					<input type="hidden" id="v_style" name="v_style" value="<?php echo isset($_POST['v_style']) ? $_POST['v_style'] : "" ;?>">
					<select id="select_fil" name="filter" >
						<option value="all" selected='selected'><?php echo JText::_('COM_REFERRAL_ALL') ?></option>
						<?php foreach($this->ref_status as $stat){
										/*if($stat->status_id==$_POST['filter']) {
											$selected = "selected='selected'";
										}
										else $selected = "";*/
										echo "<option value=\"".$stat->status_id."\" ". $selected .">".JText::_($stat->label)."</option>";
									}
									?>
					</select>
				</form>
			</div>
			<div class="clear-float"></div>
		</div>
		<?php } ?>
		<div class="referral_row">
			<?php if(count(($this->refin))) { 
				$count = 0;?>
				<?php //  echo var_dump($this->refin)?>
			<div class="clear-float ref_in_div">
				<div class="referral_title"><?php echo (count($this->refin) > 1) ? JText::_('COM_USERPROF_PROF_REFSIN') : JText::_('COM_USERPROF_PROF_REFIN'); ?></div>
				<div class="noresultsfilter" style="display: none;">
						<p>No results found</p>
					</div>
				<ul class="recommendations">
					<?php 
					$row_flag = 1;
					$cell_flag = 0;
					foreach($this->refin as $ref) {
						$printdesig = "";
						$limit = (count($ref->other_user->desigs) > 3) ? 3 : count($ref->other_user->desigs);
						$lastcm = $limit - 1;
						for($i = 0; $i<$limit; $i++){
								$printdesig .= $ref->other_user->desigs[$i]->designations;
								if($i<$lastcm)
									$printdesig .= ',';
							}
							?>
					<?php $sigs = explode(',', $ref->referral_info->signatories); ?>
					<?php 
						if(!$cell_flag) {
							echo "<div class='referral_row' id='referral_row$row_flag'>";
						}
					?>
					<li class="ref_in_li refs_all <?php echo strtolower($ref->status)?>_items">
						<div class="rbox ">
							<div class="rbox_grid ">
								<input type="hidden" id='<?php echo $ref->referral_id ?>'
									value='<?php echo json_encode($ref->history, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE); ?>'/>
								<input type="hidden" value="<?php echo isset($ref->overview) ? $ref->overview : "no overview yet" ?>"
									id="overview-<?php echo $ref->referral_id?>"/> 
                                
                                <input type="hidden"
									value='<?php echo stripslashes_all($ref->referral_info->buyer->name); ?>'
									id='client-<?php echo $ref->referral_id ?>'/>
								
                                <!--Referral Grid Upper Box-->
								<div class="referral_user">
									<?php if ($ref->other_user->image!="") { ?>
													<div class="referral_user_pic left">
													<img  width="60px" src="<?php echo strpos($ref->other_user->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $ref->other_user->image).'?'.microtime(true) :  JURI::base().'uploads/thumb_'.$ref->other_user->image.'?'.microtime(true)?>"></div>
											
											<?php } else { ?>
													<div class="referral_user_pic left">
													<img  width="60px" class="resize" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" ></div>
												
									<?php } ?>
									<div class="referral_user_details">
										<h2><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$ref->other_user->id)?>"> <?php echo stripslashes_all($ref->other_user->name)?></a></h2>
                                        <div class="referral_user_details_city"><?php echo $ref->printaddress?></div>
										<div class="status-box">
										<div class="left" style="margin-bottom: 5px">
											<?php $changeable = ($ref->status!=4 && $ref->status!=5 && $ref->status!=9 && in_array($ref->agent_b, $sigs)) ? "changestatus cursor-point" : "no_power"; 
												 if ($changeable=="changestatus cursor-point") {?> 
												<div id="updatestatus" onMouseover="ddrivetip('You may only update the status of this referral after you have signed the Referral Agreement');hover_dd()" onMouseout="hideddrivetip();hover_dd()" class="<?php echo strtolower($ref->label)." ".$changeable?> "
												data-id="<?php echo $ref->referral_id ?>"
												data="<?php echo $ref->status ?>"></div>
											<?php } else { ?>
												<div class="left <?php echo strtolower($ref->label)."".$changeable?>" data-id="<?php echo $ref->referral_id ?>" data="<?php echo $ref->status ?>"><?php echo JText::_($ref->label) ?></div>
											<?php } ?>
										</div>
										</div>
									</div>
								</div>                                
                                 <!--Referral Grid Lower Box-->
								<div class="referral_transaction_details">
                               
									<div class="referral_transaction_details_text" style="margin-top:10px">
                                    
										<?php
										if(in_array($ref->agent_b, $sigs)) {
											//echo '2sigs';
											$linkable = 'opennext text-link';
											$data = 'data="'.$ref->referral_info->buyer->buyer_id.'"';
											$clientname = stripslashes_all($ref->referral_info->buyer->name);
										}
										else{
											//echo 'notosigs';
											$linkable = '';
											$data = '';
											$clientname = format_name_global(stripslashes_all($ref->referral_info->buyer->name));
										}
										?>
										<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_CLIENT') ?></div>
										<div class=" left <?php echo $linkable?>" <?php echo $data;?>><?php echo $clientname ?></div>
                                        
								    </div>
									<div class="referral_transaction_details_text">
										<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_INT') ?></div>
										<div class=" left"><?php echo JText::_($ref->intention); ?></div>
									</div>
									
									<div class="referral_transaction_details_text price_h">
                                    <?php if($ref->intention!="Leasing" && trim($ref->price_1)){?>
										<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_PRICE') ?></div>
										<div class=" left">
											<?php echo (trim($ref->price_1) == trim($ref->price_2)) ? format_currency_global($ref->price_1,$ref->symbol) : format_currency_global($ref->price_1,$ref->symbol)." - ".format_currency_global($ref->price_2,$ref->symbol); ?><?php echo " ".$ref->correctcurr;?>
										</div>
                                       <?php } ?>&nbsp;
									</div>
									<?php if(in_array($ref->agent_b, $sigs)) { ?>
										<?php if($notMobile) {?>
											<a class="client_download_grid <?php echo $linkable?>" <?php echo $data;?> > </a>
										<?php } else {?>
											<a class="client_download_grid" href="<?php echo JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>"> </a>
										<?php } ?>
									<?php  } ?>
									<div class="l_update" style="margin-top:5px"><span class="text-link history" data="<?php echo $ref->referral_id ?>" >
										<?php
											$gmdate = gmdate($this->d_format, strtotime($ref->history[0]->created_date )-25200);
											echo JText::_('COM_REFERRAL_TXNNOTES')
										?> </span></div>
								</div>
								<div class="view_ref" style="font-size:11px">
									<?php if(isset($ref->docusign_envelope) && $ref->docusign_envelope!=""){ ?>
										<a style="font-size:11px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>"><?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>(<?php echo $ref->referral_fee?>)</a>
									<?php } else { ?>
										<?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?> (<?php echo $ref->referral_fee; ?>)
									<?php } ?>
									
								</div>
							</div>
							
							<div class="rbox_list">
								<div class="l_rbox">
									<div class="clear-float">
                               			<!--Referral List Left Box-->
										
											<?php if ($ref->other_user->image!="") { ?>
													<div class="referral_user_pic left">
													<img  width="60px" src="<?php echo strpos($ref->other_user->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $ref->other_user->image).'?'.microtime(true) :  JURI::base().'uploads/thumb_'.$ref->other_user->image.'?'.microtime(true)?>"></div>
											
											<?php } else { ?>
													<div class="referral_user_pic left">
													<img  width="60px" class="resize" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" ></div>
												
											<?php } ?>
											
										<div class="referral_transaction_details_list">
										
											<div class="referral_transaction_details_text">
												<h2><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$ref->other_user->id)?>"> <?php echo stripslashes_all($ref->other_user->name)?></a></h2>
											</div>
											<div class="referral_transaction_details_text" style="margin-top:10px">
												<?php
													if(in_array($ref->agent_b, $sigs)) {
														//echo '2sigs';
														$linkable = 'opennext text-link';
														$data = 'data="'.$ref->referral_info->buyer->buyer_id.'"';
														$clientname = $ref->referral_info->buyer->name;
													}
												   else{
														//echo 'notosigs';
														$linkable = '';
														$data = '';
														$clientname = format_name_global($ref->referral_info->buyer->name);
													}
													?>
												<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_CLIENT') ?></div>
												<div class="left <?php echo $linkable?>" <?php echo $data;?>><?php echo $clientname ?></div>
												<?php if(in_array($ref->agent_b, $sigs)) { ?>
													<!-- <a class="client_download_list left" href="<?php echo JRoute::_('index.php?option=com_activitylog')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>">
                                                    <img src="templates/agentbridge/images/download_icon.png"/> </a> -->
                                                    <?php if($notMobile) {?>
														<a class="client_download_list left <?php echo $linkable?>" <?php echo $data;?>>
                                                  			<img src="templates/agentbridge/images/download_icon.png"/> 
                                                  		</a>
													<?php } else {?>
														<a class="client_download_list left" href="<?php echo JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>">
                                                    		<img src="templates/agentbridge/images/download_icon.png"/> 
                                                    	</a>
													<?php } ?>
                                                    
												<?php } ?>
												<div class="referral_transaction_details_text">
													<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_INT') ?></div>
													<div class="left"><?php echo JText::_($ref->intention); ?></div>
												</div>
                                                <div class="referral_transaction_details_text">
			                                    	<?php if($ref->intention!="Leasing" && trim($ref->price_1)){?> 	
														<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_PRICE') ?></div>
														<div class="left"><?php echo (trim($ref->price_1) == trim($ref->price_2)) ? format_currency_global($ref->price_1,$ref->symbol) : format_currency_global($ref->price_1,$ref->symbol)." - ".format_currency_global($ref->price_2,$ref->symbol)." ".$ref->other_user->userregsa->currency; ?></div>
													<?php } ?>&nbsp;
												</div>
											</div>
										</div>
										
                                        <!--Referral List Right Box-->
										<div class="referral_user_details_list">
											<div class="left" style="margin-bottom: 10px">
												<?php if($ref->intention!="Leasing" && trim($ref->price_1)){?>
												<?php } ?>
												<div class="status-box" style="margin-bottom:30px">
												<?php  
												 if ($changeable=="changestatus cursor-point") {?> 
												<div id="updatestatus2" onMouseover="ddrivetip('You may only update the status of this referral after you have signed the Referral Agreement');hover_dd()" onMouseout="hideddrivetip();hover_dd()" class="left <?php echo  strtolower($ref->label)." ".$changeable?> "
												data-id="<?php echo $ref->referral_id ?>"
												data="<?php echo $ref->status ?>"></div>
												<?php } else { ?>
												<div class="left <?php echo strtolower($ref->label)."".$changeable?>" data-id="<?php echo $ref->referral_id ?>" data="<?php echo $ref->status ?>"><?php echo JText::_($ref->label) ?></div>
												<?php } ?>
												</div>
                                                <div class="referral_transaction_details_text" style="margin-top:10px">
												<?php if(count($ref->history)){ ?>
												<div class="c_name"><?php echo JText::_('COM_POPS_TERMS_LU') ?>:</div>
												<div class="left">
													<span class="text-link history" data="<?php echo $ref->referral_id ?>">
													<?php
														$gmdate = gmdate($this->d_format, strtotime($ref->history[0]->created_date )-25200);
														echo $ref->history[0]->created_date;
													?></span></div>
												<?php } ?>
											    </div>
												<div class="clear-float" style="font-size:11px">
													<!--
													<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>"><?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>
                                                    (<?php echo $ref->referral_fee?>)</a>
													-->
													<?php if(isset($ref->docusign_envelope) && $ref->docusign_envelope!=""){ ?>
														<a style='font-size:11px;' href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>"><?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>(<?php echo $ref->referral_fee?>)</a>
													<?php } else { ?>
														<?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?> (<?php echo $ref->referral_fee; ?>)
													<?php } ?>
												</div>
												
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php
						if(!((++$count)%3)){
										//echo "<div class='clear-float'></div>";
						}
						if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;	
						}	
					}
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;
						$cell_flag = 0;
					}
					?>
				</ul>
			</div>
			<?php } ?>
			<div class="clear-float ref_out_div"
			<?php echo (count(($this->refin))) ? 'style="padding-top: 30px;"' : '' ?>>
				<?php if((count($this->refin)+count($this->refout))) { ?>
				<div class="referral_title"><?php echo (count($this->refout) > 1) ? JText::_('COM_USERPROF_PROF_REFSOUT') : JText::_('COM_USERPROF_PROF_REFOUT'); ?></div>
				<div class="noresultsfilter" style="display: none;">
						<p>No results found</p>
					</div>
				<?php }?>
				<?php if(count(($this->refout))) { 
					$count=0;?>
				<ul class="recommendations">
					<?php foreach($this->refout as $ref) {
						//echo json_encode($ref);
					
						
						$printdesig = "";
						$limit = (count($ref->other_user->desigs) > 3) ? 3 : count($ref->other_user->desigs);
						$lastcm = $limit - 1;
						for($i = 0; $i<$limit; $i++){
								$printdesig .= $ref->other_user->desigs[$i]->designations;
								if($i<$lastcm)
									$printdesig .= ',';
							}
							?>
					<?php $sigs = explode(',', $ref->referral_info->signatories); ?>
					<?php 
						if(!$cell_flag) {
							echo "<div class='referral_row' id='referral_row$row_flag'>";
						}
					?>
					<li class="ref_out_li refs_all <?php echo strtolower($ref->status)?>_items">
						<div class="rbox ">
							<div class="rbox_grid <?php echo strtolower($ref->label)?>_items">
                            
								<input type="hidden" id='<?php echo $ref->referral_id ?>'
                                	value='<?php echo json_encode($ref->history, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE); ?>'/>
								<input type="hidden" value="<?php echo isset($ref->overview) ? $ref->overview : "no overview yet" ?>"
									id="overview-<?php echo $ref->referral_id?>"/> 
                                    
                                <input type="hidden" value='<?php echo stripslashes_all($ref->referral_info->buyer->name); ?>'
									id='client-<?php echo $ref->referral_id ?>'/>
								
                                <!--Referral Grid Upper Box-->
								
								<div class="referral_user">
									<?php if ($ref->other_user->image!="") { ?>
													<div class="referral_user_pic left">
													<img  width="60px" src="<?php echo strpos($ref->other_user->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $ref->other_user->image).'?'.microtime(true) :  JURI::base().'uploads/thumb_'.$ref->other_user->image.'?'.microtime(true)?>"></div>
											
									<?php } else { ?>
													<div class="referral_user_pic left">
													<img  width="60px" class="resize" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" ></div>
												
									<?php } ?>
									<div class="referral_user_details">
										<h2><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$ref->other_user->id)?>"> <?php echo stripslashes_all($ref->other_user->name)?></a></h2>
                                        <div class="referral_user_details_city"><?php echo $ref->printaddress?></div>
										<div class="left" style="margin-bottom: 10px">
											<div class="status-box" >
											<?php $cursor = ($classes[($ref->status-1)]=="closed") ? "cursor-point" : ""; 
												 if ($classes[($ref->status-1)]=="closed") {?> 
												<div
                                            	onMouseover="ddrivetip('Status of referral as updated by the other agent');hover_dd()"
												onMouseout="hideddrivetip();hover_dd()"
												id="updatestatus3"
												class="left <?php echo $classes[($ref->status-1)]?> change2 <?php echo $cursor ?>"
												data-id="<?php echo $ref->referral_id ?>"
												data="<?php echo $ref->status ?>">
                                            </div>
											<?php } else { ?>
											<div class="<?php echo $cursor ?> left <?php echo strtolower($ref->label) ?>no_power" data-id="<?php echo $ref->referral_id ?>" data="<?php echo $ref->status ?>"><?php echo JText::_($ref->label) ?></div>
											<?php } ?>
											</div>
										</div>
									</div>
								</div>
								
                                 <!--Referral Grid Lower Box-->
								<div class="referral_transaction_details">
									<div class="referral_transaction_details_text" style="margin-top:10px">
										<?php
											$linkable = 'opennext text-link';
											$data = 'data="'.$ref->referral_info->buyer->buyer_id.'"';
											$clientname = stripslashes_all($ref->referral_info->buyer->name);
										?>
										<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_CLIENT') ?></div>
										<div class="referral_transaction_details_actual left <?php echo $linkable?>" <?php echo $data?>>
											<?php echo $clientname ?>
										</div>
									</div>
									<div class="referral_transaction_details_text">
										<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_INT') ?></div>
										<div class="left">
											<?php echo JText::_($ref->intention); ?>
										</div>
									</div>
									<div class="referral_transaction_details_text price_h">
                                    	<?php if($ref->intention!="Leasing" && trim($ref->price_1)){?> 	
											<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_PRICE') ?></div>
											<div class="left">
												<?php echo (trim($ref->price_1) == trim($ref->price_2)) ? format_currency_global($ref->price_1,$ref->symbol) : format_currency_global($ref->price_1,$ref->symbol)." - ".format_currency_global($ref->price_2,$ref->symbol)?><?php echo " ".$ref->currency; ?>
											</div>
										<?php } ?>&nbsp;
									</div>
									<!-- <a class="client_download_grid" href="<?php echo JRoute::_('index.php?option=com_activitylog')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>"> </a> -->
									<?php if($notMobile) {?>
											<a class="client_download_grid <?php echo $linkable?>" <?php echo $data;?> > </a>
										<?php } else {?>
											<a class="client_download_grid" href="<?php echo JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>"> </a>
										<?php } ?>
									<div class="l_update" style="margin-top:5px">
									<?php if(count($ref->history)) { ?>
									<span class="text-link history" data="<?php echo $ref->referral_id ?>">
									<?php
										$gmdate = gmdate($this->d_format, strtotime($ref->history[0]->created_date )-25200);
										echo JText::_('COM_REFERRAL_TXNNOTES')
									?></span>
									<?php } ?></div>
								</div>
								<div class="view_ref" style="font-size:11px">
									<?php if(isset($ref->docusign_envelope) && $ref->docusign_envelope!=""){ ?>
										<a style='font-size:11px;' href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>"><?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>(<?php echo $ref->referral_fee?>)</a>
									<?php } else { ?>
										<?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?> (<?php echo $ref->referral_fee; ?>)
									<?php } ?>
									
								</div>
							</div>
							<div class="rbox_list">
								<div class="l_rbox">
									<div class="clear-float">
                                    	<!--Referral List Left Box-->
										<?php if ($ref->other_user->image!="") { ?>
													<div class="referral_user_pic left">
													<img  width="60px" src="<?php echo strpos($ref->other_user->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $ref->other_user->image).'?'.microtime(true) :  JURI::base().'uploads/thumb_'.$ref->other_user->image.'?'.microtime(true)?>"></div>
											
											<?php } else { ?>
													<div class="referral_user_pic left">
													<img  width="60px" class="resize" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" ></div>
												
										<?php } ?>
										<div class="referral_transaction_details_list">
											<div class="referral_transaction_details_text">
												<h2><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$ref->other_user->id)?>"> <?php echo stripslashes_all($ref->other_user->name)?></a></h2>
											</div>
											<div class="referral_transaction_details_text" style="margin-top:10px">
												<div class="referral_transaction_details_labels left">Client:</div>
												<div class="left <?php echo $linkable?>" <?php echo $data?>>
													<?php echo stripslashes_all($ref->referral_info->buyer->name) ?>
												</div>
                                               <!--  <a class="client_download_list" href="<?php echo JRoute::_('index.php?option=com_activitylog')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>">
                                                <img src="templates/agentbridge/images/download_icon.png"/> </a> -->
                                                <?php if($notMobile) {?>
														<a class="client_download_list left <?php echo $linkable?>" <?php echo $data;?>>
                                                  			<img src="templates/agentbridge/images/download_icon.png"/> 
                                                  		</a>
													<?php } else {?>
														<a class="client_download_list left" href="<?php echo JRoute::_('index.php?option=com_propertylisting')."?task=dlvcard&id=".$ref->referral_info->buyer->buyer_id; ?>">
                                                    		<img src="templates/agentbridge/images/download_icon.png"/> 
                                                    	</a>
													<?php } ?>
											</div>
											<div class="referral_transaction_details_text">
												<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_INT') ?></div>
												<div class="left">
													<?php echo JText::_($ref->intention); ?>
												</div>
											</div>
											<div class="referral_transaction_details_text">
		                                    	<?php if($ref->intention!="Leasing" && trim($ref->price_1)){?> 	
													<div class="referral_transaction_details_labels"><?php echo JText::_('COM_REFERRAL_TERMS_PRICE') ?></div>
													<div class="left">
														<?php echo (trim($ref->price_1) == trim($ref->price_2)) ? format_currency_global($ref->price_1,$ref->symbol) : format_currency_global($ref->price_1,$ref->symbol)." - ".format_currency_global($ref->price_2,$ref->symbol)." ".$ref->other_user->userregsa->currency; ?>
													</div>
												<?php } ?>&nbsp;
											</div>
										</div>
										<!--Referral List Right Box-->
										
										<div class="referral_user_details_list">
											<div class="left" style="margin-bottom: 10px">
												<?php if($ref->intention!="Leasing" && trim($ref->price_1)){?>
												<?php } ?>
												<div class="status-box" style="margin-bottom:30px">
												<?php 
												 if ($classes[($ref->status-1)]=="closed") {?> 
												<div
                                            	onMouseover="ddrivetip('Status of referral as updated by the other agent');hover_dd()"
												onMouseout="hideddrivetip();hover_dd()"
												id="updatestatus4"
												class="left <?php echo $classes[($ref->status-1)]?> change2 <?php echo $cursor ?>"
												data-id="<?php echo $ref->referral_id ?>"
												data="<?php echo $ref->status ?>">
												</div>
												<?php } else { ?>
												<div class="<?php echo $cursor ?> left <?php echo strtolower($ref->label) ?>no_power" data-id="<?php echo $ref->referral_id ?>" data="<?php echo $ref->status ?>"><?php echo JText::_($ref->label) ?></div>
												<?php } ?>
												</div>
                                                <div class="clear-float referral_transaction_details_text" style="margin-top:10px">
                                                	<div class="c_name"><?php echo JText::_('COM_POPS_TERMS_LU') ?>:</div>
                                                    <div class="text-link history left" data="<?php echo $ref->referral_id ?>">
															<?php
															echo $ref->history[0]->created_date
															?>
													</div>
                                                </div>
												<div class="clear-float" style="font-size:11px">
													<!--
													<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>">
                                                    <?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>(<?php echo $ref->referral_fee?>)</a>
													-->
									<?php if(isset($ref->docusign_envelope) && $ref->docusign_envelope!=""){ ?>
										<a style="font-size:11px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=agreement_ab'); ?>&ref_id=<?php echo $ref->referral_id?>"><?php echo JText::_('COM_REFERRAL_VIEW_AGREEMENT') ?>(<?php echo $ref->referral_fee?>)</a>
									<?php } else { ?>
										<?php echo JText::_('COM_REFERRAL_TERMS_PEND') ?> (<?php echo $ref->referral_fee; ?>)
									<?php } ?>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<?php
						if(!((++$count)%3)){
							//		echo "<div class='clear-float'></div>";
						}
						if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;	
						}
					}
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;
						$cell_flag = 0;
					}
					?>
				</ul>
				<?php } else { ?>
					<?php if(((count($this->refin))+(count($this->refout))) || isset($_POST['filter'])) {?>
						</br> <?php echo JText::_('COM_USERPROF_PROF_NOREFS') ?>
					<?php } else {?>
					<div id="nopops" class="left" style="margin: auto">
						<?php echo JText::_('COM_REFERRAL_NONE') ?><?php echo (count(($this->refin))) ? ' ' : '' ?><br /> 
	                    <a
							href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newreferral'); ?>"
							class="button gradient-green left addrefbtn"
							style="margin-top:10px; display: block; padding-top:8px"> <span class="refer-me-text" style="margin-left:-5px; padding-left:20px;"><?php echo JText::_('COM_USERPROF_PROF_CREATER') ?></span>
						</a>
					</div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="clear-float"></div>	
            	
       </div>
	</div>
	<?php include './includes/sidebar_left.php' ?>
	<!-- end wide-content -->
</div>
<div id="history" style="display: none; text-align:center; font-size:14px; margin-top:10px"></div>

<div id="changestatus" style="display: none">
	<?php echo JText::_('COM_REFERRAL_SELECTSTATUS2') ?><br /> <select style="width:270px" id="changestatusselect"
		onchange="checkNogo(this.value)">
		<option value=""><?php echo JText::_('COM_REFERRAL_SELECTSTATUS') ?></option>
	</select> <br /><br/> <select id="reason" class="optional"
		style="display: none"
	> 
		<option value=""><?php echo JText::_('COM_REFERRAL_SELECTREASON') ?></option>
		<option value="1"><?php echo JText::_('COM_REFERRAL_REASON1') ?></option>
		<option value="2"><?php echo JText::_('COM_REFERRAL_REASON2') ?></option>
		<option value="3"><?php echo JText::_('COM_REFERRAL_REASON3') ?></option>
		<option value="4"><?php echo JText::_('COM_REFERRAL_REASON4') ?></option>
	</select>
	<span class="notelabel"><?php echo JText::_('COM_REFERRAL_TXNNOTES') ?></span><br />
	<textarea  style="width:100%" id="note" class="optional" style="display: none"></textarea>
	<input type="hidden" id="tochangeid" /> <input type="hidden"
		id="value_id"
	/>
	<div class="clear-float"></div>
	<div class="right" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-changestatus1">
		<img height="26px" id="loading-image_custom_question"
			src="https://www.agentbridge.com/images/ajax_loader.gif"
			alt="Loading..."
			class="right"
		/>
	</div>
	<a href="javascript:void(0)" id="submit-changestatus1" class="button gradient-green right"
		onclick="submitStatusChange()" style="margin-top: 10px; margin-right:20px; width:120px; padding-top:7px; height:30px"
	><?php echo JText::_('COM_NRDS_FORMSUBMIT') ?></a>
	<div class="clear-float"></div>
</div>

<div id="authorizenet" style="display: none">
	
     <!-- Commission Window -->
    <form id="clientinput">
		<?php echo JText::_('COM_REFERRAL_GROSS') ?> <span class="client"
			style="font-weight: bold"
		></span>. <br/><br /><input  type="text" style="width: 100%" id="ertwocomm"
			placeholder="0"
		><br />
       
        <span style="display:none; margin-top:13px" id="grossc_message" class="error_msg left">You need to enter an amount.</span>
		<a href="javascript:void(0)"
			id="nextgrosscomm"
			class="button gradient-green right" onclick="calculateSummary()"
			style="margin-top: 10px; padding: 5px 10px; width:100px; height:30px"
		>Next</a>
		<div class="right" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-grosscomm">
			<img height="26px" id="loading-image_custom_question"
				src="https://www.agentbridge.com/images/ajax_loader.gif"
				alt="Loading..."
				class="right"
			/>
		</div>
	</form>
    
    <!-- Saved Card -->
    <div class="popsaved" style="display:none">
    		<div class="radiobuttons">
             <div id="summarysaved" style="display:none">
                <h1>Summary of Transaction</h1>
				<p style="font-size:12px">
				<br/>The <span class="clientname"></span> referral fee of <span class="yourcomm" ></span> is ready to to be disbursed.
				AgentBridge will now be collecting the service fee of <span class="total">a</span>. Confirm or enter broker info and CC#, expiry date, and
				security code.<br /> <br />
				<span>Your Gross Commission:</span> <span id="rtwocomm" >a</span><br/>
                <span>Commission of <span class="r1name"> :</span> <span id="ronecomm" >a</span><br/>
				<span id="transaction">Service Fee :</span> <span id="amounttopay">a</span><br/></p></div><br/>
				<input  type="radio" name="usesaved" value="1" id="use" /> <label style="margin-right:20px; font-size:14px"  for="use">Use saved card information <span class="savedcard"></span></label>
				<input  type="radio" name="usesaved" value="0" id="nouse"/> <label style="font-size:14px" for="nouse ">Use another card</label>
			</div>
			
			<br/>
			<div class="usesavedsubmit" style="display:none">
				<div style="line-height:18px;">
					<span> Payment Types Accepted <br />
						<img src="images/payment.jpg" width="148" height="12"/>
					</span><br />
					<input name="agree_terms_saved" type="checkbox" value="1" id="agree_terms_saved" style="margin-right:5px"/><label for="agree_terms_saved"  style="font: 11px 'OpenSansRegular',Arial,sans-serif; width:150px"> I
							have read and agree to the <span style="font: 11px 'OpenSansRegular',Arial,sans-serif;" class="text-link" onclick="popTerms()">Terms and Conditions</span></label><br />
                     <div style="display:none; margin-top:10px" class="agree_terms2 error_msg">You need to agree to our Terms before you proceed</div>
                      <div style="display:none; margin-top:10px" class="card_message2 error_msg">Some of the credit card details are now invalid. Please use a new card.</div>
					<a style="margin-top: 30px; height:30px; padding-top:5px" onclick="submitsavedPayment()" id="submit-usesaved" class="alwaysshow button gradient-green left" href="javascript:void(0)">Submit</a>
					<div class="left" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-submit-usesaved">
						<img height="26px" id="loading-image_custom_question"
							src="https://www.agentbridge.com/images/ajax_loader.gif"
							alt="Loading..."
							class="right"
						/>
					</div>
				</div>
			</div>
        </div>
	<div class="clear-float"></div>
</div>

<div id="authorizenet2" style="display:none;">
	<!-- New Card -->
    <form id="summary" style="display: none">
		<div class="pop">
			<div class="authorizefields">
				<div class="clear-float">
				<div class="pcolumn1">
                
                <h1>Summary of Transaction</h1>
				<p style="font-size:12px">
				<br/>The <span class="clientname"></span> referral fee of <span class="yourcomm" ></span> is ready to to be disbursed.
				AgentBridge will now be collecting the service fee of <span class="total">a</span>.<br/><br/>
				Confirm or Enter broker info and CC#, Exp. Date, and
				Security Code.<br /> <br />
				<span>Your Gross Commission:</span> <span id="rtwocomm" >a</span><br/>
                <span>Commission of <span class="r1name"> :</span> <span id="ronecomm" >a</span><br/>
				<span id="transaction">Service Fee :</span> <span class="total">a</span><br/></p>
		       
            			<br/><br/>
            
						<h2>Agent and Brokerage Details</h2>
                        
						<div id="agentLicenseNumber"></div>
						<div id="brokerLicense"></div>
						<h3 >Broker Info</h3>
						<input type="text" id="bslno"
						    data-panel="panel1"
							onMouseover="ddrivetip('This field is autopopulated. Please enter your Broker State License number <br/>if field is showing blank.');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="Broker State License Number"
						/> <br /> <input type="text" id="alslno"
							data-panel="panel1"
							onMouseover="ddrivetip('This field is autopopulated. Please enter your Agent License number <br/>if field is showing blank.');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="Associate/License State License Number"
                           
						/> <br /> 
						<input 
							type="text" 
							id="btino"
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your Brokerage TAX ID number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="left text-input ptext_input"
							placeholder="Broker Tax ID Number"
                            
						/> <p class="btino error_msg" style="margin-left:0px;display:none"> This field is required  <br /> <br /></p>
					</div>
					
					<div class="pcolumn2">
                    
                    	<h2>Cardholder Details</h2>
						<h3>Personal</h3>
						<input 
							type="text" 
							id="firstname"
						    data-panel="panel1" 
                            onMouseover="ddrivetip('Enter the first name that appears on your card');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input" 
							placeholder="Cardholder First Name"
                            
						/> <br/> 
						<p class="firstname error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p>
						<input 
						
							type="text" 
							id="lastname"
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter the last name that appears on your card');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input" 
							placeholder="Cardholder Last Name"
         
                            
						/> <br/>
						<p class="lastname error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p>
						<input 
							type="text" 
							id="email"
							validator="0" 
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your email address');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input ptext_input validateme"
							placeholder="Email"
          
                            
						/> <br/>
						<p class="email error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p>
						<!-- class="text-input ptext_input validateme numbermask" -->
						<label class="countCode" ></label>
						<input 
							type="text" 
							id="phone"
						    validator="1" 
							data-panel="panel1"
                            onMouseover="ddrivetip('Enter your phone number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input numbermask2"
							placeholder="Phone Number"
                     		style="width: 228px;"
                            
						/>
						<p class="phone error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p>
						<h3 style="margin-top:20px">Billing Address</h3>
						
						   <select id="jform_country" 
                           onMouseover="ddrivetip('Country');hover_dd()"
						   onMouseout="hideddrivetip();hover_dd()"
							data-panel="panel2" 
                            style="width:160px; margin-left:-10px"
							name="jform[country]"
							>
							<option value=""></option>
							<?php
							foreach($this->country_list as $country_list):
						
							echo "<option data=\"".$country_list->countries_iso_code_2."\" value=\"".$country_list->countries_id."\" >".$country_list->countries_name."</option>";
							endforeach;
							?>
						</select>
                        	<input style="width:93px" type="text" id="zip" 
                            onMouseover="ddrivetip('Enter your Zip')"
							onMouseout="hideddrivetip();hover_dd();hover_dd()"
						    data-panel="panel2" class="text-input sptext_input"
							placeholder="Zip"
                          	value="<?php echo $this->user_info->zip ?>"
						/>
						<div class="clear-float"></div>
						<p class="zip error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p>
                        <input type="text" id="address1" data-panel="panel2" 
                        onMouseover="ddrivetip('Enter your street address');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						class="text-input ptext_input" placeholder="Address 1"
                      
						/> <br/>
                        <span><p class="address1 error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p></span>
                        
                         <input type="text" id="address2"
                            class="left text-input ptext_input"
							placeholder="Address 2"
                           
						/> <br/> 
                         <div class="clear-float"></div>  
						 <input 
						 	style="margin-right:10px;width:260px"
							type="text" 
							id="city"
							data-panel="panel2" 
                            class="left text-input sptext_input"
							placeholder="City"
							value="<?php echo $this->user_info->city ?>"
						/> 
                        <div class="clear-float">
						<p class="city error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p></div>
						
						<div class="clear-float"></div>
						<input 
							type="text"
							id="state"
						    data-panel="panel2" 
							class="text-input stext_input" 
                            placeholder="State"
							style="margin-left:-10px"
                          
						/> 
						<div class="clear-float">
						<p class="state error_msg" style="display:none; margin-left:10px"> This field is required  <br /> <br /></p></div>
					</div>
					
					<div class="pcolumn3">
                    	
                        
						<h2>Order Summary</h2>
						<h3>&nbsp;</h3>
						<div class="order_box">
							<div id="item_label" class="order_service">
								<strong>Service</strong> <br /> <br /> AgentBridge <br /> Referral
								Service <br /> Fee
							</div>
							<div class="order_qty">
								<strong>QTY</strong> <br /> <br /> 1
							</div>
							<div class="order_sub">
								
								<strong>Subtotal</strong> <br /> <br /> <span class="orig_total"></span>
							</div>
							
							
							<div id="discount_item_label" style="padding-top:10px;margin-left:10px;clear:both">
										
							</div>
						</div>
						
						<div class="order_service">
							<strong>TOTAL </strong>
						</div>
						<div class="order_qty">&nbsp;</div>
						<div class="order_subx order_sub total">$xx</div>
                        
                        <br/><br/>
						<h2>Card Details</h2>
						<h3>&nbsp;</h3>
						<input 
							
							type="text" 
							id="card_number"
                            onMouseover="ddrivetip('Enter your credit card number');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" 
							data-panel="panel3"
							class="ptext_input text-input" 
							value="" 
							placeholder="Card Number (99999999999)"
						/> <br/>
						<p class="card_number error_msg" style="margin-left:0px;display:none"> This field is required  <br /> <br /></p>
						<input type="text" 
						
								id="card_expiry"
                                onMouseover="ddrivetip('Enter card expiry');hover_dd()"
								onMouseout="hideddrivetip();hover_dd()"
								data-panel="panel3"
								class="sptext_input text-input" 
								placeholder="MM-YYYY" 
								value="" 
								style="margin-right:10px; width:125px"
                    
						/> 
						
						<input 
								type="text" 
								id="security_number"
                                onMouseover="ddrivetip('Enter the security number located at the back of your credit card');hover_dd()"
								onMouseout="hideddrivetip();hover_dd()"
								data-panel="panel3"
								class="sptext_input text-input" 
								value="" 
								style="width:125px"
                                placeholder="Security Number"
						/>
						<div style="float:left;width:135px;margin-top:-17px;"> &nbsp; <p class="card_expiry error_msg" style="display:none"> This field is required </p></div>
						<div style="float:left"><p class="security_number error_msg" style="display:none"> This field is required </p></div>
						<div style="clear:both"></div>
						<div style="line-height:18px; width:290px">
							<span> Payment Type Accepted<br /> <img src="images/payment.jpg"
								width="148" height="12"
							/>
							</span> <br /><br /><input name="agree_terms" type="checkbox" value="1" id="agree_terms" style="margin-right:5px"/><label  style="font: 11px 'OpenSansRegular',Arial,sans-serif" for="agree_terms"> I agree to the <span style="font: 11px 'OpenSansRegular',Arial,sans-serif;" class="text-link" onclick="popTerms()">Terms and Conditions</span></label> <br />
                            
							<div style="clear:both"></div>							
							<input name="save_trans" type="checkbox" value="1" id="save_trans"  style="margin-right:5px;"/><label for="save_trans" style="font: 11px 'OpenSansRegular',Arial,sans-serif"> Save
								payment details for future transactions </label> <br /><br />
								<div style="float:left"><p style="display:none" class="agree_terms error_msg"> You need to agree to our Terms before you proceed </p></div>
                                <div style="float:left"><p style="display:none" class="card_message error_msg"> Some of the credit card details are invalid. <br/>Please check again. </p></div>
		
	                            <a style="margin-top: 10px; padding-top:5px; padding-left:5px;width:120px; height:30px" onclick="submitPayment()" class="button gradient-green right" href="javascript:void(0)" id="submit-nosaved">Submit</a>
								<div class="left" style="margin-top: 13px; padding-left: 10px; display:none" id="loading-image-submit-nosaved">
									<img height="26px" id="loading-image_custom_question"
										src="https://www.agentbridge.com/images/ajax_loader.gif"
										alt="Loading..."
										class="right"
									/>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			
	</form>
</div>

<div style="display: none" id="popupdetails"><!-- <br/>
	<?php echo JText::_('COM_REFERRAL_NAME') ?> <span class="cc_name"></span><br />  -->
	<div class="reftriangle"></div>
	<br/>
	<?php echo JText::_('COM_REFERRAL_CONTACTNUMBER') ?> <span
		class="cc_no"
	></span><br /> <?php echo JText::_('COM_REFERRAL_EMAIL') ?> <span class="cc_email"></span><br /><span class="cc_note"></span><br/>
</div>
<div id="termsandconditions" class="left" style="display:none; font-size:12px">
<?php echo JText::_('COM_USERPROF_CC_TERMSCON') ?>
</div>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/moment.js"/></script>
<script>
	mixpanel.track("Referrals");
	
	function popTerms(){
		jQuery("#termsandconditions").dialog(
			{
				modal:true, 
				width: "100%",
				title: "Terms and Conditions",
				});
	}
	function checkNogo(val){
		jQuery(".optional").hide();
		jQuery(".notelabel").hide();
		if(val==5){
			jQuery("#reason").show();
		}else{
			jQuery("#note").show();
			jQuery(".notelabel").show();
		}
	}
	function calculateSummary(){
		if(jQuery("#ertwocomm").val()==""){
			jQuery("#grossc_message").show();
			return false;
		}
		jQuery("#loading-image-grosscomm").show();
		jQuery("#nextgrosscomm").hide();
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=calculatefee')?>",
			type: 'POST',
			data: 	{ 
						'amount'	: jQuery("#ertwocomm").autoNumeric('get'),
						'ref_id'		: jQuery('#tochangeid').val()
					},
			success: function(response){
			
			
				//var is_premium_global 	= <?php echo $this->user_info->is_premium; ?> 
				
					jQuery("#loadingimage_custom").hide();
					console.log(response);
					//jQuery("#clientinput").hide();
				// if ( is_premium_global==1 ) { 					
					
					var values = jQuery.parseJSON(response);
					if(values.authorize!=null){
						jQuery(".savedcard").html(values.authorize.paymentProfile.payment.creditCard.cardNumber);
						jQuery(".authorizefields").hide();
						jQuery(".popsaved").show();
						jQuery("#use").click()
						usesaved = true;
					}
					else{
						jQuery(".radiobuttons, .usesavedsubmit").remove();
						usesaved = false;
						jQuery.placeholder.shim();
					}
				//	jQuery('select#jform_country').select2('destroy').select2().change();
					jQuery("#rtwocomm").html(values.rtwocomm+" USD");
					
					var is_premium 	= <?php echo $this->user_info->is_premium ? $this->user_info->is_premium : "''"; ?>;
					var status 		= <?php echo $this->refin[0]->status ? $this->refin[0]->status : "''"; ?>;
					var refin_count = <?php echo $this->refin_closed_count ? $this->refin_closed_count : "''"; ?>;
					
					if( refin_count==0 ) {
						jQuery("#amounttopay").html(values.amount+" USD");
						jQuery(".total").html(values.amount+" USD");
						jQuery(".orig_total").html(values.amount+" USD");
						jQuery("#discount_item_label").html(""+
							"<div style='color:#007BAE;margin-left:37px;font-size:11px;'>- Discount (100%) "+"<span  style='margin-left:50px;'>"+values.amount+" USD</span>"
						);
						values.amount = "$0";
					} else{
						jQuery("#amounttopay").html(values.amount+" USD");
						jQuery(".total").html(values.amount+" USD");
						jQuery(".orig_total").html(values.amount+" USD");					
					
					}
				
					jQuery(".order_subx").html(values.amount+" USD");
					jQuery("#ertwocomm").html(values.amount+" USD");
					jQuery('#ronecomm').html(values.ronecomm+" USD");
					jQuery(".clientname").html(values.clientname);
					jQuery(".r1name").html(values.r1);
					zipblur();
					jQuery("#summary")
						.show().parent().parent().center();
					jQuery("#authorizenet").dialog('close');
					jQuery("#authorizenet2").dialog({
						modal:true,
						width: "1200",
						position:{
							my: "center",
							at: "center",
							of: window
						},
						title:"Referral Payment Transaction",
					});

			}
		})
	}
	
	
function calculateSummary_nouse(){
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=calculatefee')?>",
			type: 'POST',
			data: 	{ 
						'amount'	: jQuery("#ertwocomm").autoNumeric('get'),
						'ref_id'		: jQuery('#tochangeid').val()
					},
			success: function(response){
					jQuery("#loadingimage_custom").hide();
					console.log(response);
					jQuery("#clientinput").hide();
					usesaved = false;
					jQuery.placeholder.shim();
					
					jQuery('select#jform_country').select2('destroy').select2().change();
					jQuery("#amounttopay").html(values.amount+" USD");
					jQuery("#rtwocomm").html(values.amount+" USD");
					jQuery(".total").html(values.amount+" USD");
					jQuery(".orig_total").html(values.amount+" USD");					
				
					jQuery(".order_subx").html(values.amount+" USD");
					jQuery("#ertwocomm").html(values.amount+" USD");
					jQuery('#ronecomm').html(values.ronecomm+" USD");
					jQuery(".clientname").html(values.clientname);
					jQuery(".r1name").html(values.r1);
					zipblur();
					jQuery("#summary")
						.show().parent().parent().center();

			}
		})
	}
	function jsonpCallback(data){
		console.log(data);
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val()).css('margin-left','-10px');
		jQuery("#city").val(data.postalcodes[0].placeName);
		jQuery("#s2id_state").remove();
		try{
			jQuery("#jform_state").select2('val', jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		}catch(e){}
	}
	function setValidators(){
		jQuery(".validateme").blur(function(){
			var regex = Array();
			regex[0] = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
			regex[1] = /[0-9-()+]{3,20}/;
			console.log(regex[jQuery(this).attr('validator')]);
			jQuery(this).val(this.value.match(regex[jQuery(this).attr('validator')]));
		});
		jQuery('input').keydown(function(){
			jQuery(this).qtip('destroy');
		});
	}
	
	function zipblur(){
		/*jQuery("#zip").blur(function(){
			console.log('https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+jQuery(this).val()+'&country='+jQuery('#jform_country').find(":selected").attr('data')+'&username=damianwant33');
			jQuery.ajax({
                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+jQuery(this).val()+'&country='+jQuery('#jform_country').find(":selected").attr('data')+'&username=damianwant33',
                dataType: 'jsonp',
                jsonp: 'callback',
                jsonpCallback: 'jsonpCallback',
                success: function(){
                }
            });
		});*/
		setValidators();
	}
	function form_validate(name){
		if(!(jQuery("#nouse").length) || jQuery("#nouse").is(":checked")){
			jQuery("#"+name).find('input, select').each(function(){
				if(jQuery(this).attr('required')!=undefined){
					if(jQuery(this).val()==0||jQuery(this).val()==""){
						jQuery("#"+jQuery(this).attr('data-panel')).slideDown();
						jQuery(this).on('focusout', function(){jQuery(this).removeClass('focus');});
						jQuery(this).addClass('focus');
						jQuery(this).focus();
						return false;
					}
				}
			});
		}
		else{
			if(!jQuery('#agree_terms_saved').attr('checked')){
				jQuery('.agree_terms2').show();
				return false;
			}
		}
		
		return true;
	}
	
	function submitsavedPayment(){
		var ctr = 0;
		if( !$jquery('#agree_terms_saved').is(':checked') ) {
				ctr++;
				$jquery(".agree_terms2").show();
			} else {
				$jquery(".agree_terms2").hide();
			}
		
		console.log( ctr );
			
		if(ctr==0){
		
		jQuery("#submit-usesaved").hide();
		jQuery("#loading-image-submit-usesaved").show();	
		jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=submitsavedpayment')?>",
					type: 'POST',
					data: 	{ 
							
						'ref_id'			: jQuery('#tochangeid').val(),
						'amount'			: jQuery("#ertwocomm").autoNumeric('get'),
						'usesaved'			: true,
					},
					success: function(response){
						console.log(res);
						var res = jQuery.parseJSON(response);
						console.log(response);
						//TODO remove comment
						if(res[0]==1)
							changeStatus();
					
						else{
							//alert('Error: '+res[3]);
							jQuery(".card_message2").show();
							jQuery("#loading-image-submit-usesaved").hide();
							jQuery("#submit-usesaved").show();
							jQuery("#submit-nosaved").show();
							jQuery("#loading-image-submit-nosaved").hide();
						}
					}
			});
		
		}
	}
	
	function submitPayment(){
		var ctr = 0;
		if(form_validate("summary")) {
			if($jquery("#btino").val()==""){$jquery("#btino").addClass("glow-required");$jquery(".btino").show();
				$jquery("#btino").focus();
				ctr++;
			} else { $jquery("#btino").removeClass("glow-required");$jquery(".btino").hide(); }
			
			if($jquery("#zip").val()==""){$jquery("#zip").addClass("glow-required");$jquery(".zip").show();
				$jquery("#zip").focus();
				ctr++;
			} else { $jquery("#zip").removeClass("glow-required");$jquery(".zip").hide(); }
			
			if($jquery("#address1").val()==""){$jquery("#address1").addClass("glow-required");$jquery(".address1").show();
				$jquery("#address1").focus();
				ctr++;
			} else { $jquery("#address1").removeClass("glow-required");$jquery(".address1").hide(); }
			if($jquery("#state").val()==""){$jquery("#state").addClass("glow-required");$jquery(".state").show();
				$jquery("#state").focus();
				ctr++;
			} else { $jquery("#state").removeClass("glow-required");$jquery(".state").hide(); }			
			
			if($jquery("#city").val()==""){$jquery("#city").addClass("glow-required");$jquery(".city").show();
				$jquery("#city").focus();
				ctr++;
			} else { $jquery("#city").removeClass("glow-required");$jquery(".city").hide(); }
			
			if(
				$jquery("#card_number").val()=="" ||
				$jquery("#card_number").val()==""
			
			){$jquery("#card_number").addClass("glow-required");$jquery(".card_number").show();
				$jquery("#card_number").focus();
				ctr++;
			} else { $jquery("#card_number").removeClass("glow-required");$jquery(".card_number").hide(); }
			
			if(
				$jquery("#card_expiry").val()=="" ||
				$jquery("#card_expiry").val()=="MM-YYYY"
			){$jquery("#card_expiry").addClass("glow-required");$jquery(".card_expiry").show();
				$jquery("#card_expiry").focus();
				ctr++;
			} else { $jquery("#card_expiry").removeClass("glow-required");$jquery(".card_expiry").hide(); }
			
			if($jquery("#security_number").val()==""){$jquery("#security_number").addClass("glow-required");$jquery(".security_number").show();
				$jquery("#security_number").focus();
				ctr++;
			} else { $jquery("#security_number").removeClass("glow-required");$jquery(".security_number").hide(); }
		
			if($jquery("#phone").val()==""){$jquery("#phone").addClass("glow-required");$jquery(".phone").show();
				$jquery("#phone").focus();
				ctr++;
			} else { $jquery("#phone").removeClass("glow-required");$jquery(".phone").hide(); }
		
			if($jquery("#email").val()==""){$jquery("#email").addClass("glow-required");$jquery(".email").show();
				$jquery("#email").focus();
				ctr++;
			} else { $jquery("#email").removeClass("glow-required");$jquery(".email").hide(); }
		
			if($jquery("#lastname").val()==""){$jquery("#lastname").addClass("glow-required");$jquery(".lastname").show();
				$jquery("#lastname").focus();
				ctr++;
			} else { $jquery("#lastname").removeClass("glow-required");$jquery(".lastname").hide(); }			
			
			if($jquery("#firstname").val()==""){$jquery("#firstname").addClass("glow-required");$jquery(".firstname").show();
				$jquery("#firstname").focus();
				ctr++;
			} else { $jquery("#firstname").removeClass("glow-required");$jquery(".firstname").hide(); }
	
			if( !$jquery('#agree_terms').is(':checked') ) {
				ctr++;
				$jquery(".agree_terms").show();
			} else {
				$jquery(".agree_terms").hide();
			}
			
			console.log( ctr );
			
			if(ctr==0){
			
				jQuery("#loading-image-submit-usesaved").show();
				jQuery("#submit-usesaved").hide();
				jQuery("#submit-nosaved").hide();
				jQuery("#loading-image-submit-nosaved").show();
			
				jQuery.ajax({
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=submitpayment')?>",
					type: 'POST',
					data: 	{ 
								'card_number'	: jQuery("#card_number").val(),
								'card_expiry'	: jQuery("#card_expiry").val(),
								'security_no'	: jQuery("#security_number").val(),
								'amount' 		: jQuery("#ertwocomm").autoNumeric('get'),
								'ref_id'		: jQuery('#tochangeid').val(),
								'firstname'		: jQuery("#firstname").val(),
								'lastname'		: jQuery("#lastname").val(),
								'email'			: jQuery("#email").val(),
								'phone'			: jQuery("#phone").val(),
								'address'		: jQuery("#address1").val()+" "+jQuery("#address2").val(),
								'city'			: jQuery("#city").val(),
								'zip'			: jQuery("#zip").val(),
								'country'		: jQuery("#jform_country").val(),
								'save_trans'	: jQuery("#save_trans").attr('checked'),
								'state'			: jQuery("#jform_state").val(),
								'usesaved'		: false,
								'btino'			: jQuery("#btino").val()
							},
							
					success: function(response){
						console.log(res);
						var res = jQuery.parseJSON(response);
						console.log(response);
						//TODO remove comment
						if(res[0]==1)
							changeStatus();
					
						else{
							//alert('Error: '+res[3]);
							jQuery(".card_message").show();
							jQuery("#loading-image-submit-usesaved").hide();
							jQuery("#submit-usesaved").show();
							jQuery("#submit-nosaved").show();
							jQuery("#loading-image-submit-nosaved").hide();
						}
					}
				});
			}
		}
	}
	
	function set_masks(){
		jQuery(".numbermask").inputmask("(999) 999-9999");
		jQuery(".numbermask2").inputmask("(999) 999-9999");
	}
	

	function changeStatus(){
		jQuery("#loading-image-changestatus1").show();
		jQuery("#submit-changestatus1").hide();
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=updatestatus')?>",
			type: 'POST',
			data: { 'value_id': jQuery('#value_id').val(), 'ref_id': jQuery('#tochangeid').val(), 'note':jQuery('#note').val(),'reason':jQuery('#reason').val(), 'status':jQuery('#changestatusselect').val()  },
			title: "<?php echo JText::_('COM_REFERRAL_CHANGE_STATUS_TIT')?>",
			success: function(response){
				console.log(response)
				jQuery("#loadingimage_custom").hide();
				try{
				jQuery('#changestatus').dialog('close');
				}catch(e){}
				if(jQuery('#changestatusselect').val()==4)
					window.location = "<?php echo JRoute::_('index.php?option=com_activitylog', true, -1); ?>";
				else
					location.reload();	
			}
		})
	}
	function submitStatusChange(){
		var status = jQuery.parseJSON(jQuery("#"+jQuery('#tochangeid').val()).val());
		var value_id = 0;
		if(status.length){
			value_id = status[0].value_id;
		}
		if(jQuery('#changestatusselect').val()==""){
			alert('please select status');
			return false;
		}
		
		if(jQuery('#reason').val()=="" && jQuery('#changestatusselect').val()==5){
			alert('please select reason');
			return false;
		}
		jQuery('#value_id').val(value_id);
		
		if(jQuery('#changestatusselect').val()==4){
		
			jQuery.ajax({
				url: "<?php echo JRoute::_('index.php?option=com_userprofile&task=get_current_registered_user'); ?>",
				type: 'POST',
				dataType: "json",
				data: {  },
				title: "<?php echo JText::_('COM_REFERRAL_CHANGE_STATUS_TIT')?>",
				success: function(response) {
					jQuery("#alslno").val(response.licence);
					jQuery("#bslno").val(response.brokerage_license);
					jQuery("#btino").val(response.tax_id_num);
					jQuery("#authorizenet").dialog({
						
						modal:true, 
						width: "auto",
						title:"Calculate Commission Split‏",
					});
					
					jQuery('#changestatus').dialog('destroy');
					jQuery("#loading-image-grosscomm").hide();
					jQuery("#nextgrosscomm").show();
	
					
				},
				error: function(){
					alert('error');
				}
			});
		
			return false;
		}
		else{
			jQuery('#value_id')
				.val(value_id)
				.promise()
				.done(changeStatus)
		}
	}
	
	
	jQuery(document).ready(function(){
		
		set_masks();
		jQuery("#comission").mask('99%');
		jQuery("#card_expiry").mask('99-9999');
	//	jQuery("#zip").keyup(function() {
	//		jQuery("#zip").val(this.value.match(/[0-9]*/));
	//	});
		
		
		jQuery("#jform_country").select2();
 		jQuery("#jform_country").select2("val","<?php echo $this->user_info->country ?>");

 		get_state(jQuery("#jform_country").val());


 		var sel_c='';

		/*	var options =  function(){
			var zip = jQuery("#zip").val();
			if(sel_c=="IE"){

				jQuery.ajax({
		                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
		                success: function(data){
		                	
		                	console.log(data[0]);
		                	jQuery("#jform_state option").filter(function() {

									if(this.text == data[0].county){
										jQuery("#jform_state").select2({ width: 'resolve' });
										jQuery("#jform_state").select2("val", this.value);
									}
								});				
							if(typeof data[0].posttown != 'undefined'){
								jQuery("#city").val(data[0].posttown);
							} else {
								jQuery("#city").val(data[0].dependentlocality);
							}
							jQuery("#s2id_state").remove();
		                }
		        }); 


			} else {
				jQuery.ajax({
			            url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+sel_c+'&username=damianwant33',
			            success: function(data){
			            	/*if(sel_c=='GB'){
								jQuery("#jform_state option").filter(function() {
									if(this.text == data.postalcodes[0].adminName3){
										jQuery("#jform_state").select2({ width: '100%' });
										jQuery("#jform_state").select2("val", this.value);
									}
								});
			            	} else {
			            		jQuery("#jform_state").select2("val", jQuery("#zone"+data.postalcodes[0].adminCode1).val());
			            	}
			            	if(state_variable=="adminCode1"){
	                			console.log(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
	                			jQuery("#jform_state").select2("val", jQuery("#zone"+data.postalcodes[0].adminCode1).val());
	                		} else if(sel_c=='MC'){	
		                		jQuery("#jform_state").select2("val", jQuery("#zoneMC").val());
		                	} else {
		                		console.log(state_variable);
		                		jQuery("#jform_state > option").filter(function() {				                			
									if(this.text == data.postalcodes[0][state_variable]) {
										jQuery("#jform_state").select2({ width: 'resolve' });
										jQuery("#jform_state").select2("val", this.value);
									} 
								});
		                	} 				
							jQuery("#city").val(data.postalcodes[0].placeName);
							jQuery("#s2id_state").remove();
			            }
			     }); 
			}
		};*/

		var baseurl = "<?php echo $this->baseurl?>";
		applyAddressDataPP(baseurl,jQuery("#jform_country").find(':selected').attr('data'),jQuery("#jform_country").val(),"initial");

		jQuery("#jform_country").change(function(){

			get_state(jQuery(this).val());

			jQuery("#zip").unbind("keyup");
			jQuery("#zip").val("");
			jQuery('#zip').unmask();
			jQuery('#city').val("");

			sel_c = jQuery(this).find(':selected').attr('data');

			applyAddressDataPP(baseurl,sel_c,jQuery(this).val());



		});

		jQuery("#nextgrosscomm").keyup(function() {
			jQuery("#nextgrosscomm").val(this.value.match(/[0-9]*/));
		});
		jQuery(".mobilenos").keyup(function() {
			jQuery(this).val(this.value.match(/[0-9 \-\(\)]+/));
		});
		jQuery("#security_number").keyup(function() {
			jQuery(this).val(this.value.match(/[0-9 \-\(\)]+/));
		});
		jQuery("#card_number").keyup(function() {
			jQuery(this).val(this.value.match(/[0-9 \-\(\)]+/));
		});
		
		jQuery('.changestatus').click(function(){
				
				var status = jQuery(this).attr('data');
			
				jQuery("#changestatusselect").html("");
				if(status==7){
				
					jQuery("#changestatusselect").append(""+
						"<option value='8'><?php echo JText::_('COM_REF_STATUS_ACTIVE'); ?></option>"+
						"<option value='6'><?php echo JText::_('COM_REF_STATUS_NEEDHELP'); ?></option>"+
						"<option value='5'><?php echo JText::_('COM_REF_STATUS_NOGO'); ?></option>"
					);
				}
				
				else if (status==8) {
				
					jQuery("#changestatusselect").append(""+
						"<option value='1'><?php echo JText::_('COM_REF_STATUS_UNDCONTRACT'); ?></option>"+
						"<option value='5'><?php echo JText::_('COM_REF_STATUS_NOGO'); ?></option>"+
						"<option value='6'><?php echo JText::_('COM_REF_STATUS_NEEDHELP'); ?></option>"
					);
				}
				
				else if (status==1) {
				
					jQuery("#changestatusselect").append(""+
						"<option value='4'><?php echo JText::_('COM_REF_STATUS_CLOSED'); ?></option>"+
						"<option value='6'><?php echo JText::_('COM_REF_STATUS_NEEDHELP'); ?></option>"+
						"<option value='8'><?php echo JText::_('COM_REF_STATUS_ACTIVE'); ?></option>"
					);
				}
				
				else if (status==6) {
				
					jQuery("#changestatusselect").append(""+
						"<option value='1'><?php echo JText::_('COM_REF_STATUS_UNDCONTRACT'); ?></option>"+
						"<option value='5'><?php echo JText::_('COM_REF_STATUS_NOGO'); ?></option>"+
						"<option value='8'><?php echo JText::_('COM_REF_STATUS_ACTIVE'); ?></option>"
					);
				} 
				
				if(status!=5){
					jQuery('select#changestatusselect').select2('destroy');
					jQuery('#tochangeid').val(jQuery(this).attr('data-id'));
					jQuery('.client').html(jQuery("#client-"+jQuery.trim(jQuery(this).attr('data-id'))).val());
					jQuery('select#reason').select2('destroy');
					jQuery("#reason").hide();
					jQuery('#changestatus').dialog(
						{
						  title: "<?php echo JText::_('COM_REFERRAL_CHANGE_STATUS_TIT') ?>",
						});
				
				}
				//}
			});
		
		jQuery('.change2').click(function(){
			var status = jQuery(this).attr('data');
			var changeable = ['4'];
			if(status==4){
				jQuery("#changestatusselect").html("");
				jQuery('select#changestatusselect').select2('destroy');
				jQuery("#changestatusselect").append(""+
						"<option value='9'><?php echo JText::_('COM_REF_STATUS_COMPLETED'); ?></option>"
					);
				jQuery('#tochangeid').val(jQuery(this).attr('data-id'));
				jQuery('select#reason').select2('destroy');
				jQuery("#reason").hide();
				jQuery('#changestatus').dialog(
						{
						  title: "<?php echo JText::_('COM_REFERRAL_CHANGE_STATUS_TIT') ?>",
						});

			}
		});
		
		jQuery("span.history").click(function(){
			var status = ['Under Contract','Contacted','Working','Closed','No Go','Need Help','Pending','Active','Completed'];
			var history = jQuery.parseJSON(jQuery("#"+jQuery(this).attr('data')).val());
			var current_id = <?php echo JFactory::getUser()->id; ?>;
			//console.log(jQuery("#"+jQuery(this).attr('data')).html()); return;
			if(history.length < 2) {
				var html="<?php echo JText::_('COM_REFERRAL_STATUS_HISTORY') ?>"+jQuery("#client-"+jQuery(this).attr('data')).val()+": <?php echo JText::_('COM_REFERRAL_FROM') ?>"+history[history.length-1].name+" <?php echo JText::_('COM_REFERRAL_TO') ?>  "+history[history.length-1].other_name+"<br /><br />";
			} else {
				var html="<?php echo JText::_('COM_REFERRAL_STATUS_HISTORY') ?>"+jQuery("#client-"+jQuery(this).attr('data')).val()+": <?php echo JText::_('COM_REFERRAL_FROM') ?>"+history[history.length-1].name+" <?php echo JText::_('COM_REFERRAL_TO') ?>  "+history[history.length-2].name+"<br /><br />";
			}
			
			var reasons = ['Client unresponsive','Client working with another agent','Client\'s needs changed','Retracted'];
			html += "<table class='tbl_notes'>";
			for(var i=0; i<history.length; i++){
				console.log(history[i].created_date);
			//	var date = history[i].created_date.split(' ')[0];
			//	var time = history[i].created_date.split(' ')[1];
			//	date = date.split('-');
			//	time = time.split(':');
				//console.log(new Date(date[0], date[1], date[2], time[0], time[1], time[2], 0))
				if(history[i].status == 5)
					history[i].note = reasons[history[i].note-1];
				html += "<tr class='ref_popup'>";
				//var d = moment(new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2], 0));console.log(d.format('M/D/YYYY h:m A'));"m/d/Y g:i:s A"
				//html += "	<td class='date_update'> " + moment(new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2], 0)).format('<?php echo $this->d_format;?>') + "</td>";
				html += "	<td class='date_update'> " + history[i].created_date + "</td>";
			//	html += "	<td class='date_update'> " + jQuery.datepicker.formatDate('dd M yy',new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2], 0)) + "</td>";
				if(history[i].r_signed != 0 || history[i].o_signed != 0) {
					if(history[i].r_signed == 1) {
						if(history[i].created_by != current_id) {
							html += "	<td class='status_change'> "+ history[i].name +" <?php echo JText::_('COM_REFERRAL_HAS_SIGNED') ?> <?php echo JText::_('COM_REFERRAL_CLIENT_CONTACT_DETAILS_RELEASED') ?></td>";
						} else {
							html += "	<td class='status_change'> <?php echo JText::_('COM_ACTIVITY_REF_YOU'); ?> <?php echo JText::_('COM_REFERRAL_HAVE_SIGNED') ?> " + jQuery("#client-"+jQuery(this).attr('data')).val() + "'s <?php echo JText::_('COM_REFERRAL_NAME_CONTACT_DETAILS_RELEASED') ?></td>";
						}
					} else {
						if(history[i].created_by != current_id) {
							html += "	<td class='status_change'> "+ history[i].name +" <?php echo JText::_('COM_REFERRAL_HAS_SIGNED') ?></td>";
						} else {
							html += "	<td class='status_change'> <?php echo JText::_('COM_ACTIVITY_REF_YOU'); ?> <?php echo JText::_('COM_REFERRAL_HAVE_SIGNED') ?></td>";
						}
					}
				} else {
					if(history[i].status == 7) {
						if(history[i].response != 0 || (history[i].response == 0 && history[i].edited_by == history[i].created_by)) {
							if(history[i].created_by != current_id) {
								html += "	<td class='status_change'> "+ history[i].name +" <?php echo JText::_('COM_REFERRAL_HAS_SENT') ?></td>";
							} else {
								html += "	<td class='status_change'> <?php echo JText::_('COM_REFERRAL_SENT') ?></td>";
							}
						} else {
							if(history[i].created_by != current_id) {
								html += "	<td class='status_change'>" + history[i].name + "<?php echo JText::_('COM_REFERRAL_HAS_ACCEPTED_START') ?> " + history[i].name + " <?php echo JText::_('COM_REFERRAL_HAS_ACCEPTED_MIDDLE') ?> " + jQuery("#client-"+jQuery(this).attr('data')).val() +  "<?php echo JText::_('COM_REFERRAL_HAS_ACCEPTED_END') ?></td>";
							} else {
								html += "	<td class='status_change'> <?php echo JText::_('COM_REFERRAL_ACCEPTED'); ?></td>";
							}
						}
					} else {
						if(history[i].created_by != current_id) {
							html += "	<td class='status_change'> "+ history[i].name +" <?php echo JText::_('COM_REFERRAL_HAS_CHANGED') ?> <strong>" + status[history[i].status-1] + "</strong></td>";
						} else {
							html += "	<td class='status_change'> <?php echo JText::_('COM_ACTIVITY_REF_YOU'); ?> <?php echo JText::_('COM_REFERRAL_HAVE_CHANGED') ?> <strong>" + status[history[i].status-1] + "</strong></td>";
						}
					}
				}
				
				
				
				html += "	<td class='notes'> " + ((i!=(history.length-1)) ? history[i].note : "") + "</td>";
							
				html += "</tr>";
			}
			html += "</table>";
			jQuery("#history").html(html).dialog (
					{
						modal:true, 
						width: "auto",
						title: "<?php echo JText::_('COM_REFERRAL_TXNNOTES') ?>",
					});
		});
		jQuery("strong.overview").click(function(){
			html = jQuery("#"+jQuery(this).attr('data')).val();
			jQuery("#history").html(html).dialog({modal:true, width: 300});
		});
		
		jQuery("#changeview a").click(function(){
			jQuery("div.items-filter .left .view").toggleClass('bg-bottom');
			jQuery("div.referral_row").toggleClass('l_rbox');
			if(jQuery("div.referral_row").hasClass('l_rbox')){
				jQuery("#v_style").val('list');
				jQuery('.rbox').removeAttr("style");

			} else {
				jQuery("#v_style").val('grid');
						  // Get an array of all element heights
				  var elementHeights = jQuery('.rbox').map(function() {
					return jQuery(this).height();
				  }).get();

				  // Math.max takes a variable number of arguments
				  // `apply` is equivalent to passing each height as an argument
				  var maxHeight = Math.max.apply(null, elementHeights);

				  // Set each height to the max height
				  jQuery('.rbox').css("min-height",(maxHeight+10)+"px");
			}
			Landing.format();
		});
		jQuery(".opennext").click(function(){
			var buyer = jQuery(this).attr('data');
			var thisclicked = jQuery(this);
			jQuery.ajax({
				url: "<?php echo JRoute::_('index.php?option=com_activitylog') ?>?task=json_encode_card",
				type: 'POST',
				data: { 'b_id': buyer },
				success: function(e){
					var details = jQuery.parseJSON(e);
					jQuery(".cc_name").html(details.name);
					jQuery(".cc_no").html(details.mobile);

					if(details.note)
						jQuery(".cc_note").html("Note: "+details.note);
					else
						jQuery(".cc_note").html("");

					var email = details.email.split(',');
					var i=0;
					var formated="";
					for(i=0; i<email.length; i++){
						formated+='<a style="color:#009bd0" href="mailto:'+email[i]+'">'+email[i]+'</a> ';
					}
					jQuery(".cc_email").html(formated).parent().dialog(
						{
							modal:false,							
							position: { my: "left top", at: "left-2 bottom+10", of: thisclicked, collision:"none", },
							open: function( event, ui ) {
								/*jQuery(".ui-dialog").center()*/
								jQuery(".ui-dialog").appendTo("#content");
								jQuery(".ui-dialog").css("z-index",0);
							/*	jQuery('.ui-dialog').dialog("option", "position", [offest.left, offest.top+height]);
								jQuery('.ui-dialog').dialog('open');*/
							},
							title: details.name,
						});
					//jQuery("#popupdetails")
					console.log(details.name);
				}
			})
		});
	});
	function filter(fil){
		jQuery("#filter").submit();
	}
	var $ajax = jQuery.noConflict();
	
	function get_state(cID, state){
		$ajax("#state").html("loading...");
		$ajax("#jform_state").select2('destroy');
		$ajax.ajax({
			url: '<?php echo $this->baseurl ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID, 'state': state },
			success: function(e){
				console.log(e);
				var change="#state";
				if($ajax("#state").length < 1)
					change="#jform_state";
				$ajax(change).replaceWith(
					jQuery(e).addClass('mc_select')
						.addClass('left'))
						.promise()
						.done(
							function () {
								$ajax("#jform_state").removeAttr('required');
								$ajax("#jform_state").css('width', '260px');
								$ajax("#jform_state").val("<?php echo $this->user_info->state ?>");
								$ajax("#jform_state").select2();
								$ajax("#jform_state").select2("val","<?php echo $this->user_info->state ?>");
							}
						);
			}
		});
	}
	var usesaved=true;
	jQuery(document).ready(function(){
		jQuery("#nextgrosscomm").keyup(function() {
			jQuery("#nextgrosscomm").val(this.value.match(/[0-9]*/));
		});
		jQuery("input[name='usesaved']").click(function(){
			if(!(jQuery(this).val()>0)){
				usesaved=false;
				jQuery(".authorizefields").show();
				jQuery(".usesavedsubmit").hide();
				jQuery("#summarysaved").hide();
				calculateSummary_nouse();
			}
			else{
				usesaved=true;
				jQuery(".authorizefields").hide();
				jQuery(".usesavedsubmit").show();
				jQuery("#summarysaved").show();
			}
		});
		jQuery("#ertwocomm").autoNumeric('init', {mDec: '0'});    
		jQuery("#ertwocomm").autoNumeric('init', {aSign:'<?php echo $this->user_info->symbol; ?>', mDec: '0'});
		jQuery("#ertwocomm").attr("placeholder", "<?php echo $this->user_info->symbol; ?>0 <?php echo $this->user_info->currency; ?>");
		
		jQuery("#ertwocomm").blur(function(){
			jQuery(this).val("<?php echo $this->user_info->symbol; ?>"+jQuery(this).val()+" <?php echo $this->user_info->currency; ?>");
		});
		
		
		jQuery(".referral_row").each(function(index){
			var current_id = jQuery(this).attr("id");
			
			// Get an array of all element heights
			var elementHeights = jQuery('#' + current_id + ' .rbox').map(function() {
				return jQuery(this).height();
			}).get();

			// Math.max takes a variable number of arguments
			// `apply` is equivalent to passing each height as an argument
			var maxHeight = Math.max.apply(null, elementHeights);

			// Set each height to the max height
			jQuery('#' + current_id + ' .rbox').css("min-height",(maxHeight+10)+"px");	
			
		});
	});
</script>
