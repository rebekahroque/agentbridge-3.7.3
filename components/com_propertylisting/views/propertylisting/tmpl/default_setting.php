<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();	
$user =& JFactory::getUser($_SESSION['user_id']);
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
?>
<style>body{background: #fff;}</style>
<div class="wrapper">
	<div class="wide left"><!-- start wide-content -->
		<h1>POPs&#8482; Settings</h1>
		<form id="pocket_setting" action="<?php echo JRoute::_('index.php?option=com_propertylisting&task=setting'); ?>" method="post" class="form-validate form-horizontal">	
		
			<input type="hidden" name="jform[form]" value="pocket_setting" />
		
			<div class="setting">
				<ul>
					<li class="setting-list">
						<input type="radio" id="jform_setting1" name="jform[setting]" value="1" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==1 || !$this->setting->selected_permission==1) ? "checked='true'" : "") ?> />
						<label>All agents</label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting2" name="jform[setting]" value="2" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==2) ? "checked='true'" : "") ?> />
						<label>My AgentBridge Network </label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<div class="left">
							<input type="radio" id="jform_setting3" name="jform[setting]" value="3" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==3) ? "checked='true'" : "") ?> />
							<label>Any agent who has a side over</label>
						</div>
						<span class="c90 left">
							<select id="jform_price1" name="jform[price1]" >
								<option value="">Select Price</option>
								<option <?php echo (($this->setting->selected_permission==3 && $this->setting->condition1 =="opt1") ? "selected='true'" : "") ?> value="opt1">Option 1</option>
								<option <?php echo (($this->setting->selected_permission==3 && $this->setting->condition1 =="opt2") ? "selected='true'" : "") ?> value="opt2">Option 2</option>
								<option <?php echo (($this->setting->selected_permission==3 && $this->setting->condition1 =="opt3") ? "selected='true'" : "") ?> value="opt3">Option 3</option>
								<option <?php echo (($this->setting->selected_permission==3 && $this->setting->condition1 =="opt4") ? "selected='true'" : "") ?> value="opt4">Option 4</option>
							</select> 
						</span>
						<div class="left" style="margin-left:-20px;">
						last 24 months.
						</div>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
                    	<div class="left" style="margin-bottom:10px;">
						<input type="radio" id="jform_setting4" name="jform[setting]" value="4" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==4) ? "checked='true'" : "") ?> />
						<label>Any agent with more than  </label>
                        </div>
						<span class="c90 left">
							<select id="jform_price2" name="jform[price1]" >
								<option value="">Select Price</option>
								<option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition1 =="opt1") ? "selected='true'" : "") ?> value="opt1">Option 1</option>
								<option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition1 =="opt2") ? "selected='true'" : "") ?> value="opt2">Option 2</option>
								<option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition1 =="opt3") ? "selected='true'" : "") ?> value="opt3">Option 3</option>
								<option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition1 =="opt4") ? "selected='true'" : "") ?> value="opt4">Option 4</option>
							</select> 
						</span>
						<p class="left" style="margin-left:-20px;">and</p> 
						<div class="clear-float"></div>
                        <div style="margin-top:10px;">
                            <p class="left margin-left">average sales price higher than </p>
                            <span class="c90 left">
                                <select id="jform_price3" name="jform[price2]" >
                                    <option value="">Select Price</option>
                                    <option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition2 =="opt1") ? "selected='true'" : "") ?> value="opt1">Option 1</option>
                                    <option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition2 =="opt2") ? "selected='true'" : "") ?> value="opt2">Option 2</option>
                                    <option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition2 =="opt3") ? "selected='true'" : "") ?> value="opt3">Option 3</option>
                                    <option <?php echo (($this->setting->selected_permission==4 && $this->setting->condition2 =="opt4") ? "selected='true'" : "") ?> value="opt4">Option 4</option>
                                </select> 
                            </span>
                            <p class="left" style="margin-left:-20px; ">in the last 24 months </p>
                        </div>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
                   		<div style="margin-bottom:10px;">
						<input type="radio" id="jform_setting5" name="jform[setting]" value="5" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==5) ? "checked='true'" : "") ?> />
						<label class="push-bottom">All Agents who have any sides in the last 24 months in zip code(s) � separated by commas</label>
                        </div>
						<div class="c540">
							<input type="text" class="text-input" id="jform_zip" name="jform[zip]" value="<?php echo (($this->setting->selected_permission==5) ? $this->setting->condition1 : ""); ?>" />
						</div>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting6" name="jform[setting]" value="6" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==6) ? "checked='true'" : "") ?> />
						<label>Only my brokerage</label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting7" name="jform[setting]" value="7" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==7) ? "checked='true'" : "") ?> />
						<label>Only my state</label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting8" name="jform[setting]" value="8" onclick="document.getElementById('pocket_setting').submit()" <?php echo (($this->setting->selected_permission==8) ? "checked='true'" : "") ?> />
						<label>Only in my country </label>
						<div class="clear-float"></div>
					</li>
				</ul>
			</div>
		</form>
			
	</div><!-- end wide-content -->
	<?php 
		include( 'includes/sidebar_left.php' );
	?>
</div>
<script>
	jQuery(window).load(function(){
		jQuery("section.main-content").css('min-height', '700px');
		jQuery("section.main-content").height(jQuery(document).height()-jQuery("section.header").height()-jQuery("section.footer").height());
		jQuery("div.blocks a.user-avatar img").each(function(){
			var parent = jQuery(this).parent();
			if(jQuery(this).width()>jQuery(this).height()){
				jQuery(this).css('height',parent.height());
				jQuery(this).css('margin-left', -(jQuery(this).width()-parent.width())/2);
				jQuery(this).css('margin-top', '0px');
				
			}
			else{
				jQuery(this).css('width',parent.width());
				jQuery(this).css('margin-top', '0px');
				jQuery(this).css('margin-left', '0px');
			}
		});
	});
</script>
<?php ?>