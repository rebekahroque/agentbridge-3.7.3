<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$user =& JFactory::getUser($_SESSION['user_id']);
$uid = $user->id;
$referrable = true;
$user_referral = preg_grep('/'.JFactory::getUser()->id.'.*/', $this->referrers);
if(!empty($user_referral)){
	$referrable = !count(array_diff($user_referral, array(JFactory::getUser()->id.'-5')));
}
?>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/easySlider1.8.js" ></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/jquery.tabify.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/fancybox2/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/magnificPopup/magnificPopup2.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/fancybox2/jquery.fancybox.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/magnificPopup/magnific-popup.css"/>
<style>
body {
	background: #fff;
}
span.price {
	margin-bottom:15px !important;
}
div.folded h2{
	background: #390;
    background-image: radial-gradient(transparent 30%, rgba(0, 0, 0, 0));
    border: 0 solid rgba(0,0,0,0.2);
	border-radius: 2px;
    color: #fff;
    font-size: 20px;
    font-weight: normal;
    position: relative;
    text-shadow: -1px -1px 1px rgba(0,0,0,0.2);
}
div.folded:before, div.folded:after {
    bottom: 26px;
    box-shadow: 0 28px 8px rgba(0,0,0,0.5);
    content: "";
    height: 28px;
    position: absolute;
    width: 48%;
    z-index: -1;
}
div.folded:before {
    left: 2%;
    transform: rotate(-3deg);
}
div.folded:after {
    right: 2%;
    transform: rotate(3deg);
}
div.folded h2 {
    box-shadow: 0 0px 1px rgba(0,0,0,0);
    margin: 0;
    padding: 6px 40px;
    position: absolute;
    right: -7px;
    top: 12px;
}
div.folded h2:after {
    border-width: 3px;
    border-style: solid;
    border-color: #390 transparent transparent #390;
    bottom: -4px;
    content: "";
    position: absolute;
    right: 0px;
}
#sold_text {
	color:#3399cc;
	cursor: pointer;
}

.fancybox-next {
    right: -45px;
}

.fancybox-prev {
    left: -45px;
}

#saved_buyer_section, #available_section {
	min-width:500px;
}
</style>
<?php #echo "<pre>"; print_r($this->buyers); die(); ?>
<div class="wrapper">
	<div class="wide left popsindividual"><!-- start wide-content -->
	<?php if(JFactory::getUser()->id==$this->data->user_id){?>
		<h1><?php echo JText::_('COM_POPS_VIEW_MY') ?></h1>
		<div class="border-dot">
			<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=view'); ?>" class="button gradient-gray return-listing"><span class="return-listing-text"><?php echo JText::_('COM_POPS_VIEW_RETURN') ?></span></a>
			<a href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>" class="button gradient-blue add-pocket"><?php echo JText::_('COM_USERPROF_PROF_ADDP') ?></a>
			<?php if($this->existing_buyer_count>=1) { ?>
			<a onclick="savePOPs()" class="button gradient-blue add-pocket" onMouseover="ddrivetip('Save POPs&trade; to one or more of your Buyers');hover_dd()" onMouseout="hideddrivetip();hover_dd()"href="javascript:void(0);"> <?php echo JText::_('COM_NRDS_FORM_SAVEPOPS') ?></a>
			<?php } ?>
		</div>
	<?php }else { 
		// if logged in user view other properties
		$viewed_pops = (empty($_GET['uid'])) ? '' : $_GET['uid'];
		$r2 = $this->owner->firstname;
		//component/propertylisting/?task=view&uid=1010
	?>	
		<h1 class='text-link'><a style="font-size: 28px; font-family: 'OpenSansLight', Arial, sans-serif" href='<?php echo JRoute::_("index.php?option=com_userprofile&task=profile&uid=".$this->owner->id); ?>'><?php echo JText::sprintf('COM_POPS_VIEW_R2VIEW', stripslashes($r2)) ?></a></h1>
		<div class="border-dot">
		<a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->owner->id); ?>" class="button gradient-gray return-listing"><span class="return-listing-text"><?php echo JText::sprintf('COM_POPS_VIEW_RETURNR1', stripslashes($this->owner->firstname))?></a>
		<?php 
		if($this->existing_buyer_count>=1) { ?>
		<a onclick="savePOPs()" class="button gradient-blue add-pocket" href="javascript:void(0);"> <?php echo JText::_('COM_NRDS_FORM_SAVEPOPS')?></a>
		<!--<div class="clear-float right" style="width=300px">
		<div class="left" style="margin-right:10px" >
		<?php if ($this->owner->image!=="") { ?>
		<a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->owner->id); ?>">
			 <img height="50px" src="<?php echo str_replace("loads/", "loads/thumb_",$this->owner->image); ?>"></a></div>
	    <?php } else { ?>
		     <img style="width:60px;" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" />
		<?php } ?>
		<div class="right">
			<a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->owner->id); ?>" class="text-link"><?php echo stripslashes_all($this->owner->name) ?></a>
			<?php if ($this->owner->verified_2012==1 && $this->owner->verified_2013==1 ) { ?>
					<div class="clear-float" style="margin-top:5px;margin-left:5px "><img width="60px" src="<?php echo $this->baseurl ?>/images/agent-bridge-verified.jpg" onMouseover="ddrivetip('AgentBridge has verified this agent&lsquo;s information');hover_dd()" onMouseout="hideddrivetip();hover_dd()"></div>
			<?php } ?>
		 </div>
		</div>-->
		</div>
		<?php } ?>
	<?php } ?>
		<div id="top-def-individual" style="margin-top:30px">
			<div class="large-item-details left">
				<?php if($this->data->sold==1){?>
					<div class="folded"><h2>
						<?php if ($this->data->property_type==1 || $this->data->property_type==3) {?>
							<?php echo JText::_('COM_POPS_TERMS_SOLD') ?>
						<?php } else {?> <?php echo JText::_('COM_POPS_TERMS_LEASED') ?> <?php }?></h2></div>
				<?php }?>
			<?php ?>
				<div id="slider_adin" style="height:217px">
						<ul>
						<?php
						$ti=1;
							foreach($this->images as $image){
								if(trim($image->image)) {
						?>
						<li>
							<a class="grouped_elements" rel="group1" href="#" id="image_<?php echo $ti?>">
							<img src="<?php echo trim($image->image); ?>?true" alt="" <?php echo (isset($image->default)) ? "class=\"popsindiphoto\"" : "class=\"popsindiphoto\""; ?> /></a>
						</li>
						<?php 
									$ti++;
								}
							}
						?>
					</ul>
				</div>
				<div style="display:none">
					<ul>
						<?php
						$li=1;
							foreach($this->images as $image){
								if(trim($image->image)) {
							?>
						<li>
							<a data-fancybox-type="image" class="image_<?php echo $li?> fancybox_images" rel="group2" href="<?php echo trim($image->image); ?>">
							<img src="<?php echo trim($image->image); ?>?true" alt="" <?php echo (isset($image->default)) ? "class=\"popsindiphoto\"" : "class=\"popsindiphoto\""; ?> /></a>
						</li>
						<?php 
									$li++;
								}
							}
						?>
						</ul>
				</div>
				<span class="left">Photo <span id="currslide">1</span> of <span id="maxslide"><?php echo count($this->images) ?></span></span>
				<span id="larger" class="right large-photo"><?php echo JText::_('COM_POPS_VIEW_LARGERPHOTO')?></span>
			</div>
			<div class="item-details left">
				<?php 
							$flagicon="";
							if($this->user_info->country!=$this->data->country){
								if($this->data->country == 0 && $this->user_info->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($this->data->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($this->countryIso).'-flag-lang.png">';
								}								
							}
				?>
				<a class="item-number bold"><?php echo $flagicon." ".$this->data->zip ?></a>
				<span class="reality-description">
					<?php echo stripslashes_all($this->data->property_name); ?><br/>
					<?php echo JText::_($this->data->proptype_name); ?> - <?php echo JText::_($this->data->subtype_name); ?>
				</span>
				<div class="clear-float"><br /></div>
					<span class="price">
						<?php 
							if($this->data->disclose == 1 || JFactory::getUser()->id == $this->data->user_id) {
								echo format_currency_global($this->data->price1,$this->data->symbol); 
								echo (!empty($this->data->price2) ? " - " . format_currency_global($this->data->price2,$this->data->symbol) : ""). " ".$this->correct_currency ; 
							} else {
								echo 'price undisclosed';
							}
						?>
					</span>
					<div class="clear-float pocket-needs-individual"><?php	echo $this->pops_feats_init; ?>
					</div>
                    <div class="clear-float"><br /></div>
					<?php if(JFactory::getUser()->id!=$this->data->user_id) { ?>
					<div class="item-number bold" style="font-size:12px; margin-top:20px" ><a style="font-weight: bold;" href="<?php echo $this->baseurl.'/index.php/component/userprofile/?task=profile&uid='.$this->owner->id; ?>"><?php echo JText::sprintf('COM_POPS_VIEW_CONTACTR1', stripslashes($this->owner->firstname), stripslashes($this->owner->lastname))?>:</a> <br/></div>
					<?php if(count($this->mobile)>=1) { ?> 
					<span style="margin-top:10px"> 
					<?php
						$i=0;
						$hidelanguage = "";
						foreach ($this->mobile as $mobile){
									if($i>2)
										$hidelanguage="orighidden-mobile hideitem-mobile";
									if($i>0)
										echo '<label class="language '.$hidelanguage.'">, </label>';
									echo '<label class="language '.$hidelanguage.'">'.$this->data->countryCode.' '.$mobile->value.'</label>';
									$i++;
								}
								if($i==0)
									echo "mobile not provided";
								if($i>3)
									echo '<a id="morelabel-mobile" class="text-link show-mobile">&nbsp;(more)</a>'?>
					</span>
					<?php } ?>
					<span style="margin-top:10px" class="clear-float"><a href="mailto:<?php echo $this->owner->email ?>" class="text-link"><?php echo $this->owner->email ?></a><br/></span>
					<?php } ?>
					<?php if(JFactory::getUser()->id==$this->data->user_id){?>
					<div class="edit-buyer">
						<?php echo (($this->data->views>0)) ? '<span class="viewers" style="display:block;margin-bottom:10px;color:#3399cc" data=\''.$viewHTML.'\'> '.JText::_('COM_POPS_VIEW_SEENBY').' '.$this->data->views.'</span>' : ""; ?>
						<?php if($this->data->sold==1 ){?>
							<span id="sold_text" class="sold <?php echo $this->data->property_type ?> <?php echo $this->data->listing_id?>"><?php if ($this->data->property_type==1 || $this->data->property_type==3) {?>
							<?php echo JText::_('COM_POPS_TERMS_UNMARKS') ?> <?php } else {?> <?php echo JText::_('COM_POPS_TERMS_UNMARKL') ?><?php }?></span>
						<?php } else {?>
							<span id="sold_text" class="unsold <?php echo $this->data->property_type ?> <?php echo $this->data->listing_id?>"><?php if ($this->data->property_type==1 || $this->data->property_type==3) {?>
							<?php echo JText::_('COM_POPS_TERMS_MARKS') ?> <?php } else {?> <?php echo JText::_('COM_POPS_TERMS_MARKL') ?> <?php }?> </span>
						<?php } ?>
                    </div>
					<?php } ?>
                    <div class="edit-buyer">
					<?php if(JFactory::getUser()->id==$this->data->user_id){?>
							<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=editproperty&lID=' . $_REQUEST['lID']); ?>" class="button gradient-green edit-pocket"><em class="edit-pocket-text"><?php echo JText::_('COM_POPS_VIEW_EDITP') ?></em></a>
					<?php } ?>
                    </div>
			</div>
			<div class="clear-float"></div>
		</div>
			<div style="clear:both"></div>
		<div id="tabs">
			<ul id="menu2" class="pops_menu"><!-- start tab -->
				<li class="ui_border"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#general-info"><?php echo JText::_('COM_POPS_VIEW_GENERALINFO')?></a></li>
			</ul>
			<div id="general-info" class="content">
				<div class="padded">
					<p><?php echo (stripslashes_all(nl2br($this->data->description))) ? stripslashes_all(nl2br($this->data->description)) : "".JText::_('COM_USERPROF_NONE').""  ?></p>
				</div>
				<?php #echo "<pre>"; print_r($this->data); die(); ?>
				<div class="property-features">
					<h2 style="margin-bottom:10px;"><?php echo JText::_('COM_POPS_VIEW_PROPERTYFEATURES')?></h2>
					<ul class="left first-feature">
                       <?php 
                       	$it=1;
                       	foreach ($this->pops_feats_array as $key => $value) {
                       		echo "<li>".$value. "</li>";
                       		if($it==6){
                       			echo '</ul><ul  class="left" style="margin-left: 30px;">';
                       			$it=0;
                       		}
                       		$it++;
                       	}                     
						?>							
					</ul>
				</div>
			</div>
			<ul id="menu2" class="pops_menu"><!-- start tab -->
				<li class="ui_border"><a href="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#photo-gallery"><?php echo JText::_('COM_POPS_TERMS_PHOTO_GALLERY')?></a></li>
			</ul>
			<div id="photo-gallery" class="content">
			<div class="gallery-container">
				<ul>
					<?php
						foreach($this->images as $image){
							if(trim($image->image)) {
								echo "<li class=\"thumb gallery\" >";
								echo "<a class=\"gallery-item\" href=\"".trim($image->image)."?true\">";
								echo	"<img class=\"resize\" src=\"".trim($image->image)."?true\" style=\"width:150px\" />";
								echo "</a>";
								echo		"<span class=\"open\"></span>";
								echo "</li>";
							}
						}
					?>
				</ul>
			</div>
			</div><!-- end tab -->
		</div>
	</div><!-- end wide-content -->
	</div>
	<?php
		include( 'includes/sidebar_left.php' );
	?>
</div>
<div style="display: none" id="savebuyers">
	<!--<div id="save-buyer-title" style="margin-top:10px"><h3 style="font-size:12px;"> Select from list of Buyers</h3></div>-->
	<div class="buyers-modal" style="width:100%">
		<div id="buyer-modal-container">
		<?php 
			$buyers_arr = array();
			$buyers_arr_a = array();
			$buyers_arr_b = array();
			$buyers_arr_c = array();
			foreach($this->buyers as $buyer){ array_push($buyers_arr, $buyer); }
			$toralrows = round(count($buyers_arr));
			$count_half = ceil($total_rows/2);
			$x = 1;
			//$total_rows = count($fws_children);
			foreach($buyers_arr as $buyer){
				if($x <= 3) {
					$buyers_arr_a[] = $buyer;
				} else if($x > 3 && $x <= 6) {
					$buyers_arr_b[] = $buyer;
				} else {
					$buyers_arr_c[] = $buyer;
				} 
				$x++;
			}
		?>
		<div class="left" id="buyer-m-tbl" border="1" style="width:100%">
		<input type="hidden" id="to-activate" />
		<input type="hidden" id="to-deactivate" />
		<input type="hidden" id="listing_id" value="lID" />
				<div style="border-bottom:1px solid #bfbfbf;">
                <div class="col1-popsindi"></div>
				<div class="col2-popsindi" style="font-weight:bold;text-align:left"> <?php echo JText::_('COM_BUYERS_STATUS_OPT_ABUYER') ?></div>
                <div class="col3-popsindi" style="font-weight:bold;text-align:left"> <?php echo JText::_('COM_BUYERS_STATUS_OPT_BBUYER') ?></div>
                <div class="col4-popsindi" style="font-weight:bold;text-align:leef"> <?php echo JText::_('COM_BUYERS_STATUS_OPT_CBUYER') ?> </div>
                <div style="clear:both;"></div>   
				</div>
				<!--<?php if(count($this->buyers_i)) { ?>
					<div>INACTIVE</div>
				<?php } ?>-->
            	<div id="saved_buyer_section" class="border-dot">
				<div class="col1-popsindi"><?php echo JText::_('COM_POPS_VIEW_SAVEDBUYERS')?>:</div>
				<div class="col2-popsindi">
				<?php $saved_buyer_ctr = 0; ?>
				<?php (array) $property_sets; ?>
				<?php 
					foreach( $this->existing_saved_listing as $key => $value ){
						$property_sets[] = $value->buyer_id;
					}
				?>
    				<?php if(count($this->buyers_a)) { ?>
						<?php foreach($this->buyers_a as $buyer) { ?>
							<?php if ( @in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $saved_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername"><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>
					  <?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
               <div class="col3-popsindi">
    				<?php if(count($this->buyers_b)) { ?>
						<?php foreach($this->buyers_b as $buyer) { ?>
							<?php if ( @in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $saved_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername"><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>
						<?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
				<div class="col4-popsindi">
    				<?php if(count($this->buyers_c)) { ?>
						<?php foreach($this->buyers_c as $buyer) { ?>
							<?php if ( @in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $saved_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername"><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>
					  <?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
		    <div style="clear:both;"></div>		
            </div>
            <!--Available Buyers-->
            <div id="available_section" style="margin-top:10px; margin-bottom:10px" class="border-dot">
            <div class="col1-popsindi"><?php echo JText::_('COM_POPS_VIEW_AVAILABLEBUYERS')?>:</div>
				<div class="col2-popsindi">
				<?php $available_buyer_ctr = 0; ?>
    				<?php if(count($this->buyers_a)) { ?>
						<?php foreach($this->buyers_a as $buyer) { ?>
							<?php if ( !@in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $available_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername" ><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>					
						 <?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
               <div class="col3-popsindi">
    				<?php if(count($this->buyers_b)) { ?>
						<?php foreach($this->buyers_b as $buyer) { ?>
							<?php if ( !@in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $available_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername"><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>					
					   <?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
				<div class="col4-popsindi">
    				<?php if(count($this->buyers_c)) { ?>
						<?php foreach($this->buyers_c as $buyer) { ?>
							<?php if ( !@in_array($buyer->buyer_id, $property_sets)) { ?>
							<?php $available_buyer_ctr++; ?>
							<div>
							  <div style="float:left">
								<div id="buyer-m">
								<div class="left"><input class="savetobuyercheckbox" id="buyer<?php echo $buyer->buyer_id; ?>" type="checkbox" name="savetobuyer" value="<?php echo $buyer->buyer_id; ?>"><span class="savedbuyername"><?php echo $buyer->name; ?></span></div>
								</div>
							  </div>
							</div>
						  <div style="clear:both;"></div>						
					   <?php } ?>
					<?php } ?>
				 <?php } ?>
 			   </div>
		    <div style="clear:both;"></div>
            </div>		
			 <div> 
				<?php if( $available_buyer_ctr == 0 ) { ?>
				<?php echo "
					<script>	
						jQuery(document).ready(function(){															
							var available_buyer = ".$available_buyer_ctr.";
							if(available_buyer==0){
								jQuery('#available_section').hide();
							}
						});
					</script>";
				?>
				<div class="border-dot">
					<div style="text-align:center; margin-bottom:10px" ><?php echo JText::_('COM_POPS_VIEW_NOBUYER')?>  <a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>"><span class="text-link"><?php echo JText::_('COM_USERPROF_PROF_ADDB')?> </span></a> </div>
				</div>
				<?php } ?>
				<?php 
					if( $saved_buyer_ctr == 0 ) {
						echo "
							<script>	
								jQuery(document).ready(function(){															
									var saved_buyer = ".$saved_buyer_ctr.";
									if(saved_buyer==0){
										jQuery('#saved_buyer_section').hide();
									}
								});
							</script>
						";	
					}
				?> 
			</div>
			</div>
		</div>
		</div>
		<input type="hidden" name="property_id" id="property_id" value="<?php echo $_REQUEST['lID']; ?>" />
		<input type="hidden" name="agent_id" id="agent_id" value="<?php echo JFactory::getUser()->id; ?>" />
	 <div id="result-message"></div>
		<div id="savebuyers-button-container" class="right clear-both" style="margin-top:5px;">
			<div id="save-savebuyer">
				<!--a onclick="savebuyers()" class="button gradient-green right" style="width:120px" href="javascript:void(0);"> Save </a-->
				<a class="button gradient-green right" style="width:120px" href="javascript:void(0);"><?php echo JText::_('COM_ACTIVITY_BT_SAVE')?> </a>
			</div>
            <div id="cancel-savebuyer" style="margin-top:7px">
				<a onclick="customClose()" href="javascript:void(0);"><?php echo JText::_('COM_BUYERS_CANCEL')?></a>
			</div>
		</div>
	</div>
</div> 
<script>
	mixpanel.track("POPs - <?php echo $_REQUEST['lID']; ?> - <?php echo stripslashes_all($this->data->property_name); ?>");

	function savePOPs(){
		jQuery("#savebuyers").dialog(
		{
			modal:true, 
			width: 'auto', 
			maxWidth: 1500, 
			minWidth: 500,
			title: "<?php echo JText::_('COM_POPS_VIEW_SAVE2BUYER') ?>",
			}
		);
	}
	function customClose(){
		jQuery("#savebuyers").dialog('close');
	}
	function savebuyers(){
		var pid = document.getElementById('property_id').value;
		var aid = document.getElementById('agent_id').value;
		var arr = [];
		var notchecked = [];
		var i = 0;
		//removed!!! doesent care if this is changed before or not
		jQuery('.savetobuyercheckbox:checked').each(function(){
			 arr[i++] = jQuery(this).val();
		});
		jQuery('.savetobuyercheckbox:not(:checked)').each(function(){
			notchecked[i++] = jQuery(this).val();
			console.log(jQuery(this).val());
		});
		jQuery('.savetobuyercheckbox').on('change', function(){
			//detect if this id is in original state
			//pida 				 = listing_id
			//jQuery(this).val() = buyer_id
			//aida               = user_id = loggedin user
			//current_status     = active
			var checkBoxId = '#buyer'+jQuery(this).val();
			if( jQuery(checkBoxId).is(":checked")) {
				//submit the value to post
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=check_saved_pops'); ?>",
					data: {
						aida : aid, 
						pida : pid,
						buyer_arr: jQuery(this).val(), 
						current_status: 1
						},
					success: function(data) {
						function replaceMulti(haystack, needle, replacement)
						{
							return haystack.split(needle).join(replacement);
						}
						var get_list = jQuery("#to-deactivate").val();
						if(data==0){
							jQuery("#to-deactivate").val(replaceMulti(get_list, ","+jQuery(checkBoxId).val(), "")+","+jQuery(checkBoxId).val());
						} else {
							jQuery("#to-deactivate").val(replaceMulti(get_list, ","+jQuery(checkBoxId).val(), ""));
						}
					}
				});
			} else if ( jQuery(checkBoxId).is(":not(:checked)")){
				//submit the value to post
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=check_saved_pops'); ?>",
					data: {
						aida : aid, 
						pida : pid,
						buyer_arr: jQuery(this).val(), 
						current_status: 0
						},
					success: function(data) {
						function replaceMulti(haystack, needle, replacement)
						{
							return haystack.split(needle).join(replacement);
						}	
						var get_list = jQuery("#to-activate").val();
						if(data==0) {
							jQuery("#to-activate").val(replaceMulti(get_list, ","+jQuery(checkBoxId).val(), "")+","+jQuery(checkBoxId).val());
						} else {
							jQuery("#to-activate").val(replaceMulti(get_list, ","+jQuery(checkBoxId).val(), ""));
						}
					}
				});			
			}
		});
		jQuery('#save-savebuyer').on('click', function(){
			jQuery.ajax({
				type: "POST",
				url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=savepops'); ?>",
				data: {aida : ""+aid, pida : ""+pid, buyer_arr: jQuery("#to-deactivate").val() , buyer_arr_dis: jQuery("#to-activate").val() },
				success: function(data) {
					jQuery("#savebuyers").dialog('close');
					jQuery("#save-savebuyer a").attr('onclick', '');
					jQuery("#save-savebuyer a").removeClass('button');
					jQuery("#save-savebuyer a").toggleClass('button-save');
					jQuery('.savetobuyercheckbox').prop({disabled: true});
					jQuery("#save-savebuyer a").css('cursor', 'default');
					jQuery("#save-savebuyer a").toggleClass('gray');
					location = "<?php echo JRoute::_('index.php?option=com_activitylog'); ?>";
					console.log(data);
			   }
			});
		});
	}
	jQuery(document).ready(function(){
		savebuyers();
		if(jQuery('.property-features ul').children().length < 1){
			jQuery('.property-features').html('<h2>Property Features</h2><br/>none provided');
		}
		
		/*
		jQuery(".fancybox_images").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			nextEffect	: 'none',
			prevEffect	: 'none',
			helpers : {
				media : {}
			}		
		});
		*/

 		jQuery("div a.grouped_elements").live("click",function(){
			var this_id = jQuery("#currslide").text();
			jQuery("div a.fancybox_images.image_"+this_id).click();
			console.log(this_id);
			return false;
		});

		Landing.format();
		jQuery("#larger").click(function() {
			var this_id = jQuery("#currslide").text();
			jQuery("div a.fancybox_images.image_"+this_id).click();
			//jQuery("div a.fancybox_images").click();
		});
<?php 
foreach($buyers_arr as $buyer){
		if(count($buyer->hassaved) > 0){
			foreach($buyer->hassaved as $saved){
			if($saved->listing_id == $_REQUEST['lID'] && $saved->active == 1){ ?>
				jQuery('#buyer<?php echo $saved->buyer_id; ?>').prop('checked', true);	
<?php		}
			}
		}
	}	
?> 
	});
	/*slider*/
	jQuery(document).ready(function(){
			var parent, children;	
			jQuery("#slider_adin").css("height","217px !important").animate("slow");
			<?php if(count($this->images)>1){ ?>
			jQuery("#slider_adin").easySlider({
				auto: true,
				continuous: true,
				numeric: true,
				controlsShow: false,
			});
			var slider = jQuery('#slider_adin'),
            curIndex = Math.round(Math.abs(  parseInt(slider.find('li').css('marginLeft')) / slider.width()  ));
           // console.log(curIndex);
            <?php }?>
		    jQuery("#sold_text.unsold").live("click", function(){
				var this_class=jQuery(this).attr("class").split(" ")[2];
				var this_id=jQuery(this).attr("class").split(" ")[1];
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=soldPOPs&format=raw') ?>",
					data: {'listing_id': this_class},
					success: function(data){
						if(this_id==1 || this_id==3){
						jQuery(".large-item-details").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_SOLD')?></h2></div>');
						} else {
						jQuery(".large-item-details").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_LEASED')?></h2></div>');
						}
						jQuery("#item_"+this_class+" .description-placeholder div.folded").remove();
						if(this_id==1 || this_id==3){
						jQuery("."+this_class+".unsold").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKS')?>");
						} else {
						jQuery("."+this_class+".unsold").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKL')?>");
						}
						jQuery("."+this_class+".unsold").addClass("sold").removeClass("unsold");
					}
				});
			});
			jQuery("#sold_text.sold").live("click", function(){
				var this_class=jQuery(this).attr("class").split(" ")[2];
				var this_id=jQuery(this).attr("class").split(" ")[1];
				jQuery.ajax({
					type: "POST",
					url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=unSoldPOPs&format=raw') ?>",
					data: {'listing_id': this_class},
					success: function(data){
						jQuery("div.folded").remove();
						if(this_id==1 || this_id==3){
						jQuery("."+this_class+".sold").html("<?php echo JText::_('COM_POPS_TERMS_MARKS')?>");
						} else {
						jQuery("."+this_class+".sold").html("<?php echo JText::_('COM_POPS_TERMS_MARKL')?>");
						}
						jQuery("."+this_class+".sold").addClass("unsold").removeClass("sold");
					}
				});
			});
		});
		
	jQuery(document).ready(function(){
		jQuery('.gallery-item').magnificPopup({
			type: 'image',
			gallery:{
				enabled:true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
				tPrev: 'Previous (Left arrow key)', // title for left button
				tNext: 'Next (Right arrow key)', // title for right button
			}
		});
		
		jQuery('.fancybox_images').magnificPopup({
			type: 'image',
			gallery:{
				enabled:true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
				tPrev: 'Previous (Left arrow key)', // title for left button
				tNext: 'Next (Right arrow key)', // title for right button
			}
		});
	});
</script>