<?php
    header('X-Frame-Options: GOFORIT'); 
?>

<?php
$new_date = new DateTime($this->referral->date, new DateTimeZone($this->timezone));
$userTimeZone = new DateTimeZone($this->timezone);
$offset = $userTimeZone->getOffset($new_date);
$date = gmdate($this->d_format, strtotime($this->referral->date)+intval($offset));

$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
$language_tag = JFactory::getUser()->currLanguage;
$language->load($extension, $base_dir, $language_tag, true);

#$timestamp = strtotime($this->referral->date)-25200;
#$date = gmdate($this->d_format, $timestamp);
$image = ($this->referral->agent_a == JFactory::getUser()->id) ? $this->referral->agent_b_info->image : $this->referral->agent_a_info->image;
if($this->referral->agent_b == JFactory::getUser()->id){
	$otherusername = $this->referral->agent_a_info->name;
	$name =  $this->referral->agent_b_info->name;
}
else{
	$name = $this->referral->agent_a_info->name;
	$otherusername =  $this->referral->agent_b_info->name;
}
?>

<div class="wrapper">
		<!-- start wide-content -->

		<h1 style="margin-top:20px"><?php echo JText::_('COM_REFERRAL_MY') ?></h1>
		<div class="clear-float">
			<a href="<?php echo JRoute::_('index.p	hp?option=com_propertylisting&task=referrals'); ?>" class="button gradient-blue left"
				style="margin: 10px 0; color: #fff; padding-top:7px"><?php echo JText::_('COM_REFERRALS_RETURN') ?></a>
		</div>

		<div class="referral_viewer" style="font: 11px";>
			<div class="clear-float">
				<div id="header" style="height: 30px; margin-bottom:10px">
					<div class="names" style="border-right: 1px solid; padding-right: 20px; float:left">
						<span class="name"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->referral->agent_a)?>" class="text-link"><?php echo stripslashes_all($this->referral->agent_a_info->name)?></a></span><br/>
						<span class="l_update"><?php echo JText::_('COM_LASTUPDATED') ?>: </span><span class="l_update"><?php echo $date?></span>
					</div>
					<div class="other_user left" style="margin-left: 10px;">
						<div class="other-info left" style="padding-left: 10px">
							<span class="name-small left"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->referral->agent_b)?>" class="text-link"><?php echo stripslashes_all($this->referral->agent_b_info->name)?></a></span><br/>
							<span class="l_update"><?php echo JText::_('COM_LASTUPDATED') ?>: </span><span class="l_update"><?php echo $date?></span>
						</div>
					</div>
					<!--<div class="right">
						<?php if($this->referral->signatures != 2){ ?>
								<a href="javascript:void(0)" onclick="declineUserReferral(<?php echo $_GET['ref_id'] ?>)" class="right" style="margin: 15px 15px 0 0; font-size: 10px;">Cancel referral</a>
						<?php } ?>
					</div>-->
					<div class="clear-float"></div>
				</div>
				<!--{aridoc engine="google" width="700" height="800"}
					<?php echo JUri::base().'referrals/referral-'.$this->ref_id.'.pdf';?>
				{/aridoc}-->
				
				<?php if($this->referral->status==5){ ?>
						<div style="font-size: 15px; margin: 0 auto; font-weight: bold; padding-top: 70px; width: 245px;">This referral has been declined</div>
				<?php }else {?>
						<iframe src="<?php echo $this->document?>" width="100%" height="800" border="0"></iframe>
				<?php } ?>
			</div>
			<div class="clear-float"></div>
			<div class="clear-float">
				<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
				<br /> <br />
			</div>

		</div>
	</div>


<div style="display: none" id="reason"><br/>
	<select style="width:270px" id="selectreason" style="display:none">
		<option value="">Select Reason</option>
		<option value="1">Client unresponsive</option>
		<option value="2">Client working with another agent</option>
		<option value="3">Client's needs changed</option>
		<option value="4">Retracted</option>
	</select>
	<input type="hidden" id="referral_id"
		name="referral_id"
	/>
	<input type="hidden" id="tochangeid" />
	<div class="clear-float"></div>
	<a href="javascript:void(0)" class="button gradient-green right" onclick="submitStatusChange()" style="margin-top: 10px; padding-top:5px">Submit</a>
	<div class="clear-float"></div>
</div>

<script type="text/javascript">

	function declineFully(){
		var sure = confirm('Click on OK if really want to decline this referral');
		if(sure){
			if(jQuery("#referralrequest").hasClass('ui-dialog-content'))
				jQuery("#referralrequest").dialog('close');
			jQuery("#reason").dialog(
				{
					modal:true,
					title:"Cancel Referral",
				});
			/*jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>',
				data: {'id': jQuery("#referral_id").val()},
				success: function(data){
					alert(data);
					location.reload();
				}
			});*/
		}
	}
	
	function declineUserReferral(id){
		jQuery("#referral_id").val(id);
		declineFully();
	}

	function declineReferral(id){
		jQuery("#referral_id").val(id);
		jQuery("#referralrequest_div").dialog({modal:true, width: 500});
	}

	function submitStatusChange(){
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>",
			type: 'POST',
			data: { 'id': jQuery('#referral_id').val(), 'reason':jQuery('#selectreason').val() },
			success: function(response){
				alert(response);
				jQuery('#changestatus').dialog('close');
				location.reload();
			}
		});
	}

	jQuery(document).ready(function(){
		jQuery('select#selectreason').select2('destroy');
	});

</script>