<?php
$printlanguage = "";
$limit = (count($this->languages) > 3) ? 3 : count($this->languages);
$lastcm = $limit - 1;
for($i = 0; $i<$limit; $i++){
	$printlanguage .= $this->languages[$i]->language;
	if($i<$lastcm)
		$printlanguage .= ',';
}
$printdesig = "";
$limit = (count($this->designations) > 3) ? 3 : count($this->designations);
$lastcm = $limit - 1;
for($i = 0; $i<$limit; $i++){
	$printdesig .= $this->designations[$i]->designations;
	if($i<$lastcm)
		$printdesig .= ',';
}
?>
<!-- start main content -->
<div class="wrapper">
	<div class="wide left referralform">
		<!-- start wide-content -->
		<h1><?php echo JText::_('COM_REFERRALS_NEW') ?></h1>
		<?php if(isset($this->owner)){ ?>
			<p style="margin-top:20px"><?php echo JText::_('COM_REFERRALS_REFER_TO_AGENT') ?></p><br /> 
			<div class="mclient_info">
				<ul>
					<?php if($this->owner->image!=""){ ?>
						<?php if(strpos($this->owner->image, $this->baseurl) !== false) {?>
							<li style="width:70px"><img src="<?php echo str_replace("loads/", "loads/thumb_",$this->owner->image);?>" width="60"/></li>
						<?php } else { ?>
							<li style="width:70px"><img src="<?php echo $this->baseurl . "uploads/" . str_replace("loads/", "loads/thumb_",$this->owner->image);?>" width="60"/></li>
						<?php } ?>						
					<?php } else { ?>
						<li style="width:70px">
							<img src="<?php echo $this->baseurl ?>templates/agentbridge/images/temp/blank-image.jpg" width="60"/>						
						</li>
					<?php } ?>
					<li style="width:600px">
						<h2> <a href="<?php echo $this->baseurl ?>index.php/component/userprofile/?task=profile&uid=<?php echo $this->owner->id; ?>"><?php echo $this->owner->name;?></a></h2>
						
						<?php if($this->owner->is_term_accepted==1 ) { ?>
							<?php echo ($printdesig) ? "<h4>Designation:</h4><span style='font-size:12px; width:500px'>$printdesig</span>" : ""; ?>
						<?php } else { ?>
							<p style="width:500px; font-size:12px"><?php echo $this->owner->name;?> <?php echo JText::_('COM_REFERRALS_AGENT_WILL_BE_NOTIFIED') ?></p></span>
						<?php } ?>
					</li>
					<?php if($this->owner->is_term_accepted==1 ) { ?>
						<li class="left"><?php echo ($printlanguage) ? "<h4>" . JText::_('COM_REFERRALS_LANGUAGE') . "</h4><span style='font-size:12px'>$printlanguage</span>" : ""; ?></li>
						<li>

						<?php if ($this->owner->verified_2014==1 && $this->owner->verified_2015==1)  { //2014 and 2015 ?> 

							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE2');?></h4>

							<span style="font-size:12px"><?php echo $this->owner->symbol.number_format((($this->owner->volume_2014/$this->owner->sides_2014) + ($this->owner->volume_2015/$this->owner->sides_2015))/2)." ".$this->owner->currency ?></span>
						
						<?php } else if ($this->owner->verified_2014==0 && $this->owner->verified_2015==1 )  { //2015 ?> 

							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?> 2015 </h4>

							<span style="font-size:12px"><?php echo $this->owner->symbol.number_format($this->owner->volume_2015/$this->owner->sides_2015)." ".$this->owner->currency ?></span>
						
						<?php } else if ($this->owner->verified_2014==1 && $this->owner->verified_2015==0)  { //2014 ?> 
						
							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?> 2014</h4>
						
							<span style="font-size:12px"><?php echo $this->owner->symbol.number_format($this->owner->volume_2014/$this->owner->sides_2014)." ".$this->owner->currency ?></span>
						
						<?php } else if ($this->owner->verified_2014==0 && $this->owner->verified_2013==1 && $this->owner->verified_2015==0 )  { //2013 ?> 
						
							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?> 2013</h4>
						
							<span style="font-size:12px"><?php echo $this->owner->symbol.number_format($this->owner->ave_price_2013)." ".$this->owner->currency ?></span>
						
						<?php } else if ($this->owner->verified_2012==1 && $this->owner->verified_2013==0 && $this->owner->verified_2014==0 && $this->owner->verified_2015==0) {?>
						
							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?> 2012</h4>
						
							<span style="font-size:12px"><?php echo $this->owner->symbol.number_format($this->owner->ave_price_2012)." ".$this->owner->currency ?></span>						
						
						
						<?php } else if ($this->owner->verified_2014==0 && $this->owner->verified_2015==0 && $this->owner->verified_2013==0 && $this->owner->verified_2012==0 ) {?>
						
							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?></h4>
						
							<span style="font-size:12px"><?php echo JText::_('COM_USERPROF_PROF_VERIFYINGNUM');?></span>
						
						<?php } else {?> 
						
							<h4><?php echo JText::_('COM_USERPROF_PROF_AVESALE');?></h4>
						
							<span style="font-size:12px"><?php echo JText::_('COM_USERPROF_PROF_NA');?></span>
						<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } else if(count($this->network)){ ?>
			<p style="margin-top:20px;  margin-bottom:10px"><?php echo JText::_('COM_REFERRALS_SEARCH_SEND_PREVIOUS');?></p>
			<div class="mclient_fillbox" style="padding-left:0;">
				<input type="text" name="search-email" id="search-email" placeholder="<?php echo JText::_('COM_REFERRALS_AGENTS_EMAIL'); ?>" class="text-input left" style="width:200px;"/>
				<input type="button" id="search-agent" class="button gradient-green left" value="<?php echo JText::_('COM_REFERRALS_AGENTS_EMAIL_SUBMIT'); ?>" style="margin:10px 0 10px 10px;color:#fff;width:100px;" />
				<div id="agent-email-error" class="left error_msg" style="display:table;height:30px;margin:10px 0 10px 10px;width:300px;line-height:15px;"><p style="display:table-cell;text-align:left;vertical-align:middle;"></p></div>
			</div>
			<div class="mclient_info" style="padding:5px" id="select_uid">
				<?php foreach ($this->network as $agent) {
						echo 	"<div class='agentinfo agentcount'>";
						echo		'<a href="javascript:void(0);" onclick="postToSession(\''.JRoute::_('index.php?option=com_propertylisting&task=newreferral').'&uid='.$agent['profile']->id.'\');">'.$agent["profile"]->name.'</a>';
						echo	"</div>";
					}
				?>
			</div>
			<p style="margin-top:10px;  margin-bottom:10px"><?php echo JText::_('COM_REFERRALS_DONT_KNOW_CONTACT');?></p>
			<p class="select_uid error_msg" style="display:none"> <?php echo JText::_('COM_REFERRALS_PLEASE_SELECT_AGENT');?> </p>
		<?php } else {?>
			<div id="nopops" style="margin-top:20px; line-height:26px"><?php echo JText::_('COM_REFERRALS_NO_MEMBERS_NETWORK');?></div>
		<?php } ?>
		
		<?php if(count($this->network) || isset($this->owner)){ ?>
		<form id="referralform" >
			<input type="hidden" name="uid" id="uid" value="<?php echo JFactory::getUser()->id?>" />
			<input type="hidden" value="<?php echo $this->owner->id; ?>" id="agentb" name="jform[agentb]" />
			<div class="mclient_fill">
          		    <h2 style="padding-bottom:10px; border-bottom: 1px solid rgb(191, 191, 191)"> <br/><?php echo JText::_('COM_REFERRALS_CLIENT_INFORMATION');?> </h2>
					<div class="mclient_fillbox">
						<label><?php echo JText::_('COM_REFERRALS_CLIENT_NAME');?></label><br /> 
						<input 
							onMouseover="ddrivetip('Client name, number and email are not released until other agent accepts');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							id="name" 
							name="jform[name]"
							type="text" 
							placeholder="Complete Name"
							class="text-input"
							value="<?php echo (isset($this->referral_data['jform']['name'])) ? $this->referral_data['jform']['name'] : ""; ?>" 
							/> <br />
						<div style="clear: both"></div>
						<div><p class="name error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p></div>
					</div>
				<div class="mclient_fillbox">
					<label><?php echo JText::_('COM_REFERRALS_CLIENT_MOBILE');?></label><br />
					<div id="clientmobile" style="width: 327px;">
						<label class="countCode" style="width:28px" ><?php echo $this->getCountryLangsInitial->countryCode?></label>
						<input 
							id="mobile" 
							style="width:290px";
							name="jform[mobile][]"
							onMouseover="ddrivetip('Client name, number and email are not released<br /> until other agent accepts')"
							onMouseout="hideddrivetip();hover_dd();hover_dd()" 
							class="text-input numbermask" 
							type="text"
							placeholder="i.e: (818) 123-1234"
							value="<?php echo (isset($this->referral_data['jform']['mobile'][0])) ? $this->referral_data['jform']['mobile'][0] : ""; ?>"				
						/>
						<?php 
							if(isset($this->referral_data['jform']['mobile'])) {
				
								foreach($this->referral_data['jform']['mobile'] as $key => $mobile) {
									if($key) {
						?>
							<label class="countCode" style="width:28px"><?php echo $this->getCountryLangsInitial->countryCode?></label>
							<input type="text" placeholder="i.e.: (818) 123-1234" validator="1" class="validateme numbermask" name="jform[mobile][]" style="width:290px" value="<?php echo $mobile; ?>"	/>
						<?php
									}
								}
							}
						?>
					</div>
						<div style="clear: both"></div>
						<div><p class="mobile error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p> </div>
						<a id="addphone" href="javascript:void(0)">+<?php echo JText::_('COM_REFERRALS_CLIENT_ADD_PHONE');?></a>
				</div>
				<div class="mclient_fillbox">
					<label><?php echo JText::_('COM_REFERRALS_CLIENT_EMAIL');?></label><br />
					<div id="clientemail">
						<input 
							id="clientemailx" 
							name="jform[email][]" 
							type="text"
							placeholder="clientname@companyemail.com"
							class="text-input" 
							value="<?php echo (isset($this->referral_data['jform']['email'][0])) ? $this->referral_data['jform']['email'][0] : ""; ?>"
						/><br />
						<?php 
							if(isset($this->referral_data['jform']['email'])) {
								foreach($this->referral_data['jform']['email'] as $key => $email) { 
									if($key) {
						?>
						<input type="text" placeholder="clientname@companyemail.com" validator="0" class="validateme" name="jform[email][]" value="<?php echo $email; ?>" />
						<br />
						<?php
									}
								}
							}
						?>
					</div>
					<div style="clear: both"></div>
					<div><p class="clientemailx error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p></div>
					<a id="addemail" href="javascript:void(0)">+<?php echo JText::_('COM_REFERRALS_CLIENT_ADD_EMAIL');?></a>
				</div>
				<div class="mclient_fillbox">
					<label class="lbl_spacer"><?php echo JText::_('COM_REFERRALS_CLIENT_PREFERRED_CONTACT_METHOD');?></label><br /> 
					<select
						id="jform_contact_method" 
						name="jform[contact_method]" 
						style="padding-top:10px; margin-top:10px">
						<?php foreach($this->contact as $contact) {?>
						<option value="<?php echo $contact->pk_id?>" <?php echo ($this->referral_data['jform']['contact_method'] == $contact->pk_id) ? "selected" : ""; ?>><?php echo $contact->method?></option>
						<?php } ?>
					</select>
				</div>
				<div 
					class="mclient_fillbox clear-float" 
					id="contactmeans"
                	onMouseover="ddrivetip('When to contact the referral after acceptance<br />of referral agreement.');hover_dd()"
					onMouseout="hideddrivetip();hover_dd()">
					<label class="lbl_spacer"><?php echo JText::_('COM_REFERRALS_CONTACT_MY_CLIENT');?></label><br />
					<select
						id="jform_contact_type" 
						name="jform[contact_type]">
						<?php foreach($this->contactclient as $contact) {?>
						<option value="<?php echo $contact->id;?>" <?php echo ($this->referral_data['jform']['contact_type'] == $contact->id) ? "selected" : ""; ?>><?php echo JText::_($contact->label);?></option>
						<?php }?>
					</select> <br />
				</div>
				<div 
					class="mclient_fillbox" 
					id="intention"
					onMouseover="ddrivetip('Select referral intention(s).');hover_dd()"
					onMouseout="hideddrivetip();hover_dd()">
					<label class="lbl_spacer"><?php echo JText::_('COM_REFERRALS_CLIENT_INTENTION');?></label><br /> 
						<select 
							id="jform_intention" 
							name="jform[intention]" >
						<?php foreach($this->clientintention as $intention){ ?>
						<option value="<?php echo $intention->id ?>" <?php echo ($this->referral_data['jform']['intention'] == $intention->id) ? "selected" : ""; ?>><?php echo JText::_($intention->intention); ?></option>
						<?php }?>
					</select> <br />
				</div>
				<div class="mclient_fillbox clear-float">
					<label><?php echo JText::_('COM_REFERRALS_CLIENT_ADDRESS');?></label><br /> 
					<select 
						id="jform_country"
						name="jform[country]" 
						class="required longaddress mc_select refcountry" 
					 >					
						<option value=""><?php echo JText::_('COM_REFERRALS_SELECT_COUNTRY');?></option>
						<?php
						foreach($this->country_list as $country_list):						
								$selected = ($this->referral_data['jform']['country'] == $country_list->countries_id) ? "selected" : "";
								echo "<option class='".$country_list->currency." cho_".$country_list->symbol."' data=\"".$country_list->countries_iso_code_2."\" value=\"".$country_list->countries_id."\" ".$selected.">".$country_list->countries_name."</option>";	
						endforeach;
						?>
					</select>
					<input type="hidden" id="jform_currency" name="jform[currency]" value="<?php echo $this->country[0]->currency?>"/>
                    <input 
						class="text-input text-input-refzip"
						id="jform_zip" 
						name="jform[zip]" 
						maxlength=5
						type="text"
                       
						onMouseout="hideddrivetip();hover_dd()"
                        placeholder="Postal Code" 
						style="width:120px"
						value="<?php echo (isset($this->referral_data['jform']['zip'])) ? $this->referral_data['jform']['zip'] : ""; ?>"
						/>
					<div style="clear: both"></div>
					<div>
						<p class="jform_zip error_msg" style="margin-left:190px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p> 
						<p class="jform_zip_2 error_msg" style="margin-left:190px;display:none; line-height:14px"> <?php echo JText::_('COM_REFERRALS_ZIP_ERROR');?> <br /></p>
					</div>
					<div>
						<a id="showadd" href="javascript:void(0)">+<?php echo JText::_('COM_REFERRALS_ENTER_ADDRESS');?></a>
					</div>
					<div id="address_fields" style="display:none">						
                        <label><?php echo JText::_('COM_REFERRALS_ADDRESS1');?></label>
                        <input 
							style="margin-left:10px; width:275px" 
							name="jform[address1]" 
							id="address"
							type="text" 
							onMouseover="ddrivetip('This is optional');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							placeholder="<?php echo JText::_('COM_REFERRALS_ADDRESSLINE1');?>"
							value="<?php echo (isset($this->referral_data['jform']['address1'])) ? $this->referral_data['jform']['address1'] : ""; ?>" class="text-input" /> <br /> 
                        <label><?php echo JText::_('COM_REFERRALS_ADDRESS2');?></label>
                        <input 
							style="margin-left:10px; width:275px" 
							name="jform[address2]" 
							id="address2"
							type="text"
							placeholder="<?php echo JText::_('COM_REFERRALS_ADDRESSLINE2');?>" 
							onMouseover="ddrivetip('This is optional');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							value="<?php echo (isset($this->referral_data['jform']['address2'])) ? $this->referral_data['jform']['address2'] : ""; ?>" class="text-input" />  
					</div>

                        <div class="clear left" style="width:500px">
                        <input 
							name="jform[city]" 
							onMouseover="ddrivetip('This is optional');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							id="jform_city" 
							type="text" 
							placeholder="City" 
							class="left text-input" 
							style=" margin-right:10px"
							value="<?php echo (isset($this->referral_data['jform']['city'])) ? $this->referral_data['jform']['city'] : ""; ?>" />
                        <div style="clear: both"></div>
						<div><p class="jform_city error_msg" style="margin-left:0;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?><br /><br /></p></div>
                        <div style="clear: both"></div>
                        <div class="left">
                        <select id="state" class="mc_select right"><option><?php echo JText::_('COM_REFERRALS_SELECT_STATE');?></option></select> 
                    	</div>
						<div style="clear: both"></div>
						<div><p class="jform_state error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p> </div>
                        </div>
				</div>
				<div class="left mclient_fillbox clear-float" id="price">
					<label><?php echo JText::_('COM_REFERRALS_PROPERTY_PRICE');?></label><br /><br />
					<div class="rangeError" style='font-size:10px;margin-top:-20px;margin-left:160px;margin-bottom:15px;font-size:10px;display:none'></div>
					<?php 
						$currency_def = "USD";
						$currency_sym = "$";
						
						if($this->country[0]->currency){
							//$curreData = getCountryCurrencyData($this->data->country);
							$currency_def = $this->country[0]->currency;
							$currency_sym = $this->country[0]->symbol;
						}

					?>
					<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?>" style="display:none; font-size: 13px;margin-bottom: 25px;cursor: pointer;"><a href="#" onclick="return false;"> <?php echo JText::_('COM_REFERRALS_SET_TO_CURRENCY');?> <?php echo $currency_sym; ?><?php echo $currency_def; ?>. <?php echo JText::_('COM_REFERRALS_CHANGE_TO_CURRENCY');?> <span id="chosenCurr"></span>.</a> </div>	
	
					<div 
						class="left c200" 
						style="width:150px; margin-top:5px"
						id="p_range"
						onMouseover="ddrivetip('Enter price range of referral or select Unknown.');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()">
						<select 
							style="margin-top:10px" 
							id="jform_pricerange"
							onchange="select_price_type(this.value)" 
							name="jform[price_type]" 
							class="small-width required prange">
							<option value="1" <?php echo (isset($this->referral_data['jform']['price_type']) && $this->referral_data['jform']['price_type'] == 1) ? "selected" : ""; ?>><?php echo JText::_('COM_REFERRALS_PRICE_RANGE');?></option>
							<option value="2" <?php echo (isset($this->referral_data['jform']['price_type']) && $this->referral_data['jform']['price_type'] == 2) ? "selected" : ""; ?>><?php echo JText::_('COM_REFERRALS_PRICE_RANGE_OPT_EXACT');?></option>
							<option value="3" <?php echo (isset($this->referral_data['jform']['price_type']) && $this->referral_data['jform']['price_type'] == 3) ? "selected" : ""; ?>><?php echo JText::_('COM_REFERRALS_PRICE_RANGE_OPT_UNKNOWN');?></option>
						</select>
					</div>
					<div class="left ys price_row">
						<label class="hl-label" <?php echo (isset($this->referral_data['jform']['price_type']) && ($this->referral_data['jform']['price_type'] == 2 || $this->referral_data['jform']['price_type'] == 3)) ? "style=\"display:none;\"" : ""; ?>><?php echo JText::_('COM_REFERRALS_PRICE_RANGE_LOW');?></label>
						<input  
							id="jform_price1" 
							name="jform[price1]"
							class="pricevalue small-width text-input required numberperiodcomma"
							value="<?php echo (isset($this->referral_data['jform']['price1'])) ? $this->referral_data['jform']['price1'] : ""; ?>"
							type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>"
							<?php echo (isset($this->referral_data['jform']['price_type']) && ($this->referral_data['jform']['price_type'] == 3)) ? "style=\"display:none;\"" : ""; ?>/>
						<div style="clear: both"></div>
						<div><p class="jform_price1 error_msg" style="margin-left:0;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p> </div>							
					</div>
					<div class="left ys price_row">
					<label class="hl-label" <?php echo (isset($this->referral_data['jform']['price_type']) && ($this->referral_data['jform']['price_type'] == 2 || $this->referral_data['jform']['price_type'] == 3)) ? "style=\"display:none;\"" : ""; ?>><?php echo JText::_('COM_REFERRALS_PRICE_RANGE_HIGH');?></label>
						<input  
							id="jform_price2"
							value="<?php echo (isset($this->referral_data['jform']['price2'])) ? $this->referral_data['jform']['price2'] : ""; ?>"
                            name="jform[price2]"
							class="pricevalue small-width text-input numberperiodcomma" 
                            type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>"
							<?php echo (isset($this->referral_data['jform']['price_type']) && ($this->referral_data['jform']['price_type'] == 2 || $this->referral_data['jform']['price_type'] == 3)) ? "style=\"display:none;\"" : ""; ?>/>			
				<div style="clear: both"></div>
				<div><p class="jform_price2 error_msg" style="margin-left:0;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p></div>	
					</div>
				</div>
				<div class="mclient_fillbox clear-float" >
					<label><?php echo JText::_('COM_REFERRALS_FEE');?></label><br /> 
					<input 
                    	onMouseover="ddrivetip('Suggested referral fee is 25%. Or you may enter another value.');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()" 
						value="<?php echo (isset($this->referral_data['jform']['referral_fee'])) ? $this->referral_data['jform']['referral_fee'] : "25%"; ?>"  
						id="refvalue" 
						name="jform[referral_fee]"
						class="left mc_select text-input"/>
				</div>
				<div class="mclient_fillbox clear-float" style="margin-bottom:20px;width:100%">
					<label><?php echo JText::_('COM_REFERRALS_NOTES');?></label><br /> 
					<textarea 
                    	onMouseover="ddrivetip('Add notes here');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()" 
						colspan = "350"
						style="width: 68%;color: #666666;font: 12px 'OpenSansRegular';padding: 5px;"
						id="notesvalue" 
						name="jform[note]"
						class="left mc_select wide text-input"
						maxlength="512"
						placeholder="Example: Client pre-qualified with Bank of America"><?php echo (isset($this->referral_data['jform']['note'])) ? $this->referral_data['jform']['note'] : ""; ?></textarea> 
				</div>
				<div class="clear-float">
				<a href="javascript:void(0)" id="submit-send-referral" style="display:none"/></a> 				
				<a href="javascript:void(0)" id="submit-send-referral2" class="button gradient-blue left" style="margin-left:10px; padding-top:7px"><?php echo JText::_('COM_REFERRALS_SEND');?></a> 
                <input name="" value="<?php echo JText::_('COM_REFERRALS_CANCEL');?>" type="button"  onClick="javascript:location.href = '<?php echo JRoute::_("index.php?option=com_userprofile&task=profile"); ?>';"   style="font-size:12px; width:120px; margin-left:10px; height:30px; padding-top:7px; color:#333333; border:1px solid #c9c9c9" class="button gradient-gray cancel" />
				<img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
				<input id="stateselected" type="hidden" value="<?php echo (isset($this->referral_data['jform']['state'])) ? $this->referral_data['jform']['state'] : ""; ?>"/>
				</div>
				<div class="clear-float" style="margin-bottom:120px"></div>
			</div>
		</form>
		<?php }?>
	  </div>
<?php include './includes/sidebar_left.php' ?>
</div>

<div style="display: none" id="addbrokerlicense">

	<?php echo JText::_('COM_REFERRALS_ADD_LICENSE');?> <br /> <br /> <strong><?php echo JText::_('COM_REFERRALS_BROKERAGE_LICENSE');?></strong><br />

	<input 

		type="text" 

		id="bl_value_id" 

		name="bl_value"

		class="mc_select left" 

		required="required"

		style="height:30px;  width: 186px;"/> 

	<input 

		type="hidden" 

		id="otheruserid_val" 

		class="right"

		name="otheruserid"/> 

	<input 

		type="hidden" 

		id="activityid_val" 

		class="right"

		name="activityid"/> 

	<a 

		href="javascript:void(0)"

		class="button accept gradient-blue left" 

		style="width: 80px;height:30px; padding-top:5px "

		onclick="savedialogbroker()"><?php echo JText::_('COM_REFERRALS_BROKERAGE_SAVE');?></a> 

	<p class="broker error_msg" style="padding:0;margin:0px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?></p>
</div>
<script>

var con_Code;
var con_placeholder;
	function checkprice(value){
		jQuery(".qtip").remove();
		if(value==4){
			jQuery("#price select").removeAttr('required');
			jQuery("#price input").removeAttr('required');
			jQuery("#price").hide();
		}
		else{
			jQuery("#price").show();
			jQuery("#price select").attr('required', 'required');
			jQuery("#price input").attr('required', 'required');
		}
	}
	
	function select_price_type(e){			
		jQuery("#jform_price2, #jform_price1").removeClass("glow-required");
		jQuery(".jform_price1, .jform_price2").hide();		
		jQuery(".qtip").remove();
			if(e==2) {
				document.getElementById('jform_price2').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1").show();	
					
			} else if(e==1) {				
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1, .hl-label").show();							
				document.getElementById('jform_price2').disabled=false;				
				jQuery("#jform_price2, .hl-label").show();		
			} else {
				document.getElementById('jform_price2').disabled=true;				
				document.getElementById('jform_price1').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price1").hide();				
				jQuery("#jform_price1").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();			
			}		
		}
		
	function jsonpCallback(data){
		console.log(data);
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		jQuery("#jform_city").val(data.postalcodes[0].placeName);
		jQuery("#s2id_state").remove();
		try{
			jQuery("#jform_state").select2();
		}catch(e){}
	}


	function savedialogbroker(){

		if(jQuery("#bl_value_id").val()){

			jQuery(".broker.error_msg").hide();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?format=raw&option=com_propertylisting&task=saveDialogBroker') ?>',
				data: {'otheruserid': jQuery("#otheruserid_val").val(), 'brokerage_license': jQuery("#bl_value_id").val()},
				success: function(data){
					jQuery('#addbrokerlicense').dialog('close');
					jQuery("#submit-send-referral").trigger("click");
				}
			});
		} else {
			jQuery(".broker.error_msg").show();
		}
	}
		
	jQuery(document).ready(function(){
		jQuery("#search-agent").click(function(){
			if(jQuery("#search-email").val()) {
				if(!IsEmail(jQuery("#search-email").val())) {
					jQuery("#agent-email-error p").html("");
					jQuery("#agent-email-error p").html("<?php echo JText::_("COM_REFERRALS_ERROR_ENTER_VALID_EMAIL"); ?>");
					jQuery("#agent-email-error").show();
				} else {
					jQuery.ajax({
						url: "/index.php/component/propertylisting/?task=getUserIdByEmail",
						method: "POST",
						data: {"email": jQuery("#search-email").val()},
						dataType: "json",
						success: function(data) {
							if(!data) {
								jQuery("#agent-email-error p").html("");
								
								jQuery("#agent-email-error p").html("<?php echo JText::_("COM_REFERRALS_ERROR_NO_AGENT"); ?>");
								jQuery("#agent-email-error").show();
								
							} else if(data.id == <?php echo JFactory::getUser()->id?>) {
								jQuery("#agent-email-error p").html("");
								
								jQuery("#agent-email-error p").html("<?php echo JText::_("COM_REFERRALS_ERROR_ENTER_ANOTHER_EMAIL"); ?>");
								jQuery("#agent-email-error").show();
							} else {
								postToSession('<?php echo JRoute::_("index.php?option=com_propertylisting&task=newreferral&uid="); ?>' + data.id);
								//location.href = '<?php echo JRoute::_("index.php?option=com_propertylisting&task=newreferral&uid="); ?>' + data.id;
							}
						}
					});
				}				
			} else {
				jQuery("#agent-email-error p").html("");
				jQuery("#agent-email-error p").html("<?php echo JText::_("COM_REFERRALS_ERROR_ENTER_VALID_EMAIL"); ?>");
				jQuery("#agent-email-error").show();
			}
		});
		
		con_Code = '<?php echo $this->getCountryLangsInitial->countryCode ?>';
		if (jQuery(window).width() <= 600) {
			jQuery("#name").removeAttr('onmouseover');
			jQuery("#p_range").removeAttr('onmouseover');
			jQuery("#contactmeans").removeAttr('onmouseover');
			jQuery("#intention").removeAttr('onmouseover');
			jQuery("#mobile").removeAttr('onmouseover');
			jQuery("#address1").removeAttr('onmouseover');
			jQuery("#address2").removeAttr('onmouseover');
			jQuery("#jform_zip").removeAttr('onmouseover');
			jQuery("#jform_city").removeAttr('onmouseover');	
			jQuery("#refvalue").removeAttr('onmouseover');
			jQuery("#notesvalue").removeAttr('onmouseover');				
			}
		
		
		//set_masks();

		jQuery("#submit-send-referral2").live("click",function(){

			jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?format=raw&option=com_propertylisting&task=checkIfBrokerExists') ?>',
				data: {'otheruserid': "<?php echo JFactory::getUser()->id?>"},
				beforeSend: function () {
		           jQuery("#loading").show();
		        },
				success: function(data){
					var brok_obj = JSON.parse(data);
					var bl_exist = brok_obj[0].brokerage_license;
					if(!bl_exist){
						 jQuery("#loading").hide();
						jQuery("#addbrokerlicense").dialog (
							{
								modal:true, 
								width: 305, 
								open: function( event, ui ) {jQuery(".ui-dialog").center()},
								title: "Add Brokerage License",
								close: function( event, ui) {
									//jQuery("#loading-image-questions-"+id).hide();
									//jQuery("#buttons-"+id).show();
								}
							}
						);
						jQuery("#otheruserid_val").val("<?php echo JFactory::getUser()->id?>");
					} else {
						jQuery("#jform_price1").val(jQuery("#jform_price1").autoNumeric('get'));
						jQuery("#jform_price2").val(jQuery("#jform_price2").autoNumeric('get'));
						jQuery("#submit-send-referral").trigger("click");
					}
				}, 
				error : function(){
				}
			});

		});



		jQuery(".numberperiodcomma").autoNumeric('init', {aSign:'<?php echo $this->country[0]->symbol ?>', mDec: '0'}); 
		jQuery(".small-width").css('width', '150px');




	jQuery("#jform_zip").blur(function(){

	  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

	});

	var baseurl = "<?php echo $this->baseurl?>";


	var state_variable = '<?php echo $this->getCountryLangsInitial->stateVariable; ?>';
 	
	<?php if(isset($this->referral_data['jform']['state'])) { ?>
		get_state(
			"<?php echo (isset($this->referral_data['jform']['country'])) ? $this->referral_data['jform']['country'] : $this->getCountryLangsInitial->country;?>",
			"<?php echo (isset($this->referral_data['jform']['state'])) ? $this->referral_data['jform']['state'] : ""?>"
		);
	<?php } else { ?>
		get_state("<?php echo (isset($this->referral_data['jform']['country'])) ? $this->referral_data['jform']['country'] : $this->getCountryLangsInitial->country;?>");
	<?php } ?>
	

 	var sel_c="<?php echo $this->getCountryLangsInitial->countries_iso_code_2;?>";

 	jQuery("#jform_country").val("<?php echo (isset($this->referral_data['jform']['country'])) ? $this->referral_data['jform']['country'] : $this->getCountryLangsInitial->country;?>");

	applyAddressDataCR(baseurl,sel_c,"<?php echo (isset($this->referral_data['jform']['country'])) ? $this->referral_data['jform']['country'] : $this->getCountryLangsInitial->country;?>");

	jQuery("#jform_country").change(function(){

		

		get_state(jQuery(this).val());

		jQuery("#jform_zip").unbind("keyup");
		jQuery("#jform_zip").val("");
		jQuery('#jform_zip').unmask();
		jQuery('#jform_city').val("");

		sel_c = jQuery(this).find(':selected').attr('data');
		console.log(sel_c);
		
		applyAddressDataCR(baseurl,sel_c,jQuery(this).val());

		jQuery("#clickrevertcurrency").attr('id','clickchangecurrency');

			//get_state(this_id);
		if(jQuery(this).find(":selected").attr('class').split(' ')[0]!="<?php echo $this->country->currency?>"){
			jQuery('#clickchangecurrency').show();
		}
			

	
			if(jQuery(this).find(":selected").attr('class').split(' ')[0] != "<?php echo $this->country[0]->currency;?>"){
				jQuery('#clickchangecurrency').css("display","block");

				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
				if(count_Ar.length > 2){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}


				jQuery('#clickchangecurrency').addClass(jQuery(this).find(":selected").attr('class').split(' ')[0]+" "+jQuery(this).find(":selected").attr('class').split(' ')[1]);
				var this_class=jQuery("#clickchangecurrency").attr('class').split(' ');
				jQuery("#clickchangecurrency").html("<a href='#' onclick='return false;'> Currency is "+this_class[1].split('_')[1]+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].split('_')[1]+this_class[2]+"</span>.</a></a>");
				
				jQuery("#chosenCurr").html(jQuery(this).find(":selected").attr('class').split(' ')[1].split('_')[1]+jQuery(this).find(":selected").attr('class').split(' ')[0]);
				
				jQuery("#jform_price1").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
			    jQuery("#jform_price2").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
				jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				});


			} else {
				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');

				jQuery("#jform_price1").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
			    jQuery("#jform_price2").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
				jQuery(".pricevalue").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				});

				if(count_Ar.length == 4){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				} else if(count_Ar.length == 3){					
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}

				jQuery('#clickchangecurrency').css("display","none");
			}

	});
			
		jQuery('#clickchangecurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[3].replace("cho_","")+this_class[2]+". Revert to "+this_class[1].replace("def_","")+this_class[0]+".</a>");
			jQuery("#jform_price1").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
			jQuery("#jform_price2").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
 			
			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			});

			jQuery("#jform_currency").val(this_class[2]);

			jQuery(this).attr('id','clickrevertcurrency');

		});

		jQuery('#clickrevertcurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[1].replace("def_","")+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].replace("cho_","")+this_class[2]+"</span>.</a></a>");
			jQuery("#jform_price1").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);
			jQuery("#jform_price2").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);


			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			});


			jQuery("#jform_currency").val(this_class[0]);

			jQuery(this).attr('id','clickchangecurrency');

		});	

			/*jQuery("#jform_zip").blur(function(){
				console.log('https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+jQuery(this).val()+'&country='+jQuery('#jform_country').find(":selected").attr('data')+'&username=damianwant33');
				jQuery.ajax({
	                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+jQuery(this).val()+'&country='+jQuery('#jform_country').find(":selected").attr('data')+'&username=damianwant33',
	                dataType: 'jsonp',
	                jsonp: 'callback',
	                jsonpCallback: 'jsonpCallback',
	                success: function(){
	                }
	            });
			});*/
			jQuery(".listingitem").click(function(){
				jQuery("input[name='jform[propertyid]']").val(jQuery(this).attr('data'));
			});
		
			jQuery("#refvalue").change(function() {
				var numeric = this.value.replace('%', '');
				jQuery("#refvalue").val(numeric.match(/[0-9]{1,2}/));
				jQuery("#refvalue").val(this.value+'%');
			});
			jQuery("#addphone").click(function(){
				jQuery("#clientmobile").append('<label class="countCode" style="margin-right:5px">'+con_Code+'</label><input style="width:290px" name="jform[mobile][]" type="text" class="validateme numbermask" validator="1" placeholder="'+con_placeholder+'" /> <br />');
				setValidators();
				set_masks();
			});
			jQuery("#addemail").click(function(){
				jQuery("#clientemail").append('<input name="jform[email][]" type="text" class="validateme" validator="0" placeholder="clientname@companyemail.com" /> <br />');
				setValidators();
			});
			jQuery(".agentname").click(function(){
				jQuery(this).parent().find(".listings").show();
			});
			jQuery("#showadd").live("click",function(){
				jQuery("#address_fields").slideDown();
				jQuery(this).html("-Hide address");
				jQuery(this).removeAttr("id").attr("id","hideadd");
			});
			jQuery("#hideadd").live("click",function(){
				jQuery("#address_fields").slideUp();
				jQuery("#address_fields input").val("");
				jQuery(this).html("+Enter address");
				jQuery(this).removeAttr("id").attr("id","showadd");
			});
			jQuery('.m_nholder').css('cursor', 'pointer');
			jQuery('.m_nholder').click(function(){
				var x = jQuery.parseJSON(jQuery(this).find(".data").val());
				console.log(x);
				jQuery('input[name="jform[name]"]').val(x.name);
				jQuery('#jform_contact_method').val(x.contact_method);
				jQuery('#jform_contact_method').change();
				jQuery('#jform_contact_method').select2();
				jQuery('#jform_contact_type').val(x.contact_type);
				jQuery('input[name="jform[buyerid]"]').val(x.buyer_id);
				jQuery('#jform_contact_type').select2();
				jQuery('select[name="jform[country]"]').val(x.address[0].country);
				jQuery('select[name="jform[country]"]').change()
					.promise().done(function(){
						jQuery('select[name="jform[country]"]').select2();
						jQuery('#stateselected').val(x.address[0].state);
					});
				jQuery('#jform_zip').val(x.address[0].zip);
				jQuery('input[name="jform[city]"]').val(x.address[0].city);
				jQuery('input[name="jform[address1]"]').val(x.address[0].address_1);
				jQuery('input[name="jform[address2]"]').val(x.address[0].address_2);
				for(var i=0; i<x.email.length; i++){
					if(i==0)
						jQuery('input[name="jform[email][]"]').val(x.email[i].email)
					else
						jQuery("#clientemail").append('<input name="jform[email][]" type="text" value="'+x.email[i].email+'" class="validateme" validator="0" placeholder="i.e: me@company.com" /> <br />');
				}
				for(var i=0; i<x.mobile.length; i++){
					if(i==0)
						jQuery('input[name="jform[mobile][]"]').val(x.mobile[i].number)
					else
						jQuery("#clientmobile").append('<input name="jform[mobile][]" type="text" value="'+x.mobile[i].number+'" class="validateme" validator="1" placeholder="i.e: (818) 123-1234" /> <br />');
				}
				setValidators();
			});
			jQuery('.m_nholder').on('mouseenter', function(){
				jQuery(this).parent().toggleClass('suggestion-hovered');
			});
			jQuery('.m_nholder').on('mouseleave', function(){
				jQuery(this).parent().toggleClass('suggestion-hovered');
			});
			//jQuery("#jform_country").change();
			setValidators();
		});
	
	
	function setValidators(){
		jQuery(".validateme").blur(function(){
			var regex = Array();
			regex[0] = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
			regex[1] = '[0-9\-\(\)\+ ]{3,20}';
			console.log(regex[jQuery(this).attr('validator')]);
			console.log(this.value.match(regex[jQuery(this).attr('validator')]));
			jQuery(this).val(this.value.match(regex[jQuery(this).attr('validator')]));
			});
			jQuery('input').keydown(function(){
				jQuery(this).qtip('destroy');
			});
		}
	
	var $ajax = jQuery.noConflict();
	
	function get_state(cID, state){
		$ajax("#state").html("loading...");
		$ajax.ajax({
			url: '<?php echo $this->baseurl ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID, 'state': state },
			success: function(e){
				console.log(e);
				var change="#state";

				if($ajax("#state").length < 1)
					change="#jform_state"
					$ajax(change).replaceWith(
						jQuery(e).addClass('mc_select')
							.addClass('left'))
							.promise()
							.done(
								function () {
									$ajax("#jform_state").attr('required', '');
									$ajax("#jform_state").val(<?php echo (isset($this->referral_data['jform']['state'])) ? $this->referral_data['jform']['state'] : $this->user->state; ?>);
									jQuery("#s2id_state").remove();
									jQuery("#s2id_jform_state").remove();
									try{
										$ajax("#jform_state").removeProp('required');
										$ajax("#jform_state").select2();
										$ajax("#jform_state").select2("val", jQuery('#stateselected').val());
									}catch(e){}

									
								}
							);
				}
			});
		}
		
	function set_masks(){
			jQuery(".numbermask").attr("placeholder",con_placeholder);
			jQuery(".numbermask").inputmask({mask:"<?php echo strtolower($this->getCountryLangsInitial->phoneFormat); ?>".split(",")});
		}
	
	
	function postToSession(url) {
		$ajax.ajax({
			url: '<?php echo $this->baseurl; ?>index.php/component/propertylisting/?task=referral_save_data',
			method: 'POST',
			data: $ajax("#referralform").serializeArray(),
			success: function() {
				location.href = url;
			}
		});
	}
	
	</script>
