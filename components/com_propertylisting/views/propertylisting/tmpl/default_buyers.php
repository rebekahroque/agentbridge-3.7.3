<?php
// language loading
  $language = JFactory::getLanguage();
  $extension = 'com_nrds';
  $base_dir = JPATH_SITE;
  $language_tag = JFactory::getUser()->currLanguage;
  $language->load($extension, $base_dir, $language_tag, true);
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$user =& JFactory::getUser($_SESSION['user_id']);
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
//$propertylisting_model = $this->getModel('propertylisting');
//var_dump($propertylisting_model); die();
//die();
?>
<style>
body {
	background: #fff;
}
#price2 {
	display:none;
}
#price {
	display:block;	
}
div.aa #buyer-aa-border {
	position: absolute;
    bottom: 96px;
}
.buyer_row {
	display:table;
	width:100%;
	position:relative;
}
</style>
<div class="wrapper">
	<div class="wide left buyers">
		<!-- start wide-content -->
		<input type="hidden" id="countbuyer" value="<?php echo $this->user_info->countbuyer ?>" onclick="removeBuyerButton()">
		<input type="hidden" id="usertype" value="<?php echo $this->user_info->user_type ?>">
		<h1><?php echo JText::_('COM_BUYERS_VIEW_MY') ?></h1>
		<input type="hidden" id="pageview" value="1">
		<?php $buyer_counter = count( $this->a_buyers ) + count( $this->b_buyers ) + count( $this->c_buyers ) + count( $this->i_buyers ); ?>
		<?php //if(count($this->buyers)) {?>
		<?php if($buyer_counter>=1) {?>
			<div class="items-filter">
				<!-- start list-tile filter -->
				<div id="changeview"><a id="changetotile" class="tile-view left view bg-bottom"></a> 
				<a id="changetolist" class="list-view left view bg-bottom"></a></div>
				<?php 
					/*$buyers = $this->buyers;
					$buyerctr = 0;
					foreach($buyers as $buyer){
						if(!empty($buyer->needs)){
							$buyerctr++;
						}
					}*/
				?>
				<span class="tile-number"><?php echo JText::_('COM_POPS_TERMS_YOUHAVEACTIVEPOPS') ?> <?php echo $buyer_counter.' '.JText::_('COM_BUYERS_VIEW_BUYER').''.((($buyer_counter) > 1) ? 's': ''); ?> 
				<?php #echo count($this->buyers).' Buyer'.((count($this->buyers) > 1) ? 's': ''); ?>
				</span> <a
					href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>"
					class="button gradient-green right addbuyersbtn" style="padding-top:8px"><span class="create-referral"><?php echo JText::_('COM_USERPROF_PROF_ADDB')?></span></a>
				<div class="c200 right" onMouseover="ddrivetip('Sort Buyers by category');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
					<form>
						<select name="buyer_filter" style="width:175px">
							<option value="nofilter"><?php echo JText::_('COM_POPS_VIEW_ALL')?> </option>
							<option <?php echo (isset($_GET['filter']) && $_GET['filter']=="abuyer") ? 'selected': '';?> value="buyer_a"><?php echo JText::_('COM_BUYERS_STATUS_OPT_ABUYER') ?></option>
							<option <?php echo (isset($_GET['filter']) && $_GET['filter']=="bbuyer") ? 'selected': '';?> value="buyer_b"><?php echo JText::_('COM_BUYERS_STATUS_OPT_BBUYER')?></option>
							<option <?php echo (isset($_GET['filter']) && $_GET['filter']=="cbuyer") ? 'selected': '';?> value="buyer_c"><?php echo JText::_('COM_BUYERS_STATUS_OPT_CBUYER')?></option>
							<option <?php echo (isset($_GET['filter']) && $_GET['filter']=="ibuyer") ? 'selected': '';?> value="buyer_i"><?php echo JText::_('COM_BUYERS_STATUS_OPT_INACTIVE')?></option>
						</select>
					</form>
				</div>
				<div class="clear-float"></div>
			</div>
			<!-- end list-tile filter -->
			<div id="noresultsfilter" style="display: none;">
				<p>No results found</p>
			</div>
			<?php if (!isset($_GET['filter']) || $_GET['filter']=='abuyer') : ?>
			<div id="buyer_a_items" class="items-container list-style">
				<!-- start items -->
				<?php if(count($this->a_buyers)): ?>
				<h1><?php echo JText::_('COM_BUYERS_STATUS_OPT_ABUYER') ?></h1>
				<!--ul class="items_buyer" style="width:690px"-->
				<ul class="items_buyer" >
					<?php
					$row_flag = 1;
					$cell_flag = 0;
					foreach($this->a_buyers as $buyer):
							$flagicon="";
							if($this->user_info->country!=$buyer->needs[0]->country){
								if($buyer->needs[0]->country == 0 && $this->user_info->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($buyer->needs[0]->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($buyer->needs[0]->countries_iso_code_2).'-flag-lang.png">';
								}								
							}
							//var_dump(expression)
					if(!$cell_flag) {
						echo "<div class='buyer_row' id='buyer_row$row_flag'>";
					}
					?>
					<?php #echo "<pre>"; print_r($buyer); die(); ?>
					<li>
						<div id="style_identifier" class="aa">
							<div class="rbox" style="margin-top: 10px; margin-left:2px;position:relative">
								<div class="rbox_list" style="display: block;">
										<div class="items-inner-container">
											<div class="image-placeholder buyer-image-placeholder">
												<a
													href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"
													class="item-number text-link"> 
													<img  src="<?php echo ($buyer->buyer_image) ? "images/buyers/56x46/" . $buyer->buyer_image : "https://www.agentbridge.com/images/house-thumb-60.png"; #echo trim($images[0]->image); ?>" border="0" />
												</a>
												<?php echo $buyer->buyer_image; ?>
											</div>
											<div class="description-placeholder">
												<a
													href="javascript:void(0)"
													class="item-number text-link"><?php echo $listing->zip ?> </a>
												<p class="reality-description">
													<!--<span class="text-link buyer-name-link">
														<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"><?php echo $buyer->name; ?></a>
													</span>
													Buyer
													<?php echo $buyer->needs[0]->zip; $abuyerzip = $buyer->needs[0]->zip; ?>
													<?php $price = explode('-', $buyer->price_value); ?>
													<?php echo format_currency_global($price[0]).((!empty($price[1])) ? ' - '.format_currency_global($price[1]) : ''); ?> -->
													<?php
														$buyerzip = explode(",",$buyer->needs[0]->zip);
														$buyerzip = implode(", ", $buyerzip);
													?>
													<ul class="buyer-detail-list">
														<li class="text-link"><a class="titlelink empty <?php echo $buyer->buyer_id?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"><?php echo stripslashes_all($buyer->name); ?></a></li>
														<li style="margin-top:10px"><?php echo $flagicon." ".$buyerzip; ?></li>
														<!--<li><?php $price = explode('-', $buyer->price_value); ?></li>-->
														<?php 														
															if($buyer->home_buyer){
																$buyer->needs[0]->symbol =$this->user_info->symbol;
																$buyer->needs[0]->currency =$this->user_info->currency;
															}
														?>
														<li id="price" class="price_con"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></li>
													</ul>
												</p>
												<div class="l_update_buyer" style="padding-top:5px; font-size:10px">
													<?php echo "".JText::_('COM_LASTUPDATED')."".trim($buyer->last_update); ?>
												</div>
											</div>
										</div>
										<?php 
											$buyer_type = $buyer->buyer_type;
											#echo $buyer_type;
											$buyertype = "";
											$buyerclass = "";
											if($buyer_type == "A Buyer"){
												$buyertype = "A";
												$buyerclass = "buyer_a";
											} else if($buyer_type == "B Buyer"){
												$buyertype = "B";
												$buyerclass = "buyer_b";
											} else if($buyer_type == "C Buyer"){
												$buyertype = "C";
												$buyerclass = "buyer_c";
											} else if($buyer_type == "Inactive"){
												$buyertype = "In";
												$buyerclass = "buyer_inactive";
											}
										?>
										<div id="buyer-type" class="all-view <?php echo $buyerclass; ?>">
											<?php echo $buyertype; ?>
										</div>
								</div>
								<div id="price2"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></div>
								<div id="buyer-needs-container">
								<div id="buyer-needs" class="buyer-needs">
										<?php
										echo $buyer->buyer_feats['initial_feats'];
										?>
									</div>
								</div>
                                <div class="other-description-placeholder right" style="width:210px; margin:10px">
                                		<?php if($buyer->listingcount > 0){ ?>
											 <?php $var3=JText::_('COM_BUYERS_ALLPOPS')." (".$buyer->listingcount.")";	
											 	   $mouseover=""; ?>
										<?php } else {
												$var3="";
												$mouseover = 'onMouseover="ddrivetip(\'No POPs&trade; match your buyer criteria. <br /> Consider adjusting to increase possible matches. \');hover_dd()" onMouseout="hideddrivetip();hover_dd()"';
										}
										?>
										<?php if($buyer->hasnew == 1  && $buyer->listingcount_new){ ?>
											<div style="margin-bottom: 5px;">
                                        <a style="font-size:12px;margin-left:5px; margin-right:5px" class="a-new new-list <?php echo $buyer->buyer_id; ?>" id="newctr2a<?php echo $buyer->buyer_id; ?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=New&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>">
												<?php echo JText::_('COM_BUYERS_NEWPOPS') ?> (<?php echo $buyer->listingcount_new; ?>)
										</a></div>
										<?php	} ?>
                                        <?php 											
                                            $saved_buyer_ctr = 0;
											if(count($buyer->hassaved) > 0){
												foreach($buyer->hassaved as $saved){
													if($saved->active == 1){
														$saved_buyer_ctr++;			
													}		
												}
											}
											if($saved_buyer_ctr > 0){?>
                                        <div style="margin-bottom: 5px;">
                                        <a class="savedlink empty <?php echo $buyer->buyer_id?>" style="margin-left:5px; margin-right:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=Saved&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php
												echo JText::_('COM_BUYERS_SAVEDPOPS')." (".$saved_buyer_ctr.")";
										?></a></div>
										<?php }	?>
										<?php 
											if($buyer->listingcount==0){
											 $buyer->listingcount=$buyer->listingcount_new;
											}
											?>
                                        <?php if($buyer->listingcount > 0 ){ ?>
                                        	<div style="margin-bottom: 5px;">
										<a class="alllink empty <?php echo $buyer->buyer_id?>" style="margin-right:5px; margin-left:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=All&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>" <?php echo $mouseover?>  >
											<?php echo JText::_('COM_BUYERS_ALLPOPS')?> (<?php echo $buyer->listingcount; ?>)</a></div>
											<?php } else if((!$buyer->hasnew && !$buyer->hassaved) ||  $buyer->listingcount==0){
												echo "<span class='buyermatch'>".JText::_('COM_BUYERS_NOMATCHINGPOPS')."</span>";
											}
											?>
										</div>
							</div>
						</div>
					</li>
					<?php
					if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;	
						}	
					endforeach;
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;
						$cell_flag = 0;
					}
					?>
				</ul>
			</div>
			<?php
			endif;
			?>
			<?php endif; if (!isset($_GET['filter']) || $_GET['filter']=='bbuyer') : ?>
			<?php
			if(count($this->b_buyers)):
			?>
			<div id="buyer_b_items" class="items-container list-style">
				<!-- start items -->
				<h1><?php echo JText::_('COM_BUYERS_STATUS_OPT_BBUYER') ?></h1>
				<!--ul class="items_buyer" style="width:690px"-->
				<ul class="items_buyer" >
					<?php
					foreach($this->b_buyers as $buyer):
					#echo "<pre>", print_r($buyer), "</pre>";
							$flagicon="";
							if($this->user_info->country!=$buyer->needs[0]->country){
								if($buyer->needs[0]->country == 0 && $this->user_info->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($buyer->needs[0]->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($buyer->needs[0]->countries_iso_code_2).'-flag-lang.png">';
								}								
							}
					if(!$cell_flag) {
						echo "<div class='buyer_row' id='buyer_row$row_flag'>";
					}
					?>
					<li>
					<div id="style_identifier" class="aa">
						<div class="rbox" style="margin-top: 10px; margin-left:2px">
							<div class="rbox_list" style="display: block;">
								<div class="items-inner-container">
									<div class="image-placeholder buyer-image-placeholder">
										<a
											href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"
											class="item-number text-link"> <img 
											src="<?php echo ($buyer->buyer_image) ? "images/buyers/56x46/" . $buyer->buyer_image : "https://www.agentbridge.com/images/house-thumb-60.png"; #echo trim($images[0]->image); ?>?true" border="0" />
										</a>
									</div>
									<div class="description-placeholder">
										<a
											href="javascript:void(0)"
											class="item-number text-link"><?php echo $listing->zip ?> </a>
										<p class="reality-description">
											<!--<span class="text-link buyer-name-link"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"><?php echo $buyer->name; ?></a></span><br/>
											<div class="buyer-details-small">
											Buyer<br/>
											<?php echo $buyer->needs[0]->zip; $bbuyerzip = $buyer->needs[0]->zip; ?><br/>
											<?php $price = explode('-', $buyer->price_value); ?>
											<?php echo format_currency_global($price[0]).((!empty($price[1])) ? ' - '.format_currency_global($price[1]) : ''); ?>
											</div> -->
											<?php #print_r($buyer); die(); ?>
											<?php
														$buyerzip = explode(",",$buyer->needs[0]->zip);
														$buyerzip = implode(", ", $buyerzip);
													?>
											<ul class="buyer-detail-list">
												<li class="text-link"><a class="titlelink empty <?php echo $buyer->buyer_id?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php echo stripslashes_all($buyer->name); ?></a></li>
												<li style="margin-top:10px"><?php echo $flagicon." ".$buyerzip; ?></li>
												<!--<li><?php $price = explode('-', $buyer->price_value); ?></li>-->
												<li id="price"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></li>
											</ul>
										</p>
										<div class="l_update_buyer" style="padding-top:5px; font-size:10px">
											<?php echo "".JText::_('COM_LASTUPDATED')."".trim($buyer->last_update); ?>
										</div>
									</div>
								</div>
								<?php 
									$buyer_type = $buyer->buyer_type;
									#echo $buyer_type;
									$buyertype = "";
									$buyerclass = "";
									if($buyer_type == "A Buyer"){
										$buyertype = "A";
										$buyerclass = "buyer_a";
									} else if($buyer_type == "B Buyer"){
										$buyertype = "B";
										$buyerclass = "buyer_b";
									} else if($buyer_type == "C Buyer"){
										$buyertype = "C";
										$buyerclass = "buyer_c";
									} else if($buyer_type == "Inactive"){
										$buyertype = "In";
										$buyerclass = "buyer_inactive";
									}
								?>
								<div id="buyer-type" class="all-view <?php echo $buyerclass; ?>">
									<?php echo $buyertype; ?>
								</div>
							</div>
							<div id="price2"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></div>
							<div id="buyer-needs-container">
								<div id="buyer-needs" class="buyer-needs">
									<?php
										echo $buyer->buyer_feats['initial_feats'];
										?>
									</div>
											</div>
                            <div class="other-description-placeholder right" style="width:210px; margin:10px" id="b-button-placeholder">
                            		<?php if($buyer->listingcount > 0){ ?>
											 <?php $var3=JText::_('COM_BUYERS_ALLPOPS')." (".$buyer->listingcount.")";	
											 	   $mouseover=""; ?>
										<?php } else {
												$var3="";
												$mouseover = 'onMouseover="ddrivetip(\'No POPs&trade; match your buyer criteria. <br /> Consider adjusting to increase possible matches. \');hover_dd()" onMouseout="hideddrivetip();hover_dd()"';
										}
										?>
										<?php if($buyer->hasnew == 1  && $buyer->listingcount_new){ ?>
										 <div style="margin-bottom: 5px;">
                               			 <a style="font-size: 12px;margin-left:5px; margin-right:5px" class="bnew new-list <?php echo $buyer->buyer_id; ?>" id="newctr2b<?php echo $buyer->buyer_id; ?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=New&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>">
										<?php echo JText::_('COM_BUYERS_NEWPOPS') ?> (<?php echo $buyer->listingcount_new; ?>)
										</a></div>
										<?php	
											}
											?>
									<?php 		$saved_buyer_ctr = 0;
												if(count($buyer->hassaved) > 0){
													foreach($buyer->hassaved as $saved){
														if($saved->active == 1){
															$saved_buyer_ctr++;			
														}		
													}
												}
												if($saved_buyer_ctr > 0){ ?>
									<div style="margin-bottom: 5px;">
	                                <a class="savedlink empty <?php echo $buyer->buyer_id?>" style="margin-left:5px; margin-right:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=Saved&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>">
	                                <?php
													echo JText::_('COM_BUYERS_SAVEDPOPS')." (".$saved_buyer_ctr.")";
											?></a></div>
									<?php }	?>
									<?php
										if($buyer->listingcount==0){
											 $buyer->listingcount=$buyer->listingcount_new;
											}
									?>
                                   <?php if($buyer->listingcount > 0 ){ ?>
                                   	<div style="margin-bottom: 5px;">
								   <a class="alllink empty <?php echo $buyer->buyer_id?>" style="margin-right:5px; margin-left:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=All&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>" <?php echo $mouseover?> >
										<?php echo JText::_('COM_BUYERS_ALLPOPS')?> (<?php echo $buyer->listingcount; ?>)</a></div>
									<?php } else if((!$buyer->hasnew && !$buyer->hassaved) ||  $buyer->listingcount==0) {
										echo "<span class='buyermatch'>".JText::_('COM_BUYERS_NOMATCHINGPOPS')."</span>";
									}
									?>
								</div>
						</div>
					</div>
					</li>
					<?php
					if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;	
						}						
					endforeach;
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;	
						$cell_flag = 0;
					}
					?>
				</ul>
			</div>
			<?php
			endif;
			?>			
			<?php endif; if (!isset($_GET['filter']) || $_GET['filter']=='cbuyer') : ?>
			<?php
			if(count($this->c_buyers)):
			?>
			<div id="buyer_c_items" class="items-container list-style">
				<!-- start items -->
				<h1><?php echo JText::_('COM_BUYERS_STATUS_OPT_CBUYER') ?></h1>
				<!--ul class="items_buyer" style="width:690px"-->
				<ul class="items_buyer" >
					<?php 
					foreach($this->c_buyers as $buyer):
							$flagicon="";
							if($this->user_info->country!=$buyer->needs[0]->country){
								if($buyer->needs[0]->country == 0 && $this->user_info->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($buyer->needs[0]->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($buyer->needs[0]->countries_iso_code_2).'-flag-lang.png">';
								}								
							}
					if(!$cell_flag) {
						echo "<div class='buyer_row' id='buyer_row$row_flag'>";
					}
					?>
					<li>
						<div id="style_identifier" class="aa" >
							<div class="rbox" style="margin-top: 10px; margin-left:2px">
								<div class="rbox_list" style="display: block;">
									<div class="items-inner-container">
										<div class="image-placeholder buyer-image-placeholder">
											<a
												href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"
												class="item-number text-link"> <img 
												src="<?php echo ($buyer->buyer_image) ? "images/buyers/56x46/" . $buyer->buyer_image : "https://www.agentbridge.com/images/house-thumb-60.png"; #echo trim($images[0]->image); ?>?true" border="0" />
											</a>
											<?php echo $buyer->buyer_image; ?>
										</div>
										<div class="description-placeholder">
											<a
												href="javascript:void(0)"
												class="item-number text-link"><?php echo $listing->zip ?> </a>
											<p class="reality-description">
												<!--<span class="text-link buyer-name-link"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"><?php echo $buyer->name; ?></a></span><br/>
												<div class="buyer-details-small">
												Buyer<br/>
												<?php echo $buyer->needs[0]->zip; $cbuyerzip = $buyer->needs[0]->zip; ?><br/>
												<?php $price = explode('-', $buyer->price_value); ?>
												<?php echo format_currency_global($price[0]).((!empty($price[1])) ? ' - '.format_currency_global($price[1]) : ''); ?>
												</div> -->
												<?php
														$buyerzip = explode(",",$buyer->needs[0]->zip);
														$buyerzip = implode(", ", $buyerzip);
													?>
												<ul class="buyer-detail-list">
													<li class="text-link"><a class="titlelink empty <?php echo $buyer->buyer_id?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php echo stripslashes_all($buyer->name); ?></a></li>
													<li style="margin-top:10px"><?php echo $flagicon." ".$buyerzip; ?></li>
													<!--<li><?php $price = explode('-', $buyer->price_value); ?></li>-->
													<li id="price"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></li>
												</ul>
											</p>
											<div class="l_update_buyer" style="padding-top:5px; font-size:10px">
												<?php echo "".JText::_('COM_LASTUPDATED')."".trim($buyer->last_update); ?>
											</div>
										</div>
									</div>
									<?php 
										$buyer_type = $buyer->buyer_type;
										#echo $buyer_type;
										$buyertype = "";
										$buyerclass = "";
										if($buyer_type == "A Buyer"){
											$buyertype = "A";
											$buyerclass = "buyer_a";
										} else if($buyer_type == "B Buyer"){
											$buyertype = "B";
											$buyerclass = "buyer_b";
										} else if($buyer_type == "C Buyer"){
											$buyertype = "C";
											$buyerclass = "buyer_c";
										} else if($buyer_type == "Inactive"){
											$buyertype = "In";
											$buyerclass = "buyer_inactive";
										}
									?>
									<div id="buyer-type" class="all-view <?php echo $buyerclass; ?>">
										<?php echo $buyertype; ?>
									</div>
								</div>
								<div id="price2"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></div>
								<div id="buyer-needs-container">
													<div id="buyer-needs" class="buyer-needs">
										<?php
										echo $buyer->buyer_feats['initial_feats'];
										?>
									</div>
												</div>
                                <div class="other-description-placeholder right" style="width:210px; margin:10px">
                                		<?php if($buyer->listingcount > 0){ ?>
											 <?php $var3=JText::_('COM_BUYERS_ALLPOPS')." (".$buyer->listingcount.")";	
											 	   $mouseover=""; ?>
										<?php } else {
												$var3="";
												$mouseover = 'onMouseover="ddrivetip(\'No POPs&trade; match your buyer criteria. <br /> Consider adjusting to increase possible matches. \');hover_dd()" onMouseout="hideddrivetip();hover_dd()"';
										}
										?>	
									<?php if($buyer->hasnew == 1 && $buyer->listingcount_new){ ?>
                                	<div style="margin-bottom: 5px;">
	                                    <a style="margin-left:5px; margin-right:5px;font-size:12px" class="c-new new-list <?php echo $buyer->buyer_id; ?>" id="newctr2c<?php echo $buyer->buyer_id; ?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=New&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>">
											<?php echo JText::_('COM_BUYERS_NEWPOPS') ?> (<?php echo $buyer->listingcount_new; ?>)
										</a></div>
									<?php }	?>
									<?php 	$saved_buyer_ctr = 0;
												if(count($buyer->hassaved) > 0){
													foreach($buyer->hassaved as $saved){
														if($saved->active == 1){
															$saved_buyer_ctr++;			
														}		
													}
												}
												if($saved_buyer_ctr > 0){ ?>
									<div style="margin-bottom: 5px;">
	                                    <a class="savedlink empty <?php echo $buyer->buyer_id?>" style="margin-left:5px; margin-right:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=Saved&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php
													echo JText::_('COM_BUYERS_SAVEDPOPS')." (".$saved_buyer_ctr.")";
											?></a>
									</div>
									<?php }?>
									<?php 
										if($buyer->listingcount==0){
											 $buyer->listingcount=$buyer->listingcount_new;
											}
									?>
									<?php if($buyer->listingcount > 0 ){ ?>
										<div style="margin-bottom: 5px;">
									<a class="alllink empty <?php echo $buyer->buyer_id?>" style="margin-right:5px; margin-left:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=All&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>" <?php echo $mouseover?> >
									<?php echo JText::_('COM_BUYERS_ALLPOPS')?> (<?php echo $buyer->listingcount; ?>)</a></div>
									<?php } else if((!$buyer->hasnew && !$buyer->hassaved) ||  $buyer->listingcount==0) {
										echo "<span class='buyermatch'>".JText::_('COM_BUYERS_NOMATCHINGPOPS')."</span>";
									}
									?>
									</div>
							</div>
						</div>
					</li>
					<?php					
					if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;	
						}					
					endforeach;
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;	
						$cell_flag = 0;
					}
					?>
				</ul>
			</div>
			<?php
			endif;
			?>
			<?php endif; if (!isset($_GET['filter']) || $_GET['filter']=='ibuyer') : ?>
			<?php 
			if(count($this->i_buyers)):
			?>
			<div id="buyer_i_items" class="items-container list-style">
				<!-- start items -->
				<h1><?php echo JText::_('COM_BUYERS_STATUS_OPT_INACTIVE') ?></h1>
				<!--ul class="items_buyer" style="width:690px"-->
				<ul class="items_buyer">
					<?php
					foreach($this->i_buyers as $buyer):
							$flagicon="";
							if($this->user_info->country!=$buyer->needs[0]->country){
								if($buyer->needs[0]->country == 0 && $this->user_info->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($buyer->needs[0]->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($buyer->needs[0]->countries_iso_code_2).'-flag-lang.png">';
								}								
							}
					if(!$cell_flag) {
						echo "<div class='buyer_row' id='buyer_row$row_flag'>";
					}
					?>
					<li>
						<div id="style_identifier" class="aa" >
							<div class="rbox" style="margin-top: 10px; margin-left:2px">
								<div class="rbox_list" style="display: block;">
									<div class="items-inner-container">
										<div class="image-placeholder buyer-image-placeholder">
											<a
												href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"
												class="item-number text-link"> <img 
												src="<?php echo ($buyer->buyer_image) ? "images/buyers/56x46/" . $buyer->buyer_image : "https://www.agentbridge.com/images/house-thumb-60.png"; #echo trim($images[0]->image); ?>?true" border="0" />
											</a>
											<?php echo $buyer->buyer_image; ?>
										</div>
										<div class="description-placeholder">
											<a
												href="javascript:void(0)"
												class="item-number text-link"><?php echo $listing->zip ?> </a>
											<p class="reality-description">
												<!--<span class="text-link buyer-name-link"><a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id); ?>"><?php echo $buyer->name; ?></a></span><br/>
												<div class="buyer-details-small">
												Buyer<br/>
												<?php echo $buyer->needs[0]->zip; $ibuyerzip = $buyer->needs[0]->zip; ?><br/>
												<?php $price = explode('-', $buyer->price_value); ?>
												<?php echo format_currency_global($price[0]).((!empty($price[1])) ? ' - '.format_currency_global($price[1]) : ''); ?>
												</div>-->
												<?php
														$buyerzip = explode(",",$buyer->needs[0]->zip);
														$buyerzip = implode(", ", $buyerzip);
													?>
												<ul class="buyer-detail-list">
													<li class="text-link"><a class="titlelink empty <?php echo $buyer->buyer_id?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php echo stripslashes_all($buyer->name); ?></a></li>
													<li style="margin-top:10px"><?php echo $flagicon." ".$buyerzip; ?></li>
													<!--<li><?php $price = explode('-', $buyer->price_value); ?></li>-->
													<li id="price"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></li>
												</ul>
											</p>
											<div class="l_update_buyer" style="padding-top:5px; font-size:10px">
												<?php echo "".JText::_('COM_LASTUPDATED')."".trim($buyer->last_update); ?>
											</div>
										</div>
									</div>
									<?php 
										$buyer_type = $buyer->buyer_type;
										$buyertype = "";
										$buyerclass = "";
										if($buyer_type == "A Buyer"){
											$buyertype = "A";
											$buyerclass = "buyer_a";
										} else if($buyer_type == "B Buyer"){
											$buyertype = "B";
											$buyerclass = "buyer_b";
										} else if($buyer_type == "C Buyer"){
											$buyertype = "C";
											$buyerclass = "buyer_c";
										} else if($buyer_type == "Inactive"){
											$buyertype = "In";
											$buyerclass = "buyer_inactive";
										}
									?>
									<div id="buyer-type" class="all-view <?php echo $buyerclass; ?>">
										<?php echo $buyertype; ?>
									</div>
								</div>
								<div id="price2"><?php echo format_currency_global($price[0],$buyer->needs[0]->symbol).((!empty($price[1])) ? ' - '.format_currency_global($price[1],$buyer->needs[0]->symbol) : '')." ".$buyer->needs[0]->currency; ?></div>
								<div id="buyer-needs-container">
									<div id="buyer-needs" class="buyer-needs">
										<?php
										echo $buyer->buyer_feats['initial_feats'];
										?>
									</div>
								</div>
                                <div class="other-description-placeholder right" style="width:210px; margin:10px">
                                		<?php if($buyer->listingcount > 0){ ?>
											 <?php $var3=JText::_('COM_BUYERS_ALLPOPS')." (".$buyer->listingcount.")";	
											 	   $mouseover=""; ?>
										<?php } else {
												$var3="";
												$mouseover = 'onMouseover="ddrivetip(\'No POPs&trade; match your buyer criteria. <br /> Consider adjusting to increase possible matches. \');hover_dd()" onMouseout="hideddrivetip();hover_dd()"';
										}
										?>
										<?php if($buyer->hasnew == 1 && $buyer->listingcount_new>0){ ?>
                                		<div style="margin-bottom: 5px;">
	                                    	<a style="font-size:12px;margin-left:5px; margin-right:5px" class="i-new new-list <?php echo $buyer->buyer_id; ?>" id="newctr2i<?php echo $buyer->buyer_id; ?>" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=New&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>">									
												<?php echo JText::_('COM_BUYERS_NEWPOPS') ?> (<?php echo $buyer->listingcount_new; ?>)
											</a>
										</div>
										<?php } ?>
										<?php $saved_buyer_ctr = 0;
											if(count($buyer->hassaved) > 0){
												foreach($buyer->hassaved as $saved){
													if($saved->active == 1){
														$saved_buyer_ctr++;			
													}		
												}
											}
											if($saved_buyer_ctr > 0){ 
										?>
										<div style="margin-bottom: 5px;">
                                   		<a class="savedlink empty <?php echo $buyer->buyer_id?>" style="margin-left:5px; margin-right:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=Saved&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>"><?php
												echo JText::_('COM_BUYERS_SAVEDPOPS')." (".$saved_buyer_ctr.")";
										?></a></div>
										<?php }	?>
										<?php 
											if($buyer->listingcount==0){
											 $buyer->listingcount=$buyer->listingcount_new;
											}
										?>
                                    <?php if($buyer->listingcount > 0){ ?>
                                    	<div style="margin-bottom: 5px;">
									<a class="alllink empty <?php echo $buyer->buyer_id?>" style="margin-right:5px; margin-left:5px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&def_open=All&task=individualbuyers&lID=' . $buyer->buyer_id."#divmatchpop"); ?>" <?php echo $mouseover?> >
										<?php echo JText::_('COM_BUYERS_ALLPOPS')?> (<?php echo $buyer->listingcount; ?>)</a></div>
										<?php }else if((!$buyer->hasnew && !$buyer->hassaved) ||  $buyer->listingcount==0){
											echo "<span class='buyermatch'>".JText::_('COM_BUYERS_NOMATCHINGPOPS')."</span>";
										}
										?>
									</div>
							</div>
						</div>
					</li>
					
					<?php
						if($cell_flag < 2) {
							$cell_flag++;
						} else {
							$cell_flag = 0;
							echo "</div>";
							$row_flag++;
						}			
					endforeach;
					if($cell_flag > 0) {
						echo "</div>";
						$row_flag++;
						$cell_flag = 0;
					}
					?>
				</ul>
			</div>
			<?php
			endif;
			?>
		<?php endif; ?>
		<!-- end items -->
	<?php }else { ?>
			<div id="nopops" class="right" style="margin: 10px auto">
			<a id="addbuyer" style="padding-top:8px; margin-right:10px; display:none" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>" class="button gradient-green right"> <span class="create-referral"><?php echo JText::_('COM_USERPROF_PROF_ADDB') ?></span></a> 
            <a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>" class="button gradient-green right">+ <?php echo JText::_('COM_USERPROF_PROF_ADDP')?></a>
			<a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newreferral')?>" class="button gradient-green right"><em class="refer-me-text" style="padding-left:20px; margin-left:0px; font-size:12px"><?php echo JText::_('COM_USERPROF_PROF_CREATER') ?></em></a>
			</div>
			<div id="addbuyerdiv" onclick="showbuyerdiv()" class="clear-float premium-indicatior" style="background-color:#EEEEEE; display:none;width:auto;height:40px;border:1px solid #DEDEDE">
			<div class="left" style="font-size:12px;padding:12px">
				<?php echo JText::_('COM_USERPROF_PROF_ADDBT') ?>
			</div>
			<div class="right" style="padding:4px">
				<div class="profile-btn right"><a style="padding-top:8px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile') ?>" class='button gradient-green left'> <span class='create-referral'><?php echo JText::_('COM_USERPROF_PROF_ADDB') ?></span></a> 
			   <span class="left text-link cursor-point" style="padding:8px"  id="hideaddbuyerdiv"  onclick="hidebuyerdiv()"><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div style='clear:both'></div>
			<div id='invitediv' class='premium-indicatior' style='display:none;background-color:#EEEEEE;width:auto;height:40px;border:1px solid #DEDEDE'>
			<div class='left' style='font-size:12px;padding:12px'>
				<?php echo JText::_('COM_ACTIVITY_SPONSOR')?>
			</div>
			<div class='right' style='padding:4px'>
				<div class='profile-btn right'><a class='button gradient-green left' href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile#invitesection') ?>">&nbsp;  &nbsp; <span class='create-referral'>  <?php echo JText::_('COM_USERPROF_PROF_INV') ?> </span></a>
			    <span class='left text-link cursor-point' style='padding:8px'  id='not-now' onclick='hideinvitediv()'><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div style='clear:both'></div>
	<?php } ?>
		<!--<div id="edittext" style="display: none">
			<input type="text" id="userinput" class="textbox-up"/> <input
				type="hidden" id="editid" /> <br/><span>Please input the number of days in the field e.g. 180, 60</span><br/><input type="button" value="Submit"
				id="changevalue" />
		</div>-->
	</div>
	<!-- end wide-content -->
</div>
	<?php
	include( 'includes/sidebar_left.php' );
	?>
</div>
<script>
mixpanel.track("My Buyers");

function showbuyerdiv(){
		var buyercount = jQuery( "#countbuyer" ).val();
		if (buyercount == 0)
		jQuery("#addbuyerdiv").show();		
	}
	function hidebuyerdiv(){
		var utype = jQuery( "#usertype" ).val();
		jQuery("#addbuyerdiv").remove();
		jQuery( "#addbuyer" ).show();
		if (utype == 1 || utype == 4 )
		 jQuery("#invitediv").show();	
	}
	function hideinvitediv(){
		//jQuery("#addbuyerdiv").remove();
		jQuery("#invite-button" ).show();
		jQuery("#invitediv").remove();	
	}
	function removeBuyerButton(){
		var buyercount = jQuery( "#countbuyer" ).val();
		var utype = jQuery( "#usertype" ).val();
		if (buyercount >= 1 && utype==3)
			jQuery("#addbuyer").show();
		if (buyercount >= 1 && utype!=3) {
			jQuery( "#addbuyer" ).show();
			jQuery("#invitediv").show();
		}
	}
function pageView(){
    var page = jQuery("#pageview").val();
	if (page==1)
	   jQuery(".aa").removeClass('aa').addClass('b_rbox');
}
function update(lid){
	jQuery( "#editid" ).val(lid);
	jQuery( "#edittext" ).dialog({modal: true});
}
function print(data,list){
	var html="";
	var items;
	items = jQuery.parseJSON(data);
	if(items.length==0){
		jQuery("div.items-container").html("<br/>no results found");
	}
	else{
		jQuery("div.items-container").html("<ul class='items'></ul>");
	}
	jQuery(".items").html(html).promise().done(function(){
		Landing.format();
	});
}
jQuery("#changevalue").click(function(){
	var lid = jQuery( "#editid" ).val();
	var userinput = jQuery( "#userinput" ).val();
	jQuery.post("<?php echo JRoute::_('index.php?option=com_userprofile&task=updatelisting'); ?>&id="+lid+"&value="+userinput,
			function(data){
				var result = jQuery.parseJSON(data);
				if(result.code == 1){
					alert(result.value);
					jQuery( "#edittext" ).dialog("close");
					jQuery( "#item"+lid ).html(result.days);
				}
				else
					alert(result.value);
			});
});
var list=true;
var itemsLoaded = false;
var items;
jQuery("#changeview a").click(function(){
	list=!list;
	console.log(list);
	jQuery("div.items-filter a").toggleClass('bg-bottom');
	jQuery("div#style_identifier").toggleClass('b_rbox');
	jQuery("div#style_identifier").toggleClass('aa');
	jQuery("ul.items");
	//pg = jQuery("#pageview").value();
	//pg.value = 1;
	//alert(pg);
	if(!list) {
		jQuery('.buyer_row').each(function(index){
			current_id = jQuery(this).attr("id");	
			jQuery('.list-style #' + current_id + ' .buyer-needs').attr("style", "");
			jQuery('.list-style #' + current_id + ' .buyer-detail-list').attr("style", "");
		});
	} else {
		jQuery('.buyer_row').each(function(index){
			current_id = jQuery(this).attr("id");
			// Get an array of all element heights
			var elementHeights = [];
			var ehIndex = 0;
			
			var bdlHeights = [];
			var bdlIndex = 0;
			
			var elementHeights = jQuery('.list-style #' + current_id + ' .buyer-needs').map(function() {
				return jQuery(this).height();
			}).get();
			
			var bdlHeights = jQuery('.list-style #' + current_id + ' .buyer-detail-list').map(function() {
				return jQuery(this).height();
			}).get();

			// Math.max takes a variable number of arguments
			// `apply` is equivalent to passing each height as an argument
			var maxHeight = Math.max.apply(null, elementHeights);
			var bdlMaxHeight = Math.max.apply(null, bdlHeights);
			
			//alert(maxHeight + " - " + bdlMaxHeight);
			
			
			// Set each height to the max height
			jQuery('.list-style #' + current_id + ' .buyer-needs').css("min-height",(maxHeight+10)+"px");
			jQuery('.list-style #' + current_id + ' .buyer-detail-list').css("min-height",(bdlMaxHeight)+"px");
		});
	}
});
jQuery("select[name=\"buyer_filter\"]").change(function(){
	var v_style ="";
	if(jQuery(".list-view").hasClass("bg-bottom")){
		v_style='list';
	} else {
		v_style='grid';
	}

	var id = jQuery(this).val();
	if(id=="nofilter"){
		jQuery('.items-container.list-style').show();
		jQuery("#noresultsfilter").hide();	
	} else {
		if(jQuery("#"+id+"_items").length){
			jQuery("#"+id+"_items").show();	
			jQuery("#noresultsfilter").hide();		
		} else {
			jQuery("#noresultsfilter").show();
		}
		
		jQuery('.items-container.list-style').not("#"+id+"_items").hide();
	}
	
	/*<?php $newRequest = preg_replace('/[?&]filter=[a-z]{6}/', '', $_SERVER['REQUEST_URI']);
		  $newRequest = preg_replace('/[?&]vstyle=[a-z]{4}/', '', $newRequest);
	?>
	if(jQuery(this).val()=='nofilter')
		window.location = "//<?php echo $_SERVER['HTTP_HOST'].$newRequest.(isset($_GET) ? '&' : '?');?>vstyle="+v_style;
	else
		window.location = "//<?php echo $_SERVER['HTTP_HOST'].$newRequest.(isset($_GET) ? '&' : '?'); ?>filter="+jQuery(this).val()+"&vstyle="+v_style;*/


});
jQuery(document).ready(function(){
	//getExcRates();
	jQuery("#countbuyer").trigger("click");
	jQuery("#addbuyerdiv").trigger("click");
	jQuery("#userinput").datepicker();
	<?php if($_GET['vstyle']=="grid"){ ?>
		jQuery(".b_rbox").removeClass('b_rbox').addClass('aa');
		jQuery(".tile-view").removeClass('bg-bottom');
		jQuery(".list-view").removeClass('bg-bottom');		
	<?php }?>
	
	
	jQuery('.buyer_row').each(function(index){
		current_id = jQuery(this).attr("id");
		// Get an array of all element heights
		var elementHeights = [];
		var ehIndex = 0;
		
		var bdlHeights = [];
		var bdlIndex = 0;
		
		var elementHeights = jQuery('.list-style #' + current_id + ' .buyer-needs').map(function() {
			return jQuery(this).height();
		}).get();
		
		var bdlHeights = jQuery('.list-style #' + current_id + ' .buyer-detail-list').map(function() {
			return jQuery(this).height();
		}).get();

		// Math.max takes a variable number of arguments
		// `apply` is equivalent to passing each height as an argument
		var maxHeight = Math.max.apply(null, elementHeights);
		var bdlMaxHeight = Math.max.apply(null, bdlHeights);
		
		//alert(maxHeight + " - " + bdlMaxHeight);
		
		
		// Set each height to the max height
		jQuery('.list-style #' + current_id + ' .buyer-needs').css("min-height",(maxHeight+10)+"px");
		jQuery('.list-style #' + current_id + ' .buyer-detail-list').css("min-height",(bdlMaxHeight)+"px");
	});
});
</script>
<?php 
	if(!isset($_SESSION['viewed_buyerspage']) || $_SESSION['viewed_buyerspage'] == 0){
		$_SESSION['viewed_buyerspage'] = 1;	
	}
?>