<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$user =& JFactory::getUser($_SESSION['user_id']);
if(isset($_GET['uid']) && $_GET['uid'] !== $_SESSION['user_id']) {
	$uid = $_GET['uid'];
} else {
	$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
}
$current_user = JFactory::getUser($uid);
$query= $db->getQuery(true);
$query->select('count(*) as count')
->from('#__pocket_listing')
->where('user_id = '.$uid.' and closed = 0 AND sold = 0');
$db->setQuery($query);
$listingcount_noclosed = $db->loadObject()->count;
?>
<style>
body {
	background: #fff;
}
span.price {
	margin-bottom:15px !important;
}
.list-style .items-inner-container {
	width: 40%;
}
</style>
<div class="wrapper">
	<div class="wide left pops">
		<!-- start wide-content -->
		<input type="hidden" id="countbuyer" value="<?php echo $this->user_info->countbuyer ?>" onclick="removeBuyerButton()">
		<input type="hidden" id="usertype" value="<?php echo $this->user_info->user_type ?>">
		<h1>
			<?php if (JFactory::getUser()->id == $uid) {
				echo JText::_('COM_POPS_VIEW_MY');		
			} else {
				$arr_name = explode(" ", $current_user->name);
				$first_name = array_shift($arr_name);
				$fname_length = strlen($first_name);
				$r2 ="<a class='popsviewtitle' href='".JRoute::_("index.php?option=com_userprofile&task=profile&uid=".$uid)."'>".stripslashes($first_name)."</a>";
				echo JText::sprintf('COM_POPS_VIEW_R2VIEW', $r2);
			} ?>
				<!--$arr_name = explode(" ", $current_user->name);
				$first_name = array_shift($arr_name);
				$fname_length = strlen($first_name);
				$suffix = (strtoupper(substr($firstName, $fname_length-1, 1)) == 'S') ? "'" : "'s";
				echo (JFactory::getUser()->id == $uid) ? "".JText::_('COM_POPS_VIEW_MY')." " : " $first_name ". $suffix;-->
		
		</h1>
		<?php if(count($this->listing)) {?>
			<div class="items-filter">
				<!-- start list-tile filter -->
				<div id="changeview"><a class="tile-view left view bg-bottom"></a> <a class="list-view left view bg-bottom"></a>
                </div>
				<span class="tile-number"><?php echo (JFactory::getUser()->id == $uid) ? "".JText::_('COM_POPS_TERMS_YOUHAVEACTIVEPOPS')." " : stripslashes($first_name) ." ".JText::_('COM_POPS_TERMS_HAS').""; ?> <?php echo ($listingcount_noclosed == 0) ?  "".JText::_('COM_POPS_TERMS_NOACTIVEPOPS')."": $listingcount_noclosed; ?>
					<?php echo JText::_('COM_POPS_TERMS_ACTIVEPOPS') ?>
				</span> <a
					href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>"
					class="button gradient-green addpopsbtn" style="padding-top:8px">+ <?php echo JText::_('COM_USERPROF_PROF_ADDP') ?></a>
				<div class="c200 right">
					<form>
						<select id="change_prop_type" name="change_property" style="width:180px">
							<option value="0"><?php echo JText::_('COM_POPS_VIEW_ALL') ?></option>
							<?php
							foreach($this->ptype as $ptype){
										if($ptype->type_id==$this->property_type) $selected = "selected='selected'";
										else $selected = "";
										echo "<option value=\"".$ptype->type_id."\" ". $selected .">".JText::_($ptype->type_name)."</option>";
									}
									?>
						</select>
					</form>
				</div>
				<div class="clear-float"></div>
			</div>
			<!-- end list-tile filter -->
			<input type="hidden" class="image-holder" />
			<div class="items-container grid-style">
				<!-- start items -->
				<ul class="items">
					<?php
					if(count($this->listing)):
					foreach($this->listing as $listing):
					//$model = &$this->getModel('PropertyListing');
						
					$flagicon="";
					if($this->user_info->countries_id!=$listing->country){
						if($listing->country == 0 && $this->user_info->countries_id!=223){
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
						} elseif($listing->country != 0) {
							$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($listing->countries_iso_code_2).'-flag-lang.png">';
						}								
					}
					$images = $this->images[$listing->lid];
					$images_bw = $this->images_bw[$listing->lid];
					$tempview = $this->tempview[$listing->lid];
					$listing->views = $tempview[0]->views;
					$arr_request = explode(",", $listing->request);
					$arr_allowed = explode(",", $listing->allowed);
					$arr_declined = explode(",", $listing->declined);
					$image_bw = false;
					if(JFactory::getUser()->id == $listing->user_id) {
						//viewer is the same as the owner of the listing
						//viewer is different, but the owner of the listing is in the network of the viewer
						//viewer is different, not in the network but allowed to view 


						$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $listing->lid);
						$description = stripslashes_all($listing->property_name)."<br/>";
						#$price = format_currency_global($listing->price1) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2) : "");
						$_price = (JFactory::getUser()->id == $listing->user_id || $listing->disclose == 1) ? format_currency_global($listing->price1,$listing->symbol,$listing->currency) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2,$listing->symbol,$listing->currency) : "") : JText::_('COM_POPS_TERMS_PRICEUNDISC');
						$ts1 = strtotime(date('Y-m-d'));
						$ts2 = strtotime($listing->date_expired);
						$seconds_diff = $ts2 - $ts1;
						$days = floor($seconds_diff/3600/24);
						 if(!isset($_GET['uid'])){ 
						 	$expired_text=' <a style="margin-right: 0px;"
								id="item' . $listing->listing_id . '"
								class="text-link expiry"
								href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
								onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
							</a>';
						 } else {
						 	$expired_text=(($days<1) ? "" : $days);
						 }
						$price = '
						<span class="price '.$listing->currency.'" style="padding-right:0px; margin-bottom:5px">
							' . $_price . ' 
						</span>
						<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">
							<a href="javascript:void(0)" onclick="sold('.$listing->listing_id.')" class="left">' .(($listing->property_type==1 || $listing->property_type==3 ) ? "".JText::_('COM_POPS_TERMS_MARKS')."": "".JText::_('COM_POPS_TERMS_MARKL')."").'
							</a> <span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "").$expired_text.(($days<1) ? "" .JText::_('COM_POPS_TERMS_EXPIRED')."" : "" .JText::_('COM_POPS_TERMS_DAYS')."").'
							</span>
						</div>
						';
					} else {
						#print_r($this->network);
						#if(in_array(JFactory::getUser()->id, $this->network)) {
							if($listing->setting == 1) {
								if($listing->newallow == 1) {
									$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($listing->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);
											//viewer is the same as the owner of the listing
											//viewer is different, but the owner of the listing is in the network of the viewer
											//viewer is different, not in the network but allowed to view 
											$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $listing->lid);
											$description = stripslashes_all($listing->property_name)."<br/>";
											#$price = format_currency_global($listing->price1) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2) : "");
											$_price = (JFactory::getUser()->id == $listing->user_id || $listing->disclose == 1) ? format_currency_global($listing->price1,$listing->symbol,$listing->currency) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2,$listing->symbol,$listing->currency) : "") : JText::_('COM_POPS_TERMS_PRICEUNDISC');
											 if(!isset($_GET['uid'])){ 
												$expired_text=' <a style="margin-right: 0px;"
													id="item' . $listing->listing_id . '"
													class="text-link expiry"
													href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
													onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
												</a>';
											 } else {
												$expired_text=(($days<1) ? "" : $days);
											 }
											$price = '
											<span class="price" style="padding-right:0px; margin-bottom:5px">
												' . $_price . '
											</span>
									<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">
									<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text.(($days<1) ? "" .JText::_('COM_POPS_TERMS_EXPIRED')."" : " " .JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
											';
								} else {
									$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($listing->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);
											if(!in_array(JFactory::getUser()->id, $arr_allowed)) {
												if(in_array(JFactory::getUser()->id, $arr_request)) {
													$image_bw = true;
													$link = "javascript:void(0);";
													$description = "Your request to view the listing is pending";
													$price = '
														<span class="price" style="padding-right:0px; margin-bottom:5px">
															<div class="pops-req-pending">' . JText::_('COM_NETWORK_TERMS_PEND') . '</div>
														</span>
									<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
												} else {
									$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($listing->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);
													$image_bw = true;
													$link = "javascript:void(0);";
													//$description = "This POPs&#8482; is private and only viewable by the Owner.";
													$description = JFactory::getUser($listing->user_id)->name." ".JText::_('COM_NETWORK_TERMS_PRIV2')."";
													$price = '
														<span class="price" style="padding-right:0px; margin-bottom:5px">
															<a id="bigbutton" onclick="requestAccess(' . $listing->user_id . ', ' . $listing->listing_id . ');" href="javascript:void(0)" style="width:200px;margin: 6px auto; display:block; font-size:12px; padding-top:7px" class="button gradient-blue ra' . $listing->user_id . $listing->listing_id . '">'.JText::_('COM_NETWORK_TERMS_REQ2VIEW').'</a> <img height="26px" class="loader loading' . $listing->user_id . $listing->listing_id . '" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question1" style="margin:6px auto;display:none;" />
														</span>
									<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
												}
											} else {
												if(in_array(JFactory::getUser()->id, $arr_allowed)) {
													//viewer is the same as the owner of the listing
													//viewer is different, but the owner of the listing is in the network of the viewer
													//viewer is different, not in the network but allowed to view 
													$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $listing->lid);
													$description = stripslashes_all($listing->property_name)."<br/>";
													#$price = format_currency_global($listing->price1) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2) : "");
													$_price = (JFactory::getUser()->id == $listing->user_id || $listing->disclose == 1) ? format_currency_global($listing->price1,$listing->symbol,$listing->currency) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2,$listing->symbol,$listing->currency) : "") : JText::_('COM_POPS_TERMS_PRICEUNDISC');
									$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($listing->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);
													$price = '
													<span class="price" style="padding-right:0px; margin-bottom:5px">
														' . $_price . '
													</span>
									<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
												} else if(in_array(JFactory::getUser()->id, $arr_declined)) {
									$ts1 = strtotime(date('Y-m-d'));
									$ts2 = strtotime($listing->date_expired);
									$seconds_diff = $ts2 - $ts1;
									$days = floor($seconds_diff/3600/24);
													echo $images_bw;
													$image_bw = true;
													$link = "javascript:void(0);";
													$description = "Your request to view has been denied";
													$price = '
														<span class="price" style="padding-right:0px; margin-bottom:5px">
															<a id="bigbutton" onclick="requestAccess(' . $listing->user_id . ', ' . $listing->listing_id . ');" href="javascript:void(0)" class="button gradient-blue ra" style="width:200px;margin: 6px auto; display:block; font-size:12px; padding-top:7px" class="button gradient-blue ra' . $listing->user_id . $listing->listing_id . '">'.JText::_('COM_NETWORK_TERMS_REQ2VIEW').'</a> <img height="26px" class="loader loading' . $listing->user_id . $listing->listing_id . '" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question" style="margin:6px auto;display:none;" />
														</span>
									<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
												}
											}
								}
		
							} else if($listing->setting == 2) {
						$ts1 = strtotime(date('Y-m-d'));
						$ts2 = strtotime($listing->date_expired);
						$seconds_diff = $ts2 - $ts1;
						$days = floor($seconds_diff/3600/24);
								if(!in_array(JFactory::getUser()->id, $arr_allowed)) {
									if(in_array(JFactory::getUser()->id, $arr_request)) {
										$image_bw = true;
										$link = "javascript:void(0);";
										$description = "Your request to view the listing is pending";
										$price = '
											<span class="price" style="padding-right:0px; margin-bottom:5px">
												<div class="pops-req-pending">' . JText::_('COM_NETWORK_TERMS_PEND') . '</div>
											</span>
						<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
									} else {
						$ts1 = strtotime(date('Y-m-d'));
						$ts2 = strtotime($listing->date_expired);
						$seconds_diff = $ts2 - $ts1;
						$days = floor($seconds_diff/3600/24);
										$image_bw = true;
										$link = "javascript:void(0);";
										//$description = "This POPs&#8482; is private and only viewable by the Owner.";
										$description = JFactory::getUser($listing->user_id)->name." ".JText::_('COM_NETWORK_TERMS_PRIV2')."";
										$price = '
											<span class="price" style="padding-right:0px; margin-bottom:5px">
												<a id="bigbutton" onclick="requestAccess(' . $listing->user_id . ', ' . $listing->listing_id . ');" href="javascript:void(0)" style="width:200px;margin: 6px auto; display:block; font-size:12px; padding-top:7px" class="button gradient-blue ra' . $listing->user_id . $listing->listing_id . '">'.JText::_('COM_NETWORK_TERMS_REQ2VIEW').'</a> <img height="26px" class="loader loading' . $listing->user_id . $listing->listing_id . '" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question" style="margin:6px auto;display:none;" />
											</span>
						<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
									}
								} else {
									if(in_array(JFactory::getUser()->id, $arr_allowed)) {
										//viewer is the same as the owner of the listing
										//viewer is different, but the owner of the listing is in the network of the viewer
										//viewer is different, not in the network but allowed to view 
										$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID=' . $listing->lid);
										$description = stripslashes_all($listing->property_name)."<br/>";
										#$price = format_currency_global($listing->price1) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2) : "");
										$_price = (JFactory::getUser()->id == $listing->user_id || $listing->disclose == 1) ? format_currency_global($listing->price1,$listing->symbol,$listing->currency) . (!empty($listing->price2) ? " - " . format_currency_global($listing->price2,$listing->symbol,$listing->currency) : "") : JText::_('COM_POPS_TERMS_PRICEUNDISC');
						$ts1 = strtotime(date('Y-m-d'));
						$ts2 = strtotime($listing->date_expired);
						$seconds_diff = $ts2 - $ts1;
						$days = floor($seconds_diff/3600/24);
										$price = '
										<span class="price" style="padding-right:0px; margin-bottom:5px">
											' . $_price . ' '.$listing->currency.'
										</span>
						<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
									} else if(in_array(JFactory::getUser()->id, $arr_declined)) {
						$ts1 = strtotime(date('Y-m-d'));
						$ts2 = strtotime($listing->date_expired);
						$seconds_diff = $ts2 - $ts1;
						$days = floor($seconds_diff/3600/24);
										echo $images_bw;
										$image_bw = true;
										$link = "javascript:void(0);";
										$description = "Your request to view has been denied";
										$price = '
											<span class="price" style="padding-right:0px; margin-bottom:5px">
												<a id="bigbutton" onclick="requestAccess(' . $listing->user_id . ', ' . $listing->listing_id . ');" href="javascript:void(0)" class="button gradient-blue ra" style="width:200px; margin: 6px auto; display:block; font-size:12px; padding-top:7px" class="button gradient-blue ra' . $listing->user_id . $listing->listing_id . '">'.JText::_('COM_NETWORK_TERMS_REQ2VIEW').'</a> <img height="26px" class="loader loading' . $listing->user_id . $listing->listing_id . '" alt="Loading..." src="https://www.agentbridge.com/images/ajax_loader.gif" id="loading-image_custom_question" style="margin:6px auto;display:none;" />
											</span>
						<div class="'.$listing->listing_id.' seen-expire" id="'.$listing->property_type.'" style="padding-left:5px">';

									if(!isset($_GET['uid'])){ 
									 	$expired_text=' <a style="margin-right: 0px;"
											id="item' . $listing->listing_id . '"
											class="text-link expiry"
											href="' . 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '#edittext"
											onclick="update(' . $listing->listing_id . ')"> ' .(($days<1) ? "" : $days). '
										</a>';
									 } else {
									 	$expired_text=(($days<1) ? "" : $days);
									 }
									 $price.='<span class="expired_span">'.(($days<1) ? "" : "".JText::_('COM_POPS_TERMS_EXPIRESIN'). "")." ".$expired_text." ".(($days<1) ? "".JText::_('COM_POPS_TERMS_EXPIRED')."" : "".JText::_('COM_POPS_TERMS_DAYS')."").'
										</span>
									</div>
													';
									}
								}
							}
						#}
					}
					?>
					<li id="item_<?php echo $listing->listing_id; ?>" class="prop_types prop_type_id<?php echo $listing->property_type; ?>">
                        <div class="clear-float"></div>	
						<div class="items-inner-container">
							<div class="image-placeholder">	
								<a href="<?php echo $link; ?>" class="item-number text-link">
									<?php if($image_bw) { ?>
										<img  src="<?php echo "images/buyers/212x172/" . $images_bw; ?>?true" border="0" style="display:block; margin-left:auto; margin-right:auto" />
									<?php } else { ?>							
										<img  src="<?php echo (is_object($images[0])) ? trim($images[0]->image) : "images/buyers/212x172/" . $images; ?>?true" border="0"  style="width:212px; display:block; margin-left:auto; margin-right:auto" />
									<?php } ?>
								</a>
							</div>
							<div class="description-placeholder">
								 <?php if($listing->sold==1){?>
		                        	 <script>
		                        	 jQuery(document).ready(function(){
		                        	 	<?php if ($listing->property_type==1 || $listing->property_type==3) {?>
		                        	 		jQuery("#item_<?php echo $listing->listing_id;?> .seen-expire a.left").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKS') ?>");
		                        	 	<?php } else {?>
		                        	 		jQuery("#item_<?php echo $listing->listing_id;?> .seen-expire a.left").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKL') ?>");
		                        	 	<?php }?>
										jQuery("#item_<?php echo $listing->listing_id;?> .seen-expire a.left").addClass("left-unsold").removeClass("left");
									 });		                        	 	
		                        	 </script>
		                        	 <style>
		                        	 #item_<?php echo $listing->listing_id;?> .seen-expire a.left{
		                        	 		display: none;
		                        	 	}
		                        	 #item_<?php echo $listing->listing_id;?> .expired_span{
		                        	 		display: none;
		                        	 	}
		                        	 </style>
		                        	<div class="folded"><h2>
										<?php if ($listing->property_type==1 || $listing->property_type==3) {?>
												<?php echo JText::_('COM_POPS_TERMS_SOLD') ?>
										<?php } else {?> <?php echo JText::_('COM_POPS_TERMS_LEASED') ?><?php }?></h2></div>
		                        <?php }?>	
								<?php 
									if(isset($_GET['uid'])){ 
								?>
								<?php if($listing->setting == 2 && !in_array(JFactory::getUser()->id, $arr_allowed)) { ?>
								
								<p class="itemnumber private-black bottom-margin"><?php echo $flagicon." ".$listing->zip ?></p>
								<?php } else { ?>
								<a
									href="<?php echo $link."&uid=".$_GET['uid']; ?>"
									class="item-number text-link"><?php echo $flagicon." ".$listing->zip ?> </a>
								<?php } ?>
                                <p class="reality-description">
                                <?php echo $description; ?>
                                <?php if(($listing->setting == 1 && $listing->newallow == 1) || $listing->newallow !== 1 && in_array(JFactory::getUser()->id, $arr_allowed)) {
                                	echo $listing->pops_feats['initial_feats'];
								?>
                                    </p>
								<?php } }
								else { ?>
								<a href="<?php echo $link; ?>" class="item-number text-link"><?php echo $flagicon." ".$listing->zip ?> </a>								
								<p class="reality-description">
									<?php echo $description; ?>
                                <?php 	
                                	echo $listing->pops_feats['initial_feats'];								
									?>
                                    </p>
                              <?php }  ?>
							</div>								
						</div>
						<div class="other-description-placeholder">
							<?php echo $price; ?>
						</div>
					</li>
					<?php
					endforeach;
					else:
					?>
					<li>
						<p style="text-align: center">
							<br />No Listing Found!<br /> <br /> <br /> <br /> <br />
						</p>
					</li>
					<?php
					endif;
					?>
				</ul>
			</div>
		<!-- end items -->
			<div class="pagination right">
			</div>
		</div>
	<?php }else { ?>
			<?php if(!count($this->listing)) { ?>
				<div class="whatispops"><?php echo JText::_('COM_POPS_VIEW_WHATIS') ?></div>
				<div class="answer"><?php echo JText::_('COM_POPS_VIEW_POPSDEF') ?></div>
				<div class="nopopsnewuser">
					<a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>" class="button gradient-green left">+ <?php echo JText::_('COM_USERPROF_PROF_ADDP') ?></a>
					<a href="#" id="concierge"><?php echo JText::_('COM_POPS_VIEW_CONCIERGE') ?></a>
				</div>
			<?php } else { ?>
				<div id="nopops" class="right" style="margin: 10px auto">
					<a id="addbuyer" style="padding-top:8px; margin-right:10px; display:none" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile'); ?>" class="button gradient-green right"> <span class="create-referral"><?php echo JText::_('COM_USERPROF_PROF_ADDB') ?></span></a> 
					<a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>" class="button gradient-green right">+ <?php echo JText::_('COM_USERPROF_PROF_ADDP') ?></a>
					<a style="padding-top:8px; margin-right:10px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newreferral')?>" class="button gradient-green right"><em class="refer-me-text" style="padding-left:20px; margin-left:0px; font-size:12px"><?php echo JText::_('COM_USERPROF_PROF_CREATER') ?></em></a>
				</div>
				<div id="addbuyerdiv" onclick="showbuyerdiv()" class="clear-float premium-indicatior" style="background-color:#EEEEEE; display:none;width:auto;height:40px;border:1px solid #DEDEDE">
					<div class="left" style="font-size:12px;padding:12px">
						<?php echo JText::_('COM_USERPROF_PROF_ADDBT') ?>
					</div>
					<div class="right" style="padding:4px">
						<div class="profile-btn right" style="width:250px"><a style="padding-top:8px" href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=newbuyerprofile') ?>" class='button gradient-green left'> <span class='create-referral'><?php echo JText::_('COM_USERPROF_PROF_ADDB') ?></span></a> 
					   <span class="left text-link cursor-point" style="padding:8px"  id="hideaddbuyerdiv"  onclick="hidebuyerdiv()"><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
					</div>
				</div>
			<?php } ?>
			<div style='clear:both'></div>
			<div id='invitediv' class='premium-indicatior' style='display:none;background-color:#EEEEEE;width:auto;height:40px;border:1px solid #DEDEDE'>
			<div class='left' style='font-size:12px;padding:12px'>
			<?php echo JText::_('COM_ACTIVITY_SPONSOR'); ?>
			</div>
			<div class='right' style='padding:4px'>
				<div class='profile-btn right' style='width:250px'><a class='button gradient-green left' href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile#invitesection') ?>">&nbsp;  &nbsp; <span class='create-referral'>  <?php echo JText::_('COM_USERPROF_PROF_INV')?> </span></a>
			    <span class='left text-link cursor-point' style='padding:8px'  id='not-now' onclick='hideinvitediv()'><?php echo JText::_('COM_USERPROF_PROF_NOTNOW') ?></span></div>
			</div>
			</div>
			<div style='clear:both'></div>
	<?php } ?>
	<div id="edittext" style="display: none">
			  <span><?php echo JText::_('COM_POPS_TERMS_SELEXP');?></span><br /> 
             <div style="width: 220px">
             <input style="margin-top:10px; height:30px" type="text" id="userinput" class="textbox-up left" style="width:180px" /> 
             <input type="hidden" id="editid"/> 
             <input class="button gradient-green right" style="margin-top:10px; padding-top:7px; height:30px; width:80px" type="button" value="Submit" id="changevalue"/>
            </div>

            <div id="expmsg" class="error_msg" style="display:none; padding-top:50px"><?php echo JText::_('COM_POPS_TERMS_SUCCESSEXP');?></div>
			<div id="experr" class="error_msg" style="display:none; padding-top:50px"><?php echo JText::_('COM_POPS_TERMS_UNSUCCESSEXP');?></div>
		</div>
	</div>
	<!-- end wide-content -->
	<?php
	include( 'includes/sidebar_left.php' );
	?>
</div>
<div id="concierge_dialog" style="display:none;padding-bottom:0;padding-top:10px;">
	<p style="padding-left:10px;margin-bottom:10px;">
		<?php echo JText::_('COM_POPS_VIEW_CONCIERGE_CONTACT');?><br />
	</p>
	<p style="padding-left:10px;margin-bottom:10px;"> 

		<?php echo JText::_('COM_POPS_VIEW_CONCIERGE_CONTACT2');?> <?php echo $this->user_info->email; ?> <?php echo JText::_('COM_POPS_VIEW_CONCIERGE_AND');?><?php echo $this->user_info->newcontact; ?>.
	</p>
	<input type="button" class="button gradient-blue" value="<?php echo JText::_('COM_NRDS_FORM_DONE')?>" id="closedialog" style="float:right;width:70px;line-height:12px;margin:0 10px 0 0;" />
</div>
<script>
mixpanel.track("My POPs");

function sold (id) {
	jQuery('.id seen-expire').hide();
}
	function getInvitationCount(){
		jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_userprofile&task=get_invitation_list&format=raw",
			    type: "POST",
			    success: function(data){
			    	var obj = jQuery.parseJSON(data);
			    	var count_inv = 10-obj.length;
			    	if(count_inv>0)
			    		jQuery(".counter").text(count_inv);
			    	else{
			    		var parent = jQuery(".counter").parent();
			    		parent.text("You have invited 10 licensed real estate professionals to join us on AgentBridge.");
			    		parent.next().find(".profile-btn").css("width","200px");
			    		parent.next().find(".create-referral").html("Check Invites &nbsp; &nbsp;");
			    		parent.next().find("#not-now").hide();
			    	}
			    }
		});
	}
	getInvitationCount();
function showbuyerdiv(){
		var buyercount = jQuery( "#countbuyer" ).val();
		if (buyercount == 0)
		jQuery("#addbuyerdiv").show();		
	}
	function hidebuyerdiv(){
		var utype = jQuery( "#usertype" ).val();
		jQuery("#addbuyerdiv").remove();
		jQuery( "#addbuyer" ).show();
		if (utype == 1 || utype == 4 )
		 jQuery("#invitediv").show();	
	}
	function hideinvitediv(){
		//jQuery("#addbuyerdiv").remove();
		jQuery("#invite-button" ).show();
		jQuery("#invitediv").remove();	
	}
	function removeBuyerButton(){
		var buyercount = jQuery( "#countbuyer" ).val();
		var utype = jQuery( "#usertype" ).val();
		if (buyercount >= 1 && utype==3)
			jQuery("#addbuyer").show();
		if (buyercount >= 1 && utype!=3) {
			jQuery( "#addbuyer" ).show();
			jQuery("#invitediv").show();
		}
	}
function update(lid){
	jQuery( "#editid" ).val(lid);
	jQuery( "#edittext" ).dialog(
		{
			modal: true,
			title: "Change Expiration Date",
			});
}
function print(data,list){
	var html="";
	var items;
	items = jQuery.parseJSON(data);
	if(items.length==0){
		jQuery("div.items-container").html("<br/>no results found");
	}
	else{
		jQuery("div.items-container").html("<ul class='items'></ul>");
	}
	jQuery(items).each(function(data, listing){
		console.log(listing.images);
		html+="<li>";
		html+=	"<div class=\"items-inner-container\">";
		html+=		"<div class=\"image-placeholder\">";
		html+=			"<a ";
		html+=				"href=\"<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual'); ?>&lID="+listing.listing_id+"\" ";
		html+=				"class=\"item-number text-link\"> <img width=\"100px\" ";
		html+=				"src=\""+listing.images.split(',')[0]+"?true\" border=\"0\" />";
		html+=			"</a>";
		html+=		"</div>";
		html+=		"<div class=\"description-placeholder\">";
		html+=			"<a ";
		html+=				"href=\"<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual'); ?>&lID="+listing.listing_id+"\" ";
		html+=				"class=\"item-number text-link\">"+listing.zip+"</a>";
		html+=			"<p class=\"reality-description\">";
		html+=				listing.bedroom;
		html+=				(listing.bedroom)?" Bedrooms, ":"";
		html+=				listing.bathroom;
		html+=				(listing.bathroom)?" Bathrooms ":"";
		html+=				(listing.features1!=undefined) ? listing.features1+" ": "";
		html+=				(listing.features2!=undefined) ? listing.features2+" ": "";
		html+=				(listing.features2!=undefined) ? listing.features2+" ": "";
		html+=			"</p>";
		html+=		"</div>";
		html+=	"</div>";
		text = (listing.price2>0) ? (" - $"+listing.price2) : "";
		html+=	"<div class=\"other-description-placeholder\">";
		html+=		"<span class=\"price\">$"+listing.price1+text;
		html+=		"</span>";
		var ishidden = (listing.views<1) ? "hidden" : "";
		html+=		"<div class=\"seen-expire\">";
		html+=			"<a class=\"left "+ishidden+"\">Seen by "+((listing.views>0) ? listing.views : "n/a")+"</a>";
			html+=			"<span class=\"expired_span\">Expires in <a style='margin-right:0px;'";			
			html+=			"id=\"item"+listing.listing_id+"\" ";
			html+=			"class=\"text-link expiry\" ";
			html+=			"href=\"<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#edittext\" ";
			html+=			"title=\"click to edit\" ";
			html+=			"onclick=\"update("+listing.listing_id+")\">"+(((listing.expiry)>0) ? listing.expiry : "90");
			html+=			" days ";
			html+=			"</a>";
			html+=			"</span>";
		html+=		"</div>";
		html+=	"</div>";
	});
	//jQuery(".items").html(html).promise().done(function(){
		//Landing.format();
	//});
}
jQuery("#changevalue").click(function(){
		var lid = jQuery( "#editid" ).val();
		var userinput = jQuery( "#userinput" ).val();
		jQuery.post("<?php echo JRoute::_('index.php?option=com_userprofile&task=updatelisting'); ?>&id="+lid+"&value="+userinput,
				function(data){
					var result = jQuery.parseJSON(data);
					if(result.code == 1){
					jQuery( "#item"+lid ).html(result.days);
					jQuery( "#expmsg" ).show();
					jQuery( "#experr" ).hide();
					}
					else
					jQuery( "#experr" ).show();
				});
	});
var list=true;
var itemsLoaded = false;
var items;
jQuery("#changeview a").click(function(){
	list=!list;
	console.log(list);
	jQuery("div.items-filter a").toggleClass('bg-bottom');
	jQuery("div.items-container").toggleClass('list-style');
	jQuery("div.items-container").toggleClass('grid-style');
	jQuery("div.image-placeholder a.item-number.text-link img").each(function(){
		 var src_img = jQuery(this).attr('src');                  
		if(list==false){
			jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("position","relative");
			jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("overflow","visible");
			//jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").find("div.folded h2").css({ "text-align":"center", "padding" : "4px 4px 4px 3px", "right" : "297px", "left" : "69px", "top" : "3px", "font-size" : "9px" });
			jQuery(this).attr('src', src_img.replace('212x172','100x81'));
			}
		else {
			jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("position","static");
			jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("overflow","hidden");
			//jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").find("div.folded h2").css({ "padding" : "4px 32px", "right" : "-7px", "left" : "124px", "top" : "12px", "font-size" : "17px" });
			jQuery(this).attr('src', src_img.replace( '100x81','212x172'));
		}
		var parent = jQuery(this).parent().parent();
		if(jQuery(this).width()>jQuery(this).height()){
			if(jQuery(this).width()/jQuery(this).height() <= 2.0){
				jQuery(this).css('width',parent.width());
				jQuery(this).css('margin-top', -(jQuery(this).height()-parent.height())/2);
				jQuery(this).css('margin-left', '0px');
			}
			//jQuery(this).css('height',parent.height());
			jQuery(this).css('margin-left', -(jQuery(this).width()-parent.width())/2);
			jQuery(this).css('margin-top', '0px');
		}
		else{
			jQuery(this).css('width',parent.width());
			jQuery(this).css('margin-top', -(jQuery(this).height()-parent.height())/2);
			jQuery(this).css('margin-left', '0px');
		}
	});
});
 function get_default_image(property_type, sub_type){
		jQuery.ajax({
			type: "POST",
			data: {property_type: property_type, sub_type: sub_type },
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=get_images'); ?>",
			async: false, 
			cache: false,
			success: function ( jqXHR ) {
				jQuery('.image-holder').val(jqXHR);
			}
		});
 }
 function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
jQuery("select[name=\"property_type\"]").change(function(){
	itemsLoaded=false;
	var ctr=0;
	var html="";
	jQuery.ajax({
		type: "POST",
		data: {'userid':<?php echo JFactory::getUser()->id?>, 'type': jQuery(this).val() },
		url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=get_listing'); ?>",
		dataType: "JSON",
		async: false, 
		cache: false,
		success: function ( jqXHR ) {
			jQuery.each( jqXHR, function(index, element) {
				ctr++;
				if(ctr==0){
					jQuery("div.items-container").html("<br/>no results found");
				} else {
					jQuery("div.items-container").html("<ul class='items'></ul>");
				}
					var image = "";
					var img_style="";
					if(jQuery(".items-container").hasClass("grid-style")){
						img_style='style="width: 212px; display: block; margin-left: 0px; margin-right: auto; margin-top: 0px"';
					} else {
						img_style='style="width: 100px; display: block; margin-left: 0px; margin-right: auto; margin-top: 0px"';
					}
					if ( element.images!=undefined ){
						image = element.images
						image = image.split(","); 
						var return_image = "src='"+image[0]+"?true' border='0' "+img_style+"/>";
					} else {				
						get_default_image(element.property_type, element.sub_type);
						var image = jQuery('.image-holder').val();
						image = image.substring(1, image.length-1);
						image = image.replace('"',"");
						var return_image = "src='<?php echo $this->baseurl; ?>/images/buyers/212x172/"+image+"?true' border='0' "+img_style+"/>";
					}
					var DateDiff = {
						inDays: function(d1, d2) {
							var t2 = d2.getTime();
							var t1 = d1.getTime();
							return parseInt((t2-t1)/(24*3600*1000));
						},
						inWeeks: function(d1, d2) {
							var t2 = d2.getTime();
							var t1 = d1.getTime();
							return parseInt((t2-t1)/(24*3600*1000*7));
						},
						inMonths: function(d1, d2) {
							var d1Y = d1.getFullYear();
							var d2Y = d2.getFullYear();
							var d1M = d1.getMonth();
							var d2M = d2.getMonth();
							return (d2M+12*d2Y)-(d1M+12*d1Y);
						},
						inYears: function(d1, d2) {
							return d2.getFullYear()-d1.getFullYear();
						}
					}
					var dString = element.date_expired;
					var d1 = new Date(dString);
					var d2 = new Date();
					var days = DateDiff.inDays(d2, d1)			
					html+="<li>";
					html+=	"<div class=\"items-inner-container\">";
					html+=		"<div class=\"image-placeholder\">";
					html+=			"<a ";
					html+=				"href=\"<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual'); ?>&lID="+element.listing_id+"\" ";
					html+=				"class=\"item-number text-link\"> <img ";
					html+=				return_image;
					html+=			"</a>";
					html+=		"</div>";
					html+=		"<div class=\"description-placeholder\">";
					html+=			"<a ";
					html+=				"href=\"<?php echo JRoute::_('index.php?option=com_propertylisting&task=individual'); ?>&lID="+element.listing_id+"\" ";
					html+=				"class=\"item-number text-link\">"+element.zip+"</a>";
					html+=			"<p class=\"reality-description\">";
					html+=			(element.alt_property_name!=undefined) ? element.alt_property_name+" ": "";
					html+=	"<br/>";
					html+=			(element.type_name!=undefined) ? element.type_name+" ": "";
					html+=	" - ";
					html+=			(element.name!=undefined) ? element.name+" ": "";
					html+=			"</p>";
					html+=		"</div>";
					html+=	"</div>";
					text = (element.price2>0) ? (" - $"+commaSeparateNumber(element.price2)) : "";
					html+=	"<div class=\"other-description-placeholder\">";
					html+=		"<span class=\"price\">$"+commaSeparateNumber(element.price1)+text;
					html+=		"</span>";
					var ishidden = (element.views<1) ? "hidden" : "";
					html+=		"<div class=\"seen-expire\">";
					html+=			"<a class=\"left "+ishidden+"\">Seen by "+((element.views>0) ? element.views : "n/a")+"</a>";
					html+=			"<span class=\"expired_span\">"+((days<1) ? "" : "<?php echo JText::_('COM_POPS_TERMS_EXPIRESIN')?>")+"<a style='margin-right:0px;'";
					html+=			"id=\"item"+element.alt_listing_id+"\" ";
					html+=			"class=\"text-link expiry\" ";
					html+=			"href=\"<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>#edittext\" ";
					html+=			"title=\"click to edit\" ";
					html+=			"onclick=\"update("+element.alt_listing_id+")\">"+((days<1) ? "" : days);
					html+=			"</a>";
					html+= 			((days<1) ? "<?php echo JText::_('COM_POPS_TERMS_EXPIRED')?>" : "<?php echo JText::_('COM_POPS_TERMS_DAYS')?>");
					html+=			"</span>";
					html+=		"</div>";
					html+=	"</div>";
				jQuery(".items").html(html).promise().done(function(){
					Landing.format();
				});
			});
		},
		error: function() {
		}
	});	
	if(ctr==0) {
		jQuery("div.items-container").html("<br/>no results found");
	}	
});
jQuery(document).ready(function(){
	jQuery("#userinput").datepicker();
	jQuery("#countbuyer").trigger("click");
	jQuery("#addbuyerdiv").trigger("click");
	jQuery("#change_prop_type").change(function(){
		var id = jQuery(this).val();
		if(id==0){
			jQuery('.prop_types').show();
		} else {
			jQuery('.prop_type_id' + id).show();
			jQuery('.prop_types').not('.prop_type_id' + id).hide();
		}

		jQuery("div.image-placeholder a.item-number.text-link img").each(function(){
			 var src_img = jQuery(this).attr('src');                  
			if(list==false){
				jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("position","relative");
				jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("overflow","visible");
				//jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").find("div.folded h2").css({ "text-align":"center", "padding" : "4px 4px 4px 3px", "right" : "297px", "left" : "69px", "top" : "3px", "font-size" : "9px" });
				jQuery(this).attr('src', src_img.replace('212x172','100x81'));
				}
			else {
				jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("position","static");
				jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").css("overflow","hidden");
				//jQuery(this).closest( ".image-placeholder").siblings( ".description-placeholder").find("div.folded h2").css({ "padding" : "4px 32px", "right" : "-7px", "left" : "124px", "top" : "12px", "font-size" : "17px" });
				jQuery(this).attr('src', src_img.replace( '100x81','212x172'));
			}
			var parent = jQuery(this).parent().parent();
			if(jQuery(this).width()>jQuery(this).height()){
				if(jQuery(this).width()/jQuery(this).height() <= 2.0){
					jQuery(this).css('width',parent.width());
					jQuery(this).css('margin-top', -(jQuery(this).height()-parent.height())/2);
					jQuery(this).css('margin-left', '0px');
				}
				//jQuery(this).css('height',parent.height());
				jQuery(this).css('margin-left', -(jQuery(this).width()-parent.width())/2);
				jQuery(this).css('margin-top', '0px');
			}
			else{
				jQuery(this).css('width',parent.width());
				jQuery(this).css('margin-top', -(jQuery(this).height()-parent.height())/2);
				jQuery(this).css('margin-left', '0px');
			}
		});
	});
	jQuery(".seen-expire a.left").live("click", function(){
		var this_class=jQuery(this).parent().attr("class").split(" ")[0];
		var this_id = jQuery(this).parent().attr("id");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=soldPOPs&format=raw') ?>",
			data: {'listing_id': this_class},
			success: function(data){
				if(jQuery("#changeview a").hasClass("bg-bottom")){
					if(this_id==1 || this_id==3){
						jQuery("#item_"+this_class+" .description-placeholder").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_SOLD') ?></h2></div>');
					} else {
						jQuery("#item_"+this_class+" .description-placeholder").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_LEASED') ?></h2></div>');
					}					
				} else {
					if(this_id==1 || this_id==3){
						jQuery("#item_"+this_class+" .description-placeholder").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_SOLD') ?></h2></div>');
					} else {
						jQuery("#item_"+this_class+" .description-placeholder").append('<div class="folded"><h2><?php echo JText::_('COM_POPS_TERMS_LEASED') ?></h2></div>');
					}
					jQuery("#item_"+this_class+" .description-placeholder").find("div.folded h2").css({ "padding" : "4px 8px", "right" : "297px", "left" : "69px", "top" : "3px", "font-size" : "9px" });
				}
				if(this_id==1 || this_id==3){
					jQuery("#item_"+this_class+" .seen-expire a.left").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKS') ?>");
				} else {
					jQuery("#item_"+this_class+" .seen-expire a.left").html("<?php echo JText::_('COM_POPS_TERMS_UNMARKL') ?>");
				}
				jQuery("#item_"+this_class+" .seen-expire a.left").addClass("left-unsold").removeClass("left");
				jQuery("#item_"+this_class+" .expired_span").hide();
				//location.reload();
			}
		});
	});
	jQuery(".seen-expire a.left-unsold").live("click", function(){
		var this_class=jQuery(this).parent().attr("class").split(" ")[0];
		var this_id = jQuery(this).parent().attr("id");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=unSoldPOPs&format=raw') ?>",
			data: {'listing_id': this_class},
			success: function(data){
				jQuery("#item_"+this_class+" .description-placeholder div.folded").remove();
					if(this_id==1 || this_id==3){
						jQuery("#item_"+this_class+" .seen-expire a.left-unsold").html("<?php echo JText::_('COM_POPS_TERMS_MARKS') ?> <br>");
					} else {
						jQuery("#item_"+this_class+" .seen-expire a.left-unsold").html("<?php echo JText::_('COM_POPS_TERMS_MARKL') ?> <br>");
					}				
				jQuery("#item_"+this_class+" .seen-expire a.left-unsold").addClass("left").removeClass("left-unsold");
				jQuery("#item_"+this_class+" .seen-expire a").show();
				jQuery("#item_"+this_class+" .expired_span").show();
				location.reload();
			}
		});
	});
});
function requestAccess(uid, pid){
	//console.log(uid +" "+ pid);
	jQuery(".ra"+uid+""+pid).hide();
	jQuery(".loading"+uid+""+pid).show();
	jQuery(".loading"+uid+""+pid).css("display", "block");
	jQuery.ajax({
		type: "POST",
		url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=requestaccess') ?>',
		data: {'pid': pid, 'uid': uid},
		success: function(data){
			var hid = 1;
			jQuery(".loading"+uid+""+pid).hide();	
			jQuery(".ra"+uid+""+pid).after("<div class='pops-req-pending'><?php echo JText::_('COM_NETWORK_TERMS_PEND');?></div>");
		}
	});
}
var submitted = 0;
jQuery("#concierge").click(function(e){
	e.preventDefault(); 
	if(!submitted) {
		jQuery.ajax({
			type:	"POST",
			url:	"<?php echo JRoute::_('index.php?option=com_userprofile&task=submitTicketSupport') ?>",
			data: {
				"ticket_category"	: 17,
				"ticket_content"	: "Request to Enter POPs. Please get in touch with me.",
				"ticket_priority"	: 4,
				"ticket_type"		: 3
			},
			success: function(){
				submitted = 1;
				jQuery("#concierge_dialog").dialog({
					modal: true,
					title: "<?php echo JText::_('COM_POPS_VIEW_CONCIERGE_ADD');?>",
					width: "550px"
				});
			}
		});
	} else {
		jQuery("#concierge_dialog").dialog({
			modal: true,
			title: "<?php echo JText::_('COM_POPS_VIEW_CONCIERGE_ADD');?>",
			width: "500px"
		});
	}	
});
jQuery("#closedialog").click(function(){
	jQuery("#concierge_dialog").dialog("close");
});
</script>