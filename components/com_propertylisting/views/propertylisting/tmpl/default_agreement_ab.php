<?php
    header('X-Frame-Options: GOFORIT'); 
?>

<?php
$new_date = new DateTime($this->referral->date, new DateTimeZone($this->timezone));

$userTimeZone = new DateTimeZone($this->timezone);
$offset = $userTimeZone->getOffset($new_date);
$date = gmdate($this->d_format, strtotime($this->referral->date)+intval($offset));

$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
$language_tag = JFactory::getUser()->currLanguage;
$language->load($extension, $base_dir, $language_tag, true);

#$timestamp = strtotime($this->referral->date)-25200;
#$date = gmdate($this->d_format, $timestamp);
$image = ($this->referral->agent_a == JFactory::getUser()->id) ? $this->referral->agent_b_info->image : $this->referral->agent_a_info->image;
if($this->referral->agent_b == JFactory::getUser()->id){
	$otherusername = $this->referral->agent_a_info->name;
	$name =  $this->referral->agent_b_info->name;
}
else{
	$name = $this->referral->agent_a_info->name;
	$otherusername =  $this->referral->agent_b_info->name;
}

preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
$os = current($matches);


if($os){
	$notMobile=0;
} else {
	$notMobile=1;
}

?>
<link href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/imageviewer.css" rel='stylesheet' type='text/css'>
<script src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/imageviewer.js" type="text/javascript"></script>
<style type="text/css">

@font-face {
    font-family: alleana;
    src: url("<?php echo $this->baseurl.'/templates/agentbridge/fonts/alleana/alleana.ttf'?>");
}

 .title {
    font-family: Maven Pro,Arial,sans-serif;
    font-weight: 700;
    color: #333;
    font-size: 22px;
    margin-bottom: 17px;
    margin-top: 0;
    padding-right: 12px;
}

        .slider-holder
        {
            width: 100%;
           /* height: 400px;*/
            margin-left: auto;
            margin-right: auto;
            margin-top: 0px;
            text-align: center;
            overflow: hidden;
        }
        
        .image-holder
        {
        	  width: 200%;

            clear: both;
            position: relative;
            
            -webkit-transition: left 2s;
            -moz-transition: left 2s;
            -o-transition: left 2s;
            transition: left 2s;
        }
        
        .slider-image
        {
            float: left;
            margin: 0px;
            padding: 0px;
            position: relative;
            width: 50%;
    		height: auto;

        }


		.positioning{
		   position: absolute;
		    /* background-color: #009bd0; */
		    /* color: white; */
		        font-size: 90%;
		    line-height: 18px;
		    /* font-weight: bold; */
		        padding-top: 0.7%;
		        width: 18.3%;
    /* height: 2.6%;
		    /* border-radius: 5px; */	
		    cursor: pointer;
		        bottom: 26%;
		        z-index: 0;
		}

		.signbutton_r1{
			 left: 15%;
		}

		.signbutton_r2{
			    right: 13%;
		}

		.positioning_sign{
    position: absolute;
    /* line-height: 18px; */
    /* font-weight: bold; */
    /* padding: 10px 15px; */
    font-family: alleana;
    font-size: 53px;
    /* font-style: italic; */
    bottom: 31.6%;
    /* text-shadow: 4px 4px 3px rgba(0,0,0,0.1); */

		}	

		.sign_r1{
			left: 4%;  			
		}

		.sign_r2{
			left: 57.8%;
		}


        .slider-image img
        {
            max-width:100%;
			/*max-height:100%;*/
			max-height:1358px;
        }
                
        .button-holder
        {
            position: relative;
            margin-right: 2%;
   			margin-top: 1%;
        }
        
        .slider-change
        {
            display: inline-block;
          	cursor: pointer;
          	color: #2f99cd;
        }
        .main-refsign{
        	width: 100%;
    		margin: auto;
    		text-align:right;
    		    margin-top: 15px;
        }

        .done-div{
		    text-align: center;
        }

        #done-button{
        	width: 149px;
    		display: inline-block;
		    cursor:pointer;
		   background: url(../images/green-gradient.png) repeat-x #339900;
		    color: white;
    		font-weight: bold;
        }

        .curr-page{
        	color:#000;cursor:default;
        }

        input[type=submit]{
        	background: none;
		    border: 0;
		    color: #ffF;
        }

        .hide_item{
        	display: none;
        }

        .center_item{
        	text-align: center;
        }

        .paginationPDF {
		    display: inline-block;
		}
		.paginationPDF div {
		    color:  #2f99cd;
		    float: left;
		    padding: 8px 8px;
		    text-decoration: none;
		    transition: background-color .3s;
		    
		}
		.paginationPDF div:first-child {
		    border-top-left-radius: 5px;
		    border-bottom-left-radius: 5px;
		}

		.paginationPDF div:last-child {
		    border-top-right-radius: 5px;
		    border-bottom-right-radius: 5px;
		}
		.paginationPDF div.curr-page {
		   
		    color: #000;
		 
		}

		.pdf-instruct{
			font-size: 13px;
		    font-family: Helvetica Neue,Arial,sans-serif;
		    clear: both;
		    color: #333;
		    line-height: 18px;    margin-top: 15px;
		}

		.pdf-instruct span{
			display: block;
		    margin-top: 1%;
		    font-style: italic;
		}

		.wrapper{
			width: 100%;
			max-width: 960px;
		}

		.slider-change-mob {
		    font-size: 12px;
		}

</style>
<div class="wrapper">
		<!-- start wide-content -->

		<h1 style="margin-top:20px"><?php echo JText::_('COM_REFERRAL_MY') ?></h1>
		<div class="clear-float">
			<a href="<?php echo JRoute::_('index.php?option=com_propertylisting&task=referrals'); ?>" class="button gradient-blue left"
				style="margin: 10px 0; color: #fff; padding-top:7px"><?php echo JText::_('COM_REFERRALS_RETURN') ?></a>
		</div>

		<div class="referral_viewer" style="font: 11px";>
			<div class="clear-float">
				<div id="header" style="height: 30px; margin-bottom:10px">
					<div class="names" style="border-right: 1px solid; padding-right: 20px; float:left">
						<span class="name"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->referral->agent_a)?>" class="text-link"><?php echo stripslashes_all($this->referral->agent_a_info->name)?></a></span><br/>
						<span class="l_update"><?php echo JText::_('COM_LASTUPDATED') ?>: </span><span class="l_update"><?php echo $date?></span>
					</div>
					<div class="other_user left" style="margin-left: 10px;">
						<div class="other-info left" style="padding-left: 10px">
							<span class="name-small left"><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile&uid='.$this->referral->agent_b)?>" class="text-link"><?php echo stripslashes_all($this->referral->agent_b_info->name)?></a></span><br/>
							<span class="l_update"><?php echo JText::_('COM_LASTUPDATED') ?>: </span><span class="l_update"><?php echo $date?></span>
						</div>
					</div>
					<!--<div class="right">
						<?php if($this->referral->signatures != 2){ ?>
								<a href="javascript:void(0)" onclick="declineUserReferral(<?php echo $_GET['ref_id'] ?>)" class="right" style="margin: 15px 15px 0 0; font-size: 10px;">Cancel referral</a>
						<?php } ?>
					</div>-->
					<div class="clear-float"></div>
				</div>
				<!--{aridoc engine="google" width="700" height="800"}
					<?php echo JUri::base().'referrals/referral-'.$this->ref_id.'.pdf';?>
				{/aridoc}-->
				
				<?php if($this->referral->status==5){ ?>
						<div style="font-size: 15px; margin: 0 auto; font-weight: bold; padding-top: 70px; width: 245px;">This referral has been declined</div>
				<?php }else {?>
						
						<div style="padding: 18px 24px 0;"  >
							<div <?php echo !$notMobile ? "class='center_item'" : 'style="display: inline-block;width: 69%;"' ?>>
								<div  class="title" >Please Review &amp; Act on These Documents</div>
								<div <?php echo !$notMobile ? "class='hide_item'" : "" ?> >
									<div style="display: inline-block;padding: 0 10px;">
										<img style="height: 40px;" src="<?php echo $this->baseurl; ?>templates/agentbridge/images/loading.png?<?php echo microtime(true)?>" />
									</div>
									<div style="display: inline-block;width: 180px;vertical-align: top;font-size: 14px;"> <span style="font-weight: bold;">AgentBridge Team</span> <br>AgentBridge, LLC</div>
								</div>								
							</div>
							<div <?php echo !$notMobile ? "class='hide_item'" : 'style="display: inline-block;width: 30%;vertical-align: top;"' ?>  >
								<div >
									<img src="<?php echo $this->baseurl; ?>/images/ab_logo.jpg?<?php echo microtime(true)?>" style="    max-height: 69px;" />
								</div>							
							</div>
							<div style="" <?php echo !$notMobile ? "class='center_item pdf-instruct'" : "class='pdf-instruct'" ?>>You can sign the form through the button below<br>
								<span>(<?php echo !$notMobile ? "Tap" : 'Click' ?> the page to enlarge)</span>
							</div>
						</div>
						
						
						<div class="main-refsign" >
							<div class="slider-holder">
						        <?php $refid=$_GET['ref_id'];?>
						        <div id="images-pdf" class="image-holder">
						        	<div id="image-1" class="slider-image">						
						        		<img id="main-image-1" class="pannable-image" 
						        		     src="<?php echo $this->baseurl; ?>/referrals/temp_images/<?php echo $this->referral->refcontract_filename; ?>_thumb_one.jpg?<?php echo microtime(true)?>"  
						        		     data-high-res-src="<?php echo $this->baseurl; ?>/referrals/temp_images/<?php echo $this->referral->refcontract_filename; ?>_thumb_one.jpg?<?php echo microtime(true)?>"
						        		/>
						        	</div>
						        	<div id="image-2" class="slider-image">
						        		<?php if(JFactory::getUser()->id==$this->referral->agent_a_info->id && $this->signed_o == 0){?>
						        		<form id="submit-sign" action="<?php echo JRoute::_('index.php?option=com_activitylog&task=signedbyuser&event=signing_complete&id='.$refid.'&other='.$this->referral->agent_b_info->id.'&signed='.$this->referral->agent_a_info->id);?>" method="post">
						        		<?php if($notMobile) {?>
						        		<div class="positioning_sign sign_r1" style="display:none">
								            <?php echo $this->referral->agent_a_info->name; ?>
								        </div>								        							        
						        		<div id="signr1" class="positioning signbutton_r1 sub button gradient-green">
								            <input type="submit" value="Sign" />
								        </div>
								        <?php }?>
								        <?php }?>
								        <?php if(JFactory::getUser()->id==$this->referral->agent_b_info->id && $this->signed_r == 0){?>
								        <form id="submit-sign" action="<?php echo $this->baseurl.'/index.php?option=com_activitylog&task=signedbyuser&event=signing_complete&id='.$refid.'&other='.$this->referral->agent_a_info->id.'&signed='.$this->referral->agent_b_info->id;?>" method="post">
								        <?php if($notMobile) {?>
								        <div class="positioning_sign sign_r2" style="display:none">
								            <?php echo $this->referral->agent_b_info->name; ?>
								        </div>								        
								        <div id="signr2" class="positioning signbutton_r2 sub button gradient-green">
								            <input type="submit" value="Sign" />
								        </div>
								        <?php }?>
								        <?php }?>
						        		<img id="main-image-2" class="pannable-image" 
						        		     src="<?php echo $this->baseurl; ?>/referrals/temp_images/<?php echo $this->referral->refcontract_filename; ?>_thumb_two.jpg?<?php echo microtime(true)?>"  
						        		     data-high-res-src="<?php echo $this->baseurl; ?>/referrals/temp_images/<?php echo $this->referral->refcontract_filename; ?>_thumb_two.jpg?<?php echo microtime(true)?>"
						        		     />
						        	</div>
						        </div>
						       
						    </div>

						    <div class="button-holder">
						        <!-- <div id="slider-image-1" class="slider-change curr-page" >1</div>
						        <div id="slider-image-2" class="slider-change">2</div> -->
						        <div class="paginationPDF">
								  <div id="slider-image-1" class="slider-change curr-page" <?php echo !$notMobile ? "style='font-size: 12px;'" : '' ?>>Prev</div>
						          <div id="slider-image-2" class="slider-change" <?php echo !$notMobile ? "style='font-size: 12px;'" : '' ?>>Next</div>
								</div>
						    </div>
						     <?php if(!$notMobile && (
						     			(JFactory::getUser()->id==$this->referral->agent_b_info->id && $this->signed_r == 0) || 
						     			(JFactory::getUser()->id==$this->referral->agent_a_info->id && $this->signed_o == 0) 
						     			)
						     		) {?>
						    <div class="done-div" >
						        <input type="submit" id="done-button" class="sub button gradient-green" value="Sign" />
						    </div>
						     <?php }?>
					    </div>
					    </form>
						<!--  <div class="title">Please Review &amp; Act on These Documents</div>
							 <img src="<?php echo $this->baseurl; ?>/referrals/temp_images/<?php echo $this->referral->refcontract_filename; ?>-page-001.jpg" > 
							 <object data="<?php echo $this->baseurl; ?>/referrals/3028-referral-1488765193.15.pdf" type="application/pdf" style="width:100%;height: 2000px">
						        alt : <a href="<?php echo $this->baseurl; ?>/referrals/3028-referral-1488765193.15.pdf">test.pdf</a>
						    </object> -->
						
				<?php } ?>
			</div>
			<div id="editor"></div>
			<div class="clear-float"></div>
			<div class="clear-float">
				<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
				<br /> <br />
			</div>

		</div>
	</div>


<div style="display: none" id="reason"><br/>
	<select style="width:270px" id="selectreason" style="display:none">
		<option value="">Select Reason</option>
		<option value="1">Client unresponsive</option>
		<option value="2">Client working with another agent</option>
		<option value="3">Client's needs changed</option>
		<option value="4">Retracted</option>
	</select>
	<input type="hidden" id="referral_id"
		name="referral_id"
	/>
	<input type="hidden" id="tochangeid" />
	<div class="clear-float"></div>
	<a href="javascript:void(0)" class="button gradient-green right" onclick="submitStatusChange()" style="margin-top: 10px; padding-top:5px">Submit</a>
	<div class="clear-float"></div>
</div>

<script type="text/javascript">

	function declineFully(){
		var sure = confirm('Click on OK if really want to decline this referral');
		if(sure){
			if(jQuery("#referralrequest").hasClass('ui-dialog-content'))
				jQuery("#referralrequest").dialog('close');
			jQuery("#reason").dialog(
				{
					modal:true,
					title:"Cancel Referral",
				});
			/*jQuery.ajax({
				type: "POST",
				url: '<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>',
				data: {'id': jQuery("#referral_id").val()},
				success: function(data){
					alert(data);
					location.reload();
				}
			});*/
		}
	}
	
	function declineUserReferral(id){
		jQuery("#referral_id").val(id);
		declineFully();
	}

	function declineReferral(id){
		jQuery("#referral_id").val(id);
		jQuery("#referralrequest_div").dialog({modal:true, width: 500});
	}

	function submitStatusChange(){
		jQuery.ajax({
			url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=declinereferral') ?>",
			type: 'POST',
			data: { 'id': jQuery('#referral_id').val(), 'reason':jQuery('#selectreason').val() },
			success: function(response){
				alert(response);
				jQuery('#changestatus').dialog('close');
				location.reload();
			}
		});
	}



	function swipedetect(el, callback){
  
	    var touchsurface = el,
	    swipedir,
	    startX,
	    startY,
	    distX,
	    distY,
	    threshold = 50, //required min distance traveled to be considered swipe
	    restraint = 100, // maximum distance allowed at the same time in perpendicular direction
	    allowedTime = 300, // maximum time allowed to travel that distance
	    elapsedTime,
	    startTime,
	    handleswipe = callback || function(swipedir){}
	  
	    touchsurface.addEventListener('touchstart', function(e){
	        var touchobj = e.changedTouches[0]
	        swipedir = 'none'
	        dist = 0
	        startX = touchobj.pageX
	       // startY = touchobj.pageY
	        startTime = new Date().getTime() // record time when finger first makes contact with surface
	       // e.preventDefault()
	    }, false)
	  
	    touchsurface.addEventListener('touchmove', function(e){
	       // e.preventDefault() // prevent scrolling when inside DIV
	    }, false)
	  
	    touchsurface.addEventListener('touchend', function(e){
	        var touchobj = e.changedTouches[0]
	        distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
	       // distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
	        elapsedTime = new Date().getTime() - startTime // get time elapsed
	        if (elapsedTime <= allowedTime){ // first condition for awipe met
	            if (Math.abs(distX) >= threshold ){ // 2nd condition for horizontal swipe met
	                swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
	            }
	            /*else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
	                swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
	            }*/
	        }
	        handleswipe(swipedir)
	       // e.preventDefault()
	    }, false)
	}

	jQuery(document).ready(function(){
		jQuery('select#selectreason').select2('destroy');


		var el = document.getElementById('images-pdf');



		var sliderWidth="0";



		/*document.getElementById('slider-image-1').addEventListener('click', myFunction, false);*/	
	/*	jQuery("#image-1").panzoom({
			//disableYAxis: true,
			panOnlyWhenZoomed: true
		});
		jQuery("#image-2").panzoom({
			//disableYAxis: true,
			panOnlyWhenZoomed: true
           
		});*/
		/*var viewer = ImageViewer('.pannable-image',{
		    zoomValue : 100
		});

		jQuery('.pannable-image').ImageViewer();*/
		 var viewer = ImageViewer();
		jQuery('.pannable-image').click(function () {
	        var imgSrc = this.src,
	            highResolutionImage = jQuery(this).data('high-res-img');
	 
	        viewer.show(imgSrc, highResolutionImage);
	    });

	    swipedetect(el, function(swipedir){
		    // swipedir contains either "none", "left", "right", "top", or "down"
		    if (swipedir =='right'){
		    	//alert('You just swiped left!');
		    	
				jQuery("#slider-image-1").addClass("curr-page");
				jQuery("#slider-image-2").removeClass("curr-page");
				if(sliderWidth!=0){
					sliderWidth+=50;
					jQuery('.slider-image').animate({left: sliderWidth+"%"}, 500);
				}
		    }
		        

		     if (swipedir =='left'){
		     	//alert('You just swiped right!');
		     
				jQuery("#slider-image-2").addClass("curr-page");
				jQuery("#slider-image-1").removeClass("curr-page");
				if(sliderWidth==0){
					sliderWidth-=50;
					jQuery('.slider-image').animate({left: sliderWidth+"%"}, 500);
				}
		     }
		        
		})

		// var myElement = jQuery('#image-2 .iv-large-image');
		// var mc = new Hammer.Manager(myElement);

		// mc.on("pinch", function(ev) {
		// 	console.log(ev.scale);
		// 	alert(ev.scale);
		// });
/*
		if(jQuery('.iv-snap-handle').width!="100%"){
			jQuery(".positioning").fadeOut( "slow" );
		} else {
			jQuery(".positioning").fadeIn( "slow" );
		}
		*/
		
		var orig_pos = 0;



		//Firefox
		 jQuery('#image-2 .iv-large-image').on('wheel', function(event){
		 	
		  // deltaY obviously records vertical scroll, deltaX and deltaZ exist too
		  if(event.originalEvent.deltaY < 0){
		    // wheeled up
		    orig_pos++;
		  }
		  else {
		    // wheeled down
		    if(orig_pos>0)
		    	orig_pos--;
		  }

		  console.log(orig_pos);

		  	if(orig_pos!=0){
				jQuery(".positioning").fadeOut( "slow" );
			} else {
				jQuery(".positioning").fadeIn( "slow" );
			}

		});

		 

		jQuery("#slider-image-1").bind("click", function(){
			//viewer.resetZoom();			
			jQuery(this).addClass("curr-page");
			jQuery("#slider-image-2").removeClass("curr-page");
			if(sliderWidth!=0){
				sliderWidth+=50;
				jQuery('.slider-image').animate({left: sliderWidth+"%"}, 500);
			}
		}); 
		jQuery("#slider-image-2").bind("click", function(){
			//viewer.resetZoom();
			jQuery(this).addClass("curr-page");
			jQuery("#slider-image-1").removeClass("curr-page");
			if(sliderWidth==0){
				sliderWidth-=50;
				jQuery('.slider-image').animate({left: sliderWidth+"%"}, 500);
			}
		}); 

		jQuery("#signr1").bind("click", function(){
			//jQuery('.sign_r1').toggle();
			//jQuery('.done-div').toggle();			
			jQuery(this).toggle();
		});

		jQuery("#signr2").bind("click", function(){
			//jQuery('.sign_r2').toggle();
			//jQuery('.done-div').toggle();
			jQuery(this).toggle();			
		}); 

		jQuery(".sub.button.gradient-green").on("click",function(){
			
			// do the extra stuff here
		    jQuery.ajax({
		     type: "GET",	
		     async:false,	     
		     url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=test_updatePDF') ?>",
		     data:"ref_id=<?php echo $_GET['ref_id']?>",
		      success: function() {
		      	jQuery("#submit-sign").submit();
		       }
		    });
		});

		/*jQuery("#submit-sign").submit(function() {


		    // do the extra stuff here
		    jQuery.ajax({
		     type: "GET",	
		     async:false,	     
		     url: "<?php echo JRoute::_('index.php?option=com_propertylisting&task=test_updatePDF') ?>",
		     data:"ref_id=<?php echo $_GET['ref_id']?>",
		      success: function() {
		       }
		    });


		    /*return false;
		});*/
	});

</script>