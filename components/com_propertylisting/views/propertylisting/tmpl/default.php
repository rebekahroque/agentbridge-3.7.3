<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('behavior.formvalidation');
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
$application = JFactory::getApplication();
$db = JFactory::getDbo();
$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
$user =& JFactory::getUser($_SESSION['user_id']);
$model = &$this->getModel('PropertyListing');
$images = $model->get_property_image($_GET['lID']);
$address_explode = explode('-----', $this->data->address);
if(isset($_GET['signed']) && $_GET['signed']=="agent_b"){
	echo "<script>alert('')</script>";
}
else if(isset($_GET['signed']) && $_GET['signed']=="agent_a"){
	echo "<script>alert('')</script>";
}
?>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/imagearea/css/imgareaselect-default.css" type="text/css"/>

<script>
	var ptype = 0;
	var stype = 0;
	function redirect(){
		window.location = "<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>";
	}
	function jsonpCallback(data){
		
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
				
		jQuery("#jform_city").val(data.postalcodes[0].placeName);
		//jQuery("#s2id_state").remove();
		setTimeout(function() { 
				try{
					jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());					
				}catch(e){}
		}, 1000);
	}
	var hidden_fields;
	function set_selects(v){
		var ptype_s = jQuery("#jform_ptype").val();
		var stype_s = jQuery("#jform_stype").val();
		var stype_v=v;
		switch(ptype_s){
			case "1":
				if(stype_s==1){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_ceiling'];
				} else if(stype_s==2){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_parking','jform_view','jform_bldgtype'];
				} else if(stype_s==3){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_yearbuilt','jform_parking','jform_view', 'jform_bldgtype'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "2":
				if(stype_s==5){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_view'];
				} else if(stype_s==6){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype'];
				} else if(stype_s==7){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype','jform_yearbuilt'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "3":
				if(stype_s==9){
					hidden_fields = ['jform_features1','jform_features2','jform_style','jform_yearbuilt','jform_occupancy','jform_view'];
				} else if(stype_s==10){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking'];
				} else if(stype_s==11){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_ceiling','jform_stories'];
				} else if(stype_s==12){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_stories'];
				} else if(stype_s==13){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_stories'];
				} else if(stype_s==14){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_yearbuilt','jform_stories'];
				} else {
					hidden_fields = [];
				}
				break;
			case "4":
				if(stype_s==16){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking'];
				} else if(stype_s==17){
					hidden_fields = ['jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_ceiling','jform_stories'];
				} else {
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_stories'];
				}
				break;
				
			default:
				hidden_fields = ['jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_possesion','jform_ceiling','jform_stories','jform_occupancy','jform_parking', 'jform_bldgtype','jform_view'];
		}
		load_form(stype_v);

		
	}

	function progressHandlingFunction(e){
	    if(e.lengthComputable){
	        //jQuery('progress').attr({value:e.loaded,max:e.total});
	        //jQuery("#imageloading").show();
			
			var percentComplete = Math.round((e.loaded/e.total)*100);
			jQuery("#progressbar").width(percentComplete + '%');
			jQuery("#statustxt").html(percentComplete + '%');
			if(percentComplete > 50) {
				jQuery("#statustxt").css('color', '#fff');
			}
	    }
	}
	jQuery(document).ready(function(){
		var order_img=0;
		var order_img_crop;
		var invalid;

		var state_variable = '<?php echo $this->getCountryLangsInitial->stateVariable; ?>';

		//console.log(state_variable);

		if (jQuery(window).width() <= 600) {
			jQuery("#a_photo").removeAttr('onmouseover');
			jQuery("#formdiv").removeAttr('onmouseover');
			jQuery("#jform_zip").removeAttr('onmouseover');
			jQuery("#p_type").removeAttr('onmouseover');
			jQuery("#s_type").removeAttr('onmouseover');
			jQuery("#jform_propertyname").removeAttr('onmouseover');
			jQuery("#p_range").removeAttr('onmouseover');
			jQuery("#jform_price1").removeAttr('onmouseover');
			jQuery("#jform_price2").removeAttr('onmouseover');
			jQuery("#settings_desc").removeAttr('onmouseover');
			jQuery("#p_disclose").removeAttr('onmouseover');
			jQuery("#jform_desc").removeAttr('onmouseover');
			}
		jQuery(".pricevalue").autoNumeric('init', {aSign:'<?php echo $this->getCountryLangsInitial->symbol; ?>', mDec: '0'});
		jQuery(".pricevalue_2").autoNumeric('init', {mDec: '0'});


		jQuery("#jform_pricesetting1").autoNumeric('init', {aSign:'<?php echo $this->getCountryLangsInitial->symbol; ?>', mDec: '0'});
		jQuery("#jform_pricesetting2").autoNumeric('init', {aSign:'<?php echo $this->getCountryLangsInitial->symbol; ?>', mDec: '0'});
		jQuery("#jform_pricesetting1").autoNumeric('set',20000000);
		jQuery("#jform_pricesetting2").autoNumeric('set',1000000);

		jQuery('#try_hard_upload').click(function(){jQuery("#inputfile").click();});

		jQuery("#inputfile").change(function(){
			jQuery('#image_error').hide();
			jQuery('#image_error_filesize').hide();
			jQuery('#image_error_totalfilesize').hide();
			jQuery('#image_error_filenumber').hide();
			jQuery("#image_to_crop").attr("src","");
			console.log("called");
			order_img_crop=0;
			var filename= jQuery(this).val().replace("C:\\fakepath\\", "");
			jQuery("#fakefile").val(filename);
			//jQuery("#imageloading").show();

			var i;
			var len = this.files.length;
			var	formdata = false;
			if(window.FormData){
				formdata = new FormData();
			}
			var image_to_crop_width = 0;

			var img_srcs = [];
			
			var error_filesize = 0;
			var filesize = 0;
			
			for	(i=0	;	i	<	len;	i++	)	{
				
				file	=	this.files[i];
				file.name = file.name+i;
				console.log(file.name);
				filesize = filesize + file.size;
				
				if(file.size >= 8388608) {
					error_filesize = 1;
					break;
				}
				
				if	(!!file.type.match(/image\/(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF)/))	{
					jQuery("#images_thumbs").append('<img id="img'+i+'" class="imgs_th" src=""/>');
					//jQuery("#images_thumbs").append('<div id="progressbox'+i+'" class="progressbox_small" style="display:table;clear:both;"><div class="progressbar"></div><div class="statustxt">0%</div></div>');
					if	(formdata)	{
						formdata.append("image"+i, file);
					}
				}
			}
			
			jQuery("#images_thumbs").css("height", "auto");
			jQuery("#images_thumbs").css("max-height", "200px");
			jQuery("#images_thumbs").css("min-height", "100px");
				
			console.log("Total File Size: " + filesize);
			
			var post_max_size = 83886080; //post max size in bytes, equivalent to 80MB
			if(filesize >= post_max_size) {
				jQuery('#image_error_totalfilesize').show();
				jQuery("#images_thumbs").html("");
			} else if(error_filesize) {
				jQuery('#image_error_filesize').show();
				jQuery("#images_thumbs").html("");
			} else if(this.files.length>20) {
				jQuery('#image_error_filenumber').show();
				jQuery("#images_thumbs").html("");
			} else {
				if	(	window.FileReader	)	{
					reader	=	new	FileReader();
					reader.onloadend	=	function	(e)	{
						
						var image = new Image();
						image.src = e.target.result;

						image.onload = function() {
							// access image size here 
							var width = this.width;
							var height = this.height;
							var ratio = width/height;
							
							if(width > 800) {
								if(ratio > 1) {
									width = 800;
									height = 800/ratio;
								} else {
									width = 800/ratio;
									height = 800;
								}
							}
							
							openCropDialog(formdata, width, height);
							
						};
					};
					file	=	this.files[0];
					file.name = file.name+i;
					reader.readAsDataURL(file);
				}				
			}
				


			//alert(JSON.stringify(this.files));
		});
		
		jQuery(".imgs_th").live("click",function(){
			console.log("clicked imgthumb");
			jQuery("#image_to_crop_container").removeAttr("style");
			jQuery("#ias-select").remove();
			jQuery("#image_to_crop").removeAttr("style");
			jQuery("#image_to_crop").attr('src', jQuery(this).attr("src"));
				var dimensions = jQuery("#image_to_crop").getHiddenDimensions();
				console.log(dimensions);
				jQuery("input[name=\"origwidth\"]").val(dimensions.width);
				jQuery("input[name=\"origheight\"]").val(dimensions.height);
				if(dimensions.width > dimensions.height){
					if(dimensions.width > 800){
						jQuery("#image_to_crop").css("max-width","800px");
						jQuery("#image_to_crop").width((jQuery(window).width()-20));
						jQuery("input[name=\"resized\"]").val('true');
					}
					else{
						jQuery("input[name=\"resized\"]").val('');
					}
				}
				else{
					if(dimensions.height > 800){
						jQuery("#image_to_crop").css("max-height","800px");
						jQuery("#image_to_crop").height((jQuery(window).height()-20));
						jQuery("input[name=\"resized\"]").val('true');
					}
					else{
						jQuery("input[name=\"resized\"]").val('');
					}
				}

				if(jQuery(window).width()<dimensions.width){
					jQuery("#resized_img").val('mobile');
					jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
					jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
				}
			jQuery("head").append("<style id='ias-select'>.imgareaselect-outer{ width:"+jQuery("#image_to_crop").width()+"px !important;height:"+jQuery("#image_to_crop").height()+"px !important}</style>");
				if(jQuery(window).width()<dimensions.width){
							console.log(jQuery(window).width()<dimensions.width);
							console.log(jQuery("#image_to_crop").width());
							jQuery("#resized_img").val('mobile');
							jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
							jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
						}	
						console.log(jQuery("#resized_img").val());												
						jQuery('#image_to_crop').imgAreaSelect({
							x1: 0,
							y1: 0,
							x2: 210,
							y2: 128,
							persistent: true,
							parent: "#cropimage",
							aspectRatio: '66:43',
							onSelectEnd: function (img, selection) {
											jQuery('input[name="x1"]').val(selection.x1);
											jQuery('input[name="y1"]').val(selection.y1);
											jQuery('input[name="x2"]').val(selection.x2);
											jQuery('input[name="y2"]').val(selection.y2);
											jQuery('input[name="src"]').val(jQuery(img).attr('src'));
								        	handles: true;
							        	}
						});		
		});

		jQuery(".submitform").live("click",function(){
			jQuery("#image_to_crop_container").removeAttr("style");
			jQuery("#imagetopost").val(jQuery("#image_to_crop").attr('src'));
			jQuery.post(
				jQuery("#cropform").attr('action'),
				jQuery("#cropform").serialize(),
				function(data){
					jQuery('#image_to_crop').imgAreaSelect({remove:true});
					jQuery('#image_error').hide();
					jQuery('#image_error_filesize').hide();
					jQuery('#image_error_totalfilesize').hide();
					//jQuery("#cropimage").dialog("close");
					var filename = data.replace(/^.*[\\\/]/, '');
					jQuery('#imagegallery').append('<li class="nah"><img style="position: absolute; width: 27px; display:none" src="images/delete-icon.png" /><a class=\"add-photo\" id="fileID'+filename+'"><img src="'+data+'" height=\"98\" width=\"150\" /> <input type="hidden" name="jform_image_order[]" value="'+(++order_img)+'" /> <input type="hidden" name="jform_image[]" value="' + filename.trim() + '" /></a></li>');
					try{
						jQuery("#add-photo-dynamic").parent().remove();
					}catch(e){}
					jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
					jQuery("li.nah a.add-photo").hover(function(){
						jQuery(this).css('opacity', '0.4');
						jQuery(this).prev().show();
						console.log(jQuery(this).prev());
					});
					jQuery("li.nah a.add-photo").mouseout(function(){
						jQuery(this).css('opacity', '');
						jQuery(this).prev().hide();
					});
					jQuery("li.nah a.add-photo").click(function(){
						jQuery(this).parent().remove();
					});
					
					if(jQuery('.imgs_th').length > 1){
						if(jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().length){
							console.log(jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().attr("id"));
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().click();
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").prev().remove();
						} else {
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").prev().click();
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().remove();
						}
						
						jQuery("#image_to_crop").load(function(){
							jQuery('#image_to_crop').imgAreaSelect({
								x1: 0,
								y1: 0,
								x2: 210,
								y2: 128,
								persistent: true,
								parent: "#cropimage",
								aspectRatio: '66:43',
								onSelectEnd: function (img, selection) {
									jQuery('input[name="x1"]').val(selection.x1);
									jQuery('input[name="y1"]').val(selection.y1);
									jQuery('input[name="x2"]').val(selection.x2);
									jQuery('input[name="y2"]').val(selection.y2);
									jQuery('input[name="src"]').val(jQuery(img).attr('src'));
									handles: true;
								}
							});		
						});
						
					} else {
						jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").remove();
						jQuery("#cropimage").dialog("close");
					}
					//jQuery("#cropimage").dialog("close");
				}
			);
		});


		jQuery("#pocket_setting").on('submit',function(){
			return false;
		});




	/*	<?php $ziptext_php = "Zip";
			  $statetext_php = "State";
			  if($this->this_lang_tag=="english-CA"){ 
				$ziptext_php = "Postal";
				$statetext_php = "Province";
				?> 
			  jQuery("#jform_zip").attr("placeholder","Postal Code");
			  jQuery(".jform_zip_2.error_msg").text("Postal code must be at least 5 digits.");		

		<?php }  else {?>
			  jQuery("#jform_zip").attr("placeholder","Zip Code");
			  jQuery(".jform_zip_2.error_msg").text("Zip code must be at least 5 digits.");
		<?php }?>

		
		var $ajax = jQuery.noConflict();
		$ajax.ajax({
			url: '<?php echo $this->baseurl; ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': <?php echo (!empty($this->countryId)) ? $this->countryId: '""'; ?>, 'state': <?php echo (!empty($this->data->state)) ? $this->data->state: '""'; ?> },
			success: function(e){
				$ajax("#stateDiv").html(e)
				.promise()
				.done(
						function () {
							$ajax("#jform_state").val(<?php echo $this->data->state ?>);
							try{	
								jQuery("#jform_state").select2();
								$ajax("#jform_state").select2("val","<?php echo $this->data->state ?>");
							}catch(e){}

							if(jQuery("#jform_country").val() == 38){
							//	jQuery("#s2id_jform_state a span").text("Province");
								//jQuery(".select2-results li").first().closest("span").text("Province");
							} else {
							//	jQuery("#s2id_jform_state a span").text("State");
						//		//jQuery(".select2-results li").first().closest("span").text("State");
							}
							jQuery("#s2id_jform_state a span").text("<?php echo $this->getCountryLangsInitial->stateLabel; ?>");	

						//	jQuery("#s2id_jform_state a span").text("Province");
						//	jQuery(".select2-results li").first().closest("span").text("Province");
						}
					);
			}
		});*/
		jQuery(".hide").fadeOut();
		jQuery("#show_address").click(function(){
			jQuery(this).toggleClass("hideChild");
			if(jQuery(this).hasClass("hideChild")){
				jQuery(".hide").fadeOut();
				jQuery(this).html('+ Add Complete Address');
			}
			else{
				jQuery(".hide").fadeIn();
				jQuery(this).html('- Add Complete Address');
			}
		});
		jQuery(".disclose").click(function(){
			jQuery(".disclose").removeClass("yes");
			jQuery(".disclose").removeClass("gradient-blue-toggle");
			jQuery(".disclose").removeClass("gradient-gray");
			jQuery(".disclose").addClass("no");
			jQuery(".disclose").addClass("gradient-gray");
			
			jQuery(this).removeClass("gradient-gray");
			jQuery(this).removeClass("no");
			jQuery(this).addClass("yes");
			jQuery(this).addClass("gradient-blue-toggle");
			jQuery("#jform_disclose").val(jQuery(this).attr("rel"));
		})
		jQuery(".pricerange").click(function(){
			jQuery(".pricerange").removeClass("yes");
			jQuery(".pricerange").removeClass("gradient-blue-toggle");
			jQuery(".pricerange").removeClass("gradient-gray");
			jQuery(".pricerange").addClass("no");
			jQuery(".pricerange").addClass("gradient-gray");
			select_price_type (jQuery(this).attr("rel"));
			
			jQuery(this).removeClass("gradient-gray");
			jQuery(this).removeClass("no");
			jQuery(this).addClass("yes");
			jQuery(this).addClass("gradient-blue-toggle");
			jQuery("#jform_pricerange").val(jQuery(this).attr("rel"));
			select_price_type (jQuery(this).attr("rel"));
		})
		jQuery("#imagegallery").append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
		jQuery("input[type=\"file\"]").mouseenter(function(){
			console.log("hover");
		});

		jQuery('#clickchangecountry').click(function(){				
			var status = jQuery(this).attr('data');			
			jQuery('#changecountry').dialog(
					{
					  title: "<?php echo JText::_('COM_POPS_TERMS_CHANGE_COUNTRY') ?>",
					  width:'auto'
					});
		});
		
		var this_id = "<?php echo $this->getCountryLangsInitial->country; ?>";
		var baseurl = "<?php echo $this->baseurl?>";
		get_state(jQuery("#jform_country").val());
		applyAddressDataAP(baseurl,jQuery("#country").val(),jQuery("#jform_country").val(),state_variable);

		jQuery(".choose-country").click(function(){
			var this_id = jQuery(this).attr("id");
			var this_class=jQuery(this).attr('class').split(' ');
			get_state(this_id);
			jQuery("#jform_city").val("");

						
			jQuery("#jform_zip").unbind("keyup");
			jQuery("#jform_zip").val("");
			jQuery('#jform_zip').inputmask('remove');
			
			//address auto
			applyAddressDataAP(baseurl,this_class[0],this_id,state_variable);

				jQuery("#country").val(this_class[0]);
				jQuery("#jform_country").val(this_id);	

			if(jQuery.trim( jQuery('#formdiv').html() ).length){
				console.log(jQuery("#jform_stype").val());
				jQuery('#formdiv').html("")
				set_selects(jQuery("#jform_stype").val());
			}

			

				jQuery("#clickchangecountry").html("<a href='#' onclick='return false;'> <img class='ctry-flag' style='margin-right: 6px;' src='<?php echo $this->baseurl;?>/templates/agentbridge/images/"+this_class[0].toLowerCase()+"-flag-lang.png'><?php echo JText::_('COM_POPS_TERMS_CHANGE_COUNTRY') ?></a>");
				
				jQuery("#clickrevertcurrency").attr('id','clickchangecurrency');

				if(jQuery(this).find(".this_currency").val() != "<?php echo $this->countryCurrency;?>"){
					jQuery('#clickchangecurrency').css("display","block");

					var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
					if(count_Ar.length > 2){
						var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
						jQuery('#clickchangecurrency').removeClass(lastClass);
						var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
						jQuery('#clickchangecurrency').removeClass(lastClass);
					}


					jQuery('#clickchangecurrency').addClass(jQuery(this).find(".this_currency").val()+" cho_"+jQuery(this).find(".this_symbol").val());
					var this_class=jQuery("#clickchangecurrency").attr('class').split(' ');
					jQuery("#clickchangecurrency").html("<a href='#' onclick='return false;'> <?php echo JText::_('COM_POPS_TERMS_SET_TO_CURRENCY')?> "+this_class[1].split('_')[1]+this_class[0]+". <?php echo JText::_('COM_POPS_TERMS_CHANGE_TO_CURRENCY')?> <span id='chosenCurr'>"+this_class[3].split('_')[1]+this_class[2]+"</span>.</a></a>");
					
					jQuery("#chosenCurr").html(jQuery(this).find(".this_symbol").val()+jQuery(this).find(".this_currency").val());

					
					//jQuery("#jform_price1").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");
					//jQuery("#jform_price2").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");

					jQuery("#jform_price1").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
				    jQuery("#jform_price2").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
					jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
					jQuery("#jform_pricesetting1").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
					jQuery("#jform_pricesetting2").keyup(function(){
					//	jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
					});
					
				
				} else {	

					var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
					
					jQuery("#jform_price1").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
				    jQuery("#jform_price2").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
					jQuery(".pricevalue").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
					jQuery("#jform_pricesetting1").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
					jQuery("#jform_pricesetting2").keyup(function(){
						//jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
					});
					if(count_Ar.length == 4){
						var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
						jQuery('#clickchangecurrency').removeClass(lastClass);
						var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
						jQuery('#clickchangecurrency').removeClass(lastClass);
					} else if(count_Ar.length == 3){					
						var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
						jQuery('#clickchangecurrency').removeClass(lastClass);
					}

					jQuery('#clickchangecurrency').css("display","none");
				}

			jQuery(this).css("display","none");
			jQuery('.choose-country').not(this).each(function(){
		         jQuery(this).css("display","block");
		    });
			jQuery(".choose-country."+jQuery(this).attr("class").split(' ')[0]).css("display","none");
			jQuery('#changecountry').dialog('close');



		});

		jQuery('#clickchangecurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> <?php echo JText::_('COM_POPS_TERMS_SET_TO_CURRENCY')?> "+this_class[3].replace("cho_","")+this_class[2]+". <?php echo JText::_('COM_POPS_TERMS_REVERT')?> "+this_class[1].replace("def_","")+this_class[0]+".</a>");
			jQuery("#jform_price1").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
			jQuery("#jform_price2").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);

 			
 			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
 			jQuery("#jform_pricesetting1").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				//jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			});

			jQuery("#jform_currency").val(this_class[2]);

			jQuery(this).attr('id','clickrevertcurrency');

		});

		jQuery('#clickrevertcurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> <?php echo JText::_('COM_POPS_TERMS_SET_TO_CURRENCY')?> "+this_class[1].replace("def_","")+this_class[0]+". <?php echo JText::_('COM_POPS_TERMS_CHANGE_TO_CURRENCY') ?> <span id='chosenCurr'>"+this_class[3].replace("cho_","")+this_class[2]+"</span>.</a></a>");
			jQuery("#jform_price1").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);
			jQuery("#jform_price2").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);

			jQuery("#jform_currency").val(this_class[0]);


			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			
 			jQuery("#jform_pricesetting1").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				//jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			});

			jQuery(this).attr('id','clickchangecurrency');

		});

		/* Updated Dialog Country */

		jQuery(".choose-country.<?php echo $this->countryIso;?>").css("display","none");

		  jQuery('.tabs .tab-links a').live('click', function(e)  {
		        var currentAttrValue = jQuery(this).attr('href');

		        // Show/Hide Tabs
		        jQuery(currentAttrValue).css("display","block");
			 	jQuery(currentAttrValue).siblings().hide();
		        // Change/remove current tab to active
		        jQuery(this).parent('li').addClass('country_activel').siblings().removeClass('country_activel');
		 
		        e.preventDefault();
		    });

		/* Updated Dialog Country */


		jQuery(".sub_button").live("click",function(){
			
			jQuery("#jform_price1").val(jQuery("#jform_price1").autoNumeric('get'));
			jQuery("#jform_price2").val(jQuery("#jform_price2").autoNumeric('get'));

		});


	});
	function setButtons(){
		jQuery(".pet").click(function(){
				jQuery(".pet").removeClass("yes");
				jQuery(".pet").removeClass("gradient-blue-toggle");
				jQuery(".pet").removeClass("gradient-gray");
				jQuery(".pet").addClass("no");
				jQuery(".pet").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_pet").val(jQuery(this).attr("rel"));
			})
			jQuery(".furnished").click(function(){
				jQuery(".furnished").removeClass("yes");
				jQuery(".furnished").removeClass("gradient-blue-toggle");
				jQuery(".furnished").removeClass("gradient-gray");
				jQuery(".furnished").addClass("no");
				jQuery(".furnished").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_furnished").val(jQuery(this).attr("rel"));
			})
	}
		function select_price_type(e){			
		
			jQuery("#jform_price2, #jform_price1").removeClass("glow-required");
			jQuery(".jform_price1, .jform_price2").hide();
			
			jQuery(".qtip").remove();
			
			if(e==2) {
				document.getElementById('jform_price2').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1").show();	
				jQuery(".exact-label").show();	
					
			} else if(e==1) {				
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1, .hl-label").show();							
				document.getElementById('jform_price2').disabled=false;				
				jQuery("#jform_price2, .hl-label").show();	
				jQuery(".exact-label").hide();					
			} else {
				document.getElementById('jform_price2').disabled=true;				
				document.getElementById('jform_price1').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price1").hide();				
				jQuery("#jform_price1").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();			
			}		
		}	
	
	function get_sub(f){
		jQuery("#loading-image_custom_question").show();
		ptype=f;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=subtype',
			type: 'get',
			data: { 'ptype': f },
			success: function(msg){
				jQuery("#subtype").show();
				jQuery("#loading-image_custom_question").hide();
				var html="<option value=\"\">--- Select ---</option>";
				var x = $nocon.parseJSON(msg);
				for(var i=0; i<x.length; i++){
					html+="<option value=\""+x[i].sub_id+"\">"+x[i].name+"</option>";
				}
				jQuery("#jform_stype").html(html).promise().done(function(){
					//jQuery("#jform_stype").val("");
					jQuery("#jform_stype").select2("val", "");
					jQuery("#formdiv").html("");
				});
			}
		});
	}
	function load_form(v){
		jQuery("#loading-image_custom_question2").show();
		stype=v;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>?task=form&country='+jQuery("#jform_country").val(),
			type: 'POST',
			data: { 'ptype': ptype, 'stype': stype },
			success: function(msg){
				jQuery("#loading-image_custom_question2").hide();
				var temp = $nocon.trim(msg.replace('\n',''));
				temp = temp.replace('<div class="pocket-form">', '');
				temp = jQuery('<div/>').html(temp);
				if($nocon.trim(temp)!="&lt;/div&gt;"){
					jQuery("#formdiv").html(msg);
					jQuery("#formdiv").show();
					jQuery("#prop_features").show();
					if (jQuery(window).width() <= 600) {
								jQuery("#prop_features").removeAttr('onmouseover');	
					}
					jQuery("#formdiv").css('border-bottom', '1px solid #dedede');
					jQuery("section.main-content").css('min-height', '700px');
					jQuery("section.main-content").height(jQuery(document).height()-jQuery("section.header").height()-jQuery("section.footer").height());
					setButtons();
					if(array!=null)
						jQuery(formnames).each(function(index){
							jQuery("select[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
						})
						jQuery(jQuery("#formdiv").children()[0]).append('<a id="showhidefield" href="javascript:void(0)" onclick="toggleHiddenFields()"><?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?></a>');
					
					jQuery('#jform_features1').change(function(){
						if(jQuery('#jform_features1').val()){
							jQuery('#jform_features2 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
							jQuery('#jform_features3 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
						}
					});
					jQuery('#jform_features2').change(function(){
						if(jQuery('#jform_features2').val()){
							jQuery('#jform_features1 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
							jQuery('#jform_features3 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
						}
					});
					jQuery('#jform_features3').change(function(){
						if(jQuery('#jform_features3').val()){
							jQuery('#jform_features1 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
							jQuery('#jform_features2 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
						}
					});
					jQuery('select').not("#jform_state").not("#citySelect").select2().promise().done(function(){jQuery(hidden_fields).each(function(index){ jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');})});
					//jQuery("#s2id_jform_state a span").text(jQuery("#stateLabel").text());
				}
			}
		});
	}

	var $ajax = jQuery.noConflict();
	
	function get_state(cID){
		$ajax("#jform_state").html('');
		$ajax.ajax({
			url: '<?php echo $this->baseurl; ?>/custom/_get_state.php',
			type: 'POST',
			params: {
				contentType: 'application/html; charset=utf-8'
			},	
			data: { 'cID': cID },
			success: function(e){
				$ajax("#jform_state").html(e);
				jQuery("#jform_state").select2();
				
			}
		});


	}

	function get_state_partial(cID){
		$ajax("#jform_state").html('');

		$ajax.ajax({
            url: "<?php echo $this->baseurl?>/index.php?option=com_propertylisting&task=getPartialGBCodeStates&format=raw&webservice=1",
            type: "POST",
            data: {partialCode:jQuery("#jform_zip").val()},
            async:false,
            success: function(data){
            	
            	if(data!=0){		                		
            		console.log(data);
            		var obj = data;
            		
            		$ajax.ajax({
						url: '<?php echo $this->baseurl; ?>/custom/_get_state.php',
						type: 'POST',
						params: {
							contentType: 'application/html; charset=utf-8'
						},	
						data: { 'cID': cID, 'states' : obj},
						success: function(e){
							$ajax("#jform_state").html(e);
						}
					});
					
            		
				} else {
					//jQuery("#jform_zip").val("");
					
					
				}
				//jQuery("#jform_state").select2();
				
				jQuery("#s2id_jform_state a span").text("Post Town");
            }
        });
		


	}

	function toggleHiddenFields(){
		if(jQuery('#showhidefield').html()=='<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_LESS_FIELDS') ?>')
		else
			jQuery('#showhidefield').html('<?php echo JText::_('COM_POPS_TERMS_MORE_FIELDS') ?>')
			
			
			console.log(jQuery('#showhidefield').html()=='More Fields'+"|"+jQuery('#showhidefield').html())
		jQuery(hidden_fields).each(function(index){
			jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');
		});
		
	}
	function showPrivate(){
		jQuery("#loremipsum").dialog({
			modal: true,
			title: "Set to Private",
			});
	}
	function showSettings(){
		jQuery("#settingsform").dialog({
				modal: true,
				width: 'auto',
				minHeight: '300px',
				title: "<?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM') ?>",
				open: function(){
					jQuery('input[type=\'radio\']').click(function(){
						var selectedVal = jQuery('input[name=\'jform[setting]\']:checked').val();
						console.log(selectedVal);
					});
				}
		});
	}
	
	function openCropDialog(formdata, image_to_crop_width, image_to_crop_height) {
			
			jQuery.ajax({
				url:	"<?php echo $this->baseurl; ?>/file_upload_dus.php",
				type:	"POST",
				data:	formdata,
				//async: false,
				processData:	false,
				contentType:	false,
				dataType: 'json',
				xhr: function() {  // Custom XMLHttpRequest
		            var myXhr = jQuery.ajaxSettings.xhr();
		            if(myXhr.upload){ // Check if upload property exists
		                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
		            }
		            return myXhr;
		        },
				beforeSend: function() {
					jQuery("#cropimage").dialog({
						modal:true,
						width: 'auto',
						title: "<?php echo JText::_('COM_POPS_TERMS_PHOTO_GALLERY_RESIZE') ?>",
						position: { my: "center center", at: "center center", of: window },
						dragStop: function(){

							jQuery('#image_to_crop').imgAreaSelect({
								x1: 0,
								y1: 0,
								x2: 210,
								y2: 128,
								persistent: true,
								aspectRatio: '66:43',
								onSelectEnd: function (img, selection) {
												jQuery('input[name="x1"]').val(selection.x1);
												jQuery('input[name="y1"]').val(selection.y1);
												jQuery('input[name="x2"]').val(selection.x2);
												jQuery('input[name="y2"]').val(selection.y2);
												jQuery('input[name="src"]').val(jQuery(img).attr('src'));
												handles: true;
											}
							});
						},
						open: function(){
							
						},
						close: function( event, ui ) {
									jQuery("#image_to_crop_container").removeAttr("style");
									jQuery("#ias-select").remove();
									jQuery('.imgs_th').remove();
									jQuery("#image_to_crop").width('');
									jQuery("#image_to_crop").height('');
									jQuery("input[name=\"resized\"]").val('');
									jQuery('#image_to_crop').imgAreaSelect({remove:true});
									jQuery(".submitform").unbind('click');
									try{
										jQuery("#add-photo-dynamic").parent().remove();
									}catch(e){}
									jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
								},
						dragStart: function( event, ui ) {
							jQuery('#image_to_crop').imgAreaSelect({remove:true});
						}
					});
					jQuery(".crop_image").hide();
					jQuery("#images_thumbs").hide();
					jQuery("#progressbox").show();
					jQuery("#statustxt").css("color", "#000");
					jQuery('#image_to_crop').hide();
				},
				success:	function	(response)	{
					jQuery("#image_to_crop_container").css("width", image_to_crop_width + "px");
					jQuery("#image_to_crop_container").css("height", image_to_crop_height + "px");
					jQuery("#cropimage").dialog({
						position: { my: "center center", at: "center center", of: window }
					});
					//alert(appname+"uploads/"+response);
					console.log(response.length);
					var loaded = 0;
					jQuery.each(response, function(index, element) {
			           	jQuery("#img"+index).on('load', function() {
							console.log("image loaded correctly");
							++loaded;
							if (loaded === response.length) {
								jQuery('#image_to_crop').imgAreaSelect({
									x1: 0,
									y1: 0,
									x2: 210,
									y2: 128,
									persistent: true,
									parent: "#cropimage",
									aspectRatio: '66:43',
									onSelectEnd: function (img, selection) {
										jQuery('input[name="x1"]').val(selection.x1);
										jQuery('input[name="y1"]').val(selection.y1);
										jQuery('input[name="x2"]').val(selection.x2);
										jQuery('input[name="y2"]').val(selection.y2);
										jQuery('input[name="src"]').val(jQuery(img).attr('src'));
										handles: true;
									}
								});
							}
						}).attr("src",appname+"uploads/"+element);

						console.log(index);	
						//jQuery("#imageloading").show();	
						//console.log(response);
					});
					
					jQuery('#image_to_crop').show();
					jQuery("#progressbox").hide();
					jQuery("#image_to_crop").css('max-width', "800px");
					
					jQuery("#image_to_crop").on('load', function() { console.log("loading image 1");}).attr("src", jQuery("#img0").attr("src"));
					//jQuery("#image_to_crop").attr('src', jQuery("#img0").attr("src"));
					jQuery("#images_thumbs").show();
								
							jQuery("#image_to_crop").load(function(){
								jQuery(".crop_image").show();
								
								//jQuery("#imageloading").hide();
								
								var dimensions = jQuery("#image_to_crop").getHiddenDimensions();
								console.log(dimensions);
								jQuery("input[name=\"origwidth\"]").val(dimensions.width);
								jQuery("input[name=\"origheight\"]").val(dimensions.height);
								if(dimensions.width > dimensions.height){
									if(dimensions.width > 800){
										jQuery("#image_to_crop").css("max-width","800px");
										jQuery("#image_to_crop").width((jQuery(window).width()-20));
										jQuery("input[name=\"resized\"]").val('true');
										
									}
									else{
										jQuery("input[name=\"resized\"]").val('');
									}
								}
								else{
									if(dimensions.height > 800){
										jQuery("#image_to_crop").css("max-height","800px");
										jQuery("#image_to_crop").height((jQuery(window).height()-20));
										jQuery("input[name=\"resized\"]").val('true');
									}
									else{
										jQuery("input[name=\"resized\"]").val('');
									}
								}

								if(jQuery(window).width()<dimensions.width){
									jQuery("#resized_img").val('mobile');
									jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
									jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
								}
								jQuery("head").append("<style id='ias-select'>.imgareaselect-outer{ width:"+jQuery("#image_to_crop").width()+"px !important;height:"+jQuery("#image_to_crop").height()+"px !important}</style>");
								
										if(jQuery(window).width()<dimensions.width){
											console.log(jQuery(window).width()<dimensions.width);
											console.log(jQuery("#image_to_crop").width());
											jQuery("#resized_img").val('mobile');
											jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
											jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
										}	
										console.log(jQuery("#resized_img").val());												
										/*
										jQuery('#image_to_crop').imgAreaSelect({
											x1: 0,
											y1: 0,
											x2: 210,
											y2: 128,
											persistent: true,
											parent: "#cropimage",
											aspectRatio: '66:43',
											onSelectEnd: function (img, selection) {
															jQuery('input[name="x1"]').val(selection.x1);
															jQuery('input[name="y1"]').val(selection.y1);
															jQuery('input[name="x2"]').val(selection.x2);
															jQuery('input[name="y2"]').val(selection.y2);
															jQuery('input[name="src"]').val(jQuery(img).attr('src'));
												        	handles: true;
											        	}
										});
										*/
								jQuery("#cropimage").dialog({
									modal:true,
									width: 'auto',
									title: "<?php echo JText::_('COM_POPS_TERMS_PHOTO_GALLERY_RESIZE') ?>",
									position: { my: "center center", at: "center center", of: window }									
								});
							});
					
					
				}
			});
		}
</script>
<link
	rel="stylesheet" type="text/css"
	href="<?php echo $this->baseurl ?>/templates/agentbridge/css/styles.css" />
<style>
body {
	background: #fff;
}

.imgs_th {
    padding: 5px;
    display: inline-block;
    max-height: 100px;
    max-width: 100px;
    cursor: pointer;
}

.imgs_th2 {
    height: 100px;
    width: 100px;
}

</style>

<script type="text/javascript">
	var $nocon = jQuery.noConflict();
	<?php $timestamp = time();?>
	function bindUpload(id){
		jQuery('input[type=file]').trigger('click');
		//});
	}
	function validateFormLocal(){
		if(jQuery('.add-photo').length>1)
			return true;
		else{
			document.getElementById('vpb_upload_button').scrollIntoView();
			jQuery('.vpb_main_demo_wrapper').show()
			jQuery('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Please upload an image</div>');
			return false;
		}
	}
</script>

<!-- start main content -->
<script type="text/javascript">
mixpanel.track("Add POPs");
</script>
<div class="wrapper">
	<div class="wide left">
		<!-- start wide-content -->
		<div id="pocketform" class="popsform">
			<h1><?php echo JText::_('COM_USERPROF_PROF_MYPOPS') ?></h1>
			<form id="pocket-listingx"
				class="oooooform-validateooooo form-horizontal"
				enctype="multipart/form-data" >
				<input id="buyer_form_type" type="hidden" name="jform[form]" value="add_pocket" />
				<div class="pocket-form" style="padding-bottom: 0">
					<div class="c200" style="width:150px; margin-right:30px">
						<input id="jform_zip" name="jform[zip]"
							maxlength="5" class="text-input" type="text"  
                            onMouseover="ddrivetip('Enter <?php echo $ziptext_php; ?> code of POPs&trade; location.');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" placeholder="Postal Code" />
						<div id="clickchangecountry" class="change_country"><a href="#" onclick="return false;"> <?php echo JText::_('COM_POPS_TERMS_CHANGE_COUNTRY') ?> </a></div>
						<div>
							<p class="jform_zip error_msg" style="margin-top:12px;margin-left:2px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /></p> 
							<p class="jform_zip_2 error_msg" style="margin-top:12px;margin-left:2px;display:none; line-height:14px"> <?php echo JText::_('COM_POPS_TERMS_ZIP_ERROR') ?> <br /></p>
						</div>
					</div>
					<div id="p_type" class="c200" style="width:220px" onMouseover="ddrivetip('Select type of property: Purchase or lease. Residential or Commercial.');hover_dd()" onMouseout="hideddrivetip();hover_dd()" >
						<select style="width:195px" 
							id="jform_ptype" 
							name="jform[ptype]" 
							onchange="get_sub(this.value)">
							<option value=""><?php echo JText::_('COM_POPS_TERMS_POP_TYPE') ?></option>
							<?php
							foreach($this->ptype as $ptype){
								if($ptype->type_id==$this->property_type) $selected = "selected='selected'";
								else $selected = "";
								echo "<option value=\"".$ptype->type_id."\" ". $selected .">".JText::_($ptype->type_name)."</option>";
							}
							?>
						</select>
                         <img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
						<div>
							<p class="jform_ptype error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /></p> 
						</div>
					</div>
					<div id="s_type" class="c200" onMouseover="ddrivetip('Choose sub type of property.');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
						<div id="subtype" style="display:none">
							<select style="width:195px" id="jform_stype" name="jform[stype]"
								onchange="set_selects(this.value);">
								<option value=""><?php echo JText::_('COM_POPS_TERMS_POP_SUBTYPE') ?></option>
								<?php
								foreach($this->sub_types as $stype){
									if($stype->sub_id==$this->sub_type) $selected = "selected='selected'";
									else $selected = "";
									echo "<option value=\"".$stype->sub_id."\" ".$selected.">".$stype->name."</option>";
								}
								?>
							</select>
                            
                             <img id="loading-image_custom_question2" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
							<div>
								<p class="jform_stype error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> <br /></p> 
							</div>
						</div>
					</div>
					<div class="clear-float"></div>
					<div style="font-size: 12px;">
						<div id="completeadd" class="popsaddress"
							style="margin-top: 10px">
							<div class="left" style="margin-bottom:10px;margin-right:5px;">
								<div class="left" style="display:none;"><label id="cityLabel" class="left" style="margin-bottom: 10px; margin-top:10px">City</label> </div>
								<div class="right"><input
									id="jform_city" 
									name="jform[city]" 
									class="text-input-city"
									type="text"
									placeholder="City" 
									style="margin-left:0;"
								/>
								<p class="jform_state error_msg" style="margin-top:12px;margin-left:20px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> </p></div> 
							</div>
							<div class="left popsstate">
								<label class="left" id="stateLabel" style="display:none;margin-bottom: 10px; margin-top:10px"><?php echo $statetext_php; ?></label>
								<div id="stateDiv" class="state_dropdown">
									<select 
										id="jform_state" 
										name="jform[state]">
									</select>								
								</div>
								<div><p class="jform_state error_msg" style="margin-top:12px;margin-left:65px;display:none"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> </p></div> 
                                
							</div>
							<div class="clear-float"></div>
						</div>
					</div>
					<div class="clear-float"></div>
				</div>
				<div class="pocket-form clear-float popsformdisplay" >
					<h2><?php echo JText::_('COM_POPS_TERMS_DISPLAY_NAME') ?></h2>
					<div class="c200" style="width:90%">
						<input 
							id="jform_propertyname"
							onMouseover="ddrivetip('Name that can be viewed by other agents that describes your property i.e. Gated Equestrian Estate');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							name="jform[property_name]"
							class="text-input-propertyname" 
							type="text" 
							placeholder="<?php echo JText::_('COM_POPS_TERMS_NAME') ?>" />
						<div><p class="jform_propertyname error_msg" style="margin-top:10px;display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> </p></div>
					</div>
					<div class="clear-float"></div>
					<h2 style="margin-bottom:20px"><?php echo JText::_('COM_POPS_TERMS_PROPERTY_PRICE') ?></h2>
					<div class="rangeError" style='font-size:10px;margin-top:-10px;margin-left:160px;margin-bottom:15px;font-size:10px;display:none' ></div>
					<?php 
						$currency_def = "USD";
						$currency_sym = "$";
						
						if($this->countryCurrency){
							//$curreData = getCountryCurrencyData($this->data->country);
							$currency_def = $this->countryCurrency;
							$currency_sym = $this->countryCurrSymbol;
						}

					?>
					<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?>" style="display:none;font-size: 13px;margin-bottom: 15px;cursor: pointer;"><a href="#" onclick="return false;"> <?php echo JText::_('COM_POPS_TERMS_SET_TO_CURRENCY') ?> <?php echo $currency_sym; ?><?php echo $currency_def; ?>. <?php echo JText::_('COM_POPS_TERMS_CHANGE_TO_CURRENCY') ?> <span id="chosenCurr"></span>.</a> </div>	
								
					<div id="p_range" class="left ys p_range" onMouseover="ddrivetip('Choose price range for buyer')" onMouseout="hideddrivetip();hover_dd()" >
						<label><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE') ?></label>
						<a href="javascript: void(0)" class="left <?php echo ($this->data->price_type|| !isset($this->data->price_type)) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> pricerange" style="margin-right:1px" rel="1"><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE_OPT_RANGE') ?></a>
                        <a href="javascript: void(0)" class="left <?php echo (isset($this->data->price_type) && !$this->data->price_type) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> pricerange" style="margin-right:1px" rel="2"><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE_OPT_EXACT') ?></a>
							<input 
							type="hidden" 
							value="<?php echo (isset($this->data->price_type)) ? $this->data->price_type : 1; ?>" 
							id="jform_pricerange" 
							name="jform[pricerange]"
							class="text-input"/>
					</div>
					<div class="left ys price_row">
						<label class="exact-label" style="display:none"><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE_OPT_EXACT') ?></label>
						<label class="hl-label"><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE_OPT_RANGE_LOW') ?></label>
						<input 
                        	onMouseover="ddrivetip('Low price range for your property')"
							onMouseout="hideddrivetip();hover_dd()"
							id="jform_price1" 
							name="jform[price1]"							
							class="text-input pricevalue "
							type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" />
						<div><p class="jform_price1 error_msg" style="margin-top:10px;display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> </p></div> 
					</div>
					<div class="left ys price_row">
						<label class="hl-label"><?php echo JText::_('COM_POPS_TERMS_PRICE_TYPE_OPT_RANGE_HIGH') ?></label>
						<input 
                        	onMouseover="ddrivetip('Upper price range for your property')"
							onMouseout="hideddrivetip();hover_dd()"
							id="jform_price2"
							name="jform[price2]"
							class="text-input pricevalue" 
							type="text"
							placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" />
						<div><p class="jform_price2 error_msg" style="margin-top:10px;display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR') ?> </p></div> 
					</div>
					<div id="p_disclose" class="left ys price_row" onMouseover="ddrivetip('Disclose price? If not disclosed will display at end of searches')" onMouseout="hideddrivetip();hover_dd()">
						<label><?php echo JText::_('COM_POPS_TERMS_PRICE_DISCLOSE') ?></label>
                        <a href="javascript: void(0)" class="left <?php echo ($this->data->disclose || !isset($this->data->disclose)) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> disclose" style="margin-right:1px" rel="1"><?php echo JText::_('COM_POPS_TERMS_PRICE_DISCLOSE_OPT_YES') ?></a>
                        <a href="javascript: void(0)" class="left <?php echo (isset($this->data->disclose) && !$this->data->disclose) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> disclose" style="margin-right:1px" rel="0"><?php echo JText::_('COM_POPS_TERMS_PRICE_DISCLOSE_OPT_NO') ?></a>
							<input 
							type="hidden" 
							value="<?php echo (isset($this->data->disclose)) ? $this->data->disclose : 1; ?>" id="jform_disclose" name="jform[disclose]"
							onMouseover="ddrivetip('Disclose price? If not disclosed will display at end of searches')"
							onMouseout="hideddrivetip();hover_dd()"
							class="text-input"/>
					</div>
					<p class="jform_priceinvalid error_msg clear-float pinvalid" style="display:none;"><?php echo JText::_('COM_POPS_TERMS_INVPRICE') ?></p>
				</div>
				<div style="display:none" id="formdiv" onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results');hover_dd()" onMouseout="hideddrivetip();hover_dd()" ></div>
				<div class="pocket-form popsformpermission">
					<div class="c480">
						<h2><?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS') ?></h2>
						<input 
							id="jform_setting" 
							checked="true" 
							name="jform[setting]"
							type="radio" 
							value="1"
							/>
						<label class="permissionlbl">&nbsp;&nbsp;<a style="color: #333333; cursor: default"><?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM') ?></a>
						</label>
						<input 
							id="jform_settings_private" 
							name="jform[setting]"
							type="radio" 
							value="2"
							onclick="disablePermission()"
							/>
						<label class="permissionlbl">&nbsp;&nbsp;<a
							style="color: #333333; cursor: default"><?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_PRIVATE') ?></a>
						</label> <br /> 
						<a id="settings_desc" class="whatsthis" onMouseover="ddrivetip('The default settings is set to viewable by all agents. Click on What&lsquo;s this to change.');hover_dd()" onMouseout="hideddrivetip();hover_dd()"href="javascript:showSettings()" data="general" class="custom"><?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM_WHATSTHIS') ?></a>
					</div>
				</div>
				<div class="pocket-form popsnotes">
					<div class="c480" >
						<h2><?php echo JText::_('COM_POPS_TERMS_DESCRIPTION') ?></h2>
						<textarea 
							id="jform_desc" 
							name="jform[desc]" 
                            onMouseover="ddrivetip('Enter POPs&trade; description. This will be viewed by other agents based on Permission settings');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()" class="required text-input text-area"></textarea>
					</div>
				</div>
				<div class="pocket-form">
					<h2><?php echo JText::_('COM_POPS_TERMS_PHOTO_GALLERY') ?></h2>
					<!-- Upload Form Starts Here -->
					<div id="a_photo" onMouseover="ddrivetip('Add a photo or allow system to use default image');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
						<div id="try_hard_upload" class="text-link"><?php echo JText::_('COM_POPS_TERMS_PHOTO_GALLERY_UPLOAD_LINK') ?></div>
					</div>
					<!-- Upload Form Ends Here -->
						<ul class="gallery-addphoto" id="imagegallery">
						<?php
						if(count($images) > 0):
						foreach($images as $image):
						echo "<li class='nah'> <img style=\"position: absolute; width: 27px; display:none\" src=\"images/delete-icon.png\" /> <a class=\"add-photo\"><img src=\"".trim($image->image)."\" height=\"100\" width=\"150\" /></a> <input type=\"hidden\" name=\"jform_image[]\" value=\"".$image->image."\" /></li>";
						endforeach;
						endif;
						?>
					</ul>
					<img id="imageloading"
							style="position: relative; margin-left: 5px; margin-top:20px; display: none"
							src="<?php echo $this->baseurl."/images/ajax-loader-big.gif"?>" />
					<div id="image_error" class="clear-float error_msg" style="display:none; font-size:11px; margin-bottom:10px"><?php echo JText::_('COM_NRDS_FORM_ERROR_UPLOAD_IMAGE') ?></div>	
					<div id="image_error_filesize" class="clear-float error_msg" style="display:none; font-size:11px; margin-bottom:10px"><?php echo JText::_('COM_NRDS_FORM_ERROR_UPLOAD_IMAGE_FILESIZE') ?></div>	
					<div id="image_error_totalfilesize" class="clear-float error_msg" style="display:none; font-size:11px; margin-bottom:10px"><?php echo JText::_('COM_NRDS_FORM_ERROR_UPLOAD_IMAGE_TOTALFILESIZE') ?></div>	
					<div id="image_error_filenumber" class="clear-float error_msg" style="display:none; font-size:11px; margin-bottom:10px">Please upload 20 files per batch</div>	

					<div id="savebuttons" style="clear: both"
						class="<?php echo (isset($_GET['saved']))? "gray": ""?>">
						<input name="submitformpops" id="submitformbuttonx" value="<?php echo JText::_('COM_NRDS_FORM_SAVEPOPS') ?>" type="button" class="sub_button button gradient-blue validate" /> 
            
						<input name="" value="<?php echo JText::_('COM_NRDS_FORMCANCEL') ?>" type="button"  onClick="javascript:location.href = '<?php echo JRoute::_("index.php?option=com_userprofile&task=profile"); ?>';"  class="button gradient-gray cancel" />
                          <img id="loading-image_custom_question3" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
					</div>
					<?php echo JHtml::_('form.token');?>
					<div class="clear-float"
						style="margin-top: 20px; margin-bottom: 40px;"></div>
				</div>
			</form>
		</div>
		
		<div id="cropimage" style="display:none;">
			<div id="images_thumbs">
				
			</div>
			<div id="progressbox" style="display:table;clear:both;">
				<div id="progressbar"></div>
				<div id="statustxt">0%</div>
			</div>
			<div style="clear:left"><input type="button" class="submitform crop_image" value="<?php echo JText::_('COM_USERPROF_CROP') ?>"/></div><br/>
            <div id="image_to_crop_container"><img id="image_to_crop" class="img_mobile"></div>
			    <form action="<?php echo JRoute::_("index.php?option=com_propertylisting&task=crop")?>" method="post" id="cropform">
				<input type="hidden" name="x1" value="0" /> 
                <input type="hidden" name="y1" value="0" /> 
                <input type="hidden" name="x2" value="210" /> 
                <input type="hidden" name="y2" value="128" /> 
                <input type="hidden" id="imagetopost" name="src" value="" />
				<input id="resized_img" type="hidden" name="resized" value="" />
				<input type="hidden" name="origheight" value="" />
				<input type="hidden" name="origwidth" value="" />
				<input type="button" class="submitform crop_image" value="<?php echo JText::_('COM_USERPROF_CROP') ?>"/>
			</form>
		</div>
	</div>
	<!-- end wide-content -->
	<?php
	include( 'includes/sidebar_left.php' );
	?>
</div>
<div id="loremipsum" style="display:none; line-height:18px"><p>When set to private, only the owner can view the POPs&trade;. Private POPs&trade; will still appear on Search and Buyer matches but the information is limited only to the zip code and price.</p></div>
<div id="settingsform" style="display:none" class="whats_style">
		<form id="pocket_settingx"  method="post" class="oooooform-validateooooo form-horizontal">
			<input type="hidden" name="jform[form]" value="pocket_settingx" />
			<div class="setting">
				<ul>
					<li class="setting-list" style="margin-left:5px">
						<input type="radio" id="jform_setting1" name="jform[setting]" value="1"  checked="true"/>
						<label> <?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM_OPT_ALL') ?></label>
						<div class="clear-float"></div>
					</li>
					<!--
					<li class="setting-list">
						<input type="radio" id="jform_setting2" name="jform[setting]" value="2" />
						<label>My AgentBridge Network </label>
						<div class="clear-float"></div>
					</li>
					-->
					<li class="setting-list" style="margin-left:5px">
						<input type="radio" id="jform_setting7" name="jform[setting]" value="7" />
						<label> <?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM_OPT_OUTSIDE_STATE') ?></label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list" style="margin-left:5px">
						<div class="left">
							<input type="radio" id="jform_setting3" name="jform[setting]" value="3" />
							<label> <?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM_OPT_VOLUME') ?> </label>
						</div>
						<span class="c90">
							<input id="jform_pricesetting1" name="jform[psetting1]" type="text"  value="$20,000,000" class="text-input pricevalue" style="width:110px;" />
						</span>
						<div class="left">
						
						</div>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list" style="margin-left:5px">
                    	<div class="left">
							<input type="radio" id="jform_setting4" name="jform[setting]" value="4" />
							<label>  <?php echo JText::_('COM_POPS_TERMS_PERMISSION_SETTINGS_OPT_CUSTOM_OPT_AVERAGE') ?> </label>
                        </div>
						<span class="c90 left">
							<input id="jform_pricesetting2" name="jform[psetting2]" type="text"  value="$1,000,000" class="text-input pricevalue" style="width:125px;" />
						</span>
						<div class="clear-float"></div>
					</li>
					<!--<li class="setting-list">
						<input type="radio" id="jform_setting5" name="jform[setting]" value="5" />
						<label>Any AgentBridge member with total sides of more than</label>
						<span class="c90" style="margin-top:10px;">
							<input id="jform_sidesetting" name="jform[psetting3]" type="text"  value="25" class="text-input pricevalue_2" style="margin-top:8px;width:125px;" />
						</span>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting7" name="jform[setting]" value="7" />
						<label>All AgentBridge members in my state.</label>
						<div class="clear-float"></div>
					</li>
					<li class="setting-list">
						<input type="radio" id="jform_setting8" name="jform[setting]" value="8" />
						<label>All AgentBridge members in my country.</label>
						<div class="clear-float"></div>
					</li>-->
				</ul>
				<div class="clear-float"></div>
				<div class="setting-list right">
					<input 
						value="<?php echo JText::_('COM_NRDS_FORM_DONE') ?>" 
						type="button" 
						class="setting-list right button gradient-blue validate" 
						style="padding-top:5px; width:80px; margin-right:20px"
						onclick="closeSettingsDialog()"/>
				</div>
				<input type="hidden" name="jform[country]" id="jform_country" value="<?php echo $this->countryId; ?>"/>
				<input type="hidden" name="jform[currency]" id="jform_currency" value="<?php echo $this->countryCurrency; ?>"/>
				<div class="practically-hidden"><input id="submitsettings" type="submit"/></div>
			</div>
		</form>
		<input type="hidden" name="country" id="country" value="<?php echo $this->countryIso; ?>"/>
</div>
<div style="width:1px; height:1px overflow:hidden">
<input type="file" name="newimagefile[]" id="inputfile"
						style="opacity: 0" multiple />
</div>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/imagearea/scripts/jquery.imgareaselect.js"></script>

<script>
	function closeSettingsDialog() {
		jQuery("#settingsform").dialog("close");
	}
	jQuery(".custom").click(function(){
		console.log(jQuery(this));
	});
	
	function disablePermission () {

		jQuery('#jform_setting1').removeAttr('checked');
		jQuery('#jform_setting1').attr('checked', false);
	
	}
	
	var tablecols = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','property_type','sub_type','unit_sqft','year_built','pool_spa','cap_rate','listing_class','parking_ratio','ceiling_height','room_count','type_lease','type_lease2','available_sqft','lot_sqft','lot_size','bldg_sqft','bldg_type','description'];
	var formnames = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','ptype','stype','unitsqft','yearbuilt','poolspa','cap','class','parking','ceili','roomcount','typelease','typelease2','available','lotsqft','lotsize','bldgsqft','bldgtype','desc'];
	var array = <?php echo json_encode($this->data)?>;

</script>

<!-- Updated Dialog Country -->
<div id="changecountry" style="display: none; padding:10px">
	<?php
		$cl_list = $this->getCountryLangs;
		$fav_countries = array(
		       					"Canada",
		       					"United States",
		       					"United Kingdom",
		       					"France",
		       					"China",
		       					"Germany",
		       					"Australia",
		       					"Ireland",
		       					"Monaco",
		       					"Switzerland",
		       					"Spain");
	?>
		<div id="country_modal_left">
			<?php 
			$i=0;
				  foreach($cl_list as $value){	
			?>
				<?php if(in_array($value->countries_name,$fav_countries)){?>
						<div class="<?php echo $value->countries_iso_code_2;?> choose-country"  id="<?php echo $value->countries_id?>">
				    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
				    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">
				    		<img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">
				    		<span style="line-height:1em"><?php echo $value->countries_name;?></span>			           	
		           		</div>
				<?php //unset($cl_list[$i]);
				 }  ?>	
				<?php $i++?>
			<?php } ?>
		</div>
		<div id="country_modal_right">	
			<div class="tabs">
			    <ul class="tab-links">
			        <?php 
							    $capital_letter="A";			    
							    $once=1;
							    $x=1;
			        ?>
			        <?php foreach($cl_list as $value){	?>
			        			<?php 	if($capital_letter!=(substr($value->countries_name,0,1)) ){
				        					$capital_letter=substr($value->countries_name, 0, 1);
				        					$once=1;
				        				} else {
				        					$once=0;
				        				}

				        				if($once==1 || $x==1) { ?>

				        			<li><a href="#country_<?php echo $capital_letter; ?>" class="country_<?php echo $capital_letter; ?>" ><?php echo $capital_letter; ?></a></li>

				        		<?php $x++; }	?>
			        <?php }?>
			    </ul>
			</div>
			 <div class="tab-content" >
			 		<?php 
						    $capital_letter="A";			    
						    $once=1;
						    $x=1;
						    $def_Active="country_active";
						    $t=1;
			        ?>
			        <?php foreach($cl_list as $key=>$value){	?>


			        <?php if($capital_letter!=(substr($value->countries_name,0,1))){ echo $t!=1 ? "</div>":"";$once=1;$t=1;}  ?>  
			        <?php $capital_letter=substr($value->countries_name, 0, 1);?>
			        <?php if($once==1){ $once=0;?>
			        	<?php echo $x==1 ? "":"</div>";?>
			        	<div id="country_<?php echo $capital_letter; ?>" class="tab <?php echo $def_Active; ?>">
			        <?php  } $def_Active=""; ?>

	        			<?php if($t==1 || $t==10 || $t==20){ ?>
	        					<?php echo $t==1 ? "":"</div>";?>
				        		<div style="display:inline-block;vertical-align:top;width:150px">
				        <?php   } 	?>

				        	<?php if($capital_letter==(substr($value->countries_name,0,1))){?>
			            		<div class="<?php echo $value->countries_iso_code_2;?> choose-country"  id="<?php echo $value->countries_id?>">
						    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
						    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">
						    	<!--<img class="ctry-flag-48" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lng-48.png">-->
						    		<img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">
						    		<?php echo $value->countries_name;?>
				           		</div>
			           		<?php  } ?>
			       		 <?php $t++; ?>
				    
				    <?php $x++; } ?>
			 </div>				
	</div>
</div>
<style>

.progressbox_small {
	border: 1px solid #0099CC;
	padding: 1px; 
	position:relative;
	width:100px;
	height: 70px;
	border-radius: 3px;
	margin: 10px 0;
	display:none;
	text-align:left;
}

.progressbar_small .progressbar {
	height:100%;
	border-radius: 3px;
	background-color:#009BD0;
	width:1%;
}

.progressbar_small .progressbar .statustxt {
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #000000;
}

#progressbox {
	border: 1px solid #0099CC;
	padding: 1px; 
	position:relative;
	width:400px;
	border-radius: 3px;
	margin: 10px 0;
	display:none;
	text-align:left;
}
#progressbar {
	height:20px;
	border-radius: 3px;
	background-color: #009BD0;
	width:1%;
}
#statustxt {
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #000000;
}
</style>
<!-- Updated Dialog Country -->