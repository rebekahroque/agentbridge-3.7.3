<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
jimport('tcpdf.library.tcpdf') or require_once JPATH_LIBRARIES . '/tcpdf/library/tcpdf.php';
require_once JPATH_LIBRARIES . '/tcpdf/library/config/lang/eng.php';
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
*/
class PropertyListingController extends JControllerLegacy
{
	/**
	 * Method to display the view
	 *
	 * @access    public
	 */
	
	private $arr_previous_matches;
	 
	function display($cachable = false, $urlparams = false)
	{		
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$user =& JFactory::getUser($_SESSION['user_id']);
		$task = JRequest::getVar('task');
		$error = false;
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$propertylisting_model = $this->getModel($this->getName());
		$view = &$this->getView($this->getName(), 'html');
		if(isset($_GET['uid'])) {
			$uid = $_GET['uid'];
		} else {
			$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
		}
		$user_info = $model2->get_profile_info($uid);
		$countbuyer = $propertylisting_model->count_user_buyers();
		$user_info->countbuyer = $countbuyer;
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
        $view->assignRef('this_lang_tag', JFactory::getUser()->currLanguage);
		$view->assignRef('user_info', $user_info);
		if(empty($uid)) $application->redirect('index.php?illegal=1');
		$network = array_map(function($obj){ return $obj->other_user_id; }, $model2->get_network($uid, 1));
		if(!empty($_POST['jform']['form'])):
			// PREPARE POST VARIABLES
			foreach($_POST['jform'] as $key=>$value){
				if(is_array($value)){
					foreach($value as $k=>$v){
						$data[$key][$k] = trim(addslashes($v));
					}
				}
				else{
					$data[$key] = trim(addslashes($value));
				}
			}
			#extract($data);
		endif;
		// SAVE POCKET LISTING
		if($_POST['jform']['form']=="add_pocket"):
		elseif($_POST['jform']['form']=="pocket_setting"):
			//$settings = $propertylisting_model->getPermissionChoices($user->id);
			$settings = $propertylisting_model->getPermissionChoices($_GET['lID']);
			if(empty($settings[0]->permission_id)):
				//$ret = $propertylisting_model->insertPermissionSetting($user->id, $data);
				$ret = $propertylisting_model->insertPermissionSetting( $data );
				if($ret):
					JFactory::getApplication()->enqueueMessage('POP&#8482;s Settings Saved!');
				endif;
			else:
				//$result = $propertylisting_model->updatePermissionSetting($user->id, $data);
				$result = $propertylisting_model->updatePermissionSetting( $data );
			endif;
		endif;
		if($task=="crop"){
			if ($_REQUEST['x2']==$_REQUEST['x1'] || ($_REQUEST['x2']=='' && $_REQUEST['x1']=='') ) {
				$x1=0;
				$x2=$_REQUEST['origwidth'];
				$y1=0;
				$y2=$_REQUEST['origheight'];
			} else {
				$x1=$_REQUEST['x1'];
				$x2=$_REQUEST['x2'];
				$y1=$_REQUEST['y1'];
				$y2=$_REQUEST['y2'];
			}
			$targ_w = $x2-$x1;
			$targ_h = $y2-$y1;
			$width = $_REQUEST['origwidth'];
			$height = $_REQUEST['origheight'];
			$jpeg_quality = 90;
			$src = $_REQUEST['src'];
			if($_REQUEST['resized']){
				if($_REQUEST['resized']!="mobile"){
					if($_REQUEST['origwidth'] > $_REQUEST['origheight']){
						$width=800;
						$height=round($width*$_REQUEST['origheight']/$_REQUEST['origwidth']);
					} else{
						$height=800;
						$width=round($height*$_REQUEST['origwidth']/$_REQUEST['origheight']);
					}
				} else {
					if($_REQUEST['origwidth'] > $_REQUEST['origheight']){
						$width=$_REQUEST['origwidth'];
						$height=round($width*$_REQUEST['origheight']/$_REQUEST['origwidth']);
					}
					else{
						$height=$_REQUEST['origheight'];
						$width=round($height*$_REQUEST['origwidth']/$_REQUEST['origheight']);
					}
				}
			}
			$imagename = $this->stripImageName($src);
			$filepath = "uploads/".$imagename;
			$getimagesize = getimagesize($filepath);
			$mime_type = $getimagesize['mime'];
			//$mime_type   = $this->getMimeType($src);
			switch($mime_type) {
				case "image/jpg":
				case "image/jpeg":
					$jpeg_quality = 90;
					$img_r = imagecreatefromjpeg($filepath);
					$images_fin = imagecreatetruecolor($width, $height);
					$xdaw = ImagesX($img_r);
					$ydaw = ImagesY($img_r);
					ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
					$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
					imagecopyresampled($dst_r,$images_fin,0,0,$x1,$y1,$targ_w,$targ_h,$targ_w,$targ_h);
					$base = new JConfig();
					$time = str_replace('.', '-', microtime(true));
					$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
					imagejpeg($dst_r,$destImage,$jpeg_quality);
					imagedestroy($dst_r);
					//Thumbnails creation
					$image_p = imagecreatetruecolor(60, 71);
					$image = imagecreatefromjpeg($destImage);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
					$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
					imagejpeg($image_p,$destImageT,$jpeg_quality);
					imagedestroy($image_p);
					echo JUri::base()."uploads/".JFactory::getUser()->id.$time."-".basename($src);
					break;
				case "image/png":
					$png_quality = 9;
					$img_r = imagecreatefrompng($filepath);
					$images_fin = ImageCreateTrueColor($width, $height);
					$xdaw = ImagesX($img_r);
					$ydaw = ImagesY($img_r);
					ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
					$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
					imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],
					$targ_w,$targ_h,$targ_w,$targ_h);
					$base = new JConfig();
					$time = str_replace('.', '-', microtime(true));
					$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
					imagepng($dst_r,$destImage,$png_quality);
					imagedestroy($dst_r);
					//Thumbnails creation
					$image_p = imagecreatetruecolor(60, 71);
					$image = imagecreatefrompng($destImage);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
					$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
					imagepng($image_p,$destImageT,$png_quality);
					imagedestroy($image_p);
					echo JUri::base()."uploads/".JFactory::getUser()->id.$time."-".basename($src);
					break;
				case "image/gif":
					$img_r = imagecreatefromgif($filepath);
					$images_fin = ImageCreateTrueColor($width, $height);
					$xdaw = ImagesX($img_r);
					$ydaw = ImagesY($img_r);
					ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
					$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
					imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],
					$targ_w,$targ_h,$targ_w,$targ_h);
					$base = new JConfig();
					$time = str_replace('.', '-', microtime(true));
					$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
					imagegif($dst_r,$destImage);
					imagedestroy($dst_r);
					//Thumbnails creation
					$image_p = imagecreatetruecolor(60, 71);
					$image = imagecreatefromgif($destImage);
					imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
					$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
					imagegif($image_p,$destImageT);
					imagedestroy($image_p);
					echo JUri::base()."uploads/".JFactory::getUser()->id.$time."-".basename($src);
					break;
			}
			exit;
		}
		$user =& JFactory::getUser($uid);
		$task = JRequest::getVar('task');
		$property_type = JRequest::getVar('ptype');
		$property_subtype = JRequest::getVar('stype');
		$ptype = $propertylisting_model->getAllPropertyTypes();
		$zone = $propertylisting_model->getAllStates();
		$listing = $propertylisting_model->getAllListings($user->id);
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$ex_rates = $propertylisting_model->getExchangeRates_indi();
		$previous_year = date("Y", strtotime("-1 year"));
		while(intval($previous_year) >= 2012) {
			if($userDetails->{"verified_".$previous_year}) {
				$user_previous_year_volume = $userDetails->{"volume_".$previous_year};
				$user_previous_year_ave_price = $userDetails->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}

		$langValues = $this->checkLangFiles();
		

		foreach($listing as $indi) {
		$indi->newallow = "";
		$userid = JFactory::getUser()->id;
		$user_currency = $indi->currency;

		

		if (empty($_GET['uid'])|| $_GET['uid']==$userid ) {
			$indi->newallow=1;
		} else {
			if($indi->selected_permission==1) {
				$indi->newallow=1;
			} else if ($indi->selected_permission==3) {
				if($previous_year == 2012 && !$userDetails->{"verified_".$previous_year}) {
					$indi->newallow = "";
				} else {
					$previous_year_volume_minimum = $indi->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum * $item_user_currency_rate;
					} else {
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum;
					}
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_volume_converted = $user_previous_year_volume * $user_currency_rate;
					} else {
						$user_previous_year_volume_converted = $user_previous_year_volume;
					}
					#echo $user_previous_year_volume_converted . " " . $previous_year_volume_minimum_converted;
					if($user_previous_year_volume_converted <= $previous_year_volume_minimum_converted) {
						$indi->newallow = "";
					} else {
						$indi->newallow = 1;
					}
				}
				#$indi->newallow = $propertylisting_model->checkuserVolume($userid, $_GET['uid'], $indi->listing_id);
			} else if ($indi->selected_permission==4) {
				if($previous_year == 2012 && !$userDetails->{"ave_price_".$previous_year}) {
					$indi->newallow = "";
				} else {
					$previous_year_ave_price_minimum = $indi->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum * $item_user_currency_rate;
					} else {
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum;
					}
					#echo $user_previous_year_ave_price_converted . " " . $previous_year_ave_price_minimum_converted . "<br />";
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price * $user_currency_rate;
					} else {
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price;
					}
					if($user_previous_year_ave_price_converted <= $previous_year_ave_price_minimum_converted) {
						#echo "here";
						$indi->newallow = "";
					} else {
						$indi->newallow = 1;
					}
				}	
				#$indi->newallow = $propertylisting_model->checkuserPrice($userid, $_GET['uid'],$indi->listing_id);
			} else if ($indi->selected_permission==5) {
				$indi->newallow = $propertylisting_model->checkuserSides($userid, $_GET['uid'],$indi->listing_id);
			} else if ($indi->selected_permission==6) {
				$indi->newallow = $propertylisting_model->checkuserState($userid, $_GET['uid']);
			} else if ($indi->selected_permission==7) {
				$indi->newallow = $propertylisting_model->checkuserCountry($userid, $_GET['uid']);
			} else if ($indi->selected_permission==0) {
				$indi->newallow = $propertylisting_model->checkuserAccess($userid, $_GET['uid'], $indi->listing_id);
			} else {
				$indi->newallow=1;
				}
			}
		$indi->ra=$propertylisting_model->checkpermissionValue($userid, $_GET['uid'], $indi->listing_id);

			$listing_indi = $indi;

			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $propertylisting_model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$listing_indi->pops_feats = $propertylisting_model->getPOPsDetails($indi,$listing_indi->sqM,$indi->sub_type,$langValues);
		}
		#$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		#$ex_rates = $propertylisting_model->getExchangeRates_indi();
		foreach($listing as $items) {
			if($items->user_id != JFactory::getUser()->id){
				$ex_rates_con = $ex_rates->rates->{$items->currency};
				$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
				if($items->currency!=$userDetails->currency){
					//$items->price1=$propertylisting_model->getExchangeRates($items->price1,$userDetails->currency,$items->currency);
					if($items->currency=="USD"){									
						$items->price1=$items->price1 * $ex_rates_can;
					} else {
						$items->price1=($items->price1 / $ex_rates_con)*$ex_rates_can;
					}
					if($items->price2){
						//$items->price2=$propertylisting_model->getExchangeRates($items->price2,$userDetails->currency,$items->currency);
						if($items->currency=="USD"){									
							$items->price2=$items->price2 * $ex_rates_can;
						} else {
							$items->price2=($items->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}
				}
				$items->currency = $userDetails->currency;
				$items->symbol = $userDetails->symbol;
			}
		}
		$_SESSION['listing_count'] = count($listing);
		$closedlisting = $propertylisting_model->getClosedListings($user->id);
		foreach($closedlisting as $items) {
			if($items->user_id != JFactory::getUser()->id){
				$ex_rates_con = $ex_rates->rates->{$items->currency};
				$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
				if($items->currency!=$userDetails->currency){
					//$items->price1=$propertylisting_model->getExchangeRates($items->price1,$userDetails->currency,$items->currency);
					if($items->currency=="USD"){									
						$items->price1=$items->price1 * $ex_rates_can;
					} else {
						$items->price1=($items->price1 / $ex_rates_con)*$ex_rates_can;
					}
					if($items->price2){
						//$items->price2=$propertylisting_model->getExchangeRates($items->price2,$userDetails->currency,$items->currency);
						if($items->currency=="USD"){									
							$items->price2=$items->price2 * $ex_rates_can;
						} else {
							$items->price2=($items->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}
				}
				$items->currency = $userDetails->currency;
				$items->symbol = $userDetails->symbol;
			}
		}
		$inprogresslisting = $propertylisting_model->getInProgress($user->id);
		foreach($inprogresslisting as $items) {
			if($items->user_id != JFactory::getUser()->id){
				$ex_rates_con = $ex_rates->rates->{$items->currency};
				$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
				if($items->currency!=$userDetails->currency){
					//$items->price1=$propertylisting_model->getExchangeRates($items->price1,$userDetails->currency,$items->currency);
					if($items->currency=="USD"){									
						$items->price1=$items->price1 * $ex_rates_can;
					} else {
						$items->price1=($items->price1 / $ex_rates_con)*$ex_rates_can;
					}
					if($items->price2){
						//$items->price2=$propertylisting_model->getExchangeRates($items->price2,$userDetails->currency,$items->currency);
						if($items->currency=="USD"){									
							$items->price2=$items->price2 * $ex_rates_can;
						} else {
							$items->price2=($items->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}
				}
				$items->currency = $userDetails->currency;
				$items->symbol = $userDetails->symbol;
			}
		}
		//$setting = $propertylisting_model->getPermissionChoices($user->id);
		$setting = $propertylisting_model->getPermissionChoices($_GET['lID']);
		$individual = $propertylisting_model->getSelectedListing($_GET['lID'], true);
		if(!empty($individual) && $individual[0]->user_id != JFactory::getUser()->id) {
			$propertylisting_model->logActivity($individual);
		}
		// GET SELECTED PROPERTY TYPE
		$selected_ptype = !empty($property_type) ? $property_type: $individual[0]->property_type;
		// GET SELECTED PROPERTY SUBTYPE
		$selected_stype = !empty($property_subtype) ? $property_subtype: $individual[0]->sub_type;
		$subtypes = $propertylisting_model->getSubTypes($selected_ptype);
		$country = $propertylisting_model->getCountry($user->id);
		$images = array();
		$images_bw = array();
		$tempview = array();
		foreach($listing as $list) {
			$list->listing_id = $list->lid;
			$images[$list->listing_id] = (count($propertylisting_model->get_property_image($list->listing_id))) ? $propertylisting_model->get_property_image($list->listing_id) : $propertylisting_model->get_buyer_image($list->property_type, $list->sub_type);
			$images_bw[$list->listing_id] = $propertylisting_model->get_buyer_image($list->property_type, $list->sub_type, true);
			//$tempview[$list->listing_id] = $propertylisting_model->getViews($list->listing_id);
		}
		#exit;
		// GET DISPLAY TEMPLATE

		//$getMeasurement = $propertylisting_model->getSqMeasureByCountry(73);
		//echo "<pre>"; var_dump($getMeasurement); die();
		
		$getCountryLangs = $this->getCountryLangs();
		$getCountryLangsInitial = $this->getCountryDataByID($country[0]->country);
		if(!empty($task)) $tpl = $data['task'];
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		$view->assignRef( 'getCountryLangs', $getCountryLangs );
		$view->assignRef( 'ptype', $ptype );
		$view->assignRef( 'zone', $zone );
		$view->assignRef( 'listing', $listing );
		$view->assignRef( 'data', $individual[0]);
		$view->assignRef( 'property_type', $selected_ptype );
		$view->assignRef( 'sub_type', $selected_stype );
		$view->assignRef( 'sub_types', $subtypes );
		$view->assignRef( 'setting', $setting[0] );
		$view->assignRef( 'countryCurrency', $country[0]->currency);
		$view->assignRef( 'countryCurrSymbol', $country[0]->symbol);
		$view->assignRef( 'countryId', $country[0]->country);
		$view->assignRef( 'countryIso', $country[0]->countries_iso_code_2);
		$view->assignRef( 'inProgress', $inprogresslisting);
		$view->assignRef( 'closed', $closedlisting);
		$view->assignRef( 'images', $images );
		$view->assignRef( 'images_bw', $images_bw );
		$view->assignRef( 'network', $network);
		$view->assignRef( 'tempview', $tempview );
		if($task == 'view') {
			if(isset($_GET['uid']) && JFactory::getUser()->id != $_GET['uid']) {
				$own_group_id = $model2->get_user_group(JFactory::getUser()->id);
				$view_group_id = $model2->get_user_group($_GET['uid']);
				
				$admin_group = array(7,8,11,12);
				
				if(in_array($view_group_id->group_id, $admin_group) && !in_array($own_group_id->group_id, $admin_group)) {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
				}
			}
			
			#echo "<pre>", print_r($listing), "</pre>"; 
			if(!count($listing)) {
				$customsearchmodel = JPATH_ROOT.'/components/'.'com_customsearch/models';
				JModelLegacy::addIncludePath( $customsearchmodel );
				$model3 =& JModelLegacy::getInstance('Searchlisting', 'CustomSearchModel');
				$searchZip = $user_info->zip;
				#echo $searchZip;
				$featured_result = $model3->searchDB($searchZip, 1, false, null, "desc", 3);
				#echo "<pre>", print_r($featured_result), "</pre>";
			}
			$view->display($this->getTask());
		} else {
			// PASS ALL VARIABLE TO TEMPLATE
			parent::display($tpl);
		}
	}
	function getMonacoCities(){
		$model = $this->getModel($this->getName());
		$datas = $model->getMCcities();
		$conv_datas = array();
		foreach ($datas as $value) {
			$conv_datas[] = array("city" => $value);
		}
		echo json_encode($datas);
	}
	function getCountryCities(){
		$country_id = $_POST['id_c'];
		$postcode = $_POST['postcode'];
		$model = $this->getModel($this->getName());
		$datas = $model->getCCities($country_id,$postcode);
		$conv_datas = array();
		foreach ($datas as $value) {
			$conv_datas[] = array("city" => $value);
		}
		echo json_encode($datas);
	}
	function changeCountryData(){
		$country = $_POST['country'];
		$datas = $this->getCountryDataByID($country);
		echo json_encode($datas);
	}
	function expiredPOPs(){
		$listing_id = $_POST['listing_id'];
		$model = $this->getModel($this->getName());
		if($model->expiredPOPDb($listing_id)){
			echo "Success";
		} else {
			echo "Failed";
		}
	}	
	function soldPOPs(){
		$listing_id = $_POST['listing_id'];
		$model = $this->getModel($this->getName());
		if($model->soldPOPDb($listing_id)){
			echo "Success";
		} else {
			echo "Failed";
		}
	}
	function unSoldPOPs(){
		$listing_id = $_POST['listing_id'];
		$model = $this->getModel($this->getName());
		if($model->unSoldPOPDb($listing_id)){
			echo "Success";
		} else {
			echo "Failed";
		}
	}
	function closenonpremium() {
		$model = $this->getModel($this->getName());
		$ref_id   = JRequest::getVar('ref_id');
		$grosscom = JRequest::getVar('grosscom') ;
		$date 	  = date("Y - m - d");
		$model->updatereferraltoclose( JRequest::getVar('ref_id') );
		$files=array();
		echo ($this->generate_payment_method($ref_id, $date, $grosscom));
		$files['payment_method'] = $this->writepdf_pay_method($this->generate_payment_method($ref_id, $date, $grosscom));
		$model->send_closed_referral($ref_id,$grosscom,$files,JFactory::getUser()->id);
		$referrals = $model->get_refferals($ref_id);
		//$model->insert_activity($referrals[0]->agent_b, 19, $ref_id, $referrals[0]->agent_a, date('Y-m-d H:i:s',time()));
		//$model->insert_activity($referrals[0]->agent_a, 19, $ref_id ,$referrals[0]->agent_b, date('Y-m-d H:i:s',time()));
	}
	function resetNewByBuyerId(){
		$buyer_id = $_POST['buyerId'];
		$model = $this->getModel($this->getName());
		$model->resetBuyerListings($buyer_id, 1);
		/*if($model->resetBuyerListings($buyer_id, 1)){
			echo "success";
		} else{
			echo "fail";
		}*/
	}
	function getAllPopsBuyers(){
		$model = $this->getModel($this->getName());
		$counts=0;
		$abuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"A Buyer"'));
		foreach($abuyers as $key =>$abuyer) {
			$abuyers[$key]->listingcount = $this->countlisting($abuyer->buyer_id);
			$counts+=$this->countlisting($abuyer->buyer_id);
		}
		$bbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"B Buyer"'));
		foreach($bbuyers as $key =>$bbuyer) {
			$bbuyers[$key]->listingcount = $this->countlisting($bbuyer->buyer_id);
			$counts+=$this->countlisting($bbuyer->buyer_id);
		}
		$cbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"C Buyer"'));
		foreach($cbuyers as $key =>$cbuyer) {
			$cbuyers[$key]->listingcount = $this->countlisting($cbuyer->buyer_id);
			$counts+=$this->countlisting($cbuyer->buyer_id);
		}
		return $counts;
	}
	function buyers(){

		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);

		$model = $this->getModel($this->getName());
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$user_info = $model2->get_profile_info(JFactory::getUser()->id);
		$thisuserinfo = $model2->get_user_registration(JFactory::getUser()->id);
		$d_format = $this->getCountryDataByID($thisuserinfo->country)->dateFormat;
		//$countbuyer = $model->count_user_buyers();
		//$buyers = $model->getBuyerPageDetails(JFactory::getUser()->id);
		//foreach($buyers as $key =>$buyer) {
		//	$buyers[$key]->listingcount = $this->countlisting($buyer->buyer_id);
		//}
		$langValues = $this->checkLangFiles();
		
		$abuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"A Buyer"'));
		foreach($abuyers as $key =>$abuyer) {
			//$abuyers[$key]->listingcount = $this->countlisting($abuyer->buyer_id);

			if($abuyer->reset_listing==0){
			//	var_dump($abuyer->reset_listing);
				$abuyers[$key]->listingcount = $this->countlisting($abuyer->buyer_id);
				$model->resetListingCount($abuyer->buyer_id,$abuyers[$key]->listingcount,$abuyer->reset_listing);
			} 

			$buyer_image = $model->get_buyer_image($abuyer->needs[0]->property_type, $abuyer->needs[0]->sub_type);
			$abuyers[$key]->last_update = $model->getBuyerActDate($d_format,$abuyer->buyer_id);
			$abuyers[$key]->buyer_image = $buyer_image;

			$listing_indi = $abuyer->needs[0];
			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$abuyers[$key]->buyer_feats = $model->getPOPsDetails($abuyer->needs[0],$listing_indi->sqM,$abuyer->needs[0]->sub_type,$langValues);
		}
		$bbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"B Buyer"'));
		foreach($bbuyers as $key =>$bbuyer) {
			//$bbuyers[$key]->listingcount = $this->countlisting($bbuyer->buyer_id);
			if($bbuyer->reset_listing==0){
			//	var_dump($abuyer->reset_listing);
				$bbuyers[$key]->listingcount = $this->countlisting($bbuyer->buyer_id);
				$model->resetListingCount($bbuyer->buyer_id,$bbuyers[$key]->listingcount,$bbuyer->reset_listing);
			} 
			$buyer_image = $model->get_buyer_image($bbuyer->needs[0]->property_type, $bbuyer->needs[0]->sub_type);
			$bbuyers[$key]->last_update = $model->getBuyerActDate($d_format,$bbuyer->buyer_id);
			$bbuyers[$key]->buyer_image = $buyer_image;

			$listing_indi = $bbuyer->needs[0];
			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$bbuyers[$key]->buyer_feats = $model->getPOPsDetails($bbuyer->needs[0],$listing_indi->sqM,$bbuyer->needs[0]->sub_type,$langValues);
		}
		$cbuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"C Buyer"'));
		foreach($cbuyers as $key =>$cbuyer) {
			//$cbuyers[$key]->listingcount = $this->countlisting($cbuyer->buyer_id);
			if($cbuyer->reset_listing==0){
			//	var_dump($abuyer->reset_listing);
				$cbuyers[$key]->listingcount = $this->countlisting($cbuyer->buyer_id);
				$model->resetListingCount($cbuyer->buyer_id,$cbuyers[$key]->listingcount,$cbuyer->reset_listing);
			}
			$buyer_image = $model->get_buyer_image($cbuyer->needs[0]->property_type, $cbuyer->needs[0]->sub_type);
			$cbuyers[$key]->last_update = $model->getBuyerActDate($d_format,$cbuyer->buyer_id);
			$cbuyers[$key]->buyer_image = $buyer_image;

			$listing_indi = $cbuyer->needs[0];
			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$cbuyers[$key]->buyer_feats = $model->getPOPsDetails($cbuyer->needs[0],$listing_indi->sqM,$cbuyer->needs[0]->sub_type,$langValues);
		}
		$ibuyers = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"Inactive"'));
		foreach($ibuyers as $key =>$ibuyer) {
			//$ibuyers[$key]->listingcount = $this->countlisting($ibuyer->buyer_id);
			if($ibuyer->reset_listing==0){
			//	var_dump($abuyer->reset_listing);
				$ibuyers[$key]->listingcount = $this->countlisting($ibuyer->buyer_id);
				$model->resetListingCount($ibuyer->buyer_id,$ibuyers[$key]->listingcount,$ibuyer->reset_listing);
			} 
			$buyer_image = $model->get_buyer_image($ibuyer->needs[0]->property_type, $ibuyer->needs[0]->sub_type);
			$ibuyers[$key]->last_update = $model->getBuyerActDate($d_format,$ibuyer->buyer_id);
			$ibuyers[$key]->buyer_image = $buyer_image;

			$listing_indi = $ibuyer->needs[0];
			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$ibuyers[$key]->buyer_feats = $model->getPOPsDetails($ibuyer->needs[0],$listing_indi->sqM,$ibuyer->needs[0]->sub_type,$langValues);
		}
	//	$searchreturn = $model->searchAll();
	//	$pocketlistings = array();
//
	//	foreach($searchreturn as $search){
	//		$listing = $model->get_listing(array('p.listing_id'=>$search->listing_id));
	//		array_push($pocketlistings, $listing);
	//	}
		/*if($_SESSION['viewed_buyerspage'] == 1){
			foreach($buyers as $buyer){
			//	$model->resetBuyerListings($buyer->buyer_id, 1);
			}
		}*/
		$view = &$this->getView($this->getName(), 'html');
		//$totalCount = $this->getAllPopsBuyers();
		$view->assignRef('d_format', $d_format);
		$view->assignRef('user_info', $user_info);
		//$view->assignRef('buyers', $buyers);
		$view->assignRef('a_buyers', $abuyers);
		$view->assignRef('b_buyers', $bbuyers);
		$view->assignRef('c_buyers', $cbuyers);
		$view->assignRef('i_buyers', $ibuyers);
	//	$view->assignRef('pocket_listings', $pocketlistings);
		$view->display($this->getTask());
	}
	function oneTimeHB(){
		ini_set('max_execution_time', 600);
		$db = JFactory::getDbo();
				$query3 = $db->getQuery(true);
				$query3->select('id');
				$query3->from('#__users');
				$db->setQuery($query3);
				$array_users = $db->loadColumn();
				//var_dump($array_users);
		$max_limit=100;
		foreach ($array_users as $key => $value) {
				if ( $max >= $max_limit ) {
			        // found a match in the file      
			        break; // will leave the foreach loop and also "break" the if statement
			    }
				$db = JFactory::getDbo();
				$query3 = $db->getQuery(true);
				$query3->select('COUNT(*)');
				$query3->from('#__buyer');
				$query3->where('agent_id = '.$value.' AND home_buyer = 1');
				$db->setQuery($query3);
				$home_buyer = $db->loadResult();
				if(!$home_buyer){
					echo $home_buyer."-".$value."<br>";
					$this->addHomeBuyerExist($value);
					$final_key++;
				}
				$max ++;
				echo "<br>".$max;
		}
		echo "<br>".$max."<br>";
		echo $final_key."<br>";
			/*	//check for homebuyer value
				$db = JFactory::getDbo();
				$query3 = $db->getQuery(true);
				$query3->select('COUNT(*)');
				$query3->from('#__buyer');
				$query3->where('agent_id = '.JFactory::getUser()->id.' AND home_buyer = 1');
				$db->setQuery($query3);
				$home_buyer = $db->loadResult();
				//$response = 0;
				if(!$home_buyer){
					$this->addHomeBuyerExist();
				} */
		$max_time = ini_get("max_execution_time");
		echo "<br>".$max_time;
	}
	public function addHomeBuyerExist($user_id){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model_up =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
	//	$model = $this->getModel($this->getName());
		$user_info = $model_up->get_user_registration($user_id);
		$data = array();
		$data['home_buyer'] = 1;
		$data['buyer_name'] = "My Home Market";
		$data['buyer_type'] = "A Buyer";
		$data['ptype'] = "Residential Purchase";
		$data['stype'] = "SFR";
		$data['bedroom'] = "2";
		$data['bathroom'] = "2";
		$data['bldgsqft']="1,000-1,999";
		$data["desc"]='Your "My Home Market" Buyer has been created based on your home market zip and average transaction value. This Buyer will automatically present to you all the matching POPs™ (Private, Off-market and Potential listings).';
			$data['user_id'] = $user_id;
			//$data['zip'] = $user_info->zip;
			$data['country'] = $user_info->country;
			if($user_info->country==222){
				preg_match('/\d/', $user_info->zip, $m, PREG_OFFSET_CAPTURE);
		    	if (sizeof($m))
		        	 $m[0][1]; // 24 in your example
		        $data['zip']=substr($user_info->zip, 0, ($m[0][1]+1));
		        $otherhalf_key = substr($user_info->zip, ($m[0][1]+1), strlen($user_info->zip));
		        if(is_numeric($otherhalf_key[0])){
		        	$data['zip'] = $data['zip'].$otherhalf_key[0];
		        }
			} else {
				$data['zip'] = $user_info->zip;
			}
			 if ($user_info->verified_2015==1){ 
				$ave_sales_price=$user_info->volume_2015/$user_info->sides_2015;
			 } else if ($user_info->verified_2014==1){ 
				$ave_sales_price=$user_info->volume_2014/$user_info->sides_2014;
			 } else { 
			 	$ave_sales_price=0;
			 } 	
			 $ave_sales_price = round($ave_sales_price);
			 $data['price1'] = $ave_sales_price*0.5;
			 $data['price2'] = $ave_sales_price*2.5;
			 $data['currency'] = $user_info->currency;
			if($ave_sales_price>0){
				$ch = curl_init( JURI::base()."index.php/component/propertylisting/?task=add_buyer&format=raw&webservice=1" );
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS,  $data  );
				$response = curl_exec( $ch );
				curl_close( $ch );
				echo "success";
			} else {
				echo "failed";
			}
	}

	function getBuyerAllMatchPOPs($buyerId){


	}
	
	function getBuyerAllMatchPOPsCount($buyer){
		$model = $this->getModel($this->getName());
		$popsMatches = $this->countlisting($buyer);
		return $popsMatches;
	}

	function buyers_count_raw(){
		$userId = $_GET['userId'];
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyerCount_opti($userId);	
		if(count($buyers) == 0) {
			$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
		}
		else {		
			//$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>array_slice($rows, 0, 5, true));
			$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>count($buyers));
		}
		echo json_encode($response);
	}

	function buyers_raw(){

		//$user = JFactory::getUser();
		////$session = JFactory::getSession();
		//$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
       // $language_tag = JFactory::getUser()->currLanguage;
        $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);


		$userId = $_GET['userId'];
		$limit_one = $_GET["limit_one"];
		$model = $this->getModel($this->getName());
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$langValues = $model->checkLangFiles();
		//$user_info = $model2->get_profile_info(JFactory::getUser()->id);
		//$countbuyer = $model->count_user_buyers();
	//	foreach($buyers as $key =>$buyer) {
	//		$buyers[$key]->listingcount = $this->countlisting($buyer->buyer_id);
	//	}
		$buyers = $model->getBuyerPageDetails_opti($userId,$limit_one);	

		//var_dump($buyers);
		foreach($buyers as $key =>$buyer) {
			//$abuyers[$key]->listingcount = $this->countlisting($abuyer->buyer_id);
			$buyer_image = $model->get_buyer_image($buyer['property_type'], $buyer['sub_type']);
			$buyers[$key]['buyer_image'] = $buyer_image;

			$listing_indi = $buyer;
			$listing_indi->sqM = "";
			if($listing_indi->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($listing_indi->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing_indi->bldg_sqft){
						if($listing_indi->bldg_sqft == $value[1]){
							$listing_indi->bldg_sqft=$value[0];
						}
					}
					if($listing_indi->available_sqft){
						if($listing_indi->available_sqft == $value[1]){
							$listing_indi->available_sqft=$value[0];
						}
						
					}
					if($listing_indi->unit_sqft){
						if($listing_indi->unit_sqft == $value[1]){
							$listing_indi->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing_indi->lot_sqft){
						if($listing_indi->lot_sqft == $value[1]){
							$listing_indi->lot_sqft=$value[0];
						}
						
					}
					if($listing_indi->lot_size){
						if($listing_indi->lot_size == $value[1]){
							$listing_indi->lot_size=$value[0];
						}
						
					}
				}

				$listing_indi->sqM = "_M";
			}

			$buyers[$key]['buyer_feats'] = $model->getPOPsDetails($buyer,$listing_indi->sqM,$buyer['sub_type'],$langValues);

		//	var_dump($buyers[$key]['buyer_feats']);

			foreach ($buyers[$key]['buyer_feats']['feats_array'] as $key2 => $value2) {
	            # code...
	            $arr_value= explode(": ", $value2);
	            if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type" && $arr_value[0]!="Term" && $arr_value[0]!="Possession"){
	                $buyers[$key]['buyer_feats']['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
	            } else {
	                $buyers[$key]['buyer_feats']['feats_array_edited'][]=$value2;
	            }            
        	}

        	$buyers[$key]['buyer_feats']['feats_array'] = $buyers[$key]['buyer_feats']['feats_array_edited'];

		}

		//$totalCount = $this->getAllPopsBuyers();
		$rows = array();
		$i=0;
		if($buyers){
			foreach($buyers as $val){
				$val = (object) $val;
				$rows[$i]['new_pops']=$val->listingcount_new;
				$rows[$i]['buyer_id']=$val->buyer_id;
				$rows[$i]['buyer_type']=$val->buyer_type;
				$rows[$i]['name']=$val->name;
				$rows[$i]['price_type']=$val->price_type;
				$rows[$i]['price_value']=$val->price_value;				
				$rows[$i]['desc']=$val->desc;
				$rows[$i]['listingcount']=$val->listingcount;
				$rows[$i]['saved_pops']=$val->hassaved;
				$rows[$i]['hasnew']=$val->hasnew;
				$rows[$i]['hasnew_2']=$val->hasnew_2;
				$rows[$i]['contact_type']=$val->contact_type;
				$rows[$i]['contact_method']=$val->contact_method;
				$rows[$i]['initial_feats']=$val->buyer_feats['initial_feats'];
				$rows[$i]['feats_array']=implode("#",$val->buyer_feats['feats_array_edited']);
			//	$rows[$i]['allmatches']=$this->getBuyerAllMatchPOPsCount($val->buyer_id);
				$rows[$i]['allmatches']=$val->listingcount;
				//inside $val->needs
				//foreach ($val->needs as $key => $value) {
					$rows[$i]['listing_id']=$val->listing_id;	
					$zips_ex  = explode(",", $val->zip);
					$rows[$i]['zip']=implode(", ", $zips_ex);	
					$rows[$i]['property_name']=$val->property_name;
					$rows[$i]['currency']=$val->currency ?  $val->currency : "USD" ;
					$rows[$i]['symbol']=$val->symbol;
					$rows[$i]['city']=$val->city;
					$rows[$i]['state']=$val->state;
					$rows[$i]['property_type']=$val->property_type;
					$rows[$i]['ptype_name']=JText::_($val->ptype_name);
					$rows[$i]['sub_type']=$val->sub_type;
					$rows[$i]['sub_name']=JText::_($val->sub_name);
					$rows[$i]['bedroom']=$val->bedroom;
					$rows[$i]['bathroom']=$val->bathroom;
					$rows[$i]['unit_sqft']=$val->unit_sqft;
					$rows[$i]['view']=$val->view;
					$rows[$i]['style']=$val->style;
					$rows[$i]['year_built']=$val->year_built;
					$rows[$i]['pool_spa']=$val->pool_spa;
					$rows[$i]['condition']=$val->condition;	
					$rows[$i]['garage']=$val->garage;
					$rows[$i]['units']=$val->units;
					$rows[$i]['grm']=$val->grm;
					$rows[$i]['cap_rate']=$val->cap_rate;
					$rows[$i]['occupancy']=$val->occupancy;
					$rows[$i]['type']=$val->type;
					$rows[$i]['type_lease']=$val->type_lease;
					$rows[$i]['type_lease2']=$val->type_lease2;
					$rows[$i]['listing_class']=$val->listing_class;
					$rows[$i]['parking_ratio']=$val->parking_ratio;
					$rows[$i]['ceiling_height']=$val->ceiling_height;
					$rows[$i]['stories']=$val->stories;
					$rows[$i]['room_count']=$val->room_count;
					$rows[$i]['available_sqft']=$val->available_sqft;
					$rows[$i]['lot_size']=$val->lot_size;
					$rows[$i]['lot_sqft']=$val->lot_sqft;
					$rows[$i]['bldg_sqft']=$val->bldg_sqft;
					$rows[$i]['term']=$val->term;
					$rows[$i]['furnished']=$val->furnished;
					$rows[$i]['pet']=$val->pet;
					$rows[$i]['possession']=$val->possession;
					$rows[$i]['zoned']=$val->zoned;
					$rows[$i]['bldg_type']=$val->bldg_type;
					$rows[$i]['features1']=$val->features1;
					$rows[$i]['features2']=$val->features2;
					$rows[$i]['features3']=$val->features3;
					$rows[$i]['setting']=$val->setting;
					$rows[$i]['closed']=$val->closed;
				//}			
				$rows[$i]['expiry']=90;	
				$i++;		
			}
		}
		
	/*	echo "<pre>";
		 //var_dump($buyers);
		var_dump($rows);
		var_dump($abuyers);
		// var_dump($bbuyers);
		// var_dump($cbuyers);
		echo "</pre>";
	*/

		if(count($rows) == 0) {
			$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
		}
		else {		
			//$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>array_slice($rows, 0, 5, true));
			$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
		}
		echo json_encode($response);
		/*$view->assignRef('buyers', $buyers);
		$view->assignRef('a_buyers', $abuyers);
		$view->assignRef('b_buyers', $bbuyers);
		$view->assignRef('c_buyers', $cbuyers);
		$view->assignRef('i_buyers', $ibuyers);
		$view->assignRef('pocket_listings', $pocketlistings);
		$view->display($this->getTask());*/
	}
	public function getPropertyTypes(){
		//$user = JFactory::getUser();
		////$session = JFactory::getSession();
		//$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
       // $language_tag = JFactory::getUser()->currLanguage;
        $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);

		$model = $this->getModel($this->getName());
		$ptypes = $model->getAllPropertyTypes();
		foreach ($ptypes as $key => $value) {
			# code...
			$ptypes[$key]->type_name = JText::_($ptypes[$key]->type_name);
		}
		echo json_encode($ptypes);
	}
	public function getSubPropertyTypes(){
		//$user = JFactory::getUser();
		////$session = JFactory::getSession();
		//$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
       // $language_tag = JFactory::getUser()->currLanguage;
        $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);


		$proptype_id = $_GET['prop_id'];
		$model = $this->getModel($this->getName());
		$sptypes = $model->getAllSubPropTypes($proptype_id);

		foreach ($sptypes as $key => $value) {
			# code...
			$sptypes[$key]->name = JText::_($sptypes[$key]->name);
		}

		echo json_encode($sptypes);
	}
	private function countlisting($buyer_id) {
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($buyer_id);
	//	$search_key = $buyers[0]->needs[0]->zip;
	//	$searchreturn = $model->searchZip($search_key);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		if($price[1] == ''){$usethis = 0;}
		#print_r($listing); die();
		foreach($listing as $search){	
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
				 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					 if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
				if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->ceiling_height == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
				 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		#die();
		usort($pocketlistings_0, function($a, $b){
			if($a->abcd == $b->abcd){
				if($a->price1 > $b->price1){
					return 1;
				} else {
					return -1;
				}
				if($a->price1 == $b->price1){
					if($a->listing_id > $b->listing_id){
						return 1;
					} else {
						return -1;
					}
				}
			}
			if(($a->abcd < $b->abcd)){
				return 1;
			} else {
				return -1;
			}
		});
		foreach($pocketlistings_0 as $indi){
				array_push($pocketlistings, $indi);
		}
		return count($pocketlistings);
	}
	private function countlisting_opti($buyer) {
		$model = $this->getModel($this->getName());
	//	$buyers = $model->getBuyer($buyer_id);
	//	$search_key = $buyers[0]->needs[0]->zip;
	//	$searchreturn = $model->searchZip($search_key);
		$buyers[0] = (object) $buyer;


		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter_opti(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		if($price[1] == ''){$usethis = 0;}
		#print_r($listing); die();
		foreach($listing as $search){	
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
				 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					 if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
				if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->ceiling_height == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
				 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		#die();
		usort($pocketlistings_0, function($a, $b){
			if($a->abcd == $b->abcd){
				if($a->price1 > $b->price1){
					return 1;
				} else {
					return -1;
				}
				if($a->price1 == $b->price1){
					if($a->listing_id > $b->listing_id){
						return 1;
					} else {
						return -1;
					}
				}
			}
			if(($a->abcd < $b->abcd)){
				return 1;
			} else {
				return -1;
			}
		});
		foreach($pocketlistings_0 as $indi){
				array_push($pocketlistings, $indi);
		}
		return count($pocketlistings);
	}
	public function	removethisfromsearch($sk){
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($sk);
		$listing = $model->get_pocket_listing_indi_filter(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		if($price[1] == ''){$usethis = 0;}
		#print_r($listing); die();
		foreach($listing as $search){	
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
				 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					 if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
				if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->ceiling_height == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
				 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		usort($pocketlistings_0, function($a, $b){
			if($a->abcd == $b->abcd){
				if($a->price1 > $b->price1){
					return 1;
				} else {
					return -1;
				}
				if($a->price1 == $b->price1){
					if($a->listing_id > $b->listing_id){
						return 1;
					} else {
						return -1;
					}
				}
			}
			if(($a->abcd < $b->abcd)){
				return 1;
			} else {
				return -1;
			}
		});
		if($usethis) {
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 1 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}
			#echo "<pre>"; print_r($pocketlistings); die();
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 1 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 1 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 0 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 0 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 0 && $search->price1 <= $price[1]){
					array_push($pocketlistings, $search);
				}
			}	
		} else {
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			#echo "<pre>"; print_r($pocketlistings); die();
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
		}
		$requestaccess = $model->checkRequestAccess();
		if(isset($_REQUEST['hidden'])){
			$h = 1;
		} else { $h = 0; }
		return $pocketlistings;
	}
	public function expandsearch(){
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($_REQUEST['lID']);
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$view = &$this->getView($this->getName(), 'html');
		$search_key = substr($buyers[0]->needs[0]->zip, 0, 4);
		$realsearch = $this->removethisfromsearch($_REQUEST['lID']);
		#print_r($realsearch); die();
		$searchreturn = $model->searchZip($search_key);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter2(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		if($price[1] == ''){$usethis = 0;}
		#print_r($listing); die();
		foreach($listing as $search){
			$network = $model2->get_network($search->user_id, 1);
			$search->network = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 0);
			$search->network_pending = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 2);
			$search->network_denied = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}  if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					}  if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					 if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					 if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
				 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					 if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		#print_r($pocketlistings_0); exit;
		usort($pocketlistings_0, function($a, $b){
			if($a->price1 == $b->price1) {
				if($a->abcd > $b->abcd) {
					return -1;
				} else if($a->abcd == $b->abcd){
					if($a->price2 > $b->price2) {
						return 1;
					} else {
						return -1;
					}
				} else {
					return 1;
				}				
			} else {
				if(($a->price1 < $b->price1)){
					return -1;
				} else {
					return 1;
				}
			}
		});	
		if($usethis) {
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 1 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}
			#echo "<pre>"; print_r($pocketlistings); die();
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 1 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 1 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 0 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 0 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 0 && ($search->price1 <= $price[1] && $search->price2 >= $price[0])){
					array_push($pocketlistings, $search);
				}
			}	
		} else {
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			#echo "<pre>"; print_r($pocketlistings); die();
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 1 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 1 && $search->per == 0 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 1 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
			foreach($pocketlistings_0 as $search){
				if($search->setting == 2 && $search->per == 0 && $search->disclose == 0 && $search->price1 >= $price[0]){
					array_push($pocketlistings, $search);
				}
			}
		}
		foreach($realsearch as $rs){
			foreach($pocketlistings as $key=>$value){
				#echo $value->listing_id." - ".$rs->listing_id."<br>";
				if($value->listing_id == $rs->listing_id){
					unset($pocketlistings[$key]);
					#echo "<br>";
					print_r($key);
				}
			}
		}
		#die();
		if(count($pocketlistings) <= 0){
			$pocketlistings == "aa";
		}
		#echo $pocketlistings;
		if($_SESSION['viewed_buyerspage_2'] == 1){
			foreach($buyers as $buyer){
				$model->resetBuyerListings($buyer->buyer_id, 2);
			}
		}
		$requestaccess = $model->checkRequestAccess();
		if(isset($_REQUEST['hidden'])){
			$h = 1;
		} else { $h = 0; }
		$view->assignRef('pocket_listings', $pocketlistings);
		echo "<div id=\"remove-this\">";
		foreach($pocketlistings as $pk){
			if(JFactory::getUser()->id == $pk->user_id) {
				$display = "full";
			} else {
				if(in_array(JFactory::getUser()->id, $pk->network)) {
					if($pk->setting == 1) {
						$display = "full";
					} else if($pk->setting == 2) {
						if(!$pk->per) {
							if($pk->per === "0") {
								$display = "pending";
							} else {
								$display = "no";
							}
						} else {
							if($pk->per == 1) {
								$display = "full";
							} else if($pk->per == 2) {
								$display = "no";
							}
						}
					}
				} else if(in_array(JFactory::getUser()->id, $pk->network_pending)) {
					$display = "ask network pending";
				} else if(in_array(JFactory::getUser()->id, $pk->network_denied)) {
					$display = "ask network denied";
				} else {
					$display = "ask network";
				}
			}
			$arr_images = explode(",", $pk->images);
			if(!count($arr_images) || !isset($arr_images[0]) || !$arr_images[0]) {
				$image = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type);
			} else {
				$image = $arr_images[0];
			}
			$image_bw = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type, true);
			?>
<?php }
	echo "<div id=\"remove-this2\"></div></div>";
}
	public function resetBuyerList(){
		$buyerId = $_POST['buyerId'];
		$model = $this->getModel($this->getName());
		$model->resetBuyerListings($buyerId, 2);
	}
	public function individualbuyers(){
		// language loading
 		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($_REQUEST['lID']);
		#echo "<pre>", print_r($buyers), "</pre>";
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$view = &$this->getView($this->getName(), 'html');
		$application = JFactory::getApplication();
		if($buyers[0]->agent_id!=JFactory::getUser()->id) $application->redirect('index.php');
		$search_key = $buyers[0]->needs[0]->zip;
		$buyers[0]->buyer_image = $model->get_buyer_image($buyers[0]->needs[0]->property_type, $buyers[0]->needs[0]->sub_type);
		$searchreturn = $model->searchZip($search_key);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter_buyer_match(array('pp.price'=>$buyers[0]->price_value), $buyers);
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		$ex_rates = $model->getExchangeRates_indi();
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$previous_year = date("Y", strtotime("-1 year"));

		$langValues = $this->checkLangFiles();

		while(intval($previous_year) >= 2012) {
			if($userDetails->{"verified_".$previous_year}) {
				$user_previous_year_volume = $userDetails->{"volume_".$previous_year};
				$user_previous_year_ave_price = $userDetails->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}
		
		$ex_rates = $model->getExchangeRates_indi();
		
		foreach($listing as $search){	
			$network = $model2->get_network($search->user_id, 1);
			$search->network = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 0);
			$search->network_pending = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 2);
			$search->network_denied = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$ownerDetails = $model2->get_user_registration(JFactory::getUser()->id);
			$ownerCountryDetails = $model->getCountry($search->user_id);
			$user_currency = $ownerCountryDetails[0]->currency;
			$search->is_private = 0;
			
			if($search->selected_permissions == 7 && $userDetails->state == $ownerDetails->state) {
				$search->is_private = 1;
			}
			if($search->selected_permission == 3) {
				if($previous_year == 2012 && !$userDetails->{"verified_".$previous_year}) {
					$search->is_private = 1;
				} else {
					$previous_year_volume_minimum = $search->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum * $item_user_currency_rate;
					} else {
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum;
					}
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_volume_converted = $user_previous_year_volume * $user_currency_rate;
					} else {
						$user_previous_year_volume_converted = $user_previous_year_volume;
					}
					#echo $user_previous_year_volume_converted . " " . $previous_year_volume_minimum_converted;
					if($user_previous_year_volume_converted <= $previous_year_volume_minimum_converted) {
						$search->is_private = 1;
					} else {
						$search->is_private = 0;
					}
				}			
				#$this->data['items'][$_key]->volume_private = $is_private;
				#echo $this->data['items'][$_key]->volume_private;
				#echo $item->listing_id;
			}
			if($search->selected_permission == 4) {
				if($previous_year == 2012 && !$userDetails->{"ave_price_".$previous_year}) {
					$search->is_private = 1;
				} else {
					$previous_year_ave_price_minimum = $search->values;
					if($user_currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$user_currency};
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum * $item_user_currency_rate;
					} else {
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum;
					}
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price * $user_currency_rate;
					} else {
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price;
					}
					if($user_previous_year_ave_price_converted <= $previous_year_ave_price_minimum_converted) {
						$search->is_private = 1;
					}
				}			
				#$this->data['items'][$_key]->ave_price_private = $is_private;
				#echo $this->data['items'][$_key]->volume_private;
				#echo $item->listing_id;
			}
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					if($search->furnished == $buyers[0]->needs[0]->furnished){
						$pointctr++;
					} if($search->pet == $buyers[0]->needs[0]->pet){
						$pointctr++;
					} if($search->possession == $buyers[0]->needs[0]->possession){
						$pointctr++;
					} if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
				if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					 if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		foreach($pocketlistings_0 as $indi){
				array_push($pocketlistings, $indi);
			}
		$user_info = $model2->get_user_registration(JFactory::getUser()->id);
		$s_buyers = array();
		$saved_buyers = $model->getSavedListing(JFactory::getUser()->id, $_REQUEST['lID']);
		$today = date ('Y-m-d');
		foreach($saved_buyers as $s){
			$saved_list = $model->get_saved_pocket_listing_indi(array('p.listing_id'=>$s->listing_id,"p.closed"=>"0","p.date_expired"=>$today));

			
			if($saved_list[0]->country==0){
				$saved_list[0]->countryIso = $model2->getCountryIso("223");
			} else {
				$saved_list[0]->countryIso = $model2->getCountryIso($saved_list[0]->country);
			}
			if($saved_list[0]->user_id != JFactory::getUser()->id){
				if($user_info->currency!=$saved_list[0]->currency){
					$ex_rates_con = $ex_rates->rates->{$saved_list[0]->currency};
					$ex_rates_can = $ex_rates->rates->{$user_info->currency};
					if($saved_list[0]->currency=="USD"){					
						$saved_list[0]->price1=$saved_list[0]->price1 * $ex_rates_can;
					} else {
						$saved_list[0]->price1=($saved_list[0]->price1 / $ex_rates_con) * $ex_rates_can;							
					}
					if($saved_list[0]->price2){
						if($saved_list[0]->currency=="USD"){					
							$saved_list[0]->price2=$saved_list[0]->price2 * $ex_rates_can;
						} else {
							$saved_list[0]->price2=($saved_list[0]->price2/ $ex_rates_con) * $ex_rates_can;							
						}
					}
					$saved_list[0]->currency = $user_info->currency;
					$saved_list[0]->symbol = $user_info->symbol;
					//$saved_list[0]->price1 = $model->getExchangeRates($saved_list[0]->price1,$user_info->currency,$saved_list[0]->currency);
					//if($saved_list[0]->price2)
					//	$saved_list[0]->price2 = $model->getExchangeRates($saved_list[0]->price2,$user_info->currency,$saved_list[0]->currency);
				}
				$saved_list[0]->currency = $user_info->currency;
				$saved_list[0]->symbol = $user_info->symbol;
			}
			$arr_images = explode(",", $saved_list[0]->images);
			if(!count($arr_images) || !isset($arr_images[0]) || !$arr_images[0]) {
				$image = "images/buyers/212x172/" . $model->get_buyer_image($saved_list[0]->property_type, $saved_list[0]->sub_type);
			} else {
				$image = $arr_images[0];
			}
			$saved_list[0]->images = $image;
			$saved_list[0]->images_bw = "images/buyers/212x172/" . $model->get_buyer_image($saved_list[0]->property_type, $saved_list[0]->sub_type, true);
		/*	if(  strtotime($saved_list[0]->date_created) < strtotime('-3 days')  && !$saved_list[0]->views ) {
			  	$saved_list[0]->views = 8;
			} else {
				$saved_list[0]->views = 8 + $saved_list[0]->views;
			}*/

			if(  strtotime($saved_list[0]->date_created) < strtotime('-3 days')  ) {
				if(!$saved_list[0]->views ){
			  		$saved_list[0]->views = 0;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($s->listing_id);
					$saved_list[0]->views = $rand_count + $saved_list[0]->views;
			  	}
			} else {
				if(!$saved_list[0]->views ){
			  		$saved_list[0]->views = 1;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($s->listing_id);
					$saved_list[0]->views = $rand_count + $saved_list[0]->views;
			  	}				
			}


			$saved_list[0]->sqpM= "";
		
			if($saved_list[0]->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($saved_list[0]->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($saved_list[0]->bldg_sqft){
						if($saved_list[0]->bldg_sqft == $value[1]){
							$saved_list[0]->bldg_sqft=$value[0];
						}
					}
					if($saved_list[0]->available_sqft){
						if($saved_list[0]->available_sqft == $value[1]){
							$saved_list[0]->available_sqft=$value[0];
						}
						
					}
					if($saved_list[0]->unit_sqft){
						if($saved_list[0]->unit_sqft == $value[1]){
							$saved_list[0]->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($saved_list[0]->lot_sqft){
						if($saved_list[0]->lot_sqft == $value[1]){
							$saved_list[0]->lot_sqft=$value[0];
						}
						
					}
					if($saved_list[0]->lot_size){
						if($saved_list[0]->lot_size == $value[1]){
							$saved_list[0]->lot_size=$value[0];
						}
						
					}
				}

				$saved_list[0]->sqpM = "_M";
			}

			$saved_list[0]->save_pop_feats = $model->getPOPsDetails($saved_list[0],$saved_list[0]->sqpM,$saved_list[0]->sub_type,$langValues);
			
			array_push($s_buyers, $saved_list);
		}
		if($_SESSION['viewed_buyerspage_2'] == 1){
			foreach($buyers as $buyer){
				//$model->resetBuyerListings($buyer->buyer_id, 2);
			}
		}
		$requestaccess = $model->checkRequestAccess();
		foreach($pocketlistings as $key => $pk) {
			if($pk->country==0){
				$pocketlistings[$key]->countryIso = $model2->getCountryIso("223");
			} else {
				$pocketlistings[$key]->countryIso = $model2->getCountryIso($pk->country);
			}
			if($pk->user_id != JFactory::getUser()->id){
				if($user_info->currency!=$pk->currency){
					$ex_rates_con = $ex_rates->rates->{$buyers[0]->needs[0]->currency};
					$ex_rates_can = $ex_rates->rates->{$user_info->currency};
					if($buyers[0]->needs[0]->currency=="USD"){					
						$pk->price1=$pk->price1 * $ex_rates_can;
					} else {
						$pk->price1=($pk->price1 / $ex_rates_con) * $ex_rates_can;							
					}
					if($pk->price2){
						if($buyers[0]->needs[0]->currency=="USD"){					
							$pk->price2=$pk->price2 * $ex_rates_can;
						} else {
							$pk->price2=($pk->price2 / $ex_rates_con) * $ex_rates_can;							
						}
					}
				}
				$pk->currency = $user_info->currency;
				$pk->symbol = $user_info->symbol;
			} 
			$arr_images = explode(",", $pk->images);
			if(!count($arr_images) || !isset($arr_images[0]) || !$arr_images[0]) {
				$image = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type);
			} else {
				$image = $arr_images[0];
			}
			$pocketlistings[$key]->images = $image;
			$pocketlistings[$key]->images_bw = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type, true);
			//if(  strtotime($pk->date_created) < strtotime('-3 days')  && !$pk->views ) {
			 // 	$pk->views = 8;
			//} else {
			//	$pk->views = 8 + $pk->views;
			//}

			if(  strtotime($pk->date_created) < strtotime('-3 days')  ) {
				if(!$pk->views ){
			  		$pk->views = 0;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($pk->listing_id);
					$pk->views = $rand_count + $pk->views;
			  	}
			} else {
				if(!$pk->views ){
			  		$pk->views = 1;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($pk->listing_id);
					$pk->views = $rand_count + $pk->views;
			  	}				
			}


			$pk->sqpM= "";
		
			if($pk->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($pk->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($pk->bldg_sqft){
						if($pk->bldg_sqft == $value[1]){
							$pk->bldg_sqft=$value[0];
						}
					}
					if($pk->available_sqft){
						if($pk->available_sqft == $value[1]){
							$pk->available_sqft=$value[0];
						}
						
					}
					if($pk->unit_sqft){
						if($pk->unit_sqft == $value[1]){
							$pk->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($pk->lot_sqft){
						if($pk->lot_sqft == $value[1]){
							$pk->lot_sqft=$value[0];
						}
						
					}
					if($pk->lot_size){
						if($pk->lot_size == $value[1]){
							$pk->lot_size=$value[0];
						}
						
					}
				}

				$pk->sqpM = "_M";
			}

			$pk->pop_feats = $model->getPOPsDetails($pk,$pk->sqpM,$pk->sub_type,$langValues);
		}
		$new_pocketlistings = array();
		$maxcount_new = $buyers[0]->listingcount_new;
		$i=0;
		$pocketlistings_R =array_reverse($pocketlistings);
		foreach ($pocketlistings_R as $key => $value) {
			if($i<$maxcount_new){
				$new_pocketlistings[$key]=$pocketlistings_R[$key];
			} else {
				break;
			}
			$i++;
		}
		if(isset($_REQUEST['hidden'])){
			$h = 1;
		} else { $h = 0; }


		$sqM = "";
		
		if($buyers[0]->needs[0]->sqftMeasurement=="sq. meters"){
			$getMeasurement = $model->getSqMeasureByCountry($buyers[0]->needs[0]->country);
			foreach ($getMeasurement['bldg'] as $key => $value) {
				if($buyers[0]->needs[0]->bldg_sqft){
					if($buyers[0]->needs[0]->bldg_sqft == $value[1]){
						$buyers[0]->needs[0]->bldg_sqft=$value[0];
					}
				}
				if($buyers[0]->needs[0]->available_sqft){
					if($buyers[0]->needs[0]->available_sqft == $value[1]){
						$buyers[0]->needs[0]->available_sqft=$value[0];
					}
					
				}
				if($buyers[0]->needs[0]->unit_sqft){
					if($buyers[0]->needs[0]->unit_sqft == $value[1]){
						$buyers[0]->needs[0]->unit_sqft=$value[0];
					}
					
				}
			}

			foreach ($getMeasurement['lot'] as $key => $value) {
				# code...
				if($buyers[0]->needs[0]->lot_sqft){
					if($buyers[0]->needs[0]->lot_sqft == $value[1]){
						$buyers[0]->needs[0]->lot_sqft=$value[0];
					}
					
				}
				if($buyers[0]->needs[0]->lot_size){
					if($buyers[0]->needs[0]->lot_size == $value[1]){
						$buyers[0]->needs[0]->lot_size=$value[0];
					}
					
				}
			}

			$sqM = "_M";
		}

		
		$buyers[0]->buyer_feats = $model->getPOPsDetails($buyers[0]->needs[0],$sqM,$buyers[0]->needs[0]->sub_type,$langValues);

		$view->assignRef('sqM', $sqM);


		$view->assignRef('hidden', $h);
		$view->assignRef('user_info', $user_info);
		$view->assignRef('saved_listings', $s_buyers);
		$view->assignRef('buyers_list', $saved_buyers);
		$view->assignRef('new_pocket_listings', array_reverse($new_pocketlistings));
		$view->assignRef('pocket_listings', $pocketlistings);
		if(count($buyers)) {
   			$buyers[0]->listingcount = count($pocketlistings);
  		}
		$view->assignRef('viewers', $viewers_arr);
		$view->assignRef('buyer', $buyers);
		$view->display($this->getTask());
	}
public function individualbuyers_raw(){

		$buyerId = $_GET['buyer_id'];
		$userId = $_GET['userid'];

		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($userId));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
       // $language_tag = JFactory::getUser()->currLanguage;
        $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);


		
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($buyerId);
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$view = &$this->getView($this->getName(), 'html');
		$search_key = $buyers[0]->needs[0]->zip;
		$buyers[0]->buyer_image = $model->get_buyer_image($buyers[0]->needs[0]->property_type, $buyers[0]->needs[0]->sub_type);
		$searchreturn = $model->searchZip($search_key);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();
		$ctr = 1;
		$pointctr = 0;
		$listing = $model->get_pocket_listing_indi_filter_buyer_match(array('pp.price'=>$buyers[0]->price_value), $buyers);
		
		$price = explode('-', $buyers[0]->price_value);
		$usethis = 1;
		foreach($listing as $search){	
			$network = $model2->get_network($search->user_id, 1);
			$search->network = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 0);
			$search->network_pending = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			$network = $model2->get_network($search->user_id, 2);
			$search->network_denied = array_map(function ($obj) {return $obj->other_user_id;}, $network);
			if($search->property_type == 1){
				if($search->sub_type == 1){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 2){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 3){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 4){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 2){
				if($search->sub_type == 5){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->condition == $buyers[0]->needs[0]->condition){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 6){
					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 7){
					if($search->furnished == $buyers[0]->needs[0]->furnished){
						$pointctr++;
					} if($search->pet == $buyers[0]->needs[0]->pet){
						$pointctr++;
					} if($search->possession == $buyers[0]->needs[0]->possession){
						$pointctr++;
					} if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){
						$pointctr++;
					} if($search->view == $buyers[0]->needs[0]->view){
						$pointctr++;
					} if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){
						$pointctr++;
					} if($search->garage == $buyers[0]->needs[0]->garage){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 8){
					if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 3){
				if($search->sub_type == 9){
					if($search->style == $buyers[0]->needs[0]->style){
						$pointctr++;
					}  if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 10){
					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 11){
					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 12){
					 if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 13){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 14){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 15){
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
			if($search->property_type == 4){
				if($search->sub_type == 16){
				if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					} if($search->features3 == $buyers[0]->needs[0]->features3){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 17){
					 if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){
						$pointctr++;
					} if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
				if($search->sub_type == 18){
					if($search->stories == $buyers[0]->needs[0]->stories){
						$pointctr++;
					} if($search->year_built == $buyers[0]->needs[0]->year_built){
						$pointctr++;
					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){
						$pointctr++;
					} if($search->features1 == $buyers[0]->needs[0]->features1){
						$pointctr++;
					} if($search->features2 == $buyers[0]->needs[0]->features2){
						$pointctr++;
					}
					$search->abcd = $pointctr;
					array_push($pocketlistings_0, $search);
					$pointctr = 0;
				}
			}
		}
		foreach($pocketlistings_0 as $indi){
				array_push($pocketlistings, $indi);
			}
		$s_buyers = array();
		$saved_buyers = $model->getSavedListing(JFactory::getUser()->id, $buyerId);
		foreach($saved_buyers as $s){
			$saved_list = $model->get_pocket_listing_indi(array('p.listing_id'=>$s->listing_id));
			$arr_images = explode(",", $saved_list[0]->images);
			if(!count($arr_images) || !isset($arr_images[0]) || !$arr_images[0]) {
				$image = "images/buyers/212x172/" . $model->get_buyer_image($saved_list[0]->property_type, $saved_list[0]->sub_type);
			} else {
				//$image = $arr_images[0];
				//var_dump($s->listing_id);
				$db = JFactory::getDbo();
	          	$query = $db->getQuery(true);
	          	$query = "SELECT image FROM `tbl_pocket_images` WHERE `tbl_pocket_images`.`listing_id` = ".$s->listing_id." ORDER BY order_image ASC,image_id DESC";
	          	$db->setQuery($query);           
	         	//$resultss = $db->execute();
	         	$arra_images = $db->loadColumn();
	         	$image = implode(",", $arra_images);        
	         	//  $images = $arra_images;
			}
			$saved_list[0]->images = $image;
			$saved_list[0]->images_bw = "images/buyers/212x172/" . $model->get_buyer_image($saved_list[0]->property_type, $saved_list[0]->sub_type, true);
			array_push($s_buyers, $saved_list);
		}
		$requestaccess = $model->checkRequestAccess();
		foreach($pocketlistings as $key => $pk) {
			$arr_images = explode(",", $pk->images);
			if(!count($arr_images) || !isset($arr_images[0]) || !$arr_images[0]) {
				$image = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type);
			} else {
				$image = $arr_images[0];
				$db = JFactory::getDbo();
	          	$query = $db->getQuery(true);
	          	$query = "SELECT image FROM `tbl_pocket_images` WHERE `tbl_pocket_images`.`listing_id` = ".$pk->listing_id." ORDER BY order_image ASC,image_id DESC";
	          	$db->setQuery($query);           
	         	//$resultss = $db->execute();
	         	$arra_images = $db->loadColumn();
	         	$image = implode(",", $arra_images);   
	        //  	$images = $arra_images;
			}
			$pocketlistings[$key]->images = $image;
			$pocketlistings[$key]->images_bw = "images/buyers/212x172/" . $model->get_buyer_image($pk->property_type, $pk->sub_type, true);
		}
		if(isset($_REQUEST['hidden'])){
			$h = 1;
		} else { $h = 0; }
		if(count($buyers)) {
   			$buyers[0]->listingcount = count($pocketlistings);
  		}
  		/*
		$view->assignRef('hidden', $h);
		$view->assignRef('saved_listings', $s_buyers);
		$view->assignRef('buyers_list', $saved_buyers);
		$view->assignRef('pocket_listings', $pocketlistings);
		$view->assignRef('viewers', $viewers_arr);
		$view->assignRef('buyer', $buyers);
		$view->display($this->getTask());*/
		$rows = array();
		$i=0;
		$langValues = $model->checkLangFiles();

		//var_dump($listing)
		foreach($pocketlistings as $pk){
			//$rows[$i][]=$pk;
			$rows[$i]['listing_id']=$pk->listing_id;
			$rows[$i]['type_property_type']=$pk->property_type;	
			$rows[$i]['property_name']=$pk->property_name;		
			$rows[$i]['type_name']=JText::_($pk->type_name);	
			$rows[$i]['sub_type']=$pk->sub_type;	
			$rows[$i]['sub_type_name']=JText::_($pk->name);
			$rows[$i]['price_type']=$pk->price_type;
			$rows[$i]['price1']=$pk->price1;
			$rows[$i]['price2']=$pk->price2;
			$rows[$i]['currency']=$pk->currency;
			$rows[$i]['symbol']=$pk->symbol;
			$rows[$i]['disclose']=$pk->disclose;
			$rows[$i]['sold']=$pk->sold;
			$now = time();
            $timedifference=strtotime($pk->date_expired)-$now;
            $rows[$i]['expiry']=strval(floor($timedifference/(60*60*24)));
			$rows[$i]['user_id']=$pk->user_id;
			$rows[$i]['name']=$pk->username;
			$rows[$i]['city']=$pk->city;
			$rows[$i]['zip']=$pk->zip;
			$rows[$i]['state']=$pk->state;
			$rows[$i]['state_name']=$pk->state_name;
			$rows[$i]['bedroom']=$pk->bedroom;
			$rows[$i]['bathroom']=$pk->bathroom;
			$rows[$i]['unit_sqft']=$pk->unit_sqft;
			$rows[$i]['view']=$pk->view;
			$rows[$i]['style']=$pk->style;
			$rows[$i]['year_built']=$pk->year_built;
			$rows[$i]['pool_spa']=$pk->pool_spa;
			$rows[$i]['condition']=$pk->condition;
			$rows[$i]['garage']=$pk->garage;
			$rows[$i]['units']=$pk->units;
			$rows[$i]['cap_rate']=$pk->cap_rate;
			$rows[$i]['grm']=$pk->grm;
			$rows[$i]['occupancy']=$pk->occupancy;
			$rows[$i]['type']=$pk->type;
			$rows[$i]['listing_class']=$pk->listing_class;
			$rows[$i]['parking_ratio']=$pk->parking_ratio;
			$rows[$i]['ceiling_height']=$pk->ceiling_height;
			$rows[$i]['stories']=$pk->stories;
			$rows[$i]['room_count']=$pk->room_count;
			$rows[$i]['type_lease']=$pk->type_lease;
			$rows[$i]['type_lease2']=$pk->type_lease2;
			$rows[$i]['available_sqft']=$pk->available_sqft;
			$rows[$i]['lot_sqft']=$pk->lot_sqft;
			$rows[$i]['lot_size']=$pk->lot_size;
			$rows[$i]['bldg_sqft']=$pk->bldg_sqft;
			$rows[$i]['term']=$pk->term;
			$rows[$i]['furnished']=$pk->furnished;
			$rows[$i]['pet']=$pk->pet;
			$rows[$i]['possession']=$pk->possession;
			$rows[$i]['zoned']=$pk->zoned;
			$rows[$i]['bldg_type']=$pk->bldg_type;
			$rows[$i]['features1']=$pk->features1;
			$rows[$i]['features2']=$pk->features2;
			$rows[$i]['features3']=$pk->features3;
			$rows[$i]['setting']=$pk->setting;
			$rows[$i]['desc']=$pk->desc;
			$rows[$i]['closed']=$pk->closed;
			$rows[$i]['date_created']=$pk->date_created;
			$rows[$i]['date_expired']=$pk->date_expired;
			$rows[$i]['pocket_id']=$pk->listing_id;
			$rows[$i]['setting']=$pk->setting;			
			$rows[$i]['permission']=$pk->permission;
			$rows[$i]['user_b']=$pk->user_b;
			$rows[$i]['req_acc_status']=$pk->req_acc_status;
			if($rows[$i]['user_id']!=JFactory::getUser()->id){
				if($rows[$i]['setting']==1){
					$rows[$i]['req_access_per'] = "1";  
	                $rows[$i]['selected_permission'] = "1";
				} else {
					if ($rows[$i]['req_acc_status']=="1"){
                        $rows[$i]['selected_permission'] = "1";
                        $rows[$i]['req_access_per'] = "1";
                    } else if ($rows[$i]['req_acc_status']=="0"){
                        $rows[$i]['req_access_per'] = "pending_private";
                    } else if ($rows[$i]['req_acc_status']=="2"){
                        $rows[$i]['req_access_per'] = "denied_private";
                    } else {
                        $rows[$i]['req_access_per'] = "request_private";
                    }
				}
			} else {
				$rows[$i]['req_access_per'] = "1";  
	            $rows[$i]['selected_permission'] = "1";
			}
			

			if (strpos($pk->images,'uploads/') !== false) {
				    $rows[$i]['images']=$pk->images;
				} else {
					$rows[$i]['images']=null;
				}
			$rows[$i]['show_button']="true";
			$pk->sqpM= "";
		
			if($pk->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model->getSqMeasureByCountry($pk->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($pk->bldg_sqft){
						if($pk->bldg_sqft == $value[1]){
							$pk->bldg_sqft=$value[0];
						}
					}
					if($pk->available_sqft){
						if($pk->available_sqft == $value[1]){
							$pk->available_sqft=$value[0];
						}
						
					}
					if($pk->unit_sqft){
						if($pk->unit_sqft == $value[1]){
							$pk->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($pk->lot_sqft){
						if($pk->lot_sqft == $value[1]){
							$pk->lot_sqft=$value[0];
						}
						
					}
					if($pk->lot_size){
						if($pk->lot_size == $value[1]){
							$pk->lot_size=$value[0];
						}
						
					}
				}

				$pk->sqpM = "_M";
			}

			$pop_feats = $model->getPOPsDetails($pk,$pk->sqpM,$pk->sub_type,$langValues);


			foreach ($pop_feats['feats_array'] as $key => $value) {
	            # code...
	            $arr_value= explode(": ", $value);
	            if($arr_value[1]!="Yes" && $arr_value[0]!="Lease Type"){
	                $pop_feats['feats_array_edited'][]=$arr_value[1]." ".$arr_value[0];
	            } else {
	                $pop_feats['feats_array_edited'][]=$value;
	            }
	            
	        }

			$rows[$i]['initial_feats'] = $pop_feats['initial_feats'];
			$rows[$i]['feats_array'] = implode(", ", $pop_feats['feats_array_edited']);
			$i++;		
		}
		/*echo "<pre>";
		var_dump($rows);
		echo "</pre>";*/
		if(count($rows) == 0) {
			$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>"wala");
		}
		else {		
			$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
		}

		echo json_encode($response);
	}
	function editProperty(){
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
	    $view = &$this->getView($this->getName(), 'html');
		$propertylisting_model = $this->getModel($this->getName());
		$model = $this->getModel($this->getName());
		$individual = $model->getSelectedListing($_REQUEST['lID']);

		if(JFactory::getUser()->id != $individual[0]->user_id) {
			JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));				
		}	

		$user = JFactory::getUser();
		$user_info = $model2->get_user_registration($user->id);
		$currency_data = $this->getCountryCurrencyData($user_info->country); 
		$property_type = JRequest::getVar('ptype');
		$property_subtype = JRequest::getVar('stype');
		$country = $propertylisting_model->getCountry($user->id);
		$setting = $propertylisting_model->getPermissionSetting($_GET['lID']);
		$state = $propertylisting_model->printPropertyState($individual[0]->state);
		$images = array();
		$images = $model->get_property_image($_GET['lID']);

		if(!empty($task)) $tpl = $data['task'];
		$getCountryLangsInitial = $this->getCountryDataByID($individual[0]->conCode);
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		$view->assignRef( 'user_info', $user_info);
		$view->assignRef( 'currency_data', $currency_data[0]);
		$view->assignRef( 'countryId', $country[0]->country);
		$view->assignRef( 'countryIso', $country[0]->countries_iso_code_2);
		$view->assignRef( 'countryIsoPOP', $individual[0]->conIso);
		$view->assignRef( 'countryCurrency', $currency_data[0]->currency);
		$view->assignRef( 'countryCurrSymbol', $currency_data[0]->symbol);
		$view->assignRef( 'setting', $setting[0]->selected_permission );
		$view->assignRef( 'values', $setting[0]->values );
		$view->assignRef( 'state', $state->zone_name);
		$view->assignRef( 'data', $individual[0]);
		$view->assignRef( 'images', $images);
		$view->display($this->getTask());
	}
	function array_search_partial($arr,$keyword) {
	     for ($i = 0; $i < count($arr); $i++) {
	     	$z_check_len = strlen(trim($arr[$i]));
	     	if(substr($keyword, 0, $z_check_len) == trim($arr[$i])){
	     		return true;
	     	}
			//if(strpos(strtolower($keyword),strtolower(trim($arr[$i]))) !== FALSE){
	        //    return true;
			//} 
		 }
		return false;
	}
	function edit_property(){
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$propertylisting_model = $this->getModel($this->getName());
		$langValues = $propertylisting_model->checkLangFiles();
		
		if(isset($_POST['user_id']) && isset($_GET['webservice'])){
			$user = JFactory::getUser();
			$session = JFactory::getSession();
			$session->set('user', new JUser($_POST['user_id']));
			$user =& JFactory::getUser();
			$uid =  $user->id ;
			$data = $_POST;

			
				


			foreach ($data as $key => $value) {
				if($key=="state"){
					$data[$key]=$propertylisting_model->getStateIdByName($value);
				}
				if($key=="ptype"){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}

					$data[$key]=$propertylisting_model->getPtypeByName($value);
				}
				if($key=="stype"){

					foreach ($langValues as $key2 => $value2) {
						if($data["ptype"]==1 ||$data["ptype"]==3){
							if($value==$value2){
								$value = trim($key2);
							}
						} else {
							if($value==$value2 && strpos($key2, "L_") !== false){
								$value = trim($key2);
							}
						}

					}
					
					$data[$key]=$propertylisting_model->getSubTypeByName($data["ptype"],$value);
				}
				if($key=="Bedrooms"){
					$data["bedroom"]=$data[$key];
				}
				if($key=="Bathrooms"){
					$data["bathroom"]=$data[$key];
				}
				if($key=="Bldg. Sq. Ft." || $key=="Bldg__Sq__Ft"){
					$data["bldgsqft"]=$data[$key];
				}
				if($key=="View"){
					$data["view"]=$data[$key];
				}
				if($key=="Unit_Sq__Ft_" || $key=="Unit Sq. Ft."){
					$data["unitsqft"]=$data[$key];
				}

				if($key=="Lot Size"){
					$data["lotsize"]=$data[$key];
				}
				if($key=="Zoned"){
					$data["zoned"]=$data[$key];
				}
				if($key=="Term"){
					$data["term"]=$data[$key];
				}
				if($key=="Possession"){
					$data["possession"]=$data[$key];
				}
				if($key=="Units"){
					$data["units"]=$data[$key];
				}
				if($key=="Cap Rate"){
					$data["caprate"]=$data[$key];
				}
				if($key=="GRM"){
					$data["grm"]=$data[$key];
				}
				if($key=="Lot Sq. Ft."){
					$data["lotsqft"]=$data[$key];
				}
				if($key=="Type"){
					$data["type"]=$data[$key];
				}
				if($key=="Class"){
					$data["class"]=$data[$key];
				}
				if($key=="Room Count"){
					$data["roomcount"]=$data[$key];
				}
				if($key=="Available Sq.Ft"){
					$data["available"]=$data[$key];
				}
				if($key=="Type Lease"){
					$data["typelease"]=$data[$key];
				}
				if(strpos($key, "image_") !== FALSE && strpos($key, "image_order") === FALSE){
					$key_n = str_replace("image_", "", $key);
					$_POST["jform_image"][$key_n]=$data[$key];
					$_POST['jform_image_order'][$key_n]=$key_n+1;
				}

				
			}


			$noeditsdata = $data;

		} else {
			$user =& JFactory::getUser($_SESSION['user_id']);
			$task = JRequest::getVar('task');
			$thispocketid = "";
			$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
			$noeditsdata = $_POST['jform'];
			if(empty($uid)) $application->redirect('index.php?illegal=1');
			if(!empty($_POST['jform']['form'])):
				// PREPARE POST VARIABLES
				foreach($_POST['jform'] as $key=>$value){
					if(is_array($value)){
						foreach($value as $k=>$v){
							$data[$key][$k] = trim(addslashes($v));
						}
					} else {
						$data[$key] = trim(addslashes($value));
					}
				}
				extract($data);
			endif;
		}
		
		
		$previous_data = $propertylisting_model->getSelectedListing($_REQUEST['lID']);
		$send_notif = 0;
		$mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "Edit POPs Previous and Edited Data" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( "Previous Data: \r\n" . print_r($previous_data[0],true) . "Edited Data: \r\n" . print_r($data,true));
		$mailSender ->Send(); 
		
		
		if(	(isset($data['price_type']) && $previous_data[0]->price_type != $data['price_type']) ||
			(isset($data['price1']) && $previous_data[0]->price1 != $data['price1']) ||
			(isset($data['price2']) && $previous_data[0]->price2 != $data['price2'])
		) {
			$send_notif = 1;
		} else {
			switch($data['ptype']) {
				case 1:
					if($data['stype'] == 1) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
							(isset($data['view']) && $previous_data[0]->view != $data['view'])
						) {
							$send_notif = 1;
						}
					} else if($data['stype'] == 2) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['unit_sqft']) && $previous_data[0]->unit_sqft != $data['unit_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($data['stype'] == 3) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['unit_sqft']) && $previous_data[0]->unit_sqft != $data['unit_sqft'])
						) {
							$send_notif = 1;
						}											
					} else if($data['stype'] == 4) {
						if(	(isset($data['lot_size']) && $previous_data[0]->lot_size != $data['lot_size']) ||
							(isset($data['view']) && $previous_data[0]->view != $data['view']) || 
							(isset($data['zoned']) && $previous_data[0]->zoned != $data['zoned'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 2:
					if($data['stype'] == 5) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
							(isset($data['term']) && $previous_data[0]->term != $data['term']) ||
							(isset($data['possession']) && $previous_data[0]->possession != $data['possession']) ||
							(isset($data['furnished']) && $previous_data[0]->furnished != $data['furnished']) ||
							(isset($data['pet']) && $previous_data[0]->pet != $data['pet'])
						) {
							$send_notif = 1;
						}
					} else if($data['stype'] == 6) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['unit_sqft']) && $previous_data[0]->unit_sqft != $data['unit_sqft']) ||
							(isset($data['term']) && $previous_data[0]->term != $data['term']) ||
							(isset($data['furnished']) && $previous_data[0]->furnished != $data['furnished']) ||
							(isset($data['pet']) && $previous_data[0]->pet != $data['pet'])							
						) {
							$send_notif = 1;
						}
						
					} else if($data['stype'] == 7) {
						if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
							(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
							(isset($data['unit_sqft']) && $previous_data[0]->unit_sqft != $data['unit_sqft']) ||
							(isset($data['term']) && $previous_data[0]->term != $data['term']) ||
							(isset($data['furnished']) && $previous_data[0]->furnished != $data['furnished']) ||
							(isset($data['pet']) && $previous_data[0]->pet != $data['pet'])							
						) {
							$send_notif = 1;
						}
					} else if($data['stype'] == 8) {
						if(	(isset($data['lot_size']) && $previous_data[0]->lot_size != $data['lot_size']) ||
							(isset($data['view']) && $previous_data[0]->view != $data['view']) || 
							(isset($data['zoned']) && $previous_data[0]->zoned != $data['zoned'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 3:
					if($data['stype'] == 9) {
						if(	(isset($data['units']) && $previous_data[0]->units != $data['units']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['grm']) && $previous_data[0]->grm != $data['grm']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
							(isset($data['lot_sqft']) && $previous_data[0]->lot_sqft != $data['lot_sqft'])
						) {
							$send_notif = 1;
						}
					} else if($data['stype'] == 10) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['listing_class']) && $previous_data[0]->listing_class != $data['listing_class']) || 
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($data['stype'] == 11) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($data['stype'] == 12) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($data['stype'] == 13) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['room_count']) && $previous_data[0]->room_count != $data['room_count'])
						) {
							$send_notif = 1;
						}					
					} else if($data['stype'] == 14) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
							(isset($data['room_count']) && $previous_data[0]->room_count != $data['room_count'])						
						) {
							$send_notif = 1;
						}					
					} else if($data['stype'] == 15) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 4:
					if($data['stype'] == 16) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['listing_class']) && $previous_data[0]->listing_class != $data['listing_class']) || 
							(isset($data['available_sqft']) && $previous_data[0]->available_sqft != $data['available_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($data['stype'] == 17) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['type_lease']) && $previous_data[0]->type_lease != $data['type_lease']) ||
							(isset($data['available_sqft']) && $previous_data[0]->available_sqft != $data['available_sqft']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
							(isset($data['lot_sqft']) && $previous_data[0]->lot_sqft != $data['lot_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($data['stype'] == 18) {
						if(	(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
							(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
							(isset($data['type_lease']) && $previous_data[0]->type_lease != $data['type_lease'])
						) {
							$send_notif = 1;
						}						
					}
					break;
			}
		}
		
		
		/*
		if(	(isset($data['bedroom']) && $previous_data[0]->bedroom != $data['bedroom']) || 
			(isset($data['bathroom']) && $previous_data[0]->bathroom != $data['bathroom']) || 
			(isset($data['unit_sqft ']) && $previous_data[0]->unit_sqft != $data['unit_sqft']) ||
			(isset($data['view']) && $previous_data[0]->view != $data['view']) || 
			(isset($data['style']) && $previous_data[0]->style != $data['style']) ||
			(isset($data['year_built']) && $previous_data[0]->year_built != $data['year_built']) ||
			(isset($data['pool_spa']) && $previous_data[0]->pool_spa != $data['pool_spa']) ||
			(isset($data['condition']) && $previous_data[0]->condition != $data['condition']) || 
			(isset($data['garage']) && $previous_data[0]->garage != $data['garage']) || 
			(isset($data['units']) && $previous_data[0]->units != $data['units']) ||
			(isset($data['cap_rate']) && $previous_data[0]->cap_rate != $data['cap_rate']) ||
			(isset($data['grm']) && $previous_data[0]->grm != $data['grm']) ||
			(isset($data['type']) && $previous_data[0]->type != $data['type']) ||
			(isset($data['listing_class']) && $previous_data[0]->listing_class != $data['listing_class']) || 
			(isset($data['parking_ratio']) && $previous_data[0]->parking_ratio != $data['parking_ratio']) ||
			(isset($data['ceiling_height']) && $previous_data[0]->ceiling_height != $data['ceiling_height']) ||
			(isset($data['stories']) && $previous_data[0]->stories != $data['stories']) ||
			(isset($data['room_count']) && $previous_data[0]->room_count != $data['room_count']) || 
			(isset($data['type_lease']) && $previous_data[0]->type_lease != $data['type_lease']) ||
			(isset($data['type_lease2']) && $previous_data[0]->type_lease2 != $data['type_lease2']) ||
			(isset($data['available_sqft']) && $previous_data[0]->available_sqft != $data['available_sqft']) ||
			(isset($data['lot_sqft']) && $previous_data[0]->lot_sqft != $data['lot_sqft']) ||
			(isset($data['lot_size']) && $previous_data[0]->lot_size != $data['lot_size']) ||
			(isset($data['bldg_sqft']) && $previous_data[0]->bldg_sqft != $data['bldg_sqft']) ||
			(isset($data['term']) && $previous_data[0]->term != $data['term']) ||
			(isset($data['furnished']) && $previous_data[0]->furnished != $data['furnished']) ||
			(isset($data['pet']) && $previous_data[0]->pet != $data['pet']) ||
			(isset($data['possession']) && $previous_data[0]->possession != $data['possession']) ||
			(isset($data['zoned']) && $previous_data[0]->zoned != $data['zoned']) ||
			(isset($data['bldg_type']) && $previous_data[0]->bldg_type != $data['bldg_type']) ||
			(isset($data['features1']) && $previous_data[0]->features1 != $data['features1']) ||
			(isset($data['features2']) && $previous_data[0]->features2 != $data['features2']) ||
			(isset($data['features3']) && $previous_data[0]->features3 != $data['features3']) ||
			(isset($data['price_type']) && $previous_data[0]->price_type != $data['price_type']) ||
			(isset($data['price1']) && $previous_data[0]->price1 != $data['price1']) ||
			(isset($data['price2']) && $previous_data[0]->price2 != $data['price2'])) {
				$send_notif = 1;
			}
		*/
		
		/*
		$user =& JFactory::getUser($_SESSION['user_id']);
		$task = JRequest::getVar('task');
		$thispocketid = "";
		$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
		// PREPARE POST VARIABLES
		foreach($_POST['jform'] as $key=>$value){
				if(is_array($value)){
					foreach($value as $k=>$v){
						$data[$key][$k] = trim(addslashes($v));
					}
				}
				else{
					$data[$key] = trim(addslashes($value));
				}
			}
		extract($data);
		*/
		$propertylisting_model = $this->getModel($this->getName());
		$pre_edit_data = $propertylisting_model->getPOPs_simple($_POST['lID']);		
		$propertylisting_model->get_previous_match_pop_to_buyers($pre_edit_data, $_POST['lID']);
		//$result = $propertylisting_model->deleteExistingFeatures($data, $_POST['lID']);
	    $result2 = $propertylisting_model->updatePocket($_POST['lID'], $user->id, $data);
		$result3 = $propertylisting_model->updatePropertyPrice($_POST['lID'], $data);
		$result4 = $propertylisting_model->updatePermissionSetting($_POST['lID'], $data);
		$propertylisting_model->deletePocketAddress($_POST['lID']);
		$listing_id = $_POST['lID'];
			foreach($address as $addr):
					if(!empty($addr)):
						$ret3 = $propertylisting_model->insertPocketAddress($_POST['lID'], $addr);
					endif;
			endforeach;
			if(count($_POST['jform_image']) >= 0):
					$propertylisting_model->deletePocketImages($_POST['lID']);
					foreach($_POST['jform_image'] as $k=>$v):
						if(is_numeric($k)) {
							$ret4 = $propertylisting_model->insertPocketImages($listing_id, $v, $_POST['jform_image_order'][$k]);
						}
					endforeach;
					$_SESSION['saved'] = true;
			endif;
		$activity = array(
					'user_id'		=> $user->id,
					'activity_type' => (int)1,
					'date'			=> date('Y-m-d H:i:s', time())
		);
		$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);
		$thispocketid = $listing_id;
		//$buyers = $propertylisting_model->getBuyerAll();
		$user_android_token = $propertylisting_model->getAppUserToken($user->id,'android');
		
		if($send_notif) {
			$noeditsdata = $propertylisting_model->getPOPs_simple($listing_id);				
			$propertylisting_model->match_pop_to_buyers($noeditsdata,$listing_id,$user_android_token,"edit");					
		}
		
		if(isset($_POST['admin_id'])) {
			$insert_act = new JObject();
			$insert_act->activity_type_id = 8;
			$insert_act->user_id = $_POST['admin_id'];
			$insert_act->activity_details = "Edited POPs " . $data['property_name'] . " (ID: " . $_POST['lID'] . ")";
			$db->insertObject('#__admin_activities', $insert_act);
		}
		
			
			//$this->android_send_notification($array_user_tokens,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
		//$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);
		die();
	}
	function editbuyerprofile(){
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$view = &$this->getView($this->getName(), 'html');
		$propertylisting_model = $this->getModel($this->getName());
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($_REQUEST['lID']);
		$user = JFactory::getUser();
		$user_info = $model2->get_user_registration($user->id);
		$application = JFactory::getApplication();
		if($buyers[0]->agent_id!=JFactory::getUser()->id) $application->redirect('index.php');
		if(!empty($_GET['buyer']))
			$individual = $propertylisting_model->getSelectedListing($_GET['buyer'], true);
		$property_type = JRequest::getVar('ptype');
		$property_subtype = JRequest::getVar('stype');
		$selected_ptype = !empty($property_type) ? $property_type: !empty($individual) ? $individual[0]->property_type : 0;
	//	$selected_stype = !empty($property_subtype) ? $property_subtype: !empty($individual) ? $individual[0]->sub_type : 0;
		$selected_stype = !empty($property_subtype) ? $property_subtype: $individual[0]->sub_type;
		if($selected_ptype)
			$subtypes = $propertylisting_model->getSubTypes($selected_ptype);

		foreach ($buyers[0]->needs[0] as $key => $value) {
			# code...
			$buyers[0]->{$key}=$value;
		}

		/*$langValues = $this->checkLangFiles();

		$getMeasurement = $model->getSqMeasureByCountry($buyers[0]->needs[0]->country);
		$getMeasurementUS = $model->getSqMeasureByCountry(223);
		$featurefields_array = array('bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','property_type','sub_type','unit_sqft','year_built','pool_spa','cap_rate','listing_class','parking_ratio','ceiling_height','room_count','type_lease','type_lease2','available_sqft','lot_sqft','lot_size','bldg_sqft','bldg_type','description');
		foreach ($buyers[0]->needs[0] as $key => $value) {
			# code...
			if(in_array($key,$featurefields_array)){

				foreach ($langValues as $key2 => $value2) {
					# code...
					//var_dump($value2."=".$value);
					if($value==$value2){
						//var_dump(trim($key2));
						$buyers[0]->needs[0]->{$key} = trim($key2);
					}
				}

				if(strpos($key, "sqft")){
					if(!$value || (!in_array($value,$getMeasurement['bldg']) && !in_array($value, $getMeasurement['lot']))){

						if((strpos($key, "lot") !== false)){

							foreach ($getMeasurementUS['lot'] as $key3 => $value3) {
								# code...
								if($value==$value3){
									$buyers[0]->needs[0]->{$key} = $getMeasurement['lot'][$key3];
								}

							}

						} else {

							foreach ($getMeasurementUS['bldg'] as $key2 => $value2) {
								# code...
								if($value==$value2){
									$buyers[0]->needs[0]->{$key} = $getMeasurement['bldg'][$key2];
								}
							}
						}

					}
				}
			}
		}*/


		$country = $propertylisting_model->getCountry($user->id);
		$ptype = $propertylisting_model->getAllPropertyTypes();
		$zone = $propertylisting_model->getAllStates();
		$currency_data = $this->getCountryCurrencyData($buyers[0]->needs[0]->country); 
		$getCountryLangsInitial = $this->getCountryDataByID($buyers[0]->needs[0]->country);
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		$view->assignRef( 'currency_data', $currency_data[0]);
		$view->assignRef( 'country', $country );
		$view->assignRef( 'user_info', $user_info );
		$view->assignRef( 'ptype', $ptype );
		$view->assignRef( 'zone', $zone );
		$view->assignRef( 'listing', $listing );
		$view->assignRef( 'data', $buyers[0]->needs[0]);
		$view->assignRef( 'property_type', $selected_ptype );
		$view->assignRef( 'sub_type', $selected_stype );
		$view->assignRef( 'sub_types', $subtypes );
		$view->assignRef( 'buyer', $buyers);
		$view->display($this->getTask());
	}
	function edit_buyer(){
		#echo "edit_buyer"; die();
		$db = JFactory::getDbo();
		$noeditsdata = $_REQUEST['jform'];
		$property = $_REQUEST['jform'];
		$buyer_id = $_REQUEST['jform']['bID'];
		unset($property['form']);
		unset($property['buyer_name']);
		unset($property['buyer_type']);
		unset($property['desc']);
		unset($property['price1']);
		unset($property['pricerange']);
		if(isset($property['price2']))
			unset($property['price2']);
		$buyer = new stdClass();
		$buyer->buyer_id = $buyer_id;
		$model = $this->getModel($this->getName());
		$previous_data = $model->getBuyer($buyer_id);
		$send_notif = 0;
		$buyerzip = explode(", ",$property['zip']);
		$buyerzip = implode(",", $buyerzip);
		$price_value = $_REQUEST['jform']['price1'].((isset($_REQUEST['jform']['price2'])) ? '-'.$_REQUEST['jform']['price2'] : '');
		
		/*$mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "Edit Buyer Apps" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( "Edited Data :" . print_r($property, true) . "Previous Data: " . print_r($previous_data, true));
		$mailSender ->Send(); */
		
		if(	($property['ptype'] != $previous_data[0]->needs[0]->property_type) ||
			($property['stype'] != $previous_data[0]->needs[0]->sub_type) ||
			(isset($data['price_type']) && $previous_data[0]->needs[0]->price_type != $data['price_type']) ||
			($buyerzip != $previous_data[0]->needs[0]->zip) ||
			($price_value != $previous_data[0]->needs[0]->price_value)
		) {
			$send_notif = 1;
		} else {
			switch($property['ptype']) {
				case 1:
					if($property['stype'] == 1) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
							(isset($property['view']) && $previous_data[0]->needs[0]->view != $property['view'])
						) {
							$send_notif = 1;
						}
					} else if($property['stype'] == 2) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['unit_sqft']) && $previous_data[0]->needs[0]->unit_sqft != $property['unit_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($property['stype'] == 3) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['unit_sqft']) && $previous_data[0]->needs[0]->unit_sqft != $property['unit_sqft'])
						) {
							$send_notif = 1;
						}											
					} else if($property['stype'] == 4) {
						if(	(isset($property['lot_size']) && $previous_data[0]->needs[0]->lot_size != $property['lot_size']) ||
							(isset($property['view']) && $previous_data[0]->needs[0]->view != $property['view']) || 
							(isset($property['zoned']) && $previous_data[0]->needs[0]->zoned != $property['zoned'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 2:
					if($property['stype'] == 5) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
							(isset($property['term']) && $previous_data[0]->needs[0]->term != $property['term']) ||
							(isset($property['possession']) && $previous_data[0]->needs[0]->possession != $property['possession']) ||
							(isset($property['furnished']) && $previous_data[0]->needs[0]->furnished != $property['furnished']) ||
							(isset($property['pet']) && $previous_data[0]->needs[0]->pet != $property['pet'])
						) {
							$send_notif = 1;
						}
					} else if($property['stype'] == 6) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['unit_sqft']) && $previous_data[0]->needs[0]->unit_sqft != $property['unit_sqft']) ||
							(isset($property['term']) && $previous_data[0]->needs[0]->term != $property['term']) ||
							(isset($property['furnished']) && $previous_data[0]->needs[0]->furnished != $property['furnished']) ||
							(isset($property['pet']) && $previous_data[0]->needs[0]->pet != $property['pet'])							
						) {
							$send_notif = 1;
						}
						
					} else if($property['stype'] == 7) {
						if(	(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
							(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
							(isset($property['unit_sqft']) && $previous_data[0]->needs[0]->unit_sqft != $property['unit_sqft']) ||
							(isset($property['term']) && $previous_data[0]->needs[0]->term != $property['term']) ||
							(isset($property['furnished']) && $previous_data[0]->needs[0]->furnished != $property['furnished']) ||
							(isset($property['pet']) && $previous_data[0]->needs[0]->pet != $property['pet'])							
						) {
							$send_notif = 1;
						}
					} else if($property['stype'] == 8) {
						if(	(isset($property['lot_size']) && $previous_data[0]->needs[0]->lot_size != $property['lot_size']) ||
							(isset($property['view']) && $previous_data[0]->needs[0]->view != $property['view']) || 
							(isset($property['zoned']) && $previous_data[0]->needs[0]->zoned != $property['zoned'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 3:
					if($property['stype'] == 9) {
						if(	(isset($property['units']) && $previous_data[0]->needs[0]->units != $property['units']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['grm']) && $previous_data[0]->needs[0]->grm != $property['grm']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
							(isset($property['lot_sqft']) && $previous_data[0]->needs[0]->lot_sqft != $property['lot_sqft'])
						) {
							$send_notif = 1;
						}
					} else if($property['stype'] == 10) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['listing_class']) && $previous_data[0]->needs[0]->listing_class != $property['listing_class']) || 
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($property['stype'] == 11) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($property['stype'] == 12) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($property['stype'] == 13) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['room_count']) && $previous_data[0]->needs[0]->room_count != $property['room_count'])
						) {
							$send_notif = 1;
						}					
					} else if($property['stype'] == 14) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
							(isset($property['room_count']) && $previous_data[0]->needs[0]->room_count != $property['room_count'])						
						) {
							$send_notif = 1;
						}					
					} else if($property['stype'] == 15) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate'])
						) {
							$send_notif = 1;
						}
					}
					break;
				case 4:
					if($property['stype'] == 16) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['listing_class']) && $previous_data[0]->needs[0]->listing_class != $property['listing_class']) || 
							(isset($property['available_sqft']) && $previous_data[0]->needs[0]->available_sqft != $property['available_sqft'])
						) {
							$send_notif = 1;
						}					
					} else if($property['stype'] == 17) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['type_lease']) && $previous_data[0]->needs[0]->type_lease != $property['type_lease']) ||
							(isset($property['available_sqft']) && $previous_data[0]->needs[0]->available_sqft != $property['available_sqft']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
							(isset($property['lot_sqft']) && $previous_data[0]->needs[0]->lot_sqft != $property['lot_sqft'])
						) {
							$send_notif = 1;
						}						
					} else if($property['stype'] == 18) {
						if(	(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
							(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
							(isset($property['type_lease']) && $previous_data[0]->needs[0]->type_lease != $property['type_lease'])
						) {
							$send_notif = 1;
						}						
					}
					break;
			}
		}
		
		/*
		if(	($property['ptype'] != $previous_data[0]->needs[0]->property_type) ||
			($property['stype'] != $previous_data[0]->needs[0]->sub_type) ||
			($buyerzip != $previous_data[0]->needs[0]->zip) ||
			($price_value != $previous_data[0]->needs[0]->price_value) ||
			(isset($property['bedroom']) && $previous_data[0]->needs[0]->bedroom != $property['bedroom']) || 
			(isset($property['bathroom']) && $previous_data[0]->needs[0]->bathroom != $property['bathroom']) || 
			(isset($property['unit_sqft ']) && $previous_data[0]->needs[0]->unit_sqft != $property['unit_sqft']) ||
			(isset($property['view']) && $previous_data[0]->needs[0]->view != $property['view']) || 
			(isset($property['style']) && $previous_data[0]->needs[0]->style != $property['style']) ||
			(isset($property['year_built']) && $previous_data[0]->needs[0]->year_built != $property['year_built']) ||
			(isset($property['pool_spa']) && $previous_data[0]->needs[0]->pool_spa != $property['pool_spa']) ||
			(isset($property['condition']) && $previous_data[0]->needs[0]->condition != $property['condition']) || 
			(isset($property['garage']) && $previous_data[0]->needs[0]->garage != $property['garage']) || 
			(isset($property['units']) && $previous_data[0]->needs[0]->units != $property['units']) ||
			(isset($property['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $property['cap_rate']) ||
			(isset($property['grm']) && $previous_data[0]->needs[0]->grm != $property['grm']) ||
			(isset($property['type']) && $previous_data[0]->needs[0]->type != $property['type']) ||
			(isset($property['listing_class']) && $previous_data[0]->needs[0]->listing_class != $property['listing_class']) || 
			(isset($property['parking_ratio']) && $previous_data[0]->needs[0]->parking_ratio != $property['parking_ratio']) ||
			(isset($property['ceiling_height']) && $previous_data[0]->needs[0]->ceiling_height != $property['ceiling_height']) ||
			(isset($property['stories']) && $previous_data[0]->needs[0]->stories != $property['stories']) ||
			(isset($property['room_count']) && $previous_data[0]->needs[0]->room_count != $property['room_count']) || 
			(isset($property['type_lease']) && $previous_data[0]->needs[0]->type_lease != $property['type_lease']) ||
			(isset($property['type_lease2']) && $previous_data[0]->needs[0]->type_lease2 != $property['type_lease2']) ||
			(isset($property['available_sqft']) && $previous_data[0]->needs[0]->available_sqft != $property['available_sqft']) ||
			(isset($property['lot_sqft']) && $previous_data[0]->needs[0]->lot_sqft != $property['lot_sqft']) ||
			(isset($property['lot_size']) && $previous_data[0]->needs[0]->lot_size != $property['lot_size']) ||
			(isset($property['bldg_sqft']) && $previous_data[0]->needs[0]->bldg_sqft != $property['bldg_sqft']) ||
			(isset($property['term']) && $previous_data[0]->needs[0]->term != $property['term']) ||
			(isset($property['furnished']) && $previous_data[0]->needs[0]->furnished != $property['furnished']) ||
			(isset($property['pet']) && $previous_data[0]->needs[0]->pet != $property['pet']) ||
			(isset($property['possession']) && $previous_data[0]->needs[0]->possession != $property['possession']) ||
			(isset($property['zoned']) && $previous_data[0]->needs[0]->zoned != $property['zoned']) ||
			(isset($property['bldg_type']) && $previous_data[0]->needs[0]->bldg_type != $property['bldg_type']) ||
			(isset($property['features1']) && $previous_data[0]->needs[0]->features1 != $property['features1']) ||
			(isset($property['features2']) && $previous_data[0]->needs[0]->features2 != $property['features2']) ||
			(isset($property['features3']) && $previous_data[0]->needs[0]->features3 != $property['features3']) ||
			(isset($property['pricerange']) && $previous_data[0]->needs[0]->price_type != $property['pricerange'])) {
		
			$send_notif = 1;
		
		}
		*/
		$edit_propertylisting_model = $this->getModel($this->getName());
		$this->arr_previous_matches = $edit_propertylisting_model->get_current_buyer_matching_pops($buyer_id);
		
		$buyer->name = $_REQUEST['jform']['buyer_name'];
		$buyer->price_value = $_REQUEST['jform']['price1'].((isset($_REQUEST['jform']['price2'])) ? '-'.$_REQUEST['jform']['price2'] : '');
		$buyer->buyer_type = $_REQUEST['jform']['buyer_type'];
		$buyer->desc = $_REQUEST['jform']['desc'];
		$buyer->agent_id = JFactory::getUser()->id;
		$buyer->price_type = $_REQUEST['jform']['pricerange'];
		$update = $db->updateObject('#__buyer', $buyer, 'buyer_id', false);
		$property['buyer_id'] = $buyer_id;
		
		$ret = $edit_propertylisting_model->deleteExistingNeeds($property, $buyer_id);
		$ret = $edit_propertylisting_model->updateBuyerNeeds($property, $buyer_id);
		//$currlID = $edit_propertylisting_model->getBuyer($buyer_id);
		$listing_id = $previous_data[0]->needs[0]->listing_id;
		$activity = array(
			'user_id'		=> JFactory::getUser()->id,
			'buyer_id'=> $buyer_id,
			'other_user_id'		=> JFactory::getUser()->id,
			'activity_type' => (int)24,
			'date'			=> date('Y-m-d H:i:s', time())
		);
		$ret = $edit_propertylisting_model->insertPropertyActivity($listing_id, $activity);		
		//sleep(3);
		$array_user_tokens = $edit_propertylisting_model->getAppUserToken(JFactory::getUser()->id,'android');	

		if($send_notif) {
			//$edit_propertylisting_model->resetBuyerListings2($buyer_id, 0);
			//$this->add_matching_pops($buyer_id, $buyer->price_value);
			$this->match_buyer_to_pops_controller($noeditsdata,$buyer_id,$array_user_tokens,"edit");
		}		
		die();
	}
	function edit_buyer_raw(){

		#echo "edit_buyer"; die();

		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($_POST['user_id']));
		$user =& JFactory::getUser();
		$uid =  $user->id ;


		$db = JFactory::getDbo();
		$data = $_POST;
		$buyer_id = $_POST['buyer_id'];
		$buyer_needs=$_POST;
		
		unset($buyer_needs['city']);
		unset($buyer_needs['state']);
		unset($buyer_needs['country']);
		unset($buyer_needs['currency']);
		unset($buyer_needs['symbol']);

		unset($buyer_needs['buyer_name']);
		unset($buyer_needs['buyer_type']);
		unset($buyer_needs['desc']);
		unset($buyer_needs['price1']);
		unset($buyer_needs['pricerange']);
		if(isset($buyer_needs['price2']))
			unset($buyer_needs['price2']);

		$propertylisting_model = $this->getModel($this->getName());

		$langValues = $propertylisting_model->checkLangFiles();

		$buyer = new stdClass();
		$buyer->buyer_id = $buyer_id;
		$buyer->name = $_POST['buyer_name'];
		$buyer->price_value = $_POST['price1'].((isset($_POST['price2'])) ? '-'.$_POST['price2'] : '');
		$buyer->buyer_type = $_POST['buyer_type'];
		$buyer->desc = $_POST['desc'];
		$buyer->agent_id = JFactory::getUser()->id;
		$buyer->price_type = $_POST['pricerange'];
		$update = $db->updateObject('#__buyer', $buyer, 'buyer_id', false);



	/*	if($data["units"]=="Duplex"){
			$data["units"]=2;
		} else if($data["units"]=="Triplex") {
			$data["units"]=3;
		} else if ($data["units"]=="Quad") {
			$data["units"]=4;
		}*/

		foreach ($buyer_needs as $key => $value) {

				if($key=="ptype"){

					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}

					$data[$key]=$propertylisting_model->getPtypeByName($value);

				}
				if($key=="stype"){

					foreach ($langValues as $key2 => $value2) {
						if($data["ptype"]==1 ||$data["ptype"]==3){
							if($value==$value2){
								$value = trim($key2);
							}
						} else {
							if($value==$value2 && strpos($key2, "L_") !== false){
								$value = trim($key2);
							}
						}

					}
					
					$data[$key]=$propertylisting_model->getSubTypeByName($data["ptype"],$value);
				}
				if($key=="Bedrooms"){
					$data["bedroom"]=$data[$key];
				}
				if($key=="Bathrooms"){
					$data["bathroom"]=$data[$key];
				}
				if($key=="Bldg. Sq. Ft." || $key=="Bldg__Sq__Ft" || $key=="bldg_sqft"){
					$data["bldgsqft"]=$data[$key];
				}
				if($key=="View"){
					$data["view"]=$data[$key];
				}
				if($key=="Unit_Sq__Ft_" || $key=="Unit Sq. Ft." || $key=="unit_sqft"){
					$data["unitsqft"]=$data[$key];
				}

				if($key=="Lot Size"){
					$data["lotsize"]=$data[$key];
				}
				if($key=="Zoned"){
					$data["zoned"]=$data[$key];
				}
				if($key=="Term"){
					$data["term"]=$data[$key];
				}
				if($key=="Possession"){
					$data["possession"]=$data[$key];
				}
				if($key=="Units"){
					$data["units"]=$data[$key];
				}

				if($key=="Cap Rate"){
					$data["caprate"]=$data[$key];
				}
				if($key=="GRM"){
					$data["grm"]=$data[$key];
				}
				if($key=="Lot Sq. Ft." || $key=="lot_sqft"){
					$data["lotsqft"]=$data[$key];
				}
				if($key=="Type"){
					$data["type"]=$data[$key];
				}
				if($key=="Class"){
					$data["class"]=$data[$key];
				}
				if($key=="Room Count"){
					$data["roomcount"]=$data[$key];
				}
				if($key=="Available Sq.Ft" || $key=="available_sqft"){
					$data["available"]=$data[$key];
				}
				if($key=="Type Lease"){
					$data["typelease"]=$data[$key];
				}
		}

		$noeditsdata = $data;

		/*		$mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "Edit POST" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( print_r($data,true));
		$mailSender ->Send(); */
		
		$previous_data = $propertylisting_model->getBuyer($buyer_id);
		
		$buyerzip = explode(", ",$data['zip']);
		$buyerzip = implode(",", $buyerzip);
		$price_value = $data['price1'].((isset($data['price2'])) ? '-'.$data['price2'] : '');
		
		/*$mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
		$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
		$mailSender ->setSubject( "Edit Buyer Apps" );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody( "Edited Data :" . print_r($data, true) . "Previous Data: " . print_r($previous_data, true));
		$mailSender ->Send(); */
		
		$send_notif = 0;
		if(	($data['ptype'] != $previous_data[0]->needs[0]->property_type) ||
			($data['stype'] != $previous_data[0]->needs[0]->sub_type) ||
			($buyerzip != $previous_data[0]->needs[0]->zip) ||
			($price_value != $previous_data[0]->needs[0]->price_value) ||
			(isset($data['bedroom']) && $previous_data[0]->needs[0]->bedroom != $data['bedroom']) || 
			(isset($data['bathroom']) && $previous_data[0]->needs[0]->bathroom != $data['bathroom']) || 
			(isset($data['unitsqft ']) && $previous_data[0]->needs[0]->unit_sqft != $data['unit_sqft']) ||
			(isset($data['view']) && $previous_data[0]->needs[0]->view != $data['view']) || 
			(isset($data['style']) && $previous_data[0]->needs[0]->style != $data['style']) ||
			(isset($data['year_built']) && $previous_data[0]->needs[0]->year_built != $data['year_built']) ||
			(isset($data['pool_spa']) && $previous_data[0]->needs[0]->pool_spa != $data['pool_spa']) ||
			(isset($data['condition']) && $previous_data[0]->needs[0]->condition != $data['condition']) || 
			(isset($data['garage']) && $previous_data[0]->needs[0]->garage != $data['garage']) || 
			(isset($data['units']) && $previous_data[0]->needs[0]->units != $data['units']) ||
			(isset($data['cap_rate']) && $previous_data[0]->needs[0]->cap_rate != $data['cap_rate']) ||
			(isset($data['grm']) && $previous_data[0]->needs[0]->grm != $data['grm']) ||
			(isset($data['type']) && $previous_data[0]->needs[0]->type != $data['type']) ||
			(isset($data['listing_class']) && $previous_data[0]->needs[0]->listing_class != $data['listing_class']) || 
			(isset($data['parking_ratio']) && $previous_data[0]->needs[0]->parking_ratio != $data['parking_ratio']) ||
			(isset($data['ceiling_height']) && $previous_data[0]->needs[0]->ceiling_height != $data['ceiling_height']) ||
			(isset($data['stories']) && $previous_data[0]->needs[0]->stories != $data['stories']) ||
			(isset($data['room_count']) && $previous_data[0]->needs[0]->room_count != $data['room_count']) || 
			(isset($data['type_lease']) && $previous_data[0]->needs[0]->type_lease != $data['type_lease']) ||
			(isset($data['type_lease2']) && $previous_data[0]->needs[0]->type_lease2 != $data['type_lease2']) ||
			(isset($data['available_sqft']) && $previous_data[0]->needs[0]->available_sqft != $data['available_sqft']) ||
			(isset($data['lotsqft']) && $previous_data[0]->needs[0]->lot_sqft != $data['lot_sqft']) ||
			(isset($data['lot_size']) && $previous_data[0]->needs[0]->lot_size != $data['lot_size']) ||
			(isset($data['bldgsqft']) && $previous_data[0]->needs[0]->bldg_sqft != $data['bldg_sqft']) ||
			(isset($data['term']) && $previous_data[0]->needs[0]->term != $data['term']) ||
			(isset($data['furnished']) && $previous_data[0]->needs[0]->furnished != $data['furnished']) ||
			(isset($data['pet']) && $previous_data[0]->needs[0]->pet != $data['pet']) ||
			(isset($data['possession']) && $previous_data[0]->needs[0]->possession != $data['possession']) ||
			(isset($data['zoned']) && $previous_data[0]->needs[0]->zoned != $data['zoned']) ||
			(isset($data['bldg_type']) && $previous_data[0]->needs[0]->bldg_type != $data['bldg_type']) ||
			(isset($data['features1']) && $previous_data[0]->needs[0]->features1 != $data['features1']) ||
			(isset($data['features2']) && $previous_data[0]->needs[0]->features2 != $data['features2']) ||
			(isset($data['features3']) && $previous_data[0]->needs[0]->features3 != $data['features3']) ||
			(isset($data['pricerange']) && $previous_data[0]->needs[0]->price_type != $data['pricerange']) ||
			(isset($data['price1']) && $previous_data[0]->needs[0]->price1 != $data['price1']) ||
			(isset($data['price2']) && $previous_data[0]->needs[0]->price2 != $data['price2'])) {
		
			$send_notif = 1;
		
		}


		
		$ret = $propertylisting_model->updateBuyerNeeds_raw($data, $buyer_id);
		//$currlID = $propertylisting_model->getBuyer($buyer_id);
		//$listing_id = $currlID[0]->needs[0]->listing_id;
		$listing_id = $previous_data[0]->needs[0]->listing_id;
		$activity = array(
			'user_id'		=> JFactory::getUser()->id,
			'buyer_id'=> $buyer_id,
			'other_user_id'		=> JFactory::getUser()->id,
			'activity_type' => (int)24,
			'date'			=> date('Y-m-d H:i:s', time())
		);
		$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);
		$array_user_tokens = $propertylisting_model->getAppUserToken(JFactory::getUser()->id,'android');
		//sleep(3);
		if($send_notif) {
			//$propertylisting_model->resetBuyerListings2($buyer_id, 0);
			//$this->add_matching_pops($buyer_id, $buyer->price_value);
			$this->match_buyer_to_pops_controller($noeditsdata,$buyer_id,$array_user_tokens,"edit");

		}		
		die();
	}
	public function addHomeBuyer(){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model_up =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$model = $this->getModel($this->getName());
		$user_info = $model_up->get_user_registration(JFactory::getUser()->id);
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$data = array();
		$data['home_buyer'] = 1;
		$data['buyer_name'] = "My Home Market";
		$data['buyer_type'] = "A Buyer";
		$data['ptype'] = "COM_POPS_PROPTYPE_RESP";
		$data['stype'] = "COM_POPS_PROPSTYPE_RESP_SFR";
		$data['bedroom'] = "2";
		$data['bathroom'] = "2";
		$data['bldgsqft']="1,000-1,999";
		$data["desc"]='Your "My Home Market" Buyer has been created based on your home market zip and average transaction value. This Buyer will automatically present to you all the matching POPs™ (Private, Off-market and Potential listings).';
		if(!$user_info->zip){
			$user_info->zip = $_POST['zipcode'];
		} 
		if($user_info->is_premium || isset($_GET['regpay'])){
			$data['user_id'] = JFactory::getUser()->id;
			//$data['zip'] = $user_info->zip;
			$data['country'] = $user_info->country;
			if($user_info->country==222){
				preg_match('/\d/', $user_info->zip, $m, PREG_OFFSET_CAPTURE);
		    	if (sizeof($m))
		        	 $m[0][1]; // 24 in your example
		        $data['zip']=substr($user_info->zip, 0, ($m[0][1]+1));
		        $otherhalf_key = substr($user_info->zip, ($m[0][1]+1), strlen($user_info->zip));
		        if(is_numeric($otherhalf_key[0])){
		        	$data['zip'] = $data['zip'].$otherhalf_key[0];
		        }
			} else {
				$data['zip'] = $user_info->zip;
			}
			 if ($user_info->verified_2014==1 && $user_info->verified_2015==1){ 
			 	$ave_sales_price=(($user_info->volume_2014/$user_info->sides_2014) + ($user_info->volume_2015/$user_info->sides_2015))/2;		
			 }  else if ($user_info->verified_2014==0 && $user_info->verified_2015==1){ 
				$ave_sales_price=$user_info->volume_2015/$user_info->sides_2015;
			 } else if ($user_info->verified_2014==1 && $user_info->verified_2015==0){ 
				$ave_sales_price=$user_info->volume_2014/$user_info->sides_2014;
			 } else if ($user_info->verified_2013==1 && $user_info->verified_2014==0 && $user_info->verified_2015==0){
				$ave_sales_price=$user_info->volume_2013/$user_info->sides_2013;
			 } else if ($user_info->verified_2012==1 && $user_info->verified_2013==0 && $user_info->verified_2014==0 && $user_info->verified_2015==0){
				$ave_sales_price=$user_info->volume_2012/$user_info->sides_2012;
			 } else { 
			 	$ave_sales_price=0;
			 } 	
			 $ave_sales_price = round($ave_sales_price);
			 $data['price1'] = $ave_sales_price*0.5;
			 $data['price2'] = $ave_sales_price*2.5;
			 $data['currency'] = $user_info->currency;

			 
			if($ave_sales_price>0 && $data['zip']){

				$_POST = $data;
				$_GET['webservice'] = 1;
				$this->add_buyer();
					

			    $url= JURI::base()."index.php/component/propertylisting/?task=add_buyer&format=raw&webservice=1";
			    $ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
			    $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent: curl/7.39.0');
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
				curl_setopt($ch, CURLOPT_POST, true);

    			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				curl_setopt($ch, CURLOPT_POSTFIELDS,  $data  );
				$response = curl_exec( $ch );
				$last = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
				if(curl_exec($ch) === false)
{
				    echo 'Curl error: ' . curl_error($ch);
				}
				else
				{
				    echo 'Operation completed without any errors';
				}
				curl_close( $ch );

				//echo "success".JURI::base();

				echo $last;
			} else {
				echo "failed";
			}
		} else {
			echo "failed";
		}
	}

	function remove_token(){
		$token = $_GET['device_token'];
		$db = JFactory::getDbo(); 
		$query = $db->getQuery(true);		 
		// delete all custom keys for user 1001.
		$conditions = array(
		    $db->quoteName('device_token') . ' = ' . $db->quote($token)
		);
		 
		$query->delete($db->quoteName('#__user_device_token'));
		$query->where($conditions);
		 
		$db->setQuery($query);

		$result = $db->execute();


	}

	function add_buyer(){
		$propertylisting_model = $this->getModel($this->getName());
		$langValues = $propertylisting_model->checkLangFiles();

		
		if(isset($_POST['user_id']) && isset($_GET['webservice'])){
			$user = JFactory::getUser();
			$session = JFactory::getSession();
			$session->set('user', new JUser($_POST['user_id']));
			$user =& JFactory::getUser();
			$uid =  $user->id ;
			$data = $_POST;

/*			$mailSender =& JFactory::getMailer();
	        $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
	        $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
	        $mailSender ->setSubject( "added_buyer" );
	        $mailSender ->isHTML(  true );
	        $mailSender ->setBody( print_r($data,true));
	        $mailSender ->Send(); 

*/

			foreach ($data as $key => $value) {
				if($key=="state"){
					$data[$key]=$propertylisting_model->getStateIdByName($value);
				}
				if($key=="ptype"){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}
					$data[$key]=$propertylisting_model->getPtypeByName($value);
				}
				if($key=="stype"){
					foreach ($langValues as $key2 => $value2) {
						if($data["ptype"]==1 ||$data["ptype"]==3){
							if($value==$value2){
								$value = trim($key2);
							}
						} else {
							if($value==$value2 && strpos($key2, "L_") !== false){
								$value = trim($key2);
							}
						}
					}
					$data[$key]=$propertylisting_model->getSubTypeByName($data["ptype"],$value);
				}
				if($key=="Bedrooms"){
					$data["bedroom"]=$data[$key];
				}
				if($key=="Bathrooms"){
					$data["bathroom"]=$data[$key];
				}
				if($key=="Bldg. Sq. Ft." || $key=="Bldg__Sq__Ft" || $key=="bldg_sqft"){
					$data["bldgsqft"]=$data[$key];
				}
				if($key=="View"){
					$data["view"]=$data[$key];
				}
				if($key=="Unit_Sq__Ft_" || $key=="Unit Sq. Ft." || $key=="unit_sqft"){
					$data["unitsqft"]=$data[$key];
				}
				if($key=="Lot Size"){
					$data["lotsize"]=$data[$key];
				}
				if($key=="Zoned"){
					$data["zoned"]=$data[$key];
				}
				if($key=="Term"){
					$data["term"]=$data[$key];
				}
				if($key=="Possession"){
					$data["possession"]=$data[$key];
				}
				if($key=="Units"){
					$data["units"]=$data[$key];

				}
				if($key=="Cap Rate"){
					$data["caprate"]=$data[$key];
				}
				if($key=="GRM"){
					$data["grm"]=$data[$key];
				}
				if($key=="Lot Sq. Ft." || $key=="lot_sqft" ){
					$data["lotsqft"]=$data[$key];
				}
				if($key=="Type"){
					$data["type"]=$data[$key];
				}
				if($key=="Class"){
					$data["class"]=$data[$key];
				}
				if($key=="Room Count"){
					$data["roomcount"]=$data[$key];
				}
				if($key=="Available Sq.Ft" || $key=="available_sqft"){
					$data["available"]=$data[$key];
				}
				if($key=="Type Lease"){
					$data["type_lease"]=$data[$key];
				}
			}

			// if($data["units"]=="Duplex"){
			// 	$data["units"]=2;
			// } else if($data["units"]=="Triplex") {
			// 	$data["units"]=3;
			// } else if ($data["units"]=="Quad") {
			// 	$data["units"]=4;
			// }

			

			$db = JFactory::getDbo();
			$property = $data;
			$noeditsdata = $data;
			unset($property['form']);
			unset($property['buyer_name']);
			unset($property['buyer_type']);
			unset($property['desc']);
			unset($property['price1']);
			unset($property['pricerange']);
			if(isset($property['price2']))
				unset($property['price2']);
			$buyer = new stdClass();
			$buyer->name = $db->escape($_POST['buyer_name']);
			$buyer->price_value = $_POST['price1'].((isset($_POST['price2'])) ? '-'.$_POST['price2'] : '');
			$buyer->buyer_type = $_POST['buyer_type'];
			$buyer->desc = $_POST['desc'];
			$buyer->agent_id = JFactory::getUser()->id;
			$buyer->price_type = $_POST['pricerange'];
			if($_POST['home_buyer']!=0){
				$buyer->home_buyer = $_POST['home_buyer'];
			} else {
				$buyer->home_buyer = 0;
			}
		} else {
			$db = JFactory::getDbo();
			$property = $_REQUEST['jform'];
			$noeditsdata = $_REQUEST['jform'];
			unset($property['form']);
			unset($property['buyer_name']);
			unset($property['buyer_type']);
			unset($property['desc']);
			unset($property['price1']);
			unset($property['pricerange']);
			if(isset($property['price2']))
				unset($property['price2']);
			$buyer = new stdClass();
			$buyer->name = $db->escape($_REQUEST['jform']['buyer_name']);
			$buyer->price_value = $_REQUEST['jform']['price1'].((isset($_REQUEST['jform']['price2'])) ? '-'.$_REQUEST['jform']['price2'] : '');
			$buyer->buyer_type = $_REQUEST['jform']['buyer_type'];
			$buyer->desc = $_REQUEST['jform']['desc'];
			$buyer->agent_id = JFactory::getUser()->id;
			$buyer->price_type = $_REQUEST['jform']['pricerange'];
		}
		
		$this->arr_previous_matches = array();
		
		$buyerId = $db->insertObject('#__buyer', $buyer);
		$buyerId = $db->insertid();
		$property['buyer_id'] = $buyerId;
		$listing_id = $propertylisting_model->insertBuyerNeeds($property);
		//$currlID = $propertylisting_model->getBuyer_simple($property['buyer_id']);
		// $listing_id = $currlID[0]->needs[0]->listing_id;
		//$listing_id = $ret_ID;
		$bID = $buyerId;
		//echo $buyerId; 
		//die();
		$activity = array(
			'user_id'		=> JFactory::getUser()->id,
			'buyer_id'=> $bID,
			'other_user_id'		=> JFactory::getUser()->id,
			'activity_type' => (int)23,
			'date'			=> date('Y-m-d H:i:s', time())
		);
		$array_user_tokens = $propertylisting_model->getAppUserToken(JFactory::getUser()->id,'android');	
		$propertylisting_model->insertPropertyActivity($listing_id, $activity);
		// $mailSender =& JFactory::getMailer();
  //       $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
  //       $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
  //       $mailSender ->setSubject( "noeditsdata" );
  //       $mailSender ->isHTML(  true );
  //       $mailSender ->setBody( print_r($noeditsdata,true));
  //       $mailSender ->Send(); 
		//sleep(3);
		//$this->add_matching_pops($buyerId, $buyer->price_value);
		 //$this->add_matching_pops_optimized($property);
		$this->match_buyer_to_pops_controller($noeditsdata,$buyerId,$array_user_tokens);
		die();
	}

	function match_buyer_to_pops_controller($noeditsdata,$buyerId,$array_user_tokens,$transaction="add"){

		$buyer = new stdClass();
		foreach ($noeditsdata as $key => $value)
		{
		    $buyer->$key = $value;
		}

		$model = $this->getModel($this->getName());

		if($transaction == "edit"){
			$buyer = $model->getBuyer_simple($buyerId);
			//$transaction = "edit";
		}


		$buyer->buyer_id = $buyerId;
		$buyer->property_name = $noeditsdata['property_name'];
		$buyer->price_value = $noeditsdata['price1']."-".$noeditsdata['price2'];
		$buyer->city = $noeditsdata['city'];
		$buyer->zip = $noeditsdata['zip'];
		$buyer->state = $noeditsdata['state'];
		$buyer->country = $noeditsdata['country'];
		$buyer->currency = $noeditsdata['currency'];
		$buyer->property_type = $noeditsdata['ptype'];
		$buyer->sub_type = $noeditsdata['stype'];
		$buyer->bedroom = $noeditsdata['bedroom'];
		$buyer->bathroom = $noeditsdata['bathroom'];
		$buyer->unit_sqft = $noeditsdata['unitsqft'];
		$buyer->view = $noeditsdata['view'];
		$buyer->style = $noeditsdata['style'];
		$buyer->year_built = $noeditsdata['yearbuilt'];
		$buyer->pool_spa = $noeditsdata['poolspa'];
		$buyer->condition = $noeditsdata['condition'];
		$buyer->garage = $noeditsdata['garage'];
		$buyer->units = $noeditsdata['units'];
		$buyer->cap_rate = $noeditsdata['cap'];
		$buyer->grm = $noeditsdata['grm'];
		$buyer->occupancy = $noeditsdata['occupancy'];
		$buyer->type = $noeditsdata['type'];
		$buyer->listing_class = $noeditsdata['class'];
		$buyer->parking_ratio = $noeditsdata['parking'];
		$buyer->ceiling_height = $noeditsdata['ceiling'];
		$buyer->stories = $noeditsdata['stories'];
		$buyer->room_count = $noeditsdata['roomcount'];
		$buyer->type_lease = $noeditsdata['typelease'];
		$buyer->type_lease2 = $noeditsdata['typelease2'];
		$buyer->available_sqft = $noeditsdata['available'];
		$buyer->lot_sqft = $noeditsdata['lotsqft'];
		$buyer->lot_size = $noeditsdata['lotsize'];
		$buyer->bldg_sqft = $noeditsdata['bldgsqft'];
		$buyer->term = $noeditsdata['term'];
		$buyer->furnished = $noeditsdata['furnished'];
		$buyer->pet = $noeditsdata['pet'];
		$buyer->zoned = $noeditsdata['zoned'];
		$buyer->possession = $noeditsdata['possession'];
		$buyer->bldg_type = $noeditsdata['bldgtype'];
		$buyer->features1 = $noeditsdata['features1'];
		$buyer->features2 = $noeditsdata['features2'];
		$buyer->features3 = $noeditsdata['features3'];
		$buyer->setting = $noeditsdata['settings'];


		$columns_query = array("p.listing_id",
							   "p.user_id",
							   "p.zip",
							   "p.country",
							   "p.property_type",
							   "p.sub_type",
							   "pp.price_type",                                            
							   "pp.price1",
							   "pp.price2",
							   "currcon.currency",
							   "currcon.symbol",
							   "udx.ios_device_tokens",
							   "ud.device_tokens");

		$required_attr = $model->get_columns_by_subtype($buyer->sub_type);

		foreach ($required_attr as $key => $value) {
			# code...
			$columns_query[]="p.".$value;
		}
				
		$returned_pockets = $model->match_buyer_to_pops($buyer,$columns_query);

		$new_total_pockets = count($returned_pockets);

		$activity_inserts = array();
		$listing_ids_array = array();

		//var_dump($returned_pockets);

		if(count($returned_pockets)){	

			foreach ($returned_pockets as $key => $value) {
				$listing_ids_array[] = $value->listing_id;
			}

			foreach ($returned_pockets as $key => $value) {
				
				
				# code...
				if($buyer->buyer_type!='Inactive' && !in_array($value->listing_id, $this->arr_previous_matches)) {
				
				//$model->insertToBuyerListings($buyer->buyer_id, $value->listing_id, $new_total_pockets);
				$activity = array(
					'user_id'		=> JFactory::getUser()->id,
					'buyer_id'=> $buyer->buyer_id,
					'other_user_id'		=> $value->user_id,
					'activity_type' => (int)25,
					'date'			=> date('Y-m-d H:i:s', time())
				);
				//print_r($activity);
				//$ret = $model->insertPropertyActivity($value->listing_id, $activity);
				$activity_inserts[] = $value->listing_id.", ".JFactory::getUser()->id.", ".$buyer->buyer_id.", ".$value->user_id.", 25, '".date('Y-m-d H:i:s', time())."'";
				//$listing_ids_array[] = $value->listing_id;
				if($value->device_tokens){
					$dev_tokens_arr = explode(",", $value->device_tokens);							
					foreach ($dev_tokens_arr as $key2 => $value2) {
						# code...
						$array_user_tokens[] = trim($value2);
						//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
					}
				}				
				$dev_tokens_arr = explode(",", $value->ios_device_tokens);


				if($dev_tokens_arr){
					$i=0;		
					foreach ($dev_tokens_arr as $key2 => $value2) {


						# code...
						if($buyer->buyer_name){
							$bname = $buyer->buyer_name;
						} else {
							$bname = $buyer->name;
						}
						
						$listing_ids_array = array_unique($listing_ids_array);

						$array_user_ios_tokens[$i]['devicetoken'] = trim($value2);
						//$array_user_ios_tokens[$i]['messages'] ="sample notif";
						if(count($listing_ids_array)==1){
							$array_user_ios_tokens[$i]['messages']= "A POPs™ in AgentBridge matches to ".$bname;
						} else {
							$array_user_ios_tokens[$i]['messages']= count($listing_ids_array)." POPs™ in AgentBridge matches to ".$bname;
						}
						
						//$array_user_ios_tokens[] = trim($value);
						//$array_buyer_ios_message[$buyer->agent_id][] = trim($value);
						//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
						$i++;
					}
				}		
			/*	$user_android_token = $model->getAppUserToken($listingi->user_id,'android');
				if($user_android_token)
					foreach ($user_android_token as $key => $value) {
						# code...
						$array_user_tokens[] = $value;
						//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
					} 
					*/
				}

			}

			
		}



		$model->insertToBuyerListings_optimized($buyer->buyer_id, $listing_ids_array, $transaction);
		if($activity_inserts){
			print_r($activity_inserts);
			//$model->insertToBuyerListings_optimized($buyer->buyer_id, $listing_ids_array, $transaction);
			$ret = $model->insertPropertyActivity_Multi($activity_inserts);
			if($array_user_tokens){
				//A POPs™ in AgentBridge matches to
				//$model->android_send_notification($array_user_tokens,"Buyer and POPs™ match","You have a new POPs™ match to a Buyer", "BuyerMatch");
				$listing_ids_array =array_unique($listing_ids_array);

				if($buyer->buyer_name){
						$bname = $buyer->buyer_name;
					} else {
						$bname = $buyer->name;
					}

				if(count($listing_ids_array)==1){
					
					$model->android_send_notification($array_user_tokens,"Buyer and POPs™ match","A POPs™ in AgentBridge matches to ".$bname."_idPOPs~".$buyer->buyer_id, "BuyerMatch");
				} else {
					$model->android_send_notification($array_user_tokens,"Buyer and POPs™ match",count($listing_ids_array)." POPs™ in AgentBridge matches to ".$bname."_idPOPs~".$buyer->buyer_id, "BuyerMatch");
				}
			}
			if($array_user_ios_tokens){
				//A POPs™ in AgentBridge matches to
				//$model->android_send_notification($array_user_tokens,"Buyer and POPs™ match","You have a new POPs™ match to a Buyer", "BuyerMatch");
				$listing_ids_array =array_unique($listing_ids_array);
				
				$model->ios_send_notification("Buyer and POPs™ match","test",$array_user_ios_tokens);
				//$model->ios_send_notification("Buyer and POPs™ match",count($listing_ids_array)." POPs™ in AgentBridge matches to ".$bname."_idPOPs~".$buyer->buyer_id,$array_user_ios_tokens);
				
			}
		} else {

			$db = JFactory::getDbo();
			$update = new JObject();
			$update->listingcount = count($listing_ids_array);
			$update->buyer_id = $buyer->buyer_id;	
			$update->hasnew = 0;
			$update->listingcount_new = 0;
			$update->hasnew_2 = 0;
			$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);
		}

		//updatebuyercounts
		$model->updateBuyerCounts();

		///echo "<pre>";
		//var_dump($activity_inserts);

	}


	private function add_matching_pops($buyer_id, $buyer_price) {
		$model = $this->getModel($this->getName());
		$buyers = $model->getBuyer($buyer_id);
		$buyertype = $buyers[0]->buyer_type;
		$listing = $model->get_pocket_listing_indi_filter_optimized(array('pp.price'=>$buyer_price,), $buyers);
		$price = explode('-', $buyer_price);
		$pocketlistings = array();
		$pocketlistings_0 = array();
		$pocketlistings_1 = array();

		$array_user_tokens = array();

		$b_user_android_token = $model->getAppUserToken($buyers[0]->agent_id,'android');
		if($b_user_android_token)
			foreach ($b_user_android_token as $key => $value) {
				# code...
				$array_user_tokens[] = $value;
				//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
			}
		//$totalcount = $model->countlisting($buyer_id);
		foreach($listing as $listingi) {
			if($buyertype!='Inactive') {
			
			//$model->insertToBuyerListings($buyer_id , $listingi->listing_id, $totalcount);
			$activity = array(
				'user_id'		=> JFactory::getUser()->id,
				'buyer_id'=> $buyer_id,
				'other_user_id'		=> $listingi->user_id,
				'activity_type' => (int)25,
				'date'			=> date('Y-m-d H:i:s', time())
			);
			print_r($activity);
			$ret = $model->insertPropertyActivity($listingi->listing_id, $activity);
			$user_android_token = $model->getAppUserToken($listingi->user_id,'android');
			if($user_android_token)
				foreach ($user_android_token as $key => $value) {
					# code...
					$array_user_tokens[] = $value;
					//$this->android_send_notification($value,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
				}


			}
		}

		//$this->android_send_notification($array_user_tokens,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
	}

	
	function newbuyerprofile(){
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$view = &$this->getView($this->getName(), 'html');
		$propertylisting_model = $this->getModel($this->getName());
		//$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$getCountryLangs = $this->getCountryLangs();
		$user = JFactory::getUser();
		//$user_info = $model2->get_profile_info($user->id);
		if(!empty($_GET['buyer']))
			$individual = $propertylisting_model->getSelectedListing($_GET['buyer'], true);
		$property_type = JRequest::getVar('ptype');
		$property_subtype = JRequest::getVar('stype');
		$selected_ptype = !empty($property_type) ? $property_type: !empty($individual) ? $individual[0]->property_type : 0;
		$selected_stype = !empty($property_subtype) ? $property_subtype: !empty($individual) ? $individual[0]->sub_type : 0;
		if($selected_ptype)
			$subtypes = $propertylisting_model->getSubTypes($selected_ptype);
		$country = $propertylisting_model->getCountry($user->id);
		$ptype = $propertylisting_model->getAllPropertyTypes();
		$zone = $propertylisting_model->getAllStates();
		$getCountryLangsInitial = $this->getCountryDataByID($country[0]->country);
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		//$view->assignRef( 'usercountry', $user_info->country );
		$view->assignRef( 'getCountryLangs', $getCountryLangs );
		$view->assignRef( 'country', $country );
		$view->assignRef( 'ptype', $ptype );
		$view->assignRef( 'zone', $zone );
		$view->assignRef( 'listing', $listing );
		$view->assignRef( 'data', $individual[0]);
		$view->assignRef( 'property_type', $selected_ptype );
		$view->assignRef( 'sub_type', $selected_stype );
		$view->assignRef( 'sub_types', $subtypes );
		$view->display($this->getTask());
	}
	function referrals() {
				$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$view = $this->getView($this->getName(), 'html');
		$model = $this->getModel($this->getName());
		$refstatus = $model->get_referral_status_list();
		$refin = $model->get_referral_in(JFactory::getUser()->id, $_POST['filter']);
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$ex_rates = $model->getExchangeRates_indi();
		$d_format = $this->getCountryDataByID($userDetails->country)->dateTimeFormat;
        #print_r($this->getCountryDataByID($userDetails->country));
        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;
		if(count($refin))
			foreach($refin as $ref){
				$userreg = $model2->get_user_registration($ref->agent_a);
				if($userreg->country == 223) {
					$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model2->getStateCode($userreg->state): $userreg->state);
				} else {
					$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model2->getState($userreg->state): $userreg->state);
				}
				$ref->other_user = $model2->get_user($ref->agent_a);
				$ref->other_user_reg = $model2->get_user_registration($ref->agent_a);
				$ref->other_user->userregsa = $model2->get_user_registration($ref->agent_a);
				if(JFactory::getUser()->id != $ref->agent_a){
					if($ref->currency!=$userDetails->currency){
						if(!$ref->currency){
							$ref->currency = "USD";
						}
						$ex_rates_con = $ex_rates->rates->{$ref->currency};
						$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
						//$ref->price_1=$model->getExchangeRates($ref->price_1,$userDetails->currency,$ref->currency);
						if($ref->currency=="USD"){									
							$ref->price_1=$ref->price_1 * $ex_rates_can;
						} else {
							$ref->price_1=($ref->price_1 / $ex_rates_con) * $ex_rates_can;
						}
						if($ref->price_2){
							//$ref->price_2=$model->getExchangeRates($ref->price_2,$userDetails->currency,$ref->currency);
							if($ref->currency=="USD"){								
								$ref->price_2=$ref->price_2 * $ex_rates_can;
							} else {
								$ref->price_2=($ref->price_2 / $ex_rates_con) * $ex_rates_can;
							}
						} 
						$ref->correctcurr = $userDetails->currency;
						$ref->symbol = $userDetails->symbol;
					} else {
						$ref->correctcurr = $ref->currency;
					}
				}
				$ref->other_user->desigs = $model2->get_user_designations($ref->agent_a);
				$ref->history =  $model->get_referral_history($ref->referral_id);
				foreach ($ref->history as $refs) {
					# code...
					switch($userDetails->country) {
						case 223:
						case 38:
						case 13:			
							$objTimeZone = $this->getTimeZone($userDetails->state);
							$timezone = $objTimeZone->timezone;
							break;
						case 222:
						case 103:
							$timezone = "Europe/London";
							break;
						case 204:
						case 81:
						case 141:
						case 73:
							$timezone = "Europe/Paris";
							break;
						default:
							$timezone = "America/Los_Angeles";
					}
					$new_date = new DateTime($refs->created_date, new DateTimeZone($timezone));
					$userTimeZone = new DateTimeZone($timezone);
					$offset = $userTimeZone->getOffset($new_date);
					#echo $d_format; exit;
					$refs->created_date = gmdate($d_format, strtotime($refs->created_date)+intval($offset));
					#$refs->created_date  = gmdate($d_format, strtotime($refs->created_date)-25200);
					$refs->name = stripslashes($refs->name);
					$refs->other_name = stripslashes($userDetails->name);
				}
				$ref->referral_info = $model->get_referral_info($ref->referral_id);
			}
		$refout = $model->get_referral_out(JFactory::getUser()->id, $_POST['filter']);
		if(count($refout))
			foreach($refout as $ref){
				$userreg = $model2->get_user_registration($ref->agent_b);  
				if($userreg->country == 223) {
					$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model2->getStateCode($userreg->state): $userreg->state);
				} else {
					$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model2->getState($userreg->state): $userreg->state);
				}
				$ref->other_user = $model2->get_user($ref->agent_b);
				$ref->other_user->userregsa = $model2->get_user_registration($ref->agent_b);
				$ref->other_user->desigs = $model2->get_user_designations($ref->agent_b);
				$ref->history =  $model->get_referral_history($ref->referral_id);
				foreach ($ref->history as $refs) {
					# code...
					switch($userDetails->country) {
						case 223:
						case 38:
						case 13:			
							$objTimeZone = $this->getTimeZone($userDetails->state);
							$timezone = $objTimeZone->timezone;
							break;
						case 222:
						case 103:
							$timezone = "Europe/London";
							break;
						case 204:
						case 81:
						case 141:
						case 73:
							$timezone = "Europe/Paris";
							break;
						default:
							$timezone = "America/Los_Angeles";
					}
					$new_date = new DateTime($refs->created_date, new DateTimeZone($timezone));
					$userTimeZone = new DateTimeZone($timezone);
					$offset = $userTimeZone->getOffset($new_date);
					#echo $d_format; exit;
					$refs->created_date = gmdate($d_format, strtotime($refs->created_date)+intval($offset));
					#$refs->created_date  = gmdate($d_format, strtotime($refs->created_date)-25200);
					$refs->name = stripslashes($refs->name);
					$refs->other_name = stripslashes($ref->other_user->name);
				}
				$ref->referral_info = $model->get_referral_info($ref->referral_id);
			}
		$refin_closed_count  = $model->inclosed_count(JFactory::getUser()->id);
		$userreg = $model2->get_user_registration(JFactory::getUser()->id);
		$view->assignRef('refin_closed_count', $refin_closed_count);
		$view->assignRef('user_info', $userreg);
		$view->assignRef('d_format', $d_format);
		$view->assignRef('refin', $refin);
		$view->assignRef('refout', $refout);
		$view->assignRef('country_list', $model2->get_countries());
		$view->assignRef('ref_status', $refstatus);
		$view->display($this->getTask());
	}
	public function test() {
		$model = $this->getModel($this->getName());
		$model->insertReferral($_REQUEST['jform'],$_REQUEST['uid']);
		JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_activitylog'));
		exit;
	}
	public function setting(){
		$propertylisting_model = $this->getModel($this->getName());
		$setting = $propertylisting_model->getPermissionSetting($_POST['lID']);
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('setting', $setting);
		$view->display($this->getTask());
		die();
	}
	function updatestatus(){
		$model = $this->getModel($this->getName());
		echo $model->update_ref_status($_REQUEST['value_id'], $_REQUEST['ref_id'], $_REQUEST['status'], $_REQUEST['reason'], htmlspecialchars($_REQUEST['note'], ENT_QUOTES, 'UTF-8', true));
		die();
	}
	function agreement(){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($_REQUEST['ref_id']);
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$d_format = $this->getCountryDataByID($userDetails->country)->dateFormat;
		$application = JFactory::getApplication();
		if(!in_array(JFactory::getUser()->id, array($referral->agent_a,$referral->agent_b))) $application->redirect('index.php');
		$referral->agent_a_info = $model2->get_user($referral->agent_a);
		$referral->agent_b_info = $model2->get_user($referral->agent_b);
		//$this->writepdf($referral->agent_a_info->name, $referral->agent_b_info->name, $referral->referral_fee, $_REQUEST['ref_id']);
		$signer = new HelloSign();
		$signer->login();
		$signer->setEnvelope($referral->docusign_envelope);
		//print_r($signer->getEmbeddedSigningLink());
		switch($userDetails->country) {
			case 223:
			case 38:
			case 13:			
				$objTimeZone = $this->getTimeZone($userDetails->state);
				$timezone = $objTimeZone->timezone;
				break;
			case 222:
			case 103:
				$timezone = "Europe/London";
				break;
			case 204:
			case 81:
			case 141:
			case 73:
				$timezone = "Europe/Paris";
				break;
			default:
				$timezone = "America/Los_Angeles";
		}
		$view = $this->getView($this->getName(), 'html');
		$view->assignRef('d_format', $d_format);
		$view->assignRef('ref_id', $_REQUEST['ref_id']);
		$view->assignRef('referral', $referral);
		$view->assignRef('timezone', $timezone);
		$otheruser = ($referral->agent_a == JFactory::getUser()->id) ? $referral->agent_b: $referral->agent_a;
		$view->assignRef('document', $signer->getEmbeddedSigningLink($_REQUEST['ref_id'], $otheruser, JFactory::getUser()->id));
		$view->display($this->getTask());
	}
	function generate_page1signed($refid, $date){
		 $indent4 = "&nbsp;&nbsp;&nbsp;&nbsp;";
		 $userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		 JModelLegacy::addIncludePath( $userprofilemodel );
		 $model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		 $activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		 JModelLegacy::addIncludePath( $activitylogmodel );
		 $model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		 $refstatus = $model3->get_referral_status_object($refid);
		 $model = $this->getModel($this->getName());
		 $referral = $model->get_referral($refid);
		 $agentA_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_a)->email);
		 $agentB_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_b)->email);
		 
		 if(JFactory::getUser($referral->agent_a)->currLanguage !== JFactory::getUser($referral->agent_b)->currLanguage) {
			$language->load($extension, $base_dir, "english-US");
		 }
		 
		 $agentA = new stdClass();
		 $agentA->registration = $model2->get_user($referral->agent_a);
		 $agentA->user_table = JFactory::getUser($referral->agent_a);
		 //$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_a));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_a, true),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_a));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_a, true),array($agentA->registration->countryCode));
		 $agentB = new stdClass();
		 $agentB->registration = $model2->get_user($referral->agent_b);
		 $agentB->user_table = JFactory::getUser($referral->agent_b);
		 //$agentB->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_b));
		$agentB->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_b, true),array($agentB->registration->countryCode));
		//$agentB->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_b));
		$agentB->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_b, true),array($agentB->registration->countryCode));
		 $client = $model2->get_client($referral->client_id);
		 $client->email = array_map(function($object){return $object->email;}, $client->email);
		 //$client->mobile = array_map(function($object){return $object->number;}, $client->mobile);
		$client->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->number;}, $client->mobile,array($client->address[0]->countryCode));
		 $client->address = ((trim($client->address[0]->address_1)!="") ? $client->address[0]->address_1." ".$client->address[0]->address_2.", ":"").$client->address[0]->city." ".(is_numeric($client->address[0]->state) ? $model2->get_state_name($client->address[0]->state) : $client->address[0]->state)." ".$client->address[0]->zip.", ".$model2->getCountry($client->address[0]->country);
		 $refexplode = explode('%',$referral->referral_fee);
		 $head = $this->generateHeader($referral->referral_id, $date, 1);
		return $head."
			<table border=\"0\">
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_PARAGRAPH1', stripslashes_all($agentA_broker->broker_name), stripslashes_all($agentB_broker->broker_name), stripslashes_all($client->name)) . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td>" . JText::_('COM_AGREEMENT_IDOFPARTIES') . "</td>
					<td colspan=\"2\"></td>
				</tr>
				<tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_REFERRING_AGENT_BROKER') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentA_broker->broker_name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER_STATE_LICENSE_NUM') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentA->registration->brokerage_license )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentA->user_table->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_LICENSE') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentA->registration->licence )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".(count($agentA->mobile) ? implode(',',$agentA->mobile) : 'n/a')." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".$agentA->registration->email." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_RECEIVING_AGENT_BROKER') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentB_broker->broker_name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER_STATE_LICENSE_NUM') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt($agentB->registration->brokerage_license)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentB->user_table->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_LICENSE') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentB->registration->licence )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".(count($agentB->mobile) ? implode(',',$agentB->mobile) : 'n/a')." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".$agentB->registration->email." </td>
				</tr>
		        <tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_PRINCIPAL') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_NAME') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($client->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".implode(',',$client->mobile)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".implode(',', $client->email)." </td>
				</tr>".
((isset($client->address) && $client->address!="") ?
"
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_ADDRESS') . ":</td>
					<td colspan=\"2\"> ".$client->address." </td>
				</tr>
" : "")."
		        <tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_PRINCIPAL_IS') . ":</td>
					<td colspan=\"2\"> ".JText::_($referral->intention)." </td>
				</tr>
		        <tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGREED_REFERRAL_FEE') . ":</td>
					<td colspan=\"2\"> ".$referral->referral_fee." </td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PRINCIPAL_CONSENT') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PRINCIPAL_CONSENT_PARAGRAPH') . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_AGREEMENT') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_AGREEMENT_PARAGRAPH', ucfirst(convert_number_to_words_global($refexplode[0])), $referral->referral_fee) . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PAYMENT_DUE_DATE') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PAYMENT_DUE_DATE_PARAGRAPH') . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PAYMENT_FEE_PERMISSIBLE') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PAYMENT_FEE_PERMISSIBLE_PARAGRAPH') . "</td>
				</tr>
			</table>		
			";
	}
	function generate_page2signed($refid, $date, $o_signed,$r_signed){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$refstatus = $model3->get_referral_status_object($refid);
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($refid);
		
		if(JFactory::getUser($referral->agent_a)->currLanguage !== JFactory::getUser($referral->agent_b)->currLanguage) {
			$language->load($extension, $base_dir, "english-US");
		 }
		
		$head = $this->generateHeader($refid, $date);
		$agentA = new stdClass();
		$agentA->registration = $model2->get_user($referral->agent_a);
		$agentA->user_table = JFactory::getUser($referral->agent_a);
		//$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_a));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_a),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_a));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_a),array($agentA->registration->countryCode));
		$agentB = new stdClass();
		$agentB->registration = $model2->get_user($referral->agent_b);
		$agentB->user_table = JFactory::getUser($referral->agent_b);
		//$agentB->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_b));
		$agentB->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_b),array($agentB->registration->countryCode));
		//$agentB->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_b));
		$agentB->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_b),array($agentB->registration->countryCode));
		$body="<table border=\"0\">
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_ENFORCEMENT_VENUE') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_ENFORCEMENT_VENUE_PARAGRAPH', $model2->getState($agentA->registration->state)) . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"line-height: 3px\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_INDEMNIFICATION_AGENTBRIDGE') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_INDEMNIFICATION_AGENTBRIDGE_PARAGRAPH') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"line-height: 3px\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_MISCELLANEOUS') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_MISCELLANEOUS_PARAGRAPH') . "</td>
			</tr>
		</table>";

		$signee_name_a="";
		$signee_name_b="";

		if($o_signed || $referral->agent_a == JFactory::getUser()->id){
			$signee_name_a=stripslashes_all($agentA->user_table->name);
		}

		if($r_signed || $referral->agent_b == JFactory::getUser()->id){
			$signee_name_b=stripslashes_all($agentB->user_table->name);
		}

		$body .= "
			<table border=\"0\" width=\"100%\">
				<tr>
					<td colspan=\"7\" style=\"line-height: 3px\"> </td>
				</tr>
				<tr>
					<td colspan = \"7\">
						<strong>" . JText::_('COM_AGREEMENT_SIGNATURES') . "</strong>
					</td>
				</tr>
				<tr>
					<td colspan = \"3\">
						" . JText::_('COM_AGREEMENT_REFERRING_AGENT_BROKER_LOWERCASE') . "
					</td>
					<td>&nbsp;</td>
					<td colspan = \"3\">
						" . JText::_('COM_AGREEMENT_RECEIVING_AGENT_BROKER_LOWERCASE') . "
					</td>
				</tr>
				<tr>
					<td colspan = \"7\" style=\"line-height: 10px\">&nbsp;</td>
				</tr>
				<tr>
					<td colspan = \"3\" style=\"border-bottom: 1px solid black\" >
					<span style=\"font-family: alleanascript;font-style: italic;font-size: 35pt;\"> ".$signee_name_a."</span>
					</td>
					<td>&nbsp;</td>
					<td colspan = \"3\" style=\"border-bottom: 1px solid black\">
					<span style=\"font-family: alleanascript;font-style: italic;font-size: 35pt;\"> ".$signee_name_b."</span>
					</td>
				</tr>
				<tr>
					<td colspan=\"2\">".stripslashes_all($agentA->user_table->name)."</td>
					<td></td>
					<td>&nbsp;</td>
					<td colspan=\"2\">".stripslashes_all($agentB->user_table->name)."</td>
					<td></td>
				</tr>
			</table>";
		return $head.$body;
	}

	function generateRefSignedPDF($filename,$str){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('AgentBridge');
		$pdf->SetTitle('Referral Agreement');
		$pdf->SetSubject('REFERRAL');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// set default font subsetting mode
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 10, '', true);
		foreach ($str as $data){
			$pdf->AddPage();
			$pdf->writeHTMLCell(0, 0, '', '', $data, 0, 1, 0, true, '', true);
		}
		//$filename = JFactory::getUser()->id."-referral-".microtime(true);
		$pdf->Output("referrals/$filename.pdf", 'F');
		//$pdf->out
		return $filename;
	}

	function test_updatePDF($filename,$signature){
		$refid = $_GET['ref_id'];
		$model = &$this->getModel('PropertyListing');
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$thisuserinfo = $model2->get_user_registration(JFactory::getUser()->id);
		$d_format = $this->getCountryDataByID($thisuserinfo->country)->dateFormat;
		$date = date("Y-m-d H:i:s");
		//echo date("Y-m-d H:i:s") . "<br />";
		//echo $date . "<br />";
		switch($userDetails->country) {
			case 223:
			case 38:
			case 13:			
				$objTimeZone = $this->getTimeZone($thisuserinfo->state);
				$timezone = $objTimeZone->timezone;
				break;
			case 222:
			case 103:
				$timezone = "Europe/London";
				break;
			case 204:
			case 81:
			case 141:
			case 73:
				$timezone = "Europe/Paris";
				break;
			default:
				$timezone = "America/Los_Angeles";
		}
		$new_date = new DateTime($date, new DateTimeZone($timezone));
		$userTimeZone = new DateTimeZone($timezone);
		$offset = $userTimeZone->getOffset($new_date);
		$date = gmdate($d_format, strtotime($date)+intval($offset));

		$refstatus = $model->get_referral_history($refid);
/*		$o_signed=0;
		$r_signed=0;
		foreach ($refstatus as $key => $value) {
			# code...
			if($value->o_signed==1){
				$o_signed=1;
			}
			if($value->r_signed==1){
				$r_signed=1;
			}
		}
		*/
		$signed_both_array_o_signed=array();
		$signed_both_array_r_signed=array();
		$signed_both=0;
		$signed_o=0;
		$signed_r=0;
		$old_date=0;
		foreach ($refstatus as $key => $value) {
			# code...
			if(strtotime($value->created_date) < strtotime('-90 days')) {
			     // this is true
				$old_date=1;
			 }
			$signed_both_array_o_signed[$value->update_id][]=$value->o_signed;
			$signed_both_array_r_signed[$value->update_id][]=$value->r_signed;
			
		}

		if($this->in_array_r(1 , $signed_both_array_o_signed)){
		    // found!
		    $signed_o=1;
		   
		}

		if($this->in_array_r(1 , $signed_both_array_r_signed)){
		    // found!
		     $signed_r=1;
		}
		

		$referral = $model->get_referral($refid);
		$filename = $referral->refcontract_filename;
		$pages = array();
		$pages[] = $this->generate_page1signed($refid,$date);
		$pages[] = $this->generate_page2signed($refid,$date,$signed_o,$signed_r);
		//var_dump($pages);
		$filename = $this->generateRefSignedPDF($filename,$pages);
	
	}
	function test_editpdf($filename,$signature){
		ob_start();
		$model = $this->getModel($this->getName());
		$refid = $_GET['ref_id'];
		$referral = $model->get_referral($refid);
		$filename = $referral->refcontract_filename;
		$full_filename = JPATH_ROOT.'/referrals/'.$filename.'.pdf';
		//$font_filepath = JPATH_ROOT."/templates/agentbridge/fonts/arizonia/arizonia.ttf";
		$font_filepath = JPATH_ROOT."/templates/agentbridge/fonts/jellyka/jellyka.ttf";		
		$line1 = "Mark Roynell Obre";
		// initiate FPDI
		$pdf = new FPDI();		
		// set the source file
		$pagecount=$pdf->setSourceFile($full_filename);
		// add a page
		$pdf->setPrintHeader(false);
   		$pdf->setPrintFooter(false);
		//$pdf->AddFont('arizonia', '', $font_filepath , true);
		//$pdf->AddFont('helveticai', '', K_PATH_FONTS.'helvetica.php', true);
   		//$fontname = $pdf->addTTFfont($font_filepath,'TrueTypeUnicode', 'ansi', 32);
		//$fontname = TCPDF_FONTS::addTTFfont('../common/pdf/fonts/persianFont/HiwebNazanin.ttf', 'TrueTypeUnicode', 'ansi', 32);
		$pdf->AddPage();
		for ($n = 1; $n <= $pagecount; $n++) {
			if($n==2){
			//Add the header
			$pdf->SetFont("helveticai");			
			$w=$pdf->GetStringWidth($line1)+6;
    		//$pdf->SetX((210-$w)/2);			
			//$pdf->SetXY(125, 186); 
			$pdf->SetXY(125, 195); 
	/*		$pdf->SetDrawColor(0,80,180);
			$pdf->SetFillColor(200,220,255);*/
			$pdf->SetTextColor(0, 0, 0);			
			$pdf->SetLineWidth(1);
			$pdf->Write(0, $line1);
			//$pdf->Cell($w,9,$line1,1,1,'C',true);
			//$pdf->MultiCell(0, '', $line1 , 0 , '' , false);
			}			
			//Import 1 page and set as a template			
			$tplidx = $pdf->ImportPage($n);			
			$size = $pdf->useTemplate($tplidx);
			if ($n != $pagecount) {			
				$pdf->AddPage();      		
			}
		}
		ob_end_flush(); ob_end_clean();
		$pdf->Output($full_filename, 'f');
/*		// import page 1
		$tplIdx = $pdf->importPage(1);
		// use the imported page and place it at position 10,10 with a width of 100 mm
		$pdf->useTemplate($tplIdx, 10, 10, 100);

		// now write some text above the imported page
		$pdf->SetFont('Helvetica');
		$pdf->SetTextColor(255, 0, 0);
		$pdf->SetXY(30, 30);
		$pdf->Write(0, 'This is just a simple text');
		ob_end_flush(); ob_end_clean();
		$pdf->Output();
*/
	}

	function sendEmailReferralPend(){
		$outside_key=$_GET['qkey'];
		$outside_key_en = urldecode(base64_decode($outside_key));
		$inside_key="@g3nTBr!dg320l7";
		$inside_key_en = urlencode(base64_encode($inside_key));

		if($inside_key!=$outside_key_en || !isset($_GET['qkey'])) JFactory::getApplication()->redirect('index.php?illegal=1'); 
		#QGczblRCciFkZzMyMGw3

		$model = $this->getModel($this->getName());
		$referrals = $model->get_pendingReferrals();

		if($referrals)
		foreach ($referrals as $key => $value) {
			# code...

			$body .= "Hello Admin,<br /><br />";

    		$body .= "Referral ".$value->clientName." created on ".date("m-d-Y", strtotime($value->date))." by ".$value->agentA_name." to ".$value->agentB_name." has been Pending for 10 Days now. 
    				  No action has been done by any of the parties since then. Please follow up.<br /><br />";
    		$body .= "Thank you";
    		
    		$email = array('mark.obre@keydiscoveryinc.com' );
			//$email = array('francis.alincastre@keydiscoveryinc.com','mark.obre@keydiscoveryinc.com', );
    		$subject = 'Referral '.$value->clientName.' Pending';
       	
			$mailSender = &JFactory::getMailer();
			$mailSender->addRecipient( $email );
			$mailSender->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
			$mailSender->setSubject( $subject );
			$mailSender->isHTML(  true );
			$mailSender->setBody(  $body );			
			$mailSender->Send();
			
		}		
	}

	function changeRefNoGo(){
		$outside_key=$_GET['qkey'];
		$outside_key_en = urldecode(base64_decode($outside_key));
		$inside_key="@g3nTBr!dg320l7";
		$inside_key_en = urlencode(base64_encode($inside_key));

		if($inside_key!=$outside_key_en || !isset($_GET['qkey'])) JFactory::getApplication()->redirect('index.php?illegal=1'); 
		$model = $this->getModel($this->getName());
		$referrals = $model->get_noGoReferrals();

		//var_dump($referrals);
		if($referrals)
		foreach ($referrals as $key => $value) {
			# code...
			#$model->update_ref_status($_REQUEST['value_id'], $_REQUEST['ref_id'], $_REQUEST['status'], $_REQUEST['reason'], htmlspecialchars($_REQUEST['note'], ENT_QUOTES, 'UTF-8', true));
			/*
			value_id:0
			ref_id:224
			note:
			reason:1
			status:5
			*/
			$model->update_ref_status(0, $value->referral_id, 5, 1, "");
		}		
	}

	function in_array_r($item , $array){
	    return preg_match('/"'.$item.'"/i' , json_encode($array));
	}

	function agreement_ab(){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($_REQUEST['ref_id']);
		
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$d_format = $this->getCountryDataByID($userDetails->country)->dateFormat;
		$application = JFactory::getApplication();
		if(!in_array(JFactory::getUser()->id, array($referral->agent_a,$referral->agent_b))) $application->redirect('index.php');
		$referral->agent_a_info = $model2->get_user($referral->agent_a);
		$referral->agent_b_info = $model2->get_user($referral->agent_b);

	
		$refstatus = $model->get_referral_history($_REQUEST['ref_id']);
		
		$signed_both_array_o_signed=array();
		$signed_both_array_r_signed=array();
		$signed_both=0;
		$signed_o=0;
		$signed_r=0;
		$old_date=0;
		foreach ($refstatus as $key => $value) {
			# code...
			if(strtotime($value->created_date) < strtotime('-90 days')) {
			     // this is true
				$old_date=1;
			 }
			$signed_both_array_o_signed[$value->update_id][]=$value->o_signed;
			$signed_both_array_r_signed[$value->update_id][]=$value->r_signed;
			
		}



		if($this->in_array_r(1 , $signed_both_array_o_signed)){
		    // found!
		    $signed_o=1;
		   
		}

		if($this->in_array_r(1 , $signed_both_array_r_signed)){
		    // found!
		     $signed_r=1;
		}
		
		if($signed_o && $signed_r || $old_date ){
			$signed_both=1;
			$signed_o=1;
			$signed_r=1;
		}
		


		//$this->writepdf($referral->agent_a_info->name, $referral->agent_b_info->name, $referral->referral_fee, $_REQUEST['ref_id']);
		/*$signer = new HelloSign();
		$signer->login();
		$signer->setEnvelope($referral->docusign_envelope);*/
		//print_r($signer->getEmbeddedSigningLink());
		
		switch($userDetails->country) {
			case 223:
			case 38:
			case 13:			
				$objTimeZone = $this->getTimeZone($userDetails->state);
				$timezone = $objTimeZone->timezone;
				break;
			case 222:
			case 103:
				$timezone = "Europe/London";
				break;
			case 204:
			case 81:
			case 141:
			case 73:
				$timezone = "Europe/Paris";
				break;
			default:
				$timezone = "America/Los_Angeles";
		}

		$filename = $referral->refcontract_filename;




		// instantiate Imagick 
		if (!extension_loaded('imagick'))
    		echo 'imagick not installed';

		/* phpinfo(); */

		$im = new Imagick();
 		$im->setResolution(300,300);
		$im->readimage(JPATH_ROOT.'/referrals/'.$filename.'.pdf[0]'); 
		$im->setImageFormat('jpg');    
		$im->writeImage(JPATH_ROOT.'/referrals/temp_images/'.$filename.'_thumb_one.jpg'); 
		$im->clear(); 
		$im->destroy();


		 $im2 = new Imagick();
 		$im2->setResolution(300,300);
		$im2->readimage(JPATH_ROOT.'/referrals/'.$filename.'.pdf[1]'); 
		$im2->setImageFormat('jpg');    
		$im2->writeImage(JPATH_ROOT.'/referrals/temp_images/'.$filename.'_thumb_two.jpg'); 
		$im2->clear(); 
		$im2->destroy();

		
		
		$view = $this->getView($this->getName(), 'html');
		$view->assignRef('signed_both', $signed_both);
		$view->assignRef('signed_o', $signed_o);
		$view->assignRef('signed_r', $signed_r);
		$view->assignRef('d_format', $d_format);
		$view->assignRef('ref_id', $_REQUEST['ref_id']);
		$view->assignRef('referral', $referral);
		$view->assignRef('timezone', $timezone);
		$otheruser = ($referral->agent_a == JFactory::getUser()->id) ? $referral->agent_b: $referral->agent_a;
	
		$view->assignRef('filename', $filename);
		$view->display($this->getTask());
	}

	function download(){
		$config = new JConfig();
		$path = $config->referralsDir.'/'; // change the path to fit your websites document structure
		$fullPath = $path.$_GET['download_file'];
		if ($fd = fopen ($fullPath, "r")) {
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);
			switch ($ext) {
				case "pdf":
					header("Content-type: application/pdf");
					header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a download
					break;
				default; // Other document formats (doc, docx, odt, ods etc)
				header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
				header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
			}
			header("Content-length: $fsize");
			header("Cache-control: private"); //use this to open files directly
			while(!feof($fd)) {
				$buffer = fread($fd, 2048);
				echo $buffer;
			}
		}
		fclose ($fd);
		exit;
	}
	function decrypt($encrypted_text) {
		$key = 'password to (en/de)crypt';
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return $decrypted;
	}
	function debug(){
		$model = $this->getModel($this->getName());
		print_r($model->getBuyerPageDetails(JFactory::getUser()->id));
		die();
	}
	function generate_invoice_document($refid, $invoice, $amount, $whopaid){
		$date = date("m-d-y");
		$indent4 = "&nbsp;&nbsp;&nbsp;&nbsp;";
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 = JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 = JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($refid);
		$agentA_broker = $model2->get_broker_info(JFactory::getUser($whopaid)->email);
		$agentA = new stdClass();
		$agentA->registration = $model2->get_user($whopaid);
		$agentA->user_table = JFactory::getUser($whopaid);
		//$ref->mobile_d = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($ref->agent_a),array($userreg->countryCode));
		//$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($whopaid));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($whopaid),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($whopaid));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($whopaid),array($agentA->registration->countryCode));
		$client = $model2->get_client($referral->client_id);
		$client->email = array_map(function($object){return $object->email;}, $client->email);
		//$client->mobile = array_map(function($object){return $object->number;}, $client->mobile);
		$client->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->number;}, $client->mobile,array($client->address[0]->countryCode));
		$client->address = ((trim($client->address[0]->address_1)!="") ? $client->address[0]->address_1." ".$client->address[0]->address_2.", ":"").$client->address[0]->city." ".(is_numeric($client->address[0]->state) ? $model2->get_state_name($client->address[0]->state) : $client->address[0]->state)." ".$client->address[0]->zip.", ".$model2->getCountry($client->address[0]->country);
		$refexplode = explode('%',$referral->referral_fee);
		$cardno = $model->get_payment_profile($whopaid)->paymentProfile->payment->creditCard->cardNumber;
		$cardno = $cardno[0];
		return
		"<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">
			<tr>
				<td rowspan=\"4\" colspan=\"4\">
					<img alt=\"logo\" src=\"https://www.agentbridge.com/images/logoheader.jpg\"/>
				</td>
				<td colspan=\"2\"> </td>
				<td colspan=\"3\" style=\"font-weight: bold; text-align: center\">
					Invoice
				</td>
			</tr>
			<tr>
				<td colspan=\"8\"> </td>
			</tr>
				<tr>
				<td colspan=\"8\"> </td>
			</tr>
			<tr>
				<td colspan=\"8\"> </td>
			</tr>
			<tr>
				<td style=\"font-weight: bold\">Bill To:</td>
				<td colspan=\"5\"> </td>
				<td colspan=\"3\" style=\"font-weight: bold; text-align:center;\">Customer Information</td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"3\">".$agentA_broker->broker_name."</td>
				<td colspan=\"3\" style=\"text-align: right\">Invoice No.</td>
				<td colspan=\"2\">$indent4".$invoice."</td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"3\">".stripslashes_all($agentA->user_table->name)."</td>
				<td colspan=\"3\" style=\"text-align: right\">Customer Name:</td>
				<td colspan=\"2\">$indent4".$client->name."</td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"3\">".$agentA->registration->street_address.((trim($agentA->registration->suburb!=""))?(", ".$agentA->registration->suburb):"")."</td>
				<td colspan=\"3\" style=\"text-align: right\">Customer ID:</td>
				<td colspan=\"2\">$indent4".str_pad($client->buyer_id, 10, "0", STR_PAD_LEFT)."</td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"3\">".$agentA->registration->city.", ".(is_numeric($agentA->registration->state) ? $model2->getState($agentA->registration->state)  : $agentA->registration->state )." ".$agentA->registration->zip."</td>
				<td colspan=\"5\"> </td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"3\">".$model2->getCountry($agentA->registration->country)."</td>
				<td colspan=\"5\"> </td>
			</tr>
			<tr>
				<td width=\"8%\"></td>
				<td width=\"11%\"></td>
				<td width=\"11%\"></td>
				<td width=\"14%\"></td>
				<td width=\"9%\"></td>
				<td width=\"11%\"></td>
				<td width=\"13%\"></td>
				<td width=\"11%\"></td>
				<td width=\"12%\"></td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"2\" style=\"font-weight: bold; text-align: center; \">Invoice Date</td>
				<td colspan=\"2\" style=\"font-weight: bold; text-align: center;\">Invoice Number</td>
				<td colspan=\"2\" style=\"font-weight: bold; text-align: center;\">Due Date</td>
				<td colspan=\"2\" style=\"font-weight: bold; text-align: center;\">Terms</td>
			</tr>
			<tr>
				<td> </td>
				<td colspan=\"2\" style=\"text-align: center;\">$date</td>
				<td colspan=\"2\" style=\"text-align: center;\">$invoice</td>
				<td colspan=\"2\" style=\"text-align: center;\">$date</td>
				<td colspan=\"2\" style=\"text-align: center;\"></td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td style=\"font-weight: bold; text-align: center; \">Product</td>
				<td style=\"font-weight: bold; text-align: center;\">Contract</td>
				<td style=\"font-weight: bold; text-align: center;\">Start Date</td>
				<td style=\"font-weight: bold; text-align: center;\">End Date</td>
				<td colspan=\"2\" style=\"font-weight: bold; text-align: center;\">Reference</td>
				<td style=\"font-weight: bold; text-align: center;\">Unit Price</td>
				<td style=\"font-weight: bold; text-align: center;\">Qty</td>
				<td style=\"font-weight: bold; text-align: center;\">Amount</td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td style=\"text-align: center; \">Referral</td>
				<td style=\"text-align: center;\">#$refid</td>
				<td style=\"text-align: center;\">$date</td>
				<td style=\"text-align: center;\">$date</td>
				<td colspan=\"2\" style=\"text-align: center;\">#$refid, $client->name</td>
				<td style=\"text-align: right;\">".format_currency_invoice_global($amount)."</td>
				<td style=\"text-align: right;\">1</td>
				<td style=\"text-align: right;\">".format_currency_invoice_global($amount)."</td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"4\" valign=\"center\" style=\"font-size:18px\">If you have any questions concerning this invoice, please call:</td>
				<td> </td>
				<td> </td>
				<td style=\"text-align:right\">Subtotal</td>
				<td> </td>
				<td style=\"text-align:right; \">".format_currency_invoice_global($amount)."</td>
			</tr>
			<tr>
				<td colspan=\"4\" valign=\"center\" style=\"font-size:18px\">The Accounts Receivable Department at (310) 421-2812</td>
				<td colspan=\"3\" style=\"text-align:right; font-size:30px\">Charged to Credit Card: $cardno</td>
				<td> </td>
				<td style=\"text-align:right;\">[".format_currency_invoice_global($amount)."]</td>
			</tr>
			<tr style=\"line-height: 3px;\">
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				 <td colspan=\"5\"> </td>
				 <td colspan=\"2\" style=\"font-weight:bold; text-align:right\">TOTAL DUE</td>
				 <td> </td>
				 <td style=\"text-align:right; font-weight:bold\">".(0)."</td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\" style=\"text-align:center; border-bottom: 1px dotted black; font-size: 16px\">Retain this portion for your records</td>
			</tr>
			<tr>
				<td colspan=\"9\" style=\"text-align:center; font-size: 16px\">*Please detach this stub and return with your payments*</td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"4\">".$agentA_broker->broker_name."</td>
				<td colspan=\"5\"> </td>
			</tr>
			<tr>
				<td colspan=\"4\">".stripslashes_all($agentA->user_table->name)."</td>
				<td colspan=\"5\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">".$agentA->registration->street_address.((trim($agentA->registration->suburb!=""))?(", ".$agentA->registration->suburb):"")."</td>
				<td colspan=\"6\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">".$agentA->registration->city.", ".(is_numeric($agentA->registration->state) ? $model2->getState($agentA->registration->state)  : $agentA->registration->state )." ".$agentA->registration->zip."</td>
				<td colspan=\"2\"> </td>
				<td colspan=\"3\">Please make all checks payable to:</td>
				<td colspan=\"1\"> </td>
			</tr>
			<tr>
				<td colspan=\"2\">".$model2->getCountry($agentA->registration->country)."</td>
				<td colspan=\"3\"> </td>
				<td colspan=\"2\" style=\"font-weight:bold\">AgentBridge, LLC</td>
				<td colspan=\"2\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td style=\"font-weight:bold; text-align:center;\">Cust #</td>
				<td style=\"font-weight:bold; text-align:center;\" colspan=\"2\">Customer Name</td>
				<td style=\"font-weight:bold; text-align:center;\">Invoice Name</td>
				<td style=\"font-weight:bold; text-align:center;\" colspan=\"2\">Due Date</td>
				<td style=\"font-weight:bold; text-align:center;\">Amount Due</td>
				<td style=\"font-weight:bold; text-align:center;\" colspan=\"2\">Amount Enclosed</td>
			</tr>
			<tr style=\"line-height: 3px;\">
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td style=\"text-align:center; \">$whopaid</td>
				<td style=\"text-align:center;\" colspan=\"2\">".stripslashes_all($agentA->user_table->name)."</td>
				<td style=\"text-align:center;\">$invoice</td>
				<td style=\"text-align:center;\" colspan=\"2\">$date</td>
				<td style=\"text-align:right;\">".format_currency_invoice_global($amount)."</td>
				<td style=\"text-align:center;\" colspan=\"2\"> </td>
			</tr>
			<tr>
				<td colspan=\"9\"> </td>
			</tr>
			<tr>
				<td colspan=\"5\"> </td>
				<td colspan=\"2\" style=\"font-weight:bold\">Remit to:</td>
				<td colspan=\"2\"> </td>
			</tr>
			<tr>
				<td colspan=\"5\"> </td>
				<td colspan=\"3\">AgentBridge, LLC</td>
				<td> </td>
			</tr>
			<tr>
				<td colspan=\"5\"> </td>
				<td colspan=\"3\">Attn: Accounts Receivable</td>
				<td > </td>
			</tr>
			<tr>
				<td colspan=\"5\"> </td>
				<td colspan=\"3\">608 Silver Spur</td>
				<td > </td>
			</tr>
			<tr>
				<td colspan=\"5\"> </td>
				<td colspan=\"3\">Rolling Hills Estates, CA 90274</td>
				<td > </td>
			</tr>
		</table>";
	}
	function generate_payment_method($refid, $date, $grosscom){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($refid);
		$fee = explode('%',$referral->referral_fee);
		$topayr1 = $grosscom * ('.'.$fee[0]);
		$agentA_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_a)->email);
		$agentB_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_b)->email);
		$agentA = new stdClass();
		$agentA->registration = $model2->get_user($referral->agent_a);
		$agentA->user_table = JFactory::getUser($referral->agent_a);
		//$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_a));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_a),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_a));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_a),array($agentA->registration->countryCode));
		$agentB = new stdClass();
		$agentB->registration = $model2->get_user($referral->agent_b);
		$agentB->user_table = JFactory::getUser($referral->agent_b);
		//$agentB->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_b));
		$agentB->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_b),array($agentB->registration->countryCode));
		//$agentB->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_b));
		$agentB->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_b),array($agentB->registration->countryCode));
		$client = $model2->get_client($referral->client_id);
		$client->email = array_map(function($object){return $object->email;}, $client->email);
		//$client->mobile = array_map(function($object){return $object->number;}, $client->mobile);
		$client->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->number;}, $client->mobile,array($client->address[0]->countryCode));
		$client->address = ((trim($client->address[0]->address_1)!="") ? $client->address[0]->address_1." ".$client->address[0]->address_2.", ":"").$client->address[0]->city." ".(is_numeric($client->address[0]->state) ? $model2->get_state_name($client->address[0]->state) : $client->address[0]->state)." ".$client->address[0]->zip.", ".$model2->getCountry($client->address[0]->country);
		$refexplode = explode('%',$referral->referral_fee);
		$indent4 = "&nbsp;&nbsp;&nbsp;&nbsp;";
		$indent8 = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		return
		"<table border=\"0\">
		<tr>
		<td rowspan=\"4\" style=\"text-align:center\">
		<img src=\"https://www.agentbridge.com/images/logoheader.jpg\"/>
		</td>
		<td style=\"font-weight: bold;\">
		INSTRUCTIONS TO PAY AT CLOSING
		</td>
		</tr>
		<tr>
		<td>
		Agreement Date: $date
		</td>
		</tr>
		<tr>
		<td>
		Referral of ".stripslashes_all($client->name)."
		</td>
		</tr>
		<tr>
		<td>
		Agent Bridge Referral ID: ".$referral->agent_b."
		</td>
		</tr>
		<tr>
		<td colspan=\"2\" style=\"text-align: justify\">
		Referencing AGREEMENT TO PAY REFERRAL FEE (\"Agreement\") by and between ".stripslashes_all($agentA_broker->broker_name).", (\"Referring Broker\") and ".stripslashes_all($agentB_broker->broker_name)." (\"Receiving Broker\") with regard to the referral of ".stripslashes_all($client->name).".
		</td>
		</tr>
		<tr>
		<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		</tr>
		<tr>
		<td>$indent4 IDENTIFICATIONS OF PARTIES</td>
		<td colspan=\"2\"></td>
		</tr>
		<tr>
		<td style=\" font-weight: bold\">$indent4 REFERRING AGENT/BROKER</td>
		<td colspan=\"2\"> </td>
		</tr>
		<tr>
		<td >$indent8 Broker:</td>
		<td colspan=\"2\">".stripslashes_all($agentA_broker->broker_name)."</td>
		</tr>
		<tr>
		<td >$indent8 Broker State License Number:</td>
		<td colspan=\"2\"> ".$this->decrypt($agentA->registration->brokerage_license)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent:</td>
		<td colspan=\"2\"> ".stripslashes_all($agentA->user_table->name)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent License:</td>
		<td colspan=\"2\"> ".$this->decrypt($agentA->registration->licence)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent Mobile:</td>
		<td colspan=\"2\"> ".(count($agentA->mobile) ? implode(',',$agentA->mobile) : 'n/a')." </td>
		</tr>
		<tr>
		<td >$indent8 Agent email:</td>
		<td colspan=\"2\"> ".$agentA->registration->email." </td>
		</tr>
		<tr>
		<td >$indent8 TAX ID Number:</td>
		<td colspan=\"2\">".$this->decrypt($agentA->registration->tax_id_num)." </td>
		</tr>
		<tr>
		<td style=\" font-weight: bold\">$indent4 RECEIVING AGENT/BROKER</td>
		<td colspan=\"2\"> </td>
		</tr>
		<tr>
		<td >$indent8 Broker:</td>
		<td colspan=\"2\">".stripslashes_all($agentB_broker->broker_name)."</td>
		</tr>
		<tr>
		<td >$indent8 Broker State License Number:</td>
		<td colspan=\"2\"> ".$this->decrypt($agentB->registration->brokerage_license)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent:</td>
		<td colspan=\"2\"> ".stripslashes_all($agentB->user_table->name)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent License:</td>
		<td colspan=\"2\"> ".$this->decrypt($agentB->registration->licence)." </td>
		</tr>
		<tr>
		<td >$indent8 Agent Mobile:</td>
		<td colspan=\"2\"> ".(count($agentB->mobile) ? implode(',',$agentB->mobile) : 'n/a')." </td>
		</tr>
		<tr>
		<td >$indent8 Agent e-mail:</td>
		<td colspan=\"2\"> ".$agentB->registration->email." </td>
		</tr>
		<tr>
		<td >$indent8 TAX ID Number:</td>
		<td colspan=\"2\"> ".$this->decrypt($agentB->registration->tax_id_num)." </td>
		</tr>
		<tr>
		<td style=\" font-weight: bold\">$indent4 PRINCIPAL</td>
		<td colspan=\"2\"> </td>
		</tr>
		<tr>
		<td >$indent8 Name:</td>
		<td colspan=\"2\"> ".stripslashes_all($client->name)." </td>
		</tr>
		<tr>
		<td >$indent8 Mobile:</td>
		<td colspan=\"2\"> ".implode(',',$client->mobile)." </td>
		</tr>
		<tr>
		<td >$indent8 Email:</td>
		<td colspan=\"2\"> ".implode(',', $client->email)." </td>
		</tr>
		".((isset($client->address) && $client->address!="") ?
		"
		<tr>
		<td >$indent8 Address:</td>
		<td colspan=\"2\"> ".$client->address." </td>
		</tr>
		" : "")."
		<tr>
		<td >$indent8 Principal is:</td>
		<td colspan=\"2\"> ".$referral->intention." </td>
		</tr>
		<tr>
		<td >$indent8 Agreed Referral Fee:</td>
		<td colspan=\"2\"> ".$referral->referral_fee." </td>
		</tr>
		<tr>
		<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		</tr>
		<tr>
		<td style=\" font-weight: bold\">$indent4 TRANSACTION DETAILS</td>
		<td colspan=\"2\"> </td>
		</tr>
		<tr>
		<td >$indent8 Client Transaction Completed</td>
		<td colspan=\"2\">$date</td>
		</tr>
		<tr>
		<td >$indent8 Gross Commission of Sale</td>
		<td colspan=\"2\">".format_currency_global($grosscom)."</td>
		</tr>
		<tr>
		<td >$indent8 Referral Fee Percentage</td>
		<td colspan=\"2\">".$referral->referral_fee."</td>
		</tr>
		<tr>
		<td>$indent8 <span style=\"font-size:12\">Referral Fee</span></td>
		<td colspan=\"2\" style=\"font-weight: bold\">".format_currency_global($topayr1)."</td>
		</tr>
		<tr>
		<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		</tr>
		<tr>
		<td colspan=\"2\" style=\"text-align: justify\">
		Please Issue Payment to ".stripslashes_all($agentA_broker->broker_name)." care of ".stripslashes_all($agentA->user_table->name)." for client ".stripslashes_all($client->name)." in amount of ".format_currency_global($topayr1)."
		</td>
		</tr>
		<tr>
		<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		</tr>
		<tr>
		<td colspan=\"2\" style=\"font-weight: bold\">
		Mail to:
		</td>
		</tr>
		<tr>
		<td colspan=\"2\">
		".stripslashes_all($agentA_broker->broker_name)."
		</td>
		</tr>
		<tr>
		<td colspan=\"2\">
		".$agentA->registration->street_address.((trim($agentA->registration->suburb!=""))?(", ".$agentA->registration->suburb):"")."
		</td>
		</tr>
		<tr>
		<td colspan=\"2\">
		".$agentA->registration->city.", ".(is_numeric($agentA->registration->state) ? $model2->getState($agentA->registration->state)  : $agentA->registration->state )." ".$agentA->registration->zip."
		</td>
		</tr>
		<tr>
		<td colspan=\"2\">
		".$model2->getCountry($agentA->registration->country)."
		</td>
		</tr>
		<tr>
		<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		</tr>
		<tr>
		<td colspan=\"2\">
		Please enclose Closing Statement or HUD 1 with check.   If wire transfer preferred, please email ".stripslashes_all($agentA->user_table->name)." at ".$agentA->registration->email." to receive wire instructions.
		</td>
		</tr>
		</table>";
	}
	function writepdf_invoice($str){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Aldrin Bautista');
		$pdf->SetTitle('Test');
		$pdf->SetSubject('REFERRAL');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// set default font subsetting mode
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 10, '', true);
		$pdf->AddPage();
		$pdf->writeHTMLCell(0, 0, '', '', $str, 0, 1, 0, true, '', true);
		$filename = JFactory::getUser()->id."-invoice-".microtime(true);
		$pdf->Output("referrals/$filename.pdf", 'F');
		return $filename;
	}
	function writepdf_pay_method($str){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Aldrin Bautista');
		$pdf->SetTitle('Test');
		$pdf->SetSubject('REFERRAL');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// set default font subsetting mode
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 10, '', true);
		$pdf->AddPage();
		$pdf->writeHTMLCell(0, 0, '', '', $str, 0, 1, 0, true, '', true);
		$filename = JFactory::getUser()->id."-paymethod-".microtime(true);
		$pdf->Output("referrals/$filename.pdf", 'F');
		return $filename;
	}
	function writepdf_v3($str){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Aldrin Bautista');
		$pdf->SetTitle('Test');
		$pdf->SetSubject('REFERRAL');
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
		// set default font subsetting mode
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 10, '', true);
		foreach ($str as $data){
			$pdf->AddPage();
			$pdf->writeHTMLCell(0, 0, '', '', $data, 0, 1, 0, true, '', true);
		}
		$filename = JFactory::getUser()->id."-referral-".microtime(true);
		$pdf->Output("referrals/$filename.pdf", 'F');
		//$pdf->out
		return $filename;
	}
	function generateHeader($refid, $date, $first = 0){
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		
		
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($refid);
		
		if(JFactory::getUser($referral->agent_a)->currLanguage !== JFactory::getUser($referral->agent_b)->currLanguage) {
			$language->load($extension, $base_dir, "english-US");
		 }
		
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$client = $model2->get_client($referral->client_id);
		$client->email = array_map(function($object){return $object->email;}, $client->email);
		//$client->mobile = array_map(function($object){return $object->number;}, $client->mobile);
		$client->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->number;}, $client->mobile,array($client->address[0]->countryCode));
		$client->address = ((trim($client->address[0]->address_1)!="") ? $client->address[0]->address_1." ".$client->address[0]->address_2.", ":"").$client->address[0]->city." ".(is_numeric($client->address[0]->state) ? $model2->get_state_name($client->address[0]->state) : $client->address[0]->state)." ".$client->address[0]->zip.", ".$model2->getCountry($client->address[0]->country);
		return "
			<table border=\"0\">
				<tr>
					<td rowspan=\"4\" width=\"40%\"><img src=\"https://www.agentbridge.com/images/logoheader.jpg\"/></td>
					<td colspan=\"2\" style=\"font-weight: bold\">" . JText::_('COM_AGREEMENT_HEADER_TITLE') . (($first) ? "" : JText::_('COM_AGREEMENT_HEADER_CONT'))."</td>
				</tr>
				<tr>
					<td colspan=\"2\" style=\"\">" . JText::_('COM_AGREEMENT_HEADER_DATE') . ": $date</td>
				</tr>
				<tr>
					<td colspan=\"2\" style=\"\">" . JText::sprintf('COM_AGREEMENT_HEADER_REFERRAL_OF', stripslashes_all($client->name)) . "</td>
				</tr>
				<tr>
					<td colspan=\"2\" style=\"\">" . JText::_('COM_AGREEMENT_HEADER_AGENTBRIDGE_REFERRAL_ID') . ": ".$refid."</td>
				</tr>
				<tr>
					<td colspan=\"2\"> &nbsp; </td>		
				</tr>
			</table>";
	}
	function checkPartialGBCode(){
		$postcode = $_POST['partialCode'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(*)');
		$query->from($db->quoteName('#__country_sp_address'));
		$query->where($db->quoteName('postcode')." = ".$db->quote($postcode));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		$count = $db->loadResult();
		if($count<=0){
			$db = JFactory::getDbo();
			$query2 = $db->getQuery(true);
			$query2->select('COUNT(*)');
			$query2->from($db->quoteName('#__country_partialszip_states'));
			$query2->where($db->quoteName('postcode')." = ".$db->quote($postcode));
			$db->setQuery($query2);
			$count = $db->loadResult();
		}
		echo $count;
	}
	function getPartialGBCodeStates(){
		$postcode = $_POST['partialCode'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__country_partialszip_states'));
		$query->where($db->quoteName('postcode')." = ".$db->quote($postcode));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		$states_obj = $db->loadAssoc();
		$states_ret = array();
		$keys_not_included=array("postcode","country","id");
		foreach ($states_obj as $key => $value) {
			if(!in_array($key, $keys_not_included) && $value){
				$states_ret[]=$value;
			}
		}
		echo json_encode($states_ret);
	}
	function getCountryLangs(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id  ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObjectList();
	}
	function getCountryDataByID($country){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id WHERE cl.countries_id = ".$country." ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObject();
	}
	function getCountryCurrencyData($cID){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->setQuery("
				SELECT cur.* FROM #__country_currency AS cur WHERE country=".$cID);
			$db->setQuery($query);		
			return $db->loadObjectList();
	}
	function generate_page2($refid, $date){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$refstatus = $model3->get_referral_status_object($refid);
		$model = $this->getModel($this->getName());
		$referral = $model->get_referral($refstatus->referral_id);
		
		if(JFactory::getUser($referral->agent_a)->currLanguage !== JFactory::getUser($referral->agent_b)->currLanguage) {
			$language->load($extension, $base_dir, "english-US");
		 }
		
		$head = $this->generateHeader($refstatus->referral_id, $date);
		$agentA = new stdClass();
		$agentA->registration = $model2->get_user($referral->agent_a);
		$agentA->user_table = JFactory::getUser($referral->agent_a);
		//$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_a));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_a),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_a));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_a),array($agentA->registration->countryCode));
		$agentB = new stdClass();
		$agentB->registration = $model2->get_user($referral->agent_b);
		$agentB->user_table = JFactory::getUser($referral->agent_b);
		//$agentB->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_b));
		$agentB->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_b),array($agentB->registration->countryCode));
		//$agentB->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_b));
		$agentB->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_b),array($agentB->registration->countryCode));
		$body="<table border=\"0\">
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_ENFORCEMENT_VENUE') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_ENFORCEMENT_VENUE_PARAGRAPH', $model2->getState($agentA->registration->state)) . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"line-height: 3px\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_INDEMNIFICATION_AGENTBRIDGE') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_INDEMNIFICATION_AGENTBRIDGE_PARAGRAPH') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"line-height: 3px\"> </td>
			</tr>
			<tr>
				<td colspan=\"3\">" . JText::_('COM_AGREEMENT_MISCELLANEOUS') . "</td>
			</tr>
			<tr>
				<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_MISCELLANEOUS_PARAGRAPH') . "</td>
			</tr>
		</table>";
		$body .= "
			<table border=\"0\" width=\"100%\">
				<tr>
					<td colspan=\"7\" style=\"line-height: 3px\"> </td>
				</tr>
				<tr>
					<td colspan = \"7\">
						<strong>" . JText::_('COM_AGREEMENT_SIGNATURES') . "</strong>
					</td>
				</tr>
				<tr>
					<td colspan = \"3\">
						" . JText::_('COM_AGREEMENT_REFERRING_AGENT_BROKER_LOWERCASE') . "
					</td>
					<td>&nbsp;</td>
					<td colspan = \"3\">
						" . JText::_('COM_AGREEMENT_RECEIVING_AGENT_BROKER_LOWERCASE') . "
					</td>
				</tr>
				<tr>
					<td colspan = \"7\" style=\"line-height: 10px\">&nbsp;</td>
				</tr>
				<tr>
					<td colspan = \"3\" style=\"border-bottom: 1px solid black\" >
						&nbsp;
					</td>
					<td>&nbsp;</td>
					<td colspan = \"3\" style=\"border-bottom: 1px solid black\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan=\"2\">".stripslashes_all($agentA->user_table->name)."</td>
					<td></td>
					<td>&nbsp;</td>
					<td colspan=\"2\">".stripslashes_all($agentB->user_table->name)."</td>
					<td></td>
				</tr>
			</table>";
		return $head.$body;
	}
	function generate_page1($refid, $date){
		 $indent4 = "&nbsp;&nbsp;&nbsp;&nbsp;";
		 $userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		 JModelLegacy::addIncludePath( $userprofilemodel );
		 $model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		 $activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		 JModelLegacy::addIncludePath( $activitylogmodel );
		 $model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		 $refstatus = $model3->get_referral_status_object($refid);
		 $model = $this->getModel($this->getName());
		 $referral = $model->get_referral($refstatus->referral_id);
		 $agentA_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_a)->email);
		 $agentB_broker = $model2->get_broker_info(JFactory::getUser($referral->agent_b)->email);
		 
		 if(JFactory::getUser($referral->agent_a)->currLanguage !== JFactory::getUser($referral->agent_b)->currLanguage) {
			$language->load($extension, $base_dir, "english-US");
		 }
		 
		 $agentA = new stdClass();
		 $agentA->registration = $model2->get_user($referral->agent_a);
		 $agentA->user_table = JFactory::getUser($referral->agent_a);
		 //$agentA->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_a));
		$agentA->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_a, true),array($agentA->registration->countryCode));
		//$agentA->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_a));
		$agentA->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_a, true),array($agentA->registration->countryCode));
		 $agentB = new stdClass();
		 $agentB->registration = $model2->get_user($referral->agent_b);
		 $agentB->user_table = JFactory::getUser($referral->agent_b);
		 //$agentB->mobile = array_map(function($object){return $object->value;}, $model2->get_mobile_numbers($referral->agent_b));
		$agentB->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_mobile_numbers($referral->agent_b, true),array($agentB->registration->countryCode));
		//$agentB->fax = array_map(function($object){return $object->value;}, $model2->get_fax_numbers($referral->agent_b));
		$agentB->fax  = array_map(function($object,$countryCode){return $countryCode." ".$object->value;}, $model2->get_fax_numbers($referral->agent_b, true),array($agentB->registration->countryCode));
		 $client = $model2->get_client($referral->client_id);
		 $client->email = array_map(function($object){return $object->email;}, $client->email);
		 //$client->mobile = array_map(function($object){return $object->number;}, $client->mobile);
		$client->mobile  = array_map(function($object,$countryCode){return $countryCode." ".$object->number;}, $client->mobile,array($client->address[0]->countryCode));
		 $client->address = ((trim($client->address[0]->address_1)!="") ? $client->address[0]->address_1." ".$client->address[0]->address_2.", ":"").$client->address[0]->city." ".(is_numeric($client->address[0]->state) ? $model2->get_state_name($client->address[0]->state) : $client->address[0]->state)." ".$client->address[0]->zip.", ".$model2->getCountry($client->address[0]->country);
		 $refexplode = explode('%',$referral->referral_fee);
		 $head = $this->generateHeader($referral->referral_id, $date, 1);
		return $head."
			<table border=\"0\">
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_PARAGRAPH1', stripslashes_all($agentA_broker->broker_name), stripslashes_all($agentB_broker->broker_name), stripslashes_all($client->name)) . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td>" . JText::_('COM_AGREEMENT_IDOFPARTIES') . "</td>
					<td colspan=\"2\"></td>
				</tr>
				<tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_REFERRING_AGENT_BROKER') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentA_broker->broker_name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER_STATE_LICENSE_NUM') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentA->registration->brokerage_license )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentA->user_table->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_LICENSE') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentA->registration->licence )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".(count($agentA->mobile) ? implode(',',$agentA->mobile) : 'n/a')." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".$agentA->registration->email." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_RECEIVING_AGENT_BROKER') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentB_broker->broker_name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_BROKER_STATE_LICENSE_NUM') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt($agentB->registration->brokerage_license)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($agentB->user_table->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_LICENSE') . ":</td>
					<td colspan=\"2\"> ".$this->decrypt( $agentB->registration->licence )." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".(count($agentB->mobile) ? implode(',',$agentB->mobile) : 'n/a')." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".$agentB->registration->email." </td>
				</tr>
		        <tr>
					<td style=\"padding-left: 30px; font-weight: bold\">" . JText::_('COM_AGREEMENT_PRINCIPAL') . "</td>
					<td colspan=\"2\"> </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_NAME') . ":</td>
					<td colspan=\"2\"> ".stripslashes_all($client->name)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_MOBILE') . ":</td>
					<td colspan=\"2\"> ".implode(',',$client->mobile)." </td>
				</tr>
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_EMAIL') . ":</td>
					<td colspan=\"2\"> ".implode(',', $client->email)." </td>
				</tr>".
((isset($client->address) && $client->address!="") ?
"
				<tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_ADDRESS') . ":</td>
					<td colspan=\"2\"> ".$client->address." </td>
				</tr>
" : "")."
		        <tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_PRINCIPAL_IS') . ":</td>
					<td colspan=\"2\"> ".JText::_($referral->intention)." </td>
				</tr>
		        <tr>
					<td style=\"padding-left: 50px\">$indent4 " . JText::_('COM_AGREEMENT_AGREED_REFERRAL_FEE') . ":</td>
					<td colspan=\"2\"> ".$referral->referral_fee." </td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PRINCIPAL_CONSENT') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PRINCIPAL_CONSENT_PARAGRAPH') . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_AGREEMENT') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::sprintf('COM_AGREEMENT_AGREEMENT_PARAGRAPH', ucfirst(convert_number_to_words_global($refexplode[0])), $referral->referral_fee) . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PAYMENT_DUE_DATE') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PAYMENT_DUE_DATE_PARAGRAPH') . "</td>
				</tr>
		        <tr>
		        	<td colspan=\"3\" style=\"line-height: 3px\"> </td>
		        </tr>
				<tr>
					<td colspan=\"3\">" . JText::_('COM_AGREEMENT_PAYMENT_FEE_PERMISSIBLE') . "</td>
				</tr>
				<tr>
					<td colspan=\"3\" style=\"text-align: justify\">" . JText::_('COM_AGREEMENT_PAYMENT_FEE_PERMISSIBLE_PARAGRAPH') . "</td>
				</tr>
			</table>		
			";
	}
	function pocket_setting() {
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$user =& JFactory::getUser($_SESSION['user_id']);
		$task = JRequest::getVar('task');
		$error = false;
		$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];
		if(empty($uid)) $application->redirect('index.php?illegal=1');
		if(!empty($_POST['jform']['form'])):
			// PREPARE POST VARIABLES
			foreach($_POST['jform'] as $key=>$value){
				if(is_array($value)){
					foreach($value as $k=>$v){
						$data[$key][$k] = trim(addslashes($v));
					}
				}
				else{
					$data[$key] = trim(addslashes($value));
				}
			}
			#extract($data);
		endif;
		$propertylisting_model = $this->getModel($this->getName());
		if(empty($data['ptype'])):
			JError::raiseWarning( 100, 'Please select a property type!' );
			unset($_GET['saved']);
			$error = true;
		endif;
		if(empty($data['stype'])):
			JError::raiseWarning( 100, 'Please select a property sub-type!' );
			unset($_GET['saved']);
			$error = true;
		endif;
		$settings = $propertylisting_model->getPermissionChoices($user->id);
		if(empty($settings[0]->permission_id)):
			//$ret = $propertylisting_model->insertPermissionSetting($user->id, $data);
			$ret = $propertylisting_model->insertPermissionSetting( $data );
			if($ret):
				JFactory::getApplication()->enqueueMessage('POP&#8482;s Settings Saved!');
			endif;
		else:
			//$result = $propertylisting_model->updatePermissionSetting($user->id, $data);
			$result = $propertylisting_model->updatePermissionSetting( $data );
		endif;
	}
	public function savepops(){
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$model = $this->getModel($this->getName());
		$buyers_0 = $_REQUEST['buyer_arr'];
		$buyers_0_dis = $_REQUEST['buyer_arr_dis'];
		$buyers_dis = explode(",",$buyers_0_dis);
		$aid = $_POST['aida'];
		$pid = $_POST['pida'];
		$msg = "";
		$buyers = explode(",",$buyers_0);
		/*print_r($buyers_dis);
		print_r($buyers); */
		foreach($buyers as $b){
			if($b != 0 && $b != ""){
				$insert = $model->enableSavedListing($pid, $aid, $b);
				$activity = array(
					'user_id'		=> $aid,
					'buyer_id'=> $b,
					'other_user_id'		=> JFactory::getUser()->id,
					'activity_type' => (int)26,
					'date'			=> date('Y-m-d H:i:s', time())
				);
				$ret = $model->insertPropertyActivity($pid, $activity);
			}
		}
		foreach($buyers_dis as $b){
			if($b != 0 && $b != ""){
				$insert = $model->disableSavedListing($pid, $aid, $b);
				$msg = "Property Saved";
			}
		}
		echo $msg;
		//echo $aid." ".$pid;
		//
		die();
	}
	public function testFunctions(){

		echo "<pre>";
		$model = &$this->getModel('PropertyListing');

		$model->updateBuyerCountsAll();

	}

	public function dlvcard(){


		$model = &$this->getModel('PropertyListing');

		$buyer_c = $model->get_clientCard($_GET['id']);

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

		$clientobject=$buyer_c[0];
		$clientobject->email=$model2->get_buyer_email($clientobject->buyer_id);
		$clientobject->mobile=$model2->get_buyer_mobile($clientobject->buyer_id);
		$clientobject->address=$model2->get_buyer_address($clientobject->buyer_id);

		$buyer=$clientobject;

		$buyer_card = new stdClass();
		$buyer_card->name = $buyer->name;
		$buyer_card->address = array();
		$buyer_card->mobile = array();
		$buyer_card->email = array();
		$buyer_card->note = $buyer->note;

		$buyer_card->address_breakdown = $buyer->address[0];

		foreach($buyer->address as $address){

			$state = ($address->state) ? $address->state : "";

			$con_Code = ($address->countryCode) ? $address->countryCode : "";

			$buyer_card->address[] = $address->address_1.', '.$address->address_2.', '.$address->city.' '.$state.' '.$model2->getCountry($address->country).', '.$address->zip;

		}

		foreach($buyer->mobile as $mobile){

			$buyer_card->mobile[] = $con_Code.' '.$mobile->number;

		}

		foreach($buyer->email as $email){

			$buyer_card->email[] = $email->email;

		}


		$model->download_card($buyer_card);

		//var_dump($buyer_card);

		//echo $buyer_card;

		die();

	}
	
	public function individual(){
		
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$model = &$this->getModel('PropertyListing');		
		$individual = $model->getSelectedListing($_GET['lID']);
		
		if(JFactory::getUser()->id != $individual[0]->user_id) {
			$own_group_id = $model2->get_user_group(JFactory::getUser()->id);
			$view_group_id = $model2->get_user_group($individual[0]->user_id);
			
			$admin_group = array(7,8,11,12);
			
			if(in_array($view_group_id->group_id, $admin_group) && !in_array($own_group_id->group_id, $admin_group)) {
				JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
			}
			
			if($individual[0]->setting=="2"){
				$allowed_user = $model->checkuserAccess(JFactory::getUser()->id,$individual[0]->user_id,$_GET['lID']);
				if($allowed_user!=1){
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
				}
			}	
		}				
		
		if(!count($individual)) {
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_('index.php?option=com_activitylog'));
		}
		$images = $model->get_property_image($_REQUEST['lID']);
		$application = JFactory::getApplication();
		$userDetails = $model2->get_user_registration(JFactory::getUser()->id);
		$ex_rates = $model->getExchangeRates_indi();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$previous_year = date("Y", strtotime("-1 year"));
		$owner = $model2->get_profile_info($individual[0]->user_id);
		while(intval($previous_year) >= 2012) {
			if($userDetails->{"verified_".$previous_year}) {
				$user_previous_year_volume = $userDetails->{"volume_".$previous_year};
				$user_previous_year_ave_price = $userDetails->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}
		foreach($individual as $items) {
			$is_private = 0; 
			if($items->user_id != JFactory::getUser()->id){
				$ex_rates_con = $ex_rates->rates->{$items->currency};
				$ex_rates_can = $ex_rates->rates->{$userDetails->currency};
				if($items->currency!=$userDetails->currency){
						if($items->currency=="USD"){									
							$items->price1=$items->price1 * $ex_rates_can;
						} else {
							$items->price1=($items->price1 / $ex_rates_con)*$ex_rates_can;
						}
					if($items->price2){
						if($items->currency=="USD"){									
							$items->price2=$items->price2 * $ex_rates_can;
						} else {
							$items->price2=($items->price2 / $ex_rates_con)*$ex_rates_can;
						}
					}
				}
				$correct_currency = $userDetails->currency;
				$items->symbol = $userDetails->symbol;
			} else {
				$correct_currency = $items->currency;
			}
			if(  strtotime($items->date_created) < strtotime('-3 days')  ) {
				if(!$items->views ){
			  		$items->views = 0;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($items->listing_id);
					$items->views = $rand_count + $items->views;
			  	}
			} else {
				if(!$items->views ){
			  		$items->views = 1;
			  	} else {
			  		$rand_count = $model->getRandViewPOPs($items->listing_id);
					$items->views = $rand_count + $items->views;
			  	}				
			}
			if($items->selected_permission == 3) {
				if($previous_year == 2012 && !$userDetails->{"verified_".$previous_year}) {
					$is_private = 1;
				} else {
					$previous_year_volume_minimum = $items->values;
					if($owner->currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$owner->currency};
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum * $item_user_currency_rate;
					} else {
						$previous_year_volume_minimum_converted = $previous_year_volume_minimum;
					}
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_volume_converted = $user_previous_year_volume * $user_currency_rate;
					} else {
						$user_previous_year_volume_converted = $user_previous_year_volume;
					}
					if($user_previous_year_volume_converted <= $previous_year_volume_minimum_converted) {
						$is_private = 1;
					}
				}			
			}
			if($items->selected_permission == 4) {
				if($previous_year == 2012 && !$userDetails->{"ave_price_".$previous_year}) {
					$is_private = 1;
				} else {
					$previous_year_ave_price_minimum = $items->values;
					if($owner->currency !== "USD") {
						$item_user_currency_rate = $ex_rates->rates->{$owner->currency};
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum * $item_user_currency_rate;
					} else {
						$previous_year_ave_price_minimum_converted = $previous_year_ave_price_minimum;
					}
					if($userDetails->currency !== "USD") {
						$user_currency_rate = $ex_rates->rates->{$userDetails->currency};
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price * $user_currency_rate;
					} else {
						$user_previous_year_ave_price_converted = $user_previous_year_ave_price;
					}
					if($user_previous_year_ave_price_converted <= $previous_year_ave_price_minimum_converted) {
						$is_private = 1;
					}
				}			
			}
		}
		if(!empty($individual) && $individual[0]->user_id != JFactory::getUser()->id) {
			$model->logActivity($individual);
		}
		if(!count($images)) {
			$image = array();
			$image['image'] = "images/buyers/500x500/" . $model->get_buyer_image($individual[0]->property_type, $individual[0]->sub_type);
			$image['default'] = 1;
			$images = array();
			$_images = (object) $image;
			$images[] = $_images;
		} else {
			//var_dump($images);
			$db = JFactory::getDbo();
          	$query = $db->getQuery(true);
          	$query = "SELECT * FROM `tbl_pocket_images` WHERE `tbl_pocket_images`.`listing_id` = ".$individual[0]->listing_id." ORDER BY order_image ASC,image_id DESC";
          	$db->setQuery($query);           
         	//$resultss = $db->execute();
         	$arra_images = $db->loadObjectList();
       //   	$new_arr_images = implode(",", $arra_images);
          	$images = $arra_images;
		}
		$referrals_on_property = explode(",", $individual[0]->referrers);
		$mobile = $model2->get_mobile_numbers($individual[0]->user_id);
		$work = $model2->get_work_numbers($individual[0]->user_id);
		$countryIso = $model2->getCountryIso($individual[0]->country);
		$owner->network = $model2->get_network($individual[0]->user_id, 1);
		if(
		(JFactory::getUser()->id!=$individual[0]->user_id && $individual[0]->selected_permission == 7 && $owner->state == $userDetails->state && !in_array(JFactory::getUser()->id, explode(',',$individual[0]->allowed))) ||
		(JFactory::getUser()->id!=$individual[0]->user_id && $individual[0]->selected_permission == 3 && $is_private && !in_array(JFactory::getUser()->id, explode(',',$individual[0]->allowed))) ||
		(JFactory::getUser()->id!=$individual[0]->user_id && $individual[0]->selected_permission == 4 && $is_private && !in_array(JFactory::getUser()->id, explode(',',$individual[0]->allowed))) ||
		(JFactory::getUser()->id!=$individual[0]->user_id && $individual[0]->selected_permission == 0 && !in_array(JFactory::getUser()->id, explode(',',$individual[0]->allowed)))
		){
			$application->redirect('index.php');
		}
		$buyers_a = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"A Buyer"'));
		$buyers_b = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"B Buyer"'));
		$buyers_c = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"C Buyer"'));
		$buyers_i = $model->getBuyerPageDetails(JFactory::getUser()->id, array('buyer_type'=>'"Inactive"'));
		$buyers = array_merge($buyers_a, $buyers_b, $buyers_c, $buyers_i);
		$existing_buyers = $model->getSavedListing(JFactory::getUser()->id, $_REQUEST['lID']);
		$existing_saved_listing = $model->getSavedBuyerListing(JFactory::getUser()->id, $_REQUEST['lID']);
		$ex_rates = $model->getExchangeRates_indi();
		$existing_buyer_count = $model->count_user_buyers();



		if(JFactory::getUser()->id != $individual[0]->user_id){
			$country_used = $userDetails->country;
			$sqftMeasurement_used= $userDetails->sqftMeasurement;
		} else {
			$country_used = $individual[0]->country;
			$sqftMeasurement_used= $individual[0]->sqftMeasurement;
		}


		$sqM = "";
		
		if($sqftMeasurement_used=="sq. meters"){
			$getMeasurement = $model->getSqMeasureByCountry($country_used);
			foreach ($getMeasurement['bldg'] as $key => $value) {
				if($individual[0]->bldg_sqft){
					if($individual[0]->bldg_sqft == $value[1]){
						$individual[0]->bldg_sqft=$value[0];
					}
				}
				if($individual[0]->available_sqft){
					if($individual[0]->available_sqft == $value[1]){
						$individual[0]->available_sqft=$value[0];
					}
					
				}
				if($individual[0]->unit_sqft){
					if($individual[0]->unit_sqft == $value[1]){
						$individual[0]->unit_sqft=$value[0];
					}
					
				}
			}

			foreach ($getMeasurement['lot'] as $key => $value) {
				if($individual[0]->lot_sqft){
					if($individual[0]->lot_sqft == $value[1]){
						$individual[0]->lot_sqft=$value[0];
					}
					
				}
				if($individual[0]->lot_size){
					if($individual[0]->lot_size == $value[1]){
						$individual[0]->lot_size=$value[0];
					}
					
				}
			}

			$sqM = "_M";
		}

		$langValues = $this->checkLangFiles();
		$pops_feats = $model->getPOPsDetails($individual[0],$sqM,$individual[0]->sub_type,$langValues);

		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('pops_feats_init', $pops_feats['initial_feats']);
		$view->assignRef('pops_feats_array', $pops_feats['feats_array']);
		$view->assignRef('buyers', $buyers);
		$view->assignRef('buyers_a', $buyers_a);
		$view->assignRef('buyers_b', $buyers_b);
		$view->assignRef('buyers_c', $buyers_c);
		$view->assignRef('buyers_i', $buyers_i);
		$view->assignRef('existing_buyer_count', $existing_buyer_count);
		$view->assignRef('sqM', $sqM);
		$view->assignRef('existing_buyers', $existing_buyers);
		$view->assignRef('existing_saved_listing', $existing_saved_listing);
		$view->assignRef('images', $images);
		$view->assignRef('countryIso', $countryIso);
		$view->assignRef('data', $individual[0]);
		$view->assignRef('owner', $owner);
		$view->assignRef('mobile', $mobile);
		$view->assignRef('work', $work);
		$view->assignRef('user_info', $userDetails);
		$view->assignRef('correct_currency', $correct_currency);
		$view->assignRef('referrers', $referrals_on_property);
		$view->display($this->getTask());
	}

	function insertViewfromAPP(){

		$model = &$this->getModel('PropertyListing');	

		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($_GET['user_id']));
		$user =& JFactory::getUser();
		$uid =  $user->id ;	

		$individual = $model->getSelectedListing($_GET['lID']);
		if(!empty($individual) && $individual[0]->user_id != $uid) {
			$model->logActivity($individual);
		}

		$mailSender =& JFactory::getMailer();
        $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
        $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
        $mailSender ->setSubject( "insert View Count" );
        $mailSender ->isHTML(  true );
        $mailSender ->setBody( print_r($individual,true));
        $mailSender ->Send(); 
	}


	function declinereferral(){
		$model = &$this->getModel('PropertyListing');
		echo $model->decline_referral($_REQUEST['id'], JFactory::getUser()->id, $_REQUEST['reason']);
		die();
	}
	function counterreferral(){
		$model = &$this->getModel('PropertyListing');
		echo $model->counter_referral($_REQUEST['id'], JFactory::getUser()->id, $_REQUEST['value']);
		die();
	}
	function acceptreferral(){
		$model = &$this->getModel('PropertyListing');
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$thisuserinfo = $model2->get_user_registration(JFactory::getUser()->id);
		$d_format = $this->getCountryDataByID($thisuserinfo->country)->dateFormat;
		$date = date("Y-m-d H:i:s");
		//echo date("Y-m-d H:i:s") . "<br />";
		//echo $date . "<br />";
		switch($userDetails->country) {
			case 223:
			case 38:
			case 13:			
				$objTimeZone = $this->getTimeZone($thisuserinfo->state);
				$timezone = $objTimeZone->timezone;
				break;
			case 222:
			case 103:
				$timezone = "Europe/London";
				break;
			case 204:
			case 81:
			case 141:
			case 73:
				$timezone = "Europe/Paris";
				break;
			default:
				$timezone = "America/Los_Angeles";
		}
		$new_date = new DateTime($date, new DateTimeZone($timezone));
		$userTimeZone = new DateTimeZone($timezone);
		$offset = $userTimeZone->getOffset($new_date);
		$date = gmdate($d_format, strtotime($date)+intval($offset));
		//echo $date . "<br />";
		//echo $offset . "<br />";
		//echo $new_date;
		$pages = array();
		$pages[] = $this->generate_page1($_POST['id'],$date);
		$pages[] = $this->generate_page2($_POST['id'],$date);
		//var_dump($pages);
		$filename = $this->writepdf_v3($pages);
		echo $model->accept_referral($_POST['id'], JFactory::getUser()->id, $filename);
		die();
	}
	function checkIfBrokerExists(){
		$user_id = $_POST['otheruserid'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select('ur.brokerage_license')
		->from('#__users u')
		->leftJoin('#__user_registration ur ON u.email = ur.email')
		->where('u.id = ' . $user_id);
		$db->setQuery($query);
		$objects = $db->loadObjectList();
		echo json_encode($objects);
	}
	function saveDialogBroker(){
		$user_id = $_POST['otheruserid'];
		$brokerage_license = $_POST['brokerage_license'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select('u.email')
		->from('#__users u')
		->where('u.id = ' . $user_id);
		$db->setQuery($query);
		$email = $db->loadResult();
		$fields = array($db->quoteName('brokerage_license') . '="'.$this->encrypt($brokerage_license).'"');							
		$conditions = array($db->quoteName('email') . '="'.$email.'"');							
		$query = $db->getQuery(true);
		$query->update($db->quoteName('#__user_registration'))->set($fields)->where($conditions);							 
		// Set the query using our newly populated query object and execute it.
		$db->setQuery($query);
		$res=$db->execute();
		echo json_encode($res);
	}
	function retractreferral(){
		$model = $this->getModel($this->getName());
		$reason = isset($_REQUEST['reason']) ? $_REQUEST['reason'] : 0;
		echo $model->retract_referral($_REQUEST['id'], JFactory::getUser()->id, $reason);
		exit;
	}
	function test_juser(){
		$message=array("message"=>"Successfully Added POPs, Please refresh to see the changes");
		echo json_encode($message);
		var_dump($message);
	}
	function add_pocket() {
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$propertylisting_model = $this->getModel($this->getName());

		$langValues = $propertylisting_model->checkLangFiles();

		if(isset($_POST['user_id']) && isset($_GET['webservice'])){
			$user = JFactory::getUser();
			$session = JFactory::getSession();
			$session->set('user', new JUser($_POST['user_id']));
			$user =& JFactory::getUser();
			$uid =  $user->id ;
			$data = $_POST;

				

			foreach ($data as $key => $value) {

				if($key=="state"){
					$data[$key]=$propertylisting_model->getStateIdByName($value);
				}
				if($key=="ptype"){
					foreach ($langValues as $key2 => $value2) {
						if($value==$value2){
							$value = trim($key2);
						}
					}

					$data[$key]=$propertylisting_model->getPtypeByName($value);

				
				}
				if($key=="stype"){

					foreach ($langValues as $key2 => $value2) {
						if($data["ptype"]==1 ||$data["ptype"]==3){
							if($value==$value2){
								$value = trim($key2);
							}
						} else {
							if($value==$value2 && strpos($key2, "L_") !== false){
								$value = trim($key2);
							}
						}

					}

					

					$data[$key]=$propertylisting_model->getSubTypeByName($data["ptype"],$value);

					

				}
				if($key=="Bedrooms"){
					$data["bedroom"]=$data[$key];
				}
				if($key=="Bathrooms"){
					$data["bathroom"]=$data[$key];
				}
				if($key=="Bldg. Sq. Ft." || $key=="Bldg__Sq__Ft"){
					$data["bldgsqft"]=$data[$key];
				}
				if($key=="View"){
					$data["view"]=$data[$key];
				}
				if($key=="Unit_Sq__Ft_" || $key=="Unit Sq. Ft."){
					$data["unitsqft"]=$data[$key];
				}
				if($key=="Lot Size"){
					$data["lotsize"]=$data[$key];
				}
				if($key=="Zoned"){
					$data["zoned"]=$data[$key];
				}
				if($key=="Term"){
					$data["term"]=$data[$key];
				}
				if($key=="Possession"){
					$data["possession"]=$data[$key];
				}
				if($key=="Units"){
					$data["units"]=$data[$key];
				}
				if($key=="Cap Rate"){
					$data["caprate"]=$data[$key];
				}
				if($key=="GRM"){
					$data["grm"]=$data[$key];
				}
				if($key=="Lot Sq. Ft."){
					$data["lotsqft"]=$data[$key];
				}
				if($key=="Type"){
					$data["type"]=$data[$key];
				}
				if($key=="Class"){
					$data["class"]=$data[$key];
				}
				if($key=="Room Count"){
					$data["roomcount"]=$data[$key];
				}
				if($key=="Available Sq.Ft"){
					$data["available"]=$data[$key];
				}
				if($key=="Type Lease"){
					$data["typelease"]=$data[$key];
				}

				if(strpos($key, "image_") !== FALSE && strpos($key, "image_order") === FALSE){
					$key_n = str_replace("image_", "", $key);
					$_POST["jform_image"][$key_n]=$data[$key];
					$_POST['jform_image_order'][$key_n]=$key_n+1;
				}

				$noeditsdata = $data;

			}


		} else{
			$user =& JFactory::getUser($_SESSION['user_id']);
			$task = JRequest::getVar('task');
			$thispocketid = "";
			$uid = (empty($_SESSION['user_id'])) ? $user->id : $_SESSION['user_id'];			
			if(empty($uid)) $application->redirect('index.php?illegal=1');
			if(!empty($_POST['jform']['form'])):
				// PREPARE POST VARIABLES
				foreach($_POST['jform'] as $key=>$value){
					if(is_array($value)){
						foreach($value as $k=>$v){
							$data[$key][$k] = trim(addslashes($v));
						}
					}
					else{
						$data[$key] = trim(addslashes($value));
					}
				}
				extract($data);
			endif;

			$noeditsdata = $_POST['jform'];
		}

		$user_android_token = $propertylisting_model->getAppUserToken($user->id,'android');

		$ret = $propertylisting_model->insertPocket($user->id, $data);
		$listing_id = $ret;
		if($ret):
		// GET USER INFO
			//$listing = $propertylisting_model->getListing($uid);
			$listing_id = $ret;
			$thispocketid = $listing_id;
			$ret2 = $propertylisting_model->insertNewPermissionSetting($listing_id, $data);
			$ret3 = $propertylisting_model->insertPropertyPrice($listing_id, $data);
			foreach($address as $addr):
				if(!empty($addr)):
					$ret4 = $propertylisting_model->insertPocketAddress($listing_id, $addr);
				endif;
			endforeach;
			if(count($_POST['jform_image']) > 0):				

				foreach($_POST['jform_image'] as $k=>$v):
					if($v !== "") {
						$ret5 = $propertylisting_model->insertPocketImages($listing_id, $v, $_POST['jform_image_order'][$k]);
					}
				endforeach;
			endif;
		//Insert Add property activity
		$activity = array(
				'user_id'		=> $user->id,
				'activity_type' => (int)2,
				'date'			=> date('Y-m-d H:i:s', time())
			);
			unset($_SESSION["lID_addpocket"]);
			$ret = $propertylisting_model->insertPropertyActivity($listing_id, $activity);
		endif;
		$noeditsdata['user_id'] = $user->id;
		$propertylisting_model->match_pop_to_buyers($noeditsdata,$listing_id,$user_android_token);
		//$buyers = $propertylisting_model->getBuyerAll();	
	

			//$this->android_send_notification($array_user_tokens,"Buyer and POPs match","You have a new POPs™ match to a Buyer", "BuyerMatch");
		if(isset($_POST['user_id']) && isset($_GET['webservice'])){
			//$message=array("message"=>"Successfully Added POPs, Please refresh to see the changes");
			//echo json_encode($message);
		}
		
		if(isset($_POST['admin_id'])) {
			$insert_act = new JObject();
			$insert_act->activity_type_id = 7;
			$insert_act->user_id = $_POST['admin_id'];
			$insert_act->activity_details = "Added POPs " . $data['property_name'] . " ( ID: " . $listing_id . ")";
			$db->insertObject('#__admin_activities', $insert_act);
		}
		
		die();
		$application = JFactory::getApplication();
		$application->redirect(JRoute::_("index.php?option=com_propertylisting"));
	}
	function encrypt($plain_text) {
		$key = 'password to (en/de)crypt';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
	}	
	function submitpayment(){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$model = $this->getModel($this->getName());
		//tin
		//btino
		//
		$whowillpay = $model2->get_user_registration(JFactory::getUser()->id);
		$model2->update_tax_id_num($whowillpay->user_id, $this->encrypt($_POST['btino']));
		if(!$_POST['crlfid'])
		{
			$referral = $model->get_referral($_POST['ref_id']);
			$refin_closed_count = $model->inclosed_count(JFactory::getUser()->id);
			$topay = ($referral->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			if( $refin_closed_count == 0 ) {
				$amounttopay = "0.01";
			} else {
				$amounttopay = $model->get_AB_ref_fee($_POST['amount'])->$topay;
			}
			$amount = $_POST['amount'];
			$exp=explode('%',$referral->referral_fee);
			$fee = ".".trim($exp[0]);
			$r1 = $fee*$_POST['amount'];
		}
		else{
			unset($_POST['amount']);
			$temp1 = $model3->get_closed_referral($_POST['crlfid']);
			$refin_closed_count = $model->inclosed_count(JFactory::getUser()->id);
			$topay = ($temp1['referral']->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			if( $refin_closed_count == 0 ) {
				$amounttopay = "0.01";
			} else {
				$amounttopay = $temp1['payments']->$topay;
			}
			$amount = $temp1['clrfobject']->price_paid;
			$exp=explode('%',$temp1['referral']->referral_fee);
			$r2_id = $temp1['referral']->agent_b;
			$fee = ".".trim($exp[0]);
			$r1 = $fee*$temp1['clrfobject']->price_paid;
		}
		if($topay=="r2_fee"){
			$ref = $_POST['ref_id'];
			unset($_POST['ref_id']);
		}
		else{
			$ref = $temp1['referral']->referral_id;
		}
		$state = (is_numeric($whowillpay->state)) ? $model2->get_zone_with_iso_code($whowillpay->state)->zone_code : $whowillpay->state;
		$date = date("F j, Y");		
		if($_POST['save_trans']){
			unset($_POST['bslno']);
			unset($_POST['alslno']);
			unset($_POST['btino']);
			$profile_response = $model->create_authorize_profile(JFactory::getUser()->id, $_POST['email']);
			if($profile_response['status']>0){
				$profile_id = $profile_response['profile_id'];
				$card_expiry = explode('-',$_POST['card_expiry']);
				$ctry = $model2->getCountry($whowillpay->country);
				$payment_profile_response = $model->create_payment_profile ($profile_id, $_POST['firstname'], $_POST['lastname'], $_POST['address'], $_POST['city'], $state, $_POST['zip'], $ctry, $_POST['card_number'], $card_expiry[1].'-'.$card_expiry[0]);
				//print_r($payment_profile_response);
				if($payment_profile_response['status']>0){
					$payment_profile_id = $payment_profile_response['payment_profile_id'];
					$invoice = $this->createInvoice();
					$model->save_payment_profile(JFactory::getUser()->id, $profile_id, $payment_profile_id);
					$temp = $model->create_transaction(JFactory::getUser()->id, $amounttopay, $invoice);
					$temp3=explode(',', $temp['directResponse']);
					$formated = array_merge(array('invoice'=>$invoice,'trans_id'=>(string)$temp3[6], 'amount'=>$amounttopay, 'rtwocomm'=>$amount, 'ronecomm'=>$r1, 'ref_id'=>$ref), $_POST);
					$files=array();
					$files['invoice'] = $this->writepdf_invoice($this->generate_invoice_document($ref, $invoice, $amounttopay, JFactory::getUser()->id));
					$model->insert_authorize_transaction($formated);
					if($topay=="r2_fee"){
						$date = date("F j, Y");
						$files['payment_method'] = $this->writepdf_pay_method($this->generate_payment_method($ref, $date, $amount));
						$temp[] = $model->close_listing($ref, $amount, $files);
						$model->logpayment($ref, JFactory::getUser()->id);
					}
					else{
						$temp1['clrfobject']->r1_paid = 1;
						$model->insertIfnotExists($temp1['clrfobject'], 'clrf_id', '#__closed_referrals');
						$model->logpayment($ref, JFactory::getUser()->id);
						if(isset($_POST['ref_id']))
							unset($_POST['ref_id']);
					}
					$model->send_closed_referral($ref,$amount,$files,JFactory::getUser()->id);
					echo json_encode(array_merge($temp3));
				}
				else{
					echo json_encode(array(0,0,0,$payment_profile_response['message']));
				}
			}
			else{
				echo json_encode(array(0,0,0,$profile_response['message']));
			}
			die();
		}
		unset($_POST['save_trans']);
		unset($_POST['agree_terms']);
		unset($_POST['usesaved']);
		unset($_POST['bslno']);
		unset($_POST['alslno']);
		unset($_POST['btino']);
		$invoice = $this->createInvoice();
		$post_url = "https://secure.authorize.net/gateway/transact.dll";
		$data = array();
		// $data['x_login'] = "5A25pF2n7";
		// $data['x_tran_key'] = "473yz2uA6MD8a8Zb";
		$data['x_login'] = "926t4yDN3";
		$data['x_tran_key'] = "27c8ds72fUUZj4Zh";
		$data['x_version'] = "3.1";
		$data['x_delim_data'] = "TRUE";
		$data['x_delim_char'] = "|";
		$data['x_relay_response'] = "FALSE";
		$data['x_type'] = "AUTH_CAPTURE";
		$data['x_method'] = "CC";
		$data['x_card_num'] = $_POST['card_number'];
		$data['x_exp_date'] = str_replace('-', '', $_POST['card_expiry']);
		$data['x_first_name'] = $_POST['firstname'];
		$data['x_last_name'] =	$_POST['lastname'];
		$data['x_address'] =  $_POST['address'];
		$data['x_city'] =  $_POST['city'];
		$data['x_state'] = $state;
		$data['x_zip'] =  $_POST['zip'];
		$data['x_country'] = $model2->getCountry($whowillpay->country);
		$data['x_phone'] = $_POST['phone'];
		$data['x_customer_ip'] = $_SERVER['REMOTE_ADDR'];
		$data['x_email'] = $_POST['email'];
		$data['x_description'] = "Referral Service Fee";
		$data['x_amount'] = $amounttopay;
		$data['x_method'] = 'CC';
		$data['x_type'] = "AUTH_CAPTURE";
		$data['x_card_code'] = $_POST['security_no'];
		$invoice = $data['x_invoice_num'] = $this->createInvoice();
		$post_string = "";
		foreach( $data as $key => $value )
		{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
		$request = curl_init($post_url);
		curl_setopt($request, CURLOPT_HEADER, 0);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
		$post_response = curl_exec($request);
		curl_close ($request);
		$temp = explode($data['x_delim_char'],$post_response);
		$files=array();
		$files['invoice'] = $this->writepdf_invoice($this->generate_invoice_document($ref, $invoice, $amounttopay, JFactory::getUser()->id));
		if($temp[0]==1){
			unset($_POST['amount']);
			$formated = array_merge(array('invoice'=>$invoice,'trans_id'=>$temp[6], 'amount'=>$amounttopay, 'rtwocomm'=>$amount, 'ronecomm'=>$r1, 'ref_id'=>$ref), $_POST);
			$model->insert_authorize_transaction($formated);
			if($topay=="r2_fee"){
				$date = date("F j, Y");
				$files['payment_method'] = $this->writepdf_pay_method($this->generate_payment_method($ref, $date, $amount));
				$temp[] = $model->close_listing($ref, $amount, $files);
				$model->logpayment($ref, JFactory::getUser()->id);
			}
			else{
				$temp1['clrfobject']->r1_paid = 1;
				$model->insertIfnotExists($temp1['clrfobject'], 'clrf_id', '#__closed_referrals');
				$model->logpayment($ref, JFactory::getUser()->id);
				if(isset($_POST['ref_id']))
					unset($_POST['ref_id']);
			}
			$model->send_closed_referral($ref,$amount,$files,JFactory::getUser()->id);
		}
		echo json_encode(array_merge($temp));
		die();
	}
	function submitsavedPayment(){
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$model = $this->getModel($this->getName());
		//tin
		//btino
		//		
		unset($_POST['save_trans']);
		unset($_POST['bslno']);
		unset($_POST['alslno']);
		unset($_POST['btino']);
		$whowillpay = $model2->get_user_registration(JFactory::getUser()->id);
		$state = (is_numeric($whowillpay->state)) ? $model2->get_zone_with_iso_code($whowillpay->state)->zone_code : $whowillpay->state;
		$date = date("F j, Y");
		$invoice = $this->createInvoice();
		if(!$_POST['crlfid'])
		{
			$referral = $model->get_referral($_POST['ref_id']);
			$refin_closed_count = $model->inclosed_count(JFactory::getUser()->id);
			$topay = ($referral->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			if( $refin_closed_count == 0 ) {
				$amounttopay = "0.01";
			} else {
				$amounttopay = $model->get_AB_ref_fee($_POST['amount'])->$topay;
			}
			$amount = $_POST['amount'];
			$exp=explode('%',$referral->referral_fee);
			$fee = ".".trim($exp[0]);
			$r1 = $fee*$_POST['amount'];
		}
		else{
			$temp1 = $model3->get_closed_referral($_POST['crlfid']);
			$refin_closed_count = $model->inclosed_count(JFactory::getUser()->id);
			$topay = ($temp1['referral']->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			//$amounttopay = $temp1['payments']->$topay;
			if( $refin_closed_count == 0 ) {
				$amounttopay = "0.01";
			} else {
				$amounttopay = $temp1['payments']->$topay;
			}
			$amount = $temp1['clrfobject']->price_paid;
			$exp=explode('%',$temp1['referral']->referral_fee);
			$r2_id = $temp1['referral']->agent_b;
			$fee = ".".trim($exp[0]);
			$r1 = $fee*$temp1['clrfobject']->price_paid;
		}
		if($topay=="r2_fee"){
			$ref = $_POST['ref_id'];
			unset($_POST['ref_id']);
		}
		else{
			$ref = $temp1['referral']->referral_id;
		}
		$state = (is_numeric($whowillpay->state)) ? $model2->get_zone_with_iso_code($whowillpay->state)->zone_code : $whowillpay->state;
		$date = date("F j, Y");
		$response = $model->create_transaction(JFactory::getUser()->id, $amounttopay, $invoice);
		if($response['status']>0){
				$temp3=explode(',', $response['directResponse']);
				unset($_POST['amount']);
				$formated = array_merge(array('invoice'=>$invoice,'trans_id'=>(string)$temp3[6], 'amount'=>$amounttopay, 'rtwocomm'=>$amount, 'ronecomm'=>$r1, 'ref_id'=>$ref), $_POST);
				$model->insert_authorize_transaction($formated);
				$files=array();
				$files['invoice'] = $this->writepdf_invoice($this->generate_invoice_document($ref, $invoice, $amounttopay, JFactory::getUser()->id));
				if($topay=="r2_fee"){
					$files['payment_method'] = $this->writepdf_pay_method($this->generate_payment_method($ref, $date, $amount));
					$temp[] = $model->close_listing($ref, $amount, $files);
					$model->logpayment($ref, JFactory::getUser()->id);
				}
				else{
					$temp1['clrfobject']->r1_paid = 1;
					$model->insertIfnotExists($temp1['clrfobject'], 'clrf_id', '#__closed_referrals');
					$model->logpayment($ref, JFactory::getUser()->id);
					if(isset($_POST['ref_id']))
					unset($_POST['ref_id']);
				}
				echo json_encode($temp3);
				$model->send_closed_referral($ref,$amount,$files,JFactory::getUser()->id);
			}
			else{
				echo json_encode(array(0,0,0,$response['message']));
			}
			die();
	}
	function createInvoice(){
		$model = $this->getModel($this->getName());
		return "Fee_".JFactory::getUser()->id."_".$model->get_next_invoice()."_".rand(0, 102);
	}
	function calculatefee(){
		$activitylogmodel = JPATH_ROOT.'/components/'.'com_activitylog/models';
		JModelLegacy::addIncludePath( $activitylogmodel );
		$model3 =& JModelLegacy::getInstance('ActivityLog', 'ActivityLogModel');
		$model = $this->getModel($this->getName());
		$r1=0;
		if(!$_POST['crlfid']){
			$referral = $model->get_referral($_POST['ref_id']);
			$topay = ($referral->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			$amounttopay = $model->get_AB_ref_fee(str_replace('$','',$_POST['amount']))->$topay;
			$amount = $this->format_currency($_POST['amount']);
			$amounttopay = $this->format_currency($amounttopay);
			$r1_id = $referral->agent_a;
			$r2_id = $referral->agent_b;
			$clientname = $model->get_client($_POST['ref_id']);
			$clientname = $clientname->name;
			$exp=explode('%',$referral->referral_fee);
			$fee = ".".trim($exp[0]);
			$r1 = $this->format_currency($fee*$_POST['amount']);
		}
		else{
			$temp1 = $model3->get_closed_referral($_POST['crlfid']);
			$topay = ($temp1['referral']->agent_a == JFactory::getUser()->id) ? 'r1_fee' : 'r2_fee';
			$amounttopay = $this->format_currency($temp1['payments']->$topay);
			$amount = $this->format_currency($temp1['clrfobject']->price_paid);
			$exp=explode('%',$temp1['referral']->referral_fee);
			$r2_id = $temp1['referral']->agent_b;
			$fee = ".".trim($exp[0]);
			$r1 = $this->format_currency($fee*$temp1['clrfobject']->price_paid);
			$clientname = $temp1['client']->name;
		}
		//print_r($model->get_payment_profile(866)); die();
		echo json_encode(array('amount'=>$amounttopay, 'rtwocomm'=>$amount, 'ronecomm'=>$r1, 'clientname'=>$clientname, 'r2'=>stripslashes_all(JFactory::getUser($r2_id)->name), 'r1'=>stripslashes_all(JFactory::getUser($r1_id)->name), 'authorize'=>$model->get_payment_profile(JFactory::getUser()->id) ));
		die();
	}
	function format_currency($val){	
		setlocale(LC_ALL, ''); // Locale will be different on each system.
		$locale = localeconv();
		return  "$". number_format($val, 0, ".", ",");
	}
	function newreferral(){
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$user = JFactory::getUser();
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		$model = $this->getModel($this->getName());
		$view = $this->getView($this->getName(), 'html');
		if(isset($_REQUEST['lID'])){
			$listing = $model->get_listing(array('p.listing_id'=>$_REQUEST['lID']));
			$listing = $listing[0];
			$view->assignRef('listing', $listing);
			$view->assignRef('owner', $model2->get_profile_info($listing->user_id));
			$view->assignRef('languages', $model2->get_user_spoken_language($listing->user_id));
			$view->assignRef('designations', $model2->get_user_designations($listing->user_id));
		}
		else if(isset($_REQUEST['uid'])){


			$ex_rates = $model->getExchangeRates_indi();
			$current_user_info = $model2->get_profile_info($user->id);
			$ownerDet = $model2->get_profile_info($_REQUEST['uid']);

				if($ownerDet->currency == "") {
					$ownerDet->currency = "USD";
				}
				
				if($ownerDet->currency!=$current_user_info->currency){
					$ex_rates_con = $ex_rates->rates->{$ownerDet->currency};
					$ex_rates_can = $ex_rates->rates->{$current_user_info->currency};
					if($ownerDet->currency=="USD"){									
						$ownerDet->ave_price_2012=$ownerDet->ave_price_2012 * $ex_rates_can;
						$ownerDet->ave_price_2013=$ownerDet->ave_price_2013 * $ex_rates_can;
						$ownerDet->ave_price_2014=$ownerDet->ave_price_2014 * $ex_rates_can;
						$ownerDet->ave_price_2015=$ownerDet->ave_price_2015 * $ex_rates_can;
						$ownerDet->volume_2012=$ownerDet->volume_2012 * $ex_rates_can;
						$ownerDet->volume_2013=$ownerDet->volume_2013 * $ex_rates_can;
						$ownerDet->volume_2014=$ownerDet->volume_2014 * $ex_rates_can;
						$ownerDet->volume_2015=$ownerDet->volume_2015 * $ex_rates_can;
					} else {
						$ownerDet->ave_price_2012=($ownerDet->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->ave_price_2013=($ownerDet->ave_price_2013 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->ave_price_2014=($ownerDet->ave_price_2014 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->ave_price_2015=($ownerDet->ave_price_2015 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->volume_2012=($ownerDet->volume_2012 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->volume_2013=($ownerDet->volume_2013 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->volume_2014=($ownerDet->volume_2014 / $ex_rates_con)*$ex_rates_can;
						$ownerDet->volume_2015=($ownerDet->volume_2015 / $ex_rates_con)*$ex_rates_can;
					}
					$ownerDet->currency = $current_user_info->currency;
					$ownerDet->symbol = $current_user_info->symbol;
				}

			if(isset($_SESSION['referral_data'])) {
				$view->assignRef('referral_data', $_SESSION['referral_data']);
			}
			
			$view->assignRef('owner', $ownerDet);
			$view->assignRef('languages', $model2->get_user_spoken_language($_REQUEST['uid']));
			$view->assignRef('designations', $model2->get_user_designations($_REQUEST['uid']));
		}
		else{
			$refout = $model->get_referral_out(JFactory::getUser()->id);
			
			$arr_added = array();
			foreach($refout as $agent) {
				$agentinfo = array();
				if(in_array($agent->agent_b, $arr_added) === false) {
					$agentinfo['profile'] = $model2->get_profile_info($agent->agent_b, false);
					$agentinfo['listings'] = $model->getAllListings($agent->agent_b);
				}
				
				$arr_added[] = $agent->agent_b;
				
				$agents_in_network[] = $agentinfo;
			}
			
			$view->assignRef('network', $agents_in_network);
		//	$view->assignRef('owner', $model2->get_profile_info($user->id));
		}
		$country = $model->getCountry($user->id);
		$getCountryLangs = $this->getCountryLangs();
		if(isset($_REQUEST['uid'])){
			$country2 = $model->getCountry($_REQUEST['uid']);
			$getCountryLangsInitial = $this->getCountryDataByID($country2[0]->country);
		} else {
			$getCountryLangsInitial = $this->getCountryDataByID($country[0]->country);
		}
		
		if(isset($_SESSION['referral_data'])) {
			$getCountryLangsInitial = $this->getCountryDataByID($_SESSION['referral_data']['jform']['country']);
		}
		
		$view->assignRef('baseurl', JURI::base());
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );		
		$view->assignRef( 'getCountryLangs', $getCountryLangs );	
		$view->assignRef('contact', $model2->get_payment_methods());
		$view->assignRef('contactclient', $model->get_contact_my_client());
		$view->assignRef('clientintention', $model->get_client_intention());
		$view->assignRef('country_list', $getCountryLangs);
		$view->assignRef('country', $country);
		$view->assignRef('dropdown_referral', $model->get_referral_fee_dropdown());
		$view->assignRef('buyers', $model2->get_buyers(JFactory::getUser()->id));
		$view->display($this->getTask());
	}
	function subtype(){

		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
		$language_tag = ($language_tag) ? $language_tag : "english-US";
        $language->load($extension, $base_dir, $language_tag, true);
		$ptype = $_GET['ptype'];

		//var_dump($language_tag);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('sub_id', 'name'));
		$query->from('#__property_sub_type');
		$query->where('property_id LIKE \''.$ptype.'\'');
		$db->setQuery($query);
		$subtypes = $db->loadObjectList();
		foreach ($subtypes as $key => $value) {
			# code...
			//echo $subtypes[$key]->name;
			$subtypes[$key]->name = JText::_($subtypes[$key]->name);
		}
		echo json_encode($subtypes);
		die();
	}


	function checkLangFiles(){

		$ini_file = file_get_contents(JPATH_SITE."/language/english-US/english-US.com_nrds.ini");
		$ini_lines = explode("\n", $ini_file);
		$duplicated = array();

		foreach ($ini_lines as $line)
		{
		    $parts = explode('=', $line);

		    if (count($parts) != 2)
		    {
		        continue;
		    }

		    if(strpos($parts[0], "COM_POPS_TERMS") !== false){
		    	$k = $parts[0];
		    	$v = $parts[1];

		    	$duplicated[$k] = trim(str_replace('"', '', $v));
		    }
	    
		}

		return $duplicated;

	}

	function pops_features(){

		$model = $this->getModel($this->getName());

		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);


		$_GET['country'] = 223;

		$getMeasurement = $model->getSqMeasureByCountry($_GET['country']);

		/*if(isset($_GET['country'])){
			if($_GET['country']==13){
				$defsqft = "square meters";
			} else if($_GET['country']==103){
				$defsqft = "m²";
			}
		}*/

		if($getMeasurement['sqmeasure'] == 'sq. ft.'){
			$defsqft = JText::_('COM_POPS_TERMS_BLDG');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE');
		} else {
			$defsqft = JText::_('COM_POPS_TERMS_BLDG_M');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL_M');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ_M');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT_M');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE_M');
		}

		$ptype = $_GET['ptype'];
		$subtype = $_GET['subtype'];
		$formtype = $_GET['formtype'];
		//echo $this->form();
		$selects="";
		
		if($formtype=="apops"){
			switch ($ptype) {
				case 1: //Residential Purchase
					switch ($subtype) {
						case 1: //SFR
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							$selects.= $model->getFieldsView("required reqfield");
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
							break;
						case 4: //Land
							# code...
							$selects.= $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
							//$selects.= $model->getFieldsView("required reqfield");
							$selects.= $model->getFieldsZoned("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				case 2: //Residential Lease
					switch ($subtype) {
						case 1: //SFR
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							$selects.= $model->getFieldsTerm("required reqfield");
							$selects.= $model->getFieldsTermPossesion("required reqfield");
							$selects.= $model->getFieldsPet();
							$selects.= $model->getFieldsFurnished();
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
							$selects.= $model->getFieldsTerm("required reqfield");
							$selects.= $model->getFieldsPet();
							$selects.= $model->getFieldsFurnished();
							break;
						case 4: //Land
							# code...
							$selects.= $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
							$selects.= $model->getFieldsView("required reqfield");
							$selects.= $model->getFieldsZoned("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				case 3: //Commercial Purchase
					switch ($subtype) {
						case 1: // Multi Family
							# code...
							$selects.= $model->getFieldsTermUnits("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsTermGRM("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							$selects.= $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
							break;
						case 2: //Office
							# code...
							$selects.= $model->getFieldOfficeType("required reqfield");
							$selects.= $model->getFieldOfficeClass("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");		
							break;
						case 3: //Industrial
							# code...
							$selects.= $model->getFieldsIndusType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
						case 4: // Retail
							# code...
							$selects.= $model->getFieldsRetailType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							break;	
						case 5: // Motel/Hotel
							# code...
							$selects.= $model->getFieldsHotelType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsRoomCount("required reqfield");
							break;	
						case 6: // Assisted Care
							# code...
							$selects.= $model->getFieldsAssistType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsRoomCount("required reqfield");
							break;	
						case 7: // Special Purpose
							# code...
							$selects.= $model->getFieldsSpecialType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							break;				
						default:
							# code...
							break;
					}
					break;
				case 4: //Commercial Lease
					switch ($subtype) {
						case 1:// Office
							# code...
							$selects.= $model->getFieldOfficeType("required reqfield");
							$selects.= $model->getFieldOfficeClass("required reqfield");
							$selects.= $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
							$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							break;
						case 2: //Industrial
							# code...
							$selects.= $model->getFieldsIndusType("required reqfield");
							$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							$selects.= $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							$selects.= $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
							break;
						case 3: //Retail
							# code...
							$selects.= $model->getFieldsRetailType("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				default:
					# code...
					break;
			}
		}
		elseif($formtype=="abuyer"){
			switch ($ptype) {
				case 1: //Residential Purchase
					switch ($subtype) {
						case 1: //SFR
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							//$selects.= $model->getFieldsView("required reqfield");
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
							break;
						case 4: //Land
							# code...
							$selects.= $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
							$selects.= $model->getFieldsView("required reqfield");
							$selects.= $model->getFieldsZoned("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				case 2: //Residential Lease
					switch ($subtype) {
						case 1: //SFR
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							$selects.= $model->getFieldsTerm("required reqfield");
							$selects.= $model->getFieldsTermPossesion("required reqfield");
							$selects.= $model->getFieldsPet();
							$selects.= $model->getFieldsFurnished();
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$selects.= $model->getFieldsBedrooms("required reqfield");
							$selects.= $model->getFieldsBathrooms("required reqfield");
							$selects.= $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
							$selects.= $model->getFieldsTerm("required reqfield");
							$selects.= $model->getFieldsPet();
							$selects.= $model->getFieldsFurnished();
							break;
						case 4: //Land
							# code...
							$selects.= $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
							$selects.= $model->getFieldsView("required reqfield");
							$selects.= $model->getFieldsZoned("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				case 3: //Commercial Purchase
					switch ($subtype) {
						case 1: // Multi Family
							# code...
							$selects.= $model->getFieldsTermUnits("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsTermGRM("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
							$selects.= $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
							break;
						case 2: //Office
							# code...
							$selects.= $model->getFieldOfficeType("required reqfield");
							$selects.= $model->getFieldOfficeClass("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");		
							break;
						case 3: //Industrial
							# code...
							$selects.= $model->getFieldsIndusType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
						case 4: // Retail
							# code...
							$selects.= $model->getFieldsRetailType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							break;	
						case 5: // Motel/Hotel
							# code...
							$selects.= $model->getFieldsHotelType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsRoomCount("required reqfield");
							break;	
						case 6: // Assisted Care
							# code...
							$selects.= $model->getFieldsAssistType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							$selects.= $model->getFieldsRoomCount("required reqfield");
							break;	
						case 7: // Special Purpose
							# code...
							$selects.= $model->getFieldsSpecialType("required reqfield");
							$selects.= $model->getFieldsTermCAP("required reqfield");
							break;				
						default:
							# code...
							break;
					}
					break;
				case 4: //Commercial Lease
					switch ($subtype) {
						case 1:// Office
							# code...
							$selects.= $model->getFieldOfficeType("required reqfield");
							$selects.= $model->getFieldOfficeClass("required reqfield");
							$selects.= $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
							$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							break;
						case 2: //Industrial
							# code...
							$selects.= $model->getFieldsIndusType("required reqfield");
							$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							$selects.= $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							$selects.= $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
							break;
						case 3: //Retail
							# code...
							$selects.= $model->getFieldsRetailType("required reqfield");
							$selects.= $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
							//$selects.= $model->getFieldsOfficeTypeLease("required reqfield");
							break;					
						default:
							# code...
							break;
					}
					break;
				default:
					# code...
					break;
			}
		
		}


		$dom = new DOMDocument;
		$dom->loadHTML($selects);
		foreach($dom->getElementsByTagName('label') as $node)
		{
		    $array_select_labels[] = array(utf8_decode(strip_tags($dom->saveHTML($node))));
		    
		}

		$i=0;
		foreach($dom->getElementsByTagName('select') as $node)
		{
			$array_select_ids[] = array($node->getAttribute("id"),$array_select_labels[$i][0]);
			$i++;
		}

		//$i=0;
		foreach($dom->getElementsByTagName('input') as $node)
		{
			$array_select_ids[] = array($node->getAttribute("id"),$array_select_labels[$i][0]);
			$i++;
		}

		//var_dump($array_select_ids);

		$xpath = new DOMXPath($dom);

		foreach ($array_select_ids as $key => $value) {
			# code...
			$tags = $xpath->query('//select[@id="'.$value[0].'"]/option[@value!=""]');
	
			foreach ($tags as $tag) {
				//var_dump(trim($tag->getAttribute('value')));
			    //var_dump(trim($tag->nodeValue));
			   // var_dump($value[0]);
				if($value[0]=="jform_class" || $value[0]=="jform_units"){
					$features[0][$value[1].$value[0]][]=trim($tag->nodeValue."%%".trim($tag->getAttribute('value')));
				} else {
					$features[0][$value[1].$value[0]][]=trim($tag->nodeValue);
				}
			    
			}

			if($value[0]=="jform_pet" || $value[0]=="jform_furnished"){
				$features[0][$value[1].$value[0]]=array("0","1");
			}





		}

		$response = array('status'=>1, 'message'=>"POPs Features", 'features'=>$features);
		echo json_encode($response);




	}


	function old_pops_features(){
		$ptype = $_GET['ptype'];
		$subtype = $_GET['subtype'];
		$formtype = $_GET['formtype'];
		

		if($formtype=="apops"){
			switch ($ptype) {
				case 1: //Residential Purchase
					switch ($subtype) {
						case 1: //SFR
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											);
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Unit Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 4: //Land
							# code...
							$features[0] = array(
											"Lot Size"=>array("0-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											"Zoned"=>array("1 Unit","2 Units","3-4 Units","5-20 Units","20+ Units"),
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				case 2: //Residential Lease
					switch ($subtype) {
						case 1: //SFR
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Bldg. Sq. Ft"=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Term"=>array("Short Term","M to M","Year Lease","Multi Year Lease","Lease Option"),
											"Possession"=>array("Immediately","Within 30 days","Within 60 days","Within 90 days","Within 180 days"),
											"Pet"=>array("0","1"),
											"Furnished"=>array("0","1"),
											);
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$features[0]= array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Unit Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Term"=>array("Short Term","M to M","Year Lease","Multi Year Lease","Lease Option"),
											"Pet"=>array("0","1"),
											"Furnished"=>array("0","1"),
											);
							break;
						case 4: //Land
							# code...
							$features[0] = array(
											"Lot Size"=>array("0-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											"Zoned"=>array("1 Unit","2 Units","3-4 Units","5-20 Units","20+ Units"),
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				case 3: //Commercial Purchase
					switch ($subtype) {
						case 1: // Multi Family
							# code...
							$features[0] = array(
											"Units"=>array("Duplex","TriPlex","Quad","5-9","10-15","16-29","30-50","50-100","101-150","151-250","251+","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),
											"GRM"=>array("1-2","3-4","4-5","5-6","6-7","7-8","8-9","9-10","10-11","11-12","12-13","13-14","14-15","15-16","16-17","17-18","18-19","19-20","20+"),
											"Bldg. Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed","Land Only"),										
											"Lot Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),																				
											);
							break;
						case 2: //Office
							# code...
							$features[0] = array(
											"Type"=>array("Office","Institutional","Medical","Warehouse","R&D","Business Park","Land"),
											"Class"=>array("A","B","C","D","Not Disclosed"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Bldg. Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000-249,000","250,000-499,000","500,000+","Undisclosed","Land Only"),										
											);
							break;
						case 3: //Industrial
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),			
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 4: // Retail
							# code...
							$features[0] = array(
											"Type"=>array("Community Center","Strip Center","Outlet Center","Power Center","Anchor","Restaurant","Service Station","Retail Pad","Free Standing","Day Care/Nursery","Post Office","Vehicle"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Bldg. Sq. Ft"=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000-249,000","250,000-499,000","500,000+","Undisclosed","Land Only"),										
											);
							break;	
						case 5: // Motel/Hotel
							# code...
							$features[0] = array(
											"Type"=>array("Economy","Full Service","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Room Count"=>array("1-9","10-19","20-29","30-39","40-49","50-99","100-149","150-199","200+"),										
											);
							break;	
						case 6: // Assisted Care
							# code...
							$features[0] = array(
											"Type"=>array("Assisted","Acute Care","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Room Count"=>array("1-9","10-19","20-29","30-39","40-49","50-99","100-149","150-199","200+"),										
											);
							break;	
						case 7: // Special Purpose
							# code...
							$features[0] = array(
											"Type"=>array("Golf","Marina","Theater","Religious","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),																		
											);
							break;				
						default:
							# code...
							break;
					}
					break;
				case 4: //Commercial Lease
					switch ($subtype) {
						case 1:// Office
							# code...
							$features[0] = array(
											"Type"=>array("Office","Institutional","Medical","Warehouse","R&D","Business Park","Land"),
											"Class"=>array("A","B","C","D","Not Disclosed"),
											"Available Sq.Ft"=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),										
											);
							break;
						case 2: //Industrial
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),
											"Available Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Lot Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 3: //Retail
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				default:
					# code...
					break;
			}
		}
		elseif($formtype=="abuyer"){
			switch ($ptype) {
				case 1: //Residential Purchase
					switch ($subtype) {
						case 1: //SFR
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											//"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											);
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Unit Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 4: //Land
							# code...
							$features[0] = array(
											"Lot Size"=>array("0-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											//"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											"Zoned"=>array("1 Unit","2 Units","3-4 Units","5-20 Units","20+ Units"),
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				case 2: //Residential Lease
					switch ($subtype) {
						case 1: //SFR
							# code...
							$features[0] = array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Bldg. Sq. Ft"=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Term"=>array("Short Term","M to M","Year Lease","Multi Year Lease","Lease Option"),
											"Possession"=>array("Immediately","Within 30 days","Within 60 days","Within 90 days","Within 180 days"),
											"Pet"=>array("0","1"),
											"Furnished"=>array("0","1"),
											);
							break;
						case 2: //Condo
						case 3: //Townhouse
							# code...
							$features[0]= array(
											"Bedrooms"=>array("1","2","3","4","5","6+"),
											"Bathrooms"=>array("1","2","3","4","5","6+"),
											"Unit Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Term"=>array("Short Term","M to M","Year Lease","Multi Year Lease","Lease Option"),
											"Pet"=>array("0","1"),
											"Furnished"=>array("0","1"),
											);
							break;
						case 4: //Land
							# code...
							$features[0] = array(
											"Lot Size"=>array("0-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											"View"=>array("None","Panoramic","City","Mountains/Hills","Coastline","Water","Ocean","Lake/River","Landmark","Desert","Bay","Vineyard","Golf","Other"),
											"Zoned"=>array("1 Unit","2 Units","3-4 Units","5-20 Units","20+ Units"),
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				case 3: //Commercial Purchase
					switch ($subtype) {
						case 1: // Multi Family
							# code...
							$features[0] = array(
											"Units"=>array("Duplex","TriPlex","Quad","5-9","10-15","16-29","30-50","50-100","101-150","151-250","251+","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),
											"GRM"=>array("1-2","3-4","4-5","5-6","6-7","7-8","8-9","9-10","10-11","11-12","12-13","13-14","14-15","15-16","16-17","17-18","18-19","19-20","20+"),
											"Bldg. Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed","Land Only"),										
											"Lot Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),																				
											);
							break;
						case 2: //Office
							# code...
							$features[0] = array(
											"Type"=>array("Office","Institutional","Medical","Warehouse","R&D","Business Park","Land"),
											"Class"=>array("A","B","C","D","Not Disclosed"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Bldg. Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000-249,000","250,000-499,000","500,000+","Undisclosed","Land Only"),										
											);
							break;
						case 3: //Industrial
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 4: // Retail
							# code...
							$features[0] = array(
											"Type"=>array("Community Center","Strip Center","Outlet Center","Power Center","Anchor","Restaurant","Service Station","Retail Pad","Free Standing","Day Care/Nursery","Post Office","Vehicle"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Bldg. Sq. Ft"=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000-249,000","250,000-499,000","500,000+","Undisclosed","Land Only"),										
											);
							break;	
						case 5: // Motel/Hotel
							# code...
							$features[0] = array(
											"Type"=>array("Economy","Full Service","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Room Count"=>array("1-9","10-19","20-29","30-39","40-49","50-99","100-149","150-199","200+"),										
											);
							break;	
						case 6: // Assisted Care
							# code...
							$features[0] = array(
											"Type"=>array("Assisted","Acute Care","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),										
											"Room Count"=>array("1-9","10-19","20-29","30-39","40-49","50-99","100-149","150-199","200+"),										
											);
							break;	
						case 7: // Special Purpose
							# code...
							$features[0] = array(
											"Type"=>array("Golf","Marina","Theater","Religious","Land"),
											"Cap Rate"=>array("0-.9","1-1.9","2-2.9","3-3.9","4-4.9","5-5.9","6-6.9","7-7.9","8-8.9","9-9.9","10-10.9","11-11.9","12-12.9","13-13.9","14-14.9","15+","Not Disclosed"),																		
											);
							break;				
						default:
							# code...
							break;
					}
					break;
				case 4: //Commercial Lease
					switch ($subtype) {
						case 1:// Office
							# code...
							$features[0] = array(
											"Type"=>array("Office","Institutional","Medical","Warehouse","R&D","Business Park","Land"),
											"Class"=>array("A","B","C","D","Not Disclosed"),
											"Available Sq.Ft"=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),										
											);
							break;
						case 2: //Industrial
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),
											"Available Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											"Lot Sq. Ft."=>array("1-999","1,000-1,999","2,000-4,999","5,000-9,999","10,000-19,999","20,000-29,999","30,000-49,999","50,000-74,999","75,000-149,999","150,000+","Undisclosed"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											);
							break;
						case 3: //Retail
							# code...
							$features[0] = array(
											"Type"=>array("Flex Space","Business Park","Condo","Land","Manufacturing","Office Showroom","R&D","Self/Mini Storage","Truck Terminal/Hub","Warehouse","Distribution","Cold Storage"),
											"Bldg. Sq. Ft."=>array("1-499","500-999","1,000-1,999","2,000-2,499","2,500-2,999","3,000-3,999","4,000-4,999","5,000-7,499","7,500-10,000","10,001-19,999","20,000+","Undisclosed"),
											//"Type Lease"=>array("NNN","FSG","MG","Modified Net","Not disclosed"),
											
											);
							break;					
						default:
							# code...
							break;
					}
					break;
				default:
					# code...
					break;
			}
		}
		$response = array('status'=>1, 'message'=>"Found Buyers", 'features'=>$features);
		echo json_encode($response);
	}
function formedit(){
	    $user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$defsqft = "Sq. Ft.";	

		$model = $this->getModel($this->getName());
		$getMeasurement = $model->getSqMeasureByCountry($_GET['country']);
		if($getMeasurement['sqmeasure'] == 'sq. ft.'){
			$defsqft = JText::_('COM_POPS_TERMS_BLDG');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE');
		} else {
			$defsqft = JText::_('COM_POPS_TERMS_BLDG_M');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL_M');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ_M');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT_M');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE_M');
		}
		$buyer_data = $_POST['buyerdata'];
		$data = $_POST['data'];
		?>
<div
	class="pocket-form">
	<div 
		id="prop_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></h2></div>
	<div 
		id="buyer_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your buyer needs. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_MINIMUM_FEATURES') ?></h2></div>
	<?php
	switch ($_POST['stype']){
		// RESIDENTIAL PURCHASE + SFR
		case 1:
			?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select
			id="jform_bedroom"
			name="jform[bedroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500-10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
    	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]" class="reqfield">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?>/option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CONDITION');?></label> <select id="jform_condition"
			name="jform[condition]">
			<?php
				$selected_condition = $buyer_data[0]['needs'][0]['condition'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Fixer" <?php echo ($selected_condition == "Fixer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FIX')?></option>
			<option value="Good" <?php echo ($selected_condition == "Good" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOOD')?></option>
			<option value="Excellent" <?php echo ($selected_condition == "Excellent" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EXCELLENT')?></option>
			<option value="Remodeled" <?php echo ($selected_condition == "Remodeled" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_REMODEL')?></option>
			<option value="New Construction" <?php echo ($selected_condition == "New Construction" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NEWCONSTRUCT')?></option>
			<option value="Under Construction" <?php ($selected_condition == "Under Construction" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDERCONSTRUCT')?></option>
			<option value="Not Disclosed" <?php echo ($selected_condition == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!-- -------------------------------------------------------------- -->
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL_SPA')?></option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Water Access" <?php echo ($selected_features1 == "Water Access" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATERACCESS')?></option>
			<option value="Horse Property" <?php echo ($selected_features1 == "Horse Property" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSE')?></option>
			<option value="Golf Course" <?php echo ($selected_features1 == "Golf Course" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLFCOURSE')?></option>
			<option value="Walkstreet" <?php echo ($selected_features1 == "Walkstreet" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WALKSTREET')?></option>
			<option value="Media Room" <?php echo ($selected_features1 == "Media Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDIA')?></option>
			<option value="Guest House" <?php echo ($selected_features1 == "Guest House" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GUESTHOUSE')?></option>
			<option value="Wine Cellar" <?php echo ($selected_features1 == "Wine Cellar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WINECELLAR')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Den/Library" <?php echo ($selected_features1 == "Den/Library" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DENLIB')?></option>
			<option value="Green Const." <?php echo ($selected_features1 == "Green Const." ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREENCONSTRUCT')?></option>
			<option value="Basement" <?php echo ($selected_features1 == "Basement" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BASEMENT')?></option>
			<option value="RV/Boat Parking" <?php echo ($selected_features1 == "RV/Boat Parking" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RV')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Water Access" <?php echo ($selected_features2 == "Water Access" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATERACCESS')?></option>
			<option value="Horse Property" <?php echo ($selected_features2 == "Horse Property" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSE')?></option>
			<option value="Golf Course" <?php echo ($selected_features2 == "Golf Course" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLFCOURSE')?></option>
			<option value="Walkstreet" <?php echo ($selected_features2 == "Walkstreet" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WALKSTREET')?></option>
			<option value="Media Room" <?php echo ($selected_features2 == "Media Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDIA')?></option>
			<option value="Guest House" <?php echo ($selected_features2 == "Guest House" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GUESTHOUSE')?></option>
			<option value="Wine Cellar" <?php echo ($selected_features2 == "Wine Cellar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WINECELLAR')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Den/Library" <?php echo ($selected_features2 == "Den/Library" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DENLIB')?></option>
			<option value="Green Const." <?php echo ($selected_features2 == "Green Const." ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREENCONSTRUCT')?></option>
			<option value="Basement" <?php echo ($selected_features2 == "Basement" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BASEMENT')?></option>
			<option value="RV/Boat Parking" <?php echo ($selected_features2 == "RV/Boat Parking" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RV')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features3"
			name="jform[features3]">
			<?php
				$selected_features3 = $buyer_data[0]['needs'][0]['features3'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="One Story" <?php echo ($selected_features3 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features3 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features3 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Water Access" <?php echo ($selected_features3 == "Water Access" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATERACCESS')?></option>
			<option value="Horse Property" <?php echo ($selected_features3 == "Horse Property" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSE')?></option>
			<option value="Golf Course" <?php echo ($selected_features3 == "Golf Course" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLFCOURSE')?></option>
			<option value="Walkstreet" <?php echo ($selected_features3 == "Walkstreet" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WALKSTREET')?></option>
			<option value="Media Room" <?php echo ($selected_features3 == "Media Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDIA')?></option>
			<option value="Guest House" <?php echo ($selected_features3 == "Guest House" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GUESTHOUSE')?></option>
			<option value="Wine Cellar" <?php echo ($selected_features3 == "Wine Cellar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WINECELLAR')?></option>
			<option value="Tennis Court" <?php echo ($selected_features3 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Den/Library" <?php echo ($selected_features3 == "Den/Library" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DENLIB')?></option>
			<option value="Green Const." <?php echo ($selected_features3 == "Green Const." ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREENCONSTRUCT')?></option>
			<option value="Basement" <?php echo ($selected_features3 == "Basement" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BASEMENT')?></option>
			<option value="RV/Boat Parking" <?php echo ($selected_features3 == "RV/Boat Parking" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RV')?></option>
			<option value="Senior" <?php echo ($selected_features3 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + CONDO
case 2:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select id="jform_bedroom"
			name="jform[bedroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
				<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_u ?></label> <select id="jform_unitsqft"
			name="jform[unitsqft]" class="reqfield">
			<?php
				$selected_unitsqft = $buyer_data[0]['needs'][0]['unit_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_unitsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
    <div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BLDG_TYPE');?></label> <select id="jform_bldgtype"
			name="jform[bldgtype]">
			<?php
				$selected_bldgtype = $buyer_data[0]['needs'][0]['bldg_type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NORTH')?></option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SOUTH')?></option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EAST')?></option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WEST')?></option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LOWRISE')?></option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDRISE')?></option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HIGHRISE')?></option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COOP')?></option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL_SPA')?></option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE
case 3:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select
			id="jform_bedroom"
			name="jform[bedroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_u ?></label> <select id="jform_bldgsqft"
			name="jform[unitsqft]" class="reqfield">
			<?php
				$selected_unitsqft = $buyer_data[0]['needs'][0]['unit_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label>
			<select id="jform_view" name="jform[view]">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		   </select>
			<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
    <div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BLDG_TYPE');?></label> <select id="jform_bldgtype"
			name="jform[bldgtype]">
			<?php
				$selected_bldgtype = $buyer_data[0]['needs'][0]['bldg_type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NORTH')?></option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SOUTH')?></option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EAST')?></option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WEST')?></option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LOWRISE')?></option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDRISE')?></option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HIGHRISE')?></option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COOP')?></option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + LAND
case 4:
	?>
	<div class="left ys push-top2">
		<label><?php echo $defsqft_ls ?></label> <select id="jform_lotsize"
			name="jform[lotsize]" class="reqfield">
			<?php
				$selected_lotsize = $buyer_data[0]['needs'][0]['lot_size'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsize == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsize == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsize == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsize == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsize == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsize == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsize == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsize == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsize == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsize == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsize == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsize error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]" class="reqfield">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_ZONED') ?></label> <select id="jform_zoned" class="reqfield" name="jform[zoned]">
			<?php
				$selected_zoned = $buyer_data[0]['needs'][0]['zoned'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1" <?php echo ($selected_zoned == "1" ? "selected" : ""); ?>>1 <?php echo JText::_('COM_POPS_TERMS_UNITONE')?></option>
			<option value="2" <?php echo ($selected_zoned == "2" ? "selected" : ""); ?>>2 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="3-4" <?php echo ($selected_zoned == "3-4" ? "selected" : ""); ?>>3-4 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="5-20" <?php echo ($selected_zoned == "5-20" ? "selected" : ""); ?>>5-20 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="20" <?php echo ($selected_zoned == "20" ? "selected" : ""); ?>>20+ <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
		</select>
		<p class="jform_zoned error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Sidewalks" <?php echo ($selected_features1 == "Sidewalks" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SIDEWALKS')?></option>
			<option value="Utilities" <?php echo ($selected_features1 == "Utilities" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UTILITIES')?></option>
			<option value="Curbs" <?php echo ($selected_features1 == "Curbs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CURBS')?></option>
			<option value="Horse Trails" <?php echo ($selected_features1 == "Horse Trails" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSETRAILS')?></option>
			<option value="Rural" <?php echo ($selected_features1 == "Rural" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RURAL')?></option>
			<option value="Urban" <?php echo ($selected_features1 == "Urban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_URBAN')?></option>
			<option value="Suburban" <?php echo ($selected_features1 == "Suburban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SUBURBAN')?></option>
			<option value="Permits" <?php echo ($selected_features1 == "Permits" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PERMITS')?></option>
			<option value="HOA" <?php echo ($selected_features1 == "HOA" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HOA')?></option>
			<option value="Sewer" <?php echo ($selected_features1 == "Sewer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SEWER')?></option>
			<option value="CC&Rs" <?php echo ($selected_features1 == "CC&Rs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CCR')?></option>
			<option value="Coastal" <?php echo ($selected_features1 == "Coastal" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COASTAL')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Sidewalks" <?php echo ($selected_features2 == "Sidewalks" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SIDEWALKS')?></option>
			<option value="Utilities" <?php echo ($selected_features2 == "Utilities" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UTILITIES')?></option>
			<option value="Curbs" <?php echo ($selected_features2 == "Curbs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CURBS')?></option>
			<option value="Horse Trails" <?php echo ($selected_features2 == "Horse Trails" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSETRAILS')?></option>
			<option value="Rural" <?php echo ($selected_features2 == "Rural" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RURAL')?></option>
			<option value="Urban" <?php echo ($selected_features2 == "Urban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_URBAN')?></option>
			<option value="Suburban" <?php echo ($selected_features2 == "Suburban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SUBURBAN')?></option>
			<option value="Permits" <?php echo ($selected_features2 == "Permits" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PERMITS')?></option>
			<option value="HOA" <?php echo ($selected_features2 == "HOA" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HOA')?></option>
			<option value="Sewer" <?php echo ($selected_features2 == "Sewer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SEWER')?></option>
			<option value="CC&Rs" <?php echo ($selected_features2 == "CC&Rs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CCR')?></option>
			<option value="Coastal" <?php echo ($selected_features2 == "Coastal" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COASTAL')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + SFR
case 5:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select
			class="reqfield"
			id="jform_bedroom"
			name="jform[bedroom]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TERM') ?></label> <select id="jform_term" name="jform[term]" class="reqfield">
			<?php
				$selected_term = $buyer_data[0]['needs'][0]['term'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SHORTTERM')?></option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_M2M')?></option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_YEARLEASE')?></option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIYEARLEASE')?></option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LEASEOPTION')?></option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POSSESSION') ?></label> <select id="jform_possession"
			name="jform[possession]" class="reqfield">
			<?php
				$selected_possession = $buyer_data[0]['needs'][0]['possession'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_IMMEDIATELY')?></option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_30DAYS')?></option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_60DAYS')?></option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_90DAYS')?></option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_180DAYS')?></option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label><?php echo JText::_('COM_POPS_TERMS_PET') ?></label>
        <?php $selected_pet = $buyer_data[0]['needs'][0]['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="pyes" style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a href="javascript: void(0)" id="pno" class="left gradient-gray no pet" rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
		<input type="hidden" value="<?php echo $selected_pet ?><?php echo $data_pet ?>" onchange="get_pet(this.value)" id="jform_pet" name="jform[pet]" class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px; width:100px">
		<label><?php echo JText::_('COM_POPS_TERMS_FURNISHED') ?></label>
        <?php $selected_furnished = $buyer_data[0]['needs'][0]['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="fyes"
            class="left gradient-gray no furnished" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a
			href="javascript: void(0)"
            id="fno"
            class="left gradient-blue-toggle yes furnished"
			rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
        <input onchange="get_furnished(this.value)" value="<?php echo $selected_furnished ?><?php echo $data_furnished ?>" type="hidden" value="<?php echo $selected_furnished ?>" id="jform_furnished"
			name="jform[furnished]" class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]" >
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL_SPA')?></option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CONDITION');?></label> <select id="jform_condition"
			name="jform[condition]">
			<?php
				$selected_condition = $buyer_data[0]['needs'][0]['condition'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Fixer" <?php echo ($selected_condition == "Fixer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FIX')?></option>
			<option value="Good" <?php echo ($selected_condition == "Good" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOOD')?></option>
			<option value="Excellent" <?php echo ($selected_condition == "Excellent" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EXCELLENT')?></option>
			<option value="Remodeled" <?php echo ($selected_condition == "Remodeled" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_REMODEL')?></option>
			<option value="New Construction" <?php echo ($selected_condition == "New Construction" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NEWCONSTRUCT')?></option>
			<option value="Under Construction" <?php ($selected_condition == "Under Construction" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDERCONSTRUCT')?></option>
			<option value="Not Disclosed" <?php echo ($selected_condition == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
    <div class="clear-float"></div>
    <!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Water Access" <?php echo ($selected_features1 == "Water Access" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATERACCESS')?></option>
			<option value="Horse Property" <?php echo ($selected_features1 == "Horse Property" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSE')?></option>
			<option value="Golf Course" <?php echo ($selected_features1 == "Golf Course" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLFCOURSE')?></option>
			<option value="Walkstreet" <?php echo ($selected_features1 == "Walkstreet" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WALKSTREET')?></option>
			<option value="Media Room" <?php echo ($selected_features1 == "Media Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDIA')?></option>
			<option value="Guest House" <?php echo ($selected_features1 == "Guest House" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GUESTHOUSE')?></option>
			<option value="Wine Cellar" <?php echo ($selected_features1 == "Wine Cellar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WINECELLAR')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Den/Library" <?php echo ($selected_features1 == "Den/Library" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DENLIB')?></option>
			<option value="Green Const." <?php echo ($selected_features1 == "Green Const." ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREENCONSTRUCT')?></option>
			<option value="Basement" <?php echo ($selected_features1 == "Basement" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BASEMENT')?></option>
			<option value="RV/Boat Parking" <?php echo ($selected_features1 == "RV/Boat Parking" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RV')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Water Access" <?php echo ($selected_features2 == "Water Access" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATERACCESS')?></option>
			<option value="Horse Property" <?php echo ($selected_features2 == "Horse Property" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSE')?></option>
			<option value="Golf Course" <?php echo ($selected_features2 == "Golf Course" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLFCOURSE')?></option>
			<option value="Walkstreet" <?php echo ($selected_features2 == "Walkstreet" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WALKSTREET')?></option>
			<option value="Media Room" <?php echo ($selected_features2 == "Media Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDIA')?></option>
			<option value="Guest House" <?php echo ($selected_features2 == "Guest House" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GUESTHOUSE')?></option>
			<option value="Wine Cellar" <?php echo ($selected_features2 == "Wine Cellar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WINECELLAR')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Den/Library" <?php echo ($selected_features2 == "Den/Library" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DENLIB')?></option>
			<option value="Green Const." <?php echo ($selected_features2 == "Green Const." ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREENCONSTRUCT')?></option>
			<option value="Basement" <?php echo ($selected_features2 == "Basement" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BASEMENT')?></option>
			<option value="RV/Boat Parking" <?php echo ($selected_features2 == "RV/Boat Parking" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RV')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + Condo
case 6:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select id="jform_bedroom"
			name="jform[bedroom]"class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_u ?></label> <select id="jform_bldgsqft"
			name="jform[unitsqft]" class="reqfield">
			<?php
				$selected_unitsqft = $buyer_data[0]['needs'][0]['unit_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TERM') ?></label> <select id="jform_term" name="jform[term]" class="reqfield">
			<?php
				$selected_term = $buyer_data[0]['needs'][0]['term'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SHORTTERM')?></option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_M2M')?></option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_YEARLEASE')?></option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIYEARLEASE')?></option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LEASEOPTION')?></option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label><?php echo JText::_('COM_POPS_TERMS_PET') ?></label>
        <?php $selected_pet = $buyer_data[0]['needs'][0]['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="pyes" style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a href="javascript: void(0)" id="pno" class="left gradient-gray no pet" rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
		<input type="hidden" value="<?php echo $selected_pet ?><?php echo $data_pet ?>" onchange="get_pet(this.value)" id="jform_pet" name="jform[pet]"
			class="text-input reqfield" />
	</div>
  	<div class="left ys" style="margin-top: 30px; width:100px">
		<label><?php echo JText::_('COM_POPS_TERMS_FURNISHED') ?></label>
        <?php $selected_furnished = $buyer_data[0]['needs'][0]['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="fyes"
            class="left gradient-gray no furnished" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a
			href="javascript: void(0)"
            id="fno"
            class="left gradient-blue-toggle yes furnished"
			rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
        <input onchange="get_furnished(this.value)" value="<?php echo $selected_furnished ?><?php echo $data_furnished ?>" type="hidden" id="jform_furnished"
			name="jform[furnished]" class="text-input reqfield" />
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]">			
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POSSESSION') ?></label> <select id="jform_possession"
			name="jform[possession]">
			<?php
				$selected_possession = $buyer_data[0]['needs'][0]['possession'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_IMMEDIATELY')?></option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_30DAYS')?></option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_60DAYS')?></option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_90DAYS')?></option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_180DAYS')?></option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
    <div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BLDG_TYPE');?></label> <select id="jform_bldgtype" name="jform[bldgtype]">
			<?php
				$selected_bldgtype = $buyer_data[0]['needs'][0]['bldg_type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NORTH')?></option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SOUTH')?></option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EAST')?></option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WEST')?></option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LOWRISE')?></option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDRISE')?></option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HIGHRISE')?></option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COOP')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">			
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL_SPA')?></option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	 <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Townhouse/Row House
case 7:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BEDS') ?></label> <select
			class="reqfield"
			id="jform_bedroom"
			name="jform[bedroom]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bedroom = $buyer_data[0]['needs'][0]['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BATHROOMS');?></label> <select id="jform_bathroom"
			name="jform[bathroom]" class="reqfield">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_u ?></label> <select id="jform_bldgsqft" class="reqfield"
			name="jform[unitsqft]">
			<?php
				$selected_unitsqft = $buyer_data[0]['needs'][0]['unit_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TERM') ?></label> <select id="jform_term" name="jform[term]" class="reqfield">
			<?php
				$selected_term = $buyer_data[0]['needs'][0]['term'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SHORTTERM')?></option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_M2M')?></option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_YEARLEASE')?></option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIYEARLEASE')?></option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LEASEOPTION')?></option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label><?php echo JText::_('COM_POPS_TERMS_PET') ?></label>
        <?php $selected_pet = $buyer_data[0]['needs'][0]['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="pyes" style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a href="javascript: void(0)" id="pno" class="left gradient-gray no pet" rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
		<input type="hidden" value="<?php echo $selected_pet ?><?php echo $data_pet ?>" onchange="get_pet(this.value)" id="jform_pet" name="jform[pet]"
			class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px; width:100px">
		<label><?php echo JText::_('COM_POPS_TERMS_FURNISHED') ?></label>
        <?php $selected_furnished = $buyer_data[0]['needs'][0]['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="fyes"
            class="left gradient-gray no furnished" rel="1"><?php echo JText::_('COM_POPS_TERMS_PETYES')?></a>
        <a
			href="javascript: void(0)"
            id="fno"
            class="left gradient-blue-toggle yes furnished"
			rel="0"><?php echo JText::_('COM_POPS_TERMS_PETNO')?></a>
        <input onchange="get_furnished(this.value)" value="<?php echo $selected_furnished ?><?php echo $data_furnished ?>" type="hidden" value="<?php echo $selected_furnished ?>" id="jform_furnished"
			name="jform[furnished]" class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POSSESSION') ?></label> <select id="jform_possession"
			name="jform[possession]">
			<?php
				$selected_possession = $buyer_data[0]['needs'][0]['possession'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_IMMEDIATELY')?></option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_30DAYS')?></option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_60DAYS')?></option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_90DAYS')?></option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_180DAYS')?></option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_BLDG_TYPE');?></label> <select id="jform_bldgtype"
			name="jform[bldgtype]">
			<?php
				$selected_bldgtype = $buyer_data[0]['needs'][0]['bldg_type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NORTH')?></option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SOUTH')?></option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_EAST')?></option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WEST')?></option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LOWRISE')?></option>
			<option value="Detached" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DETACHED')?></option>
			<option value="Attached" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ATTACHED')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_POOL_SPA') ?></label> <select id="jform_poolspa"
			name="jform[poolspa]">
			<?php
				$selected_poolspa = $buyer_data[0]['needs'][0]['pool_spa'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL_SPA')?></option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label><?php echo JText::_('COM_POPS_TERMS_GARAGE') ?></label> <select id="jform_garage" name="jform[garage]">
			<?php
				$selected_garage = $buyer_data[0]['needs'][0]['garage'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GYM')?></option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECURITY')?></option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TENNISCOURT')?></option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DOORMAN')?></option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PENTHOUSE')?></option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ONESTORY')?></option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TWOSTORY')?></option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THREESTORY')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Land
case 8:
	?>
	<div class="left ys push-top2">
		<label><?php echo $defsqft_ls ?></label> <select
			class="reqfield"
			id="jform_lotsize"
			name="jform[lotsize]">
			<?php
				$selected_lotsize = $buyer_data[0]['needs'][0]['lot_size'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsize == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsize == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsize == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsize == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsize == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsize == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsize == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsize == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsize == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsize == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsize == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsize error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_ZONED') ?></label> <select  id="jform_zoned" name="jform[zoned]" class="reqfield">
			<?php
				$selected_zoned = $buyer_data[0]['needs'][0]['zoned'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1" <?php echo ($selected_zoned == "1" ? "selected" : ""); ?>>1 <?php echo JText::_('COM_POPS_TERMS_UNITONE')?></option>
			<option value="2" <?php echo ($selected_zoned == "2" ? "selected" : ""); ?>>2 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="3-4" <?php echo ($selected_zoned == "3-4" ? "selected" : ""); ?>>3-4 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="5-20" <?php echo ($selected_zoned == "5-20" ? "selected" : ""); ?>>5-20 <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
			<option value="20" <?php echo ($selected_zoned == "20" ? "selected" : ""); ?>>20+ <?php echo JText::_('COM_POPS_TERMS_UNIT')?></option>
		</select>
		<p class="jform_zoned error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]"class="reqfield">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Sidewalks" <?php echo ($selected_features1 == "Sidewalks" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SIDEWALKS')?></option>
			<option value="Utilities" <?php echo ($selected_features1 == "Utilities" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UTILITIES')?></option>
			<option value="Curbs" <?php echo ($selected_features1 == "Curbs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CURBS')?></option>
			<option value="Horse Trails" <?php echo ($selected_features1 == "Horse Trails" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSETRAILS')?></option>
			<option value="Rural" <?php echo ($selected_features1 == "Rural" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RURAL')?></option>
			<option value="Urban" <?php echo ($selected_features1 == "Urban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_URBAN')?></option>
			<option value="Suburban" <?php echo ($selected_features1 == "Suburban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SUBURBAN')?></option>
			<option value="Permits" <?php echo ($selected_features1 == "Permits" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PERMITS')?></option>
			<option value="HOA" <?php echo ($selected_features1 == "HOA" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HOA')?></option>
			<option value="Sewer" <?php echo ($selected_features1 == "Sewer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SEWER')?></option>
			<option value="CC&Rs" <?php echo ($selected_features1 == "CC&Rs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CCR')?></option>
			<option value="Coastal" <?php echo ($selected_features1 == "Coastal" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COASTAL')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Sidewalks" <?php echo ($selected_features2 == "Sidewalks" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SIDEWALKS')?></option>
			<option value="Utilities" <?php echo ($selected_features2 == "Utilities" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UTILITIES')?></option>
			<option value="Curbs" <?php echo ($selected_features2 == "Curbs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CURBS')?></option>
			<option value="Horse Trails" <?php echo ($selected_features2 == "Horse Trails" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HORSETRAILS')?></option>
			<option value="Rural" <?php echo ($selected_features2 == "Rural" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RURAL')?></option>
			<option value="Urban" <?php echo ($selected_features2 == "Urban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_URBAN')?></option>
			<option value="Suburban" <?php echo ($selected_features2 == "Suburban" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SUBURBAN')?></option>
			<option value="Permits" <?php echo ($selected_features2 == "Permits" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PERMITS')?></option>
			<option value="HOA" <?php echo ($selected_features2 == "HOA" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HOA')?></option>
			<option value="Sewer" <?php echo ($selected_features2 == "Sewer" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SEWER')?></option>
			<option value="CC&Rs" <?php echo ($selected_features2 == "CC&Rs" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CCR')?></option>
			<option value="Coastal" <?php echo ($selected_features2 == "Coastal" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COASTAL')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Multi Family
case 9:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_UNIT') ?></label>
			<select id="jform_units" name="jform[units]" class="reqfield">
				<?php
					$selected_units = $buyer_data[0]['needs'][0]['units'];
					$selected_units = $data['units'];
				?>
				<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
				<option value="2" <?php echo ($selected_units == "2" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DUPLEX')?></option>
				<option value="3" <?php echo ($selected_units == "3" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TRIPLEX')?></option>
				<option value="4" <?php echo ($selected_units == "4" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_QUAD')?></option>
				<option value="5-9" <?php echo ($selected_units == "5-9" ? "selected" : ""); ?>>5-9</option>
				<option value="10-15" <?php echo ($selected_units == "10-15" ? "selected" : ""); ?>>10-15</option>
				<option value="16-29" <?php echo ($selected_units == "16-29" ? "selected" : ""); ?>>16-29</option>
				<option value="30-50" <?php echo ($selected_units == "30-50" ? "selected" : ""); ?>>30-50</option>
				<option value="50-100" <?php echo ($selected_units == "50-100" ? "selected" : ""); ?>>50-100</option>
				<option value="101-150" <?php echo ($selected_units == "101-150" ? "selected" : ""); ?>>101-150</option>
				<option value="151-250" <?php echo ($selected_units == "151-250" ? "selected" : ""); ?>>151-250</option>
				<option value="251" <?php echo ($selected_units == "251" ? "selected" : ""); ?>>251+</option>
				<option value="Land" <?php echo ($selected_units == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
			</select>
		<p class="jform_units error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_GRM');?></label> <select id="jform_grm" name="jform[grm]" class="reqfield">
			<?php
				$selected_grm = $buyer_data[0]['needs'][0]['grm'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0" <?php echo ($selected_grm == "0" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
			<option value="1-2" <?php echo ($selected_grm == "1-2" ? "selected" : ""); ?>>1-2</option>
			<option value="3-4" <?php echo ($selected_grm == "3-4" ? "selected" : ""); ?>>3-4</option>
			<option value="4-5" <?php echo ($selected_grm == "4-5" ? "selected" : ""); ?>>4-5</option>
			<option value="5-6" <?php echo ($selected_grm == "5-6" ? "selected" : ""); ?>>5-6</option>
			<option value="6-7" <?php echo ($selected_grm == "6-7" ? "selected" : ""); ?>>6-7</option>
			<option value="7-8" <?php echo ($selected_grm == "7-8" ? "selected" : ""); ?>>7-8</option>
			<option value="8-9" <?php echo ($selected_grm == "8-9" ? "selected" : ""); ?>>8-9</option>
			<option value="9-10" <?php echo ($selected_grm == "9-10" ? "selected" : ""); ?>>9-10</option>
			<option value="10-11" <?php echo ($selected_grm == "10-11" ? "selected" : ""); ?>>10-11</option>
			<option value="11-12" <?php echo ($selected_grm == "11-12" ? "selected" : ""); ?>>11-12</option>
			<option value="12-13" <?php echo ($selected_grm == "12-13" ? "selected" : ""); ?>>12-13</option>
			<option value="13-14" <?php echo ($selected_grm == "13-14" ? "selected" : ""); ?>>13-14</option>
			<option value="14-15" <?php echo ($selected_grm == "14-15" ? "selected" : ""); ?>>14-15</option>
			<option value="15-16" <?php echo ($selected_grm == "15-16" ? "selected" : ""); ?>>15-16</option>
			<option value="16-17" <?php echo ($selected_grm == "16-17" ? "selected" : ""); ?>>16-17</option>
			<option value="17-18" <?php echo ($selected_grm == "17-18" ? "selected" : ""); ?>>17-18</option>
			<option value="18-19" <?php echo ($selected_grm == "18-19" ? "selected" : ""); ?>>18-19</option>
			<option value="19-20" <?php echo ($selected_grm == "19-20" ? "selected" : ""); ?>>19-20</option>
			<option value="20" <?php echo ($selected_grm == "20" ? "selected" : ""); ?>>20+</option>
		</select>
		<p class="jform_grm error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
    <div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft" name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_bldgsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft" name="jform[lotsqft]" class="reqfield">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
    <div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_VIEW') ?></label> <select id="jform_view" name="jform[view]">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NONE')?></option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PANORAMIC')?></option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CITY')?></option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MOUNT')?></option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COAST')?></option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WATER')?></option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OCEAN')?></option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAKE')?></option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LANDMARK')?></option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DESERT')?></option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAY')?></option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VINE')?></option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
<!---------------------------------------------------------------- -->
	<div style="clear:both"></div>
    <div class="left ys push-top2" style="margin-top: 24x;">
		<label><?php echo JText::_('COM_POPS_TERMS_STYLE');?></label> 
		<select id="jform_style" style="width:200px" name="jform[style]">
			<?php
				$selected_style = $buyer_data[0]['needs'][0]['style'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_AMERICANFARM')?></option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARTDECO')?></option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIDCENT')?></option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CAPECOD')?></option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLONIAL')?></option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONTEMPORARY')?></option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CRAFTSMAN')?></option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FRENCH')?></option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ITALIAN')?></option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PRAIRIE')?></option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_PUEBLO')?></option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RANCH')?></option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SPANISH')?></option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SWISS')?></option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TUDOR')?></option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VICTORIAN')?></option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_HISTORIC')?></option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ARCHITECTURE')?></option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GREEN')?></option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_OCCUPANCY');?></label> <select id="jform_occupancy"
			name="jform[occupancy]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Undisclosed"><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
			<?php
			$selected_occupancy = $buyer_data[0]['needs'][0]['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Rent Control" <?php echo ($selected_features1 == "Rent Control" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RENTCONTROL')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
			<option value="Assoc-Pool" <?php echo ($selected_features1 == "Assoc-Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCPOOL')?></option>
			<option value="Assoc-Spa" <?php echo ($selected_features1 == "Assoc-Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCSPA')?></option>
			<option value="Assoc-Tennis" <?php echo ($selected_features1 == "Assoc-Tennis" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCTENNIS')?></option>
			<option value="Assoc-Other" <?php echo ($selected_features1 == "Assoc-Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCOTHER')?></option>
			<option value="Section 8" <?php echo ($selected_features1 == "Section 8" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECTION8')?></option>
			<option value="25% Occupied" <?php echo ($selected_features1 == "25% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_25OCCUPIED')?></option>
			<option value="50% Occupied" <?php echo ($selected_features1 == "50% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_50OCCUPIED')?></option>
			<option value="75% Occupied" <?php echo ($selected_features1 == "75% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_75OCCUPIED')?></option>
			<option value="100% Occupied" <?php echo ($selected_features1 == "100% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_100OCCUPIED')?></option>
			<option value="Cash Cow" <?php echo ($selected_features1 == "Cash Cow" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CASHCOW')?></option>
			<option value="Value Add" <?php echo ($selected_features1 == "Value Add" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VALUEADD')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Rent Control" <?php echo ($selected_features2 == "Rent Control" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RENTCONTROL')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
			<option value="Assoc-Pool" <?php echo ($selected_features2 == "Assoc-Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCPOOL')?></option>
			<option value="Assoc-Spa" <?php echo ($selected_features2 == "Assoc-Spa" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCSPA')?></option>
			<option value="Assoc-Tennis" <?php echo ($selected_features2 == "Assoc-Tennis" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCTENNIS')?></option>
			<option value="Assoc-Other" <?php echo ($selected_features2 == "Assoc-Other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSOCOTHER')?></option>
			<option value="Section 8" <?php echo ($selected_features2 == "Section 8" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SECTION8')?></option>
			<option value="25% Occupied" <?php echo ($selected_features2 == "25% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_25OCCUPIED')?></option>
			<option value="50% Occupied" <?php echo ($selected_features2 == "50% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_50OCCUPIED')?></option>
			<option value="75% Occupied" <?php echo ($selected_features2 == "75% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_75OCCUPIED')?></option>
			<option value="100% Occupied" <?php echo ($selected_features2 == "100% Occupied" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_100OCCUPIED')?></option>
			<option value="Cash Cow" <?php echo ($selected_features2 == "Cash Cow" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CASHCOW')?></option>
			<option value="Value Add" <?php echo ($selected_features2 == "Value Add" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SENIOR')?></option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VALUEADD')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Office
case 10:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select  id="jform_type" name="jform[type]" class="reqfield">
		<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Office" <?php echo ($selected_type == "Office" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OFFICE')?></option>
			<option value="Institutional" <?php echo ($selected_type == "Institutional" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_INSTITUTIONAL')?></option>
			<option value="Medical" <?php echo ($selected_type == "Medical" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDICAL')?></option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WAREHOUSE')?></option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONDO')?></option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RD')?></option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BUSINESSPARK')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CLASS') ?></label> <select id="jform_class" name="jform[class]" class="reqfield">
			<?php
				$selected_class = $buyer_data[0]['needs'][0]['listing_class'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="4" <?php echo ($selected_class == 4 ? "selected" : ""); ?>>A</option>
			<option value="3" <?php echo ($selected_class == 3 ? "selected" : ""); ?>>B</option>
			<option value="2" <?php echo ($selected_class == 2 ? "selected" : ""); ?>>C</option>
			<option value="1" <?php echo ($selected_class == 1 ? "selected" : ""); ?>>D</option>
			<option value="Not Disclosed" <?php echo ($selected_class == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_class error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft" name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
            <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_OCCUPANCY');?></label> <select id="jform_occupancy"
			name="jform[occupancy]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Undisclosed"><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
			<?php
			$selected_occupancy = $buyer_data[0]['needs'][0]['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Industrial
case 11:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Flex Space" <?php echo ($selected_type == "Flex Space" ? "selected" : ""); ?>>Flex Space</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BUSINESSPARK')?></option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONDO')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
			<option value="Manufacturing" <?php echo ($selected_type == "Manufacturing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MANUFACTURING')?></option>
			<option value="Office Showroom" <?php echo ($selected_type == "Office Showroom" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OFFICESHOWROOM')?></option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RD')?></option>
			<option value="Self/Mini Storage" <?php echo ($selected_type == "Self/Mini Storage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELFSTORAGE')?></option>
			<option value="Truck Terminal/Hub" <?php echo ($selected_type == "Truck Terminal/Hub" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TRUCK')?></option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WAREHOUSE')?></option>
			<option value="Distribution" <?php echo ($selected_type == "Distribution" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DISTRIBUTION')?></option>
			<option value="Cold Storage" <?php echo ($selected_type == "Cold Storage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLDSTORAGE')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select  id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
			<p class="jform_cap error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			 <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
			<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CEILING_HEIGHT') ?></label> <select id="jform_ceiling"
			name="jform[ceiling]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_ceiling = $buyer_data[0]['needs'][0]['ceiling_height'];
			for($i=12; $i<=34; $i=$i+2){
					if($i==$selected_ceiling) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_OCCUPANCY');?></label> <select id="jform_occupancy"
			name="jform[occupancy]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Undisclosed"><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
			<?php
			$selected_occupancy = $buyer_data[0]['needs'][0]['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Retail
case 12:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Community Center" <?php echo ($selected_type == "Community Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COMMUNITYCENTER')?></option>
			<option value="Strip Center" <?php echo ($selected_type == "Strip Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_STRIPCENTER')?></option>
			<option value="Outlet Center" <?php echo ($selected_type == "Outlet Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OUTLETCENTER')?></option>
			<option value="Power Center" <?php echo ($selected_type == "Power Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POWERCENTER')?></option>
			<option value="Anchor" <?php echo ($selected_type == "Anchor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ANCHOR')?></option>
			<option value="Restaurant" <?php echo ($selected_type == "Restaurant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RESTAURANT')?></option>
			<option value="Service Station" <?php echo ($selected_type == "Service Station" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SERVICESTATION')?></option>
			<option value="Retail Pad" <?php echo ($selected_type == "Retail Pad" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RETAILPAD')?></option>
			<option value="Free Standing" <?php echo ($selected_type == "Free Standing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FREESTANDING')?></option>
			<option value="Day Care/Nursery" <?php echo ($selected_type == "Day Care/Nursery" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DAYCARE')?></option>
			<option value="Post Office" <?php echo ($selected_type == "Post Office" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POSTOFFICE')?>
			<option value="Vehicle"<?php echo ($selected_type == "Vehicle" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VEHICLE')?>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select  id="jform_bldgsqft"
			name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
		    <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_OCCUPANCY');?></label> <select id="jform_occupancy"
			name="jform[occupancy]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Undisclosed"><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
			<?php
			$selected_occupancy = $buyer_data[0]['needs'][0]['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Motel/Hotel
case 13:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Economy" <?php echo ($selected_type == "Economy" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ECONOMY')?>
			<option value="Full Service" <?php echo ($selected_type == "Full Service" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FULLSERVICE')?>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]"class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_ROOM_COUNT') ?></label> <select id="jform_roomcount" name="jform[roomcount]" class="reqfield">
			<?php
				$selected_roomcount = $buyer_data[0]['needs'][0]['room_count'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-9" <?php echo ($selected_roomcount == "1-9" ? "selected" : ""); ?>>1-9</option>
			<option value="10-19" <?php echo ($selected_roomcount == "10-19" ? "selected" : ""); ?>>10-19</option>
			<option value="20-29" <?php echo ($selected_roomcount == "20-29" ? "selected" : ""); ?>>20-29</option>
			<option value="30-39" <?php echo ($selected_roomcount == "30-39" ? "selected" : ""); ?>>30-39</option>
			<option value="40-49" <?php echo ($selected_roomcount == "40-49" ? "selected" : ""); ?>>40-49</option>
			<option value="50-99" <?php echo ($selected_roomcount == "50-99" ? "selected" : ""); ?>>50-99</option>
			<option value="100-149" <?php echo ($selected_roomcount == "100-149" ? "selected" : ""); ?>>100-149</option>
			<option value="150-199" <?php echo ($selected_roomcount == "150-199" ? "selected" : ""); ?>>150-199</option>
			<option value="200" <?php echo ($selected_roomcount == "200" ? "selected" : ""); ?>>200+</option>
		</select>
		<p class="jform_roomcount error_msg" style="margin-left:0px;display:none"><?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Restaurant" <?php echo ($selected_features1 == "Restaurant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RESTAURANT')?></option>
			<option value="Bar" <?php echo ($selected_features1 == "Bar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAR')?>
			<option value="Pool" <?php echo ($selected_features1 == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Banquet Room" <?php echo ($selected_features1 == "Banquet Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BANQUETROOM')?>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Restaurant" <?php echo ($selected_features2 == "Restaurant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RESTAURANT')?></option>
			<option value="Bar" <?php echo ($selected_features2 == "Bar" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BAR')?>
			<option value="Pool" <?php echo ($selected_features2 == "Pool" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POOL')?></option>
			<option value="Banquet Room" <?php echo ($selected_features2 == "Banquet Room" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BANQUETROOM')?>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Assisted Care
case 14:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Assisted" <?php echo ($selected_type == "Assisted" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ASSISTED')?>
			<option value="Acute Care" <?php echo ($selected_type == "Acute Care" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ACUTECARE')?>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_ROOM_COUNT') ?></label> <select id="jform_roomcount"
			name="jform[roomcount]" class="reqfield">
			<?php
				$selected_roomcount = $buyer_data[0]['needs'][0]['room_count'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-9" <?php echo ($selected_roomcount == "1-9" ? "selected" : ""); ?>>1-9</option>
			<option value="10-19" <?php echo ($selected_roomcount == "10-19" ? "selected" : ""); ?>>10-19</option>
			<option value="20-29" <?php echo ($selected_roomcount == "20-29" ? "selected" : ""); ?>>20-29</option>
			<option value="30-39" <?php echo ($selected_roomcount == "30-39" ? "selected" : ""); ?>>30-39</option>
			<option value="40-49" <?php echo ($selected_roomcount == "40-49" ? "selected" : ""); ?>>40-49</option>
			<option value="50-99" <?php echo ($selected_roomcount == "50-99" ? "selected" : ""); ?>>50-99</option>
			<option value="100-149" <?php echo ($selected_roomcount == "100-149" ? "selected" : ""); ?>>100-149</option>
			<option value="150-199" <?php echo ($selected_roomcount == "150-199" ? "selected" : ""); ?>>150-199</option>
			<option value="200" <?php echo ($selected_roomcount == "200" ? "selected" : ""); ?>>200+</option>
		</select>
		<p class="jform_roomcount error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_bldgsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Special Purpose
case 15:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Golf" <?php echo ($selected_type == "Golf" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_GOLF')?></option>
			<option value="Marina" <?php echo ($selected_type == "Marina" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MARINA')?>
			<option value="Theater" <?php echo ($selected_type == "Theater" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_THEATER')?>
			<option value="Religious" <?php echo ($selected_type == "Religious" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RELIGIOUS')?>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CAP');?></label> <select id="jform_cap" name="jform[cap]" class="reqfield">
			<?php
				$selected_caprate = $buyer_data[0]['needs'][0]['cap_rate'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + OFFICE
case 16:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Office" <?php echo ($selected_type == "Office" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OFFICE')?></option>
			<option value="Institutional" <?php echo ($selected_type == "Institutional" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_INSTITUTIONAL')?></option>
			<option value="Medical" <?php echo ($selected_type == "Medical" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MEDICAL')?></option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WAREHOUSE')?></option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONDO')?></option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RD')?></option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BUSINESSPARK')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CLASS') ?></label> <select id="jform_class" name="jform[class]" class="reqfield" >
			<?php
				$selected_class = $buyer_data[0]['needs'][0]['listing_class'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="4" <?php echo ($selected_class == 4 ? "selected" : ""); ?>>A</option>
			<option value="3" <?php echo ($selected_class == 3 ? "selected" : ""); ?>>B</option>
			<option value="2" <?php echo ($selected_class == 2 ? "selected" : ""); ?>>C</option>
			<option value="1" <?php echo ($selected_class == 1 ? "selected" : ""); ?>>D</option>
			<option value="Not Disclosed" <?php echo ($selected_class == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_class error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_a ?></label> <select id="jform_available" name="jform[available]" class="reqfield">
			<?php
				$selected_availablesqft = $buyer_data[0]['needs'][0]['available_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
            <option value="1-999" <?php echo ($selected_availablesqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_availablesqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_availablesqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_availablesqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_availablesqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_availablesqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_availablesqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_availablesqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_availablesqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_availablesqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_availablesqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_availablesqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_availablesqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_available error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE_LEASE') ?></label> <select id="jform_typelease" name="jform[typelease]" class="reqfield">
			<?php
				$selected_typelease = $buyer_data[0]['needs'][0]['type_lease'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NNN')?>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> ><?php echo JText::_('COM_POPS_TERMS_FSG')?>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MG')?>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MODIFIEDNET')?>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft"
			name="jform[bldgsqft]">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + INDUSTRIAL
case 17:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Flex Space" <?php echo ($selected_type == "Flex Space" ? "selected" : ""); ?>>Flex Space</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_BUSINESSPARK')?></option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_CONDO')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
			<option value="Manufacturing" <?php echo ($selected_type == "Manufacturing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MANUFACTURING')?></option>
			<option value="Office Showroom" <?php echo ($selected_type == "Office Showroom" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OFFICESHOWROOM')?></option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RD')?></option>
			<option value="Self/Mini Storage" <?php echo ($selected_type == "Self/Mini Storage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELFSTORAGE')?></option>
			<option value="Truck Terminal/Hub" <?php echo ($selected_type == "Truck Terminal/Hub" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_TRUCK')?></option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_WAREHOUSE')?></option>
			<option value="Distribution" <?php echo ($selected_type == "Distribution" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DISTRIBUTION')?></option>
			<option value="Cold Storage" <?php echo ($selected_type == "Cold Storage" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COLDSTORAGE')?></option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_LAND')?></option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE_LEASE') ?></label> <select id="jform_typelease" name="jform[typelease]" class="reqfield">
			<?php
				$selected_typelease = $buyer_data[0]['needs'][0]['type_lease'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NNN')?>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> ><?php echo JText::_('COM_POPS_TERMS_FSG')?>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MG')?>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MODIFIEDNET')?>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_a ?></label> <select id="jform_available" name="jform[available]" class="reqfield">
			<?php
				$selected_availablesqft = $buyer_data[0]['needs'][0]['available_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
            <option value="1-999" <?php echo ($selected_availablesqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_availablesqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_availablesqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_availablesqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_availablesqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_availablesqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_availablesqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_availablesqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_availablesqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_availablesqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_availablesqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_availablesqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_availablesqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_available error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft" name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft" name="jform[lotsqft]" class="reqfield">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_CEILING_HEIGHT') ?></label> <select id="jform_ceiling"
			name="jform[ceiling]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_ceiling = $buyer_data[0]['needs'][0]['ceiling_height'];
			for($i=12; $i<=34; $i=$i+2){
					if($i==$selected_ceiling) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div style="clear:both"	></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features1'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + RETAIL
case 18:
	?>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE2') ?></label> <select id="jform_type" name="jform[type]" class="reqfield">
			<?php
				$selected_type = $buyer_data[0]['needs'][0]['type'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Community Center" <?php echo ($selected_type == "Community Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_COMMUNITYCENTER')?></option>
			<option value="Strip Center" <?php echo ($selected_type == "Strip Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_STRIPCENTER')?></option>
			<option value="Outlet Center" <?php echo ($selected_type == "Outlet Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OUTLETCENTER')?></option>
			<option value="Power Center" <?php echo ($selected_type == "Power Center" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POWERCENTER')?></option>
			<option value="Anchor" <?php echo ($selected_type == "Anchor" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_ANCHOR')?></option>
			<option value="Restaurant" <?php echo ($selected_type == "Restaurant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RESTAURANT')?></option>
			<option value="Service Station" <?php echo ($selected_type == "Service Station" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SERVICESTATION')?></option>
			<option value="Retail Pad" <?php echo ($selected_type == "Retail Pad" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_RETAILPAD')?></option>
			<option value="Free Standing" <?php echo ($selected_type == "Free Standing" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_FREESTANDING')?></option>
			<option value="Day Care/Nursery" <?php echo ($selected_type == "Day Care/Nursery" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_DAYCARE')?></option>
			<option value="Post Office" <?php echo ($selected_type == "Post Office" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_POSTOFFICE')?>
			<option value="Vehicle"<?php echo ($selected_type == "Vehicle" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VEHICLE')?>
		</select>
		<p class="jform_type error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo JText::_('COM_POPS_TERMS_BLDG') ?></label> <select id="jform_bldgsqft" name="jform[bldgsqft]" class="reqfield">
			<?php
				$selected_bldgsqft = $buyer_data[0]['needs'][0]['bldg_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
            <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE_LEASE') ?></label> <select id="jform_typelease" name="jform[typelease]" class="reqfield">
			<?php
				$selected_typelease = $buyer_data[0]['needs'][0]['type_lease'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NNN')?>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> ><?php echo JText::_('COM_POPS_TERMS_FSG')?>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MG')?>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MODIFIEDNET')?>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_STORIES') ?></label> <select id="jform_stories"
			name="jform[stories]">
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<?php
			$selected_stories = $buyer_data[0]['needs'][0]['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry"><?php echo $defsqft_l ?></span></label> <select id="jform_lotsqft"
			name="jform[lotsqft]">
			<?php
				$selected_lotsqft = $buyer_data[0]['needs'][0]['lot_sqft'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
        <p class="jform_lotsqft error_msg" style="display:none"> <?php echo JText::_('COM_NRDS_FORMERROR');?> </p>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_YEAR_BUILT') ?></label> <select id="jform_yearbuilt"
			name="jform[yearbuilt]">
			<?php
				$selected_yearbuilt = $buyer_data[0]['needs'][0]['year_built'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_PARKING_RATIO') ?></label> <select id="jform_parking"
			name="jform[parking]">
			<?php
				$selected_parking = $buyer_data[0]['needs'][0]['parking_ratio'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OTHER')?></option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features1"
			name="jform[features1]">
			<?php
				$selected_features1 = $buyer_data[0]['needs'][0]['features'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></label> <select id="jform_features2"
			name="jform[features2]">
			<?php
				$selected_features2 = $buyer_data[0]['needs'][0]['features2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MIXEDUSE')?></option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SINGLETENANT')?></option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MULTIPLETENANT')?></option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_SELLERCARRY')?></option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NETLEASED')?></option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_OWNERUSER')?></option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_VACANT')?></option>
		</select>
	</div>
	<div class="left ys push-top2" style="display:none;">
		<label><?php echo JText::_('COM_POPS_TERMS_TYPE_LEASE') ?></label> <select id="jform_typelease2"
			name="jform[typelease2]">
			<?php
				$selected_typelease2 = $buyer_data[0]['needs'][0]['type_lease2'];
			?>
			<option value="">--- <?php echo JText::_('COM_POPS_TERMS_DEFAULT_SELECT') ?> ---</option>
			<option value="NNN" <?php echo ($selected_typelease2 == "NNN" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_NNN')?>
			<option value="FSG" <?php echo ($selected_typelease2 == "FSG" ? "selected" : ""); ?> ><?php echo JText::_('COM_POPS_TERMS_FSG')?>
			<option value="MG" <?php echo ($selected_typelease2 == "MG" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MG')?>
			<option value="Modified Net" <?php echo ($selected_typelease2 == "Modified Net" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_MODIFIEDNET')?>
			<option value="Not Disclosed" <?php echo ($selected_typelease2 == "Not Disclosed" ? "selected" : ""); ?>><?php echo JText::_('COM_POPS_TERMS_UNDISC')?></option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	}
	?>
</div>
<?php
die();
}
function form(){
	    $user = JFactory::getUser();	   
		$session = JFactory::getSession();
		$curr_user = $user->id ? $user->id : $_GET["user_id"];
		$session->set('user', new JUser($curr_user));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
		if(!$language_tag) $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);
		$defsqft = "Sq. Ft.";	
		$model = $this->getModel($this->getName());
		$getMeasurement = $model->getSqMeasureByCountry($_GET['country']);

		/*if(isset($_GET['country'])){
			if($_GET['country']==13){
				$defsqft = "square meters";
			} else if($_GET['country']==103){
				$defsqft = "m²";
			}
		}*/

		if($getMeasurement['sqmeasure'] == 'sq. ft.'){
			$defsqft = JText::_('COM_POPS_TERMS_BLDG');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE');
		} else {
			$defsqft = JText::_('COM_POPS_TERMS_BLDG_M');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL_M');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ_M');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT_M');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE_M');
		}
		
		$dataArray = json_decode(json_encode($_POST['data']));
		$data = isset($_POST['data']) ? $_POST['data'] : $dataArray[0];
		?>
<div
	class="pocket-form">
	<div 
		id="prop_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></h2></div>
	<div 
		id="buyer_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your buyer needs. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_MINIMUM_FEATURES') ?></h2></div>
	<?php
	switch ($_POST['stype']){

		// RESIDENTIAL PURCHASE + SFR
		case 1:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
			echo $model->getFieldsView("required reqfield");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldsCondition();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsStyle();
			echo $model->getFieldsGarage();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsFeatures("features1");
			echo $model->getFieldsFeatures("features2");
			echo $model->getFieldsFeatures("features3");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + CONDO
		case 2:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsView();
			echo $model->getFieldsBldgType();
			echo $model->getFieldsStyle();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsGarage();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsCondoFeatures("features1");
			echo $model->getFieldsCondoFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE
		case 3:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
			echo $model->getFieldsView();
			echo $model->getFieldsBldgType();
			echo $model->getFieldsStyle();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsGarage();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsCondoFeatures("features1");
			echo $model->getFieldsCondoFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + LAND
		case 4:
			echo $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
			echo $model->getFieldsView("required reqfield");
			echo $model->getFieldsZoned("required reqfield");
			echo $model->getFieldsLandFeatures("features1");
			echo $model->getFieldsLandFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + SFR
		case 5:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsTerm("required reqfield");
			echo $model->getFieldsTermPossesion("required reqfield");
			echo $model->getFieldsPet();
			echo $model->getFieldsFurnished();	
			echo '<div style="clear:both"></div>';		
			echo $model->getFieldsView();
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldsStyle();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsCondition();
			echo $model->getFieldsGarage();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsFeatures("features1");
			echo $model->getFieldsFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Condo
		case 6:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
			echo $model->getFieldsTerm("required reqfield");
			echo $model->getFieldsPet();
			echo $model->getFieldsFurnished();	
			echo $model->getFieldsView();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsTermPossesion();
			echo $model->getFieldsBldgType();
			echo $model->getFieldsStyle();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsGarage();
			echo $model->getFieldsCondoFeatures("features1");
			echo $model->getFieldsCondoFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Townhouse/Row House
		case 7:
			echo $model->getFieldsBedrooms("required reqfield");
			echo $model->getFieldsBathrooms("required reqfield");
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield");
			echo $model->getFieldsTerm("required reqfield");
			echo $model->getFieldsPet();
			echo $model->getFieldsFurnished();	
			echo $model->getFieldsView();
			echo $model->getFieldsTermPossesion();
			echo $model->getFieldsBldgType();
			echo $model->getFieldsStyle();
			echo $model->getFieldsPoolSpa();
			echo $model->getFieldsGarage();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsCondoFeatures("features1");
			echo $model->getFieldsCondoFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Land
		case 8:
			echo $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield");
			echo $model->getFieldsView("required reqfield");
			echo $model->getFieldsZoned("required reqfield");
			echo $model->getFieldsLandFeatures("features1");
			echo $model->getFieldsLandFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Multi Family
		case 9:
			echo $model->getFieldsTermUnits("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsTermGRM("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
			echo $model->getFieldsView();
			echo $model->getFieldsStyle();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldOccupancy();
			echo $model->getFieldsMultiFamFeatures("features1");
			echo $model->getFieldsMultiFamFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;	
		// COMMERCIAL + Office
		case 10:
			echo $model->getFieldOfficeType("required reqfield");
			echo $model->getFieldOfficeClass("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");			
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldsParkingRatio();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldOccupancy();
			echo $model->getFieldsOfficeFeatures("features1");
			echo $model->getFieldsOfficeFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Industrial
		case 11:
			echo $model->getFieldsIndusType("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldCeiling();
			echo $model->getFieldStories();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldOccupancy();
			echo $model->getFieldsParkingRatio();
			echo $model->getFieldsIndusFeatures("features1");
			echo $model->getFieldsIndusFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Retail
		case 12:
			echo $model->getFieldsRetailType("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldStories();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsParkingRatio();
			echo $model->getFieldOccupancy();
			echo $model->getFieldsIndusFeatures("features1");
			echo $model->getFieldsIndusFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Motel/Hotel
		case 13:
			echo $model->getFieldsHotelType("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsRoomCount("required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldStories();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsHotelFeatures("features1");
			echo $model->getFieldsHotelFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Assisted Care
		case 14:
			echo $model->getFieldsAssistType("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo $model->getFieldsRoomCount("required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldStories();
			echo $model->getFieldsYearBuilt();
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Special Purpose
		case 15:
			echo $model->getFieldsSpecialType("required reqfield");
			echo $model->getFieldsTermCAP("required reqfield");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + OFFICE
		case 16:
			echo $model->getFieldOfficeType("required reqfield");
			echo $model->getFieldOfficeClass("required reqfield");
			echo $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsOfficeTypeLease("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsParkingRatio();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsOfficeFeatures("features1");
			echo $model->getFieldsOfficeFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + INDUSTRIAL
		case 17:
			echo $model->getFieldsIndusType("required reqfield");
			echo $model->getFieldsOfficeTypeLease("required reqfield");
			echo $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield");
			echo $model->getFieldCeiling();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldStories();
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsParkingRatio();
			echo $model->getFieldsIndusFeatures("features1");
			echo $model->getFieldsIndusFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + RETAIL
		case 18:
			echo $model->getFieldsRetailType("required reqfield");
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield");
			echo $model->getFieldsOfficeTypeLease("required reqfield");
			echo '<div style="clear:both"></div>';
			echo $model->getFieldStories();
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l);
			echo $model->getFieldsYearBuilt();
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsParkingRatio();
			echo $model->getFieldsOfficeFeatures("features1");
			echo $model->getFieldsOfficeFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
	}
	?>
</div>
<?php
die();
	}
	function get_listing(){
		$model = $this->getModel($this->getName());
		//var_dump($_REQUEST);
		extract($_REQUEST);	
		$results = $model->get_listing(array('p.user_id'=>$userid, 'p.property_type'=>$type, 'p.bedroom'=>$bedroom, 'p.bathroom'=>$bathroom, 'pp.price'=>$price));
		//print_r(array('p.user_id'=>$userid, 'p.property_type'=>$type, 'p.bedroom'=>$bedroom, 'p.bathroom'=>$bathroom, 'pp.price'=>$price));
		echo json_encode($results);
		die();
	}
	function get_images() {
		// $uid = $_POST['userid'];
		$model = $this->getModel($this->getName());
		$result = $model->get_buyer_image($_POST[property_type], $_POST[sub_type]);
		echo json_encode( $result );
		die();
	}
	function check_saved_pops(){
		$model = $this->getModel($this->getName());
		$listing_id = JRequest::getVar('pida');
		$buyer_id 	= JRequest::getVar('buyer_arr');
		$active 	= JRequest::getVar('current_status');
		echo $model->check_existing_pops($listing_id, $buyer_id, $active);
		die();
	}
	function stripImageName($src) {
		 $url = urldecode($src);
		 $image_name = (stristr($url,'?',true))?stristr($url,'?',true):$url;
		 $pos = strrpos($image_name,'/');
		 $image_name = substr($image_name,$pos+1);
		 $extension = stristr($image_name,'.');
		 return $image_name;
		}
	function sendRemindertoSign() {
		$model = $this->getModel($this->getName());
		$referrals = $model->getReferralsSigned();
		foreach($referrals as $ref){
				$refr2 = $model->Checkr2Sign($ref->activity_id);
				$datediff = strtotime(date('m/d/Y h:i:s a', time()))- strtotime($ref->date);
				$closed = $model->CheckIfClosed($ref->activity_id);
				$userr1 = $model->getR1Details($ref->activity_id);
				//language loading
				  	$language = JFactory::getLanguage();
			      	$extension = 'com_nrds';
			      	$base_dir = JPATH_SITE;
			      	$language_tag = $userr1[0]->currLanguage;
			      	$language->load($extension, $base_dir, $language_tag, true);
			    //language loading	
				if($userr1[0]->gender == "Female"){
					$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($userr1[0]->lastname);
				} else {
					$salut = JText::_('COM_USERACTIV_HEADER')." ".JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($userr1[0]->lastname);
				}
				$userr2 = $model->getR2Details($ref->activity_id);
				if ($datediff>48*3600 && empty($refr2) && empty($closed)) {
				$subject= JText::_('COM_REFERRALS_SIGN_R1');
				$userprofile = urlencode(base64_encode("index.php?option=com_userprofile&task=profile&uid=".$userr2[0]->id));
				$myreferrals = urlencode(base64_encode("index.php?option=com_propertylisting&task=referrals"));
				$root_url=JURI::base();
				$r2 ="<a style='text-decoration:none' href='".$root_url."index.php?returnurl=".$userprofile."'>".$userr2[0]->name."</a>";
				$refpage = "<a style='text-decoration:none' href='".$root_url."index.php?returnurl=".$myreferrals."'>AgentBridge</a>";
				$body = "";
				$body .= JText::sprintf('COM_REFERRALS_SIGN_R1_P1', $salut, $userr1[0]->client, $r2, $refpage ) ;
				$sender = array(  'support@agentbridge.com' , 'AgentBridge');
				$mailSender =& JFactory::getMailer();
				$mailSender ->addRecipient( $userr1->email );
				if($userr1[0]->cc_all && $userr1[0]->cc_all==1){
					$mailSender ->addCC( $userr1[0]->a_email );
				}
				$cc = "rroque@agentbridge.com";
				$mailSender ->addCC( $cc );
				$mailSender ->setSender( $sender );
				$mailSender ->setSubject( $subject );
				$mailSender ->isHTML(  true );
				$mailSender ->setBody(  $body );
				$mailSender ->Encoding = 'base64';
				$mailSender ->wordWrap = 50;
				if ($mailSender ->Send()) {
						echo json_encode("success");
					} else {
						echo json_encode("fail");
					}
				}
			}
		}
	public function getTimeZone($zone_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT ct.timezone FROM #__country_timezones ct WHERE ct.zone_id = " . $zone_id);
		$db->setQuery($query);		
		return $db->loadObject();
	}

	public function android_send_notification($token,$title,$message,$topic){


		$url = 'https://fcm.googleapis.com/fcm/send';

		if(!is_array($token)){
			$token = array($token);
		} else{
			$token = array_unique($token);
		}

		$fields = array(
			// "to" => "/topics/".$topic,
			 'registration_ids' => $token,
			 'data' =>  array("title" => $title,"message" => $message)
			);

		$headers = array(
			'Authorization:key = AIzaSyB-6XSF_wghYrGTL5NlKk3MPBwHgrmpujI ',
			'Content-Type: application/json'
			);

	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);

        $mailSender =& JFactory::getMailer();
        $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
        $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
        $mailSender ->setSubject( "Sent Notif Firebase" );
        $mailSender ->isHTML(  true );
        $mailSender ->setBody( print_r($result,true)."-----".print_r($token,true));
        $mailSender ->Send(); 

	}
	
	public function getUserIdByEmail() {
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		
		$result = $model2->getUserIdByEmail($_POST['email']);
		
		$json = json_encode($result[0]);
		
		echo $json;
		
		exit;
		
	}
	
	
	public function referral_save_data() {
		$_SESSION['referral_data'] = $_POST;
		
		exit;
	}
}
?>