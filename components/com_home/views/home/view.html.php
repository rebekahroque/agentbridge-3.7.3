<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class HomeViewHome extends JViewLegacy
{
    function display($tpl = null)
    {
        $greeting = "Home Page for Agentbridge Users!";
        $this->assignRef( 'greeting', $greeting );
 
        parent::display($tpl);
    }
}

?>