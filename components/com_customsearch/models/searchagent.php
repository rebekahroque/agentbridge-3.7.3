<?php
/**
 * @version     1.0.0
 * @package     com_customsearch
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com 
 */

// No direct access.
defined('_JEXEC') or die;
jimport('joomla.database.database.mysql' );
jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

JLoader::import('joomla.application.component.model');
// Import MylibraryModelBooks's books model from com_mylibrary,
// located in administrator section of the website.
// if you want access a model from public section, then remove JPATH_ADMINISTRATOR . DS .
JLoader::import( 'userprofile', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_userprofile' . DS . 'models' );

/**
 * Customsearch model.
 */
class CustomsearchModelSearchagent extends JModelForm
{
    
    var $_item = null;
    
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_customsearch');

		// Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit') {
            $id = JFactory::getApplication()->getUserState('com_customsearch.edit.searchagent.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_customsearch.edit.searchagent.id', $id);
        }
		$this->setState('searchagent.id', $id);

		// Load the parameters.
		$params = $app->getParams();
        $params_array = $params->toArray();
        if(isset($params_array['item_id'])){
            $this->setState('searchagent.id', $params_array['item_id']);
        }
		$this->setState('params', $params);

	}
        

	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id)) {
				$id = $this->getState('searchagent.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published) {
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			} elseif ($error = $table->getError()) {
				$this->setError($error);
			}
		}

		return $this->_item;
	}
    
	public function getTable($type = 'Searchagent', $prefix = 'CustomsearchTable', $config = array())
	{   
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        return JTable::getInstance($type, $prefix, $config);
	}     

    
	/**
	 * Method to check in an item.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int)$this->getState('searchagent.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
            if (method_exists($table, 'checkin')) {
                if (!$table->checkin($id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param	integer		The id of the row to check out.
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int)$this->getState('searchagent.id');

		if ($id) {
            
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
            if (method_exists($table, 'checkout')) {
                if (!$table->checkout($user->get('id'), $id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
		}

		return true;
	}    
    
	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML 
     * 
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_customsearch.searchagent', 'searchagent', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		$data = $this->getData(); 
        
        return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function save($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('searchagent.id');
        $state = (!empty($data['state'])) ? 1 : 0;
        $user = JFactory::getUser();

        if($id) {
            //Check the user can edit this item
            $authorised = $user->authorise('core.edit', 'com_customsearch') || $authorised = $user->authorise('core.edit.own', 'com_customsearch');
            if($user->authorise('core.edit.state', 'com_customsearch') !== true && $state == 1){ //The user cannot edit the state of the item.
                $data['state'] = 0;
            }
        } else {
            //Check the user can create new items in this section
            $authorised = $user->authorise('core.create', 'com_customsearch');
            if($user->authorise('core.edit.state', 'com_customsearch') !== true && $state == 1){ //The user cannot edit the state of the item.
                $data['state'] = 0;
            }
        }

        if ($authorised !== true) {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }
        
        $table = $this->getTable();
        if ($table->save($data) === true) {
            return $id;
        } else {
            return false;
        }
        
	}
    
     function delete($data)
    {
        $id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('searchagent.id');
        if(JFactory::getUser()->authorise('core.delete', 'com_customsearch') !== true){
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }
        $table = $this->getTable();
        if ($table->delete($data['id']) === true) {
            return $id;
        } else {
            return false;
        }
        
        return true;
    }
    
    function getCategoryName($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query 
            ->select('title')
            ->from('#__categories')
            ->where('id = ' . $id);
        $db->setQuery($query);
        return $db->loadObject();
    }
    
    function searchDb($filters = null, $refine = false, $refinements = null, $page = 1, $limitUpper = 0){
		
		$user =& JFactory::getUser($_SESSION['user_id']);

		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

		JModelLegacy::addIncludePath( $userprofilemodel );

		$model3 =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
	
		$offset = (($page-1)*$limitUpper);
		
		
    
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$sql = 	' 
    			select	tu.id
				  from	tbl_users tu
		 	  
			 LEFT JOIN 	tbl_user_registration tur
					on	tur.email = tu.email
			 LEFT JOIN 	tbl_user_sales tus
					on	tus.agent_id = tur.user_id
			 LEFT JOIN 	tbl_broker tb
					on	tb.broker_id = tur.brokerage
			 LEFT JOIN 	tbl_zones tz
					on	tz.zone_id = tur.state 
			 LEFT JOIN 	tbl_user_usergroup_map ugmp
					on	ugmp.user_id = tu.id 
			 LEFT JOIN  tbl_countries as ct 
			 		ON  ct.countries_id = tur.country
			 LEFT JOIN 	tbl_user_zips_workaround uz
					on	uz.user_id = tur.user_id
			 LEFT JOIN ( SELECT 
						 GROUP_CONCAT(DISTINCT  bn.zip) AS bnzipslist,b.agent_id FROM tbl_buyer_needs bn 
						 LEFT JOIN  tbl_buyer b	on	b.buyer_id = bn.buyer_id
						 GROUP BY b.agent_id) as buyerZipLists ON buyerZipLists.agent_id = tu.id
			 LEFT JOIN ( SELECT 
						GROUP_CONCAT(DISTINCT pl.zip) AS plzipslist,pl.user_id FROM tbl_pocket_listing pl 
						 GROUP BY pl.user_id) as popLists ON popLists.user_id = tu.id
			 LEFT JOIN 	tbl_country_sp_address csp
					on	(tur.zip LIKE CONCAT(csp.postcode, "%") OR 
						buyerZipLists.bnzipslist LIKE CONCAT(csp.postcode, "%") OR 
						popLists.plzipslist LIKE CONCAT(csp.postcode, "%"))

					 ';

    	$filters = array_filter($filters);



    	$user_grp = $model3->get_user_group($user->id)->group_id;
 

    	if(count($filters)){
    		$where = "where ugmp.group_id!=12 and ugmp.group_id!=11 and ugmp.group_id!=8 and ugmp.group_id!=7  AND (";
    		foreach($filters as $key => $filter){
	    		$where.=" tu.name LIKE '%".$filter."%' OR";
	    		$where.=" tur.street_address LIKE '%".$filter."%' OR";
	    		$where.=" ct.countries_iso_code_3 LIKE '%".$filter."%' OR";
	    		$where.=" ct.countries_name LIKE '%".$filter."%' OR";
	    		$where.=" tur.suburb LIKE '%".$filter."%' OR";
	    		$where.=" tur.city LIKE '%".$filter."%' OR";
	    		$where.=" tur.zip like '%".$filter."%' OR";
	    		$where.=" buyerZipLists.bnzipslist like '%".$filter."%' OR";
	    		$where.=" popLists.plzipslist like '%".$filter."%' OR";
	    		$where.=" uz.zip_workaround like '%".$filter."%' OR";
	    		$where.=" csp.county LIKE '%".$filter."%' OR";	    		
	    		$where.=" csp.address1 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address2 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address3 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address4 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address5 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address6 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address7 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address8 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address9 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address10 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address11 LIKE '%".$filter."%' OR";
	    		$where.=" csp.address12 LIKE '%".$filter."%' OR";
	    		//$where.=" broker_name LIKE '%".$filter."%' OR";
	    		$where.=" tz.zone_code LIKE '%".$filter."%' ";
	    		if(!($key == count($filters) -1))
	    			$where.=" OR ";
	    	}

	    	foreach ($filters as $key => $filter) {
	    		# code...
	    		$db = JFactory::getDbo();
		    	$query3 = $db->getQuery(true);
		    	$query3->select('postcode')
		    	->from('#__country_sp_address csp')
		    	->where("csp.address1 LIKE '%".$filter."%' OR 
		    		     csp.address2 LIKE '%".$filter."%' OR 
		    		     csp.address3 LIKE '%".$filter."%' OR
		    		     csp.address4 LIKE '%".$filter."%' OR
		    		     csp.address5 LIKE '%".$filter."%' OR
		    		     csp.address6 LIKE '%".$filter."%' OR
		    		     csp.address7 LIKE '%".$filter."%' OR
		    		     csp.address8 LIKE '%".$filter."%' OR
		    		     csp.address9 LIKE '%".$filter."%' OR
		    		     csp.address10 LIKE '%".$filter."%' OR
		    		     csp.address11 LIKE '%".$filter."%' OR 
		    		     csp.address12 LIKE '%".$filter."%' ");
		    	$wz_postcodes_arr = $db->setQuery($query3)->loadObjectList();
	    	}

    	if(count($wz_postcodes_arr)){
	    		foreach ($wz_postcodes_arr as $key => $value) {
	    			$where.=" OR tur.zip like '%".$value->postcode."%' OR";
		    		$where.=" buyerZipLists.bnzipslist like '%".$value->postcode."%' OR";
		    		$where.=" popLists.plzipslist like '%".$value->postcode."%' ";
		    	}
	    	}
	    	$where.=")";
    	} else {    		
	    	if($user_grp==8){
	    		$where="";
	    	} else {
	    		$where = "where tu.id=0";
	    	}
    	}
    	


    	//$where = (count($filters)) ? "where " : $where_no ;

    		
		//die( $where );
		
    	//var_dump($sql.$where." GROUP BY tu.id");
    	
    	$users = $db->setQuery($sql.$where." AND tur.user_type != 86 GROUP BY tu.id")->loadObjectList();


    //	var_dump($users);
    	
    	$where = "in ";
    	$userlist = array_map(function($obj){ return $obj->id; }, $users);


		$db = JFactory::getDbo();
		    	$query3 = $db->getQuery(true);
		    	$query3->select('country,postcode')
		    	->from('#__country_sp_address csp')
		    	->where("csp.address1 LIKE '%".$filter."%' OR 
		    		     csp.address2 LIKE '%".$filter."%' OR 
		    		     csp.address3 LIKE '%".$filter."%' OR
		    		     csp.address4 LIKE '%".$filter."%' OR
		    		     csp.address5 LIKE '%".$filter."%' OR
		    		     csp.address6 LIKE '%".$filter."%' OR
		    		     csp.address7 LIKE '%".$filter."%' OR
		    		     csp.address8 LIKE '%".$filter."%' OR
		    		     csp.address9 LIKE '%".$filter."%' OR
		    		     csp.address10 LIKE '%".$filter."%' OR
		    		     csp.address11 LIKE '%".$filter."%' OR 
		    		     csp.address12 LIKE '%".$filter."%' ");
		    	$wz_postcodes_arr = $db->setQuery($query3)->loadObjectList();


		  $country_cond ="";

    	if(count($wz_postcodes_arr)){
    		
    	foreach ($wz_postcodes_arr as $key => $value) {
    		# code...
    		
    		//var_dump($value->postcode);
	    	$query4 = $db->getQuery(true);
	    	$query4->select('u.id')
	    	->from('#__user_zips_workaround uz')
	    	->leftJoin('#__user_registration ur ON ur.user_id = uz.user_id')
	    	->leftJoin('#__users u ON u.email = ur.email')
	    	->where("FIND_WILD_IN_SET('".$value->postcode."%',uz.zip_workaround)");

	    	$user_id_include = $db->setQuery($query4)->loadObjectList();

	    	foreach ($user_id_include as $key2 => $value2) {
	    		# code...
	    		array_push($userlist, $value2->id); 
	    	}

	    	
    	}

    	#$country_cond = " AND ur.country = ".$country_i;
		#$country_cond = "";

       }

    	$where.='("'.implode('","', $userlist).'")';

    	//var_dump($where);
    	
    	$conditions = array();
    	$temp = array();
    	if(count($refinements))
		foreach ($refinements as $key => $refinement){
    		$temp = array();
    		foreach($refinement as $value){
    			if($key=='newtblang.lang_result'){
    				$temp[] = $key." like '%".($db->escape($value))."%'";
    			} else if($key=='zip'){
    				$temp[] = "ur.zip like '%".($db->escape($value))."%'";
    				$temp[] = "buyerZipLists.bnzipslist like '%".($db->escape($value))."%'";
    				$temp[] = "popLists.plzipslist like '%".($db->escape($value))."%'";
    				$temp[] = "uz.zip_workaround like '%".($db->escape($value))."%'";
    				
	    		} else if ($key=='countries_name'){
	    			$temp[] = "country.countries_iso_code_3 LIKE '%".($db->escape($value))."%'";
	    			$temp[] = "country.countries_name LIKE '%".($db->escape($value))."%'";
    				
    			} else if ($key=='city'){
	    			$temp[] = "ur.city LIKE '%".($db->escape($value))."%'";
	    			///$temp[] = "country.city LIKE '%".($db->escape($value))."%'";
    				
    			}else
					$temp[] = $key." = '".($db->escape($value))."'";
    		}
    		$conditions[] = implode(" OR ", $temp);
	
    	}
    	if($refine && !empty($refinements))
    		$where.=" AND ( ".implode(" ) AND ( ", $conditions)." ) ";


    	/*$query = $db->getQuery(true);
    	$query->select(' STRAIGHT_JOIN 
			*,
			wn.value as work,
			mn.value as mobile, 
			mn.show as mobile_status, 
			newtblang.lang_result as user_language,
			designations.designation_list as user_designation,
			((IFNULL(buyerActive.activeBUYcount,0))+(IFNULL(popActive.activePOPcount,0))) AS popbcounts
		')
    	->from('#__users u')
    	->leftJoin('#__user_registration ur on u.email = ur.email')
		->where('id '.$where.' AND ur.is_term_accepted=1 AND ugmp.group_id!=8')
		->leftJoin('#__user_sales us ON us.agent_id = ur.user_id')
		->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id')
		->leftJoin('#__user_mobile_numbers mn ON mn.user_id = ur.user_id')
		->leftJoin('#__user_work_numbers wn ON wn.user_id = ur.user_id')
    	->leftJoin('#__zones as zn ON ur.state = zn.zone_id')
		->leftJoin('#__broker as br ON ur.brokerage = br.broker_id')
		->leftJoin('#__countries as country ON country.countries_id = zn.zone_country_id')
		->leftJoin('(
			SELECT STRAIGHT_JOIN
				ls.user_id, 
				GROUP_CONCAT(lang.language) as lang_result,
				lang.language as  one_language
			FROM 
				#__user_lang_spoken ls
			LEFT JOIN
				#__user_languages lang ON lang.lang_id = ls.lang_id
			GROUP BY 
				ls.user_id) as newtblang  
			ON newtblang.user_id = u.id'
			
		)
		->leftJoin('
			(SELECT STRAIGHT_JOIN
				uds.user_id,
				GROUP_CONCAT(ds.designations) as designation_list
			FROM
				#__user_designations uds
			LEFT JOIN
				#__designations ds ON ds.id = uds.desig_id
			GROUP BY
				uds.user_id) as designations 
			ON
				designations.user_id = u.id
		')		
		->leftJoin('( 
			SELECT STRAIGHT_JOIN
				COUNT(pl.listing_id) AS activePOPcount, pl.user_id
			FROM 
				#__pocket_listing pl
			WHERE pl.sold != 1 AND pl.closed != 1
			GROUP BY
				pl.user_id) as popActive  
			ON popActive.user_id = u.id'
		)
		->leftJoin('( 
			SELECT STRAIGHT_JOIN
				COUNT(bu.buyer_id) AS activeBUYcount, bu.agent_id
			FROM 
				#__buyer bu
			WHERE bu.buyer_type != "Inactive" AND bu.buyer_type != ""
			GROUP BY
				bu.agent_id) as buyerActive  
			ON buyerActive.agent_id = u.id'
		)
		->group('u.id')
		->order("popbcounts DESC");*/

		//die( $query );


		$sql2 = 'select 
		  buyerZipLists.bnzipslist,popLists.plzipslist,u.*,ur.*,us.*,ugmp.*,country_curr.*,country.*,
		 wn.value as work, 
		 mn.value as mobile,
		 mn.show as mobile_status, 
		 newtblang.lang_result as user_language, 
		 zn.zone_name as user_zone_name, zn.zone_code as user_zone_code,
		 conva.countryCode,
		 ((IFNULL(buyerActive.activeBUYcount,0))+(IFNULL(popActive.activePOPcount,0))) AS popbcounts 
		 FROM tbl_users u 
		 LEFT JOIN tbl_user_registration ur on u.email = ur.email 
		 LEFT JOIN tbl_user_zips_workaround uz on uz.user_id = ur.user_id 
		 LEFT JOIN tbl_user_sales us ON us.agent_id = ur.user_id 
		 LEFT JOIN tbl_user_usergroup_map ugmp ON ugmp.user_id = u.id 
		 LEFT JOIN tbl_user_mobile_numbers mn ON mn.user_id = ur.user_id 
		 LEFT JOIN tbl_user_work_numbers wn ON wn.user_id = ur.user_id 
		 LEFT JOIN tbl_zones as zn ON ur.state = zn.zone_id 
		 LEFT JOIN tbl_broker as br ON ur.brokerage = br.broker_id 
		 LEFT JOIN tbl_countries as country ON country.countries_id = zn.zone_country_id
		 LEFT JOIN tbl_country_validations as conva ON conva.country = ur.country
		 LEFT JOIN tbl_country_currency as country_curr ON country_curr.country = ur.country
		 LEFT JOIN ( SELECT  
		 				 ls.user_id,
		 				 GROUP_CONCAT(lang.language) as lang_result, 
		 				 lang.language as one_language 
		 				 FROM tbl_user_lang_spoken ls 
		 				 LEFT JOIN tbl_user_languages lang ON lang.lang_id = ls.lang_id 
		 				 GROUP BY ls.user_id) as newtblang ON newtblang.user_id = u.id 
		 LEFT JOIN ( SELECT 
						COUNT(pl.listing_id) AS activePOPcount, 
						pl.user_id FROM tbl_pocket_listing pl 
						WHERE pl.sold != 1 AND pl.closed != 1 GROUP BY pl.user_id) as popActive ON popActive.user_id = u.id 
		 LEFT JOIN ( SELECT 
						GROUP_CONCAT(DISTINCT pl.zip) AS plzipslist,pl.user_id FROM tbl_pocket_listing pl 
						 GROUP BY pl.user_id) as popLists ON popLists.user_id = u.id
		 LEFT JOIN ( SELECT 
						 GROUP_CONCAT(DISTINCT  bn.zip) AS bnzipslist,b.agent_id FROM tbl_buyer_needs bn 
						 LEFT JOIN  tbl_buyer b	on	b.buyer_id = bn.buyer_id
						 GROUP BY b.agent_id) as buyerZipLists ON buyerZipLists.agent_id = u.id
		 LEFT JOIN ( SELECT  
						COUNT(bu.buyer_id) AS activeBUYcount, 
						bu.agent_id FROM tbl_buyer bu WHERE bu.buyer_type != "Inactive" AND bu.buyer_type != "" GROUP BY bu.agent_id) as buyerActive ON buyerActive.agent_id = u.id';  

		$where2 = ' WHERE u.id '.$where.' '.$country_cond.' AND ur.is_term_accepted=1 AND ugmp.group_id!=12 AND ugmp.group_id!=11 AND ugmp.group_id!=8 AND ugmp.group_id!=7  GROUP BY u.id ORDER BY 
													CASE 
													WHEN popLists.plzipslist REGEXP "^'.$filter.'[a-z]{1,}" THEN 1
													WHEN FIND_IN_SET("'.$filter.'",uz.zip_workaround) THEN 2
													WHEN FIND_WILD_IN_SET("'.$filter.'%",uz.zip_workaround) THEN 3													
													WHEN popLists.plzipslist REGEXP "[[.,.]]'.$filter.'[a-z]{1,}" THEN 5
													WHEN FIND_IN_SET("'.$filter.'",buyerZipLists.bnzipslist) THEN 6
													WHEN buyerZipLists.bnzipslist REGEXP "^'.$filter.'[a-z]{1,}" THEN 5													
													WHEN buyerZipLists.bnzipslist REGEXP "[[.,.]]'.$filter.'[a-z]{1,}" THEN 5 
													WHEN popLists.plzipslist REGEXP "[[.,.]]'.$filter.'[0-9]{1,}" THEN 6 
													ELSE 6
													END,popbcounts DESC';
		$limit = ($limitUpper) ? " LIMIT $offset, $limitUpper" : "";

		//die( $sql2.$where2 );
		
		$usersl = $db->setQuery($sql2.$where2.$limit)->loadObjectList();

    	foreach ($usersl as $user){
    		$user->name = stripslashes_all($user->name);
    		$user->firstname = stripslashes_all($user->firstname);
    		$user->lastname = stripslashes_all($user->lastname);
    	//	var_dump($user->name."-'".$user->bnzipslist."'<br>");
    	//	var_dump($user->name."-'".$user->plzipslist."'<br>");
			$user->lang_result = explode(",", $user->lang_result);
			//print_r ($user->lang_result);

			/*foreach ($user->lang_result as $lr) {
				print_r ($lr);
			}
			if($user->country){
				$user->currency = $this->getCountryCurrencyData($user->country)[0]->currency;	
			} else{
				$user->currency = $this->getCountryCurrencyData("223")[0]->currency;
			}*/
			
			$arr_user_ids[] = $user->id;
			$arr_emails[] = $user->email;
			$user->designation = $this->get_designations($user->id);
			$user->languages = $this->get_lang($user->id);
    		$user->brokerage = $this->get_brokerage($user->email);
    		//$user->image = $this->get_image($user->email);
    		#$user->info = $this->get_user_registration($user->email);
    	}



    	return array("users" => $usersl, "user_ids" => $arr_user_ids, "emails" => $arr_emails);

    }

    function getCountryCurrencyData($cID){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->setQuery("
				SELECT cur.* FROM #__country_currency AS cur WHERE country=".$cID);
			$db->setQuery($query);		
			return $db->loadObjectList();
	}
    
    function get_user_registration($email){
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->select('*')->from('#__user_registration ur')->leftJoin('#__zones z on ur.state=z.zone_id')->leftJoin('#__countries c on z.zone_country_id=c.countries_id')-$query->leftJoin('#__country_validations conva ON conva.country = ur.country')->where('email =\''.$email.'\'');
    	return $db->setQuery($query)->loadObject();
    }
    
    function get_lang($uid){
    	$sql=	"
		                SELECT	language
						  FROM	tbl_user_languages tul
					STRAIGHT_JOIN	tbl_user_lang_spoken tuls
							ON	tuls.lang_id = tul.lang_id
						 WHERE	tuls.user_id = ".$uid;
    	
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->setQuery($sql);
    	$db->setQuery($query);
    	return $db->loadObjectList();
    }
    
    function get_designations($uid){
    	$sql=	"
    					SELECT designations
    					  from tbl_user_designations tud
    				 STRAIGHT_JOIN tbl_designations td
    				 		on tud.desig_id=td.id
    				 	 where user_id = ".$uid;
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->setQuery($sql);
    	$db->setQuery($query);
    	return $db->loadObjectList();
    }
    
    function get_brokerage($email){
    	$sql=	"select broker_name
    			   from tbl_user_registration tur
    		  STRAIGHT_JOIN tbl_broker tb
    				 on tur.brokerage = tb.broker_id
    			  where email = '".$email."'";
    	 
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->setQuery($sql);
    	$db->setQuery($query);
    	return $db->loadObject()->broker_name;
    }
    
    function get_image($email){
    	$sql=	"select image
    			   from tbl_user_registration
    			  where email = '".$email."'";
    	
    	$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query->setQuery($sql);
    	$db->setQuery($query);
    	return $db->loadObject()->image;
    }
    
}
