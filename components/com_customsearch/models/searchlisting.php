<?php

/**

 * @version     1.0.0

 * @package     com_customsearch

 * @copyright   Copyright (C) 2013. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com 

 */

//error_reporting(E_ALL);

// No direct access.

defined('_JEXEC') or die;



jimport('joomla.application.component.modelform');

jimport('joomla.event.dispatcher');



/**

 * Customsearch model.

*/

class CustomsearchModelSearchlisting extends JModelForm

{



	var $_item = null;



	/**

	 * Method to auto-populate the model state.

	 *

	 * Note. Calling getState in this method will result in recursion.

	 *

	 * @since	1.6

	 */

	protected function populateState()

	{

		$app = JFactory::getApplication('com_customsearch');



		// Load state from the request userState on edit or from the passed variable on default

		if (JFactory::getApplication()->input->get('layout') == 'edit') {

			$id = JFactory::getApplication()->getUserState('com_customsearch.edit.searchlisting.id');

		} else {

			$id = JFactory::getApplication()->input->get('id');

			JFactory::getApplication()->setUserState('com_customsearch.edit.searchlisting.id', $id);

		}

		$this->setState('searchlisting.id', $id);



		// Load the parameters.

		$params = $app->getParams();

		$params_array = $params->toArray();

		if(isset($params_array['item_id'])){

			$this->setState('searchlisting.id', $params_array['item_id']);

		}

		$this->setState('params', $params);



	}



	/**

	 * Method to get an ojbect.

	 *

	 * @param	integer	The id of the object to get.

	 *

	 * @return	mixed	Object on success, false on failure.

	 */

	public function &getData($id = null)

	{

		if ($this->_item === null)

		{

			$this->_item = false;



			if (empty($id)) {

				$id = $this->getState('searchlisting.id');

			}



			// Get a level row instance.

			$table = $this->getTable();



			// Attempt to load the row.

			if ($table->load($id))

			{

				// Check published state.

				if ($published = $this->getState('filter.published'))

				{

					if ($table->state != $published) {

						return $this->_item;

					}

				}



				// Convert the JTable to a clean JObject.

				$properties = $table->getProperties(1);

				$this->_item = JArrayHelper::toObject($properties, 'JObject');

			} elseif ($error = $table->getError()) {

				$this->setError($error);

			}

		}



		return $this->_item;

	}



	public function getTable($type = 'Searchlisting', $prefix = 'CustomsearchTable', $config = array())

	{

		$this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');

		return JTable::getInstance($type, $prefix, $config);

	}





	/**

	 * Method to check in an item.

	 *

	 * @param	integer		The id of the row to check out.

	 * @return	boolean		True on success, false on failure.

	 * @since	1.6

	 */

	public function checkin($id = null)

	{

		// Get the id.

		$id = (!empty($id)) ? $id : (int)$this->getState('searchlisting.id');



		if ($id) {



			// Initialise the table

			$table = $this->getTable();



			// Attempt to check the row in.

			if (method_exists($table, 'checkin')) {

				if (!$table->checkin($id)) {

					$this->setError($table->getError());

					return false;

				}

			}

		}



		return true;

	}



	/**

	 * Method to check out an item for editing.

	 *

	 * @param	integer		The id of the row to check out.

	 * @return	boolean		True on success, false on failure.

	 * @since	1.6

	 */

	public function checkout($id = null)

	{

		// Get the user id.

		$id = (!empty($id)) ? $id : (int)$this->getState('searchlisting.id');



		if ($id) {



			// Initialise the table

			$table = $this->getTable();



			// Get the current user object.

			$user = JFactory::getUser();



			// Attempt to check the row out.

			if (method_exists($table, 'checkout')) {

				if (!$table->checkout($user->get('id'), $id)) {

					$this->setError($table->getError());

					return false;

				}

			}

		}



		return true;

	}



	/**

	 * Method to get the profile form.

	 *

	 * The base form is loaded from XML

	 *

	 * @param	array	$data		An optional array of data for the form to interogate.

	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.

	 * @return	JForm	A JForm object on success, false on failure

	 * @since	1.6

	 */

	public function getForm($data = array(), $loadData = true)

	{

		// Get the form.

		$form = $this->loadForm('com_customsearch.searchlisting', 'searchlisting', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {

			return false;

		}



		return $form;

	}



	/**

	 * Method to get the data that should be injected in the form.

	 *

	 * @return	mixed	The data for the form.

	 * @since	1.6

	 */

	protected function loadFormData()

	{

		$data = $this->getData();



		return $data;

	}



	/**

	 * Method to save the form data.

	 *

	 * @param	array		The form data.

	 * @return	mixed		The user id on success, false on failure.

	 * @since	1.6

	 */

	public function save($data)

	{

		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('searchlisting.id');

		$state = (!empty($data['state'])) ? 1 : 0;

		$user = JFactory::getUser();



		if($id) {

			//Check the user can edit this item

			$authorised = $user->authorise('core.edit', 'com_customsearch') || $authorised = $user->authorise('core.edit.own', 'com_customsearch');

			if($user->authorise('core.edit.state', 'com_customsearch') !== true && $state == 1){ //The user cannot edit the state of the item.

				$data['state'] = 0;

			}

		} else {

			//Check the user can create new items in this section

			$authorised = $user->authorise('core.create', 'com_customsearch');

			if($user->authorise('core.edit.state', 'com_customsearch') !== true && $state == 1){ //The user cannot edit the state of the item.

				$data['state'] = 0;

			}

		}



		if ($authorised !== true) {

			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

			return false;

		}



		$table = $this->getTable();

		if ($table->save($data) === true) {

			return $id;

		} else {

			return false;

		}



	}



	function delete($data)

	{

		$id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('searchlisting.id');

		if(JFactory::getUser()->authorise('core.delete', 'com_customsearch') !== true){

			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

			return false;

		}

		$table = $this->getTable();

		if ($table->delete($data['id']) === true) {

			return $id;

		} else {

			return false;

		}



		return true;

	}



	function getCategoryName($id){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query

		->select('title')

		->from('#__categories')

		->where('id = ' . $id);

		$db->setQuery($query);

		return $db->loadObject();

	}



	/*custom search*/



	public function searchDB($searchval, $oldval, $refine = false, $refinement = null, $order, $limitUpper = 0){



		$searchval = trim($searchval);

		#$oldval = ($refine) ? 1 : $oldval;		
		
		$columns = array();


		$columns['pt.type_name']['regex']='/(residential)|(commercial)|(residential purchase)|(residential lease)|(commercial purchase)|(commercial lease)|(lease)/';

		//$columns['pt.type_name']['regex']='~\b(residential|commercial|residential purchase|residential lease|commercial purchase|commercial lease|lease)\b~i';

		$columns['pts.name']['regex'] ='/(sfr)|(townhouse)|(condo)|(row house)|(office)|(hotel)|(motel)|(special purpose)|(assisted care)|(industrial)|(retail)|(multi-family)|(land)/';

		$columns['p.bedroom']['regex']=	'/((with)|([0-9]+)) bedrooms?/';

		
		//$columns['p.zip']['regex'] = '/[a-zA-Z0-9 ]{3,}/';

	//	$columns['p.zip']['regex'] = '/([0-9]{3,})/';

		$columns['p.bathroom']['regex']=	'/((with)|([0-9]+)) ((bathrooms?)|(baths?))/';

		$columns['p.view']['regex']=	'/(none)|(panoramic)|(city)|(mountains\/hills)|(coastline)|(coastline)|(water)|(ocean)|(lake\/river)|(landmark)|(desert)|(bay)|(vineyard)|(golf)|(other)/';

		$columns['p.style']['regex']=	'/(american farmhouse)|(art deco)|(art modern\/mid century)|(cape cod)|(colonial revival)|(contemporary)|(craftsman)|(french)|(italian\/tuscan)|(prairie style)|(pueblo revival)|(ranch)|(spanish\/mediterranean)|(swiss cottage)|(tudor)|(victorian)|(historic)|(artichitucaul significnat)|(green)/';

		$columns['p.pool_spa']['regex']=	'/(with )?((spa and pool)|(pool and spa)|(pool)|(spa))/';

		$columns['p.condition']['regex']=	'/(fixer)|(good)|(excellent)|(remodeled)|(new construction)|(under construction)|(not disclosed)/';

		$columns['p.garage']['regex']=	'/((with)|([0-9]+)) garage/';

		$columns['p.units']['regex']=	'/(triplex)|(quad)/';

		$columns['p.occupancy']['regex']=	'/([0-9]+ occupants)/';

		$columns['p.type']['regex']=	'/(office)|(institutional)|(medical)|(warehouse)|(condo)|(r&d)|(business park)||(flex space)|(manufacturing)|(office showroom)|(self\/mini storage)|(truck terminal\/hub)|(distribuiotn)|(cold storage)|(community center)|(strip center)|(outlet center)|(power center)|(anchor)|(restaurant)|(service station)|(retail pad)|(free standing)|(day care\/nursery)|(post office)|(vehicle)|(economy)|(full service)|(assisted)|(acute care)|(golf)|(marina)|(theater)|(religious)/';

		$columns['p.listing_class']['regex']=	'/(class [a-eA-E])/';

		$columns['p.stories']['regex']=	'/([0-9]+ ((story)|(stories)))/';

		$columns['p.room_count']['regex']=	'/([0-9]+ (rooms?))/';

		$columns['p.type_lease']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';

		$columns['p.type_lease2']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';

		$columns['p.term']['regex']=	'/(blank)|(short term)|(m to m)|(year lease)|(multi year lease)/';

		$columns['p.furnished']['regex']=	'/(furnished)|(unfurnished)/';

		$columns['p.pet']['regex']=	'/(pet)/';

		$columns['p.zoned']['regex']=	'/((with)|([0-9]+)) units/';

		$columns['p.bldg_type']['regex']=	'/(north facing)|(south facing)|(east facing)|(west facing)|(low rise)|(mid rise)|(high rise)|(co-op)|(undisclosed)|(detached)|(attached)/';

		$columns['p.features1']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const\.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc\-pool)|(assoc\-spa)|(assoc\-tennis)|(assoc\-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net\-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';

		$filters = array();

		$workaround = array();

		$filtered;

		$raw = array();

		foreach($columns as $key => $column){

			preg_match_all($column['regex'], $searchval, $matches);

			$filtered = array_filter(array_map('array_filter', $matches));

			if(count($filtered)>0){

				$filtered = array_values($filtered);

				$filtered = $filtered[0];

				$column['matches'] = $matches;

				$raw[] = $matches[0];

				$constants = array('with', 'bedrooms', 'bedroom', 'bathrooms', 'bathroom', 'baths', 'bath', 'stories');

				$column['processed'] = str_replace($constants, '', $matches[0]);

				if($key=="p.furnished"){

					if($filtered[0]=="furnished")

						$column['processed'] = 1;

					else

						$column['processed'] = 0;

				}

				$filters[$key] = $column['processed'];

			}

		}

		$processed_refinement = array();

		if($refinement!=null){

			$refinev2 = array();

			foreach($refinement as $fields){

				$processed_refinement = array();

				foreach($fields as $r_item){

					if($r_item){

						$items = explode("{-}",$r_item);

						if($items[1] == "p.features1") {
							$processed_refinement[] = $items[1]." = '".$items[0]."'";
							$processed_refinement[] = "p.features2 = '".$items[0]."'";
							$processed_refinement[] = "p.features3 = '".$items[0]."'";
						
						} else if($items[1] == "p.sub_type") {
							#Select Residential Purchase and Lease SFR
							if($items[0] == 1 || $items[0] == 5) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '5'";
							#Select Residential Purchase and Lease Condo
							} else if($items[0] == 2 || $items[0] == 6) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '6'";
							#Select Residential Purchase and Lease Townhouse
							} else if($items[0] == 3 || $items[0] == 7) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '7'";
							#Select Residential Purchase and Lease Land
							} else if($items[0] == 4 || $items[0] == 8) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '8'";
							#Select Commercial Purchase and Lease Office
							} else if($items[0] == 10 || $items[0] == 16) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '16'";
							#Select Commercial Purchase and Lease Industrial
							} else if($items[0] == 11 || $items[0] == 17) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '17'";
							#Select Commercial Purchase and Lease Retail
							} else if($items[0] == 12 || $items[0] == 18) {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
								$processed_refinement[] = $items[1]." = '18'";
							} else {
								$processed_refinement[] = $items[1]." = '".$items[0]."'";
							}
						} else {
							$processed_refinement[] = $items[1]." = '".$items[0]."'";
						}
						
					}

				}

				$refinev2[] =  "( ".implode(" OR ", $processed_refinement)." )";

			}

		}



		$terms = array();

		foreach($raw as $search){

			foreach($search as $individual){

				$terms[] = $individual;

			}

		}



		$stripped = str_replace($terms, '', $searchval);

		$raw[] = array($stripped);

		$more_terms = array($stripped);





		foreach ($more_terms as $term){

			if(trim($term)!=""){

				$filters['p.zip'][] = $term;

				$filters['p.city'][] = $term;

				$filters['p.state'][] = $term;

				$filters['z.zone_name'][] = $term;

				$filters['country.countries_name'][] = $term;

				$filters['pt.type_name'][] = $term;

				$filters['pts.name'][] = $term;

				$filters['p.type'][] = $term;

			}

		}

		$filters['include_all'] = 1;



		
		if(!$refine)

			return serialize(array($this->get_listing($filters, "OR", $oldval, true, false, null, $order, $limitUpper), $raw));

		else

			return serialize(array($this->get_listing($filters, "OR", $oldval, true, true, $refinev2, $order, $limitUpper), $raw));

	}



	public function getViews($lid){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('count(distinct(user_id)) as views');

		$query->from('#__views');

		$query->where('property_id='.$lid);

		$query->order('property_id desc');

		$db->setQuery($query);

		return $db->loadObjectList();

	}



	public function count(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('count(*) as count'));

		$query->from('#__pocket_listing as pl');

		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');

		$db->setQuery($query);

		$allitems = $db->loadObjectList();

		return $allitems;

	}



	public function get_property_image($lID){



		$db		= JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('*'));

		$query->from('#__pocket_images');

		$query->where('listing_id = '. $lID);

		$db->setQuery($query);

		$images = $db->loadObjectList();

		return $images;



	}

	public function get_property_types(){

		$db		= JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_type');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;

	}

	public function get_property_subtypes(){

		$db		= JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;

	}


	public function get_listing($filters, $sticky = "AND", $oldval = 1, $search = false, $refine = false, $refinement = null, $order = "DESC", $limitUpper = 0){


		


		$conditions = array();

		$limit = (($oldval-1)*$limitUpper);

		//$limitUpper = 12;

		$today = date ('Y-m-d');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('p.*, country.*,ps.*,pp.*,cvs.*,
						ccon.symbol, 
						pi.image as images,
						users.name as username, users.email as useremail, users.id as userid,
						reg.user_type, reg.user_id as regid, reg.state as user_state, reg.user_type as usertype, reg.email as email,
						sales.verified_2012 as v2012, 
						sales.verified_2013 as v2013, 
						sales.verified_2014 as v2014, 
						z.zone_name, 
						pt.type_id,
						pt.type_name,
						pts.sub_id,
						pts.name, 
						p.listing_id, 
						viewers.user_b as allowed, 
						requests.user_b as requests, 
						denied.user_b as denied,
						ugm.group_id');

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__country_currency ccon ON ccon.currency = p.currency');

		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');

		$query->leftJoin('#__countries country ON country.countries_id = z.zone_country_id');

		$query->leftJoin('#__property_type pt on pt.type_id =  p.property_type');

		$query->leftJoin('#__property_sub_type pts on pts.sub_id =  p.sub_type');

		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');

		//$query->leftJoin('#__country_sp_address csp	on	csp.country = p.country AND 	');

		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');

		$query->leftJoin('#__user_usergroup_map ugm ON ugm.user_id = p.user_id');

		$query->where('date_expired > \''. $today .'\' AND sold = 0 AND group_id NOT IN(11,12)');

		$zip_key="usa";
		$country['canada'] = "xxx xxx";
		$country['usa'] = "00000";




		foreach ($filters as $key => $value){

			if(!$search){

				if($value!="" && $value){

					if($key=="include_all")

						continue;

					else if($key=="p.user_id")

						$conditions[] = $key." = ".$value;

					else if($key=="p.zip"){

						if(preg_match("/^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/",$value)){
							$zip_key="canada";
						}

						foreach ($country as $key3 => $value3) {
							if($key3==$zip_key){
								$value =wordwrap($value , 3 , ' ' , true );
								$conditions[] = $key." like '%".$value."%'";
							} else {
								
							} 
						}

					}

					else if(

						$key=="p.property_type" || 

						$key=="p.sub_type" || 

						$key=="p.city" || 

						$key=="p.state" || 

						$key=="z.zone_name" 

					)

						$conditions[] = $key." like '%".$value."%'";

					else if($key!="pp.price")

						$conditions[] = $key." = '".$value."'";

					else{

						$explode = explode('-', $value);

						if(count($explode)>1){

							$conditions[] = $key."1 > ".$explode[0]." AND ".$key."2 < ".$explode[1];

						}

						else

							$conditions[] = $key."1 > ".$value;

					}

				}

			}

			else{

				if($key=="include_all")

						continue;



				foreach($filters[$key] as $key2=>$value){

					if($key=="p.user_id")

						$conditions[] = $key." = ".$value;

					else if(trim($key)=="p.features1"){

						$conditions[] = $key." = '".$value."'";

						$conditions[] = "p.features2 = '".$value."'";

						$conditions[] = "p.features3 = '".$value."'";

					}

					else if($key=="p.zip"){

						if(preg_match("/^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/",$value)){
							$zip_key="canada";
						}

						foreach ($country as $key3 => $value2) {
							if($key3==$zip_key){
								$value2 =wordwrap($value2 , 3 , ' ' , true );
								$conditions[] = $key." like '%".$value."%'";
							} else {
								$conditions[] = $key." like '%".trim($value)."%'";
							} 
						}



						
					}

					else if(	

						$key=="pt.type_name"||

						$key=="pts.name" ||	

						$key=="p.type" ||	

						$key=="p.city" || 

						$key=="p.state" || 

						$key=="z.zone_name"

					)

						$conditions[] = $key." like '%".$value."%'";

					else if($key!="pp.price")

						$conditions[] = $key." = '".$value."'";

					else{

						$explode = explode('-', $value);

						if(count($explode)>1){

							$conditions[] = $key."1 > ".$explode[0]." AND ".$key."2 < ".$explode[1];

						}

						else

							$conditions[] = $key."1 > ".$value;

					}

				}



			}

		}


		$db = JFactory::getDbo();
    	$query3 = $db->getQuery(true);
    	$query3->select('postcode')
    	->from('#__country_sp_address csp')
    	->where("csp.county LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address1 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address2 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address3 LIKE '%".$filters['p.zip'][0]."%' OR
    		     csp.address4 LIKE '%".$filters['p.zip'][0]."%' OR
    		     csp.address5 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address6 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address7 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address8 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address9 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address10 LIKE '%".$filters['p.zip'][0]."%' OR 
    		     csp.address11 LIKE '%".$filters['p.zip'][0]."%' OR   
    		     csp.address12 LIKE '%".$filters['p.zip'][0]."%' ");


    	//$filters['csp.zip'] =
    	$wz_postcodes= $db->setQuery($query3)->loadObjectList();
    	$last_cond = count($conditions);
		
		if(count($wz_postcodes)){
			foreach ($wz_postcodes as $key => $value) {
				# code...
				$conditions[$last_cond] = "p.zip like '".$value->postcode."%'";
				$last_cond++;
			}
			
		}

		$where="";

		if(count($conditions)>0){

			$where = "(".implode(" OR ",$conditions).")";

			if($refine && !empty($refinement))

				$where.= " AND ";

		}

		if($refine && !empty($refinement))

			$where.= implode(" AND ", $refinement);



		//if($filters['include_all'])

			//$where.= " AND NOT p.user_id = ".JFactory::getUser()->id.' ';


		//if(isset($filters["p.city"]) && strlen($filters["p.city"][0])<3){
		//		$where="p.listing_id = 0";
		//}

		$order_case= "";
		foreach ($filters as $key => $value) {
			# code...
			if(is_array($value)){
				
				$order_case .= "CASE
							    WHEN ".$key." LIKE '".$value[0]."%' THEN 1
							    WHEN ".$key." LIKE '%".$value[0]."' THEN 3
							    ELSE 2
							  END,";
			}

		}
		
		
		$query->where($where. " AND reg.user_type != 86");



		if(!(count($conditions)>0) && ! ($refine && !empty($refinement)))

			return null;



		$query->leftJoin('#__users users ON users.id = p.user_id');
		$query->leftJoin('#__user_registration reg ON reg.email = users.email');
		$query->leftJoin('#__user_sales sales ON sales.agent_id = reg.user_id');


		$query->leftJoin('#__pocket_images as pi ON p.listing_id = pi.listing_id');
		
		$query->leftJoin("#__request_access as viewers ON viewers.property_id = p.listing_id AND viewers.user_b = '" . JFactory::getUser()->id . "' AND viewers.permission = 1");
		
		$query->leftJoin("#__request_access as requests ON requests.property_id = p.listing_id AND requests.user_b = '" . JFactory::getUser()->id . "' AND requests.permission = 0");
		
		$query->leftJoin("#__request_access as denied ON denied.property_id = p.listing_id AND denied.user_b = '" . JFactory::getUser()->id . "' AND denied.permission = 2");



		$query->group("p.listing_id");

		$query->order($order_case."p.date_created ".$order.',p.listing_id '.$order);
		
		if(!$limitUpper) {
			$db->setQuery($query);
			
		} else {
			$db->setQuery($query,$limit,$limitUpper);
		}
		

		

		// echo $query; var_dump((string)$db->getQuery());	

		// die();

		//$db->setQuery($query);

		

		return $db->loadObjectList();



	}

	

	public function get_default_listing_image($property_type, $sub_type, $bw = false) {

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$config = new JConfig();

		$query->select('type_name')->from('#__property_type')->where('type_id = '.$property_type);

		$db->setQuery($query);

		$rows = $db->loadObjectList();

		$query->select('name')->from('#__property_sub_type')->where('type_id = '.$property_type.' AND sub_id='.$sub_type);

		$db->setQuery($query);

		$rows2 = $db->loadObjectList();

		
	//var_dump($rows2);
		if($rows2){
			$property_type = strtolower(str_replace(" ", "-", $rows2[0]->type_name));

			$sub_type = strtolower($rows2[0]->name);
		}



		switch($sub_type) {

			case "motel/hotel":

				$sub_type = "motel";

				break;

			case "townhouse/row house":

				$sub_type = "townhouse";

				break;

			case "assisted care":

				$sub_type = "assist";

				break;

			case "special purpose":

				$sub_type = "special";

				break;

			case "multi family":

				$sub_type = "multi-family";

				break;

		}

		

		$img_filename = ($bw) ? $property_type . "-" . $sub_type . "-bw.jpg" : $property_type . "-" . $sub_type . ".jpg";

		return $img_filename;

	}

	

	public function get_network($userid, $status = 1){



		$db = JFactory::getDbo();



		$conditions = array();



		$conditions[] = 'user_id = '.$userid;



		$conditions[] = 'status = '.$status;



		$innerquery = $db->getQuery(true);



		//$broker_id = $this->get_broker_id(JFactory::getUser($userid)->email, 'email');



		$query = $db->getQuery(true);



		



		$add = "";



		$query->setQuery("



				SELECT	other_user_id



				  FROM	#__request_network



				 WHERE	user_id =".$userid."



				   AND	status = ".$status.$add



				);



		$db->setQuery($query);



		return $db->loadObjectList();



	}

	

	public function get_owner($userid){



		$db = JFactory::getDbo();



		//$broker_id = $this->get_broker_id(JFactory::getUser($userid)->email, 'email');



		$query = $db->getQuery(true);



		$query->select('u.name, ur.firstname, ur.lastname, us.verified_2012, us.verified_2013, ur.state');

		

		$query->from('#__users u');

		

		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		

		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');

		

		$query->where('u.id ='.$userid);

		

		$db->setQuery($query);



		return $db->loadObjectList();

		





	}

	

	

}