<?php

/**

 * @version     1.0.0

 * @package     com_customsearch

 * @copyright   Copyright (C) 2013. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com 
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
//$lang = JFactory::getLanguage();
//$lang->load('com_customsearch', JPATH_ADMINISTRATOR);
$map = array(
			"zip"				=> JText::_("COM_NRDS_FORMZIP"),
			"ur.city"			=> JText::_("COM_NRDS_FORMCITY"),
			"mn.value"			=> JText::_('COM_NRDS_FORMMOBILE'),
			"wn.value"			=> JText::_('COM_NRDS_FORMWORK'),
			"zone_code"			=> JText::_("COM_NRDS_FORMSTATE"),
			"countries_name"	=> JText::_("COM_NRDS_FORMCOUNTRY"),
			"broker_name"		=> JText::_('COM_NRDS_FORMBROKER'),
			"ave_sales"			=> JText::_('COM_USERPROF_PROF_AVESALE'),
			"ave_sides"			=> JText::_('COM_USERPROF_PROF_TOTSID'),
			"total_sales"		=> JText::_('COM_USERPROF_PROF_TOTVOL'),
			"newtblang.lang_result" 			=> JText::_('COM_USERPROF_SPOKENLANG'),
			"designations.designation_list"	=> JText::_('COM_USERPROF_PROF_DESIGS'),
		
		);
$language = JFactory::getLanguage();
$extension = 'com_nrds';
$base_dir = JPATH_SITE;
$language_tag = JFactory::getUser()->currLanguage;
$language->load($extension, $base_dir, $language_tag, true);

	$nb_elem_per_page = 21;
	
	$number_of_pages = intval($this->result_count/$nb_elem_per_page)+1;


	function compareByName($a, $b) {
	  return strcmp($a["country"], $b["country"]);
	}

	function findKey($array, $keySearch)
	{
	    foreach ($array as $key => $item) {
	        if ($key == $keySearch) {
	         //   echo 'yes, it exists';
	            return true;
	        }
	        else {
	            if (is_array($item) && findKey($item, $keySearch)) {
	               return true;
	            }
	        }
	    }

	    return false;
	}
?>
<style type="text/css">
.alert.alert-warning{
	display: none !important;
}

/*.sagent_user_details_lower.wz {
    min-height: 67px;
}*/
</style>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.infinitescroll.js"/></script>
<div class="wrapper">
	<div class="wide left search-wrap">
		<!-- start wide-content -->
		<form id="refine_form" method="POST"
			action="<?php echo JRoute::_('index.php?option=com_customsearch') ?>"
			name="refine_form">
			<input type="hidden" name="refine_search" value="refine_search" />
			<h1><?php echo JText::_('COM_SEARCH_RESULT');?></h1>
			<div class="items-filter">
				<!-- start list-tile filter -->
	
				<?php $ctrert=0; ?>
				<?php 
					/*
					foreach ($this->users as $user) { 
						
						$groupsUserIsIn = JAccess::getGroupsByUser($user->id);
						if(!in_array(7,$groupsUserIsIn) && !in_array(8,$groupsUserIsIn)) {
							$ctrert++;
						}
					}
					*/
					$ctrert = $this->result_count;
				?>
				<span class="tile-number"> 
					<?php  
						// only 1 result
						echo ( $ctrert==1 ) ? $ctrert." ".JText::_('COM_SEARCH_AGENTFOUND')."." : "";
						// greater than one result
						echo ( $ctrert>1  ) ? $ctrert." ".JText::_('COM_SEARCH_AGENTSFOUND').".": "";
						// no result
						echo ( $ctrert==0 && $this->originaltext !== "" ) ?  " ".JText::_('COM_SEARCH_NOAGENTS')."" : ""; 
						
						echo ( crtert== 0 && $this->originaltext == "" ) ? " ".JText::_('COM_SEARCH_NOAGENTS_EMPTYSEARCH')."" : "";
					?>
				</span>
			</div>
			<!-- end list-tile filter -->
			<?php if( $ctrert>1 ) { ?>
				<div class="search-left left">
					<!-- Start Refine Search -->
					<div class="refine-container">
						<input type="hidden" name="jform[name]" value="refine_search" /> 
						<input type="hidden" name="task" value="searchagent" /> 
						<input type="hidden" name="searchword" id="oldtext" value="<?php echo $this->originaltext; ?>" />
						<h2><?php echo JText::_('COM_SEARCH_REFINE');?></h2>
						<?php
						$search_keyword = "";
						foreach ($this->keys as $inner){
						if($search_keyword == "") $search_keyword = $inner;	
						?>
						<div class="breadbox_orig">
							<div class="facet-title"><?php echo $inner;?></div>
							<div class="facet-remove"><input type="hidden" class="oldtext" value="<?php echo $inner?>" /></div>
						</div>
						<div class="clear-float"></div>
              
						<?php 
							}
							if(isset($this->fields))
								foreach ($this->fields as $key => $temp){
									foreach($temp as $value){
										?>
										<div class="breadbox">
											<div class="facet-title"><?php echo $value;?></div>
											<div class="facet-remove"><input type="hidden" value="<?php echo $value;?>" name="fields[<?php echo $key ?>][]" />
											</div>
										</div>
										<img id="loading-small" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; margin-left:90px; height:20px" />

						<?php 
									}
								}
						
						?>
						<div class="show-hide-refinement"><a href="javascript:void(0);">Refine Search</a></div>
						<div class="refinement-options">
						<input style="padding: 8px 15px 15px 15px;width: 150px;font-size: 12px;margin: 20px auto 0px;  display: block;" class="button gradient-green" type="button" id="submitrefinesearch" name="refine_button" value="<?php echo JText::_('COM_SEARCH_REFINEBT');?>" />
						<?php if($this->refine)
							foreach ($this->refine as $key => $refinement) {
							$i=0;
							$temp = array_unique($refinement);
							asort($temp);
							$counts = @array_count_values($refinement);
						?>
					
						<?php 
						if (!empty($temp[0]) || !empty($temp[1]) ) { ?>
							<div class="search-summarize">
								<!-- Start Summarize -->
								<div class="search-summarize-title">
									<?php echo $map[$key]; ?>
								</div>
								<div class="sub-nav">
									<ul>
										<?php $x = 0; $y = 1; $z = 0;?>
										<?php foreach($temp as $value) {?>
										<?php 
									
										if($value) { ?>
										<?php if($x == 0) { ?>
											<?php if(strpos($key, ".") !== FALSE ) { 
												$_key = explode(".", $key);
												$key = $_key[1];
											} ?>
											<?php $style = ($y > 1) ? "style='display:none;'" : ""; ?>
											<?php echo "<div class='more_" . $key . "_" . $y . "' ". $style .">"; ?>
										<?php } ?>
										<li style="margin-bottom:8px">
         
                                        <input class="keyword left" id="<?php echo $key.$i;?>" type="checkbox"
											value="<?php echo $value?>"
											name="fields[<?php echo $key?>][]">
										<label class="clear-both" style="width:170px; line-height:16px"
											for="<?php echo $key.$i++;?>"><?php echo $value;?><span> (<?php echo $counts[$value]?>)
											</span> </label>
										</li><div class="clear-float"></div>
										<?php 
											if($x < 9) { 
												$x++;
											} else { 
												$x = 0;
												$y++;
												echo "</div>";
											}
											$z++;
										?>                                
										<?php } ?>
										<?php } ?>
										<?php echo ($x < 9 && $x > 0) ? "</div>" : ""; ?>
										<?php 
											if ($z > 9) {
												echo "
													<div id='more_" . $key . "_options' data-key='" . $key . "' style='color:#007bae;font-size:12px;cursor:pointer;margin-top:10px;margin-left:-10px;'>+ " . JText::_('COM_SEARCH_MORE_OPTIONS') . "</div>
													<script type='text/javascript'>
														var " . $key . "_ctr = 2;
														jQuery('#more_" . $key . "_options').click(function(){
															jQuery('.more_" . $key . "_' + " . $key . "_ctr).removeAttr('style');
															" . $key . "_ctr+=1;
															if(!jQuery('.more_" . $key . "_' + " . $key . "_ctr).length) {
																jQuery(this).attr('style', 'display:none;');
															}
														});
													</script>											
												"; 
											} else {
												echo "";
											}
										?>
									</ul>
								</div>
							</div>
							<?php } ?>
						<?php } ?>
					<input style="padding: 8px 15px 15px 15px;width: 150px;font-size: 12px;margin: 20px auto 0px;  display: block;" class="button gradient-green" type="button" id="submitrefinesearch2" name="refine_button" value="<?php echo JText::_('COM_SEARCH_REFINEBT');?>" />	
						</div>
					</div>
				</div>
			<?php } ?>
		</form>
	<!-- End Refine Search -->
	<div class="search-right left">
		
		<div class="items-container">
		    
			<ul class="referral_row">
				<?php


				 foreach ($this->users as $user){?>
				<?php
					//$groupsUserIsIn = JAccess::getGroupsByUser($user->id);
					//if(!in_array(7,$groupsUserIsIn) && !in_array(8,$groupsUserIsIn)) {
					//avoids super users
					
				?>
				<?php //if ($user->is_term_accepted==1) { ?>
				<li>
					<div class="clear-float"></div>
					<div class="sbox_agent">
     					 <!--Agent Grid Upper Box-->
								<div>
                                	<div class="sagent">
                                	    <div class="sagent_user">
											<?php if ($user->image!="") { ?>
												<a href="<?php echo ($user->id == JFactory::getUser()->id) ? JRoute::_('index.php?option=com_userprofile&task=profile') : JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$user->id;  ?>">
													<div class="sagent_user_pic left">
													<img  width="60px" src="<?php echo strpos($user->image, JURI::base()) !== false ? str_replace("loads/", "loads/thumb_", $user->image).'?'.microtime(true) :  JURI::base().'uploads/thumb_'.$user->image.'?'.microtime(true)?>"></div>
												</a>
												
											<?php } else { ?>
												<a href="<?php echo ($user->id == JFactory::getUser()->id) ? JRoute::_('index.php?option=com_userprofile&task=profile') : JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$user->id;  ?>">
													<div class="sagent_user_pic left">
													<img  width="60px" src="<?php echo $this->baseurl ?>/templates/agentbridge/images/temp/blank-image.jpg" ></div>
												</a>							
												
											<?php } ?>
									    </div>
                                 
									<div class="sagent_user_name">
										<div>
										<h2 style="margin-left:5px"><a href="<?php echo ($user->id == JFactory::getUser()->id) ? JRoute::_('index.php?option=com_userprofile&task=profile') : JRoute::_('index.php?option=com_userprofile&task=profile').'&uid='.$user->id;  ?>" class="item-number text-link"><?php echo stripslashes_all($user->name); ?>
							</a></h2></div>
                            			<div><?php echo ($user->verified_2014==1) ? "<span style='margin-top:0px; margin-left:5px' class='network_user_logo'></span>" : "<span style='margin-top:0px; margin-left:5px' class='network_unverified_logo'></span>" ; ?></div>
								</div>
								<div class="sagent_user_details">
								 <ul>
                                        <li class="a_wrap_search wrap_brokerage"><?php echo stripslashes_all($user->brokerage); ?><br/>
                                        </li>
                                        <li><?php echo stripslashes_all($user->city); ?><?php echo ($user->city || $user->user_zone_code) ? ", ":""?><?php echo ($user->country == 223) ? $user->user_zone_code : $user->user_zone_name; ?></li>
											
										<li><?php echo $user->countries_name; ?></li>
                                        <li><?php echo $user->countryCode; ?> <?php echo ($user->mobile!=="") ? $user->mobile : $user->work;?></li>
                                        <li class="a_wrap_search"><!--email_off--><a style="font-size:11px" href='mailto:<?php echo $user->email; ?>'><?php echo $user->email; ?></a><!--/email_off--></li>
                                       </ul>
								</div>
                                </div>
      					 <!--Agent Grid Lower Box-->        
       							<div style="margin:10px"> 
                                	<div class="sagent_user_details_lower wz"> 
       									<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_WORKSAROUND');?></div>
                              			<?php 
                              			/*	$concat_zips = $user->zips.",".$user->b_zips;
                              				$w_zips_arr = explode(",", $concat_zips);
                              				$w_zips_distinct = implode(", ", array_unique($w_zips_arr));
                              			*/?>                              			
                              			<div class="sagent_user_details_actual left zips">
	                              			<?php

				                        		$zip_arr_val = $user->zip_arr_val;
												$i=0;
												$zips_list = array();
												$span_act=0;
													
												$currenCountArray = array_diff($this->allcountriesabled, array($user->country));
												sort($currenCountArray);
												array_unshift($currenCountArray, $user->country);

												uasort($zip_arr_val, function ($a, $b) use ($currenCountArray) {
												    $pos_a = array_search($a['country'], $currenCountArray);
												    $pos_b = array_search($b['country'], $currenCountArray);
												    return $pos_a - $pos_b;
												});
												
													//var_dump($currenCountArray);

												if(count($zip_arr_val)>=1){
													$prev_zip = "";
													$inviclass="b";
													$displaB = "display:block;";
													$displaIB = "display:inline-block;";


														 
													 foreach($zip_arr_val as $zip => $vals){
													 	$flagicon="";
													 		if($i==3 ){
														 		//echo "<div class='more-zip' style='display:none'>";
														 		$inviclass=" hidethis";
														 		$displaB = "display:none;";
																$displaIB = "display:none;";
															} 
													 	 if($prev_zip != $vals['country']) {
															if($this->userDetails->country!=$vals['country']){
																if($vals['country'] == 0 && $this->userDetails->country!=223){
																	$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
																} elseif($vals['country'] != 0) {
																	$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($vals['this_country_iso']).'-flag-lang.png">';
																}
															}
															if($prev_zip!=""){
																echo "</div>";
															}
															echo "<div class='".$inviclass."' style='".$displaB."clear: both;float:left;margin-right: 10px;'>".$flagicon."</div> <div style='display: table-cell;font-weight: bold;'>";
														} else {

														}	

														
														/*
														foreach($user->zips_b as $zips_bs) { 
													 		
													 		if((strpos($zips_bs['zip'], $zip) !== FALSE) || $zips_bs['zip']==$zip){
													 			if($this->userDetails->country!=$zips_bs['country']){
														 			if($zips_bs['country'] == 0 && $this->userDetails->country!=223){
																		$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
																	} elseif($zips_bs['country'] != 0) {
																		$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($zips_bs['this_country_iso']).'-flag-lang.png">';
																	}
																}
													 		}  else {
													 			foreach($this->user_info->buyersDets as $buyer_s) { 
													 				if($buyer_s->needs[0]->zip==$zip){
														 				if($this->user_info->countryId!=$buyer_s->needs[0]->country){
																 			if($buyer_s->needs[0]->country == 0 && $this->user_info->countryId!=223){
																				$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
																			} elseif($buyer_s->needs[0]->country != 0) {
																				$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($buyer_s->needs[0]->countries_iso_code_2).'-flag-lang.png">';
																			}
																		}	
													 				}
													 			}
													 		}
														 	}
															*/
														 	if($vals["pop"]!=0 && (!isset($vals["buyer"])) || $vals["buyer"]<1){
														 		$qtips=$vals["pop"]." POPs™";
														 	} else if($vals["buyer"]!=0 && (!isset($vals["pop"]) || $vals["pop"]<1)){
														 		$qtips=$vals["buyer"]>1 ? $vals["buyer"]." buyers":$vals["buyer"]." buyer";
														 	}	else {
														 
														 		$qtips=$vals["buyer"]>1 ? $vals["pop"]." POPs™ and ".$vals["buyer"]." buyers":$vals["pop"]." POPs™ and ".$vals["buyer"]." buyer";
														 	}

														 	
														 	if($i==3 ){
														 		$span_act =1;
														 		//echo "<div class='more-zip' style='display:none'>";
														 		/*$inviclass=" hidethis";
														 		$displaB = "display:none;";
																$displaIB = "display:none;";*/
															} 
														 	
														 	 
															 echo " <div style='".$displaIB.";height:20px;padding-top: 4px;' class= '".$inviclass."'>".' <label 
															 			class="workzip_count" 
																		style="color:#007bae;padding-top: 2px;display: inline-block;"
																		data="'.$zip.'"';

															 if($vals["pop"] || $vals["buyer"]){
															 echo 'onMouseover="ddrivetip(\''.$qtips.'\'); hover_dd()" onMouseout="hideddrivetip(); hover_dd()"';}

															 if(($i++!=count($zip_arr_val)-1) && $zip){
															 	if($i==3){
															 		$displayspan = "display:none";
															 	} else {
															 		$displayspan = "display:inline";
															 	}
															 $com = "<span class='span".$i."' style='".$displayspan."'>,</span>";}
															 else{
															 $com = "";}

															 echo '>'.trim($zip).'</label>'.$com.'</div>';
															//$i++;

															
															
															 $prev_zip = $vals['country'];

															/* if($prev_zip != $vals['country']) {
																echo "</div>";
															}*/

													    }

													 	if($span_act && count($zip_arr_val) > 3){
														 	echo "</div><a href='javascript:void(0)' class='see-zip' style='font-size:11px;white-space: nowrap;clear: both;padding-top: 5px;display: inline-block;'>+".JText::_('COM_USERPROF_PROF_SEEMORE')."</a>";
														 } else {
														 	echo "</div>";
														 	if(count($zip_arr_val)==1){
														 		//echo "<div style='height: 14px;'></div>";
														 	} else {
														 		//echo "<div style='height: 12px;'></div>";
														 	}
														 	
														 }
												} 
											?>
											<?php ?>
										</div>  
                                    </div>  
                              		<div class="sagent_user_details_lower">
									  <?php if ($user->verified_2015==1 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE2');?><br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format((($user->volume_2015/$user->sides_2015) + ($user->volume_2016/$user->sides_2016))/2)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?> 2016<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2016/$user->sides_2016)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==1 && $user->verified_2016==0 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?> 2015<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->ave_price_2015)." ".$this->userDetails->currency ?></div>
									 <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==1 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?> 2014<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->ave_price_2014)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==1 ) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?> 2013 <br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->ave_price_2013)." ".$this->userDetails->currency ?></div>
									  <?php }  else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==0) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?><br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } else {?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_AVESALE');?> <br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } ?>								  
									  </div>
                              
                              	    <div class="sagent_user_details_lower">
									<?php if ($user->verified_2015==1 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL2');?><br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2015 + $user->volume_2016)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?> 2016<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2016)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==1 && $user->verified_2016==0 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?> 2015<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2015)." ".$this->userDetails->currency ?></div>
									 <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==1 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?> 2014<br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2014)." ".$this->userDetails->currency ?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==1 ) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?> 2013 <br/></div>
											<div class="sagent_user_details_actual left"><span class="curr_symbol"><?php echo $this->userDetails->symbol."</span> ".number_format($user->volume_2013)." ".$this->userDetails->currency ?></div>
									  <?php }  else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==0) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?><br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } else {?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTVOL');?> <br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } ?>								  
                                    </div>       
                                    
									<div class="sagent_user_details_lower">
									<?php if ($user->verified_2015==1 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES2');?><br/></div>
											<div class="sagent_user_details_actual left"><?php echo number_format($user->sides_2015 + $user->sides_2016)?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==1)  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?> 2016<br/></div>
											<div class="sagent_user_details_actual left"><?php echo number_format($user->sides_2016)?></div>
									  <?php } else if ($user->verified_2015==1 && $user->verified_2016==0 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?> 2015<br/></div>
											<div class="sagent_user_details_actual left"><?php echo number_format($user->sides_2015) ?></div>
									 <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==1 )  { ?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?> 2014<br/></div>
											<div class="sagent_user_details_actual left"><?php echo number_format($user->sides_2014) ?></div>
									  <?php } else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==1 ) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?> 2013 <br/></div>
											<div class="sagent_user_details_actual left"><?php echo number_format($user->sides_2013) ?></div>
									  <?php }  else if ($user->verified_2015==0 && $user->verified_2016==0 && $user->verified_2014==0 && $user->verified_2013==0) {?>
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?><br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } else {?> 
											<div class="sagent_user_details_labels"><?php echo JText::_('COM_SEARCH_TOTSIDES');?> <br/></div>
											<div style="font-size:12px; margin-top:5px; margin-bottom:2px"><?php echo JText::_('COM_SEARCH_VERIFYINGNUM');?></div>
									 <?php } ?>	
                                   </div>      
                               </div>   
						</div>
					</div>
				</li>
				<?php //} ?>
					<?php //} ?>
				<?php } ?>
			</ul>
			<ul id='paginator' style="position:relative;float:left;clear:both;color:#fff !important;" >
				<?php if(isset($_GET['page']) && $_GET['page']>1){?>
					<li><a class="page_prev" href='./task=searchagent&page=<?php echo $_GET['page']-1?>&<?php echo $this->query_string; ?>'><< Prev </a>|</li>
				<?php }?>				
				<?php for($i=1;$i<$number_of_pages;$i++){?>
					<li><a class="page_number" href='./task=searchagent&page=<?php echo $i?>&<?php echo $this->query_string; ?>'><?php echo $i?></a></li>
				<?php }?>
				<?php if(isset($_GET['page']) && $_GET['page']<($number_of_pages-1)){?>
					<li> |<a class="page_next" href='./?task=searchagent&page=<?php echo $_GET['page']+1?>&<?php echo $this->query_string; ?>'> Next>></a></li>
				<?php } else if($number_of_pages>1) {?>
					<li> |<a class="page_next" href='./?task=searchagent&page=2&<?php echo $this->query_string; ?>'> Next>></a></li>
				<?php }?>
			</ul>
		</div>
	</div>
</div>
<!-- end wide-content -->
</div>
<a href="javascript:void(0);" class="button gradient-blue back-to-top">Back To Top</a>
<script>
<?php if(!isset($_GET['page']) || $_GET['page'] < 2){?>
mixpanel.track("Search Agent - <?php echo $search_keyword; ?>");
<?php } ?>
function setPage(page){
	jQuery("input[name=\"page\"]").val(page);
}

	function setConformingHeight(el, newHeight) {
		// set the height to something new, but remember the original height in case things change
		el.data("originalHeight", (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight")));
		el.css("min-height",(newHeight+10)+"px");
	}
	function getOriginalHeight(el) {
		// if the height has changed, send the originalHeight
		return (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight"));
	}

	function columnConform() {
		var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = new Array();
		// find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
		jQuery('li .sbox_agent').each(function() {

		// "caching"
		var $el = jQuery(this);
		var topPosition = $el.position().top;
		if (currentRowStart != topPosition) {
		// we just came to a new row. Set all the heights on the completed row
		for(currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) setConformingHeight(rowDivs[currentDiv], currentTallest);
		// set the variables for the new row
		rowDivs.length = 0; // empty the array
		currentRowStart = topPosition;
		currentTallest = getOriginalHeight($el);
		rowDivs.push($el);
		} else {
		// another div on the current row. Add it to the list and check if it's taller
		rowDivs.push($el);
		currentTallest = (currentTallest < getOriginalHeight($el)) ? (getOriginalHeight($el)) : (currentTallest);
		}
		});
		// do the last row
		for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			setConformingHeight(rowDivs[currentDiv], currentTallest);
		}
	}


	function setConformingHeight_wz(el, newHeight) {
		// set the height to something new, but remember the original height in case things change
		el.data("originalHeight_wz", (el.data("originalHeight_wz") == undefined) ? (el.height()) : (el.data("originalHeight_wz")));
		el.css("min-height",(newHeight)+"px");
	}
	function getOriginalHeight_wz(el) {
		// if the height has changed, send the originalHeight
		return (el.data("originalHeight_wz") == undefined) ? (el.height()) : (el.data("originalHeight_wz"));
	}
	function columnConform_wz() {
		// find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
		var currentTallest_wz = 0,
		currentRowStart_wz = 0,
		rowDivs_wz = new Array();
		jQuery('.sagent_user_details_lower.wz').each(function() {
		// "caching"
		var $el = jQuery(this).offsetParent(".sbox_agent");
		var topPosition_wz = $el.position().top;
		var $el_this = jQuery(this);
		console.log(currentRowStart_wz);
		console.log(topPosition_wz);
		if (currentRowStart_wz != topPosition_wz) {

		// we just came to a new row. Set all the heights on the completed row
		for(currentDiv_wz = 0 ; currentDiv_wz < rowDivs_wz.length ; currentDiv_wz++) setConformingHeight_wz(rowDivs_wz[currentDiv_wz], currentTallest_wz);
		// set the variables for the new row
		rowDivs_wz.length = 0; // empty the array
		currentRowStart_wz = topPosition_wz;
		currentTallest_wz = getOriginalHeight_wz($el_this);
		rowDivs_wz.push($el_this);
		} else {
		// another div on the current row. Add it to the list and check if it's taller
		rowDivs_wz.push($el_this);
		currentTallest_wz = (currentTallest_wz < getOriginalHeight_wz($el_this)) ? (getOriginalHeight_wz($el_this)) : (currentTallest_wz);
		}
		});
		// do the last row
		for (currentDiv_wz = 0 ; currentDiv_wz < rowDivs_wz.length ; currentDiv_wz++) {
		setConformingHeight_wz(rowDivs_wz[currentDiv_wz], currentTallest_wz);
		}
	}
	function setConformingHeight_useri(el, newHeight) {
	// set the height to something new, but remember the original height in case things change
		el.data("originalHeight_useri", (el.data("originalHeight_useri") == undefined) ? (el.height()) : (el.data("originalHeight_useri")));
		el.css("min-height",(newHeight)+"px");
	}
	function getOriginalHeight_useri(el) {
	// if the height has changed, send the originalHeight
		return (el.data("originalHeight_useri") == undefined) ? (el.height()) : (el.data("originalHeight_useri"));
	}
	function columnConform_useri() {
		// find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
		var currentTallest_useri = 0,
		currentRowStart_useri = 0,
		rowDivs_useri = new Array();
		jQuery('.sagent_user_details').each(function() {
		// "caching"
		var $el = jQuery(this).offsetParent(".sbox_agent");
		var topPosition_useri = $el.position().top;
		var $el_this = jQuery(this);
		console.log(currentRowStart_useri);
		console.log(topPosition_useri);
		if (currentRowStart_useri != topPosition_useri) {

		// we just came to a new row. Set all the heights on the completed row
		for(currentDiv_useri = 0 ; currentDiv_useri < rowDivs_useri.length ; currentDiv_useri++) setConformingHeight_useri(rowDivs_useri[currentDiv_useri], currentTallest_useri);
		// set the variables for the new row
		rowDivs_useri.length = 0; // empty the array
		currentRowStart_useri = topPosition_useri;
		currentTallest_useri = getOriginalHeight_useri($el_this);
		rowDivs_useri.push($el_this);
		} else {
		// another div on the current row. Add it to the list and check if it's taller
		rowDivs_useri.push($el_this);
		currentTallest_useri = (currentTallest_useri < getOriginalHeight_useri($el_this)) ? (getOriginalHeight_useri($el_this)) : (currentTallest_useri);
		}
		});
		// do the last row
		for (currentDiv_useri = 0 ; currentDiv_useri < rowDivs_useri.length ; currentDiv_useri++) {
		setConformingHeight_useri(rowDivs_useri[currentDiv_useri], currentTallest_useri);
		}
	}

jQuery(document).ready(function(){

   
	columnConform();
	columnConform_wz();
	columnConform_useri(); 




	jQuery(".see-zip").live("click",function(){

			if(jQuery(this).text()=="+<?php echo JText::_('COM_USERPROF_PROF_SEEMORE') ?>"){
				jQuery(this).text("-<?php echo JText::_('COM_USERPROF_PROF_SEELESS') ?>");
				jQuery(this).siblings(".hidethis").css('display', 'block');
				jQuery(this).siblings().children(".hidethis").css('display', 'inline-block');
				jQuery(this).siblings().children("div").children(".span3").css('display', 'inline');
				
			} else {
				jQuery(this).text("+<?php echo JText::_('COM_USERPROF_PROF_SEEMORE') ?>");			
				jQuery(this).siblings(".hidethis").toggle();
				jQuery(this).siblings().children(".hidethis").toggle();
				jQuery(this).siblings().children("div").children(".span3").hide();
			
			}

		});

	 

	Landing.format();
	jQuery("#submitrefinesearch").click(function(){
		var x ="";
		jQuery('input[class=\'oldtext\']').each(function(){ x+=(jQuery(this).val()+","); })
		jQuery("#oldtext").val(x);
		jQuery(".breadbox").remove();
		jQuery("#loading-small").show();
		jQuery("#refine_form").submit();
	});
	jQuery("#submitrefinesearch2").click(function(){
		var x ="";
		jQuery('input[class=\'oldtext\']').each(function(){ x+=(jQuery(this).val()+","); })
		jQuery("#oldtext").val(x);
		jQuery(".breadbox").remove();
		jQuery("#loading-small").show();
		jQuery("#refine_form").submit();
	});
	jQuery("input[type='hidden']").each(function(){
		var hidden_val=jQuery(this).val();
		jQuery("input[type='checkbox']").each(function(){			
			var check_val=jQuery(this).val();
			if(hidden_val==check_val){
				//alert(jQuery(this).attr("checked","checked"));
				jQuery(this).attr("checked","checked")
			}
		});
	});
	jQuery(".facet-remove").click(function(){

			var hidden_val=jQuery(this).find("input[type='hidden']").val();
			jQuery("input[type='checkbox']").each(function(){			
				var check_val=jQuery(this).val();
				//alert(hidden_val);
				if(hidden_val==check_val){
					jQuery(this).removeAttr("checked");
				}
			});

		jQuery(this).parent().remove();
		jQuery("#loading-small").show();
		jQuery("#submitrefinesearch").click();
	});


	jQuery(".show-hide-refinement a").click(function(){
		if(jQuery(".refinement-options").attr("style") == "display: none;" || !jQuery(".refinement-options").attr("style")) {
			jQuery(".refinement-options").slideDown();
			//jQuery(".show-hide-refinement a").html("Refine Search");
		} else {
			jQuery(".refinement-options").slideUp();
			//jQuery(".show-hide-refinement a").html("Refine Search");
		}
	});
	
	jQuery( window ).resize(function() {
		if(jQuery(window).width() > 600) {
			jQuery(".refinement-options").attr("style", "display: block;");
			//jQuery(".show-hide-refinement a").html("Refine Search");
		} else {
			jQuery(".refinement-options").attr("style", "display: none;");
			//jQuery(".show-hide-refinement a").html("Refine Search");
		}
	});
	
	jQuery(".back-to-top").click(function(){
		jQuery(window).scrollTop(0);
	});
});

jQuery('.referral_row').infinitescroll({
	navSelector  : "#paginator",            
	nextSelector : "#paginator a.page_next",    
	itemSelector : ".referral_row" , 
	loading: {
		img   : "https://www.agentbridge.com/images/ajax_loader.gif",                
		msgText  : "",      
	 }        
  }, function(arrayOfNewElems){
	  
	columnConform();
	columnConform_wz();
	columnConform_useri(); 

	 if(!jQuery("ul.referral_row").last().html().trim().length) {
		 jQuery(window).unbind('.infscr');
	 }

  });

</script>

<style>
	#infscr-loading {
		display:table;
		float:left;
		height:30px;
		width:100%;
		text-align:center;
		margin:10px 0;
	}
	#infscr-loading img {
		height:30px;
	}
	
	ul#paginator li a:link,
	ul#paginator li a:visited,
	ul#paginator li a:hover,
	ul#paginator li a:active {
		color:#fff !important;
	}
</style>	