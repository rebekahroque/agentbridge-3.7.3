<?php


/**


 * @version     1.0.0

 * @package     com_customsearch
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com
 */
// no direct access
defined('_JEXEC') or die;


//Load admin language file

//$lang = JFactory::getLanguage();
//$lang->load('com_customsearch', JPATH_ADMINISTRATOR);

$map = array(
		"new"		=>	JText::_("COM_SEARCH_NEW"),
		"countries_name"	=> JText::_("COM_NRDS_FORMCOUNTRY"),
		"property_type"		=>	JText::_("COM_POPS_TERMS_POP_TYPE"),
		"sub_type"		=>	JText::_("COM_POPS_TERMS_POP_SUBTYPE"),
		"zone_name"		=>	JText::_("COM_NRDS_FORMSTATE"),
		"city"			=>	JText::_("COM_NRDS_FORMCITY"),
		"zip"			=>	JText::_("COM_NRDS_FORMZIP"),
		"bedroom"		=>	JText::_("COM_POPS_TERMS_BEDS"),
		"bathroom"		=>	JText::_("COM_POPS_TERMS_BATHS"),
		"view"			=>	JText::_("COM_POPS_TERMS_VIEW"),
		"bldg_sqft"		=>	JText::_("COM_POPS_TERMS_BLDG"),
		"unit_sqft"		=>	JText::_("COM_POPS_TERMS_UNITSQ"),
		"pet"			=>	JText::_("COM_POPS_TERMS_PET"),
		"furnished"		=>	JText::_("COM_POPS_TERMS_FURNISHED"),
		"possession"	=>	JText::_("COM_POPS_TERMS_POSSESSION"),
		"style"			=>	JText::_("COM_POPS_TERMS_STYLE"),
		"year_built"	=>	JText::_("COM_POPS_TERMS_YEAR_BUILT"),
		"pool_spa"		=>	JText::_("COM_POPS_TERMS_POOL_SPA"),
		"garage"		=>	JText::_("COM_POPS_TERMS_GARAGE"),
		"condition"		=>	JText::_("COM_POPS_TERMS_CONDITION"),
		"units"			=>	JText::_("COM_POPS_TERMS_UNIT"),
		"cap_rate"		=>	JText::_("COM_POPS_TERMS_CAP"),
		"grm"			=>	JText::_("COM_POPS_TERMS_GRM"),
		"occupancy"		=>	JText::_("COM_POPS_TERMS_OCCUPANCY"),
		"listing_class"	=>	JText::_("COM_POPS_TERMS_CLASS"),
		"parking_ratio"	=>	JText::_("COM_POPS_TERMS_PARKING_RATIO"),
		"ceiling_height"=>	JText::_("COM_POPS_TERMS_CEILING_HEIGHT"),
		"stories"		=>	JText::_("COM_POPS_TERMS_STORIES"),
		"room_count"	=>	JText::_("COM_POPS_TERMS_ROOM_COUNT"),
		"term"			=>	JText::_("COM_POPS_TERMS_TERM"),
		"type_lease"	=>	JText::_("COM_POPS_TERMS_TYPE_LEASE"),
		"type"			=>	JText::_("COM_POPS_TERMS_TYPE_LEASE"),
		"type_lease2"	=>	JText::_("COM_POPS_TERMS_CLASS"),
		"available_sqft"=> 	JText::_("COM_POPS_TERMS_AVAIL"),
		"lot_sqft"		=>	JText::_("COM_POPS_TERMS_LOT"),
		"lot_size"		=>	JText::_("COM_POPS_TERMS_LOT_SIZE"),
		"furnished"		=>	JText::_("COM_POPS_TERMS_FURNISHED"),
		"pet"			=>	JText::_("COM_POPS_TERMS_PET"),
		"bldg_type"		=>	JText::_("COM_POPS_TERMS_BLDG_TYPE"),
		"features1"		=>	JText::_("COM_POPS_TERMS_FEATURES"),
		"features2"		=>	JText::_("COM_POPS_TERMS_FEATURES"),
		"features3"		=>	JText::_("COM_POPS_TERMS_FEATURES"),
);
echo "<br />";
echo array_search( preg_grep("/^Bedroom.*/", $map), $map);

function get_pname($pid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('type_name')->from('#__property_type')->where('type_id = '.$pid);
			$db->setQuery($query);
			return $db->loadObject()->type_name;
		}

function get_sname($sid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('name')->from('#__property_sub_type')->where('sub_id = '.$sid);
			$db->setQuery($query);
			return $db->loadObject()->name;
	}

function sortArrayByArray(Array $array, Array $orderArray) {
    $ordered = array();
    foreach($orderArray as $key) {
        if(array_key_exists($key,$array)) {
            $ordered[$key] = $array[$key];
            unset($array[$key]);
        }
    }
    return $ordered + $array;
}

	$order_array = array ( 
						 "countries_name",
						 "property_type",
						 "sub_type",
						 "zone_name",
						 "city",
						 "zip",						 
						 "bedroom",
						 "bathroom",
						 "bldg_sqft",
						 "unit_sqft",
						 "view",
						 "style",
						 "pool_spa",
						 "condition",
						 "garage",
						 "features1",
						 "features2",
						 "features3",
						 "type",
						 "listing_class",
						 "type_lease",
						 "term",
						 "furnished",
						 "units",
						 "bldg_type",
						 "pet",
						 "zoned");

	$ordered_array=sortArrayByArray($this->refine,$order_array);
	
	
	$nb_elem_per_page = 21;
	
	$number_of_pages = intval($this->count/$nb_elem_per_page)+1;

function cmp($a, $b) {
	$arr_a = explode("-", $a);
	$arr_b = explode("-", $b);
	
	$_a = preg_replace("/[^0-9]/", "", $arr_a[0]);
	$_b = preg_replace("/[^0-9]/", "", $arr_b[0]);
	
	if($a == "Undisclosed") return 1;
	
	if($b == "Undisclosed")	return -1;
	
	if($_a == $_b) {
		return 0;
	}
	
	return $_a < $_b ? -1 : 1;
}

	//var_dump($this->proptypes);
	//var_dump($this->propsubtypes);
?>
<style>
.select2-match, .select2-result-label, #s2id_sort_items a span{
	font-size: 12px !important;
}
 #s2id_sort_items {
 	width:140px !important;
 }
</style>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.infinitescroll.js"/></script>
<div class="wrapper">
	<div class="wide left search-wrap">
		<!-- start wide-content -->
		<form id="refine_form" method="POST" action = "<?php echo JRoute::_('index.php?option=com_customsearch') ?>" name="refine_form">
			<h1><?php echo JText::_('COM_SEARCH_RESULT');?></h1>
			<div class="items-filter">
				<!-- start list-tile filter -->
				<span class="tile-number">
				<?php
						// only 1 result
						echo ( $this->count==1 ) ? $this->count." ".JText::_('COM_SEARCH_POPSFOUND')."." : ""; 
						// greater than one result
						echo ( $this->count>1  ) ? $this->count." ".JText::_('COM_SEARCH_POPSFOUND')."." : "";
						// no result
						echo ( $this->count==0 && $this->originaltext !== "" ) ? " ".JText::_('COM_SEARCH_NOPOPS')."." : ""; 		
						
						echo ( $this->count==0 && $this->originaltext == "" ) ? " ".JText::_('COM_SEARCH_NOAGENTS_EMPTYSEARCH')."" : "";
				?>
				<?php ?>
			</div>
			<!-- end list-tile filter -->
		
			<?php if( $this->count>0 ){ ?>
			<div class="search-left left">
					<?php if($this->order_by == "DESC"){
		    				$de_checked ="checked";		    			
		    		  } else {
		    		  		$de_checked ="";
		    		  }
		    	?>

				<!-- Start Refine Search -->
				<div class="refine-container">
					<h2><?php echo JText::_('COM_SEARCH_NEW')?></h2>
					<div style="padding-bottom: 20px;font-size: 12px;padding-left: 15px;padding-top: 5px;border-bottom: 1px solid #ababab;margin-bottom: 10px;">
								<input type="hidden" id="sort_items" name="sort_items" value="DESC" />
								<input type="checkbox" id="sort_items2" name="sort_items2" value="<?php echo $this->order_by ?>" <?php echo $de_checked?>/><span style="margin-left:10px"><?php echo JText::_('COM_SEARCH_RECENTPOPS')?></span>
							<!--	<select id="sort_items" name="sort_items">
									<option value="DESC" <?php echo $de_checked?>>Newest to Oldest</option>
									<option value="ASC"  <?php echo $as_checked?>>Oldest to Newest</option>
								</select>-->
					</div>
					<input type="hidden" name="jform[name]" value="refine_search" />
					<input type="hidden" name="task" value="searchlisting" />
					<input type="hidden" name="searchword" id="oldtext" value="<?php echo addslashes($this->originaltext); ?>" />
					<input type="hidden" name="refine" id="andtext" value="<?php echo $this->andtext; ?>" />
					<h2><?php echo JText::_('COM_SEARCH_REFINE');?></h2>
					<?php
						$search_keyword = "";
						foreach ($this->keys as $key){
							foreach($key as $key_inner => $inner){
								if(trim($inner)!=""){
								if($search_keyword == "") $search_keyword = $inner;
					?>
									<div class="breadbox_orig">
										<div class="facet-title"><?php echo $inner;?></div>
										<div class="facet-remove">
											<input type="hidden" class="oldtext" value="<?php echo $inner?>" />
										</div>
										<div class="clear-float"></div>
									</div>
									
					<?php 
								}
							}
						}
						
						if(isset($this->fields))
						foreach ($this->fields as $key => $temp){
							foreach($temp as $value){
								$values = explode("{-}", $value);
								if(count($values)>1 && !empty($value) && $value!=""){
									$column = str_replace("p.", "", $values[1]);
									$column = str_replace("z.", "", $column);
									$column = str_replace("zone_name", "", $column);
									$column = str_replace("pts.", "", $column);
									$column = str_replace("pt.", "", $column);
					?>
									<div class="breadbox">
										<?php if($column=="bathroom" || $column=="bedroom" || $column=="bathroom" ||$column=="units" || $column=="room") { ?>
											<div class="facet-title"><?php echo $values[0]." ".$column."s";?></div>
										<?php } else if ($column=="property_type"){ ?>
											<div class="facet-title"><?php echo JText::_(get_pname($values[0]));?></div>
										<?php } else if ($column=="sub_type"){ ?>
											<div class="facet-title"><?php echo JText::_(get_sname($values[0]));?></div>
										<?php } else { ?>
											<div class="facet-title"><?php echo $values[0];?></div>
										<?php } ?>
										<div class="facet-remove">
											<input type="hidden" value="<?php echo $value;?>" name="fields[<?php echo $key ?>][]" />
										</div>
									</div>
									<img id="loading-small" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; margin-left:90px; height:20px" />

					<?php 
								}
							}
						}
					

					?>
                    <div class="show-hide-refinement"><a href="javascript:void(0);">Refine Search</a></div>
					<div class="refinement-options">
					<input style="padding: 8px 15px 15px 15px;width: 150px;font-size: 12px;margin: 20px auto 0px;  display: block;"class="button gradient-green" type="button" id="submitrefinesearch" value="<?php echo JText::_('COM_SEARCH_REFINEBT');?>"/>
					

					<?php 
					//Merge the filters for features1, features2, and features3
					if(isset($ordered_array['features1']) || isset($ordered_array['features2']) || isset($ordered_array['features3'])) {
						if(isset($ordered_array['features2'])) {
							$ordered_array['features1'] = array_merge($ordered_array['features1'], $ordered_array['features2']);
							unset($ordered_array['features2']);
						}
						
						if(isset($ordered_array['features3'])) {
							$ordered_array['features1'] = array_merge($ordered_array['features1'], $ordered_array['features3']);
							unset($ordered_array['features3']);
						}						
					}
					
					$arr_usort = array('bldg_sqft', 'unit_sqft', 'lot_sqft', 'lot_size', 'available_sqft', 'units');
					foreach ($ordered_array as $key => $refinement) {
							$i=0;
							$temp = array_unique($refinement);

							if(in_array($key, $arr_usort)) {
								usort($temp, "cmp");					
							} else {
								asort($temp);
							}
												
							$counts = array_count_values($refinement);
							
							#COMBINE THE COUNT OF THE FOLLOWING:
							if($key == 'sub_type') {
								foreach($counts as $ckey => $cval) {
									#Residential Purchase and Lease SFR
									if($ckey == 5) {
										if(!isset($counts[1])) {
											$counts[1] = $cval;
										} else {
											$counts[1] = $counts[1] + $cval;
										}										
										unset($counts[$ckey]);
									}
									#Residential Purchase and Lease Condominium
									if($ckey == 6) {
										if(!isset($counts[2])) {
											$counts[2] = $cval;
										} else {
											$counts[2] = $counts[2] + $cval;
										}
										unset($counts[$ckey]);
									}
									#Residential Purchase and Lease Townhouse
									if($ckey == 7) {
										if(!isset($counts[3])) {
											$counts[3] = $cval;
										} else {
											$counts[3] = $counts[3] + $cval;
										}
										unset($counts[$ckey]);
									}
									#Residential Purchase and Lease Land
									if($ckey == 8) {
										if(!isset($counts[4])) {
											$counts[4] = $cval;
										} else {
											$counts[4] = $counts[4] + $cval;
										}
										unset($counts[$ckey]);
									}
									#Commercial Purchase and Lease Office
									if($ckey == 16) {
										if(!isset($counts[10])) {
											$counts[10] = $cval;
										} else {
											$counts[10] = $counts[10] + $cval;
										}
										unset($counts[$ckey]);
									}
									#Commercial Purchase and Lease Industrial
									if($ckey == 17) {
										if(!isset($counts[11])) {
											$counts[11] = $cval;
										} else {
											$counts[11] = $counts[11] + $cval;
										}
										unset($counts[$ckey]);
									}
									#Commercial Purchase and Lease Retail
									if($ckey == 18) {
										if(!isset($counts[12])) {
											$counts[12] = $cval;
										} else {
											$counts[12] = $counts[12] + $cval;
										}
										unset($counts[$ckey]);
									}
								}
								
							}
							
					?>
						<div class="search-summarize">
							<!-- Start Summarize -->
							<?php if(isset($map[$key])) { ?>
							<div class="search-summarize-title"><?php echo isset($map[$key]) ? $map[$key] : "";?></div>
							<div class="sub-nav">
								<ul>
									<?php $x = 0; $y = 1; $z = 0;?>
									<?php 
										$_sfr = false;
										$_condo = false;
										$_town = false;
										$_land = false;
										$_office = false;
										$_industrial = false;
										$_retail = false;
									?>
									<?php foreach($temp as $value) {
											if($key == "zoned") {
												switch($value) {
													case "1":
														$display = $value." ".JText::_('COM_POPS_TERMS_UNITONE');
														break;
													case "20":
														$display = $value."+ ".JText::_('COM_POPS_TERMS_UNIT');
														break;												
													default:
														$display = $value." ".JText::_('COM_POPS_TERMS_UNIT');
												}
											} else if($key == "lot_size") {
												switch($value) {
													case "Undisclosed":
														$display = JText::_('COM_POPS_TERMS_UNDISC');
														break;
													case "150,000":
														$display = $value."+";
														break;												
													default:
														$display = $value;
												}
											} else if($key == "unit_sqft") {
												switch($value) {
													case "Undisclosed":
														$display = JText::_('COM_POPS_TERMS_UNDISC');
														break;
													case "20,000":
														$display = $value."+";
														break;												
													default:
														$display = $value;
												}
											} else if($key == "lot_sqft") {
												switch($value) {
													case "Undisclosed":
														$display = JText::_('COM_POPS_TERMS_UNDISC');
														break;
													case "150,000":
														$display = $value."+";
														break;												
													default:
														$display = $value;
												}
											} else if($key == "bldg_type") {
												switch($value) {
													case "North Facing":
														$display = JText::_('COM_POPS_TERMS_NORTH');
														break;
													case "South Facing":
														$display = JText::_('COM_POPS_TERMS_SOUTH');
														break;
													case "West Facing":
														$display = JText::_('COM_POPS_TERMS_WEST');
														break;
													case "East Facing":
														$display = JText::_('COM_POPS_TERMS_EAST');
														break;
													case "Low Rise":
														$display = JText::_('COM_POPS_TERMS_LOWRISE');
														break;
													case "Mid Rise":
														$display = JText::_('COM_POPS_TERMS_MIDRISE');
														break;
													case "High Rise":
														$display = JText::_('COM_POPS_TERMS_HIGHRISE');
														break;
													case "Co-op":
														$display = JText::_('COM_POPS_TERMS_COOP');
														break;												
													default:
														$display = $value;
												}
											} else if($key == "condition") {
												switch($value) {
													case "Fixer":
														$display = JText::_('COM_POPS_TERMS_FIX');
														break;
													case "Good":
														$display =JText::_('COM_POPS_TERMS_GOOD');
														break;	
													case "Excellent":
														$display =JText::_('COM_POPS_TERMS_EXCELLENT');
														break;	
													case "Remodeled":
														$display =JText::_('COM_POPS_TERMS_REMODEL');
														break;	
													case "New Construction":
														$display =JText::_('COM_POPS_TERMS_NEWCONSTRUCT');
														break;
													case "New Construction":
														$display =JText::_('COM_POPS_TERMS_NEWCONSTRUCT');
														break;
													case "Undisclosed":
														$display =JText::_('COM_POPS_TERMS_UNDISC');
														break;
													default:
														$display = $value;
												}
											} else if($key == "style") {
												switch($value) {
													case "American Farmhouse":
														$display = JText::_('COM_POPS_TERMS_AMERICANFARM');
														break;
													case "Art Deco":
														$display = JText::_('COM_POPS_TERMS_ARTDECO');
														break;
													case "Art/Mid Century":
														$display = JText::_('COM_POPS_TERMS_MIDCENT');
														break;
													case "Cape Cod":
														$display = JText::_('COM_POPS_TERMS_CAPECOD');
														break;
													case "Colonial Revival":
														$display = JText::_('COM_POPS_TERMS_COLONIAL');
														break;
													case "Contemporary":
														$display = JText::_('COM_POPS_TERMS_CONTEMPORARY');
														break;
													case "Craftsman":
														$display = JText::_('COM_POPS_TERMS_CRAFTSMAN');
														break;
													case "French":
														$display = JText::_('COM_POPS_TERMS_FRENCH');
														break;
													case "Spanish/Mediterranean":
														$display = JText::_('COM_POPS_TERMS_SPANISH');
														break;
													case "Swiss Cottage":
														$display =JText::_('COM_POPS_TERMS_SWISS');
														break;	
													case "Tudor":
														$display =JText::_('COM_POPS_TERMS_TUDOR');
														break;	
													case "Victorian":
														$display =JText::_('COM_POPS_TERMS_VICTORIAN');
														break;	
													case "Historic":
														$display =JText::_('COM_POPS_TERMS_HISTORIC');
														break;
													case "Architecturally Significant":
														$display =JText::_('COM_POPS_TERMS_ARCHITECTURE');
														break;
													case "Green":
														$display =JText::_('COM_POPS_TERMS_GREEN');
														break;
													case "Undisclosed":
														$display =JText::_('COM_POPS_TERMS_UNDISC');
														break;
													default:
														$display = $value;
												}
											} else if($key == "garage") {
												switch($value) {
													case "8":
														$display =$value."+";
														break;
													default:
														$display = $value;
												}
											} else if($key == "year_built") {
												switch($value) {
													case "Undisclosed":
														$display =JText::_('COM_POPS_TERMS_UNDISC');
														break;
													default:
														$display = $value;
												}
											}  else if($key == "pool_spa") {
												switch($value) {
													case "None":
														$display =JText::_('COM_POPS_TERMS_NONE');
														break;
													case "Pool":
														$display =JText::_('COM_POPS_TERMS_POOL');
														break;
													case "Other":
														$display =JText::_('COM_POPS_TERMS_OTHER');
														break;
													case "Pool/Spa":
														$display =JText::_('COM_POPS_TERMS_POOL_SPA');
														break;
													default:
														$display = $value;
												}
											} else if($key == "features1" || $key == "features2" || $key == "features3") {
												switch($value) {
													case "One Story":
														$display =JText::_('COM_POPS_TERMS_ONESTORY');
														break;
													case "Two Story":
														$display =JText::_('COM_POPS_TERMS_TWOSTORY');
														break;
													case "Three Story":
														$display =JText::_('COM_POPS_TERMS_THREESTORY');
														break;
													case "Water Access":
														$display =JText::_('COM_POPS_TERMS_WATERACCESS');
														break;
													case "Horse Property":
														$display =JText::_('COM_POPS_TERMS_HORSE');
														break;
													case "Golf Course":
														$display =JText::_('COM_POPS_TERMS_GOLFCOURSE');
														break;
													case "Walkstreet":
														$display =JText::_('COM_POPS_TERMS_WALKSTREET');
														break;
													case "Media Room":
														$display =JText::_('COM_POPS_TERMS_MEDIA');
														break;
													case "Guest House":
														$display =JText::_('COM_POPS_TERMS_GUESTHOUSE');
														break;
													case "Wine Cellar":
														$display =JText::_('COM_POPS_TERMS_WINECELLAR');
														break;
													case "Tennis Court":
														$display =JText::_('COM_POPS_TERMS_TENNISCOURT');
														break;
													case "Den/Library":
														$display =JText::_('COM_POPS_TERMS_DENLIB');
														break;
													case "Green Const.":
														$display =JText::_('COM_POPS_TERMS_GREENCONSTRUCT');
														break;
													case "Basement":
														$display =JText::_('COM_POPS_TERMS_BASEMENT');
														break;
													case "RV/Boat Parking":
														$display =JText::_('COM_POPS_TERMS_RV');
														break;
													case "Senior":
														$display =JText::_('COM_POPS_TERMS_SENIOR');
														break;
													case "Sidewalks":
														$display =JText::_('COM_POPS_TERMS_SIDEWALKS');
														break;
													case "Utilites":
														$display =JText::_('COM_POPS_TERMS_UTILITIES');
														break;
													case "Curbs":
														$display =JText::_('COM_POPS_TERMS_CURBS');
														break;
													case "Horse Trails":
														$display =JText::_('COM_POPS_TERMS_HORSETRAILS');
														break;
													case "Rural":
														$display =JText::_('COM_POPS_TERMS_RURAL');
														break;
													case "Urban":
														$display =JText::_('COM_POPS_TERMS_URBAN');
														break;
													case "Suburban":
														$display =JText::_('COM_POPS_TERMS_SUBURBAN');
														break;
													case "Permits":
														$display =JText::_('COM_POPS_TERMS_PERMITS');
														break;
													case "HOA":
														$display =JText::_('COM_POPS_TERMS_HOA');
														break;
													case "Sewer":
														$display =JText::_('COM_POPS_TERMS_SEWER');
														break;
													case "CC&Rs":
														$display =JText::_('COM_POPS_TERMS_CCR');
														break;
													case "Coastal":
														$display =JText::_('COM_POPS_TERMS_COASTAL');
														break;
													default:
														$display = $value;
												}
											} else if($key == "view") {
												switch($value) {
													case "None":
														$display = JText::_('COM_POPS_TERMS_NONE');
														break;
													case "Panoramic":
														$display = JText::_('COM_POPS_TERMS_PANORAMIC');
														break;
													case "City":
														$display = JText::_('COM_POPS_TERMS_CITY');
														break;
													case "Mountains/Hills":
														$display = JText::_('COM_POPS_TERMS_MOUNT');
														break;
													case "Coastline":
														$display = JText::_('COM_POPS_TERMS_COAST');
														break;
													case "Water":
														$display = JText::_('COM_POPS_TERMS_WATER');
														break;
													case "Ocean":
														$display = JText::_('COM_POPS_TERMS_OCEAN');
														break;
													case "Lake/River":
														$display = JText::_('COM_POPS_TERMS_LAKE');
														break;
													case "Landmark":
														$display = JText::_('COM_POPS_TERMS_LANDMARK');
														break;
													case "Desert":
														$display = JText::_('COM_POPS_TERMS_DESERT');
														break;
													case "Bay":
														$display = JText::_('COM_POPS_TERMS_BAY');
														break;
													case "Vineyard":
														$display = JText::_('COM_POPS_TERMS_VINE');
														break;
													case "Golf":
														$display = JText::_('COM_POPS_TERMS_VINE');
														break;
													case "Other":
														$display = JText::_('COM_POPS_TERMS_VINE');
														break;
													default:
														$display = $value;
												}
											} else if($key == "bldg_sqft") {
												switch($value) {
													case "Undisclosed":
														$display = JText::_('COM_POPS_TERMS_UNDISC');
														break;
													case "20,000":
														$display = $value."+";
														break;
													default:
														$display = $value;
												}
											} else if($key == "listing_class") {
												switch($value) {
													case 4:
														$display = "A";
														break;
													case 3:
														$display = "B";
														break;
													case 2:
														$display = "C";
														break;
													case 1:
														$display = "D";
														break;
													default:
														$display = $value;
												}
											} else if ($key == "pet"){
												switch($value) {
													case 2:
														$display = JText::_('COM_POPS_TERMS_PETNO');
														break;
													case 1:
														$display = JText::_('COM_POPS_TERMS_PETYES');
														break;
													case 0:
														$display = JText::_('COM_POPS_TERMS_PETNO');
														break;
													}
											} else if ($key == "furnished"){
												switch($value) {
													case 1:
														$display = JText::_('COM_POPS_TERMS_PETYES');
														break;
													case 0:
														$display = JText::_('COM_POPS_TERMS_PETNO');
														break;
													}
											} else if ($key == "property_type"){

												foreach ($this->proptypes as $key_pt => $value_pt) {
													# code...
													if($value == $value_pt->type_id){
														$display = $value_pt->type_name;
														continue;
													}
												}
												/*switch($value) {
													case 1:
														$display = "Residential Purchase";
														break;
													case 2:
														$display = "Residential Lease";
														break;
													case 3:
														$display = "Commercial Purchase";
														break;
													case 4:
														$display = "Commercial Lease";
														break;
													}*/
											} else if ($key == "sub_type"){
												#Skip listing of the following:
												#Residential Lease SFR, Condo, Townhouse, and Land
												#Commercial Lease Office, Industrial, and Retail
												if($value == 1) {
													$_sfr = true;
												} else if($value == 2) {
													$_condo = true;
												} else if($value == 3) {
													$_town = true;
												} else if($value == 4) {
													$_land = true;
												} else if($value == 5) {
													if($_sfr === true) {
														continue;
													} else {
														$value = 1;
													}													
												} else if($value == 6) {
													if($_condo === true) {
														continue;
													} else {
														$value = 2;
													}
												} else if($value == 7) {
													if($_town === true) {
														continue;
													} else {
														$value = 3;
													}
												} else if($value == 8) {
													if($_land === true) {
														continue;
													} else {
														$value = 4;
													}
												} else if($value == 10) {
													$_office = true;
												} else if($value == 11) {
													$_industrial = true;
												} else if($value == 12) {
													$_retail = true;
												} else if($value == 16) {
													if($_office === true) {
														continue;
													} else {
														$value = 10;
													}
												} else if($value == 17) {
													if($_industrial === true) {
														continue;
													} else {
														$value = 11;
													}
												} else if($value == 18) {
													if($_retail === true) {
														continue;
													} else {
														$value = 12;
													}
												}
												
												foreach ($this->propsubtypes as $key_pt => $value_pt) {
													# code...
													if($value == $value_pt->sub_id){
														$display = $value_pt->name;
														continue;
													}
												}
												/*switch($value) {
													case 1:
														$display = "SFR";
														break;
													case 2:
														$display = "Condo";
														break;
													case 3:
														$display = "Townhouse/Row House";
														break;
													case 4:
														$display = "Land";
														break;
													case 5:
														$display = "SFR";
														break;
													case 6:
														$display = "Condo";
														break;
													case 7:
														$display = "Townhouse/Row House";
														break;
													case 8:
														$display = "Land";
														break;
													case 9:
														$display = "Multi-family";
														break;
													case 10:
														$display = "Office";
														break;
													case 11:
														$display = "Industrial";
														break;
													case 12:
														$display = "Retail";
														break;
													case 13:
														$display = "Motel/Hotel";
														break;
													case 14:
														$display = "Assisted Care";
														break;
													case 15:
														$display = "Special Purpose";
														break;
													case 16:
														$display = "Office";
														break;
													case 17:
														$display = "Industrial";
														break;
													case 18:
														$display = "Retail";
														break;
													}*/
											} else {
												$display = $value;
											}
									?>
											<?php 
												if($key=="zone_name"){
													$table="z";
												} else if($key=="countries_name"){
													$table="country";
												} else {
													$table="p";
												}
											?>
											<?php if($x == 0) { ?>
												<?php $style = ($y > 1) ? "style='display:none;'" : ""; ?>
												<?php echo "<div class='more_" . $key . "_" . $y . "' ". $style .">"; ?>
											<?php } ?>
											<li>
                                        
												<input class="left" id="<?php echo $key.$i;?>" type="checkbox" value="<?php echo $value."{-}".$table.".".$key?>" name="fields[<?php echo $key?>][]">
                                                <label class="clear-both" style="width:170px" for="<?php echo $key.$i++;?>"><?php echo stripslashes_all($display);?><span> (<?php echo $counts[$value]?>)</span></label>
											</li>
											<div class="clear-float"></div>
											<?php 
												if($x < 10) { 
													$x++;
												} else { 
													$x = 0;
													$y++;
													echo "</div>";
												}
												$z++;
											?>                                           
									<?php } ?>
									<?php echo ($x < 10 && $x > 0) ? "</div>" : ""; ?>
									<?php 
										if ($z > 11) {
											echo "
												<div id='more_" . $key . "_options' data-key='" . $key . "' style='color:#007bae;font-size:12px;cursor:pointer;margin-top:10px;margin-left:-10px;'>+ " . JText::_('COM_SEARCH_MORE_OPTIONS') . "</div>
												<script type='text/javascript'>
													var " . $key . "_ctr = 2;
													jQuery('#more_" . $key . "_options').click(function(){
														jQuery('.more_" . $key . "_' + " . $key . "_ctr).removeAttr('style');
														" . $key . "_ctr+=1;
														if(!jQuery('.more_" . $key . "_' + " . $key . "_ctr).length) {
															jQuery(this).attr('style', 'display:none;');
														}
													});
												</script>											
											"; 
										} else {
											echo "";
										}
									?>
								</ul>
								
							</div>
							<?php } ?>
						</div>
						<div class="clear-float"></div>
					<?php }
					?>
				<input style="padding: 8px 15px 15px 15px;width: 150px;font-size: 12px;margin: 20px auto 0px;  display: block;"class="button gradient-green" type="button" id="submitrefinesearch2" value="<?php echo JText::_('COM_SEARCH_REFINEBT');?>"/>
				</div>
				</div>
			
		</div>
		<?php } ?>
		<!-- End Refine Search -->
		    <div class="search-right left">
			</form>
			<div style="clear:both"></div>
			<div class="items-container">
				
				<ul class="items">
					<?php if($this->items!=null) foreach($this->items as $item){
							//$images = explode(",",$item->images);
							$images[0] = $item->images;
							$bw_image = false;
							$ts1 = strtotime(date('Y-m-d'));
							$ts2 = strtotime($item->date_expired);
							$seconds_diff = $ts2 - $ts1;
							$days = floor($seconds_diff/3600/24);
							$expiration = $days;
							$message = 	"";
							$property_type = $item->property_type;
							$sub_type = $item->sub_type;

							$message = $item->pops_feats['initial_feats'];
							

							$userinfos = $this->userDetails;
							$flagicon="";
							if($userinfos->country!=$item->country){
								if($item->country == 0 && $userinfos->country!=223){
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/us-flag-lang.png">';
								} elseif($item->country != 0) {
									$flagicon='<img class="ctry-flag" src="'.$this->baseurl.'/templates/agentbridge/images/'.strtolower($item->countryDetails).'-flag-lang.png">';
								}								
							}
							//Logged-in user's POPs&trade;
							if (JFactory::getUser()->id == $item->user_id) {
								$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
								$priceb 	= format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '');
								$image 		= ($bw_image) ? $item->images_bw : $images[0];
								$zip   		= $item->zip;
								$name 		= stripslashes_all($item->property_name);
								$ts1 = strtotime(date('Y-m-d'));
								$ts2 = strtotime($item->date_expired);
								$seconds_diff = $ts2 - $ts1;
								$days = floor($seconds_diff/3600/24);
								echo '<li>
											<div class="items-inner-container">
												<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
												<div class="description-placeholder desc-below-photo" style="height:100px">												
													<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
													<span class="search-pname">'.$name.'</span>
													<p class="reality-description">'.$message.'</p>
												</div>
											</div>
											<div class="other-description-placeholder"><span class="price" style="height:40px">'.$priceb.' '.$item->currency.'</span></div>
											<div style="width:223px"> 
												<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'">My POPs&trade;</a></div>
												<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
												</div>
											<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
												'.JText::_('COM_POPS_TERMS_EXPIRESIN').'
												<a 
												id="item'.$item->listing_id.'"
												class="text-link expiry"
												style="font-size:11px"
												onclick="update('.$item->listing_id.')"> 
												'.$days.'
												</a> 
												days
											</div>
										</li>';
							
							//Other user's POPs&trade
							} else {	
								#if(in_array(JFactory::getUser()->id, $item->user_network)) {
										if( $item->setting == 1) {
											if($item->selected_permission == 7) {
												if($this->user_state === $item->user_state) {
													//Approved Request for Private POP&trade; 
													if (in_array(JFactory::getUser()->id, explode(",", $item->allowed))){
														$images = explode(",",$item->images);
														$bw_image = false;
														$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
														
														if($item->disclose == 1) {
															$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
														} else {
														$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
															}
															
														$name 		= stripslashes_all($item->property_name);
														
														echo '<li>
																<div class="items-inner-container">
																	<div class="image-placeholder" ><img style="width:212px" src="'.$images[0].'" /></div>
																	<div class="description-placeholder desc-below-photo" style="height:100px">
																		<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>  
																		<span class="search-pname">'.$name.'</span>
																		<p class="reality-description">'.$message.'</p>
																	</div>
																</div>
																<div class="other-description-placeholder">'.$price.'</div>
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." days" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
															</li>';
													}
													//Pending Request for Private POPs&trade;
													else if (in_array(JFactory::getUser()->id, explode(",", $item->requests))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "<div class=\"left button search-req-pending\" style=\"margin-right: 20px;margin-left:20px;width:180px;padding: 8px;margin-bottom:10px;\">".JText::_('COM_REFERRAL_TERMS_PEND')."</div><div style=\"clear:both\"></div>";
														
														echo '<li>
																<div class="items-inner-container">
																<div class="image-placeholder" ><img style="width:212px" src="'.$item->images_bw.'" /></div>
																<div class="description-placeholder desc-below-photo" style="height:100px">
																	<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
																	<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder"><span>'.$price.'</span></div>	
															<div style="width:223px"> 
															<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
															<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
															</div>
															<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
															</div>
															
														  </li>' ;
													//Declined Request for Private POPs&trade;
													} else if (in_array(JFactory::getUser()->id, explode(",", $item->denied))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; padding: 5px; margin-bottom:10px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5.5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													//No Request for Private POPs&trade;
													} else {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; margin-bottom: 10px; padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													}
													
												} else {
													//price undisclosed or disclosed for public POPs&trade
													if($item->disclose == 1) {
													$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') . ' '.$this->user_currency."</span>";
														} else {
													$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
														}
														
													$image 		= ($bw_image) ? $item->images_bw : $images[0];
													$zip   		= $item->zip;
													$name 		= stripslashes_all($item->property_name);
													$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
													
													echo '<li>
														<div class="items-inner-container">
															<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
																<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
																<span class="search-pname">'.$name.'</span>
																<p class="reality-description">'.$message.'</p>
															</div>
														</div>
														<div class="other-description-placeholder">'.$price.'</div>
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
														</li>';	
												}
																				
											} else if($item->selected_permission == 3) {
												if($item->volume_private === 1) {
													//Approved Request for Private POP&trade; 
													if (in_array(JFactory::getUser()->id, explode(",", $item->allowed))){
														$images = explode(",",$item->images);
														$bw_image = false;
														$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
														
														if($item->disclose == 1) {
															$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
														} else {
														$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
															}
														
														$name 		= stripslashes_all($item->property_name);
														
														echo '<li>
																<div class="items-inner-container">
																	<div class="image-placeholder" ><img style="width:212px" src="'.$images[0].'" /></div>
																	<div class="description-placeholder desc-below-photo" style="height:100px">
																		<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>  
																		<span class="search-pname">'.$name.'</span>
																		<p class="reality-description">'.$message.'</p>
																	</div>
																</div>
																<div class="other-description-placeholder">'.$price.'</div>
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
															</li>';
													}
													//Pending Request for Private POPs&trade;
													else if (in_array(JFactory::getUser()->id, explode(",", $item->requests))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "<div class=\"req-search-pending\">".JText::_('COM_REFERRAL_TERMS_PEND')."</div><div style=\"clear:both\"></div>";
														
														echo '<li>
																<div class="items-inner-container">
																<div class="image-placeholder" ><img style="width:212px" src="'.$item->images_bw.'" /></div>
																<div class="description-placeholder desc-below-photo" style="height:100px">
																	<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
																	<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder"><span>'.$price.'</span></div>	
															<div style="width:223px"> 
															<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
															<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
															</div>
															<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
															</div>
															
														  </li>' ;
													//Declined Request for Private POPs&trade;
													} else if (in_array(JFactory::getUser()->id, explode(",", $item->denied))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; padding: 5px; margin-bottom:10px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5.5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													//No Request for Private POPs&trade;
													} else {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; margin-bottom: 10px; padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													}
												} else {
													//price undisclosed or disclosed for public POPs&trade
													if($item->disclose == 1) {
													$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') . ' '.$this->user_currency."</span>";
														} else {
													$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
														}
														
													$image 		= ($bw_image) ? $item->images_bw : $images[0];
													$zip   		= $item->zip;
													$name 		= stripslashes_all($item->property_name);
													$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
													
													echo '<li>
														<div class="items-inner-container">
															<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
																<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
																<span class="search-pname">'.$name.'</span>
																<p class="reality-description">'.$message.'</p>
															</div>
														</div>
														<div class="other-description-placeholder">'.$price.'</div>
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
														</li>';	
												}
											} else if($item->selected_permission == 4) {
												if($item->ave_price_private === 1) {
													//Approved Request for Private POP&trade; 
													if (in_array(JFactory::getUser()->id, explode(",", $item->allowed))){
														$images = explode(",",$item->images);
														$bw_image = false;
														$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
														
														if($item->disclose == 1) {
															$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
														} else {
														$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
															}
															
														$name 		= stripslashes_all($item->property_name);
															
														echo '<li>
																<div class="items-inner-container">
																	<div class="image-placeholder" ><img style="width:212px" src="'.$images[0].'" /></div>
																	<div class="description-placeholder desc-below-photo" style="height:100px">
																		<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>  
																		<span class="search-pname">'.$name.'</span>
																		<p class="reality-description">'.$message.'</p>
																	</div>
																</div>
																<div class="other-description-placeholder">'.$price.'</div>
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
															</li>';
													}
													//Pending Request for Private POPs&trade;
													else if (in_array(JFactory::getUser()->id, explode(",", $item->requests))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "<div class=\"left button search-req-pending\" style=\"margin-right: 20px;margin-left:20px;width:180px;padding: 8px;margin-bottom:10px;\">".JText::_('COM_REFERRAL_TERMS_PEND')."</div><div style=\"clear:both\"></div>";
														
														echo '<li>
																<div class="items-inner-container">
																<div class="image-placeholder" ><img style="width:212px" src="'.$item->images_bw.'" /></div>
																<div class="description-placeholder desc-below-photo" style="height:100px">
																	<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
																	<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder"><span>'.$price.'</span></div>	
															<div style="width:223px"> 
															<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
															<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
															</div>
															<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
															</div>
															
														  </li>' ;
													//Declined Request for Private POPs&trade;
													} else if (in_array(JFactory::getUser()->id, explode(",", $item->denied))) {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; padding: 5px; margin-bottom:10px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5.5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													//No Request for Private POPs&trade;
													} else {
														$bw_image = true;
														$link = "javascript:void(0);";
														$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
														$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
														$price = "
															<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; margin-bottom: 10px;padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
															<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5px; display:none;\" />
															<div style=\"clear:both\"></div>
															";
														echo '<li>
															<div class="items-inner-container">
															<div class="image-placeholder" >
															<img style="width:212px" src="'.$item->images_bw.'" />
															</div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
																</div>
															</div>
															<div class="other-description-placeholder">'.$price.'
																<div style="width:223px"> 
																<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
																<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
																</div>
																<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
																<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
																</div>
															</div>		
														  </li>' ;
													}
												} else {
													//price undisclosed or disclosed for public POPs&trade
													if($item->disclose == 1) {
													$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') . ' '.$this->user_currency."</span>";
														} else {
													$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
														}
														
													$image 		= ($bw_image) ? $item->images_bw : $images[0];
													$zip   		= $item->zip;
													$name 		= stripslashes_all($item->property_name);
													$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
													
													echo '<li>
														<div class="items-inner-container">
															<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
																<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
																<span class="search-pname">'.$name.'</span>
																<p class="reality-description">'.$message.'</p>
															</div>
														</div>
														<div class="other-description-placeholder">'.$price.'</div>
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
														</li>';	
												}
											} else {
												//price undisclosed or disclosed for public POPs&trade
												if($item->disclose == 1) {
												$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') . ' '.$this->user_currency."</span>";
													} else {
												$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
													}
													
												$image 		= ($bw_image) ? $item->images_bw : $images[0];
												$zip   		= $item->zip;
												$name 		= stripslashes_all($item->property_name);
												$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
												
												echo '<li>
													<div class="items-inner-container">
														<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
														<div class="description-placeholder desc-below-photo" style="height:100px">
															<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
															<span class="search-pname">'.$name.'</span>
															<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'</div>
													<div style="width:223px"> 
													<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
													<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
													</div>
													<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
													</li>';
											}
										} else {
											//Approved Request for Private POP&trade; 
											if (in_array(JFactory::getUser()->id, explode(",", $item->allowed))){
												$images = explode(",",$item->images);
												$bw_image = false;
												$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
												
												if($item->disclose == 1) {
													$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
												} else {
												$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
													}
												
												$name 		= stripslashes_all($item->property_name);
												
												echo '<li>
														<div class="items-inner-container">
															<div class="image-placeholder" ><img style="width:212px" src="'.$images[0].'" /></div>
															<div class="description-placeholder desc-below-photo" style="height:100px">
																<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>  
																<span class="search-pname">'.$name.'</span>
																<p class="reality-description">'.$message.'</p>
															</div>
														</div>
														<div class="other-description-placeholder">'.$price.'</div>
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span></div>
													</li>';
											}
											//Pending Request for Private POPs&trade;
											else if (in_array(JFactory::getUser()->id, explode(",", $item->requests))) {
												$bw_image = true;
												$link = "javascript:void(0);";
												$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
												$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
												$price = "<div class=\"left button search-req-pending\" style=\"margin-right: 20px;margin-left:20px;width:180px;padding: 8px;margin-bottom:10px;\">".JText::_('COM_REFERRAL_TERMS_PEND')."</div><div style=\"clear:both\"></div>";
												
												echo '<li>
														<div class="items-inner-container">
														<div class="image-placeholder" ><img style="width:212px" src="'.$item->images_bw.'" /></div>
														<div class="description-placeholder desc-below-photo" style="height:100px">
															<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
															<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder"><span>'.$price.'</span></div>	
													<div style="width:223px"> 
													<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
													<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
													</div>
													<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
														<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
													</div>
													
												  </li>' ;
											//Declined Request for Private POPs&trade;
											} else if (in_array(JFactory::getUser()->id, explode(",", $item->denied))) {
												$bw_image = true;
												$link = "javascript:void(0);";
												$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
												$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
												$price = "
													<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; padding: 5px; margin-bottom:10px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
													<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5.5px; display:none;\" />
													<div style=\"clear:both\"></div>
													";
												echo '<li>
													<div class="items-inner-container">
													<div class="image-placeholder" >
													<img style="width:212px" src="'.$item->images_bw.'" />
													</div>
													<div class="description-placeholder desc-below-photo" style="height:100px">
													<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
													<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
														<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
														</div>
													</div>		
												  </li>' ;
											//No Request for Private POPs&trade;
											} else {
												$bw_image = true;
												$link = "javascript:void(0);";
												$r2 = "<a style='font-size:11px' href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>".stripslashes_all(JFactory::getUser($item->user_id)->name)."</a>";
												$message = JText::sprintf('COM_NETWORK_TERMS_PRIV', $r2);
												$price = "
													<a href=\"javascript:void(0);\" onclick=\"requestAccess(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 20px;margin-left:20px; width:180px; margin-bottom: 10px; padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
													<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:95px; padding: 5px; display:none;\" />
													<div style=\"clear:both\"></div>
													";
												echo '<li>
													<div class="items-inner-container">
													<div class="image-placeholder" >
													<img style="width:212px" src="'.$item->images_bw.'" />
													</div>
													<div class="description-placeholder desc-below-photo" style="height:100px">
													<div class="private-pops-label">'.$flagicon.' '.$item->zip.'</div>
													<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
														<span class="right">'.(($days>0) ? "".JText::_('COM_POPS_TERMS_EXPIRESIN')." ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "".JText::_('COM_POPS_TERMS_EXPIRED')."" ).'</span>
														</div>
													</div>		
												  </li>' ;
											}
											
										
										}
								//Pending Request for Outside of Network
								/*
								} else if (in_array(JFactory::getUser()->id, $item->user_network_pending)) {

										//var_dump($item->selected_permission); 

										if($item->selected_permission==1 && $item->setting!=2) {
										
											$image 		= ($bw_image) ? $item->images_bw : $images[0];
											$zip   		= $item->zip;
											$images = explode(",",$item->images);
											$bw_image = false;
											$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
											$name 		= stripslashes_all($item->property_name);
											$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
			
											if($item->disclose == 1) {
											$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
											} else {
											$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
												}
													
											echo '<li>
													<div class="items-inner-container">
														<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
														<div class="description-placeholder desc-below-photo" style="height:100px">
															<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
															<span class="search-pname">'.$name.'</span>
															<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'</div>
													<div style="width:223px"> 
													<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
													<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
													</div>
													<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "Expires in ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "Expired" ).'</span></div>
												</li>';

										} else {

											$bw_image = true;
											$link = "javascript:void(0);";
											$message = "<a style='font-size:11px'  href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>". stripslashes_all(JFactory::getUser($item->user_id)->name)."</a> has restricted this POPs&#8482; to Network members only.";
											$price = "<a href=\"javascript:void(0);\" class=\"left button gradient-blue\" style=\"margin-right: 20px;margin-left:20px; width:180px; padding: 5px; margin-bottom:10px\">".JText::_('COM_REFERRAL_TERMS_PEND')."</a><div style=\"clear:both\"></div>";
											echo '<li>
														<div class="items-inner-container">
														<div class="image-placeholder" >
														<img style="width:212px" src="'.$item->images_bw.'" />
														</div>
														<div class="description-placeholder desc-below-photo" style="height:100px">
														<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>
														<p class="reality-description">'.$message.'</p>
															</div>
														</div>
														<div class="other-description-placeholder">'.$price.'
															<div style="width:223px"> 
															<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
															<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
															</div>
															<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
															<span class="right">'.(($days>0) ? "Expires in ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "Expired" ).'</span>
															</div>
														</div>		
													</li>' ;
										}

								//Declined Request for Outside of Network	
								} else if (in_array(JFactory::getUser()->id, $item->user_network_declined)) {
										$bw_image = true;
										$link = "javascript:void(0);";
										$message = "<a style='font-size:11px'  href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>". stripslashes_all(JFactory::getUser($item->user_id)->name)."</a> has restricted this POPs&#8482; to Network members only.";
										$price = "
										<a href=\"javascript:void(0);\" onclick=\"requestNetwork(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:35px; padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
										<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 20px;margin-left:95px; padding: 5px; display:none;\" />
										<div style=\"clear:both\"></div>
										";		
										echo '<li>
													<div class="items-inner-container">
													<div class="image-placeholder" >
													<img style="width:212px" src="'.$item->images_bw.'" />
													</div>
													<div class="description-placeholder desc-below-photo" style="height:100px">
													<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>
													<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
														<span class="right">'.(($days>0) ? "Expires in ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "Expired" ).'</span>
														</div>
													</div>		
												  </li>' ;
									//No Request for Outside of Network	
									} else {
										//new for public pops
										if($item->selected_permission==1 && $item->setting!=2) {
										
											$image 		= ($bw_image) ? $item->images_bw : $images[0];
											$zip   		= $item->zip;
											$images = explode(",",$item->images);
											$bw_image = false;
											$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
											$name 		= stripslashes_all($item->property_name);
											$link = JRoute::_('index.php?option=com_propertylisting&task=individual&lID='.$item->listing_id);
			
											if($item->disclose == 1) {
											$price = "<span class=\"price\" style=\"height:40px\">" . format_currency_global($item->price1,$item->symbol).((!empty($item->price2)) ? ' - '.format_currency_global($item->price2,$item->symbol) : '') .' '.$this->user_currency. "</span>";
											} else {
											$price = "<span class=\"price\" style=\"height:40px\">price undisclosed</span>";
												}
													
											echo '<li>
													<div class="items-inner-container">
														<div class="image-placeholder" ><img style="width:212px" src="'.$image.'" /></div>
														<div class="description-placeholder desc-below-photo" style="height:100px">
															<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$zip.'</a>  
															<span class="search-pname">'.$name.'</span>
															<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'</div>
													<div style="width:223px"> 
													<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
													<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
													</div>
													<div class="clear-float seen-expire" style="padding:5px; margin-top:45px"><span class="right">'.(($days>0) ? "Expires in ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "Expired" ).'</span></div>
												</li>';
										} else {

										$bw_image = true;
										$link = "javascript:void(0);";
										$message = "<a style='font-size:11px'  href='".JRoute::_("index.php?option=com_userprofile&task=profile")."&uid=".$item->user_id."'>". stripslashes_all(JFactory::getUser($item->user_id)->name)."</a> has restricted this POPs&#8482; to Network members only.";
										$price = "
										<a href=\"javascript:void(0);\" onclick=\"requestNetwork(" . $item->user_id . ", " . $item->listing_id . ");\" class=\"left button gradient-blue ra" . $item->user_id . $item->listing_id . "\" style=\"margin-right: 5px; margin-bottom: 10px;margin-left:35px; padding: 5px\">".JText::_('COM_NETWORK_TERMS_REQ2VIEW')."</a>
										<img height=\"30px\" class=\"left loading" . $item->user_id . $item->listing_id . "\" alt=\"Loading...\" src=\"https://www.agentbridge.com/images/ajax_loader.gif\" id=\"loading-image_custom_question1\" style=\"margin-right: 5px; margin-bottom: 20px;margin-left:95px; padding: 5px; display:none;\" />
										<div style=\"clear:both\"></div>
										";		
										echo '<li>
													<div class="items-inner-container">
													<div class="image-placeholder" >
													<img style="width:212px" src="'.$item->images_bw.'" />
													</div>
													<div class="description-placeholder desc-below-photo" style="height:100px">
													<a href="'.$link.'" class="item-number text-link">'.$flagicon.' '.$item->zip.'</a>
													<p class="reality-description">'.$message.'</p>
														</div>
													</div>
													<div class="other-description-placeholder">'.$price.'
														<div style="width:223px"> 
														<div class="left text-link" style="padding-left:5px" ><a style="font-size:11px" href="'.JRoute::_("index.php?option=com_userprofile&task=profile").'&uid='.$item->user_id.'">'.stripslashes_all($item->username).'</a></div>
														<div class="right">'.(($item->v2012==1 && $item->v2013==1) ? "<img width='80px' src='".$this->baseurl."/images/agent-bridge-verified.jpg' style='padding-right:15px; margin-top:-5px'>" : "" ).'</div>
														</div>
														<div class="clear-float seen-expire" style="padding:5px; margin-top:45px">
														<span class="right">'.(($days>0) ? "Expires in ".$days." ".JText::_('COM_POPS_TERMS_DAYS')."" : "Expired" ).'</span>
														</div>
													</div>		
													</li>' ;
												}

									}
								*/
							}
						} 
								
						?>
	
				</ul>
				<ul id='paginator' style="position:relative;float:left;">
					<?php if(isset($_GET['page']) && $_GET['page']>1){?>
						<li><a class="page_prev" href='./task=searchlisting&page=<?php echo $_GET['page']-1?>&<?php echo $this->query_string; ?>'><< Prev </a>|</li>
					<?php }?>				
					<?php for($i=1;$i<$number_of_pages;$i++){?>
						<li><a class="page_number" href='./?task=searchlisting&page=<?php echo $i?>&<?php echo $this->query_string; ?>'><?php echo $i?></a></li>
					<?php }?>
					<?php if(isset($_GET['page']) && $_GET['page']<($number_of_pages-1)){?>
						<li> |<a class="page_next" href='./?task=searchlisting&page=<?php echo $_GET['page']+1?>&<?php echo $this->query_string; ?>'> Next>></a></li>
					<?php } else if($number_of_pages>1) {?>
						<li> |<a class="page_next" href='./?task=searchlisting&page=2&<?php echo $this->query_string; ?>'> Next>></a></li>
					<?php }?>
				</ul>
			</div>
		</div>
	</div>
	<div id="edittext" style="display: none">
			 <span>Please select new date of expiration</span><br /> 
             
             <div style="width: 220px">
             <input style="margin-top:10px; height:30px" type="text" id="userinput" class="textbox-up left" style="width:180px" /> 
             <input type="hidden" id="editid" /> 
             <input class="button gradient-green right" style="margin-top:10px; padding-top:7px; height:30px; width:80px" type="button" value="Submit" id="changevalue"/>
			</div>
		
			<div id="expmsg" class="error_msg" style="display:none; padding-top:50px">You have successfully updated this POPs&trade; expiry.</div>
			<div id="experr" class="error_msg" style="display:none; padding-top:50px">You cannot set the expiry to today's date or a past date.</div>
	</div>
	<!-- end wide-content -->
</div>
<a href="javascript:void(0);" class="button gradient-blue back-to-top">Back To Top</a>
<script>
<?php if(!isset($_GET['page']) || $_GET['page'] < 2){?>
mixpanel.track("Search POPs - <?php echo $search_keyword; ?>");
<?php } ?>

function update(lid){
		jQuery( "#editid" ).val(lid);
		jQuery( "#edittext" ).dialog(
			{
				modal: true,
				title: "Change Expiration Date",
				}
			);
	}
function setPage(page){
	jQuery("input[name=\"page\"]").val(page);
}
jQuery(document).ready(function(){

	Landing.format();
	
	jQuery("#userinput").datepicker();

	jQuery("#submitrefinesearch").click(function(){
		var x ="";
		jQuery('input[class=\'oldtext\']').each(function(){ x+=(jQuery(this).val()+" "); })
		jQuery("#oldtext").val(x);
		jQuery(".breadbox").remove();
		jQuery(".breadbox_orig").remove();
		jQuery("#loading-small").show();
		jQuery("#refine_form").submit();
	});
	jQuery("#submitrefinesearch2").click(function(){
		var x ="";
		jQuery('input[class=\'oldtext\']').each(function(){ x+=(jQuery(this).val()+" "); })
		jQuery("#oldtext").val(x);
		jQuery(".breadbox").remove();
		jQuery(".breadbox_orig").remove();
		jQuery("#loading-small").show();
		jQuery("#refine_form").submit();
	});

	jQuery(".facet-remove").click(function(){

			var hidden_val=jQuery(this).find("input[type='hidden']").val();
			jQuery("input[type='checkbox']").each(function(){			
				var check_val=jQuery(this).val();
				//alert(hidden_val);
				if(hidden_val==check_val){
					
					jQuery(this).removeAttr("checked");
				}
			});

		jQuery(this).parent().remove();
		jQuery("#loading-small").show();
		jQuery("#submitrefinesearch").click();
	});


	jQuery("input[type='hidden']").each(function(){
		var hidden_val=jQuery(this).val();
		jQuery("input[type='checkbox']").each(function(){			
			var check_val=jQuery(this).val();
			if(hidden_val==check_val){
				//alert(jQuery(this).attr("checked","checked"));
				jQuery(this).attr("checked","checked")
			}
		});
	});

	jQuery("#sort_items2").change(function(){

		if(jQuery('input[name="sort_items2"]:checked').length > 0 ){
			jQuery('#sort_items').val("DESC");
		} else {
			jQuery("#sort_items").val("ASC");
		}
		var x ="";
		jQuery('input[class=\'oldtext\']').each(function(){ x+=(jQuery(this).val()+" "); })
		jQuery("#oldtext").val(x);
		jQuery(".breadbox").remove();
		jQuery(".breadbox_orig").remove();
		jQuery("#loading-small").show();
		jQuery("#refine_form").submit();
	});

	jQuery("input[type=checkbox][name='fields[zip][]']").each(function(){
		var checkbox_val=jQuery(this).val();
		jQuery("input[type='hidden']").each(function(){			
			var hidden_val=jQuery(this).val();
			if(hidden_val!=checkbox_val){
				//alert(jQuery(this).attr("checked","checked"));breadbox
				jQuery(this).parent("breadbox").remove();
			}
		});
	});

	jQuery("input[type=checkbox][name='fields[zip][]']").each(function(){
		var checkbox_val=jQuery(this).val();
		jQuery("input[type='hidden']").each(function(){			
			var hidden_val=jQuery(this).val();
			if(hidden_val!=checkbox_val){
				//alert(jQuery(this).attr("checked","checked"));breadbox
				jQuery(this).parent("breadbox").remove();
			}
		});
	});
	
	jQuery(".show-hide-refinement a").click(function(){
		if(jQuery(".refinement-options").attr("style") == "display: none;" || !jQuery(".refinement-options").attr("style")) {
			jQuery(".refinement-options").slideDown();
			//jQuery(".show-hide-refinement a").html("Hide Refinement Options");
		} else {
			jQuery(".refinement-options").slideUp();
			//jQuery(".show-hide-refinement a").html("Show Refinement Options");
		}
	});
	
	jQuery( window ).resize(function() {
		if(jQuery(window).width() > 600) {
			jQuery(".refinement-options").attr("style", "display: block;");
			//jQuery(".show-hide-refinement a").html("Hide Refinement Options");
		} else {
			jQuery(".refinement-options").attr("style", "display: none;");
			//jQuery(".show-hide-refinement a").html("Show Refinement Options");
		}
	});
	
	jQuery(".back-to-top").click(function(){
		jQuery(window).scrollTop(0);
	});

});
function requestAccess(uid, pid){
	jQuery(".ra"+uid+""+""+pid).hide();
	jQuery(".loading"+uid+""+pid).show();
	jQuery.ajax({
		type: "POST",
		url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=requestaccess') ?>',
		data: {'pid': pid, 'uid': uid},
		success: function(data){

			jQuery(".loading"+uid+""+pid).hide();	
			jQuery(".ra"+uid+""+pid).after("<div class=\"left button search-req-pending\" style=\"margin-right: 20px;margin-left:20px;width:180px;padding: 8px;margin-bottom:10px;\"><?php echo JText::_('COM_REFERRAL_TERMS_PEND')?></div>");
			jQuery(".ra"+uid+""+pid).parent().find(".seen-expire").css("margin-top", "45px")
		}
	});
}
function requestNetwork(other_user_id, aid){
	jQuery(".ra"+other_user_id+""+aid).hide();
	jQuery(".loading"+other_user_id+""+aid).show();
	jQuery.ajax({
		type: "GET",
		url: '<?php echo JRoute::_('index.php?option=com_userprofile&task=request_network'); ?>&id='+other_user_id+"&pid="+aid,
		success: function(data){
			var hid = 1;

			jQuery(".loading"+other_user_id+""+aid).hide();

			jQuery(".ra"+other_user_id+""+aid).after("<div class=\"left button search-req-pending\" style=\"margin-right: 20px;margin-left:20px;width:180px;padding: 8px;margin-bottom:10px;\"><?php echo JText::_('COM_REFERRAL_TERMS_PEND')?></div>");
		}				
	});
}

jQuery("#changevalue").click(function(){
	var lid = jQuery( "#editid" ).val();
	var userinput = jQuery( "#userinput" ).val();
	jQuery.post("<?php echo JRoute::_('index.php?option=com_userprofile&task=updatelisting'); ?>&id="+lid+"&value="+userinput,
			function(data){
				var result = jQuery.parseJSON(data);
				if(result.code == 1){
					//alert(result.value);
					jQuery( "#item"+lid ).html(result.days);
					jQuery( "#expmsg" ).show();
					jQuery( "#experr" ).hide();
				}
				else
					jQuery( "#experr" ).show();
			});
});



jQuery('.items').infinitescroll({
	navSelector  : "#paginator",            
	nextSelector : "#paginator a.page_next",    
	itemSelector : ".items" ,         
	loading: {
		img   : "https://www.agentbridge.com/images/ajax_loader.gif",                
		msgText  : "",      
	 }        
  }, function(arrayOfNewElems){
	 if(!jQuery("ul.items").last().html().trim().length) {
		 jQuery(window).unbind('.infscr');
	 }
  });

</script>
<style>
	#infscr-loading {
		display:table;
		float:left;
		height:30px;
		width:100%;
		text-align:center;
		margin:10px 0;
	}
	#infscr-loading img {
		height:30px;
	}
	
	ul#paginator li a:link,
	ul#paginator li a:visited,
	ul#paginator li a:hover,
	ul#paginator li a:active {
		color:#fff !important;
	}
</style>
