<?php
/**
 * @version     1.0.0
 * @package     com_customsearch
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      AgentBridge <support@agentbridge.com> - https://www.agentbridge.com 
 */

defined('_JEXEC') or die;

// Include dependancies
 if(JFactory::getUser()->id){

 	
 } else {
    if (isset($_GET['webservice']) && $_GET['webservice']=="1") {

    } else {
      JFactory::getApplication()->redirect('index.php?illegal=1&returnurl='.base64_encode(JURI::current().$_SERVER['REQUEST_URI']));
    }
 }


// Execute the task.
$controller	= JControllerLegacy::getInstance('Customsearch');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
