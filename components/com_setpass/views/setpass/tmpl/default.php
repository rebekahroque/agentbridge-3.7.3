<?php
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

jimport('joomla.application.component.controller');
jimport('joomla.user.helper');

$session =& JFactory::getSession(); 
$session->set( 'userid', $this->useregid);

$start_activated=date("m-d-Y, h:i:s A");
$session->set( 'start_activated', $start_activated );


?>

<script type="text/javascript">
mixpanel.track("Set Password");
</script>


<jdoc:include type="message" />

<div style="padding-bottom:40px; min-height:800px">
<div style="display:none">
<button id="activate" type="submit" onclick="updateActivationDate()"></button></div>
<div class="pw-container full-radius setup-password" style="display: block; color:#000000"><!-- start setup password -->
	
	<h3 class="pw-heading"><?php echo JText::_('COM_PW_SETUP');?></h3>
	<div class="pw-container-left">
		<div class="pw-volume right" style="width: 366px;text-align: center;">
		<img style="width:20px" src="<?php echo $this->baseurl?>/templates/agentbridge/images/ca-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl?>/templates/agentbridge/images/us-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl?>/templates/agentbridge/images/uk-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl?>/templates/agentbridge/images/au-flag.png"> 
		<img style="width:20px" src="<?php echo $this->baseurl?>/templates/agentbridge/images/ie-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl;?>/templates/agentbridge/images/fr-flag.png">					
		<img style="width:20px" src="<?php echo $this->baseurl;?>/templates/agentbridge/images/de-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl;?>/templates/agentbridge/images/mc-flag.png">
		<img style="width:20px" src="<?php echo $this->baseurl;?>/templates/agentbridge/images/ch-flag.png">		<br>
		<span class="pw-ticker"><?php echo "$".number_format($this->total, 0, '.', ',') ?></span> <br/><?php echo JText::_('COM_NRDS_AVEVOLUME') ?></div>
		<div class="clear-both right"><img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_corrales.png" />
		<img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_edler.png" />
		<img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_lee.jpg" /></div>
		<div class="clear-both right"><img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_ketko.png" />
		<img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_stearns.png" />
		<img class="img-pw" src="<?php echo $this->baseurl?>/templates/agentbridge/images/pw_schulz.png" /></div>
	</div>
	<div class="pw-container-right">
		<form id="set_password"  enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $this->useregid ?><?php echo $_GET['regid'] ?>" id="jform_regid" name="jform[useregid]" />		
		<input type="hidden" value="<?php echo $this->useregid ?>" name="regid" />	
		<input type="hidden" value="set_password" id="jform_form" name="jform[form]" />			
			<div class="forms-head">
				<br/><br/><strong><?php echo JText::_('COM_PW_FREE');?></strong><br/>
				<span class="pw-subheading"><?php echo JText::_('COM_PW_MINS');?></span>
			</div>

			<div>
				<input type="password" size="30" style="color:#999999" class="validate-password required text-input-pw" autocomplete="off" value="" placeholder="<?php echo JText::_('COM_PW_CREATE');?>" id="jform_password1" name="jform[password1]" />
			</div>
			<div>
				<p class="jform_password1 error_msg" style="display:none; font-size:11px"> <?php echo JText::_('COM_PW_ERREQ');?>  <br /></p> 
				<p id="pw_min" class="jform_password1_min error_msg" style="display:none; font-size:11px"> <?php echo JText::_('COM_PW_ERROR1');?> <br /></p> 
				<p id="pw_req" class="jform_password1_req error_msg" style="display:none; line-height:16px; font-size:11px"> <?php echo JText::_('COM_PW_ERROR2');?>  <br /></p> 
			</div>
			<div>
				<input type="password" size="30" style="color:#999999" class="validate-password required text-input-pw" autocomplete="off" value="" placeholder="<?php echo JText::_('COM_PW_RETYPE');?>" id="jform_password2" name="jform[password2]" oncopy="return false" onpaste="return false" oncut="return false"/>		
			</div>
			<div>
				<p class="jform_password2 error_msg" style="display:none; font-size:11px"> <?php echo JText::_('COM_PW_ERREQ');?> <br /></p> 
			</div>
	
			<?php if($this->ispremium==1) { ?>
			<div class="pw-subheading"><?php echo JText::_('COM_PW_TERMSACCEPT');?></div>
			<?php } ?>
			<div class="login-form left">
				<input id="submit-user-password" name="" value="<?php echo JText::_('COM_USERPROF_CC_JOIN');?>" type="submit" class="submit-btn short-btn" />
				<input type="hidden" name="option" value="com_setpass" />
				<input id="jform_password_task" type="hidden" name="task" value="set_pass" />

				<?php echo JHtml::_('form.token');?>
			</div>
		</form>
	  </div>
	</div><!-- end setup password -->
</div>

<div id="termsandconditions" class="left" style="display:none; font-size:12px; overflow:auto">

	<?php echo JText::_('COM_USERPROF_CC_TERMSCON');?>


</div>

<div id="policy" class="left" style="display:none; font-size:12px; overflow:auto">
	<?php echo JText::_('COM_USERPROF_CC_PRIVTEXT');?>
</div>


<div id="guidelines" class="left" style="display:none; font-size:12px; overflow:auto">
	<?php echo JText::_('COM_USERPROF_CC_MEMTEXT');?>
</div>

<script>

	function updateActivationDate(){
		jQuery.ajax({
			url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=updateActivationDate",
			type: "POST",
			data: {useregid: "<?php echo $this->useregid?>"},
			success: function(data){
				    console.log("success");
				    }
				});
	}
	
	function popTerms(){
		jQuery("#termsandconditions").dialog(
			{
				modal:true, 
				width: "auto",
				title: "<?php echo JText::_('COM_USERPROF_CC_TERMS') ?>",
				height:600
				});
	}
	function popPolicy(){
		jQuery("#policy").dialog(
			{
				modal:true, 
				width: "auto",
				title: "<?php echo JText::_('COM_USERPROF_CC_PRIV') ?>",
				height:600
				});
	}
	function popGuidelines(){
		jQuery("#guidelines").dialog(
			{
				modal:true, 
				width: "auto",
				title: "<?php echo JText::_('COM_USERPROF_CC_MEM') ?>",
				height:600
				});
	}
//Your timing variables in number of seconds
	 
	//total length of session in seconds
	var sessionLength = (30*60);
	//var sessionLength = 10;
	//time warning shown (10 = warning box shown 10 seconds before session starts)
	var warning = 10; 
	//time redirect forced (10 = redirect forced 10 seconds after session ends)    
	var forceRedirect = 0;
	 
	//time session started
	var pageRequestTime = new Date();
	 
	//session timeout length
	var timeoutLength = sessionLength*1000;
	 
	//set time for first warning, ten seconds before session expires
	var warningTime = timeoutLength - (warning*1000);
	 
	//force redirect to log in page length (session timeout plus 10 seconds)
	var forceRedirectLength = timeoutLength + (forceRedirect*1000);
	 
	//set number of seconds to count down from for countdown ticker
	var countdownTime = warning;
	 
	//warning dialog open; countdown underway
	var warningStarted = false;

	var teset_seconds=1;

	function CheckForSession() {

		console.log(teset_seconds++);
		//get time now
	    var timeNow = new Date();
	     
	    //event create countdown ticker variable declaration
	    var countdownTickerEvent;  
	     
	    //difference between time now and time session started variable declartion
	    var timeDifference = 0;
	     
	    timeDifference = timeNow - pageRequestTime;
	          
	    if (timeDifference > forceRedirectLength)
	        {   
	           //clear (stop) checksession event
	           jQuery("#submit-user-password").data('clicked', true);
	           clearInterval(checkSessionTimeEvent);
		       jQuery.ajax({		          	
					url: "<?php echo JRoute::_('index.php?option=com_setpass&task=emailProgressAbandon&format=raw')?>",
		          	type: "POST",
					data: {useregid: "<?php echo $this->useregid?>", page: "<?php echo $_GET['option']?>", start:"<?php echo $start_activated?>",abandoned:timeDifference},
		          	success: function(data){
		          		window.location="<?php echo $this->baseurl?>"
						//alert('success');
		          	}
		          });
	 			//$jquery("#logout_link").click();
	            //force relocation
	           // window.location="<?php echo $this->baseurl?>";
	        }

	}

	var checkSessionTimeEvent;

jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	jQuery("#activate").trigger('click');
	jQuery("ul").hide();
	checkSessionTimeEvent = setInterval("CheckForSession()", (30*60*1000));
	
	jQuery("#submit-user-password").click(function(){
		jQuery(this).data('clicked', true);
	});

	
	window.onbeforeunload = function() {

					var timeNow = new Date();
		     
				    //event create countdown ticker variable declaration
				    var countdownTickerEvent;  
				     
				    //difference between time now and time session started variable declartion
				    var timeDifference = 0;
				     
				    timeDifference = timeNow - pageRequestTime;
				    if(!jQuery("#submit-user-password").data('clicked')){

		    		   jQuery.ajax({
			          	url: "<?php echo $this->baseurl?>/index.php?option=com_setpass&task=emailProgressAbandon&format=raw",
			          	type: "POST",
			          	async:false,
			          	data: {useregid: "<?php echo $this->useregid?>", page: "<?php echo $_GET['option']?>", start:"<?php echo $start_activated?>",abandoned:timeDifference},
			          	success: function(data){
			          		window.location="<?php echo $this->baseurl?>"
							//alert('success');
			          	}
			          });
		    		}
		}
	

});
</script>