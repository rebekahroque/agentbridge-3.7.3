<?php



defined('_JEXEC') or die;



/**

 * Registration model class for Users.

 *

 * @package     Joomla.Site

 * @subpackage  com_users

 * @since       1.6

 */

class SetPassModelSetPass extends JModelLegacy{



	/**

	 * @var		object	The user registration data.

	 * @since   1.6

	 */

	protected $data;



	/**

	 * Method to activate a user account.

	 *

	 * @param   string  The activation token.

	 * @return  mixed  	False on failure, user object on success.

	 * @since   1.6

	 */
	 


	public function activate($token)

	{

		$config	= JFactory::getConfig();

		$userParams	= JComponentHelper::getParams('com_users');

		$db		= $this->getDbo();



		// Get the user id based on the token.

		$db->setQuery(

			'SELECT '.$db->quoteName('id').' FROM '.$db->quoteName('#__users') .

			' WHERE '.$db->quoteName('activation').' = '.$db->Quote($token) .

			' AND '.$db->quoteName('block').' = 1' .

			' AND '.$db->quoteName('lastvisitDate').' = '.$db->Quote($db->getNullDate())

		);

		$userId = (int) $db->loadResult();



		// Check for a valid user id.

		if (!$userId)

		{

			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));

			return false;

		}



		// Load the users plugin group.

		JPluginHelper::importPlugin('user');



		// Activate the user.

		$user = JFactory::getUser($userId);



		// Admin activation is on and user is verifying their email

		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))

		{

			$uri = JURI::getInstance();



			// Compile the admin notification mail values.

			$data = $user->getProperties();

			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());

			$user->set('activation', $data['activation']);

			$data['siteurl']	= JUri::base();

			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);

			$data['fromname'] = $config->get('fromname');

			$data['mailfrom'] = $config->get('mailfrom');

			$data['sitename'] = $config->get('sitename');

			$user->setParam('activate', 1);

			$emailSubject	= JText::sprintf(

				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',

				$data['name'],

				$data['sitename']

			);



			$emailBody = JText::sprintf(

				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',

				$data['sitename'],

				$data['name'],

				$data['email'],

				$data['username'],

				$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation']

			);



			// get all admin users

			$query = 'SELECT name, email, sendEmail, id' .

						' FROM #__users' .

						' WHERE sendEmail=1';



			$db->setQuery($query);

			$rows = $db->loadObjectList();



			// Send mail to all users with users creating permissions and receiving system emails

			foreach ($rows as $row)

			{

				$usercreator = JFactory::getUser($row->id);

				if ($usercreator->authorise('core.create', 'com_users'))

				{

					$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);



					// Check for an error.

					if ($return !== true)

					{

						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

						return false;

					}

				}

			}

		}



		//Admin activation is on and admin is activating the account

		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))

		{

			$user->set('activation', '');

			$user->set('block', '0');



			$uri = JURI::getInstance();



			// Compile the user activated notification mail values.

			$data = $user->getProperties();

			$user->setParam('activate', 0);

			$data['fromname'] = $config->get('fromname');

			$data['mailfrom'] = $config->get('mailfrom');

			$data['sitename'] = $config->get('sitename');

			$data['siteurl']	= JUri::base();

			$emailSubject	= JText::sprintf(

				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',

				$data['name'],

				$data['sitename']

			);



			$emailBody = JText::sprintf(

				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',

				$data['name'],

				$data['siteurl'],

				$data['username']

			);



			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);



			// Check for an error.

			if ($return !== true)

			{

				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

				return false;

			}

		}

		else

		{

			$user->set('activation', '');

			$user->set('block', '0');

		}



		// Store the user object.

		if (!$user->save())

		{

			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));

			return false;

		}



		return $user;

	}



	/**

	 * Method to get the registration form data.

	 *

	 * The base form data is loaded and then an event is fired

	 * for users plugins to extend the data.

	 *

	 * @return  mixed  	Data object on success, false on failure.

	 * @since   1.6

	 */

	public function getData()

	{

		if ($this->data === null)

		{

			$this->data	= new stdClass;

			$app	= JFactory::getApplication();

			$params	= JComponentHelper::getParams('com_users');



			// Override the base user data with any data in the session.

			$temp = (array) $app->getUserState('com_users.registration.data', array());

			foreach ($temp as $k => $v)

			{

				$this->data->$k = $v;

			}



			// Get the groups the user should be added to after registration.

			$this->data->groups = array();



			// Get the default new user group, Registered if not specified.

			$system	= $params->get('new_usertype', 2);



			$this->data->groups[] = $system;



			// Unset the passwords.

			unset($this->data->password1);

			unset($this->data->password2);



			// Get the dispatcher and load the users plugins.

			$dispatcher	= JEventDispatcher::getInstance();

			JPluginHelper::importPlugin('user');



			// Trigger the data preparation event.

			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));



			// Check for errors encountered while preparing the data.

			if (count($results) && in_array(false, $results, true))

			{

				$this->setError($dispatcher->getError());

				$this->data = false;

			}

		}



		return $this->data;

	}



	/**

	 * Method to get the registration form.

	 *

	 * The base form is loaded from XML and then an event is fired

	 * for users plugins to extend the form with extra fields.

	 *

	 * @param   array  $data		An optional array of data for the form to interogate.

	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.

	 * @return  JForm	A JForm object on success, false on failure

	 * @since   1.6

	 */

	public function getForm($data = array(), $loadData = true)

	{

		// Get the form.

		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))

		{

			return false;

		}



		return $form;

	}



	/**

	 * Method to get the data that should be injected in the form.

	 *

	 * @return  mixed  The data for the form.

	 * @since   1.6

	 */

	protected function loadFormData()

	{

		return $this->getData();

	}



	/**

	 * Override preprocessForm to load the user plugin group instead of content.

	 *

	 * @param   object	A form object.

	 * @param   mixed	The data expected for the form.

	 * @throws	Exception if there is an error in the form event.

	 * @since   1.6

	 */

	protected function preprocessForm(JForm $form, $data, $group = 'user')

	{

		$userParams	= JComponentHelper::getParams('com_users');



		//Add the choice for site language at registration time

		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)

		{

			$form->loadFile('sitelang', false);

		}



		parent::preprocessForm($form, $data, $group);

	}



	/**

	 * Method to auto-populate the model state.

	 *

	 * Note. Calling getState in this method will result in recursion.

	 *

	 * @since   1.6

	 */

	protected function populateState()

	{

		// Get the application object.

		$app	= JFactory::getApplication();

		$params	= $app->getParams('com_users');



		// Load the parameters.

		$this->setState('params', $params);

	}



	/**

	 * Method to save the form data.

	 *

	 * @param   array  The form data.

	 * @return  mixed  	The user id on success, false on failure.

	 * @since   1.6

	 */

	public function register($temp)

	{

		$config = JFactory::getConfig();

		$db		= $this->getDbo();

		$params = JComponentHelper::getParams('com_setpass');



		// Initialise the table with JUser.

		$user = new JUser;

		$data = (array) $this->getData();

		

		// Merge in the registration data.

		foreach ($temp as $k => $v)

		{

			$data[$k] = $v;

		}





		// Prepare the data for the user object.

		$data['email']		= $data['email1'];

		$data['password']	= $data['password1'];

		$useractivation = $params->get('useractivation');

		$sendpassword = $params->get('sendpassword', 1);



		
		if($data['password1']!=$data['password2']){

			$data['error'] = "Passwords didnt match";

			return false;

		}

		// Check if the user needs to activate their account.

		if (($useractivation == 1) || ($useractivation == 2))

		{

			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());

			$data['block'] = 1;

		}




		// Bind the data.

		if (!$user->bind($data))

		{

			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));

			return false;

		}

		// Load the users plugin group.

		JPluginHelper::importPlugin('user');



		// Store the data.

		if (!$user->save())

		{

			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));

			return false;

		}



		// Compile the notification mail values.

		$data = $user->getProperties();

		$data['fromname']	= $config->get('fromname');

		$data['mailfrom']	= $config->get('mailfrom');

		$data['sitename']	= $config->get('sitename');

		$data['siteurl']	= JUri::root();





		// Handle account activation/confirmation emails.

		if ($useractivation == 2)

		{

			// Set the link to confirm the user email.

			$uri = JURI::getInstance();

			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);



			$emailSubject	= JText::sprintf(

				'COM_USERS_EMAIL_ACCOUNT_DETAILS',

				$data['name'],

				$data['sitename']

			);



			if ($sendpassword)

			{

				$emailBody = JText::sprintf(

					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',

					$data['name'],

					$data['sitename'],

					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],

					$data['siteurl'],

					$data['username'],

					$data['password_clear']

				);

			}

			else

			{

				$emailBody = JText::sprintf(

					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',

					$data['name'],

					$data['sitename'],

					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],

					$data['siteurl'],

					$data['username']

				);

			}

		}

		elseif ($useractivation == 1)

		{

			// Set the link to activate the user account.

			$uri = JURI::getInstance();

			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

			$data['activate'] = $base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$data['activation'], false);



			$emailSubject	= JText::sprintf(

				'COM_USERS_EMAIL_ACCOUNT_DETAILS',

				$data['name'],

				$data['sitename']

			);



			if ($sendpassword)

			{

				$emailBody = JText::sprintf(

					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',

					$data['name'],

					$data['sitename'],

					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],

					$data['siteurl'],

					$data['username'],

					$data['password_clear']

				);

			}

			else

			{

				$emailBody = JText::sprintf(

					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',

					$data['name'],

					$data['sitename'],

					$data['siteurl'].'index.php?option=com_users&task=registration.activate&token='.$data['activation'],

					$data['siteurl'],

					$data['username']

				);

			}

		}

		else

		{


/*
			$emailSubject	= JText::sprintf('Welcome to AgentBridge!');



			if ($sendpassword)

			{

				$emailBody = "Dear ".$data['name'].",<br /><br /> Congratulations.  You are now a member of AgentBridge, the most powerful network of real estate professionals in the world.<br /><br />



						Click this link to access your account and take advantage of your membership privileges.<br />



						<a href='https://www.agentbridge.com/'>AgentBridge Log In</a>



						<br /><br /> Sincerely, <br/><br/>The AgentBridge Team<br/><a href='mailto:service@agentbridge.com'>service@AgentBridge.com</a>";

			}

			else

			{

				$emailBody = JText::sprintf(

					'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',

					$data['name'],

					$data['sitename'],

					$data['siteurl']

				);

			}
		*/

		}



		// Send the registration email.

		
		// $sendmail = JFactory::getMailer();
					// $return = true;
		

		// $sendmail->isHTML( true );

		// if($sendmail->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody)){

			// $return = true;

		// }
		
		$return = true;

		

		

		

		//Send Notification mail to administrators

		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))

		{

			$emailSubject = JText::sprintf(

				'COM_USERS_EMAIL_ACCOUNT_DETAILS',

				$data['name'],

				$data['sitename']

			);



			$emailBodyAdmin = JText::sprintf(

				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',

				$data['name'],

				$data['username'],

				$data['siteurl']

			);



			// get all admin users

			$query = 'SELECT name, email, sendEmail' .

					' FROM #__users' .

					' WHERE sendEmail=1';



			$db->setQuery($query);

			$rows = $db->loadObjectList();



			// Send mail to all superadministrators id

			foreach ($rows as $row)

			{

				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);



				// Check for an error.

				if ($return !== true)

				{

					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));

					return false;

				}

			}

		}

		// Check for an error.

		if ($return !== true)

		{

			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));



			// Send a system message to administrators receiving system mails

			$db = JFactory::getDBO();

			$q = "SELECT id

				FROM #__users

				WHERE block = 0

				AND sendEmail = 1";

			$db->setQuery($q);

			$sendEmail = $db->loadColumn();

			if (count($sendEmail) > 0)

			{

				$jdate = new JDate;

				// Build the query to add the messages

				$q = "INSERT INTO ".$db->quoteName('#__messages')." (".$db->quoteName('user_id_from').

				", ".$db->quoteName('user_id_to').", ".$db->quoteName('date_time').

				", ".$db->quoteName('subject').", ".$db->quoteName('message').") VALUES ";

				$messages = array();



				foreach ($sendEmail as $userid)

				{

					$messages[] = "(".$userid.", ".$userid.", '".$jdate->toSql()."', '".JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')."', '".JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])."')";

				}

				$q .= implode(',', $messages);

				$db->setQuery($q);

				$db->execute();

			}

			return false;

		}



		if ($useractivation == 1)

		{

			return "useractivate";

		}

		elseif ($useractivation == 2)

		{

			return "adminactivate";

		}

		else

		{

			return $user->id;

		}

	}

	

	public function get_user_details($select, $regid){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select($select);

		$query->from('#__user_registration ur');

		$query->where('user_id = '.$regid);

		$query->leftJoin('#__countries c ON c.countries_id = ur.country');

		$query->leftJoin('#__country_languages cl ON cl.country = ur.country');

		$query->leftJoin('#__broker br ON br.broker_id = ur.brokerage');

		$query->leftJoin('#__zones z ON z.zone_id = ur.state');

		$db->setQuery($query);

		return $db->loadObject();

	}
	
	public function get_user_activation($actid){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('activation_status');

		$query->from('#__user_registration');

		$query->where('user_id = '.$actid);

		$db->setQuery($query);

		return $db->loadObject();

	}

	public function get_broker_data($brokerage){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('broker_name');

		$query->from('#__broker');

		$query->where('broker_id = '.$brokerage);

		$db->setQuery($query);

		return $db->loadObject();

	}
	

	public function insert_activate_activity(){

		$db = JFactory::getDbo();


		$insert_activity = new JObject();

		$session =& JFactory::getSession();

		$insert_activity->activity_type = (int)10;

		$insert_activity->activity_id = JFactory::getUser()->id;

		$insert_activity->user_id = JFactory::getUser()->id;

		$insert_activity->date = date('Y-m-d H:i:s',time());

		$insert_activity->other_user_id = JFactory::getUser()->id;

		$ret = $db->insertObject('#__activities', $insert_activity);
		
		//Update the activity status to 1
		
		$query = $db->getQuery(true);
		
		$query->setQuery("

			UPDATE
				#__user_registration
			SET 
				activation_status = 1
			WHERE
				email = '".JFactory::getUser()->email."'");
		$db->setQuery($query);
		
		$db->execute();

	}

	

	public function insert_activate_activity_premium(){

		$db = JFactory::getDbo();

		$insert_activity = new JObject();

		$session =& JFactory::getSession();

		$insert_activity->activity_type = (int)27;

		$insert_activity->activity_id = JFactory::getUser()->id;

		$insert_activity->user_id = JFactory::getUser()->id;

		$insert_activity->date = date('Y-m-d H:i:s',time());

		$insert_activity->other_user_id = JFactory::getUser()->id;

		$ret = $db->insertObject('#__activities', $insert_activity);

		

		//update invitation tracker user accepted the invitation

		$query = $db->getQuery(true);

		

		$query->setQuery("

			UPDATE

				#__invitation_tracker

			SET 

				invitation_status = 1

			WHERE

				invited_email = '".JFactory::getUser()->email."'");

		$db->setQuery($query);

		$db->execute();

		

		$query->setQuery("

			UPDATE

				#__user_registration

			SET 

				activation_status = 1

			WHERE

				email = '".JFactory::getUser()->email."'");

		$db->setQuery($query);

		$db->execute();



		//select if invited

		$query->setQuery("

			SELECT 

				a.invited_email as invited_email, 

				b.email as email,

				b.image as image

			FROM 

				#__invitation_tracker a

			LEFT JOIN 

				#__user_registration b 

			ON 

				a.agent_id = b.user_id

			WHERE

				a.invited_email = '".JFactory::getUser()->email."'"

		);



		$db->setQuery($query);

		

		if ( !empty($db->loadObject()->invited_email ) && !empty($db->loadObject()->email ) ){

			

			$invited_email 	= $db->loadObject()->invited_email;

			$email			= $db->loadObject()->email;

			$session =& JFactory::getSession();

			

			$query->setQuery("

				SELECT 

					*

				FROM

					#__users

				WHERE 

					email = '".$invited_email."'

			");

			$db->setQuery($query);

			

			$other_user_id = $db->loadObject()->id;

			$invitees_full_name = $db->loadObject()->name;

			

			$query->setQuery("

				SELECT 

					*

				FROM

					#__users

				WHERE 

					email = '".$email."'

			");

			$db->setQuery($query);

			$user_id = $db->loadObject()->id;

			$inviters_full_name = $db->loadObject()->name;



			$insert_request_network->user_id = $user_id;

			$insert_request_network->other_user_id = $other_user_id;

			$insert_request_network->status = (int)1;

			$db->insertObject('#__request_network', $insert_request_network);

	

			$insert_request_network->user_id = $other_user_id;

			$insert_request_network->other_user_id = $user_id;

			$insert_request_network->status = (int)1;

			$db->insertObject('#__request_network', $insert_request_network);



			

			$session->set( 'invitees_fullname', $invitees_full_name );		

			$session->set( 'inviters_full_name', $inviters_full_name );	

		}

	

	}
	
	public function insert_terms_accepted($email) {
		
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		

		$query->setQuery("

			UPDATE

				#__user_registration

			SET 

				is_term_accepted = 1

			WHERE

				email = '".JFactory::getUser()->email."'");

		$db->setQuery($query);

		$db->execute();
	}
	
	public function get_totalave2012(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2012,ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__users u ON u.email = ur.email');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('us.verified_2012=1 AND ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2012 = $db->loadObjectList();
		
		return $totalave2012;


	}
	
	public function get_totalave2013(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2013,ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__users u ON u.email = ur.email');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('us.verified_2013=1 AND ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2013 = $db->loadObjectList();
		
		return $totalave2013;


	}

	public function get_totalave2014(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2014, ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__users u ON u.email = ur.email');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('us.verified_2014=1 AND ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2014 = $db->loadObjectList();
		
		return $totalave2014;


	}
	
	public function get_totalave2015(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2015, ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__users u ON u.email = ur.email');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('us.verified_2015=1 AND ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2015 = $db->loadObjectList();
		
		return $totalave2015;

	}

	public function get_AveMemberVolume(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('u.*, us.*, ur.*, c.currency as user_currency');

		$query->from('#__users u');

		$query->leftJoin('#__user_registration ur ON ur.email  = u.email');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2015 = $db->loadObjectList();
		
		return $totalave2015;

	}
	
	function getExchangeRates($price,$currency_user,$currency_pops){

			$file = 'latest.json';
			$appId = 'd73f8525552048a7a39aaac9977299fd';

			// Open CURL session:
			$ch = curl_init("https://openexchangerates.org/api/{$file}?app_id={$appId}");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);

			// Decode JSON response:
			$exchangeRates = json_decode($json);

			// You can now access the rates inside the parsed object, like so:
			/*printf(
			    "1 %s in GBP: %s (as of %s)",
			    $exchangeRates->base,
			    $exchangeRates->rates->GBP,
			    date('H:i jS F, Y', $exchangeRates->timestamp)
			);*/

							
			if(!$currency_pops){
				$currency_pops="USD";
			}
			if($currency_user!="USD" && $currency_user!=""){
				
				$converted_price = $price * $exchangeRates->rates->{$currency_user};
			} else {
				$converted_price = $price / $exchangeRates->rates->{$currency_pops};
			}
			

			return $converted_price;

			

		}
		
	


	

}	

?>