<?php



defined( '_JEXEC' ) or die( 'Restricted access' );



jimport('joomla.application.component.controller');



require_once JPATH_LIBRARIES . '/tcpdf/library/config/lang/eng.php';



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



*/



class UserProfileController extends JControllerLegacy



{



	/**



	 * Method to display the view



	 *



	 * @access    public



	 */



	function display()



	{

		$model = $this->getModel('UserProfile');
		$application = JFactory::getApplication();
		//$security = $model->get_security(JFactory::getUser()->id);
		//if ($security->uid=='') 
			//$application->redirect(JRoute::_('index.php?option=com_securityquestion'));
		//$uid = JFactory::getUser()->id;
		//if(empty($uid))
			//$application->redirect('index.php?illegal=1');
		$userinfo = $model->get_user_registration(JFactory::getUser()->id);
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('userprofile', $model->get_user_registration(JFactory::getUser()->id));
		$view->assignRef('languages_list', $model->get_languages());
		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));
		parent::display();



	}



	function createInvoice()



	{



		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';



		JModelLegacy::addIncludePath( $userprofilemodel );



		$model =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');



		return "Fee_".JFactory::getUser()->id."_".$model->get_next_invoice()."_".rand(0, 102);



	}	



	



	//function to send xml request via curl



	function send_request_via_curl($host,$path,$content)



	{



		$posturl = "https://" . $host . $path;



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_URL, $posturl);



		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);



		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));



		curl_setopt($ch, CURLOPT_HEADER, 1);



		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);



		curl_setopt($ch, CURLOPT_POST, 1);



		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);



		$response = curl_exec($ch);



		return $response;



	}



	



	//function to parse Authorize.net response



	function parse_return($content)



	{



		$refId = $this->substring_between($content,'<refId>','</refId>');



		$resultCode = $this->substring_between($content,'<resultCode>','</resultCode>');



		$code = $this->substring_between($content,'<code>','</code>');



		$text = $this->substring_between($content,'<text>','</text>');



		$subscriptionId = $this->substring_between($content,'<subscriptionId>','</subscriptionId>');



		return array ($refId, $resultCode, $code, $text, $subscriptionId);



	}



	//helper function for parsing response



	function substring_between($haystack,$start,$end) 



	{



		if (strpos($haystack,$start) === false || strpos($haystack,$end) === false) 



		{



			return false;



		} 



		else 



		{



			$start_position = strpos($haystack,$start)+strlen($start);



			$end_position = strpos($haystack,$end);



			return substr($haystack,$start_position,$end_position-$start_position);



		}



	}	



	



	function membershippayment()



	{



		$model 		= $this->getModel('UserProfile');



		$loginname="926t4yDN3";



		$transactionkey="27c8ds72fUUZj4Zh";



		$host = "api.authorize.net";



		$path = "/xml/v1/request.api";



		$exp_date = explode("-", $_POST['card_expiry']);



		$whowillpay = $model->get_user_registration(JFactory::getUser()->id);

		

		$model->update_tax_id_num($whowillpay->user_id, $this->encrypt($_POST['btino']));



		$schedule = trim( $_POST['schedule'] );



		switch( $schedule ) {



		



			case 'Monthly' : 				



				$length 			= 30;



				$totalOccurrences 	= 12;		



				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every month";



				break;



				



			case 'Quarterly' : 



				



				$length 			= 90;



				$totalOccurrences 	= 4;



				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every quarter";



			break;



			



			case 'Semi-annual' : 



				



				$length 			= 180;



				$totalOccurrences 	= 2;



				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every semi annual";



			break;			



	



			case 'Annual' : 



				



				$length 			= 365;



				$totalOccurrences 	= 1;



				$message 			= "Renewals are paid on the ".date("j").date("S")." of ".date("F")." every year";



			break;



			



			case 'Bi-annual' : 



				



				$length 			= 365;



				$totalOccurrences 	= 2;



				$message 			= "Renewals are paid on the ".date("j").date("S")." of ".date("F")." every 2 years";



			break;



			



				



		



		}



		



		$content = 



		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .



		"<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/" .



			"AnetApiSchema.xsd\">" .



			"<merchantAuthentication>" .



				"<name>926t4yDN3</name>" .



				"<transactionKey>27c8ds72fUUZj4Zh</transactionKey>" .



			"</merchantAuthentication>" .



			"<refId>".$this->createInvoice()."</refId>" .



			"<subscription>" .



				"<name>AgentBridge Membership Payment Fee</name>" .



				"<paymentSchedule>" .



					"<interval>" .



						"<length>".$length."</length>" .



						"<unit>days</unit>" .



					"</interval>" .



					"<startDate>".date('Y-m-d')."</startDate>" .



					"<totalOccurrences>".$totalOccurrences."</totalOccurrences>" .



					"<trialOccurrences>0</trialOccurrences>" .



				"</paymentSchedule>" .



			"<amount>".(int) str_replace(',', '', str_replace('$', '', $_POST['amount']))."</amount>" .



			"<trialAmount>0</trialAmount>".



			"<payment>" .



				"<creditCard>" .



					"<cardNumber>".$_POST['card_number']."</cardNumber>" .



					"<expirationDate>".$exp_date[1]."-".$exp_date[0]."</expirationDate>" .



				"</creditCard>" .



			"</payment>" .



			"<order>" .



					"<invoiceNumber>".$invoice."</invoiceNumber>" .



					"<description>".$_POST['schedule']." Subscription</description>" .



			"</order>" .



			"<customer>" .



					"<email>".$_POST['email']."</email>" .



					"<phoneNumber>".$_POST['phone']." </phoneNumber>" .



			"</customer>" .



			"<billTo>" .



				"<firstName>".$_POST['firstname']."</firstName>" .



				"<lastName>".$_POST['lastname']."</lastName>" .



				"<address>".$_POST['address']."</address>" .



				"<city>".$_POST['city']."</city>" .



				"<state>".$model->getState( $_POST['state'] )."</state>" .



				"<zip>".$_POST['zip']."</zip>" .



				"<country>".$model->getCountry(  $_POST['country'] )."</country>" .



			"</billTo>" .



			"<shipTo>" .



				"<firstName>n/a</firstName>" .



				"<lastName>n/a</lastName>" .



				"<address>n/a</address>" .



				"<city>n/a</city>" .



				"<state>n/a</state>" .



				"<zip>n/a</zip>" .



				"<country>n/a</country>" .



			"</shipTo>" .



			"</subscription>" .



		"</ARBCreateSubscriptionRequest>";		



		



		$response = $this->send_request_via_curl($host,$path,$content);



		



		//if the connection and send worked $response holds the return from Authorize.net



		if ($response)



		{



			list ($refId, $resultCode, $code, $text, $subscriptionId) =$this->parse_return($response);



			if ( $resultCode == "Ok" ) {



				



				$model->insert_autorization_transaction(



					$subscriptionId,



					$_POST,



					$_POST['state'],



					$_POST['country'],



					//str_replace('-', '', $_POST['card_expiry']),



					$_POST['card_expiry'],



					$this->createInvoice()



				);



				



				$model->update_user_subsctiption(



					$model->get_user_registration(JFactory::getUser()->id)->user_id,



					trim( $_POST['promo_code_id'] ),



					trim( $_POST['schedule'] )



				);



			



			$body = "";



			



			$subject = "AgentBridge Membership Receipt";



			$email = $_POST['email'];



			$body  .= "<span style='font-size:20px; color:#006699;'><strong>Congratulations!</strong></span><br/><br/>";



			$body  .= "<span style='font-size:16px'>You are now a member of the most powerful real estate network in the world.</span><br/><br/> ";



			$body  .= "<span style='font-size:16px'><strong>Order Number:</strong>".$subscriptionId."</span><br />";



			$body  .= "<span style='font-size:16px'><strong>Order Date:</strong> ".date("d-m-Y H:m:s")."</span><br />";



			$body  .= "<span style='font-size:16px'><strong>Membership Plan:</strong> ".$_POST['schedule']."</span><br/><br/>";



			$body  .= "<span style='font-size:16px; color:#006699'><strong>Member information:</strong></span><br /><br />";



			$body  .= $_POST['firstname']." ".$_POST['lastname']."<br />";



			$body  .= $_POST['city']."<br />";



			$body  .= $model->getState( $_POST['state'] )."<br />";



			$body  .= $_POST['zip']."<br />";



			$body  .= $model->getCountry(  $_POST['country'] )."<br />";



			$body  .= $_POST['phone']."<br />";



			$body  .= $_POST['email']."<br /><br />";



			



			$body  .= "<span style='font-size:16px;color:#006699'><strong>Membership details:</strong></span><br /><br />";



			$body  .= "<table>



						<tr>



						<td width='300'><strong>Service</strong></td>



						<td width='100'><strong>QTY</strong></td>



						<td width='100'><strong>Total</strong></td>



						</tr>



						<tr>



						<td width='300'>AgentBridge ".$_POST['schedule']." Membership Fee <br /> ".$message."</td>



						<td width='100'>1</td>



						<td width='100'>$".number_format( $_POST['amount'] )."</td>



						</tr>



						</table><br/><br/>";



			



			$body  .= "<span style='font-size:16px;color:#006699'><strong>Membership inquiries:</strong></span><br /><br />";



			$body  .= "You can make changes to your membership plan anytime on your <a href='https://www.agentbridge.com/index.php/component/userprofile/?task=membership'>Membership </a>page. If you need general support for your order, call us at (310) 870-1231 or use our <a href='https://www.agentbridge.com/index.php/component/userprofile/component/userprofile/?task=contactus'>Contact Us</a> form.<br /><br />";



			



			$mailSender =& JFactory::getMailer();



			$mailSender ->addRecipient( $email );



			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );



			$mailSender ->setSubject( $subject );



			$mailSender ->isHTML(  true );



			$mailSender ->setBody(  $body );



			$mailSender ->Send();				



			



			echo trim($resultCode."|".$text);



		}



			else {



			echo trim($resultCode."|".$text);



			}



		



			}



	



		



		die();



	}



	function contactus()



    {



		$view = &$this->getView($this->getName(), 'html');



        $view->display($this->getTask());



    }

	

	function help()



    {



    	$model = $this->getModel($this->getName());

        $video_data = $model->getListVideos();

		$fee = $model->getReferralTable();

		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef('video_datas', $video_data);

		$view->assignRef('fee', $fee);

        $view->display($this->getTask());



    }



    function mobilehelp()



    {



    //	$model = $this->getModel($this->getName());



      //  $video_data = $model->getListVideos();

        

	//	$view = &$this->getView($this->getName(), 'html');



	//	$view->assignRef('video_datas', $video_data);



     //   $view->display($this->getTask());



    }



	



	function membership()



    {



		$model	= $this->getModel('UserProfile');



		$view = &$this->getView($this->getName(), 'html');



		$view->assignRef('current_subscription', $model->get_subscription_type($model->get_user_registration(JFactory::getUser()->id)->fee_id));



		$view->assignRef('payment_options', $model->get_payment_options());



		$is_prem =$model->check_user_invitation($model->get_user_registration(JFactory::getUser()->id)->email);

		

		if($is_prem ==",")

			$view->assignRef('is_premium', $model->get_user_registration(JFactory::getUser()->id)->is_premium );





		$view->assignRef('is_premium_invited', $model->check_user_invitation($model->get_user_registration(JFactory::getUser()->id)->email));



        $view->display($this->getTask());



	    ;



    }



	



	function submitTicketSupport() {



	



		$model	= $this->getModel('UserProfile');



	



		// set API configuration



		$apiUrl 	= "https://agentbridge.kayako.com/api/index.php?e=/Tickets/Ticket";



		$apiKey 	= "bdb372d7-55b7-a384-415c-fcfcf4b6cdf8";



		$salt 		= mt_rand();



		$secretKey 	= "MTM3NzMzZjctYTdkMy1mYzM0LWMxYTAtYWNjMGY2ZWNlNDI1YWIxNDc1MDctMjAyMy02MGQ0LWJkMDgtMjI0ZTliYTA3OTEy";



		$signature 	= base64_encode(hash_hmac('sha256',$salt,$secretKey,true));



		// users messages



		$subject 			= "AgentBridge Support";



		$fullname 			= $model->get_user_registration(JFactory::getUser()->id)->firstname." ".$model->get_user_registration(JFactory::getUser()->id)->lastname;



		$email 				= JFactory::getUser()->email;



		$contents 			= $_POST['ticket_content'];



		$departmentid 		= $_POST['ticket_category'];



		$ticketstatusid 	= "1";



		$ticketpriorityid 	= $_POST['ticket_priority'];



		$tickettypeid 		= $_POST['ticket_type'];



		$staffid 			= "1";



		$post_data = array('subject' => $subject,



		 'fullname' => $fullname,



		 'email' => $email,



		 'contents' => $contents,



		 'departmentid' => $departmentid,



		 'ticketstatusid' => $ticketstatusid,



		 'ticketpriorityid' => $ticketpriorityid,



		 'tickettypeid' => $tickettypeid,



		 'staffid' => $staffid, 



		 'apikey' => $apiKey, 



		 'salt' => $salt, 



		 'signature' => $signature);



		$post_data = http_build_query($post_data, '', '&');



		$curl = curl_init($apiUrl); 



		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);



		curl_setopt($curl, CURLOPT_POST, true);



		curl_setopt($curl, CURLOPT_URL, $apiUrl);



		curl_setopt($curl, CURLOPT_HEADER, false); 



		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);



		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);



		$response = curl_exec($curl);  



		curl_close($curl);



		



		echo 'proceed';



		die();



	}



	



	function aboutus()



    {



		$view = &$this->getView($this->getName(), 'html');



        $view->display($this->getTask());



    }



		



	



	public function mywallet(){



		$view = &$this->getView($this->getName(), 'html');



		$view->display($this->getTask());



	}



	



	public function continue_complete(){

		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$model = $this->getModel('UserProfile');
		$user_type = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		$model->getImage();
		$model->update_about(JFactory::getUser()->email, $_POST['jform']['about']);
		if(isset($_POST['jform']['designations']) && $_POST['jform']['designations']!="") {
			$model->update_designations(JFactory::getUser()->id, $_POST['jform']['designations']);
		}
		$model->update_languages(JFactory::getUser()->id, $_POST['jform']['languages']);
		$broker = $model->get_broker($model->get_user_registration(JFactory::getUser()->id)->brokerage);
		$broker_id = $model->get_broker_id(JFactory::getUser()->email);
		if($model->get_user_registration(JFactory::getUser()->id)->licence) {
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '';
		}
		if($model->get_user_registration(JFactory::getUser()->id)->brokerage_license) {
			$bl_license = $this->decrypt( $model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ;
		} else {
			$bl_license =  '';
		}
		$mobile = $model->get_mobile_numbers(JFactory::getUser()->id);
		$work = $model->get_work_numbers(JFactory::getUser()->id);
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('country_list', $model->get_countries());
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);
		$view->assignRef('city', $model->get_user_registration(JFactory::getUser()->id)->city);
		$view->assignRef('state', $model->get_user_registration(JFactory::getUser()->id)->state);
		$view->assignRef('broker', $broker);	
		$view->assignRef('broker_id', $broker_id);	
		$view->assignRef('al_license',  $al_license);
		$view->assignRef('volume_2012', $model->get_user_registration(JFactory::getUser()->id)->volume_2012) ;
		$view->assignRef('sides_2012', $model->get_user_registration(JFactory::getUser()->id)->sides_2012) ;
		$view->assignRef('volume_2013', $model->get_user_registration(JFactory::getUser()->id)->volume_2013) ;
		$view->assignRef('sides_2013', $model->get_user_registration(JFactory::getUser()->id)->sides_2013) ;
		$view->assignRef('volume_2014', $model->get_user_registration(JFactory::getUser()->id)->volume_2014) ;
		$view->assignRef('sides_2014', $model->get_user_registration(JFactory::getUser()->id)->sides_2014) ;
		$view->assignRef('verified_2012', $model->get_user_registration(JFactory::getUser()->id)->verified_2012);
		$view->assignRef('verified_2013', $model->get_user_registration(JFactory::getUser()->id)->verified_2013);
		$view->assignRef('verified_2014', $model->get_user_registration(JFactory::getUser()->id)->verified_2014);
		$view->assignRef('is_premium', $is_premium);
		$view->assignRef('street_address', $model->get_user_registration(JFactory::getUser()->id)->street_address);
		$view->assignRef('suburb', $model->get_user_registration(JFactory::getUser()->id)->suburb);
		$view->assignRef('user_type', $user_type->user_type);
		$view->assignRef('mobile', $mobile[0]->value);	
		$view->assignRef('work', $work[0]->value);	
		$view->display($this->getTask());
		
	}
	
	public function continue_express(){

		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$model = $this->getModel('UserProfile');
		$user_type = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		$broker = $model->get_broker($model->get_user_registration(JFactory::getUser()->id)->brokerage);
		$broker_id = $model->get_broker_id(JFactory::getUser()->email);
		if($model->get_user_registration(JFactory::getUser()->id)->licence) {
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '';
		}
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);
		$view->assignRef('city', $model->get_user_registration(JFactory::getUser()->id)->city);
		$view->assignRef('state', $model->get_user_registration(JFactory::getUser()->id)->state);
		$view->assignRef('broker', $broker);	
		$view->assignRef('broker_id', $broker_id);	
		$view->assignRef('al_license',  $al_license);
		$view->assignRef('volume_2014', $model->get_user_registration(JFactory::getUser()->id)->volume_2014) ;
		$view->assignRef('sides_2014', $model->get_user_registration(JFactory::getUser()->id)->sides_2014) ;
		$view->assignRef('verified_2014', $model->get_user_registration(JFactory::getUser()->id)->verified_2014);
		$view->assignRef('is_premium', $is_premium);
		$view->assignRef('user_type', $user_type->user_type);
		$view->display($this->getTask());
		
	}



	public function payments(){
		$model = $this->getModel('UserProfile');
		if(!empty($model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ){
			$bl_license = $this->decrypt( $model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ;
		} else {
			$bl_license =  '&nbsp';
		}

		if(!empty($model->get_user_registration(JFactory::getUser()->id)->licence) ){
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '&nbsp';
		}

		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('is_premium_invited', $model->check_user_invitation( $model->get_user_registration(JFactory::getUser()->id)->email ));
		$view->assignRef('country_list', $model->get_countries());
		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);	
		$view->assignRef('bl_license', $bl_license);	
		$view->assignRef('al_license', $al_license);
		$view->assignRef('payment_options', $model->get_payment_options());
		$view->assignRef('referral_fees', $model->get_referral_fees());
		$view->display($this->getTask());



	}	


	




	public function term_acceptance(){



		$model = $this->getModel($this->getName());



		$view = &$this->getView($this->getName(), 'html');		



		$view->display($this->getTask());



	}



	



	public function verify_terms(){



		$model = $this->getModel($this->getName());



		$model->update_terms_verification();



		// welcome message



		$emailSubject	= JText::sprintf('Welcome to AgentBridge!');



		$emailBody = "Dear ".$model->get_user_registration(JFactory::getUser()->id)->firstname.",<br /><br /> Congratulations.  You are now a member of AgentBridge, the most powerful network of real estate professionals in the world.<br /><br />



				Click this link to access your account and take advantage of your membership privileges.<br />



				<a href='https://www.agentbridge.com'>AgentBridge.com</a>



				<br /><br /> Sincerely, <br/><br/>The AgentBridge Team<br/><a href='mailto:service@agentbridge.com'>service@AgentBridge.com</a>";



		



		// Send the registration email.



		$sendmail = JFactory::getMailer();



		$sendmail->isHTML( true );



		if($sendmail->sendMail('no-reply@agentbridge.com', 'Agent Bridge', $model->get_user_registration(JFactory::getUser()->id)->email, $emailSubject, $emailBody)){

			$this->emailTermsAccepted($model->get_user_registration(JFactory::getUser()->id)->user_id);

			$return = true;



		}		



		JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));



	}


	function emailTermsAccepted($userId){

				
				// Get User Activation
				
				//echo "connected";
				
				$actid = $userId;

				$model = $this->getModel('UserProfile');

				$result = $model->get_user_details(array('*'), $actid);
				
				echo $result->brokerage;
				
				echo "connected";

				if ($result->brokerage!='') {
				
					$broker_result = $model->get_broker_data($result->brokerage);
					
				} else {

					$broker_result = "not available";
				
				}
				
				$body = "";

				$subject = "New Terms Accepted Agent: ".stripslashes($result->firstname)." ".stripslashes($result->lastname);

				$body .= "Environment: <strong>QA</strong>";
				
				$body .= "<br/>";
				
				$body .= "Member firstname: ".stripslashes($result->firstname);

				$body .= "<br/>";

				$body .= "Member lastname: ".stripslashes($result->lastname);

				$body .= "<br/>";

				$body .= "Email address: ".$result->email;

				$body .= "<br/>";

				$body .= "User Type: ".$result->user_type;

				$body .= "<br/>";

				$body .= "Brokerage: ".$broker_result->broker_name;	

				$body .= "<br/>";

				$body .= "Charter: ";
				
				$body .= $result->is_premium == 1 ? "Yes" : "No";			

				date_default_timezone_set( 'America/Los_Angeles' );
				$add_date= date('m-d-Y, h:i:s A');

				$body .= "<br/>";

				$body .= "Date/Time Terms Accepted: ".$add_date;				
				
				$body .= "<br/>";
				
				$mailSender = JFactory::getMailer();
				
				$recipient = array( 'redler@agentbridge.com', 'ebooker@agentbridge.com', 'focon@agentbridge.com');
				
				$cc = "rebekah.roque@keydiscoveryinc.com";
				
				$bcc = "mark.obre@keydiscoveryinc.com";
				
				$mailSender->sendMail('no-reply@agentbridge.com', 'AgentBridge', $recipient, $subject, $body, true, $cc, $bcc);

		

	}


	public function save_profile() {

		$model = $this->getModel('UserProfile');
		$usertype = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		
		if (!empty($_POST['jform']['sides_2012'])) {
			$sides_2012 = str_replace(",","",$_POST['jform']['sides_2012']);
		} else {
			$sides_2012 = 0;
		}		
		if (!empty($_POST['jform']['volume_2012'])) {
			$volume_2012 = str_replace("$", "", (str_replace(",","",$_POST['jform']['volume_2012'])));	
		} else {
			$volume_2012 = 0;
		}
		if (!empty($_POST['jform']['volume_2013'])) {
			$volume_2013 = str_replace("$", "", (str_replace(",","",$_POST['jform']['volume_2013'])));	
		} else {
			$volume_2013 = 0;
		}

		if (!empty($_POST['jform']['sides_2013'])) {
			$sides_2013 = str_replace(",","",$_POST['jform']['sides_2013']);
		} else {
			$sides_2013 = 0;
		}
		
		if (!empty($_POST['jform']['volume_2014'])) {
			$volume_2014 = str_replace("$", "", (str_replace(",","",$_POST['jform']['volume_2014'])));
		} else {
			$volume_2014 = 0;
		}
		
		if (!empty($_POST['jform']['sides_2014'])) {
			$sides_2014 = str_replace("$", "", (str_replace(",","",$_POST['jform']['sides_2014'])));
		} else {
			$sides_2014 = 0;
		}
		if ($_POST['jform']['terms'] == 1) {
			$date = date('Y-m-d H:i:s',time());
		} else {	
			$date = "0000-00-00 00:00:00";
		}
		if (!empty($_POST['jform']['brokerage_license'])) {
			$brokerageLicense = $this->encrypt($_POST['jform']['brokerage_license']);
		} else {
			$brokerageLicense = '';
		}
		if (!empty($_POST['jform']['agent_license_number'])) {
			$agentLicense = $this->encrypt($_POST['jform']['agent_license_number']);
		} else {
			$agentLicense = '';
		}
		$model->update_single_numbers(
			$_POST['jform']['mobile'], 
			$_POST['jform']['work'],
			$_POST['jform']['country'],
			$_POST['jform']['zip'],
			$_POST['jform']['city'],
			$agentLicense,
			$brokerageLicense,
			$_POST['jform']['brokerage'],
			$_POST['jform']['state'],
			$_POST['jform']['street'],
			$_POST['jform']['suburb'],
			$volume_2012,
			$sides_2012,
			$volume_2013,		
			$sides_2013,				
			$volume_2014,			
			$sides_2014,
			$_POST['jform']['terms'],		
			$date
	
		);


		if ($is_premium== 1) {
				echo "redirect";
				//$this->emailTermsAccepted($model->get_user_registration(JFactory::getUser()->id)->user_id);
		} else {
				echo "payments";

		}
		die();

	}
	
	


	

	public function complete(){



		$model = $this->getModel('UserProfile');



		$user_object = new JObject();



		$user_object->image = $_POST['jform']['imagePath'];



		$user_object->email = JFactory::getUser()->email;



		$model->updateImage($user_object);



		$model->getImage();



		$model->update_about(JFactory::getUser()->email, $_POST['jform']['about']);



		$model->update_designations(JFactory::getUser()->id, $_POST['jform']['designations']);



		$model->update_languages(JFactory::getUser()->id, $_POST['jform']['languages']);



		JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_userprofile&task=profile"));



	}



	public function start(){



		$model = $this->getModel($this->getName());



		$view = &$this->getView($this->getName(), 'html');



		$suggestions = $model->suggest(JFactory::getUser()->email);



		if(count($suggestions) > 1)



			$view->assignRef('suggestions', $suggestions);



		else



			JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));



		$view->display($this->getTask());



	}



	



	public function download_vcard(){



		header('Content-Type: text/x-vcard; charset=utf-8');



		echo "BEGIN:VCARD\n";



		echo "VERSION:3.0\n";



		echo "FN:Kenny Alameda\n";



		echo "N:Alameda Kenny;;;\n";



		echo "EMAIL;TYPE=INTERNET;TYPE=HOME:ClipperOil@aol.com\n";



		echo "TEL;TYPE=CELL:+1-619-540-0884\n";



		echo "ADD:\n";



		echo "END:VCARD\n";



		echo "\";\n";



		die();



	}



	



	public function addcontacts(){



		$model = $this->getModel($this->getName());



		foreach($_POST['jform']['contact'] as $contact){



			$model->request_contact_network( $contact );





		}





		JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_activitylog"));



	}



	public function profile(){


		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));

		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
	  


		// sanitize data!!! vulnerable if not
		

		$is_premium_notification_hidden = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['invitation']);
		if( $is_premium_notification_hidden==1 ) {
			$session =& JFactory::getSession();
			$session->set( 'is_premium_notification_hidden', $is_premium_notification_hidden );
		}

		$model = $this->getModel($this->getName());
		$view = &$this->getView($this->getName(), 'html');
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		$userid = (isset($_GET['uid'])) ? $_GET['uid'] : JFactory::getUser()->id;
		$own = (isset($_GET['uid'])) ? false : true;
		$user_info = $model->get_profile_info($userid, $own);
		$current_user_info = $model->get_profile_info(JFactory::getUser()->id, $own);		
		$current_user_reg = $model->get_user_registration(JFactory::getUser()->id);
		$invitation = $model->get_invitation($userid);
		
		$prog_count = 0;

		if(JFactory::getUser()->id==$userid){
			$time = strtotime("-1 year", time());
  			$prev_year = date("Y", $time);

			$progressData = $model->getProgressData($userid);
			$prog_count = 0;


			if($progressData[0]->brokerage_license || $progressData[0]->is_term_accepted){
				$prog_count++;
			}


			if($progressData[0]->desig_count || $progressData[0]->desig_count ||  $progressData[0]->lang_count){
				$prog_count++;
			}

			if($progressData[0]->volume_{$prev_year} && $progressData[0]->sides_{$prev_year}){
				$prog_count++;
			}

		}

		foreach ($invitation as $invite)
			$invitee =$model->get_invitee($invite->invited_email);

		$user_info->countryId = $current_user_info->country;
		$user_info->countryIso = $model->getCountryIso($current_user_info->country);
		$user_info->country = (is_numeric($user_info->country)) ? $model->getCountry($user_info->country) : $user_info->country;
		$user_info->state = (is_numeric($user_info->state)) ? $model->getState($user_info->state) : $user_info->state;
		$user_info->mobile_numbers = $model->get_mobile_numbers($userid, $own);
		$user_info->fax_numbers = $model->get_fax_numbers($userid, $own);
		$user_info->work_numbers = $model->get_work_numbers($userid, $own);
		$user_info->languages = $model->get_user_spoken_language($userid, $own);
		$user_info->listings_count = $model->count_all_listings($userid);
		$user_info->works_zip = $model->get_pocket_zips($userid);
		$user_info->works_zip_b = $model->get_buyer_zips($userid);
		$user_info->network = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 1));
		$user_info->pending = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 0));
		$user_info->declined = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 2));
		if(!empty($user_info->brokerage) && $user_info->brokerage){
			$user_info->brokerage_label = $model->get_broker($user_info->brokerage);
		}
		$user_info->inprogress_listings = $model->get_listings($userid, 0, 0);
		$user_info->closed_listings = $model->get_listings($userid, 1, 1);
		$model->detect_expired_pops($userid, 0, 0);
		$countbuyer = $model2->count_user_buyers();
		$user_info->countbuyer = $countbuyer;
		$refin = $model2->get_referral_in($userid, $_POST['filter']);
		if(count($refin))
			foreach($refin as $ref){
				$userreg = $model->get_user_registration($ref->agent_a);
				$ref->other_user = $model->get_user($ref->agent_a);
				$ref->other_user->userregsa = $model->get_user_registration($ref->agent_a);
				if($ref->currency!=$current_user_reg->currency){
					$ref->price_1=$model2->getExchangeRates($ref->price_1,$current_user_reg->currency,$ref->currency);
					if($ref->price_2){
						$ref->price_2=$model2->getExchangeRates($ref->price_2,$current_user_reg->currency,$ref->currency);
					}		
					$ref->correctcurr = $current_user_reg->currency;
				} else {
					$ref->correctcurr = $ref->currency;
				}

				$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model->getState($userreg->state): $userreg->state);
				$ref->other_user = $model->get_user($ref->agent_a);
				$ref->this_user_currency = $current_user_reg->currency;
				$ref->other_user->desigs = $model->get_user_designations($ref->agent_a);
				$ref->history =  $model2->get_referral_history($ref->referral_id);
				if($current_user_reg->country=="38"){
					$d_format = "Y-m-d h:m:s A";
				} else {
					$d_format = "m/d/Y h:m:s A";
				}
				$ref->history[0]->created_date = date($d_format,strtotime($ref->history[0]->created_date));
				$ref->referral_info = $model2->get_referral_info($ref->referral_id);
			}
		$refout = $model2->get_referral_out($userid, $_POST['filter']);
		if(count($refout))
			foreach($refout as $ref){
				$userreg = $model->get_user_registration($ref->agent_b);
				$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model->getState($userreg->state): $userreg->state);
				$ref->other_user = $model->get_user($ref->agent_b);
				$ref->other_user_currency = $userreg->currency;
				$ref->other_user->desigs = $model->get_user_designations($ref->agent_b);
				$ref->history =  $model2->get_referral_history($ref->referral_id);
				if($current_user_reg->country=="38"){
					$d_format = "Y-m-d h:m:s A";
				} else {
					$d_format = "m/d/Y h:m:s A";
				}
				$ref->history[0]->created_date = date($d_format,strtotime($ref->history[0]->created_date));
				$ref->referral_info = $model2->get_referral_info($ref->referral_id);
				$ref->correctcurr = $ref->currency;
			}
		$user_info->refin = $refin;
		$user_info->refout = $refout;
		$view->assignRef('invitation', $invitation);	
		$view->assignRef('invitee', $invitee);
		$view->assignRef('user_info', $user_info);
		$network_temp = $model->get_network(JFactory::getUser()->id, 1, $search);
		$str=strip_tags(JRequest::getVar('searchstringnetwork'));
		$network = array();
		
		//initialize array result

		$view->assignRef('prog_count', $prog_count);
		$view->assignRef('network', $network);
		$view->assignRef('designations', $model->get_user_designations($userid, $own));
		$is_term_accepted = $model->check_if_terms_accepted();	
		$usertype = $model->get_user_type(JFactory::getUser()->email); 
		$security = $model->get_security(JFactory::getUser()->id);
		$view->display($this->getTask());

	}



	public function request_network(){



		$other_user_id = $_REQUEST['id'];



		$property_id = $_REQUEST['pid'];



		$model = $this->getModel($this->getName());



		if ( isset( $property_id ) && !empty($property_id) ){



			$model->request_network_pid($other_user_id, $property_id);



		} else {



			$model->request_network($other_user_id);



		}

		

		

		echo 'success';



		die();



	}

	public function request_network_raw(){


		$userId = $_GET['userId'];

		$other_user_id = $_GET['otherUserId'];

		$property_id = $_GET['listingId'];



		$model = $this->getModel($this->getName());


		if ( isset( $property_id ) && !empty($property_id) ){

			$model->request_network_pid_WS($userId, $other_user_id, $property_id);

		} else {

			$model->request_network_WS($userId, $other_user_id);

		}


		//echo 'success';

		if(count($rows) == 0) {
			$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
		}
		else {		
			$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
		}

		echo json_encode($response);




	}

	public function sendmail(){



		$model = $this->getModel($this->getName());



		$model->sendpropertyrequest_mail('866', '970', '82');



	}



	public function changepass(){



		$model = $this->getModel('UserProfile');

		

		if($_POST['jform']['form']=="change_pass"){



			$app = JFactory::getApplication();



			$oldpassword = $_POST['jform']['passwordold'];



			$user = JFactory::getUser();


			$username = $user->username;
			$original_password = $user->password;

			$hashparts = explode (':' , $original_password);

			$userhash = md5($oldpassword.$hashparts[1]);

			// Get a database object
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true)
				->select('id, password')
				->from('#__users')
				->where('username=' . $db->quote($username));

			$db->setQuery($query);
			$result = $db->loadObject();


			$match = JUserHelper::verifyPassword($oldpassword, $result->password, $result->id);

			if($match){

				if($_POST['jform']['password1']==$_POST['jform']['password2']){



					$model->insertUpdatePassword(JFactory::getUser()->id);



					$new_user = new JObject();



					$new_user->id = $user->id;



					$newpass = md5($_POST['jform']['password1'].$hashparts[1]);



					$new_user->password = JUserHelper::hashPassword($_POST['jform']['password1']);



					$result = JFactory::getDbo()->updateObject('#__users', $new_user, 'id');

					

					$app->redirect(JRoute::_('index.php?option=com_activitylog'));



				}



				else



					$app->enqueueMessage('Passwords do not match', 'error');



			}



			else{



				$app->enqueueMessage('Incorrect password', 'error');



			}



		}



		$view = &$this->getView($this->getName(), 'html');



		$view->display($this->getTask());



	}



	public function contact(){



		$model = $this->getModel('UserProfile');



		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);



		$user_info->country = (is_numeric($user_info->country)) ? $model->getCountry($user_info->country) : $user_info->country;



		$user_info->state = (is_numeric($user_info->state)) ? $model->getState($user_info->state) : $user_info->state;



		$user_info->mobile_numbers = $model->get_mobile_numbers(JFactory::getUser()->id, true);



		$user_info->fax_numbers = $model->get_fax_numbers(JFactory::getUser()->id, true);



		$user_info->work_numbers = $model->get_work_numbers(JFactory::getUser()->id, true);



		$user_info->websites = $model->get_websites(JFactory::getUser()->id, true);



		$user_info->socialsites = $model->get_socialsites(JFactory::getUser()->id, true);



		$view = &$this->getView($this->getName(), 'html');



		$view->assignRef('user', $user_info);



		$view->assignRef('contact_options', $model->get_contact_options(JFactory::getUser()->id));



		$view->assignRef('notiftypes', $model->get_payment_methods());



		$view->display($this->getTask());



	}



	public function emailsnotif(){



		$model = $this->getModel('UserProfile');



		$view = &$this->getView($this->getName(), 'html');



		//$view->assignRef('contact_options', $model->get_contact_options(JFactory::getUser()->id));



		$view->assignRef('contact_options', $model->retrieveNotifcations());



		$view->assignRef('notiftypes', $model->get_payment_methods());



		$view->assignRef('user', $model->get_profile_info(JFactory::getUser()->id, true));



		$view->display($this->getTask());



	}



	



	public function update_single_designation(){



		$model 	= $this->getModel('UserProfile');



		if( JRequest::getVar('desgination_id')!="" ) {



			$designation_id = JRequest::getVar('desgination_id');



			$model->update_single_designation( $designation_id );



		}



		die();



	}



	



	//delete designation on delete click



	public function delete_single_designation(){



		$model 			= $this->getModel('UserProfile');



		$designation_id = $_POST['desgination_id'];



		$model->delete_single_designation( $designation_id );



		die();



	}



	

	public function edit(){



		$model = $this->getModel('UserProfile');



		if($_POST['jform']['form']=="user_edit"){



			$model->update($_POST['jform']);



			die();



		}



		else if($_POST['jform']['form']=="contact_edit"){



			$model->update($_POST['jform'], true);



	

			JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=contact'));



		}



		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);

		

		$user_info->email = $model->get_user_registration(JFactory::getUser()->id, true)->email ;

		

		$user_info->mobile_numbers = $model->get_mobile_numbers(JFactory::getUser()->id, true);



		$user_info->fax_numbers = $model->get_fax_numbers(JFactory::getUser()->id, true);



		$user_info->work_numbers = $model->get_work_numbers(JFactory::getUser()->id, true);



		$user_info->websites = $model->get_websites(JFactory::getUser()->id, true);



		$user_info->socialsites = $model->get_socialsites(JFactory::getUser()->id, true);



		$user_info->brokerage_license = $model->get_user(JFactory::getUser()->id, true)->brokerage_license;

		

		$user_info->volume_2012 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->volume_2012) ;

		

		$user_info->volume_2013 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->volume_2013 );
		
		
		$user_info->volume_2014 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->volume_2014 );

		

		$user_info->sides_2012 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->sides_2012 ) ;

		

		$user_info->sides_2013 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->sides_2013 );
		
		
		$user_info->sides_2014 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->sides_2014 );

		

		$user_info->ave_price_2012 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->ave_price_2012) ;

		

		$user_info->ave_price_2013 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->ave_price_2013) ;
		
		
		$user_info->ave_price_2014 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->ave_price_2014) ;

		

		$user_info->verified_2012 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->verified_2012) ;

		

		$user_info->verified_2013 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->verified_2013) ;

		
		$user_info->verified_2014 = (int) str_replace(",", "", $model->get_user(JFactory::getUser()->id, true)->verified_2014) ;

	



		if($user_info->brokerage)



			$user_info->brokerage_label =  $model->get_broker($user_info->brokerage);



		$user_info->languages = array_map(function($obj){ return $obj->lang_id; }, $model->get_user_spoken_language(JFactory::getUser()->id));



		$view = &$this->getView($this->getName(), 'html');



		$view->assignRef('user', $user_info);



		$country = $model->getCountryIso($user_info->country);



		$view->assignRef('countyIso', $country);



		$view->assignRef('country_list', $model->get_countries());



		$view->assignRef('languages_list', $model->get_languages());



		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));



		$view->assignRef('designations', $model->get_user_designations(JFactory::getUser()->id, true));



		$view->display($this->getTask());



	}

	public function sampleSend(){

		$model = $this->getModel($this->getName());

		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);

	    $sides=0;
		$volumes=0;
		$year = "";
		$changes = array();

		$model->sendVolumeChanges($user_info,$year,$changes,$volume,$sides);

	}

	public function contacts(){



		$view = $this->getView($this->getName(),'html');



		$model = $this->getModel($this->getName());



		$network_temp = $model->get_network(JFactory::getUser()->id, 1, $search);

	



		// search sting goes here!



		//$str=$_POST['searchstringnetwork'];



		$str=strip_tags(JRequest::getVar('searchstringnetwork'));



		



		$network = array();



		



		//initialize array result



		$search_resultx = array();



		foreach ($network_temp as $single_network){



			// query actual agents contacts



			$contact = $model->get_profile_info($single_network->other_user_id, false);



			



			// query for search 



			$search_result =  $model->search_profile_info($single_network->other_user_id, false, $str);



			if(isset($_POST['searchstringnetwork']) && $_POST['searchstringnetwork']!=""){



				



				echo "<pre>";



				//print_r($contact);



				



				echo "</pre>";



				if(strpos($contact->zip, $str));



				//zip city state country brokerage



			}



			



			// collate results if any



			$search_resultx[] = $search_result;



			$network[] = $contact;



		}





		$view->assignRef('search_string', $str);



		



		// final search result



		$view->assignRef('search_result', $search_resultx);			



		$view->assignRef('network', $network);



		$view->assignRef('user', $model->get_profile_info(JFactory::getUser()->id, true));



		$view->display($this->getTask());

		

		//$this->get_invitation_list();



	}



	public function requestaccess(){



		$model = $this->getModel($this->getName());



		$model->sendrequestaccess($_POST['uid'], $_POST['pid'], JFactory::getUser()->id);



		$model->sendrequest_mail_pid($_POST['uid'], JFactory::getUser()->id, $_POST['pid']);



		die();



	}


	public function requestaccess_raw(){


		$userId = $_GET['userId'];

		$otherUserId = $_GET['otherUserId'];

		$listingId = $_GET['listingId'];


		$model = $this->getModel($this->getName());

		$model->sendrequestaccess($otherUserId, $listingId, $userId);

		$model->sendrequest_mail_pid($otherUserId, $userId, $listingId);

		$response = array('status'=>1, 'message'=>"Request Sent", 'data'=>array());

		echo json_encode($response);


	}



	public function update_brokerage(){



		$broker_id 	= $_POST['broker_id'];



		$country_id = $_POST['country_id']; 



		$model 		= $this->getModel($this->getName());



		echo json_encode( $model->updatebrokerage_info( $_SESSION['uid'], $country_id,  $broker_id ) );



		die();



	}



	



	public function broanddesig(){



		$view = &$this->getView($this->getName(), 'html');



		$model = $this->getModel($this->getName());



		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);



		$w_tax =$model->get_broker_tax_id($user_info->user_id);

		

		if($w_tax !="")

			$user_info->tax_id =  $this->decrypt($w_tax);



		$user_info->country_name =  $model->getCountry($user_info->country);



		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));



		$view->assignRef('designations', $model->get_user_designations(JFactory::getUser()->id, true));



		$view->assignRef('country', $model->get_user_registration(JFactory::getUser()->id)->country);



		$view->assignRef('user', $user_info);



		$view->display($this->getTask());



	}



	public function savebroker(){



		$model = $this->getModel($this->getName());



		if($model->updatebrokerage(JFactory::getUser()->id, $_REQUEST['brokerid']))



			echo 'success';



		else



			echo 'failed';



		die();



	}



	public function acceptrequest(){



		$db = JFactory::getDbo();



		$edit = new JObject();



		$edit->permission = 1;



		$edit->pkId = $_POST['id'];



		//echo "pk id: " . $_POST['id']; die();



		$result = $db->updateObject('#__request_access', $edit, 'pkId');



		$insert_activity = new JObject();



		$insert_activity->activity_type = (int)7;



		$insert_activity->user_id =  $_POST['other'];



		$insert_activity->activity_id =  $_POST['id'];



		$insert_activity->date = date('Y-m-d H:i:s',time());



		$insert_activity->other_user_id = JFactory::getUser()->id;



		$ret_act = $db->insertObject('#__activities', $insert_activity);



		die();



	}



	public function getlanguage($lid){



		$db = JFactory::getDbo();



		$query= $db->getQuery(true);



		$query->select('language')



		->from('#__user_languages')



		->where('lang_id = '.$lid);



		$db->setQuery($query);



		return $db->loadObject()->language;



	}



	public function getdesignation($lid){



		$db = JFactory::getDbo();



		$query= $db->getQuery(true);



		$query->select('designations')



		->from('#__designations')



		->where('id = '.$lid);



		$db->setQuery($query);



		return $db->loadObject()->designations;



	}



	public function unmap($column){



		$selected = trim($column);



		if($selected=="cell"){



			return "Mobile Phone";



		}



		else if($selected=="s_languages"){



			return "Languages";



		}



		else if($selected=="designations"){



			return "Designations";



		}



		else if($selected=="about"){



			return "About me";



		}



		else if($selected=="email"){



			return "Email";



		}



		else if($selected=="wfax"){



			return "Fax";



		}



		else if($selected=="wphone"){



			return "Work Phone";



		}



		else if($selected=="city"){



			return "City";



		}



		else if($selected=="zip"){



			return "Zip";



		}



		else if($selected=="country"){



			return "Country";



		}



		else if($selected=="street_address"){



			return "Address line 1";



		}



		else if($selected=="brokerage"){



			return "Brokerage";



		}



		else if($selected=="state"){



			return "State";



		}



		else if($selected=="suburb"){



			return "Address line 2";



		}



		else if($selected="imagelink"){



			return "Image link";



		}



		else if($selected="image"){



			return "Image";



		}



	}



	public function updatelisting(){



		$model = &$this->getModel('UserProfile');



		$listing_owner = $model->getListingOwner($_REQUEST['id']);



		if(JFactory::getUser()->id != $listing_owner)



			echo json_encode(array('code'=>0, 'value'=>'you do not have permission to edit this listing'));



		else {



			$date = date('Y-m-d', strtotime($_REQUEST['value']));



			$lisiting_id = $_REQUEST['id'];



			



			$ts1 = strtotime(date('Y-m-d'));



			$ts2 = strtotime($_REQUEST['value']);



			$seconds_diff = $ts2 - $ts1;



			$days = floor($seconds_diff/3600/24);



			



			



			if($days>0) {



				$model->updatePropertyExpiry(



					$lisiting_id , 



					$date



				);



				echo json_encode(array('code'=>1, 'days' => $days." ", 'value'=>'You have successfully update the expiry of your listing to '.$days.' days'));



			} else {



				echo json_encode(array('code'=>0, 'value'=>'You cannot set the expiry to this date'));



			}



			

		}



		die();



	}



	public function update_setting(){



		$model = &$this->getModel('UserProfile');



		$update_object = new JObject();



		$update_object->show = $_REQUEST['value'];



		$update_object->pk_id = $_REQUEST['pkid'];



		$model->insertIfnotExists($update_object, 'pk_id', $_REQUEST['table']);



		echo json_encode($_REQUEST);



		die();



	}



	public function savenotifs(){



		$notiftype = new JObject();



		$notiftype->contact_method = $_REQUEST['select2'];



		$notiftype->contact_options = serialize($_REQUEST['chbox']);



		$notiftype->user_id = JFactory::getUser()->id;



		$model = &$this->getModel('UserProfile');



		$model->insertIfnotExists($notiftype, 'user_id', '#__user_contact_method');



		//JFactory::getApplication()->enqueueMessage('You have successfully updated your notification settings' ,'success');



		//$this->setRedirect(JRoute::_('index.php?option=com_userprofile&task=contact'));



		$this->setRedirect(JRoute::_('index.php?option=com_activitylog'));



	}



	



	public function saveNotification() {



		$values = json_encode( JRequest::getVar('chbox') );



		$model  = &$this->getModel('UserProfile');



		$model->saveNotification( JFactory::getUser()->id, $values );



		die();



	}



	public function search(){



		$view = $this->getView($this->getName(), 'html');



		$view->display('guestview');



	}



	public function searchresult(){



		$columns = array();



		$columns['p.bedroom']['regex']=	'/((with)|([0-9]+)) bedroom/';



		$columns['p.bathroom']['regex']=	'/((with)|([0-9]+)) baths/';



		$columns['p.view']['regex']=	'/(none)|(panoramic)|(city)|(mountains\/hills)|(coastline)|(coastline)|(water)|(ocean)|(lake\/river)|(landmark)|(desert)|(bay)|(vineyard)|(golf)|(other)/';



		$columns['p.style']['regex']=	'/(american farmhouse)|(art deco)|(art modern\/mid century)|(cape cod)|(colonial revival)|(contemporary)|(craftsman)|(french)|(italian\/tuscan)|(prairie style)|(pueblo revival)|(ranch)|(spanish\/mediterranean)|(swiss cottage)|(tudor)|(victorian)|(historic)|(artichitucaul significnat)|(green)/';



		$columns['p.pool_spa']['regex']=	'/(with )?((pool)|(pool and spa)|(spa)|(spa and pool))/';



		$columns['p.condition']['regex']=	'/(fixer)|(good)|(excellent)|(remodeled)|(new construction)|(under construction)|(not disclosed)/';



		$columns['p.garage']['regex']=	'/((with)|([0-9]+)) garage/';



		$columns['p.units']['regex']=	'/(triplex)|(quad)|(land)/';



		$columns['p.occupancy']['regex']=	'/([0-9]+ occupants)/';



		$columns['p.type']['regex']=	'/(office)|(institutinal)|(medical)|(warehouse)|(condo)|(r&d)|(business park)|(land)|(flex space)|(manufacturing)|(office showroom)|(self\/mini storage)|(truck terminal\/hub)|(distribuiotn)|(cold storage)|(community center)|(strip center)|(outlet center)|(power center)|(anchor)|(restaurant)|(service station)|(retail pad)|(free standing)|(day care\/nursery)|(post office)|(vehicle)|(economy)|(full service)|(assisted)|(acute care)|(golf)|(marina)|(theater)|(religious)/';



		$columns['p.listing_class']['regex']=	'/(class [a-eA-E])/';



		$columns['p.stories']['regex']=	'/([0-9]+ ((story)|(stories)))/';



		$columns['p.room_count']['regex']=	'/([0-9]+ (rooms?))/';



		$columns['p.type_lease']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';



		$columns['p.type_lease2']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';



		$columns['p.term']['regex']=	'/(blank)|(short term)|(m to m)|(year lease)|(multi year lease)/';



		$columns['p.furnished']['regex']=	'/(furnished)|(unfurnished)/';



		$columns['p.pet']['regex']=	'/(pet)/';



		$columns['p.zoned']['regex']=	'/((with)|([0-9]+)) units/';



		$columns['p.bldg_type']['regex']=	'/(north facing)|(south facing)|(east facing)|(west facing)|(low rise)|(mid rise)|(high rise)|(co-op)|(undisclosed)|(detached)|(attached)/';



		$columns['p.features1']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';



		$columns['p.features2']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';



		$columns['p.features3']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';



		$filters = array();



		echo "<pre>";



		foreach($columns as $key => $column){



			//echo $key.'<br/>';



			//echo $column['regex']."<br/>";



			preg_match($column['regex'], $_REQUEST['keys'], $matches);



			if(count($matches)){



				$column['matches'] = $matches[0];



				$constants = array('with', 'bedroom', 'bedrooms', 'bath', 'baths', 'stories');



				$column['processed'] = str_replace($constants, '', $matches[0]);



				$filters[$key] = $column['processed'];



				print_r($column['processed']);



			}



			//echo "______________<br/>";



			//print_r($column['matches']);



		}



		$model = $this->getModel($this->getName());



		print_r($model->get_listing($filters, "OR"));



		echo "sad";



		echo "</pre>";



		die();



	}



	



	// count current invites



	// SELECT * FROM tbl_invitation_tracker a LEFT JOIN tbl_user_registration b ON a.agent_id = b.user_id;



	public function count_current_invites(){



		$model = $this->getModel($this->getName());



		echo $model->get_current_invites_count()->current_invite_count;



		die();



	}



	



	



	// check if existing user exists



	public function invites(){



		$model = $this->getModel($this->getName());



		$email = JRequest::getVar('email');



		$user_count = $model->check_existing_accounts( $email );



		$user_details = $model->get_existing_details( $email ); 

		

		$user_type = $model->get_user_type($email);



		// pending invitation but doesn't belong to charter list



		if ( $user_type->user_type==2 && $user_count->user_id_count==1 && $user_details->activation_status==0 ) {



			echo json_encode( 



				array(



					'response' 	=> 1, 



					'firstname' => stripslashes($user_details->firstname),



					'lastname' 	=> stripslashes($user_details->lastname),



					'inviter' 	=> stripslashes($user_details->agent_id),



					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id



				) 



			);



		// manual applicant not approved yet and invited by charter



		} else if ($user_type->user_type==3 && $user_count->user_id_count==1 && $user_details->activation_status==0) {





			echo json_encode( 



				array(



					'response' 	=> 2, 



					'firstname' => stripslashes($user_details->firstname),



					'lastname' 	=> stripslashes($user_details->lastname),



					'inviter' 	=> stripslashes($user_details->agent_id),



					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id



				) 



			);



		} 



		// existing charter but not activated

		

		else if ($user_type->user_type==1 && $user_count->user_id_count==1 && $user_details->activation_status==0) {





			echo json_encode( 



				array(



					'response' 	=> 3, 



					'firstname' => stripslashes($user_details->firstname),



					'lastname' 	=> stripslashes($user_details->lastname),



					'email' 	=> $model->get_user_registration( JFactory::getUser()->id )->email



				) 



			);

	





		}

		

		// existing charter but not activated, and invited 

		

		else if ($user_type->user_type==4 && $user_count->user_id_count==1 && $user_details->activation_status==0) {





			echo json_encode( 



				array(



					'response' 	=> 4, 



					'firstname' => stripslashes($user_details->firstname),



					'lastname' 	=> stripslashes($user_details->lastname),



					'inviter' 	=> stripslashes($user_details->agent_id),



					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id



				) 



			);



		} 

		

		// activated member



		else if ($user_count->user_id_count==1 && $user_details->activation_status==1) {



			echo json_encode( 



				array(



					'response' 	=> 5, 



					'firstname' => stripslashes($user_details->firstname),



					'lastname' 	=> stripslashes($user_details->lastname),

					

					'match' 	=> $user_details->id

					



				) 



			);





		} 

		

		

		else {

		



			echo json_encode( 



				array(



					'response' 	=> 0,

					

					'count'		=> $user_count->user_id_count,

					

					'activation' => $user_details,

					

					'email' => $email



				) 



			);



			die();



		} 

	



	}



	



	public function get_invitation_list() {



		$model = $this->getModel($this->getName());



		echo json_encode( $model->get_invited_emails() );



		die();



	}



	



	public function emailInvites(){



		$model = $this->getModel($this->getName());



		$mainframe = JFactory::getApplication();



		$invited_email = JRequest::getVar('invitedEmail');



		$email = JFactory::getUser()->email;

		

		$first_name    = JRequest::getVar('firstName');



		$last_name     = JRequest::getVar('lastName');



		$zip_code      = JRequest::getVar('zipCode');



		$country_id    = JRequest::getVar('countryId');



		$model->insert_invited_emails( $invited_email, $email, $first_name, $last_name, $zip_code, $country_id );		



		$model->notifyAdminInvitation( $email, $invited_email, $first_name, $last_name, $zip_code, $country_id  );	



	}

	

	public function emailInvitesExisting(){



		$model = $this->getModel($this->getName());



		$mainframe = JFactory::getApplication();



		$agent_email = JFactory::getUser()->email;



		$email   = JRequest::getVar('invitedEmail');

		

		$first_name    = JRequest::getVar('firstName');



		$last_name     = JRequest::getVar('lastName');



		$zip_code      = JRequest::getVar('zipCode');



		$country_id    = JRequest::getVar('countryId');



		$model->insert_to_inviteusertype( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  );		



		$model->notifyAdminInvitation( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  );	



	}

	

	public function emailInvitesExistingManual(){



		$model = $this->getModel($this->getName());



		$mainframe = JFactory::getApplication();



		$agent_email = JFactory::getUser()->email;



		$email   = JRequest::getVar('invitedEmail');

		

		$first_name    = JRequest::getVar('firstName');



		$last_name     = JRequest::getVar('lastName');



		$zip_code      = JRequest::getVar('zipCode');



		$country_id    = JRequest::getVar('countryId');



		$model->insert_to_inviteusertypemanual( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  );		



		$model->notifyAdminInvitation( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  );	



	}



	public function get_current_registered_user(){



		$model = $this->getModel($this->getName());



		echo json_encode( $model->get_user_registration( JFactory::getUser()->id ) );



		die();



	}



	



	public function encrypt($plain_text) {



	



		$key = 'password to (en/de)crypt';



			



		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));



		return $encrypted;



		



	}	



	



	public function decrypt($encrypted_text) {



	$key = 'password to (en/de)crypt';



	



	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");



	



	return $decrypted;



}



	



	public function decrypt_license() {



		echo $this->decrypt(JRequest::getVar('alslno'));



		die();



	}

	

	public function decrypt_taxid() {



		$btino = $model->get_broker_tax_id(JFactory::getUser()->id);

		

		if($btino !="")

			echo $this->decrypt(JRequest::getVar('btino'));





		die();



	}



	



}



class AuthorizeNetx{



	private $login_name;



	private $transaction_key;



	private $apihost = "api.authorize.net";



	private $apipath = "/xml/v1/request.api";



	



	public function __construct($login_name, $transaction_key){



		$this->login_name = $login_name;



		$this->transaction_key = $transaction_key;



		



	}



	



	public function get_payment_profile($cs_profile_id, $cs_pp_id){



		$content =



		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .



		"<getCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .



		$this->MerchantAuthenticationBlock().



		"<customerProfileId>".$cs_profile_id."</customerProfileId>".



 		"<customerPaymentProfileId>".$cs_pp_id."</customerPaymentProfileId>".



		"</getCustomerPaymentProfileRequest>";



		$response = $this->send_xml_request($content);



		$parsedresponse = $this->parse_api_response($response);



		



		return $parsedresponse;



	}



	



	public function create_payment_profile($address, $city, $state, $zip, $country, $cs_profile_id, $fname, $lname, $number, $cardno, $cardexp, $testmode = 1){



		if($testmode)



			$mode = 'testMode';



		else



			$model = 'liveMode';



		$content =



		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .



		"<createCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .



		$this->MerchantAuthenticationBlock().



		"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".



		"<paymentProfile>".



		"<billTo>".



		"<firstName>". $fname ."</firstName>".



		"<lastName>". $lname ."</lastName>".



		"<address>". $address ."</address>".



		"<city>". $city ."</city>".



		"<state>". $state ."</state>".



		"<zip>". $zip ."</zip>".



		"<country>". $country ."</country>".



		"</billTo>".



		"<payment>".



		"<creditCard>".



		"<cardNumber>". $cardno ."</cardNumber>".



		"<expirationDate>". $cardexp ."</expirationDate>". // required format for API is YYYY-MM



		"</creditCard>".



		"</payment>".



		"</paymentProfile>".



		"<validationMode>". $mode ."</validationMode>". // or testMode



		"</createCustomerPaymentProfileRequest>";



		



		//echo $content;



		



		$response = $this->send_xml_request($content);



		$parsedresponse = $this->parse_api_response($response);



		



		return $parsedresponse;



	}



	



	public function profile_create($cs_id,$email){



		$content =



		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .



		"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .



			$this->MerchantAuthenticationBlock().



		"<profile>".



		"<merchantCustomerId>".$cs_id."</merchantCustomerId>". // Your own identifier for the customer.



		"<description></description>".



		"<email>" . $email . "</email>".



		"</profile>".



		"</createCustomerProfileRequest>";



		$response = $this->send_xml_request($content);



		$parsedresponse = $this->parse_api_response($response);



		



		return $parsedresponse;



	}



	



	public function transaction_create($ammount,$cs_profile_id,$cs_payment_id,$invoice){



		$content =



			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .



			"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .



			$this->MerchantAuthenticationBlock().



			"<transaction>".



			"<profileTransAuthOnly>".



			"<amount>" . $ammount . "</amount>". // should include tax, shipping, and everything.



			"<lineItems>".



			"<itemId>000000001</itemId>".



			"<name>Service Fee</name>".



			"<description>Service Fee collected by AgentBridge for Referral {clientname}</description>".



			"<quantity>1</quantity>".



			"<unitPrice>".$ammount."</unitPrice>".



			"<taxable>false</taxable>".



			"</lineItems>".



			"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".



			"<customerPaymentProfileId>" . $cs_payment_id . "</customerPaymentProfileId>".



			"<customerShippingAddressId>" . 0 . "</customerShippingAddressId>".



			"<order>".



			"<invoiceNumber>".$invoice."</invoiceNumber>".



			"</order>".



			"</profileTransAuthOnly>".



			"</transaction>".



			"</createCustomerProfileTransactionRequest>";



		



		$response = $this->send_xml_request($content);



		$parsedresponse = $this->parse_api_response($response);



		return $parsedresponse;



	}



	



	private function send_xml_request($content){



		return $this->send_request_via_fsockopen($content);



	}



	



	private function send_request_via_fsockopen($content){



		$posturl = "ssl://" . $this->apihost;



		$header = "Host: $this->host\r\n";



		$header .= "User-Agent: PHP Script\r\n";



		$header .= "Content-Type: text/xml\r\n";



		$header .= "Content-Length: ".strlen($content)."\r\n";



		$header .= "Connection: close\r\n\r\n";



		$fp = fsockopen($posturl, 443, $errno, $errstr, 30);



		if (!$fp)



		{



			$body = false;



		}



		else



		{



			//error_reporting(E_ERROR);



			fputs($fp, "POST $this->apipath  HTTP/1.1\r\n");



			fputs($fp, $header.$content);



			fwrite($fp, $out);



			$response = "";



			while (!feof($fp))



			{



				$response = $response . fgets($fp, 128);



			}



			fclose($fp);



			//error_reporting(E_ALL ^ E_NOTICE);



	



			$len = strlen($response);



			$bodypos = strpos($response, "\r\n\r\n");



			if ($bodypos <= 0)



			{



				$bodypos = strpos($response, "\n\n");



			}



			while ($bodypos < $len && $response[$bodypos] != '<')



			{



				$bodypos++;



			}



			$body = substr($response, $bodypos);



		}



		return $body;



	}



	



	private function parse_api_response($content){



		$parsedresponse = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOWARNING);



		return $parsedresponse;



	}



	



	private function MerchantAuthenticationBlock() {



		return



		"<merchantAuthentication>".



		"<name>" . $this->login_name . "</name>".



		"<transactionKey>" . $this->transaction_key . "</transactionKey>".



		"</merchantAuthentication>";



	}



}



?>