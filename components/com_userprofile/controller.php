<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
require_once JPATH_LIBRARIES . '/tcpdf/library/config/lang/eng.php';
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
*/
class UserProfileController extends JControllerLegacy
{
	/**
	 * Method to display the view
	 *
	 * @access    public
	 */
	function display()
	{
		$model = $this->getModel('UserProfile');
		$application = JFactory::getApplication();
		//$security = $model->get_security(JFactory::getUser()->id);
		//if ($security->uid=='') 
			//$application->redirect(JRoute::_('index.php?option=com_securityquestion'));
		$uid = JFactory::getUser()->id;
		$userinfo = $model->get_user_registration(JFactory::getUser()->id);
		
		//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    //language loading	

		if(empty($uid))
			$application->redirect('index.php?illegal=1');
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('userprofile',$userinfo);
		$view->assignRef('languages_list', $model->get_languages());
		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));
		parent::display();
	}
	function createInvoice()
	{
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		return "Fee_".JFactory::getUser()->id."_".$model->get_next_invoice()."_".rand(0, 102);
	}	
	
	function getUserOrigCountryData(){
		$model = $this->getModel('UserProfile');
		$userDetails = $model->get_user_registration(JFactory::getUser()->id);

		echo json_encode($userDetails);
	}

	function getUserOrigCountryData_WS(){
		$uid = $_GET['uid'];
		$model = $this->getModel('UserProfile');
		$userDetails = $model->get_user_registration($uid);

		echo json_encode($userDetails);
	}

	function checkUserBrowser(){

		$window_id = $_POST['window_id'];
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__user_browser_open');
		$query->where('user_id="'.JFactory::getUser()->id.'" && open=0');
		$openBrowse = $db->setQuery($query)->loadObjectList();
		$has_zero=0;

		if($openBrowse){
			foreach ($openBrowse as $key => $value) {
				if($value->browser_open != $window_id){
						$has_zero=1;
						$query = $db->getQuery(true);
				 
						// delete all custom keys for user 1001.
						$conditions = array(
						    'user_id' . ' = "'.JFactory::getUser()->id.'"', 
						    'browser_open' . ' = "' . $value->browser_open . '"'
						);
						 
						$query->delete('#__user_browser_open');
						$query->where($conditions);
						 
						$db->setQuery($query);
						$result = $db->execute();
						
				}
			}
		}

		if($has_zero){
			session_destroy();
			echo "reload";
			die();
		}


		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__user_browser_open');
		$query->where('user_id="'.JFactory::getUser()->id.'" && browser_open="'.$window_id.'"');
		$result_row = $db->setQuery($query)->loadObject();

		
		if(!$result_row){ //not exists
			 
			// Create a new query object.
			$query = $db->getQuery(true);
			 
			// Insert columns.
			$columns = array('user_id', 'browser_open', 'time_effect', 'open');
			 
			// Insert values.
			$values = array(JFactory::getUser()->id, "'".$window_id."'", time(), 1);
			 
			// Prepare the insert query.
			$query
			    ->insert('#__user_browser_open')
			    ->columns($columns)
			    ->values(implode(',', $values));
			 
			// Set the query using our newly populated query object and execute it.
			$db->setQuery($query);
			$db->execute();

		} else {


			$db = JFactory::getDbo(); 
			$query = $db->getQuery(true);
			 
			// Fields to update.
			$fields = array(
			    'time_effect = ' . time(),
			    'open = 1 ',
			);
			 
			// Conditions for which records should be updated.
			$conditions = array(
			    'browser_open = "'.$window_id.'"', 
			);
			 
			$query->update('#__user_browser_open')->set($fields)->where($conditions);
			 
			$db->setQuery($query);
			 
			$result = $db->execute();

		} 

		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__user_browser_open');
		$query->where('user_id="'.JFactory::getUser()->id.'"');
		$results = $db->setQuery($query)->loadObjectList();


		echo $results[0]->browser_open;
		var_dump($results);
		var_dump( count($results) );

		if(count($results)==1) {

				$allowed_time = 10;	
				$current_time = time();			

				$query = $db->getQuery(true);
				$query->select("*");
				$query->from('#__user_browser_open');
				$query->where('user_id="'.JFactory::getUser()->id.'"');
				$results = $db->setQuery($query)->loadObjectList();

				$lapse_time = $current_time - $results[0]->time_effect;

				if($results[0]->browser_open != $window_id){
					//session_destroy();
					//die();
				}

		}

	}

	function removeUserBrowser(){

		$window_id = $_POST['window_id'];

		//check if user_browse existing in db
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		 
		// Fields to update.
		$fields = array(
		    'time_effect = ' . time(),
		    'open = 0 ',
		);
		 
		// Conditions for which records should be updated.
		$conditions = array(
		    'browser_open = "'.$window_id.'"', 
		);
		 
		$query->update('#__user_browser_open')->set($fields)->where($conditions);
		 
		$db->setQuery($query);
		 
		$result = $db->execute();

		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__user_browser_open');
		$query->where('user_id="'.JFactory::getUser()->id.'"');
		$results = $db->setQuery($query)->loadObjectList();



		if(count($results)>1){

			$query = $db->getQuery(true);
			 
			// delete all custom keys for user 1001.
			$conditions = array(
			    'user_id' . ' = "'.JFactory::getUser()->id.'"', 
			    'browser_open' . ' = "' . $window_id . '"'
			);
			 
			$query->delete('#__user_browser_open');
			$query->where($conditions);
			 
			$db->setQuery($query);
			$result = $db->execute();

		} 



	}

	function checkWindowId(){
		$wind_id =  $_POST['window_id'];
		$active_e=	$_POST['active_e'];
		$pf_ntype=  $_POST['pf_ntype'];

		if(count($_SESSION['window'])<=0 && $_SESSION['window']!=0 && $_SESSION['window']==""){
			//$app = JFactory::getApplication('site');

			//if(JFactory::getUser()->id && $active_e=="BODY" && $pf_ntype=="0") {
			 //    $app->logout();
			     session_destroy();
			//}
		}

		if(!$wind_id && !$_SESSION['window']){
			session_destroy();
		}

		
		if(!is_array($_SESSION['window'])){
			$_SESSION['window'] =array();
		}
		array_push($_SESSION['window'],$wind_id);
		$_SESSION['window'] =array_unique($_SESSION['window']);



		
		echo json_encode($_SESSION["window"]);
		
	}


	function webhooksChimp(){
		


		$data_chimp = $_POST;
		$body ="";

		$body .=json_encode($data_chimp);
		//$body .=json_encode($data_chimp['data']);
		//$body .= print_r($data_chimp,true);
		$type=$_POST['type'];

		if($type=='unsubscribe'){
				$email = strip_tags($_POST['email']);
				//preg_match('~>\K[^<>]*(?=<)~', $email, $match);
				//$body .=print_r($match,true);
				//$email=$match;

				$db = JFactory::getDbo();
				$query = $db->getQuery(true);
				$query->select("u.email,uc.*");
				$query->from('#__users u');
				$query->leftJoin('#__user_contact_method uc ON uc.user_id = u.id');
				$query->where('u.email="'.$email.'"');
				$results = $db->setQuery($query)->loadObject();


				$rem_bracks = str_replace(array('[',']'),'',$results->contact_options);
				$co_array = explode(",",$rem_bracks);
				$co_array[3]='"0"';
				$co_string = "[".implode(",", $co_array)."]";

				$body .="<div> email=".$email.";</div>";
				$body .="<div> type=".$_POST['type'].";</div>";
				$body .="<div> c_o=".$co_string.";</div>";
				

				$contact_options = new JObject();
				$contact_options->user_id 			= $results->user_id;
				$contact_options->contact_options 	= $co_string;
				$db->updateObject('#__user_contact_method', $contact_options, 'user_id');
				$db->execute();
		}



		/*$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( $subject );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody(  $body );
			$mailSender ->Send();*/
	}
	
	function webhooksChimpQA(){

		$app = JFactory::getApplication();
		$postData = JRequest::get( 'post' );

		$data_chimp = $_POST;
		$body ="";

		$body .= print_r($_REQUEST,true);
		$body .= print_r($_POST,true);
		foreach ($postData as $key => $value) {
			$body .="<div> ".$value." </div>";
		}

		foreach ($_REQUEST as $key => $value) {
			$body .="<div> ".$value." </div>";
		}
		foreach ($_POST['data'] as $key => $value) {
			$body .="<div> $_POST\[".$key."\] = ".$value."; </div>";
		}
		$subject="Mailchimp";

		$type = 'POST';

		$to_send= array_merge($_POST['data'], $_POST);

		$ch = curl_init( "http://108.163.201.122/~forqa/index.php/component/userprofile/?task=webhooksChimp&format=raw&webservice=1" );
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		/*curl_setopt( $ch, CURLOPT_HTTPHEADER, array
		(
			'Content-Type: application/json', 
	//		'Authorization: ' . $api['login'] . ' ' . $api['key'],
			'X-HTTP-Method-Override: ' . $type,
		) );*/

		//curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,  $to_send  );

		//if( $data_chimp )
		//	curl_setopt( $ch, CURLOPT_POSTFIELDS,  $data_chimp  );

		$response = curl_exec( $ch );
		curl_close( $ch );

		//return 

		$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( $subject );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody(  json_encode( $data_chimp ) );
			$mailSender ->Send();
	}

	function subsChimp(){
		$subs_val = $_POST['newsl_val'];
		$model = $this->getModel('UserProfile');

		$user_details=array();
		$ureg = $model->get_user_registration(JFactory::getUser()->id);
		$result = $model->get_user_details_bc(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $ureg->user_id);
		//$user_details['email'] = JFactory::getUser()->email;		
		$user_details = array(
			'email' => $result->email, 
			'fname' => $result->firstname,
			'lname' => $result->lastname,
			'broker' => $result->broker_name,
			'state' => $result->zone_code,
			'country' => $result->countries_iso_code_3,
			'zip' => $result->zip,
			'city' => $result->city,
		);
		
		$chimpStatus = json_decode($model->checkUserChimp($user_details));
		
		if($subs_val==0){
			if($chimpStatus->status == "subscribed") {
				$user_details['subs_val'] = "unsubscribed";
				$model->addUserToChimp($user_details,"subs_change");
			}			
		} else {
			if($chimpStatus->status == "404") {
				$user_details['subs_val'] = "subscribed";
				$model->addUserToChimp($user_details);
			} else if($chimpStatus->status == "unsubscribed") {
				$user_details['subs_val'] = "pending";
				$model->addUserToChimp($user_details,"subs_change");
			}
		}
	}
	function addUserToChimp($user_details){

		$apikey = 'df5c601248a76b681350af7249052b3d-us11';
		$email = 'mark.obre@keydiscoveryinc.com';
		$fname = "Mark Roynell";
		$lname = "Obre";
		$broker= "New Sample broker";
        $state= "CA";
        $country="USA";
        $zip="90210";
        $city="Beverly Hills";


		$api=array("login"=>"dwant@agentbridge.com","key"=>'df5c601248a76b681350af7249052b3d-us11',"url"=>'https://us11.api.mailchimp.com/3.0/');
		/* Add User
 		$type = "POST";
		$target = "lists/3f34696141/members";

		    $data = array(
                'apikey'        => $apikey,
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $fname,
                    'LNAME' => $lname,
                    'BROKER' => $broker,
                    'STATE' => $state,
                    'COUNTRY' => $country,
                    'CITY' => $city,
                    'ZIP' => $zip,
                )
            );*/

		$type = "PATCH";
		$target = "lists/3f34696141/members/".md5($email);
		echo $target;
		    $data = array(
                'apikey'        => $apikey,
                'email_address' => $email,
                'status'        => 'unsubscribed',
              
            );

		$result = $this->mc_request($api,$type,$target,$data);


		var_dump($result);
	}


	// ---------------------------------------------------------------------------------------------- //
	/*	mc_request

		$api = array
		(
			'login' => 'CanBeAnything',
			'key'   => 'YourAPIKey',
			'url'   => 'https://<dc> .api.mailchimp .com/3.0/
		)

		$type		'GET','POST','PUT','PATCH','DELETE'

		$target		Whatever you're after from the API. Could be:
					'', 'lists', lists/abcd12345',
					'lists/abcd12345/members/abcd12345abcd12345abcd12345abcd12345', etc.

		$data		Associative array with the key => values to be passed.
					Don't forget to match the strucutre of whatever you're trying to PATCH
					or PUT. e.g., For patching a member's first name, use:

					$data = array( 'merge_fields' => array( 'FNAME': "New Name" ) );
					mc_request( $my_api_info, 'PATCH',
						'lists/abcd12345/members/abcd12345abcd12345abcd12345abcd12345', $data );

		If you need to tunnel your requests through a service that only supports POST through Curl,
		uncomment the two lines below and comment out:
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );	
	*/
	// ---------------------------------------------------------------------------------------------- //

	function mc_request( $api, $type, $target, $data = false )
	{
		$ch = curl_init( $api['url'] . $target );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array
		(
			'Content-Type: application/json', 
			'Authorization: ' . $api['login'] . ' ' . $api['key'],
	//		'X-HTTP-Method-Override: ' . $type,
		) );

	//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );

		if( $data )
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );

		$response = curl_exec( $ch );
		curl_close( $ch );

		return $response;
	}

	function removeWindowId(){
		$wind_id =  $_POST['window_id'];
		$active_e=	$_POST['active_e'];
		$pf_ntype=  $_POST['pf_ntype'];


		$_SESSION['window'] = array_diff($_SESSION['window'], array($wind_id));

		if((($key = array_search($wind_id, $_SESSION['window'])) !== false) || (($key = array_search("", $_SESSION['window'])) !== false) )  {
		   unset($_SESSION['window'][$key]);
		}

		

		echo json_encode($_SESSION["window"]);
	}


	//function to send xml request via curl
	function send_request_via_curl($host,$path,$content)
	{
		$posturl = "https://" . $host . $path;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		return $response;
	}
	
	//function to parse Authorize.net response
	function parse_return($content)
	{
		$refId = $this->substring_between($content,'<refId>','</refId>');
		$resultCode = $this->substring_between($content,'<resultCode>','</resultCode>');
		$code = $this->substring_between($content,'<code>','</code>');
		$text = $this->substring_between($content,'<text>','</text>');
		$subscriptionId = $this->substring_between($content,'<subscriptionId>','</subscriptionId>');
		return array ($refId, $resultCode, $code, $text, $subscriptionId);
	}
	//helper function for parsing response
	function substring_between($haystack,$start,$end) 
	{
		if (strpos($haystack,$start) === false || strpos($haystack,$end) === false) 
		{
			return false;
		} 
		else 
		{
			$start_position = strpos($haystack,$start)+strlen($start);
			$end_position = strpos($haystack,$end);
			return substr($haystack,$start_position,$end_position-$start_position);
		}
	}	
	
	function membershippayment()
	{
		$model 		= $this->getModel('UserProfile');
		$loginname="926t4yDN3";
		$transactionkey="27c8ds72fUUZj4Zh";
		$host = "api.authorize.net";
		$path = "/xml/v1/request.api";
		$exp_date = explode("-", $_POST['card_expiry']);
		$whowillpay = $model->get_user_registration(JFactory::getUser()->id);
		
		$model->update_tax_id_num($whowillpay->user_id, $this->encrypt($_POST['btino']));
		$schedule = trim( $_POST['schedule'] );
		switch( $schedule ) {
		
			case 'Monthly' : 				
				$length 			= 30;
				$totalOccurrences 	= 12;		
				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every month";
				break;
				
			case 'Quarterly' : 
				
				$length 			= 90;
				$totalOccurrences 	= 4;
				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every quarter";
			break;
			
			case 'Semi-annual' : 
				
				$length 			= 180;
				$totalOccurrences 	= 2;
				$message 			= "Renewals are paid on the ".date("j").date("S")." day of every semi annual";
			break;			
	
			case 'Annual' : 
				
				$length 			= 365;
				$totalOccurrences 	= 1;
				$message 			= "Renewals are paid on the ".date("j").date("S")." of ".date("F")." every year";
			break;
			
			case 'Bi-annual' : 
				
				$length 			= 365;
				$totalOccurrences 	= 2;
				$message 			= "Renewals are paid on the ".date("j").date("S")." of ".date("F")." every 2 years";
			break;
			
				
		
		}
		
		$content = 
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<ARBCreateSubscriptionRequest xmlns=\"AnetApi/xml/v1/schema/" .
			"AnetApiSchema.xsd\">" .
			"<merchantAuthentication>" .
				"<name>926t4yDN3</name>" .
				"<transactionKey>27c8ds72fUUZj4Zh</transactionKey>" .
			"</merchantAuthentication>" .
			"<refId>".$this->createInvoice()."</refId>" .
			"<subscription>" .
				"<name>AgentBridge Membership Payment Fee</name>" .
				"<paymentSchedule>" .
					"<interval>" .
						"<length>".$length."</length>" .
						"<unit>days</unit>" .
					"</interval>" .
					"<startDate>".date('Y-m-d')."</startDate>" .
					"<totalOccurrences>".$totalOccurrences."</totalOccurrences>" .
					"<trialOccurrences>0</trialOccurrences>" .
				"</paymentSchedule>" .
			"<amount>".(int) str_replace(',', '', str_replace('$', '', $_POST['amount']))."</amount>" .
			"<trialAmount>0</trialAmount>".
			"<payment>" .
				"<creditCard>" .
					"<cardNumber>".$_POST['card_number']."</cardNumber>" .
					"<expirationDate>".$exp_date[1]."-".$exp_date[0]."</expirationDate>" .
				"</creditCard>" .
			"</payment>" .
			"<order>" .
					"<invoiceNumber>".$invoice."</invoiceNumber>" .
					"<description>".$_POST['schedule']." Subscription</description>" .
			"</order>" .
			"<customer>" .
					"<email>".$_POST['email']."</email>" .
					"<phoneNumber>".$_POST['phone']." </phoneNumber>" .
			"</customer>" .
			"<billTo>" .
				"<firstName>".$_POST['firstname']."</firstName>" .
				"<lastName>".$_POST['lastname']."</lastName>" .
				"<address>".$_POST['address']."</address>" .
				"<city>".$_POST['city']."</city>" .
				"<state>".$model->getState( $_POST['state'] )."</state>" .
				"<zip>".$_POST['zip']."</zip>" .
				"<country>".$model->getCountry(  $_POST['country'] )."</country>" .
			"</billTo>" .
			"<shipTo>" .
				"<firstName>n/a</firstName>" .
				"<lastName>n/a</lastName>" .
				"<address>n/a</address>" .
				"<city>n/a</city>" .
				"<state>n/a</state>" .
				"<zip>n/a</zip>" .
				"<country>n/a</country>" .
			"</shipTo>" .
			"</subscription>" .
		"</ARBCreateSubscriptionRequest>";		
		
		$response = $this->send_request_via_curl($host,$path,$content);
		
		//if the connection and send worked $response holds the return from Authorize.net
		if ($response)
		{
			list ($refId, $resultCode, $code, $text, $subscriptionId) =$this->parse_return($response);
			if ( $resultCode == "Ok" ) {
				
				$model->insert_autorization_transaction(
					$subscriptionId,
					$_POST,
					$_POST['state'],
					$_POST['country'],
					//str_replace('-', '', $_POST['card_expiry']),
					$_POST['card_expiry'],
					$this->createInvoice()
				);
				
				$model->update_user_subsctiption(
					$model->get_user_registration(JFactory::getUser()->id)->user_id,
					trim( $_POST['promo_code_id'] ),
					trim( $_POST['schedule'] )
				);


			$result_c = $model->get_user_details_c(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $whowillpay->user_id);

			$user_details = array(
				'email' => $result_c->email, 
				'fname' => $result_c->firstname,
				'lname' => $result_c->lastname,
				'broker' => $result_c->broker_name,
				'state' => $result_c->zone_code,
				'country' => $result_c->countries_iso_code_3,
				'zip' => $result_c->zip,
				'city' => $result_c->city,
			);
			$model->addUserToChimp($user_details,"add");

			$body = "";
			
			$subject = "AgentBridge Membership Receipt";
			$email = $_POST['email'];
			$body  .= "<span style='font-size:20px; color:#006699;'><strong>Congratulations!</strong></span><br/><br/>";
			$body  .= "<span style='font-size:16px'>You are now a member of the most powerful real estate network in the world.</span><br/><br/> ";
			$body  .= "<span style='font-size:16px'><strong>Order Number:</strong>".$subscriptionId."</span><br />";
			$body  .= "<span style='font-size:16px'><strong>Order Date:</strong> ".date("d-m-Y H:m:s")."</span><br />";
			$body  .= "<span style='font-size:16px'><strong>Membership Plan:</strong> ".$_POST['schedule']."</span><br/><br/>";
			$body  .= "<span style='font-size:16px; color:#006699'><strong>Member information:</strong></span><br /><br />";
			$body  .= $_POST['firstname']." ".$_POST['lastname']."<br />";
			$body  .= $_POST['city']."<br />";
			$body  .= $model->getState( $_POST['state'] )."<br />";
			$body  .= $_POST['zip']."<br />";
			$body  .= $model->getCountry(  $_POST['country'] )."<br />";
			$body  .= $_POST['phone']."<br />";
			$body  .= $_POST['email']."<br /><br />";
			
			$body  .= "<span style='font-size:16px;color:#006699'><strong>Membership details:</strong></span><br /><br />";
			$body  .= "<table>
						<tr>
						<td width='300'><strong>Service</strong></td>
						<td width='100'><strong>QTY</strong></td>
						<td width='100'><strong>Total</strong></td>
						</tr>
						<tr>
						<td width='300'>AgentBridge ".$_POST['schedule']." Membership Fee <br /> ".$message."</td>
						<td width='100'>1</td>
						<td width='100'>$".number_format( $_POST['amount'] )."</td>
						</tr>
						</table><br/><br/>";
			
			$body  .= "<span style='font-size:16px;color:#006699'><strong>Membership inquiries:</strong></span><br /><br />";
			$body  .= "You can make changes to your membership plan anytime on your <a href='https://www.agentbridge.com/index.php/component/userprofile/?task=membership'>Membership </a>page. If you need general support for your order, call us at (310) 870-1231 or use our <a href='https://www.agentbridge.com/index.php/component/userprofile/component/userprofile/?task=contactus'>Contact Us</a> form.<br /><br />";
			
			$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( $email );
			$mailSender ->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( $subject );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody(  $body );
			$mailSender ->Send();				
			
			echo trim($resultCode."|".$text);
		}
			else {
			echo trim($resultCode."|".$text);
			}
		
			}
	
		
		die();
	}
	function contactus()
    {
		$view = &$this->getView($this->getName(), 'html');
        $view->display($this->getTask());
    }
	
	function help()
    {
    	$model = $this->getModel($this->getName());
        $video_data = $model->getListVideos();
		$fee = $model->getReferralTable();
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('video_datas', $video_data);
		$view->assignRef('fee', $fee);
        $view->display($this->getTask());
    }
    function mobilehelp()
    {
    //	$model = $this->getModel($this->getName());
      //  $video_data = $model->getListVideos();
        
	//	$view = &$this->getView($this->getName(), 'html');
	//	$view->assignRef('video_datas', $video_data);
     //   $view->display($this->getTask());
    }
	
	function membership()
    {
		$model	= $this->getModel('UserProfile');
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('current_subscription', $model->get_subscription_type($model->get_user_registration(JFactory::getUser()->id)->fee_id));
		$view->assignRef('payment_options', $model->get_payment_options());
		$is_prem =$model->check_user_invitation($model->get_user_registration(JFactory::getUser()->id)->email);
		
		if($is_prem ==",")
			$view->assignRef('is_premium', $model->get_user_registration(JFactory::getUser()->id)->is_premium );
		$view->assignRef('is_premium_invited', $model->check_user_invitation($model->get_user_registration(JFactory::getUser()->id)->email));
        $view->display($this->getTask());
	    ;
    }
	
	function submitTicketSupport() {
	
		$model	= $this->getModel('UserProfile');
	
		// set API configuration
		//$apiUrl 	= "https://agentbridge.kayako.com/api/index.php?e=/Tickets/Ticket";
		$apiUrl 	= "https://agentbridge.kayako.com/api/v1/conversations.json";
		$apiKey 	= "bdb372d7-55b7-a384-415c-fcfcf4b6cdf8";
		$salt 		= mt_rand();
		$secretKey 	= "MTM3NzMzZjctYTdkMy1mYzM0LWMxYTAtYWNjMGY2ZWNlNDI1YWIxNDc1MDctMjAyMy02MGQ0LWJkMDgtMjI0ZTliYTA3OTEy";
		$signature 	= base64_encode(hash_hmac('sha256',$salt,$secretKey,true));
		// users messages
		$subject 			= "AgentBridge Support";
		$fullname 			= $model->get_user_registration(JFactory::getUser()->id)->firstname." ".$model->get_user_registration(JFactory::getUser()->id)->lastname;
		$email 				= JFactory::getUser()->email;
		$contents 			= $_POST['ticket_content'];
		$departmentid 		= $_POST['ticket_category'];
		$ticketstatusid 	= "1";
		$ticketpriorityid 	= $_POST['ticket_priority'];
		$tickettypeid 		= $_POST['ticket_type'];
		$staffid 			= "1";
		$post_data = array(
		 'name' => stripslashes_all($fullname),	
		 'subject' => $subject,
		 'fullname' => $fullname,
		 'email' => $email,
		 'contents' => $contents,
		 'departmentid' => $departmentid,
		 'ticketstatusid' => $ticketstatusid,
		 'ticketpriorityid' => $ticketpriorityid,
		 'tickettypeid' => $tickettypeid,
		 'staffid' => $staffid, 
		 'apikey' => $apiKey, 
		 'salt' => $salt, 
		 'signature' => $signature);
		$post_data = http_build_query($post_data, '', '&');
		$curl = curl_init($apiUrl); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $apiUrl);
		curl_setopt($curl, CURLOPT_HEADER, false); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
		$response = curl_exec($curl);  
		curl_close($curl);
		
		echo 'proceed';
		die();
	}
	
	function aboutus()
    {
		$view = &$this->getView($this->getName(), 'html');
        $view->display($this->getTask());
    }
		
	public function unsubscribe(){
		$application = JFactory::getApplication();
		if(!isset($_GET['h321']))
			$application->redirect('index.php?illegal=1');

		$user_id = base64_decode($_GET['h321']);

		// Create an object for the record we are going to update.
		$object = new stdClass();
		$regid = $user_id;
		
		// Must be a valid primary key value.
		//$object->user_id = $user_id;
		//$object->is_unsubscribed = 1;
		$model = $this->getModel('UserProfile');
		$contact_options = $model->retrieveNotifcationsById($user_id);
		$arr_contact_options = json_decode($contact_options->contact_options);
		
		$object->user_id = $contact_options->user_id;
		
		$sub_weekly_key = array_search(8, $arr_contact_options);
		if($sub_weekly_key !== FALSE) {
			$arr_contact_options[$sub_weekly_key] = "0";
		}

		$json_contact_options = json_encode($arr_contact_options);
		$object->contact_options = $json_contact_options;
		
		
		$result = $model->get_user_details_bc(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $regid);
		$user_details = array(
			'email' => $result->email, 
			'fname' => $result->firstname,
			'lname' => $result->lastname,
			'broker' => $result->broker_name,
			'state' => $result->zone_code,
			'country' => $result->countries_iso_code_3,
			'zip' => $result->zip,
			'city' => $result->city,
			'subs_val' => 'unsubscribed'
		);
		
		
		// Update their details in the users table using id as the primary key.
		//$result = JFactory::getDbo()->updateObject('#__user_registration', $object, 'user_id');
		$result = JFactory::getDbo()->updateObject('#__user_contact_method', $object, 'user_id');
		
		$chimpStatus = json_decode($model->checkUserChimp($user_details));
		if($chimpStatus->status == "subscribed") {
			$model->addUserToChimp($user_details,"subs_change");
		}

		$view = &$this->getView($this->getName(), 'html');
		$view->display($this->getTask());
	}
	
	public function unsubscribe_matches_email() {
		$application = JFactory::getApplication();
		if(!isset($_GET['h321']))
			$application->redirect('index.php?illegal=1');

		$user_id = base64_decode($_GET['h321']);

		// Create an object for the record we are going to update.
		$object = new stdClass();
		$regid = $user_id;
		#echo $regid; exit;
		// Must be a valid primary key value.
		//$object->user_id = $user_id;
		//$object->is_unsubscribed = 1;
		$model = $this->getModel('UserProfile');
		$contact_options = $model->retrieveNotifcationsById($user_id);
		$arr_contact_options = json_decode($contact_options->contact_options);
		
		$object->user_id = $contact_options->user_id;
		
		$sub_weekly_key = array_search(7, $arr_contact_options);

		if($sub_weekly_key !== FALSE) {
			$arr_contact_options[$sub_weekly_key] = "0";
		}
		#print_r($arr_contact_options);
		$json_contact_options = json_encode($arr_contact_options);
		$object->contact_options = $json_contact_options;
		#print_r($object); exit;
		
		// Update their details in the users table using id as the primary key.
		//$result = JFactory::getDbo()->updateObject('#__user_registration', $object, 'user_id');
		$result = JFactory::getDbo()->updateObject('#__user_contact_method', $object, 'user_id');
		
		$view = &$this->getView($this->getName(), 'html');
		$view->display($this->getTask());
	}
	
	public function subscribe_matches_email(){

		if(!isset($_POST['user_id'])){
			echo "fail";
			die();
		}

		$user_id = base64_decode($_POST['user_id']);

		// Create an object for the record we are going to update.
		$object = new stdClass();
		 
		// Must be a valid primary key value.
		$object->user_id = $user_id;
		$regid = $user_id;
		//$object->is_unsubscribed = 0;
		 
		// Update their details in the users table using id as the primary key.
		$model = $this->getModel('UserProfile');
		$contact_options = $model->retrieveNotifcationsById($user_id);
		$arr_contact_options = json_decode($contact_options->contact_options);
		
		$object->user_id = $contact_options->user_id;
		$user_id = $object->user_id;
		
		$sub_weekly_key = array_search(7, $arr_contact_options);
		if($sub_weekly_key === FALSE) {
			$arr_contact_options[2] = "7";
		}

		$json_contact_options = json_encode($arr_contact_options);
		$object->contact_options = $json_contact_options;
		
		$result = $model->get_user_details_bc(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $regid);
		$user_details = array(
			'email' => $result->email, 
			'fname' => $result->firstname,
			'lname' => $result->lastname,
			'broker' => $result->broker_name,
			'state' => $result->zone_code,
			'country' => $result->countries_iso_code_3,
			'zip' => $result->zip,
			'city' => $result->city,
		);
		
		// Update their details in the users table using id as the primary key.
		//$result = JFactory::getDbo()->updateObject('#__user_registration', $object, 'user_id');
		$result = JFactory::getDbo()->updateObject('#__user_contact_method', $object, 'user_id');

		echo $user_id;
	}
	
	

	public function subscribe_email(){

		if(!isset($_POST['user_id'])){
			echo "fail";
			die();
		}

		$user_id = base64_decode($_POST['user_id']);

		// Create an object for the record we are going to update.
		$object = new stdClass();
		 
		// Must be a valid primary key value.
		$object->user_id = $user_id;
		$regid = $user_id;
		//$object->is_unsubscribed = 0;
		 
		// Update their details in the users table using id as the primary key.
		$model = $this->getModel('UserProfile');
		$contact_options = $model->retrieveNotifcationsById($user_id);
		$arr_contact_options = json_decode($contact_options->contact_options);
		
		$object->user_id = $contact_options->user_id;
		$user_id = $object->user_id;
		
		$sub_weekly_key = array_search(8, $arr_contact_options);
		if($sub_weekly_key === FALSE) {
			$arr_contact_options[3] = "8";
		}

		$json_contact_options = json_encode($arr_contact_options);
		$object->contact_options = $json_contact_options;
		
		$result = $model->get_user_details_bc(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $regid);
		$user_details = array(
			'email' => $result->email, 
			'fname' => $result->firstname,
			'lname' => $result->lastname,
			'broker' => $result->broker_name,
			'state' => $result->zone_code,
			'country' => $result->countries_iso_code_3,
			'zip' => $result->zip,
			'city' => $result->city,
		);
			
		// Update their details in the users table using id as the primary key.
		//$result = JFactory::getDbo()->updateObject('#__user_registration', $object, 'user_id');
		$result = JFactory::getDbo()->updateObject('#__user_contact_method', $object, 'user_id');
		
		$chimpStatus = json_decode($model->checkUserChimp($user_details));
		if($chimpStatus->status == "404") {
			$user_details['subs_val'] = "subscribed";
			$model->addUserToChimp($user_details);
		} else if($chimpStatus->status == "unsubscribed") {
			$user_details['subs_val'] = "subscribed";
			$model->addUserToChimp($user_details,"subs_change");
		}
		
		//$result = JFactory::getDbo()->updateObject('#__user_registration', $object, 'user_id');

		echo $user_id;
	}

	public function mywallet(){
		$view = &$this->getView($this->getName(), 'html');
		$view->display($this->getTask());
	}
	
	public function continue_complete(){
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$user_object = new JObject();
		$user_object->image = $_POST['jform']['imagePath'];
		$user_object->email = JFactory::getUser()->email;
		$model = $this->getModel('UserProfile');
		$user_type = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		$model->updateImage($user_object);
		$model->update_about(JFactory::getUser()->email, $_POST['jform']['about']);
		if(isset($_POST['jform']['designations']) && $_POST['jform']['designations']!="") {
			$model->update_designations(JFactory::getUser()->id, $_POST['jform']['designations']);
		}
		if(isset($_POST['jform']['languages']) && $_POST['jform']['languages']!="") {
			$model->update_languages(JFactory::getUser()->id, $_POST['jform']['languages']);
		}
		$broker = $model->get_broker($model->get_user_registration(JFactory::getUser()->id)->brokerage);
		$broker_id = $model->get_broker_id(JFactory::getUser()->email);
		if($model->get_user_registration(JFactory::getUser()->id)->licence) {
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '';
		}
		if($model->get_user_registration(JFactory::getUser()->id)->brokerage_license) {
			$bl_license = $this->decrypt( $model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ;
		} else {
			$bl_license =  '';
		}


		$getCountryLangs = $this->getCountryLangs();
		$getCountryLangsInitial = $this->getCountryDataByID($model->get_user_registration(JFactory::getUser()->id)->country);
		
		$mobile = $model->get_mobile_numbers(JFactory::getUser()->id);
		$work = $model->get_work_numbers(JFactory::getUser()->id);
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );

		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('country_list', $getCountryLangs);
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);
		$view->assignRef('city', $model->get_user_registration(JFactory::getUser()->id)->city);
		$view->assignRef('state', $model->get_user_registration(JFactory::getUser()->id)->state);
		$view->assignRef('broker', $broker);	
		$view->assignRef('broker_id', $broker_id);	
		$view->assignRef('al_license',  $al_license);
		$view->assignRef('bl_license',  $bl_license);
		$view->assignRef('volume_2012', $model->get_user_registration(JFactory::getUser()->id)->volume_2012) ;
		$view->assignRef('sides_2012', $model->get_user_registration(JFactory::getUser()->id)->sides_2012) ;
		$view->assignRef('volume_2013', $model->get_user_registration(JFactory::getUser()->id)->volume_2013) ;
		$view->assignRef('sides_2013', $model->get_user_registration(JFactory::getUser()->id)->sides_2013) ;
		$view->assignRef('volume_2014', $model->get_user_registration(JFactory::getUser()->id)->volume_2014) ;
		$view->assignRef('sides_2014', $model->get_user_registration(JFactory::getUser()->id)->sides_2014) ;
		$view->assignRef('volume_2015', $model->get_user_registration(JFactory::getUser()->id)->volume_2015) ;
		$view->assignRef('sides_2015', $model->get_user_registration(JFactory::getUser()->id)->sides_2015) ;
		$view->assignRef('volume_2016', $model->get_user_registration(JFactory::getUser()->id)->volume_2016) ;
		$view->assignRef('sides_2016', $model->get_user_registration(JFactory::getUser()->id)->sides_2016) ;
		$view->assignRef('verified_2012', $model->get_user_registration(JFactory::getUser()->id)->verified_2012);
		$view->assignRef('verified_2013', $model->get_user_registration(JFactory::getUser()->id)->verified_2013);
		$view->assignRef('verified_2014', $model->get_user_registration(JFactory::getUser()->id)->verified_2014);
		$view->assignRef('verified_2015', $model->get_user_registration(JFactory::getUser()->id)->verified_2015);
		$view->assignRef('verified_2016', $model->get_user_registration(JFactory::getUser()->id)->verified_2016);
		$view->assignRef('is_premium', $is_premium);
		$view->assignRef('street_address', $model->get_user_registration(JFactory::getUser()->id)->street_address);
		$view->assignRef('suburb', $model->get_user_registration(JFactory::getUser()->id)->suburb);
		$view->assignRef('user_type', $user_type->user_type);
		$view->assignRef('mobile', $mobile[0]->value);	
		$view->assignRef('work', $work[0]->value);	
		$view->display($this->getTask());
		
	}
	
	
	public function continue_express(){
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);
		$model = $this->getModel('UserProfile');
		$user_type = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		$broker = $model->get_broker($model->get_user_registration(JFactory::getUser()->id)->brokerage);
		$broker_id = $model->get_broker_id(JFactory::getUser()->email);
		if($model->get_user_registration(JFactory::getUser()->id)->licence) {
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '';
		}

		$getCountryLangs = $this->getCountryLangs();
		$getCountryLangsInitial = $this->getCountryDataByID($model->get_user_registration(JFactory::getUser()->id)->country);

		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);
		$view->assignRef('city', $model->get_user_registration(JFactory::getUser()->id)->city);
		$view->assignRef('state', $model->get_user_registration(JFactory::getUser()->id)->state);
		$view->assignRef('broker', $broker);	
		$view->assignRef('broker_id', $broker_id);	
		$view->assignRef('al_license',  $al_license);
		$view->assignRef('volume_2016', $model->get_user_registration(JFactory::getUser()->id)->volume_2016) ;
		$view->assignRef('sides_2016', $model->get_user_registration(JFactory::getUser()->id)->sides_2016) ;
		$view->assignRef('verified_2016', $model->get_user_registration(JFactory::getUser()->id)->verified_2016);
		$view->assignRef('volume_2015', $model->get_user_registration(JFactory::getUser()->id)->volume_2015) ;
		$view->assignRef('sides_2015', $model->get_user_registration(JFactory::getUser()->id)->sides_2015) ;
		$view->assignRef('verified_2015', $model->get_user_registration(JFactory::getUser()->id)->verified_2015);
		$view->assignRef('is_premium', $is_premium);
		$view->assignRef('user_type', $user_type->user_type);
		$view->display($this->getTask());
		
	}
	public function payments(){
		$model = $this->getModel('UserProfile');
		$user_info = $model->get_user_registration(JFactory::getUser()->id);
		if(!empty($model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ){
			$bl_license = $this->decrypt( $model->get_user_registration(JFactory::getUser()->id)->brokerage_license) ;
		} else {
			$bl_license =  '&nbsp';
		}
		if(!empty($model->get_user_registration(JFactory::getUser()->id)->licence) ){
			$al_license = $this->decrypt($model->get_user_registration(JFactory::getUser()->id)->licence) ;
		} else {
			$al_license =  '&nbsp';
		}
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('is_premium_invited', $model->check_user_invitation( $model->get_user_registration(JFactory::getUser()->id)->email ));
		$view->assignRef('country_list', $model->get_countries());
		$view->assignRef('country_id', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('zip_code', $model->get_user_registration(JFactory::getUser()->id)->zip);	
		$view->assignRef('bl_license', $bl_license);	
		$view->assignRef('al_license', $al_license);
		$view->assignRef('payment_options', $model->get_payment_options());
		$view->assignRef('referral_fees', $model->get_referral_fees());
		$view->assignRef('user_info', $user_info);
		$view->display($this->getTask());
	}	
	
	public function term_acceptance(){
		$model = $this->getModel($this->getName());
		$view = &$this->getView($this->getName(), 'html');		
		$view->display($this->getTask());
	}
	
	public function verify_terms(){
		$model = $this->getModel($this->getName());
		$model->update_terms_verification();
		$user_assist_details = $model->getUserAssistant(JFactory::getUser()->id);

		$user_info = $model->get_user_registration(JFactory::getUser()->id);

		//language loading
		  	$language = JFactory::getLanguage();
	      	$extension = 'com_nrds';
	      	$base_dir = JPATH_SITE;
	      	$language_tag = $user_info->currLanguage;
	      	$language->load($extension, $base_dir, $language_tag, true);
	    //language loading	


   		if($user_info->gender == "Female"){
   			$salut = JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($user_info->lastname);
    		} else {
   			$salut = JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($user_info->lastname);
 		}



		if($user_assist_details->cc_all && $user_assist_details->cc_all==1){
			$cc_email=$user_assist_details->a_email;
		}

		// welcome message
		$emailSubject	= JText::sprintf('Welcome to AgentBridge!');
		$emailBody = "Dear ".$salut.",<br /><br /> Congratulations.  You are now a member of AgentBridge, the most powerful network of real estate professionals in the world.<br /><br />
				Click this link to access your account and take advantage of your membership privileges.<br />
				<a href='https://www.agentbridge.com'>AgentBridge.com</a>
				<br /><br /> Sincerely, <br/><br/>The AgentBridge Team<br/><a href='mailto:service@agentbridge.com'>service@AgentBridge.com</a>";
		
		// Send the registration email.
		$sendmail = JFactory::getMailer();
		$sendmail->isHTML( true );
		if($sendmail->sendMail('ebooker@agentbridge.com', 'Agent Bridge', $model->get_user_registration(JFactory::getUser()->id)->email, $emailSubject, $emailBody, true, $cc_email)){
			$this->emailTermsAccepted($model->get_user_registration(JFactory::getUser()->id)->user_id);
			$return = true;
		}		
		JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
	}
	function emailTermsAccepted($userId){
				
				// Get User Activation
				
				//echo "connected";
				
				$actid = $userId;
				$model = $this->getModel('UserProfile');
				$result = $model->get_user_details(array('*'), $actid);
				
				echo $result->brokerage;
				
				echo "connected";
				if ($result->brokerage!='') {
				
					$broker_result = $model->get_broker_data($result->brokerage);
					
				} else {
					$broker_result = "not available";
				
				}
				
				$body = "";
				$subject = "New Terms Accepted Agent: ".stripslashes_all($result->firstname)." ".stripslashes_all($result->lastname);
				$body .= "Environment: <strong>PROD</strong>";
				
				$body .= "<br/>";
				
				$body .= "Member firstname: ".stripslashes_all($result->firstname);
				$body .= "<br/>";
				$body .= "Member lastname: ".stripslashes_all($result->lastname);
				$body .= "<br/>";
				$body .= "Email address: ".$result->email;
				$body .= "<br/>";
				$body .= "User Type: ".$result->user_type;
				$body .= "<br/>";
				$body .= "Brokerage: ".$broker_result->broker_name;	
				$body .= "<br/>";
				$body .= "Charter: ";
				
				$body .= $result->is_premium == 1 ? "Yes" : "No";			
				date_default_timezone_set( 'America/Los_Angeles' );
				$add_date= date('m-d-Y, h:i:s A');
				$body .= "<br/>";
				$body .= "Date/Time Terms Accepted: ".$add_date;				
				
				$body .= "<br/>";
				
				$mailSender = JFactory::getMailer();
				
				$recipient = array( 'rebekah.roque@keydiscoveryinc.com', 'ebooker@agentbridge.com', 'focon@agentbridge.com','mark.obre@keydiscoveryinc.com','redler@agentbridge.com');
				
				$mailSender->sendMail('ebooker@agentbridge.com', 'AgentBridge', $recipient, $subject, $body, true);
		
	}
	public function save_profile() {
		$model = $this->getModel('UserProfile');
		$usertype = $model->get_user_type(JFactory::getUser()->email);
		$is_premium = $model->get_user_registration(JFactory::getUser()->id)->is_premium;
		
		/*
		if (!empty($_POST['jform']['volume_2014'])) {
			$volume_2014 = preg_replace("/[^0-9,.]/", "", (str_replace(",","",$_POST['jform']['volume_2014'])));
		}
		
		if (!empty($_POST['jform']['sides_2014'])) {
			$sides_2014 = str_replace("$", "", (str_replace(",","",$_POST['jform']['sides_2014'])));
		} 
		*/
		if (!empty($_POST['jform']['volume_2015'])) {
			$volume_2015 = preg_replace("/[^0-9,.]/", "", (str_replace(",","",$_POST['jform']['volume_2015'])));
		}
		
		if (!empty($_POST['jform']['sides_2015'])) {
			$sides_2015 = str_replace("$", "", (str_replace(",","",$_POST['jform']['sides_2015'])));
		} 
		
		if (!empty($_POST['jform']['volume_2016'])) {
			$volume_2016 = preg_replace("/[^0-9,.]/", "", (str_replace(",","",$_POST['jform']['volume_2016'])));
		}
		
		if (!empty($_POST['jform']['sides_2016'])) {
			$sides_2016 = str_replace("$", "", (str_replace(",","",$_POST['jform']['sides_2016'])));
		} 
		
		if ($_POST['jform']['terms'] == 1) {
			$date = date('Y-m-d H:i:s',time());
		} else {	
			$date = "0000-00-00 00:00:00";
		}
		if (!empty($_POST['jform']['brokerage_license'])) {
			$brokerageLicense = $this->encrypt($_POST['jform']['brokerage_license']);
		} else {
			$brokerageLicense = '';
		}
		if (!empty($_POST['jform']['agent_license_number'])) {
			$agentLicense = $this->encrypt($_POST['jform']['agent_license_number']);
		} else {
			$agentLicense = '';
		}
		$model->update_single_numbers(
			$_POST['jform']['mobile'], 
			$_POST['jform']['work'],
			$_POST['jform']['country'],
			$_POST['jform']['zip'],
			$_POST['jform']['city'],
			$agentLicense,
			$brokerageLicense,
			$_POST['jform']['brokerage'],
			$_POST['jform']['state'],
			$_POST['jform']['street'],
			$_POST['jform']['suburb'],			
			$volume_2015,			
			$sides_2015,
			$volume_2016,			
			$sides_2016,
			$_POST['jform']['terms'],		
			$date
	
		);
		
		$_SESSION['update_sales'] = 1;
		
		if ($is_premium== 1) {
				echo "redirect";
				//$this->emailTermsAccepted($model->get_user_registration(JFactory::getUser()->id)->user_id);
		} else {
				echo "payments";
		}
		die();
	}
	
	
	
	public function complete(){
		$model = $this->getModel('UserProfile');
		$user_object = new JObject();
		$user_object->image = $_POST['jform']['imagePath'];
		$user_object->email = JFactory::getUser()->email;
		$model->updateImage($user_object);
		$model->getImage();
		$model->update_about(JFactory::getUser()->email, $_POST['jform']['about']);
		$model->update_designations(JFactory::getUser()->id, $_POST['jform']['designations']);
		$model->update_languages(JFactory::getUser()->id, $_POST['jform']['languages']);
		JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_userprofile&task=profile"));
	}
	public function start(){
		$model = $this->getModel($this->getName());
		$view = &$this->getView($this->getName(), 'html');
		$suggestions = $model->suggest(JFactory::getUser()->email);
		if(count($suggestions) > 1)
			$view->assignRef('suggestions', $suggestions);
		else
			JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
		$view->display($this->getTask());
	}
	
	public function download_vcard(){
		header('Content-Type: text/x-vcard; charset=utf-8');
		echo "BEGIN:VCARD\n";
		echo "VERSION:3.0\n";
		echo "FN:Kenny Alameda\n";
		echo "N:Alameda Kenny;;;\n";
		echo "EMAIL;TYPE=INTERNET;TYPE=HOME:ClipperOil@aol.com\n";
		echo "TEL;TYPE=CELL:+1-619-540-0884\n";
		echo "ADD:\n";
		echo "END:VCARD\n";
		echo "\";\n";
		die();
	}
	
	public function addcontacts(){
		$model = $this->getModel($this->getName());
		foreach($_POST['jform']['contact'] as $contact){
			$model->request_contact_network( $contact );
		}
		JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_activitylog"));
	}
	public function updateToH(){		
		$user_email = $_POST['user_email'];
		$model = $this->getModel($this->getName());
		$return = $model->updateIs_100($user_email);
		echo $return;
	}
	public function updateToH2(){		
		$user_email = $_POST['user_email'];
		$model = $this->getModel($this->getName());
		$return = $model->updateIs_100_no($user_email);
		echo $return;
	}
	public function logMeOut(){
		

		$app = JFactory::getApplication('site');
		if(JFactory::getUser()->id) {
		     $app->logout();
		}
	}
	public function profile(){	
		
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set('user', new JUser($user->id));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
        $language->load($extension, $base_dir, $language_tag, true);


		// sanitize data!!! vulnerable if not
		
		$is_premium_notification_hidden = preg_replace('/[^-a-zA-Z0-9_]/', '', $_GET['invitation']);
		if( $is_premium_notification_hidden==1 ) {
			$session =& JFactory::getSession();
			$session->set( 'is_premium_notification_hidden', $is_premium_notification_hidden );
		}
		$model = $this->getModel($this->getName());
		$view = &$this->getView($this->getName(), 'html');
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		$userid = (isset($_GET['uid'])) ? $_GET['uid'] : JFactory::getUser()->id;
		$own = (isset($_GET['uid'])) ? false : true;
		
		if(!$own) {
			$own_group_id = $model->get_user_group(JFactory::getUser()->id);
			$view_group_id = $model->get_user_group($_GET['uid']);
			
			$admin_group = array(7,8,11,12);
			
			if(in_array($view_group_id->group_id, $admin_group) && !in_array($own_group_id->group_id, $admin_group)) {
				JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
			}
		}
		
		$user_info = $model->get_profile_info($userid, $own);
		if(!count($user_info) || (count($user_info) && !count($model->get_regid($user_info->email)))) {
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_('index.php?option=com_activitylog'));
		}
		
		$current_user_info = $model->get_profile_info(JFactory::getUser()->id, $own);		
		$current_user_reg = $model->get_user_registration(JFactory::getUser()->id);
		
		$previous_year = date("Y", strtotime("-1 year"));

		while(intval($previous_year) >= 2012) {
			if($current_user_info->{"verified_".$previous_year}) {
				$viewer_previous_year_volume = $current_user_info->{"volume_".$previous_year};
				$viewer_previous_year_ave_price = $current_user_info->{"ave_price_".$previous_year};
				break;
			}
			$previous_year = date("Y", strtotime($previous_year . "-01-01 -1 year"));
		}
		
		$invitation = $model->get_invitation($current_user_reg->user_id);

		$ex_rates = $model2->getExchangeRates_indi();
        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;
		$prog_count = 0;
	
		if(JFactory::getUser()->id==$userid){
			
			$t_date = strtotime('31-01-'.date("Y"));
			$now = time();

			if($t_date > $now) {
			    $time = strtotime("-2 years", time());
			} else {
			    $time = strtotime("-1 year", time());
			}

  			$prev_year = date("Y", $time);
		
  			
			$progressData = $model->getProgressData($userid);
			$prev_year_data = 0;
			$prev_year_data_inc = 0;
			
			//var_dump($is_100);
			if($progressData[0]->brokerage_license || $progressData[0]->is_term_accepted){
				$prog_count++;
			}
	
			if(($progressData[0]->desig_count && $progressData[0]->mobile_count &&  $progressData[0]->lang_count) || ($progressData[0]->about && $progressData[0]->mobile_count &&  $progressData[0]->lang_count)){
				$prog_count++;
			}
			if($progressData[0]->{"volume_".$prev_year} && $progressData[0]->{"sides_".$prev_year}){
				$prev_year_data_inc=1;
				$prog_count++;
			}

			if($prog_count==2 && $prev_year_data_inc==0){
				$prev_year_data=1;
			}
			
			

			$is_100 = $model->getIs_100($userid);

			if($is_100[0]->is_100 && $prog_count==3){
				$prog_count = 0;
			} else if($prog_count<3){
				$model->updateIs_100_no($user_info->email);
			}

		}
		foreach ($invitation as $invite)
			$invitee =$model->get_invitee($invite->invited_email);
		$user_info->countryId = $current_user_info->country;
		$user_info->countryIso = $model->getCountryIso($current_user_info->country);
		$user_info->getCountryLangsInitial = $this->getCountryDataByID($user_info->country);
		$user_info->country_i = $user_info->country;
		$user_info->country = (is_numeric($user_info->country)) ? $model->getCountry($user_info->country) : $user_info->country;
		$user_info->state_id = $user_info->state;
		$user_info->state = (is_numeric($user_info->state)) ? $model->getState($user_info->state) : $user_info->state;
		$user_info->mobile_numbers = $model->get_mobile_numbers($userid, $own);
		$user_info->fax_numbers = $model->get_fax_numbers($userid, $own);
		$user_info->work_numbers = $model->get_work_numbers($userid, $own);
		$user_info->languages = $model->get_user_spoken_language($userid, $own);
		$user_info->listings_count = $model->count_all_listings($userid);

		$user_info->network = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 1));
		$user_info->pending = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 0));
		$user_info->declined = array_map(function($obj){ return $obj->other_user_id; }, $model->get_network($userid, 2));
		if(!empty($user_info->brokerage) && $user_info->brokerage){
			$user_info->brokerage_label = $model->get_broker($user_info->brokerage);
		}

		$d_format = $this->getCountryDataByID($current_user_reg->country)->dateFormat;
		$getCountryLangsInitial = $this->getCountryDataByID($current_user_reg->country);
		$user_info->inprogress_listings = $model->get_listings($userid, 0, 0);


		$langValues = $model2->checkLangFiles();

		foreach ($user_info->inprogress_listings as $listing) {

			if(JFactory::getUser()->id != $userid){
				$country_used = $current_user_info->country;
			} else {
				$country_used = $listing->country;
			}

			$listing->sqM = "";
			if($listing->sqftMeasurement=="sq. meters"){
				//var_dump($country_used);
				//var_dump($user_info->user_id);
				$getMeasurement = $model2->getSqMeasureByCountry($country_used);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing->bldg_sqft){
						if($listing->bldg_sqft == $value[1]){
							$listing->bldg_sqft=$value[0];
						}
					}
					if($listing->available_sqft){
						if($listing->available_sqft == $value[1]){
							$listing->available_sqft=$value[0];
						}
						
					}
					if($listing->unit_sqft){
						if($listing->unit_sqft == $value[1]){
							$listing->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing->lot_sqft){
						if($listing->lot_sqft == $value[1]){
							$listing->lot_sqft=$value[0];
						}
						
					}
					if($listing->lot_size){
						if($listing->lot_size == $value[1]){
							$listing->lot_size=$value[0];
						}
						
					}
				}

				$listing->sqM = "_M";
			}

			$listing->pop_feats = $model2->getPOPsDetails($listing,$listing->sqpM,$listing->sub_id,$langValues);

			
		}

		$user_info->closed_listings = $model->get_listings($userid, 1, 1);

		foreach ($user_info->closed_listings as $listing) {

			$listing->sqM = "";
			if($listing->sqftMeasurement=="sq. meters"){
				$getMeasurement = $model2->getSqMeasureByCountry($listing->country);
				foreach ($getMeasurement['bldg'] as $key => $value) {
					if($listing->bldg_sqft){
						if($listing->bldg_sqft == $value[1]){
							$listing->bldg_sqft=$value[0];
						}
					}
					if($listing->available_sqft){
						if($listing->available_sqft == $value[1]){
							$listing->available_sqft=$value[0];
						}
						
					}
					if($listing->unit_sqft){
						if($listing->unit_sqft == $value[1]){
							$listing->unit_sqft=$value[0];
						}
						
					}
				}

				foreach ($getMeasurement['lot'] as $key => $value) {
					# code...
					if($listing->lot_sqft){
						if($listing->lot_sqft == $value[1]){
							$listing->lot_sqft=$value[0];
						}
						
					}
					if($listing->lot_size){
						if($listing->lot_size == $value[1]){
							$listing->lot_size=$value[0];
						}
						
					}
				}

				$listing->sqM = "_M";
			}
			$listing->pop_feats = $model2->getPOPsDetails($listing,$listing->sqpM,$listing->sub_id,$langValues);
		}


		$model->detect_expired_pops($userid, 0, 0);
		$countbuyer = $model2->count_user_buyers();
		$user_info->countbuyer = $countbuyer;
		$refin = $model2->get_referral_in($userid, $_POST['filter']);
		if(count($refin))
			foreach($refin as $ref){
				$userreg = $model->get_user_registration($ref->agent_a);
				$ref->other_user = $model->get_user($ref->agent_a);
				$ref->other_user->userregsa = $model->get_user_registration($ref->agent_a);
				if($ref->currency!=$current_user_reg->currency){

					if(!$ref->currency){
						$ref->currency = "USD";
					}

					$ex_rates_con = $ex_rates->rates->{$ref->currency};
					$ex_rates_can = $ex_rates->rates->{$current_user_reg->currency};
					//$ref->price_1=$model2->getExchangeRates($ref->price_1,$current_user_reg->currency,$ref->currency);

					if($ref->currency=="USD"){
						$ref->price_1=$ref->price_1  * $ex_rates_can;
					} else {
						$ref->price_1=($ref->price_1 / $ex_rates_con)*$ex_rates_can;
					}
					if($ref->price_2){
					//	$ref->price_2=$model2->getExchangeRates($ref->price_2,$current_user_reg->currency,$ref->currency);
						if($ref->currency=="USD"){
							$ref->price_2=$ref->price_2 * $ex_rates_can;
						} else {
							$ref->price_2=($ref->price_2 / $ex_rates_con)*$ex_rates_can;
						}
					}		
					$ref->correctcurr = $current_user_reg->currency;
					$ref->symbol = $current_user_reg->symbol;
				} else {
					$ref->correctcurr = $ref->currency;
				}
				$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model->getState($userreg->state): $userreg->state);
				//$ref->other_user = $model->get_user($ref->agent_a);
				$ref->this_user_currency = $current_user_reg->currency;
				$ref->other_user->desigs = $model->get_user_designations($ref->agent_a);
				$ref->history =  $model2->get_referral_history($ref->referral_id);

				$ref->history[0]->created_date = gmdate($d_format,strtotime($ref->history[0]->created_date)-25200);
				$ref->referral_info = $model2->get_referral_info($ref->referral_id);
			}
		$refout = $model2->get_referral_out($userid, $_POST['filter']);
		if(count($refout))
			foreach($refout as $ref){
				$userreg = $model->get_user_registration($ref->agent_b);
				$ref->printaddress = $userreg->city.", ".((is_numeric($userreg->state))? $model->getState($userreg->state): $userreg->state);
				$ref->other_user = $model->get_user($ref->agent_b);
				$ref->other_user_currency = $userreg->currency;
				$ref->other_user->desigs = $model->get_user_designations($ref->agent_b);
				$ref->history =  $model2->get_referral_history($ref->referral_id);

				$ref->history[0]->created_date = gmdate($d_format,strtotime($ref->history[0]->created_date)-25200);
				$ref->referral_info = $model2->get_referral_info($ref->referral_id);
				$ref->correctcurr = $ref->currency;
			}
		$user_info->refin = $refin;
		$user_info->refout = $refout;

		$user_info->works_zip = $model->get_pocket_zips($userid);

		

		$user_info->works_zip_b = $model->get_buyer_zips($userid);
		$user_info->w_zips = $model->get_w_zips($user_info->email)[0]->zip_workaround;

     	$zip_arr= array();
		$zip_arr_val= array();
		$zip_arr_country= array();
		  if(count($user_info->works_zip)>=1 || count($user_info->works_zip_b)>=1){
		  	foreach($user_info->works_zip as $zip){
		  		$zip_arr[] = $zip->zip;
		  		$zip_arr_val[$zip->zip]["pop"]+=$zip->count;
		  	}
		  	foreach($user_info->works_zip_b as $zip){
		  		if(strpos($zip->zip,',') !== false){
		  			$zip_ex=explode(",",$zip->zip);
		  			$zip_arr[] = $zip_ex[0];
		  			$zip_arr_val[$zip_ex[0]]["buyer"]+=$zip->count;
		  			$zip_arr[] = $zip_ex[1];
		  			$zip_arr_val[$zip_ex[1]]["buyer"]+=$zip->count;
		  		} 
		  		else{
		  			$zip_arr[] = $zip->zip;                        	  		
		  		    $zip_arr_val[$zip->zip]["buyer"]+=$zip->count;
		  		}
		  	}
		  }

		  $expl_w_zips = explode(",",$user_info->w_zips);

			foreach ($expl_w_zips as $key => $value) {
				# code...
				if($value){
					$zip_arr[] = $value;
					$zip_arr_val[$value]["pop"]+=0;                        	  		
			  		$zip_arr_val[$value]["buyer"]+=0;
		  		}
			}

		
		$zips_imploded="'(".implode("|", array_unique(array_filter($zip_arr))).")'";



		$zips_b_raw = $model->get_buyer_zip_country($userid, $zips_imploded);
		$zips_p_raw = $model->get_pops_zip_country($userid, $zips_imploded);
		$zips_w_raw = $model->get_w_zips_country($zips_imploded);



		$zips_b=array();	
		$i=0;
		foreach ($zips_b_raw as $value) {
			$this_country=$value->country;
			$this_country_iso=$value->countries_iso_code_2;

			$exploded_zips = explode(",", $value->zip);
			foreach ($exploded_zips as $zip_e) {

				$zips_b[$i]['zip']=$zip_e;
				$zips_b[$i]['country']=$this_country;
				$zips_b[$i]['this_country_iso']=$this_country_iso;
				$i++;
			}

			
		}



		foreach ($zips_p_raw as $value) {
			$this_country=$value->country;
			$this_country_iso=$value->countries_iso_code_2;
			$exploded_zips = explode(",", $value->zip);
			foreach ($exploded_zips as $zip_e) {

				$zips_b[$i]['zip']=$zip_e;
				$zips_b[$i]['country']=$this_country;
				$zips_b[$i]['this_country_iso']=$this_country_iso;
				$i++;
			}
			
		}

		foreach ($expl_w_zips as $zip_e) {
			if($zip_e){
				$zips_b[$i]['zip']=$zip_e;
				$zips_b[$i]['country']=$zips_w_raw[0]->countries_id;
				$zips_b[$i]['this_country_iso']=$zips_w_raw[0]->countries_iso_code_2;
				$i++;
			}
		}


			

		$zip_arr[] = $user_info->zip;
		$zip_arr_val[$user_info->zip]["pop"]+=0;                        	  		
  		$zip_arr_val[$user_info->zip]["buyer"]+=0;
  		$last_row = count($zips_b);
		$zips_b[$last_row]['zip']=$user_info->zip;
		$zips_b[$last_row]['country']=$user_info->countryId;
		$zips_b[$last_row]['this_country_iso']=$user_info->countries_iso_code_2;

		
		$t=0;
		$l=0;
		$up_zip_arr_val=array();
		//var_dump($zips_b);
		foreach ($zip_arr_val as $key => $value) {
			//var_dump($key);
			foreach ($zips_b as $key2 => $value2) {
				if($key==$value2['zip']){

					if($value2['country']==222){

						if($value2['country']==222 && (strcspn($value2['zip'], '0123456789') != strlen($value2['zip'])) ){					
							preg_match('/\d/', $value2['zip'], $m, PREG_OFFSET_CAPTURE);
					    	if (sizeof($m))
					        	 $m[0][1]; // 24 in your example
					        $newkey=substr($value2['zip'], 0, ($m[0][1]+1));
					        $otherhalf_key = substr($value2['zip'], ($m[0][1]+1), strlen($value2['zip']));
						        if(is_numeric($otherhalf_key[0])){
						        	$newkey = $newkey.$otherhalf_key[0];
						        }
						} else {
							$newkey = $value2['zip'];
						}
						if($zip_arr_val[$value2['zip']]['pop'])
							$up_zip_arr_val[$newkey]["pop"] += $zip_arr_val[$value2['zip']]['pop'];
						if($zip_arr_val[$value2['zip']]['buyer'])
							$up_zip_arr_val[$newkey]["buyer"] += $zip_arr_val[$value2['zip']]['buyer'];

						if(!$zip_arr_val[$value2['zip']]['buyer'] && !$zip_arr_val[$value2['zip']]['pop']){
								$up_zip_arr_val[$newkey]=$zip_arr_val[$value2['zip']];
							}
						//unset($zip_arr_val[$value2['zip']]);
						//$zip_arr_val[$t]
						//var_dump($value2['zip']);
						//var_dump( $value['buyer']);
						//var_dump( $value['pop']);
					} else {
						$up_zip_arr_val[$value2['zip']]=$zip_arr_val[$value2['zip']];
					}	
					continue 2;			
				}
				$l++;
			}
			$t++;				
		}

	 	uasort( $up_zip_arr_val, function($a, $b) {
		    if( $a['pop'] == $b['pop'] )
		        return 0;
		    return ( $a['pop'] < $b['pop'] ) ? 1 : -1;
		} );

	 	



		/*

				echo "<br><pre>";

		foreach ($zips_b as $key => $value) {
			foreach ($zip_arr_val as $key2 => $value2) {
				if($key2==$value['zip']){
					if($value['country']==222){
						var_dump($value['zip']);
						var_dump( $value2['buyer']);
						var_dump( $value2['pop']);
					}				
				}
			}				
		}
			if($this_country==222){					
				preg_match('/\d/', $zip_e, $m, PREG_OFFSET_CAPTURE);
		    	if (sizeof($m))
		        	 $m[0][1]; // 24 in your example
		        $zip_e=substr($zip_e, 0, ($m[0][1]+1));
			}
		*/

		//$zips_b = array_unique($zips_b,SORT_REGULAR);

		//var_dump($zips_b);		
		//var_dump($zip_arr_val);
		//	$ex_rates = $modelPropertyListing->getExchangeRates_indi();

		if($userid != JFactory::getUser()->id){
			if($user_info->currency == "") {
				$user_info->currency = "USD";
			}
			
			if($user_info->currency!=$current_user_info->currency){
				$ex_rates_con = $ex_rates->rates->{$user_info->currency};
				$ex_rates_can = $ex_rates->rates->{$current_user_info->currency};
				if($user_info->currency=="USD"){									
					//$user->price1=$listing->price1 * $ex_rates_CAD;
					$user_info->ave_price_2012=$user_info->ave_price_2012 * $ex_rates_can;
					$user_info->ave_price_2013=$user_info->ave_price_2013 * $ex_rates_can;
					$user_info->ave_price_2014=$user_info->ave_price_2014 * $ex_rates_can;
					$user_info->ave_price_2015=$user_info->ave_price_2015 * $ex_rates_can;
					$user_info->volume_2012=$user_info->volume_2012 * $ex_rates_can;
					$user_info->volume_2013=$user_info->volume_2013 * $ex_rates_can;
					$user_info->volume_2014=$user_info->volume_2014 * $ex_rates_can;
					$user_info->volume_2015=$user_info->volume_2015 * $ex_rates_can;
				} else {
					//$user->price1=$listing->price1 / $ex_rates_CAD;
					//$item->price2=($item->price2 / $ex_rates_con)*$ex_rates_can;
					$user_info->ave_price_2012=($user_info->ave_price_2012 / $ex_rates_con)*$ex_rates_can;
					$user_info->ave_price_2013=($user_info->ave_price_2013 / $ex_rates_con)*$ex_rates_can;
					$user_info->ave_price_2014=($user_info->ave_price_2014 / $ex_rates_con)*$ex_rates_can;
					$user_info->ave_price_2015=($user_info->ave_price_2015 / $ex_rates_con)*$ex_rates_can;
					$user_info->volume_2012=($user_info->volume_2012 / $ex_rates_con)*$ex_rates_can;
					$user_info->volume_2013=($user_info->volume_2013 / $ex_rates_con)*$ex_rates_can;
					$user_info->volume_2014=($user_info->volume_2014 / $ex_rates_con)*$ex_rates_can;
					$user_info->volume_2015=($user_info->volume_2015 / $ex_rates_con)*$ex_rates_can;
				}
				$user_info->currency = $current_user_info->currency;
				$user_info->symbol = $current_user_info->symbol;
			}
		}

		$view->assignRef('zips_b', $zips_b);
		$view->assignRef('prev_year_val', $prev_year);
		$view->assignRef('prev_year', $prev_year_data);
		$view->assignRef('zip_arr_val', $up_zip_arr_val);
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );		
		$view->assignRef('invitation', $invitation);	
		$view->assignRef('invitee', $invitee);
		$view->assignRef('user_info', $user_info);
		$view->assignRef('current_user_reg', $current_user_reg);
		
		$view->assignRef('owner_rate', $ex_rates->rates->{$user_info->currency});
		$view->assignRef('viewer_rate', $ex_rates->rates->{$current_user_reg->currency});
		$view->assignRef('viewer_previous_year_volume', $viewer_previous_year_volume);
		$view->assignRef('viewer_previous_year_ave_price', $viewer_previous_year_ave_price);
		$view->assignRef('previous_year', $previous_year);
		
		
		$network_temp = $model->get_network(JFactory::getUser()->id, 1, $search);
		$str=strip_tags(JRequest::getVar('searchstringnetwork'));
		$network = array();
		
		//initialize array result
		$view->assignRef('prog_count', $prog_count);
		$view->assignRef('network', $network);
		$view->assignRef('designations', $model->get_user_designations($userid, $own));
		$is_term_accepted = $model->check_if_terms_accepted();	
		$usertype = $model->get_user_type(JFactory::getUser()->email); 
		$security = $model->get_security(JFactory::getUser()->id);
		$view->display($this->getTask());
	}
	public function request_network(){
		$other_user_id = $_REQUEST['id'];
		$property_id = $_REQUEST['pid'];
		$model = $this->getModel($this->getName());
		if ( isset( $property_id ) && !empty($property_id) ){
			$model->request_network_pid($other_user_id, $property_id);
		} else {
			$model->request_network($other_user_id);
		}
		
		
		echo 'success';
		die();
	}
	public function request_network_raw(){
		$userId = $_GET['userId'];
		$other_user_id = $_GET['otherUserId'];
		$property_id = $_GET['listingId'];
		$model = $this->getModel($this->getName());
		if ( isset( $property_id ) && !empty($property_id) ){
			$model->request_network_pid_WS($userId, $other_user_id, $property_id);
		} else {
			$model->request_network_WS($userId, $other_user_id);
		}
		//echo 'success';
		if(count($rows) == 0) {
			$response = array('status'=>0, 'message'=>"No Buyers Found", 'data'=>array());
		}
		else {		
			$response = array('status'=>1, 'message'=>"Found Buyers", 'data'=>$rows);
		}
		echo json_encode($response);
	}
	public function sendmail(){
		$model = $this->getModel($this->getName());
		$model->sendpropertyrequest_mail('866', '970', '82');
	}
	public function changepass(){
		$model = $this->getModel('UserProfile');
		
		if($_POST['jform']['form']=="change_pass"){
			$app = JFactory::getApplication();
			$oldpassword = $_POST['jform']['passwordold'];
			$user = JFactory::getUser();
			$username = $user->username;
			$original_password = $user->password;
			$hashparts = explode (':' , $original_password);
			$userhash = md5($oldpassword.$hashparts[1]);
			// Get a database object
			$db    = JFactory::getDbo();
			$query = $db->getQuery(true)
				->select('id, password')
				->from('#__users')
				->where('username=' . $db->quote($username));
			$db->setQuery($query);
			$result = $db->loadObject();
			$match = JUserHelper::verifyPassword($oldpassword, $result->password, $result->id);
			if($match){
				if($_POST['jform']['password1']==$_POST['jform']['password2']){
					$model->insertUpdatePassword(JFactory::getUser()->id);
					$new_user = new JObject();
					$new_user->id = $user->id;
					$newpass = md5($_POST['jform']['password1'].$hashparts[1]);
					$new_user->password = JUserHelper::hashPassword($_POST['jform']['password1']);
					$result = JFactory::getDbo()->updateObject('#__users', $new_user, 'id');
					
					$app->redirect(JRoute::_('index.php?option=com_activitylog'));
				}
				else
					$app->enqueueMessage('Passwords do not match', 'error');
			}
			else{
				$app->enqueueMessage('Incorrect password', 'error');
			}
		}
		$view = &$this->getView($this->getName(), 'html');
		$view->display($this->getTask());
	}
	public function contact(){
		$model = $this->getModel('UserProfile');
		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);
		$user_info->getCountryLangsInitial = $this->getCountryDataByID($user_info->country);
		$user_info->country = (is_numeric($user_info->country)) ? $model->getCountry($user_info->country) : $user_info->country;

		$user_info->state = (is_numeric($user_info->state)) ? $model->getState($user_info->state) : $user_info->state;
		$user_info->mobile_numbers = $model->get_mobile_numbers(JFactory::getUser()->id, true);
		$user_info->fax_numbers = $model->get_fax_numbers(JFactory::getUser()->id, true);
		$user_info->work_numbers = $model->get_work_numbers(JFactory::getUser()->id, true);
		$user_info->websites = $model->get_websites(JFactory::getUser()->id, true);
		$user_info->socialsites = $model->get_socialsites(JFactory::getUser()->id, true);
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('user', $user_info);
		$view->assignRef('contact_options', $model->get_contact_options(JFactory::getUser()->id));
		$view->assignRef('notiftypes', $model->get_payment_methods());
		$view->display($this->getTask());
	}
	public function emailsnotif(){
		$model = $this->getModel('UserProfile');
		$view = &$this->getView($this->getName(), 'html');
		//$view->assignRef('contact_options', $model->get_contact_options(JFactory::getUser()->id));
		$view->assignRef('contact_options', $model->retrieveNotifcations());
		$view->assignRef('notiftypes', $model->get_payment_methods());
		$view->assignRef('user', $model->get_profile_info(JFactory::getUser()->id, true));
		$view->display($this->getTask());
	}
	
	public function update_single_designation(){
		$model 	= $this->getModel('UserProfile');
		if( JRequest::getVar('desgination_id')!="" ) {
			$designation_id = JRequest::getVar('desgination_id');
			$model->update_single_designation( $designation_id );
		}
		die();
	}
	
	//delete designation on delete click
	public function delete_single_designation(){
		$model 			= $this->getModel('UserProfile');
		$designation_id = $_POST['desgination_id'];
		$model->delete_single_designation( $designation_id );
		die();
	}
	
	public function edit(){

		$model = $this->getModel('UserProfile');
		if($_POST['jform']['form']=="user_edit"){
			$model->update($_POST['jform']);
			die();
		}
		else if($_POST['jform']['form']=="contact_edit"){
			$model->update($_POST['jform'], true);
	
			JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_userprofile&task=contact'));
		}
		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);
		
		$user_info->email = $model->get_user_registration(JFactory::getUser()->id, true)->email ;
		
		$user_info->mobile_numbers = $model->get_mobile_numbers(JFactory::getUser()->id, true);
		$user_info->fax_numbers = $model->get_fax_numbers(JFactory::getUser()->id, true);
		$user_info->work_numbers = $model->get_work_numbers(JFactory::getUser()->id, true);
		$user_info->websites = $model->get_websites(JFactory::getUser()->id, true);
		$user_info->socialsites = $model->get_socialsites(JFactory::getUser()->id, true);
		$user_info->brokerage_license = $model->get_user(JFactory::getUser()->id, true)->brokerage_license;
		
		$user_info->volume_2012 = $model->get_user(JFactory::getUser()->id)->volume_2012 ;
		
		$user_info->volume_2013 = $model->get_user(JFactory::getUser()->id)->volume_2013;		
		
		$user_info->volume_2014 = $model->get_user(JFactory::getUser()->id)->volume_2014;

		$user_info->volume_2015 = $model->get_user(JFactory::getUser()->id)->volume_2015;

		
		$user_info->sides_2012 = $model->get_user(JFactory::getUser()->id)->sides_2012;
		
		$user_info->sides_2013 = $model->get_user(JFactory::getUser()->id)->sides_2013;		
		
		$user_info->sides_2014 = $model->get_user(JFactory::getUser()->id)->sides_2014;

		$user_info->sides_2015 = $model->get_user(JFactory::getUser()->id)->sides_2015;

		
		$user_info->ave_price_2012 = $model->get_user(JFactory::getUser()->id)->ave_price_2012 ;
		
		$user_info->ave_price_2013 = $model->get_user(JFactory::getUser()->id)->ave_price_2013 ;		
		
		$user_info->ave_price_2014 = $model->get_user(JFactory::getUser()->id)->ave_price_2014  ;

		$user_info->ave_price_2015 = $model->get_user(JFactory::getUser()->id)->ave_price_2015 ;
		
		
		$user_info->verified_2012 = $model->get_user(JFactory::getUser()->id)->verified_2012 ;
		
		$user_info->verified_2013 = $model->get_user(JFactory::getUser()->id)->verified_2013 ;
		
		$user_info->verified_2014 = $model->get_user(JFactory::getUser()->id)->verified_2014 ;

		$user_info->verified_2015 = $model->get_user(JFactory::getUser()->id)->verified_2015;
	
		$userid = JFactory::getUser()->id;
			$prog_count = 0;
	
		if(JFactory::getUser()->id==$userid){
			$t_date = strtotime('31-01-'.date("Y"));
			$now = time();

			if($t_date > $now) {
			    $time = strtotime("-2 years", time());
			} else {
			    $time = strtotime("-1 year", time());
			}

  			$prev_year = date("Y", $time);
  			
			$progressData = $model->getProgressData($userid);
			$prev_year_data = 0;
			$prev_year_data_inc = 0;
			
			//var_dump($is_100);
			if($progressData[0]->brokerage_license || $progressData[0]->is_term_accepted){
				$prog_count++;
			}
	
			if(($progressData[0]->desig_count && $progressData[0]->mobile_count &&  $progressData[0]->lang_count) || ($progressData[0]->about && $progressData[0]->mobile_count &&  $progressData[0]->lang_count)){
				$prog_count++;
			}
			if($progressData[0]->{"volume_".$prev_year} && $progressData[0]->{"sides_".$prev_year} ){
				$prev_year_data_inc=1;
				$prog_count++;
			}

			if($prog_count==2 && $prev_year_data_inc==0){
				$prev_year_data=1;
			}
			
			$is_100 = $model->getIs_100($userid);
			if($is_100[0]->is_100 && $prog_count==3){
				$prog_count = 0;
			} else if($prog_count<3){
				$model->updateIs_100_no($user_info->email);
			}

		}
		if($user_info->brokerage)
			$user_info->brokerage_label =  $model->get_broker($user_info->brokerage);

		$getCountryLangs = $this->getCountryLangs();
		$getCountryLangsInitial = $this->getCountryDataByID($user_info->country);

	
		$user_info->languages = array_map(function($obj){ return $obj->lang_id; }, $model->get_user_spoken_language(JFactory::getUser()->id));
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('user', $user_info);
		$country = $model->getCountryIso($user_info->country);
		$view->assignRef('prev_year', $prev_year_data);
		$view->assignRef('prog_count', $prog_count);
		$view->assignRef('countyIso', $country);
		$view->assignRef('getCountryLangsInitial', $getCountryLangsInitial);
		$view->assignRef('country_list', $getCountryLangs);
		$view->assignRef('languages_list', $model->get_languages());
		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));
		$view->assignRef('designations', $model->get_user_designations(JFactory::getUser()->id, true));
		$view->display($this->getTask());
	}

	function getCountryLangs(){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id  ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObjectList();
	}

	public function getCountryDataByID($country){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id WHERE cl.countries_id = ".$country." ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObject();
	}

	public function sampleSend(){
		$model = $this->getModel($this->getName());
		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);
	    $sides=0;
		$volumes=0;
		$year = "";
		$changes = array();
		$model->sendVolumeChanges($user_info,$year,$changes,$volume,$sides);
	}
	public function contacts(){
		$view = $this->getView($this->getName(),'html');
		$model = $this->getModel($this->getName());
		$network_temp = $model->get_network(JFactory::getUser()->id, 1, $search);
	
		// search sting goes here!
		//$str=$_POST['searchstringnetwork'];
		$str=strip_tags(JRequest::getVar('searchstringnetwork'));
		
		$network = array();
		
		//initialize array result
		$search_resultx = array();
		foreach ($network_temp as $single_network){
			// query actual agents contacts
			$contact = $model->get_profile_info($single_network->other_user_id, false);
			
			// query for search 
			$search_result =  $model->search_profile_info($single_network->other_user_id, false, $str);
			if(isset($_POST['searchstringnetwork']) && $_POST['searchstringnetwork']!=""){
				
				echo "<pre>";
				//print_r($contact);
				
				echo "</pre>";
				if(strpos($contact->zip, $str));
				//zip city state country brokerage
			}
			
			// collate results if any
			$search_resultx[] = $search_result;
			$network[] = $contact;
		}
		$view->assignRef('search_string', $str);
		
		// final search result
		$view->assignRef('search_result', $search_resultx);			
		$view->assignRef('network', $network);
		$view->assignRef('user', $model->get_profile_info(JFactory::getUser()->id, true));
		$view->display($this->getTask());
		
		//$this->get_invitation_list();
	}
	public function requestaccess(){
		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $propmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		


		$r1_user_android_token = $model2->getAppUserToken($_POST['uid'],'android');

		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$array_user_tokens[] = $value;
			}


		if($array_user_tokens){
			$model2->android_send_notification($array_user_tokens,"Request to View POPs™", $r1_name." has requested to view your POPs™ ".$pop_name, "Request to View");
		}


		$model = $this->getModel($this->getName());
		$model->sendrequestaccess($_POST['uid'], $_POST['pid'], JFactory::getUser()->id);
		$model->sendrequest_mail_pid($_POST['uid'], JFactory::getUser()->id, $_POST['pid']);
		die();
	}
	public function requestaccess_raw(){
		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $propmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');


		$userId = $_GET['userId'];
		$otherUserId = $_GET['otherUserId'];
		$listingId = $_GET['listingId'];

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.$userId);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		$r1_user_android_token = $model2->getAppUserToken($otherUserId,'android');
		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$array_user_tokens[] = $value;
			}


		if($array_user_tokens){
			$model2->android_send_notification($array_user_tokens,"Request to View POPs™", $r1_name." has requested to view your POPs™, ".$pop_name, "Request to View");
		}

		$model = $this->getModel($this->getName());
		$model->sendrequestaccess($otherUserId, $listingId, $userId);
		$model->sendrequest_mail_pid($otherUserId, $userId, $listingId);
		$response = array('status'=>1, 'message'=>"Request Sent", 'data'=>array());
		echo json_encode($response);
	}
	public function update_brokerage(){
		$broker_id 	= $_POST['broker_id'];
		$country_id = $_POST['country_id']; 
		$model 		= $this->getModel($this->getName());
		echo json_encode( $model->updatebrokerage_info( $_SESSION['uid'], $country_id,  $broker_id ) );
		die();
	}
	
	public function broanddesig(){
		$view = &$this->getView($this->getName(), 'html');
		$model = $this->getModel($this->getName());
		$user_info = $model->get_profile_info(JFactory::getUser()->id, true);
		$w_tax =$model->get_broker_tax_id($user_info->user_id);
		
		if($w_tax !="")
			$user_info->tax_id =  $this->decrypt($w_tax);
		$user_info->country_name =  $model->getCountry($user_info->country);
		$view->assignRef('designationAuto', $model->get_designations($model->get_user_registration(JFactory::getUser()->id)->country));
		$view->assignRef('designations', $model->get_user_designations(JFactory::getUser()->id, true));
		$view->assignRef('country', $model->get_user_registration(JFactory::getUser()->id)->country);
		$view->assignRef('user', $user_info);
		$view->display($this->getTask());
	}
	
	// check Zip Validation based on Country

	public function zipCheck(){

		$ZIPREG=array(
			"US"=>"^\d{5}([\-]?\d{4})?$",
			"UK"=>"^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$",
			"DE"=>"\b((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))\b",
			"CA"=>"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$",
			"FR"=>"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$",
			"IT"=>"^(V-|I-)?[0-9]{5}$",
			"AU"=>"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$",
			"IE"=>"^(A|[C-F]|H|K|N|P|R|T|[V-Y])([0-9])([0-9]|W)( )?([0-9]|A|[C-F]|H|K|N|P|R|T|[V-Y]){4}$",
			"NL"=>"^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$",
			"ES"=>"^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$",
			"DK"=>"^([D-d][K-k])?( |-)?[1-9]{1}[0-9]{3}$",
			"SE"=>"^(s-|S-){0,1}[0-9]{3}\s?[0-9]{2}$",
			"BE"=>"^[1-9]{1}[0-9]{3}$"
		);

		/*$country_code="US";
		$zip_postal="11111";		 

		 
		if ($ZIPREG[$country_code]) {
		 
			if (!preg_match("/".$ZIPREG[$country_code]."/i",$zip_postal)){
				//Validation failed, provided zip/postal code is not valid.
			} else {
				//Validation passed, provided zip/postal code is valid.
			}
		 
		} else {
		 
			//Validation not available
		 
		}*/

		$country = $_POST['country'];
		$zip = $_POST['zip'];
		$valid_return_value = 'valid';
		$invalid_return_value = 'invalid';

		if($country==223){
			$length = strlen($zip);
			if($zip<5){
				echo $invalid_return_value;
			    die();
			} else {
				echo $valid_return_value;
			    die();
			}
		} else if($country==38) {
			if (!preg_match("/".$ZIPREG["CA"]."/i",$zip)){
				//Validation failed, provided zip/postal code is not valid.
				echo $invalid_return_value;
			    die();
			} else {
				//Validation passed, provided zip/postal code is valid.
				echo $valid_return_value;
			    die();
			}
		} else if($country==13) {

			echo $valid_return_value;
			die();

		} else if($country==222) {
			    // Start config

				$string = $zip;			    
			    $exceptions = array('BS981TL', 'BX11LT', 'BX21LB', 'BX32BB', 'BX55AT', 'CF101BH', 'CF991NA', 'DE993GG', 'DH981BT', 'DH991NS', 'E161XL', 'E202AQ', 'E202BB', 'E202ST', 'E203BS', 'E203EL', 'E203ET', 'E203HB', 'E203HY', 'E981SN', 'E981ST', 'E981TT', 'EC2N2DB', 'EC4Y0HQ', 'EH991SP', 'G581SB', 'GIR0AA', 'IV212LR', 'L304GB', 'LS981FD', 'N19GU', 'N811ER', 'NG801EH', 'NG801LH', 'NG801RH', 'NG801TH', 'SE18UJ', 'SN381NW', 'SW1A0AA', 'SW1A0PW', 'SW1A1AA', 'SW1A2AA', 'SW1P3EU', 'SW1W0DT', 'TW89GS', 'W1A1AA', 'W1D4FA', 'W1N4DJ');
			    // Add Overseas territories ?
			    array_push($exceptions, 'AI-2640', 'ASCN1ZZ', 'STHL1ZZ', 'TDCU1ZZ', 'BBND1ZZ', 'BIQQ1ZZ', 'FIQQ1ZZ', 'GX111AA', 'PCRN1ZZ', 'SIQQ1ZZ', 'TKCA1ZZ');
			    // End config


			    $string = strtoupper(preg_replace('/\s/', '', $string)); // Remove the spaces and convert to uppercase.
			    $exceptions = array_flip($exceptions);
			    if(isset($exceptions[$string])){return $valid_return_value;} // Check for valid exception
			    $length = strlen($string);
			    if($length < 5 || $length > 7){return $invalid_return_value;} // Check for invalid length
			    $letters = array_flip(range('A', 'Z')); // An array of letters as keys
			    $numbers = array_flip(range(0, 9)); // An array of numbers as keys

			    switch($length){
			        case 7:
			            if(!isset($letters[$string[0]], $letters[$string[1]], $numbers[$string[2]], $numbers[$string[4]], $letters[$string[5]], $letters[$string[6]])){break;}
			            if(isset($letters[$string[3]]) || isset($numbers[$string[3]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			        case 6:
			            if(!isset($letters[$string[0]], $numbers[$string[3]], $letters[$string[4]], $letters[$string[5]])){break;}
			            if(isset($letters[$string[1]], $numbers[$string[2]]) || isset($numbers[$string[1]], $letters[$string[2]]) || isset($numbers[$string[1]], $numbers[$string[2]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			        case 5:
			            if(isset($letters[$string[0]], $numbers[$string[1]], $numbers[$string[2]], $letters[$string[3]], $letters[$string[4]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			    }

			    echo $invalid_return_value;
			    die();

		} else if($country==103){

			if (!preg_match("/".$ZIPREG["IE"]."/i",$zip)){
				//Validation failed, provided zip/postal code is not valid.
				echo $invalid_return_value;
			    die();
			} else {
				//Validation passed, provided zip/postal code is valid.
				echo $valid_return_value;
			    die();
			}
			die();
		}
	
	}

	public function savebroker(){
		$model = $this->getModel($this->getName());
		if($model->updatebrokerage(JFactory::getUser()->id, $_REQUEST['brokerid']))
			echo 'success';
		else
			echo 'failed';
		die();
	}
	public function acceptrequest(){
		$db = JFactory::getDbo();
		$edit = new JObject();
		$edit->permission = 1;
		$edit->pkId = $_POST['id'];
		//echo "pk id: " . $_POST['id']; die();
		$result = $db->updateObject('#__request_access', $edit, 'pkId');
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)7;
		$insert_activity->user_id =  $_POST['other'];
		$insert_activity->activity_id =  $_POST['id'];
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = JFactory::getUser()->id;
		$ret_act = $db->insertObject('#__activities', $insert_activity);

		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $propmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		$r1_user_android_token = $model2->getAppUserToken($_POST['other'],'android');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__pocket_listing pl');
		$query->where('pl.listing_id='.$_POST['id']);
		$db->setQuery($query);
		$pop_name = $db->loadObject()->property_name;

		if($r1_user_android_token)
			foreach ($r1_user_android_token as $key => $value) {
				# code...
				$array_user_tokens[] = $value;
			}


		if($array_user_tokens){
			$model2->android_send_notification($array_user_tokens,"Request to View POPs™", $r1_name." has accepted your request to view ".$pop_name, "Request to View");
		}
		

			
		die();
	}
	public function getlanguage($lid){
		$db = JFactory::getDbo();
		$query= $db->getQuery(true);
		$query->select('language')
		->from('#__user_languages')
		->where('lang_id = '.$lid);
		$db->setQuery($query);
		return $db->loadObject()->language;
	}
	public function getdesignation($lid){
		$db = JFactory::getDbo();
		$query= $db->getQuery(true);
		$query->select('designations')
		->from('#__designations')
		->where('id = '.$lid);
		$db->setQuery($query);
		return $db->loadObject()->designations;
	}
	public function unmap($column){
		$selected = trim($column);
		if($selected=="cell"){
			return "Mobile Phone";
		}
		else if($selected=="s_languages"){
			return "Languages";
		}
		else if($selected=="designations"){
			return "Designations";
		}
		else if($selected=="about"){
			return "About me";
		}
		else if($selected=="email"){
			return "Email";
		}
		else if($selected=="wfax"){
			return "Fax";
		}
		else if($selected=="wphone"){
			return "Work Phone";
		}
		else if($selected=="city"){
			return "City";
		}
		else if($selected=="zip"){
			return "Zip";
		}
		else if($selected=="country"){
			return "Country";
		}
		else if($selected=="street_address"){
			return "Address line 1";
		}
		else if($selected=="brokerage"){
			return "Brokerage";
		}
		else if($selected=="state"){
			return "State";
		}
		else if($selected=="suburb"){
			return "Address line 2";
		}
		else if($selected="imagelink"){
			return "Image link";
		}
		else if($selected="image"){
			return "Image";
		}
	}
	public function updatelisting(){
		$model = &$this->getModel('UserProfile');
		$listing_owner = $model->getListingOwner($_REQUEST['id']);
		if(JFactory::getUser()->id != $listing_owner)
			echo json_encode(array('code'=>0, 'value'=>'you do not have permission to edit this listing'));
		else {
			$date = date('Y-m-d', strtotime($_REQUEST['value']));
			$lisiting_id = $_REQUEST['id'];
			
			$ts1 = strtotime(date('Y-m-d'));
			$ts2 = strtotime($_REQUEST['value']);
			$seconds_diff = $ts2 - $ts1;
			$days = floor($seconds_diff/3600/24);
			
			
			if($days>0) {
				$model->updatePropertyExpiry(
					$lisiting_id , 
					$date
				);
				echo json_encode(array('code'=>1, 'days' => $days." ", 'value'=>'You have successfully update the expiry of your listing to '.$days.' days'));
			} else {
				echo json_encode(array('code'=>0, 'value'=>'You cannot set the expiry to this date'));
			}
			
		}
		die();
	}
	public function update_setting(){
		$model = &$this->getModel('UserProfile');
		$update_object = new JObject();
		$update_object->show = $_REQUEST['value'];
		$update_object->pk_id = $_REQUEST['pkid'];
		$model->insertIfnotExists($update_object, 'pk_id', $_REQUEST['table']);
		echo json_encode($_REQUEST);
		die();
	}
	public function savenotifs(){
		$notiftype = new JObject();
		$notiftype->contact_method = $_REQUEST['select2'];
		$notiftype->contact_options = serialize($_REQUEST['chbox']);
		$notiftype->user_id = JFactory::getUser()->id;
		$model = &$this->getModel('UserProfile');
		$model->insertIfnotExists($notiftype, 'user_id', '#__user_contact_method');
		//JFactory::getApplication()->enqueueMessage('You have successfully updated your notification settings' ,'success');
		//$this->setRedirect(JRoute::_('index.php?option=com_userprofile&task=contact'));
		$this->setRedirect(JRoute::_('index.php?option=com_activitylog'));
	}
	
	public function saveNotification() {
		$values = json_encode( JRequest::getVar('chbox') );
		$model  = &$this->getModel('UserProfile');
		$model->saveNotification( JFactory::getUser()->id, $values );
		die();
	}
	public function search(){
		$view = $this->getView($this->getName(), 'html');
		$view->display('guestview');
	}
	public function searchresult(){
		$columns = array();
		$columns['p.bedroom']['regex']=	'/((with)|([0-9]+)) bedroom/';
		$columns['p.bathroom']['regex']=	'/((with)|([0-9]+)) baths/';
		$columns['p.view']['regex']=	'/(none)|(panoramic)|(city)|(mountains\/hills)|(coastline)|(coastline)|(water)|(ocean)|(lake\/river)|(landmark)|(desert)|(bay)|(vineyard)|(golf)|(other)/';
		$columns['p.style']['regex']=	'/(american farmhouse)|(art deco)|(art modern\/mid century)|(cape cod)|(colonial revival)|(contemporary)|(craftsman)|(french)|(italian\/tuscan)|(prairie style)|(pueblo revival)|(ranch)|(spanish\/mediterranean)|(swiss cottage)|(tudor)|(victorian)|(historic)|(artichitucaul significnat)|(green)/';
		$columns['p.pool_spa']['regex']=	'/(with )?((pool)|(pool and spa)|(spa)|(spa and pool))/';
		$columns['p.condition']['regex']=	'/(fixer)|(good)|(excellent)|(remodeled)|(new construction)|(under construction)|(not disclosed)/';
		$columns['p.garage']['regex']=	'/((with)|([0-9]+)) garage/';
		$columns['p.units']['regex']=	'/(triplex)|(quad)|(land)/';
		$columns['p.occupancy']['regex']=	'/([0-9]+ occupants)/';
		$columns['p.type']['regex']=	'/(office)|(institutinal)|(medical)|(warehouse)|(condo)|(r&d)|(business park)|(land)|(flex space)|(manufacturing)|(office showroom)|(self\/mini storage)|(truck terminal\/hub)|(distribuiotn)|(cold storage)|(community center)|(strip center)|(outlet center)|(power center)|(anchor)|(restaurant)|(service station)|(retail pad)|(free standing)|(day care\/nursery)|(post office)|(vehicle)|(economy)|(full service)|(assisted)|(acute care)|(golf)|(marina)|(theater)|(religious)/';
		$columns['p.listing_class']['regex']=	'/(class [a-eA-E])/';
		$columns['p.stories']['regex']=	'/([0-9]+ ((story)|(stories)))/';
		$columns['p.room_count']['regex']=	'/([0-9]+ (rooms?))/';
		$columns['p.type_lease']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';
		$columns['p.type_lease2']['regex']=	'/(nnn)|(fsg)|(mg)|(modified net)/';
		$columns['p.term']['regex']=	'/(blank)|(short term)|(m to m)|(year lease)|(multi year lease)/';
		$columns['p.furnished']['regex']=	'/(furnished)|(unfurnished)/';
		$columns['p.pet']['regex']=	'/(pet)/';
		$columns['p.zoned']['regex']=	'/((with)|([0-9]+)) units/';
		$columns['p.bldg_type']['regex']=	'/(north facing)|(south facing)|(east facing)|(west facing)|(low rise)|(mid rise)|(high rise)|(co-op)|(undisclosed)|(detached)|(attached)/';
		$columns['p.features1']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';
		$columns['p.features2']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';
		$columns['p.features3']['regex']=	'/([1-3] story)|(blank)|(one story)|(two story)|(three story)|(water access)|(horse property)|(golf course)|(walkstreet)|(media room)|(guest house)|(wine cellar)|(tennis court)|(den\/library)|(green const.)|(basement)|(rv\/boat parking)|(senior)|(gym)|(security)|(tennis cour)|(doorman)|(penthouse)|(sidewalks)|(utilities)|(curbs)|(horse trails)|(rural)|(urban)|(suburban)|(permits)|(hoa)|(sewer)|(cc&rs)|(coastal)|(assoc-pool)|(assoc-spa)|(assoc-tennis)|(assoc-other)|(section 8)|(25% occupied)|(50% occupied)|(75% occupied)|(100% occupied)|(cash cow)|(value add)|(seller carry)|(mixed use)|(single tenant)|(multiple tenant)|(net-leased)|(owner user)|(vacant)|(restaurant)|(bar)|(pool)|(banquet room)/';
		$filters = array();
		echo "<pre>";
		foreach($columns as $key => $column){
			//echo $key.'<br/>';
			//echo $column['regex']."<br/>";
			preg_match($column['regex'], $_REQUEST['keys'], $matches);
			if(count($matches)){
				$column['matches'] = $matches[0];
				$constants = array('with', 'bedroom', 'bedrooms', 'bath', 'baths', 'stories');
				$column['processed'] = str_replace($constants, '', $matches[0]);
				$filters[$key] = $column['processed'];
				print_r($column['processed']);
			}
			//echo "______________<br/>";
			//print_r($column['matches']);
		}
		$model = $this->getModel($this->getName());
		print_r($model->get_listing($filters, "OR"));
		echo "sad";
		echo "</pre>";
		die();
	}
	
	// count current invites
	// SELECT * FROM tbl_invitation_tracker a LEFT JOIN tbl_user_registration b ON a.agent_id = b.user_id;
	public function count_current_invites(){
		$model = $this->getModel($this->getName());
		echo $model->get_current_invites_count()->current_invite_count;
		die();
	}
	
	
	// check if existing user exists
	public function invites(){
		$model = $this->getModel($this->getName());
		$email = JRequest::getVar('email');
		$user_count = $model->check_existing_accounts( $email );
		$user_details = $model->get_existing_details( $email ); 
		
		$user_type = $model->get_user_type($email);
		// pending invitation but doesn't belong to charter list
		if ( $user_type->user_type==2 && $user_count->user_id_count==1 && $user_details->activation_status==0 ) {
			echo json_encode( 
				array(
					'response' 	=> 1, 
					'firstname' => stripslashes_all($user_details->firstname),
					'lastname' 	=> stripslashes_all($user_details->lastname),
					'inviter' 	=> stripslashes_all($user_details->agent_id),
					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id
				) 
			);
		// manual applicant not approved yet and invited by charter
		} else if ($user_type->user_type==3 && $user_count->user_id_count==1 && $user_details->activation_status==0) {
			echo json_encode( 
				array(
					'response' 	=> 2, 
					'firstname' => stripslashes_all($user_details->firstname),
					'lastname' 	=> stripslashes_all($user_details->lastname),
					'inviter' 	=> stripslashes_all($user_details->agent_id),
					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id
				) 
			);
		} 
		// existing charter but not activated
		
		else if ($user_type->user_type==1 && $user_count->user_id_count==1 && $user_details->activation_status==0) {
			echo json_encode( 
				array(
					'response' 	=> 3, 
					'firstname' => stripslashes_all($user_details->firstname),
					'lastname' 	=> stripslashes_all($user_details->lastname),
					'email' 	=> $model->get_user_registration( JFactory::getUser()->id )->email
				) 
			);
	
		}
		
		// existing charter but not activated, and invited 
		
		else if ($user_type->user_type==4 && $user_count->user_id_count==1 && $user_details->activation_status==0) {
			echo json_encode( 
				array(
					'response' 	=> 4, 
					'firstname' => stripslashes_all($user_details->firstname),
					'lastname' 	=> stripslashes_all($user_details->lastname),
					'inviter' 	=> stripslashes_all($user_details->agent_id),
					'match' 	=> $model->get_user_registration( JFactory::getUser()->id )->user_id
				) 
			);
		} 
		
		// activated member
		else if ($user_count->user_id_count==1 && $user_details->activation_status==1) {
			echo json_encode( 
				array(
					'response' 	=> 5, 
					'firstname' => stripslashes_all($user_details->firstname),
					'lastname' 	=> stripslashes_all($user_details->lastname),
					
					'match' 	=> $user_details->id
					
				) 
			);
		} 
		
		
		else {
		
			echo json_encode( 
				array(
					'response' 	=> 0,
					
					'count'		=> $user_count->user_id_count,
					
					'activation' => $user_details,
					
					'email' => $email
				) 
			);
			die();
		} 
	
	}
	
	public function get_invitation_list() {
		$model = $this->getModel($this->getName());
		echo json_encode( $model->get_invited_emails() );
		die();
	}
	
	public function emailInvites(){
		$model = $this->getModel($this->getName());
		$mainframe = JFactory::getApplication();
		$invited_email = JRequest::getVar('invitedEmail');
		$email = JFactory::getUser()->email;
		
		$first_name    = JRequest::getVar('firstName');
		$last_name     = JRequest::getVar('lastName');
		$gender        = JRequest::getVar('gender');
		$country_id    = JRequest::getVar('countryId');
		$model->insert_invited_emails( $invited_email, $email, $first_name, $last_name, $gender, $country_id );		
		$model->notifyAdminInvitation( $email, $invited_email, $first_name, $last_name, $gender, $country_id  );	
	}
	
	public function emailInvitesExisting(){
		$model = $this->getModel($this->getName());
		$mainframe = JFactory::getApplication();
		$agent_email = JFactory::getUser()->email;
		$email   = JRequest::getVar('invitedEmail');
		
		$first_name    = JRequest::getVar('firstName');
		$last_name     = JRequest::getVar('lastName');
		$gender        = JRequest::getVar('gender');
		$zip_code      = JRequest::getVar('zipCode');
		$country_id    = JRequest::getVar('countryId');
		$model->insert_to_inviteusertype( $agent_email, $email, $first_name, $last_name, $gender, $zip_code, $country_id  );		
		$model->notifyAdminInvitation( $agent_email, $email, $first_name, $last_name, $gender, $zip_code, $country_id  );	
	}
	
	public function emailInvitesExistingManual(){
		$model = $this->getModel($this->getName());
		$mainframe = JFactory::getApplication();
		$agent_email = JFactory::getUser()->email;
		$email   = JRequest::getVar('invitedEmail');
		
		$first_name    = JRequest::getVar('firstName');
		$last_name     = JRequest::getVar('lastName');
		$gender        = JRequest::getVar('gender');
		$zip_code      = JRequest::getVar('zipCode');
		$country_id    = JRequest::getVar('countryId');
		$model->insert_to_inviteusertypemanual( $agent_email, $email, $first_name, $last_name, $gender, $zip_code, $country_id  );		
		$model->notifyAdminInvitation( $agent_email, $email, $first_name, $last_name, $gender, $zip_code, $country_id  );	
	}
	public function get_current_registered_user(){
		$model = $this->getModel($this->getName());
		$user_info = $model->get_user_registration( JFactory::getUser()->id );
		$user_info->licence = ($user_info->licence) ? $this->decrypt($user_info->licence) : "";
		$user_info->brokerage_license = ($user_info->brokerage_license) ? $this->decrypt($user_info->brokerage_license) : "";
		$user_info->tax_id_num = ($user_info->tax_id_num) ? $this->decrypt($user_info->tax_id_num) : "";
		echo json_encode( $user_info );
		die();
	}
	
	public function encrypt($plain_text) {
	
		$key = 'password to (en/de)crypt';
			
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
		
	}	
	
	public function decrypt($encrypted_text) {
	$key = 'password to (en/de)crypt';
	
	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	
	return $decrypted;
}
	
	public function decrypt_license() {
		echo $this->decrypt(JRequest::getVar('alslno'));
		die();
	}
	
	public function decrypt_taxid() {
		$btino = $model->get_broker_tax_id(JFactory::getUser()->id);
		
		if($btino !="")
			echo $this->decrypt(JRequest::getVar('btino'));
		die();
	}
	
	public function requestaccessfromemail(){
		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $propmodel );
		$model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		$query->leftjoin('#__user_registration ur on u.email = ur.email');
		$query->where('u.id='.JFactory::getUser()->id);
		$db->setQuery($query);
		$r1_name = $db->loadObject()->name;

		


		$r1_user_android_token = $model2->getAppUserToken($_GET['uid'],'android');

		if($r1_user_android_token){
			$i=0;
			foreach ($r1_user_android_token as $key => $value) {
				# code...				
				if($value['device_os']=='ios'){
					$array_user_ios_tokens[$i]['devicetoken'] = trim($value['device_token']);
					$array_user_ios_tokens[$i]['messages']= $r1_name." has requested to view your POPs™";
					//$this->ios_send_notification($value['device_token'],"New Referral", $r1_name." has sent you a referral", "Referrals");
					$i++;
				} else {
					$array_user_tokens[] = $value['device_token'];
				}
			}
		}
			


		if($array_user_tokens){
			$model2->android_send_notification($array_user_tokens,"Request to View POPs™", $r1_name." has requested to view your POPs™ ".$pop_name, "Request to View");
		}

		if($array_user_ios_tokens){
			$model2->ios_send_notification("Request to View POPs™","test",$array_user_ios_tokens);
		}
		


		$model = $this->getModel($this->getName());
		$model->sendrequestaccess($_GET['uid'], $_GET['pid'], JFactory::getUser()->id);
		$model->sendrequest_mail_pid($_GET['uid'], JFactory::getUser()->id, $_GET['pid']);

		JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_activitylog'));
	}
	
}
class AuthorizeNetx{
	private $login_name;
	private $transaction_key;
	private $apihost = "api.authorize.net";
	private $apipath = "/xml/v1/request.api";
	
	public function __construct($login_name, $transaction_key){
		$this->login_name = $login_name;
		$this->transaction_key = $transaction_key;
		
	}
	
	public function get_payment_profile($cs_profile_id, $cs_pp_id){
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<getCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>".$cs_profile_id."</customerProfileId>".
 		"<customerPaymentProfileId>".$cs_pp_id."</customerPaymentProfileId>".
		"</getCustomerPaymentProfileRequest>";
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		
		return $parsedresponse;
	}
	
	public function create_payment_profile($address, $city, $state, $zip, $country, $cs_profile_id, $fname, $lname, $number, $cardno, $cardexp, $testmode = 1){
		if($testmode)
			$mode = 'testMode';
		else
			$model = 'liveMode';
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
		$this->MerchantAuthenticationBlock().
		"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".
		"<paymentProfile>".
		"<billTo>".
		"<firstName>". $fname ."</firstName>".
		"<lastName>". $lname ."</lastName>".
		"<address>". $address ."</address>".
		"<city>". $city ."</city>".
		"<state>". $state ."</state>".
		"<zip>". $zip ."</zip>".
		"<country>". $country ."</country>".
		"</billTo>".
		"<payment>".
		"<creditCard>".
		"<cardNumber>". $cardno ."</cardNumber>".
		"<expirationDate>". $cardexp ."</expirationDate>". // required format for API is YYYY-MM
		"</creditCard>".
		"</payment>".
		"</paymentProfile>".
		"<validationMode>". $mode ."</validationMode>". // or testMode
		"</createCustomerPaymentProfileRequest>";
		
		//echo $content;
		
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		
		return $parsedresponse;
	}
	
	public function profile_create($cs_id,$email){
		$content =
		"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
		"<profile>".
		"<merchantCustomerId>".$cs_id."</merchantCustomerId>". // Your own identifier for the customer.
		"<description></description>".
		"<email>" . $email . "</email>".
		"</profile>".
		"</createCustomerProfileRequest>";
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		
		return $parsedresponse;
	}
	
	public function transaction_create($ammount,$cs_profile_id,$cs_payment_id,$invoice){
		$content =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
			"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
			$this->MerchantAuthenticationBlock().
			"<transaction>".
			"<profileTransAuthOnly>".
			"<amount>" . $ammount . "</amount>". // should include tax, shipping, and everything.
			"<lineItems>".
			"<itemId>000000001</itemId>".
			"<name>Service Fee</name>".
			"<description>Service Fee collected by AgentBridge for Referral {clientname}</description>".
			"<quantity>1</quantity>".
			"<unitPrice>".$ammount."</unitPrice>".
			"<taxable>false</taxable>".
			"</lineItems>".
			"<customerProfileId>" . $cs_profile_id . "</customerProfileId>".
			"<customerPaymentProfileId>" . $cs_payment_id . "</customerPaymentProfileId>".
			"<customerShippingAddressId>" . 0 . "</customerShippingAddressId>".
			"<order>".
			"<invoiceNumber>".$invoice."</invoiceNumber>".
			"</order>".
			"</profileTransAuthOnly>".
			"</transaction>".
			"</createCustomerProfileTransactionRequest>";
		
		$response = $this->send_xml_request($content);
		$parsedresponse = $this->parse_api_response($response);
		return $parsedresponse;
	}
	
	private function send_xml_request($content){
		return $this->send_request_via_fsockopen($content);
	}
	
	private function send_request_via_fsockopen($content){
		$posturl = "ssl://" . $this->apihost;
		$header = "Host: $this->host\r\n";
		$header .= "User-Agent: PHP Script\r\n";
		$header .= "Content-Type: text/xml\r\n";
		$header .= "Content-Length: ".strlen($content)."\r\n";
		$header .= "Connection: close\r\n\r\n";
		$fp = fsockopen($posturl, 443, $errno, $errstr, 30);
		if (!$fp)
		{
			$body = false;
		}
		else
		{
			//error_reporting(E_ERROR);
			fputs($fp, "POST $this->apipath  HTTP/1.1\r\n");
			fputs($fp, $header.$content);
			fwrite($fp, $out);
			$response = "";
			while (!feof($fp))
			{
				$response = $response . fgets($fp, 128);
			}
			fclose($fp);
			//error_reporting(E_ALL ^ E_NOTICE);
	
			$len = strlen($response);
			$bodypos = strpos($response, "\r\n\r\n");
			if ($bodypos <= 0)
			{
				$bodypos = strpos($response, "\n\n");
			}
			while ($bodypos < $len && $response[$bodypos] != '<')
			{
				$bodypos++;
			}
			$body = substr($response, $bodypos);
		}
		return $body;
	}
	
	private function parse_api_response($content){
		$parsedresponse = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOWARNING);
		return $parsedresponse;
	}
	
	private function MerchantAuthenticationBlock() {
		return
		"<merchantAuthentication>".
		"<name>" . $this->login_name . "</name>".
		"<transactionKey>" . $this->transaction_key . "</transactionKey>".
		"</merchantAuthentication>";
	}
}
?>