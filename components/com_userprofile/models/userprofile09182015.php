<?php
defined('_JEXEC') or die;
class UserProfileModelUserProfile extends JModelLegacy {
	
	public function getReferralTable(){
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(true);
		
		$query->setQuery("
			SELECT 
				*
			FROM 
				#__referral_fee"
		);
		$db->setQuery($query);
		
		return $db->loadObjectList();
	}
	public function getProgressData($user_id){
		
		$time = strtotime("-1 year", time());
  		$prev_year = date("Y", $time);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("count(ud.desig_id) AS desig_count,ur.email,ur.about,ur.is_term_accepted,ur.licence,ur.brokerage_license,count(umn.value) AS mobile_count,us.volume_".$prev_year.",us.sides_".$prev_year.",ur.about, count(ul.lang_id) as lang_count");
		$query->from('#__users u');
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');
		$query->leftJoin('#__user_mobile_numbers umn ON ur.user_id = umn.user_id');
		$query->leftJoin('#__user_sales us ON ur.user_id = us.agent_id');
		$query->leftJoin('#__user_designations ud ON ud.user_id = u.id');
		$query->leftJoin('#__user_lang_spoken ul ON u.id = ul.user_id');
		$query->where('id = '.$user_id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function getIs_100($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("ur.is_100");
		$query->from('#__users u');
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');
		$query->where('id = '.$user_id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function updateIs_100($user_email){
		$db = JFactory::getDbo();
		$fields = array("is_100=1");							
		$conditions = array($db->quoteName('email') . '=\''.$user_email.'\'');							
		$query = $db->getQuery(true);
		$query->update($db->quoteName('#__user_registration'))->set($fields)->where($conditions);							 
		// Set the query using our newly populated query object and execute it.
		$db->setQuery($query);
		return $db->execute();
	}
	
	//insert activity for password change
	public function insertUpdatePassword ($userid) {
		$db = JFactory::getDbo();
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)31;
		$insert_activity->user_id = (int)$userid;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = $userid;
		$ret_activity = $db->insertObject('#__activities', $insert_activity);
	
	}

    public function get_user_details_bc($select, $regid){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select($select);

		$query->from('#__user_registration ur');

		$query->where('user_id = '.$regid);

		$query->leftJoin('#__countries c ON c.countries_id = ur.country');

		$query->leftJoin('#__country_languages cl ON cl.country = ur.country');

		$query->leftJoin('#__broker br ON br.broker_id = ur.brokerage');

		$query->leftJoin('#__zones z ON z.zone_id = ur.state');

		$db->setQuery($query);

		return $db->loadObject();

	}

	public function get_user_details($select, $regid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($select);
		$query->from('#__user_registration');
		$query->where('user_id = '.$regid);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function get_user_group($regid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("group_id");
		$query->from('#__user_usergroup_map');
		$query->where('user_id = '.$regid);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function get_broker_data($brokerage){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('broker_name');
		$query->from('#__broker');
		$query->where('broker_id = '.$brokerage);
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	//get sales data
	public function get_salesV2012($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2012 = $db->loadObjectList();
		return $sales2012[0];
	}
	public function get_salesV2013($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2013 = $db->loadObjectList();
		return $sales2013[0];
	}


	public function addUserToChimp($user_details, $process="add"){

		$apikey = 'df5c601248a76b681350af7249052b3d-us11';
		$email=$user_details['email'];

		if($process=="add"){
			
			$fname=$user_details['fname'];
	        $lname=$user_details['lname'];
	        $broker=$user_details['broker'];
	        $state=$user_details['state'];
	        $country=$user_details['country'];
	        $zip=$user_details['zip'];
	        $city=$user_details['city'];


			
			$type = "POST";
			$target = "lists/3f34696141/members";
		    $data = array(
                'apikey'        => $apikey,
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $fname,
                    'LNAME' => $lname,
                    'BROKER' => $broker,
                    'STATE' => $state,
                    'COUNTRY' => $country,
                    'ZIP' => $zip,
                    'CITY' => $city,
                )
            );

		} else if($process=="subs_change"){

				$email=$user_details['email'];
				$subs_val = $user_details['subs_val'];

				$type = "PATCH";
				$target = "lists/3f34696141/members/".md5($email);
			    $data = array(
	                'apikey'        => $apikey,
	                'email_address' => $email,
	                'status'        => $subs_val,	              
	            );
		} else if($process=="update"){

				$email=$user_details['email'];
							$fname=$user_details['fname'];
	        $lname=$user_details['lname'];
	        $broker=$user_details['broker'];
	        $state=$user_details['state'];
	        $country=$user_details['country'];
	        $zip=$user_details['zip'];
	        $city=$user_details['city'];

				$type = "PATCH";
				$target = "lists/3f34696141/members/".md5($email);
			    $data = array(
	                'apikey'        => $apikey,
	                'email_address' => $email,
	                'status'        => 'subscribed',
	                'merge_fields'  => array(
	                    'FNAME' => $fname,
	                    'LNAME' => $lname,
	                    'BROKER' => $broker,
	                    'STATE' => $state,
	                    'COUNTRY' => $country,
	                    'ZIP' => $zip,
	                    'CITY' => $city,
	                )              
	            );
		}

        $api=array("login"=>"dwant@agentbridge.com","key"=>'df5c601248a76b681350af7249052b3d-us11',"url"=>'https://us11.api.mailchimp.com/3.0/');

		$result = $this->mc_request($api,$type,$target,$data);


		$mailSender =& JFactory::getMailer();
			$mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
			$mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
			$mailSender ->setSubject( $subject );
			$mailSender ->isHTML(  true );
			$mailSender ->setBody(  $result.json_encode($user_details) );
			$mailSender ->Send();

		return $result;
		//var_dump($result);
	}


	// ---------------------------------------------------------------------------------------------- //
	/*	mc_request

		$api = array
		(
			'login' => 'CanBeAnything',
			'key'   => 'YourAPIKey',
			'url'   => 'https://<dc> .api.mailchimp .com/3.0/
		)

		$type		'GET','POST','PUT','PATCH','DELETE'

		$target		Whatever you're after from the API. Could be:
					'', 'lists', lists/abcd12345',
					'lists/abcd12345/members/abcd12345abcd12345abcd12345abcd12345', etc.

		$data		Associative array with the key => values to be passed.
					Don't forget to match the strucutre of whatever you're trying to PATCH
					or PUT. e.g., For patching a member's first name, use:

					$data = array( 'merge_fields' => array( 'FNAME': "New Name" ) );
					mc_request( $my_api_info, 'PATCH',
						'lists/abcd12345/members/abcd12345abcd12345abcd12345abcd12345', $data );

		If you need to tunnel your requests through a service that only supports POST through Curl,
		uncomment the two lines below and comment out:
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );	
	*/
	// ---------------------------------------------------------------------------------------------- //

	function mc_request( $api, $type, $target, $data = false )
	{
		$ch = curl_init( $api['url'] . $target );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array
		(
			'Content-Type: application/json', 
			'Authorization: ' . $api['login'] . ' ' . $api['key'],
	//		'X-HTTP-Method-Override: ' . $type,
		) );

	//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );

		if( $data )
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );

		$response = curl_exec( $ch );
		curl_close( $ch );

		return $response;
	}

	
	
	
	// get current subscription of the user
	public function get_subscription_type( $fee_id ) {
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery(
			"
				SELECT	
					mf.fee_title as fee_title,
					mf.fee_id
				FROM
					#__membership_fees mf
				WHERE
					mf.fee_id = $fee_id
			"
		);
				
		$db->setQuery( $query );
	    return $db->loadObject()->fee_title."|".$db->loadObject()->fee_id;	
	}
	
	// update users subscription
	public function update_user_subsctiption( $user_id, $promo_code_id, $schedule ) {
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery(
			"
				SELECT	
					mf.fee_id as fee_id
				FROM
					#__membership_fees mf
				WHERE
					mf.promo_code = $promo_code_id
				AND 
					mf.fee_title = '".$schedule."'
			"
		);
		
		
		$db->setQuery( $query );
	    $fee_id = $db->loadObject()->fee_id;
		
		$query->setQuery(
			"
				UPDATE 
					#__user_registration ur
				SET
					ur.fee_id = $fee_id
				WHERE
					ur.user_id = $user_id
			"
		);
		
		$db->setQuery( $query );
		$db->execute();
		
	}
	// get referral fee
	public function get_referral_fees() {
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery(
			"
				SELECT	
					rf.tier1,
					rf.tier2,
					rf.r1_fee,
					rf.r2_fee
				FROM
					#__referral_fee rf
			"
		);
		
		$db->setQuery( $query );
		return $db->loadObjectList();
	}
	
	
	// get payment information
	public function get_payment_options() {
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery(
			"
				SELECT
					mf.fee_id,
					mf.fee_title,
					mf.fee_value,
					mf.promo_code,
					mf.savings
				FROM
					#__membership_fees mf
			"
		);
		
		$db->setQuery( $query );
		return $db->loadObjectList();
	}
	public function encrypt($plain_text) {
	
		$key = 'password to (en/de)crypt';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
	}
	
	public function decrypt($encrypted_text) {
		$key = 'password to (en/de)crypt';
		
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		
		return $decrypted;
	}
	
	public function check_user_invitation( $email ) {
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
	
		$query->setQuery(
			"
				SELECT
					ur.user_id, 
					ur.firstname, 
					ur.lastname, 
					ur.email, 
					ur.is_premium,
					it.invited_email ,
					it.agent_id
				FROM
					#__user_registration ur 
				LEFT JOIN 
					#__invitation_tracker it
				ON
					it.invited_email = ur.email
				WHERE it.invited_email= '".$email."'
			"
		);
		//die($query);
		$db->setQuery( $query );
		return $db->loadObject()->invited_email.",".$db->loadObject()->is_premium;
	}
	
	public function insert_autorization_transaction( $subscriptionId, $result, $state, $country, $cc_expiration, $invoice ) {
	
		$db = JFactory::getDbo();
		$desig_object = new JObject();
		$desig_object->trans_id 	= $subscriptionId;
		$desig_object->amount 		= str_replace(',', '', str_replace('$', '', $result['amount']) );
		$desig_object->state		= $state;
		$desig_object->country 		= $country;
		$desig_object->zip	 		= $result['zip'];
		$desig_object->city	 		= $result['city'];
		$desig_object->card_expiry	= $cc_expiration;
		$desig_object->address	 	= $result['address'];
		$desig_object->firstname	= $result['firstname'];
		$desig_object->lastname		= $result['lastname'];
		$desig_object->email		= $result['email'];
		$desig_object->phone		= $result['phone'];
		$desig_object->invoice		= $invoice;
		
		$db->insertObject('#__authorize_transactions', $desig_object);
	}
		
	public function check_if_terms_accepted(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT is_term_accepted FROM #__user_registration
			WHERE email = '".JFactory::getUser()->email."' LIMIT 1" 
		);
		$db->setQuery($query);
		$db->execute();
		return $db->loadObjectList();
	}
	public function update_terms_verification() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery("
			UPDATE #__user_registration
			SET is_term_accepted = 1
			WHERE email = '".JFactory::getUser()->email."'"
		);
		$db->setQuery($query);
		$db->execute();
	}
	// Updates mobile number if user_id exist and insert if not
	public function update_single_numbers(
		$mobile,
		$work,
		$country_id,
		$zip,
		$city,
		$agent_license_number,
		$brokerage_license_number,
		$brokerage,
		$state,
		$street,
		$suburb,
		$volume_2012,
		$sides_2012,
		$volume_2013,
		$sides_2013,
		$volume_2014,
		$sides_2014,
		$terms,
		$date
		
	){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser()->email.'\'');
		$db->setQuery($query);
		$userinfo = $db->loadObject();
		$user_id = $userinfo->user_id;
		if ($mobile) {
			
			$query->setQuery("
			SELECT 
				count(1) as count 
			FROM 
				#__user_mobile_numbers
			WHERE 
				user_id = ".$userinfo->user_id."
			AND 
				value = '".$mobile_number."'"
		);
		$db->setQuery($query);
		
		//detect if any exisiting mobile number
		if($db->loadObject()->count==0) {
		
			$query->setQuery("
				INSERT INTO	
					#__user_mobile_numbers (user_id, value)
				VALUES(".$user_id.", '".$mobile_number."')
			");
			$db->setQuery($query);
			$db->execute();
			}
		
		}
		
		if($work) {
		$query->setQuery("
			SELECT 
				count(1) as count 
			FROM 
				#__user_work_numbers
			WHERE 
				user_id = ".$userinfo->user_id."
			AND 
				value = '".$work_number."'"
		);
		$db->setQuery($query);
		
		//detect if any existing work number
		if($db->loadObject()->count==0) {
			$query->setQuery("
				INSERT INTO	
					#__user_work_numbers (user_id, value)
				VALUES(".$user_id.", '".$work_number."')
			");
			$db->setQuery($query);
			$db->execute();
			}
		}
		//insert other labels
		$query->setQuery("
			UPDATE 
				#__user_registration 
			SET 
				country 		  = ".$country_id.",
				zip 			  = '".$zip."',
				city 			  = '".$city."',
				licence 		  = '".$agent_license_number."',
				brokerage		  = ".$brokerage.",
				state		  	  = ".$state.",
				completeReg_date  = '".$date."'
			WHERE 
				user_id 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		//insert brokerage license
		if ($brokerage_license_number) {
			
			$query->setQuery("
			UPDATE 
				#__user_registration 
			SET 
				brokerage_license = '".$brokerage_license_number."'
			WHERE 
				user_id 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		//insert terms
		if ($terms) {
			
			$query->setQuery("
			UPDATE 
				#__user_registration 
			SET 
				is_term_accepted = ".$terms."
			WHERE 
				user_id 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		
		//insert address
		if ($street || $suburb) {
			
			$query->setQuery("
			UPDATE 
				#__user_registration 
			SET 
				street_address = '".$street."',
				
				suburb = '".$suburb."'
			WHERE 
				user_id 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		//insert sales 
		if ($volume_2014) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				
				volume_2014 		  = ".$volume_2014."
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		if ($sides_2014) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				
				sides_2014 		  = ".$sides_2014."
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		
		if ($volume_2013) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				
				volume_2013 		  = ".$volume_2013."
				
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		
		if ($sides_2013) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				
				sides_2013 		  = ".$sides_2013."
				
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		if ($volume_2012) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				volume_2012 		  = ".$volume_2012."
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
		
		if ($sides_2012) {
		
			$query->setQuery("
			UPDATE 
				#__user_sales 
			SET 
				sides_2012 		  = ".$sides_2012."
			WHERE 
				agent_id 	 	  = ".$user_id
		);
		$db->setQuery($query);		
		$db->execute();
		
		}
	}
	
	public function count_all_listings($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(*) as count');
		$query->from('#__pocket_listing');
		$query->where('user_id = '.$id.' and (closed = 0 OR sold =0)');
		$db->setQuery($query);
		return $db->loadObject()->count;
	}
	
	public function get_client($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__buyer')->where('buyer_id = \''.$id.'\'');
		$db->setQuery($query);
		
		$clientobject=$db->loadObject();
		$clientobject->email=$this->get_buyer_email($clientobject->buyer_id);
		$clientobject->mobile=$this->get_buyer_mobile($clientobject->buyer_id);
		$clientobject->address=$this->get_buyer_address($clientobject->buyer_id);
		
		return $clientobject;
	}
	public function get_buyers($agent){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__buyer b')
			->where('agent_id = '.$agent);
		$buyers = $db->setQuery($query)->loadObjectList();
		foreach ($buyers as $buyer) {
			$buyer->email=$this->get_buyer_email($buyer->buyer_id);
			$buyer->mobile=$this->get_buyer_mobile($buyer->buyer_id);
			$buyer->address=$this->get_buyer_address($buyer->buyer_id);
		}
		return $buyers;
	}
	public function get_buyer_email($buyer_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__buyer_email')
			->where('buyer_id = '.$buyer_id);
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_buyer_mobile($buyer_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__buyer_mobile')
			->where('buyer_id = '.$buyer_id);
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_buyer_note($buyer_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('re_u.note')
			->from('#__referral_status_update re_u')
			->leftJoin('#__referral ref ON re_u.referral_id = ref.referral_id')
			->where('ref.client_id = '.$buyer_id);
		return $db->setQuery($query)->loadObject()->note;
	}
	public function get_buyer_address($buyer_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__buyer_address')
			->where('buyer_id = '.$buyer_id);
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_network($userid, $status = 1){
		$db = JFactory::getDbo();
		$conditions = array();
		$conditions[] = 'user_id = '.$userid;
		$conditions[] = 'status = '.$status;
		$innerquery = $db->getQuery(true);
		$broker_id = $this->get_broker_id(JFactory::getUser($userid)->email, 'email');
		$query = $db->getQuery(true);	
		$add = "";
		$query->setQuery("
	   SELECT DISTINCT	other_user_id
				  FROM	#__request_network
				 WHERE	user_id =".$userid."
				   AND	status = ".$status.$add
				);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_broker($broker_id){
	
		if(!empty($broker_id)) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('broker_name')->from('#__broker')->where('broker_id = '.$broker_id);
			$db->setQuery($query);
			return $db->loadObject()->broker_name;
		
		}
	}
	
	public function get_broker_tax_id($uid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('tax_id_num')->from('#__user_registration')->where('user_id = '.$uid);
		$db->setQuery($query);
		return $db->loadObject()->tax_id_num;
	}
	public function get_broker_info($user_email){
		$db = JFactory::getDbo();
		$broker_id = $this->get_broker_id($user_email, 'email');
		if($broker_id){
			$query = $db->getQuery(true);
			$query->select('*')->from('#__broker')->where('broker_id = '.$broker_id);
			$db->setQuery($query);
			return $db->loadObject();
		}
		return null;
	}
	public function get_broker_id($value, $column = 'email'){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('brokerage')->from('#__user_registration')->where($column.' = \''.$value.'\'');
		$db->setQuery($query);
		return $db->loadObject()->brokerage;
	}
	public function detect_expired_pops($uid, $status = 3, $sold){
	
		$db = JFactory::getDbo();
	
		$query = $db->getQuery(true);
		$closed = ($status==3) ? '' : ' AND pl.closed = '.$status.' OR pl.sold ='.$sold;
		
		$query->setQuery('
				SELECT	pl.*, pp.price_type, pp.price1, pp.price2, pp.disclose,
						pi.image, pv.views, viewers.allowed as allowed, request.request as request,
						pt.*, pst.*, pt.type_name as property_type, pst.name as sub_type
				  FROM	#__pocket_listing as pl
			LEFT JOIN	#__permission_setting as ps
					ON	ps.listing_id = pl.listing_id			
			LEFT JOIN	#__property_price as pp
					ON	pl.listing_id = pp.pocket_id
			 LEFT JOIN	#__property_type pt
					ON	pt.type_id = pl.property_type
			 LEFT JOIN	#__property_sub_type pst
					ON	pst.sub_id = pl.sub_type
			 LEFT JOIN	(
						SELECT image, listing_id
						  FROM #__pocket_images
					  GROUP BY listing_id
						) as pi
					ON	( pl.listing_id = pi.listing_id )
			 LEFT JOIN	(
						SELECT count(distinct(user_id)) as views, property_id
						  FROM #__views
					  GROUP BY property_id
						) as pv
					ON	( pv.property_id = pl.listing_id )
		 	LEFT JOIN	(
						SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed
						  FROM #__request_access racs
						 WHERE permission = 1
					  GROUP BY racs.property_id
						) as viewers
					ON	viewers.id = pp.pocket_id
			LEFT JOIN	(
						SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as request
						  FROM #__request_access racs
						 WHERE permission = 0
					  GROUP BY racs.property_id
						) as request
					ON	request.id = pp.pocket_id
				 WHERE	pl.user_id = '.$uid.$closed);
		$db->setQuery($query);
		$listings=$db->loadObjectList();
		
		foreach($listings as $key => $listing){
		
			$ts1 = strtotime(date('Y-m-d'));
			$ts2 = strtotime($listing->date_expired);
			$seconds_diff = $ts2 - $ts1;
			$days = floor($seconds_diff/3600/24);
			
			if ($days<=0) {
			
				$query->setQuery('
					UPDATE 
						#__pocket_listing
					SET
						closed = 1
					WHERE
						listing_id = '.$listing->listing_id
				);
				$db->execute();	
			}
		}	
	}
	public function get_listings($uid, $status = 3, $sold){
		$propmodel = JPATH_ROOT.'/components/'.'com_propertylisting/models';
		JModelLegacy::addIncludePath( $propmodel );
		$modelPropertyListing =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');
		$db = JFactory::getDbo();
		$closed = ($status!=0) ? ' AND (pl.closed = '.$status. ' OR pl.sold ='.$sold.')' : ' AND (pl.closed = '.$status. ' AND pl.sold ='.$sold.')' ;
		$query = $db->getQuery(true);
		$query->setQuery('
				SELECT	STRAIGHT_JOIN pl.*, pp.price_type, pp.price1, pp.price2, pp.disclose,
						pi.image, pv.views, viewers.allowed as allowed, request.request as request,
						pt.*, pst.*, pt.type_name as property_type, pst.name as sub_type, ps.selected_permission,
						
						count.*
				  FROM	#__pocket_listing as pl
			 LEFT JOIN	#__permission_setting as ps
					ON	ps.listing_id = pl.listing_id
			 LEFT JOIN	#__countries as count
					ON	count.countries_id = pl.country
			 LEFT JOIN	#__property_price as pp
					ON	pl.listing_id = pp.pocket_id
			 LEFT JOIN	#__property_type pt
					ON	pt.type_id = pl.property_type
			 LEFT JOIN	#__property_sub_type pst
					ON	pst.sub_id = pl.sub_type
			 LEFT JOIN	(
						SELECT image, listing_id
						  FROM #__pocket_images
					  GROUP BY listing_id
						) as pi
					ON	( pl.listing_id = pi.listing_id )
			 LEFT JOIN	(
						SELECT count(distinct(user_id)) as views, property_id
						  FROM #__views
					  GROUP BY property_id
						) as pv
					ON	( pv.property_id = pl.listing_id )
		 	LEFT JOIN	(
						SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed
						  FROM #__request_access racs
						 WHERE permission = 1
					  GROUP BY racs.property_id
						) as viewers
					ON	viewers.id = pp.pocket_id
			LEFT JOIN	(
						SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as request
						  FROM #__request_access racs
						 WHERE permission = 0
					  GROUP BY racs.property_id
						) as request
					ON	request.id = pp.pocket_id
				 WHERE	pl.user_id = '.$uid.$closed.'
				 
				 ORDER BY pl.date_created DESC');
		$db->setQuery($query);
		$listings=$db->loadObjectList();	
		$ex_rates = $modelPropertyListing->getExchangeRates_indi();
        
        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;
		foreach($listings as $key => $listing){
			$userDetails = $this->get_user_registration(JFactory::getUser()->id);
			$listing->viewers = $this->get_viewers($listing->listing_id, 5);
			$property_type = strtolower(str_replace(" ", "-", $listing->property_type));
			$sub_type = strtolower($listing->sub_type);
			switch($sub_type) {
				case "motel/hotel":
					$sub_type = "motel";
					break;
				case "townhouse/row house":
					$sub_type = "townhouse";
					break;
				case "assisted care":
					$sub_type = "assist";
					break;
				case "special purpose":
					$sub_type = "special";
					break;
				case "multi family":
					$sub_type = "multi-family";
					break;
			}
			if(JFactory::getUser()->id!=$listing->user_id){
				if($listing->currency!=$userDetails->currency){
					//$listing->price1=$modelPropertyListing->getExchangeRates($listing->price1,$userDetails->currency,$listing->currency);
					if($listing->currency!="USD"){									
						$listing->price1=$listing->price1 / $ex_rates_CAD;
					} else {
						$listing->price1=$listing->price1 * $ex_rates_CAD;
					}
					if($listing->price2){
						//$listing->price2=$modelPropertyListing->getExchangeRates($listing->price2,$userDetails->currency,$listing->currency);
						if($listing->currency!="USD"){									
							$listing->price2=$listing->price2 / $ex_rates_CAD;
						} else {
							$listing->price2=$listing->price2 * $ex_rates_CAD;
						}
					}
					$listing->currency=$userDetails->currency;
				}
			}
			$img_filename = $property_type . "-" . $sub_type . ".jpg";
			$img_filename_bw = $property_type . "-" . $sub_type . "-bw.jpg";
			$listings[$key]->default_image = "images/buyers/100x81/" . $img_filename;
			$listings[$key]->default_image_bw = "images/buyers/100x81/" . $img_filename_bw;
		}
		
		return $listings;
	
	}
	
	public function get_public_listings($uid){
		$db = JFactory::getDbo();
		//$closed = ($status==3) ? '' : ' AND pl.closed = '.$status;
		$query = $db->getQuery(true);
		$query->setQuery('
				SELECT	ps.listing_id
				  FROM	#__permission_setting as ps
				  
			 LEFT JOIN	#__pocket_listing pl
				
					ON	pl.listing_id = ps.listing_id
					
				WHERE ps.selected_permission=1 AND pl.user_id='.$uid.' AND pl.closed=0
			');
		$db->setQuery($query);
		$listings=$db->loadObjectList();	
		return $listings;
	
	}
	
	public function get_viewers($id, $limit=0){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$concat = ($limit) ? 'limit '.$limit : '';
		$query->select('distinct(user_id) as user')->from('#__views')->where('property_id='.$id.' '.$concat);
		$users = array();
		$userlist = $db->setQuery($query)->loadObjectList();
		foreach($userlist as $user){
			$users[] = $this->get_user_registration($user->user);
		}
		return $users;
	}
	public function get_pocket_zips($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('distinct(zip) as zip, count(zip) as count');
		$query->from('#__pocket_listing');
		$query->group('zip');
		$query->where('user_id = '.$id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_buyer_zips($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('distinct(zip) as zip, count(zip) as count');
		$query->group('zip');
		$query->from('#__buyer_needs bn');
		$query->leftJoin('#__buyer b ON b.buyer_id = bn.buyer_id');
		$query->where('agent_id = '.$id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_user_spoken_language($id, $own=false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('uls.*, ul.language as language'));
		$query->from('#__user_lang_spoken uls');
		$query->leftJoin('#__user_languages ul ON ul.lang_id = uls.lang_id');
		$query->where('user_id = '.$id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function redirect(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_registration');
		$query->where('email = \''.JFactory::getUser()->email.'\'');
		$db->setQuery($query);
		$info = $db->loadObject();
		return !empty($info->about);
	}
	public function get_mobile_numbers($id, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_mobile_numbers');
		if(!$own)
			$query->where('`show` = 1');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE email = \''.JFactory::getUser($id)->email.'\')');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	public function get_invitation($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users u');
		
		$query->where('u.id ='.$id);
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');
		
		$query->leftJoin('#__invitation_tracker it ON it.agent_id = ur.user_id');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	public function get_invitee($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_registration ur');
		
		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		
		$query->leftJoin('#__zones z ON z.zone_id = ur.state');
		
		$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
		$query->where('ur.email = \''.$email.'\'');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_fax_numbers($id, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_fax_numbers');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE email = \''.JFactory::getUser($id)->email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_work_numbers($id, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_work_numbers');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE email = \''.JFactory::getUser($id)->email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_websites($id, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_websites');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE email = \''.JFactory::getUser($id)->email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_socialsites($id, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_social_sites');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE email = \''.JFactory::getUser($id)->email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function get_user_details_c($select, $regid){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select($select);

		$query->from('#__user_registration ur');

		$query->where('user_id = '.$regid);

		$query->leftJoin('#__countries c ON c.countries_id = ur.country');

		$query->leftJoin('#__country_languages cl ON cl.country = ur.country');

		$query->leftJoin('#__broker br ON br.broker_id = ur.brokerage');

		$query->leftJoin('#__zones z ON z.zone_id = ur.state');

		$db->setQuery($query);

		return $db->loadObject();

	}
	
	public function get_user_registration($uid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('ur.*,us.*,u.*,conc.*');
		
		$query->from('#__users u');
		$query->where('u.id ='.$uid);
		
		$query->leftJoin('#__user_registration ur ON u.email = ur.email');
		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		$query->leftJoin('#__country_currency conc ON conc.country = ur.country');
		
		$db->setQuery($query);
		$db->loadObject()->firstname = str_replace("\\", "", $db->loadObject()->firstname);
		$db->loadObject()->lastname = str_replace("\\", "", $db->loadObject()->lastname);
		return $db->loadObject();
	}
	public function get_countries(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('countries_id', 'countries_name', 'countries_iso_code_2', 'currency', 'symbol'));
		$query->from('#__countries');
		$query->leftJoin('#__country_currency u ON country = countries_id');
		$db->setQuery($query);
		$countries = $db->loadObjectList();
		return $countries;
	}
	public function get_user_designations($userid, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id','designations'));
		$query->from('#__designations');
		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');
		return $db->setQuery($query)->loadObjectList();
	}
	public function get_designation_name($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__designations');
		$query->where('id LIKE \''.$id.'\'');
		$db->setQuery($query);
		$designations = $db->loadObjectList();
		return $designations[0]->designations;
	}
	public function get_user_languages($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_languages');
		$query->where('lang_id LIKE \''.$id.'\'');
		$db->setQuery($query);
		$language = $db->loadObjectList();
		return $language[0]->language;
	}
	public function get_languages(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_languages');
		$query->order('lang_id ASC');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_zone_with_iso_code($state){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__zones');
		if(!is_numeric($state)):
		$query->where('zone_name LIKE \''.$state.'\'');
		else:
		$query->where('zone_id LIKE \''.$state.'\'');
		endif;
		$db->setQuery($query);
		$zone = $db->loadObjectList();
		return $zone[0];
	}
	public function get_property_image($lID){
		$db		= $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__pocket_images');
		$query->where('listing_id LIKE \''. $lID .'\'');
		$db->setQuery($query);
		$images = $db->loadObjectList();
		return $images;
	}
	public function getViews($lid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(distinct(user_id)) as views');
		$query->from('#__views');
		$query->where('property_id='.$lid);
		$query->order('property_id desc');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function getListingOwner($lid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('user_id');
		$query->from('#__pocket_listing');
		$query->where('listing_id='.$lid);
		$db->setQuery($query);
		return $db->loadObject()->user_id;
	}
	public function getCountryIso($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('countries_iso_code_2');
		$query->from('#__countries');
		$query->where('countries_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->countries_iso_code_2;
	}
	public function getCountry($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('countries_name');
		$query->from('#__countries');
		$query->where('countries_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->countries_name;
	}
	public function getState($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where('zone_id='.$id);
		$db->setQuery($query);
		return $db->loadObject()->zone_name;
	}
	public function get_user($userid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
		->select('STRAIGHT_JOIN *')
		->from('#__users u 
			STRAIGHT_JOIN #__user_registration ur ON ur.email = u.email 
			STRAIGHT_JOIN #__user_sales us ON us.agent_id = ur.user_id ');
	//	$query->select('*');
	//	$query->from('#__users u');
	//	$query->leftJoin('#__user_registration ur ON ur.email = u.email');
	//	$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		$query->where('u.id = '.$userid);
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	public function get_payment_methods(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__contact_method');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	public function get_contact_method_label($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__contact_method');
		$query->where('pk_id = '.$id);
		$db->setQuery($query);
		return $db->loadObject()->method;
	}
	public function get_contact_options($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_contact_method');
		$query->where('user_id = '.$id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function search_profile_info($userid, $own = false, $search){
		$enhance_search = 
			array_filter(
				explode(	
					" ", 
					$search
				)
			);
			
			
		$temptable1 = "";
		
		foreach($enhance_search as $result){
			$temptable1 .= "
				OR ur.firstname like '%".$result."%' OR
				ur.lastname like '%".$result."%' OR
				ur.zip like '%".$result."%'
			";
		}
		
		$temptable = "";
		
		foreach($enhance_search as $result){
			$temptable .= "
				OR temptable.firstname like '%".$result."%' OR
				temptable.lastname like '%".$result."%' OR
				temptable.zip like '%".$result."%' 
			";
		}
		
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("u.*, b.broker_name, b.zone_id, b.broker_id, ur.*,temptable.*");
		$query->from('#__users u');
		$query->where('id = '.$userid);
		$query->leftJoin('#__user_registration ur ON u.email = ur.email');
		$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
		
		$query->leftJoin('(
			SELECT 
				temptable1.firstname,
				temptable1.lastname,
				temptable1.zip,
				temptable1.newid,
				temptable1.user_id, 
				group_concat(temptable1.value) as newcontact
			FROM 
				(
					SELECT 
						ur.firstname as firstname,
						ur.lastname as lastname,
						ur.zip as zip,
						us.id as newid,
						ss.user_id, 
						ss.value 
					FROM 
						#__user_work_numbers ss
					LEFT JOIN 
						#__user_registration ur
					ON 
						ss.user_id = ur.user_id
					LEFT JOIN
						#__users us 
					ON 
						us.email = ur.email
					WHERE
						us.id = '.$userid.' 
					AND
					
					(
						ur.firstname like "%'.$search.'%"
						'.$temptable1.'
					)
				) temptable1 
			) temptable 
			ON 
			(
				temptable.firstname like "%'.$search.'%" 
				'.$temptable.'
			)
			AND 
				temptable.newid = '.$userid
		);
		
		// echo $query;
		// die();
		$db->setQuery($query);
		$ret = $db->loadObject();
		return $ret;
	}
	public function get_profile_info($userid, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("u.*, 
			b.broker_name, 
			z.zone_code, 
			con.*, 
			b.zone_id, 
			b.broker_id, 
			ur.tax_id_num,
			us.*, ur.*,temptable.*");
		$query->from('#__users u');
		$query->where('id = '.$userid);
		$query->leftJoin('#__user_registration ur ON u.email = ur.email');
		$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
		$query->leftJoin('#__zones z ON ur.state = z.zone_id');
		$query->leftJoin('#__countries con ON con.countries_id = z.zone_country_id');
		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		
		//$query->leftJoin('(select temptable1.user_id, group_concat(temptable1.value) from (select user_id, value from #__user_work_numbers where user_id = '.$userid.' limit 3) temptable1 ) temptable ON temptable.user_id = '.$userid);
		$query->leftJoin('(
			SELECT 
				temptable1.newid,
				temptable1.user_id, 
				group_concat(temptable1.value)  as newcontact
			FROM 
				(
					SELECT 
						us.id as newid,
						ss.user_id, 
						ss.value 
					FROM 
						#__user_work_numbers ss
					LEFT JOIN 
						#__user_registration ur
					ON 
						ss.user_id = ur.user_id
					LEFT JOIN
						#__users us 
					ON 
						us.email = ur.email
					WHERE
						us.id = '.$userid.' 
					LIMIT 3
				) temptable1 
			) temptable 
			ON 
				temptable.newid = '.$userid
		);
	//	echo $query; die();
		$db->setQuery($query);
		$stripped_name= stripslashes($db->loadObject()->name);
		$stripped_firstname = stripslashes($db->loadObject()->firstname);
		$stripped_lastname = stripslashes($db->loadObject()->lastname);
		//$db->loadObject()->lastname = $stripped_lastname ;
		$ret = $db->loadObject();
		//$ret->name = $stripped_name;
       //$ret->firstname = $stripped_firstname;
		//$ret->lastname = $stripped_lastname;
		return $ret;
	}
	public function get_designations($countryID){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id as value', 'designations as label'));
		$query->from('#__designations');
		$query->where('country_available LIKE \''.$countryID.'\'');
		$db->setQuery($query);
		return  $db->loadObjectList();
	}
	public function get_notiftypes($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("*");
		$query->from('#__user_contact_method');
		$query->where('user_id='.$id);
		return $db->setQuery($query)->loadObject();
	}
	public function saveNotification( $user_id, $values ){
		var_dump($values);
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_contact_method')->where('user_id = \''.JFactory::getUser()->id.'\'');	
		
		//echo count($db->setQuery($query)->loadObject());
		
		if( count($db->setQuery($query)->loadObject()) == 0){
			$contact_options = new JObject();
			$contact_options->user_id = $user_id;
			$contact_options->contact_options = $values;
			$db->insertObject('#__user_contact_method', $contact_options);
		} else {
			$contact_options = new JObject();
			$contact_options->user_id 			= $user_id;
			$contact_options->contact_options 	= $values;
			$db->updateObject('#__user_contact_method', $contact_options, 'user_id');
			$db->execute();
		}
		die();
	}
	
	public function retrieveNotifcations(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('contact_options')->from('#__user_contact_method')->where('user_id = \''.JFactory::getUser()->id.'\'');	
		
		if(count($db->setQuery($query)->loadObject()->contact_options)==0){
			$contact_options = new JObject();
			$contact_options->user_id = JFactory::getUser()->id;
			$contact_options->contact_options = '["1","2","3","4","5","6","7","8","9"]';
			$db->insertObject('#__user_contact_method', $contact_options);
		}
		
		$query2 = $db->getQuery(true);
		$query2->select('contact_options')->from('#__user_contact_method')->where('user_id = \''.JFactory::getUser()->id.'\'');		
		
		return $db->setQuery($query2)->loadObject()->contact_options;
	}
	public function insertIfnotExists($object, $primary, $table){
		//QUERY
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("count(*) as count");
		$query->from($table);
		$query->where($primary.' = '.$object->$primary);
		$result = $db->setQuery($query)->loadObject();
		if($result->count){
			//EDIT
			$result = JFactory::getDbo()->updateObject($table, $object, $primary);
		}
		else{
			$result = JFactory::getDbo()->insertObject($table, $object, $primary);
		}
	}
	public function updatePropertyExpiry($lid, $newval){
		$edit2 = new JObject();
		$edit2->listing_id = $lid;
		$edit2->date_expired = $newval;
		$result2 = JFactory::getDbo()->updateObject('#__pocket_listing', $edit2, 'listing_id');
	}
	
	public function updateImage($user_object){
		$db = JFactory::getDbo();
		return $db->updateObject('#__user_registration', $user_object, 'email');
	}
	
	public function getImage(){
			
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$session =& JFactory::getSession();
			$query->setQuery("
			
				SELECT 
					*
				FROM
					#__user_registration a
				LEFT JOIN 
					tbl_invitation_tracker b 
				ON 
					a.user_id=b.agent_id
				WHERE 
					b.invited_email = '".JFactory::getUser()->email."';
			");
			
			$db->setQuery($query);
			$image 	= $db->loadObject()->image;
			$session->set( 'inviters_image', $image );	
	}
	
	public function update_about($email, $value){
		$db = JFactory::getDbo();
		$about_object = new JObject();
		$about_object->email = $email;
		$about_object->about = $value;
		$db->updateObject('#__user_registration', $about_object, 'email');
	}
	
	public function update_tax_id_num($user_id, $value){
		$db = JFactory::getDbo();
		$about_object = new JObject();
		$about_object->user_id = $user_id;
		$about_object->tax_id_num = $value;
		$db->updateObject('#__user_registration', $about_object, 'user_id');
	}
	public function update_designations($id, $designations_arr){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_designations')->where('user_id = \''.JFactory::getUser()->id.'\'');
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->desig_id; }, $db->loadObjectList());
		$query = $db->getQuery(true);
		$query->delete('#__user_designations');
		$query->where('user_id = '.$id);
		$db->setQuery($query);
		$db->execute();
		foreach($designations_arr as $desig){
		
			$desig_object = new JObject();
			$desig_object->desig_id = $desig;
			$desig_object->user_id = $id;
			$db->insertObject('#__user_designations', $desig_object);
		}
		return $this->set_message($orig_info, $designations_arr, 'designations');
	}
	
	public function delete_designations($id, $designations_arr){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->delete('#__user_designations')->where('user_id = \''.JFactory::getUser()->id.'\'');
		$db->setQuery($query);
		$orig_info = "deleted";
		$db->execute();
		if(empty($designations_arr)){
			return "";
		} else {
			return "You have deleted a designation.";
		}
		//return $this->set_message($orig_info, $designations_arr, 'designations');
	}
	
	public function update_single_designation( $designation_id ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__user_designations')->where("user_id = ".JFactory::getUser()->id." AND desig_id=".$designation_id );
		$db->setQuery($query);
		if( $db->loadObject()->count==0 ){
			$desig_object = new JObject();
			$desig_object->desig_id = $designation_id;
			$desig_object->user_id = JFactory::getUser()->id;
			$db->insertObject('#__user_designations', $desig_object);
		}
		
		
	}
	
	public function delete_single_designation( $designation_id ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__user_designations')->where("user_id = ".JFactory::getUser()->id." AND desig_id=".$designation_id );
		$db->setQuery($query);
		$db->execute();
		if( $db->loadObject()->count==1 ){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__user_designations');
			$query->where("desig_id = ".$designation_id." AND user_id = ".JFactory::getUser()->id);
			$db->setQuery($query);
			$db->execute();
		}
	}
	public function update_languages($id, $language_arr){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_lang_spoken')->where('user_id = \''.$id.'\'');
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->lang_id; }, $db->loadObjectList());
		$query = $db->getQuery(true);
		$query->delete('#__user_lang_spoken');
		$query->where('user_id = '.$id);
		$db->setQuery($query);
		$db->execute();
		foreach($language_arr as $language){
			$lang_object = new JObject();
			$lang_object->lang_id = $language;
			$lang_object->user_id = $id;
			$db->insertObject('#__user_lang_spoken', $lang_object);
		}
		return $this->set_message($orig_info, $language_arr, 'languages');
	}
	public function updatebrokerage($userid, $val){
		$db = JFactory::getDbo();
		$registration_object = new JObject();
		$registration_object->brokerage = $val;
		$registration_object->email = JFactory::getUser($userid)->email;
		return $db->updateObject('#__user_registration', $registration_object, 'email');
	}
	
	
	public function updatebrokerage_info($userid, $country_id, $val){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$registration_object = new JObject();
		$registration_object->brokerage = $val;
		$registration_object->email = JFactory::getUser($userid)->email;
		$db->updateObject('#__user_registration', $registration_object, 'email');
		$query->select('countries_name')->from('#__countries')->where("countries_id = ".$country_id );
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	public function update($data, $contact = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		//load user reg info
		$query->select('*');
		$query->from('#__user_registration ur');
		$query->where('ur.email = \''.JFactory::getUser()->email.'\'');
		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		$db->setQuery($query);
		$userinfo = $db->loadObject();
	
		$message = "";
		if(!$contact){
			$userData = $this->get_profile_info(JFactory::getUser()->id, true);
			$usrname_object = new JObject();
			$namearr = array($db->escape( $data['firstname']),$db->escape( $data['lastname']));
			$name = implode(" ",$namearr);
			$usrname_object->name = $name;
			$usrname_object->email = JFactory::getUser()->email;
			$db->updateObject('#__users', $usrname_object, 'email');
			
			$usrsales_object = new JObject();
			$usrsales_object->agent_id = $data['uid'];
			$volume_2012 = str_replace(",", "", $data['volume_2012']);
			$volume_2013 = str_replace(",", "", $data['volume_2013']);
			
			$volume_2014 = str_replace(",", "", $data['volume_2014']);
			$usrsales_object->volume_2012 = (int) str_replace("$", "", $volume_2012);
			$usrsales_object->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);
			$usrsales_object->ave_price_2012 = $usrsales_object->volume_2012/$usrsales_object->sides_2012;
			$usrsales_object->volume_2013 = (int) str_replace("$", "", $volume_2013);
			$usrsales_object->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);
			$usrsales_object->ave_price_2013 = $usrsales_object->volume_2013/$usrsales_object->sides_2013;
			
			$usrsales_object->volume_2014 = (int) str_replace("$", "", $volume_2014);
			$usrsales_object->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
			$usrsales_object->ave_price_2014 = $usrsales_object->volume_2014/$usrsales_object->sides_2014;
			$db->updateObject('#__user_sales', $usrsales_object, 'agent_id');
			
			$registration_object = new JObject();
			$registration_object->email = JFactory::getUser()->email;
			$registration_object->firstname = $db->escape( $data['firstname'] );
			$registration_object->lastname = $db->escape( $data['lastname'] );
			$registration_object->street_address = $data['street'];
			$registration_object->suburb = $data['suburb'];
			$registration_object->licence = $this->encrypt($data['licence']);
			$registration_object->city = $data['city'];
			$registration_object->zip = $data['zip'];
			$registration_object->state = $data['state'];
			$registration_object->country = $data['country'];
			$registration_object->brokerage = $data['brokerage'];
			$registration_object->brokerage_license = $this->encrypt($data['brokerage_license']);
			$registration_object->image = trim($data['imagePath']);
			$registration_object->about = $data['about'];
			$db->updateObject('#__user_registration', $registration_object, 'email');
			
			$user = JFactory::getUser();
			$session = JFactory::getSession();
			$session->set('user', new JUser($user->id));
			//$userData = $this->get_profile_info(JFactory::getUser()->id, true);
			$changes=array();
			$old_vals=array();
			$year="";
			$sides=0;
			$volumes=0;
			foreach($registration_object as $key => $value){
				if(trim($registration_object->$key) != trim($userinfo->$key)){
					if($key=="brokerage"){
						$value = $this->get_broker($value);
						
						$message .= "You have updated your Brokerage to ".stripslashes($value).'<br/>';
						
						continue;
					}
					
					else if($key=="country"){
					
						$value = $this->getCountry($value);
					
						$message .= "You have updated your country to ".stripslashes($value).'<br/>';
						continue;
					}
					
					else if($key=="firstname"){
						$message .= "You have updated your First Name to ".stripslashes($value).'<br/>';
						continue;
					}
					
					else if($key=="lastname"){
						$message .= "You have updated your Last Name to ".stripslashes($value).'<br/>';
						continue;
					}
					
					else if($key=="image"){
						$message .= "You have updated your ".$key.'<br/>';
						continue; 
					}
					
					else if($key=="about"){
						$message .= "You have changed your About Me description<br/>";
						continue; 
					}
					else if($key=="brokerage_license"){
						$message .= "You have updated your brokerage license to ".$this->decrypt($value).'<br/>';
						
						continue; 
						
					}
					
					else if($key=="licence"){
						$message .= "You have updated your ".$key." to ".$this->decrypt($value).'<br/>';
						
						continue; 
						
					} else if($key=="state"){
						$message .= "You have updated your ".$key." to ".$this->get_state_name($value).'<br/>';
					} else {
						$message .= "You have updated your ".$key." to ".$value.'<br/>';
					}
				}
			};
			
			foreach($usrsales_object as $key => $value){
				if(trim($usrsales_object->$key) != trim($userinfo->$key)){
					if($key=="volume_2012"){
						$volumes=1;
						$year="2012";
						$changes[$year."_vol"]=$value;
					    $message .= "You have updated your 2012 Total Volume to "."$".number_format($value).'<br/>';
						continue;
					}
					
					else if($key=="sides_2012"){
						$sides=1;
						$year="2012";
						$changes[$year."_sides"]=$value;
						$message .= "You have updated your 2012 Total Sides to ".number_format($value).'<br/>';
						continue;
					}
					
					else if($key=="volume_2013"){
						$volumes=1;
						$year="2013";
						$changes[$year."_vol"]=$value;
						$message .= "You have updated your 2013 Total Volume to "."$".number_format($value).'<br/>';
						continue;
					}
					else if($key=="sides_2013"){
						$sides=1;
						$year="2013";
						$changes[$year."_sides"]=$value;
						$message .= "You have updated your 2013 Total Sides to ".number_format($value).'<br/>';
						continue;
					}
					
					else if($key=="sides_2014"){
						$sides=1;
						$year="2014";
						$changes[$year."_sides"]=$value;
						$message .= "You have updated your 2014 Total Sides to ".number_format($value).'<br/>';
						continue;
					}
					
					else if($key=="volume_2014"){
						$volumes=1;
						$year="2014";
						$changes[$year."_vol"]=$value;
						$message .= "You have updated your 2014 Total Volume to "."$".number_format($value).'<br/>';
						continue;
					}
					
					else if($key=="ave_price_2012"){
						$message .= "";
						continue;
					}
					
					else if($key=="ave_price_2013"){
						$message .= "";
						continue;
						
					} 
					
					else if($key=="ave_price_2014"){
						$message .= "";
						continue;
						
					}		
					
					else {
					}		
				}
			}
			
			
		}
		
		$regid = $this->get_regid(JFactory::getUser()->email);
		$add = ($contact) ? true: false;
		if(count($data['phone']))
			$message .= $this->update_work_number($data['phone'], $regid, $add);
		if(count($data['cell']))
			$message .= $this->update_mobile_number($data['cell'], $regid, $add);
		if(count($data['fax']))
			$message .= $this->update_fax_number($data['fax'], $regid, $add);
		if(count($data['languages']))
			$message .= $this->update_languages(JFactory::getUser()->id, $data['languages']);
		if(count($data['designations'])){
			$message .= $this->update_designations(JFactory::getUser()->id, $data['designations']);
		} else {
			$message .= $this->delete_designations(JFactory::getUser()->id, $data['designations']);
			//$message .= "You have deleted a designation.";
		}
		if(count($data['site']))
			$message .= $this->update_site($data['site'], $regid, $add);
		if(count($data['social']))
			$message .= $this->update_social($data['social'], $regid, $add);
		if($message!=""){
			$user = JFactory::getUser();

			$result = $this->get_user_details_bc(array('is_premium', 'user_type', 'firstname', 'lastname', 'email','lang','ur.city','ur.zip','countries_name','zone_code','broker_name','countries_iso_code_2','countries_iso_code_3'), $regid);


			$user_details = array(
				'email' => $result->email, 
				'fname' => $result->firstname,
				'lname' => $result->lastname,
				'broker' => $result->broker_name,
				'state' => $result->zone_code,
				'country' => $result->countries_iso_code_3,
				'zip' => $result->zip,
				'city' => $result->city,
			);

			$this->addUserToChimp($user_details,"update");


			$insert = new JObject();
			$insert->message = $message;
			$ret = $db->insertObject('#__edit_activity', $insert);
			$insert_activity = new JObject();
			$insert_activity->activity_type = (int)3;
			$insert_activity->user_id = (int)$user->id;
			$insert_activity->activity_id =  $db->getConnection()->insert_id;
			$insert_activity->date = date('Y-m-d H:i:s',time());
			$insert_activity->other_user_id = (int)$user->id;
			
			if($volume || $sides){
			
				$this->sendVolumeChanges($userData,$year,$changes,$volumes,$sides);
			
			} 
			$ret_activity = $db->insertObject('#__activities', $insert_activity);
		}





	}
	
	function sendVolumeChanges($userData,$year,$changes,$volumes,$sides) {

		$send=0;
		$volume_exist=0;
		$sides_exist=0;
		
		$subject="New Sales Numbers to Verify - ".$userData->firstname." ".$userData->lastname;
		
		$userprofile = urlencode(base64_encode("index.php?option=com_userprofile&task=profile&uid=".$userData->user_id));
		
		
		$root_url=JURI::base();
		
		$body = "";
		
		$body .= "<table style='width:100%; padding-bottom:20px; padding-top:20px; font-family: Arial, Helvetica, sans-serif; font-size:14px;'>
				
				<tr>
					<td>
					Hello Admin, <br/><br/>
					Member firstname: ".$userData->firstname." <br/>
					Member lastname: ".$userData->lastname." <br/>
					Email Address: ".$userData->email." <br/>
					Brokerage: ".$userData->broker_name." <br/><br/>
					" ;
		
		$old_vol=array();
		$old_sides=array();
		$new_vol=array();
		$new_sides=array();


		foreach ($changes as $key => $value) {
			if($volumes && $userData->volume_2012!=$value){
				if($key=="2012_vol"){
					$old_vol["2012"]="2012 Total Volume : $".number_format($userData->volume_2012,0, '.', ',');
					$new_vol["2012"]="2012 Total Volume : $".number_format($value,0, '.', ',');
				}
				if($key=="2013_vol"){
					$old_vol["2013"]="2013 Total Volume : $".number_format($userData->volume_2013,0, '.', ',');
					$new_vol["2013"]="2013 Total Volume : $".number_format($value,0, '.', ',');
				}
				if($key=="2014_vol"){
					$old_vol["2014"]="2014 Total Volume : $".number_format($userData->volume_2014,0, '.', ',');
					$new_vol["2014"]="2014 Total Volume : $".number_format($value,0, '.', ',');
				}
				$volume_exist=1;
				$send=1;
			}
			if($sides && $userData->volume_2012!=$value){
				if($key=="2012_sides"){
					$old_sides["2012"]="2012 Total Sides : ".number_format($userData->sides_2012,0, '.', ',');
					$new_sides["2012"]="2012 Total Sides : ".number_format($value,0, '.', ',');
				}
				if($key=="2013_sides"){
					$old_sides["2013"]="2013 Total Sides : ".number_format($userData->sides_2013,0, '.', ',');
					$new_sides["2013"]="2013 Total Sides : ".number_format($value,0, '.', ',');
				}
				if($key=="2014_sides"){
					$old_sides["2014"]="2014 Total Sides : ".number_format($userData->sides_2014,0, '.', ',');
					$new_sides["2014"]="2014 Total Sides : ".number_format($value,0, '.', ',');
				}
				$sides_exist=1;
				$send=1;
			}
		}
		$body .="<b>Old Sales Numbers</b><br>";
		if($volume_exist){
			foreach ($old_vol as $key => $value) {
				$body.=$value."<br/>";
			}
		}
		if($sides_exist){
			foreach ($old_sides as $key => $value) {
				$body.=$value."<br/>";
			}
		}
			
		$body .="<b>New Sales Numbers</b><br>";
		if($volume_exist){
			foreach ($new_vol as $key => $value) {
				$body.=$value."<br/>";
			}
		}
		if($sides_exist){
			foreach ($new_sides as $key => $value) {
				$body.=$value."<br/>";
			}
		}

		$body .=" </td></tr></table>";
		
		$body .= "<br />";
		
    	$body .= "To verify the numbers log in to <a href=\"".JUri::root()."administrator/index.php?option=com_useractivation\">".JUri::root()."administrator/index.php?option=com_useractivation</a>";
		
		$sender = array(  'no-reply@agentbridge.com' , 'AgentBridge');
		$mailSender =& JFactory::getMailer();
		
		$recipient = "support@agentbridge.com";
		
		$cc = "rebekah.roque@keydiscoveryinc.com";
		
		$bcc = "2beta@yopmail.com";
		
		$mailSender ->addRecipient( $recipient );
		
		$mailSender ->addCc( $cc );
		
		$mailSender ->addBcc( $bcc );
		
		$mailSender ->setSender( $sender );
		$mailSender ->setSubject( $subject );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody(  $body );
		$mailSender ->Encoding = 'base64';
		$mailSender ->wordWrap = 50;
		//var_dump($body);	
		
		if($send){
			if ($mailSender->Send()) {
				echo json_encode("success");
			} else {
				echo json_encode("fail");
			}
		}
		
	
	}
	
	public function get_regid($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('user_id');
		$query->from('#__user_registration');
		$query->where("email = '".$email."'");
		$db->setQuery($query);
		return $db->loadObject()->user_id;
	}
	public function get_state_name($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where("zone_id = '".$id."'");
		$db->setQuery($query);
		return $db->loadObject()->zone_name;
	}
	public function update_work_number($dataList, $regid, $add = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_work_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_work_numbers')->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_work_numbers', $number_object);
			}
		}
		return $this->set_message($orig_info, $dataList, 'work numbers', $add);
	}
	public function update_mobile_number($dataList, $regid, $add = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_mobile_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_mobile_numbers');
			$query->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_mobile_numbers', $number_object);
			}
		}
		return $this->set_message($orig_info, $dataList, 'mobile numbers', $add);
	}
	public function update_fax_number($dataList, $regid, $add = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_fax_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_fax_numbers');
			$query->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_fax_numbers', $number_object);
			}
		}
		return $this->set_message($orig_info, $dataList, 'fax numbers', $add);
	}
	public function update_social($dataList, $regid, $add = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_social_sites')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_social_sites')->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_social_sites', $number_object);
			}
		}
		return $this->set_message($orig_info, $dataList, 'social sites', $add);
	}
	public function update_site($dataList, $regid, $add = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_websites')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_websites')->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_websites', $number_object);
			}
		}
		return $this->set_message($orig_info, $dataList, 'websites', $add);
	}
	public function set_message($original, $new, $label, $add=false){
		$diff_removed = array_diff($original, $new);
		$diff_added = array_diff($new, $original);
		$removed = array();
		$added = array();
		$message="";
		if(strtolower($label == "languages")){
			foreach($diff_removed as $lang){
				$removed[] = $this->get_language($lang);
			}
			foreach($diff_added as $lang){
				$added[] = $this->get_language($lang);
			}
		}
		else if($label=="designations"){
			foreach($diff_removed as $lang){
				$removed[] = $this->get_designation_name($lang);
			}
			foreach($diff_added as $lang){
				$added[] = $this->get_designation_name($lang);
			}
		}
		else{
			$removed = $diff_removed;
			$added = $diff_added;
		}
		if((count($diff_added)>0 && implode(",",$added)!="" && implode(",",$added)!=" ")|| count($diff_removed)>0)
			$message = "You have updated your ".$label."";
		if(count($diff_added)>0 && implode(",",$added)!="" && implode(",",$added)!=" "){
			$message .= ", you have added ".implode(",",$added)."";
		}
		if(count($diff_removed)>0 & !$add){
			if(count($diff_added)>0) $message.=" and removed ";
			else $message.=" you have removed ";
			$message .= "".implode(",",$removed)."";
		}
		if($message!="")
			return $message."<br/>";
	}
	function get_language($lid){
		$db = JFactory::getDbo();
		$query= $db->getQuery(true);
		$query->select('language')
		->from('#__user_languages')
		->where('lang_id = '.$lid);
		$db->setQuery($query);
		return $db->loadObject()->language;
	}
	// request to join with property name included
	function request_network_pid($other_user_id, $pid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$own = JFactory::getUser()->id;
		$activity = new JObject();
		$activity->user_id = $other_user_id;
		$activity->other_user_id = JFactory::getUser()->id;
		$activity->status = 0;
		$query->select('count(1) as count')->from('#__request_network')->where("user_id = ".$other_user_id." AND other_user_id = ".JFactory::getUser()->id);
		$result = $db->setQuery($query)->loadObject();
		if($result->count==0)
			$ret_activity = $db->insertObject('#__request_network', $activity);
	
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)8;
		$insert_activity->user_id = (int)$other_user_id;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = (int)JFactory::getUser()->id;
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity);
			
		$insert_activity_other = new JObject();
		$insert_activity_other->activity_type = (int)30;
		$insert_activity_other->user_id = (int)JFactory::getUser()->id;
		$insert_activity_other->activity_id =  $insert_activity->activity_id;
		$insert_activity_other->date = date('Y-m-d H:i:s',time());
		$insert_activity_other->other_user_id = (int)$other_user_id;
		if($result->count==0)
			$ret_activity_other = $db->insertObject('#__activities', $insert_activity_other);
		$this->sendrequest_mail($other_user_id, JFactory::getUser()->id, $pid);
	}	
	
	function request_network_pid_WS($userId,$other_user_id, $pid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$own = $userId;
		$activity = new JObject();
		$activity->user_id = $other_user_id;
		$activity->other_user_id = $userId;
		$activity->status = 0;
		$query->select('count(1) as count')->from('#__request_network')->where("user_id = ".$other_user_id." AND other_user_id = ".$userId);
		$result = $db->setQuery($query)->loadObject();
		if($result->count==0)
			$ret_activity = $db->insertObject('#__request_network', $activity);
	
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)8;
		$insert_activity->user_id = (int)$other_user_id;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = (int)$userId;
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity);
			
		$insert_activity_other = new JObject();
		$insert_activity_other->activity_type = (int)30;
		$insert_activity_other->user_id = (int)$userId;
		$insert_activity_other->activity_id =  $insert_activity->activity_id;
		$insert_activity_other->date = date('Y-m-d H:i:s',time());
		$insert_activity_other->other_user_id = (int)$other_user_id;
		if($result->count==0)
			$ret_activity_other = $db->insertObject('#__activities', $insert_activity_other);
		$this->sendrequest_mail($other_user_id, $userId, $pid);
	}
	function request_network($other_user_id)
	{
	
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$activity = new JObject();
		$own = JFactory::getUser()->id;
		$activity->user_id = $other_user_id;
		$activity->other_user_id = JFactory::getUser()->id;
		$activity->status = 0;
		
		$query->select('count(1) as count')->from('#__request_network')->where("user_id = ".$other_user_id." AND other_user_id = ".JFactory::getUser()->id);
		$result = $db->setQuery($query)->loadObject();
		
		if($result->count==0)
			$ret_activity = $db->insertObject('#__request_network', $activity);
		
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)8;
		$insert_activity->user_id = (int)$other_user_id;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = (int)JFactory::getUser()->id;
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity);
			
		$insert_activity_other = new JObject();
		$insert_activity_other->activity_type = (int)30;
		$insert_activity_other->user_id = (int)JFactory::getUser()->id;
		$insert_activity_other->activity_id =  $insert_activity->activity_id;
		$insert_activity_other->date = date('Y-m-d H:i:s',time());
		$insert_activity_other->other_user_id = (int)$other_user_id;
		
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity_other);
		$this->sendrequest_mail($other_user_id, JFactory::getUser()->id);
	}
	function request_network_WS($userId,$other_user_id)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$activity = new JObject();
		$own = $userId;
		$activity->user_id = $other_user_id;
		$activity->other_user_id = $userId;
		$activity->status = 0;
		
		$query->select('count(1) as count')->from('#__request_network')->where("user_id = ".$other_user_id." AND other_user_id = ".$userId);
		$result = $db->setQuery($query)->loadObject();
		
		if($result->count==0)
			$ret_activity = $db->insertObject('#__request_network', $activity);
		
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)8;
		$insert_activity->user_id = (int)$other_user_id;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = (int)$userId;
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity);
			
		$insert_activity_other = new JObject();
		$insert_activity_other->activity_type = (int)30;
		$insert_activity_other->user_id = (int)$userId;
		$insert_activity_other->activity_id =  $insert_activity->activity_id;
		$insert_activity_other->date = date('Y-m-d H:i:s',time());
		$insert_activity_other->other_user_id = (int)$other_user_id;
		
		if($result->count==0)
			$ret_activity = $db->insertObject('#__activities', $insert_activity_other);
		$this->sendrequest_mail($other_user_id, $userId);
	}
	
	function request_contact_network($other_user_id)
	{
		$db = JFactory::getDbo();
		$activity = new JObject();
		//person adding is the user_id which is JFactory::getUser()->id;
		$activity->user_id = JFactory::getUser()->id; 
		
		//person added is $other_user_id
		$activity->other_user_id = $other_user_id;
		//save it as pending = 0
		$activity->status = 0;
		$ret_activity = $db->insertObject('#__request_network', $activity);
		//save it to activity log 28 = New Network Request
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)28;
		//person added is $other_user_id
		$insert_activity->user_id = (int)$other_user_id;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		
		//person adding is the other_user_id if you are in activity logs which is JFactory::getUser()->id;
		$insert_activity->other_user_id = (int)JFactory::getUser()->id;
		$ret_activity = $db->insertObject('#__activities', $insert_activity);
		$this->sendrequest_mail($other_user_id, JFactory::getUser()->id);
	}
	function sendrequest_mail($mailto, $other)
	{
		// load the receiver
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($mailto)->email.'\'');
		$db->setQuery($query);
		$recipient = $db->loadObject();
		
		// load the transmitter
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($other)->email.'\'');
		$db->setQuery($query);
		$other = $db->loadObject();
		
		$other->lastname = stripslashes($other->lastname);
		
		$other->firstname = stripslashes($other->firstname);
		
		$recipient->firstname = stripslashes($recipient->firstname);
		
		$mailer = JFactory::getMailer();
		$mailsubject = $other->firstname." ".$other->lastname.' requested to join your network';
		$body = $recipient->firstname.", <br/><br/>";
		$body .= $other->firstname." ".$other->lastname." has requested to view your POPs&trade;.  <br><br>";
		$body .= "To grant access to ".$other->firstname." please sign into <a href='".JUri::root()."'>AgentBridge</a>. All requests to view your POPs&trade; are on your Activity Page. <br/><br/>";
		$body .= "Sincerely, <br/><br/>";
		$body .= "The AgentBridge Team";
		
		$mailer->IsHTML(true);
		echo $mailer->sendMail('no-reply@agentbridge.com', 'AgentBridge', $recipient->email, $mailsubject, $body);
	}
	
	function sendrequest_mail_pid($mailto, $other, $pid)
	{
		// load the receiver
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($mailto)->email.'\'');
		$db->setQuery($query);
		$recipient = $db->loadObject();
		
		// load the transmitter
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser($other)->email.'\'');
		$db->setQuery($query);
		$other = $db->loadObject();
		
		//load property name
		$query = $db->getQuery(true);
		$query->select('*')->from('#__pocket_listing')->where("listing_id = $pid");
		$db->setQuery($query);
		$pops = $db->loadObject();
		
		$other->lastname = stripslashes($other->lastname);
		
		$other->firstname = stripslashes($other->firstname);
		
		$recipient->firstname = stripslashes($recipient->firstname);
		
		$pops->property_name = stripslashes($pops->property_name);
		
		$mailer = JFactory::getMailer();
		$mailsubject = $other->firstname." ".$other->lastname.' requested to view one of your private POPs';
		$body = $recipient->firstname.", <br/><br/>";
		$body .= $other->firstname." ".$other->lastname." has requested to view your POPs&trade;,<strong> ".$pops->property_name."</strong>.<br /><br/>";
		$body .= "To grant access to ".$other->firstname." please sign into <a href='".JUri::root()."'>AgentBridge</a>. All requests to view your POPs&trade; are on your Activity Page. <br/><br/>";
		$body .= "Sincerely, <br/><br/>";
		$body .= "The AgentBridge Team";
		
		$mailer->IsHTML(true);
		echo $mailer->sendMail('no-reply@agentbridge.com', 'AgentBridge', $recipient->email, $mailsubject, $body);
	}	
	function sendrequestaccess($uid, $pid, $otherid)
	{
	
		$db = JFactory::getDbo();
		$insert = new JObject();
		$insert->user_a = $uid;
		$insert->user_b = $otherid;
		$insert->property_id = $pid;
		$insert->permission = 0;
		$ret = $db->insertObject('#__request_access', $insert);
		$db = JFactory::getDbo();
		$insert_activity = new JObject();
		$insert_activity->activity_type = (int)6;
		$insert_activity->user_id = $uid;
		$insert_activity->activity_id =  $db->getConnection()->insert_id;
		$insert_activity->date = date('Y-m-d H:i:s',time());
		$insert_activity->other_user_id = $otherid;
		return $db->insertObject('#__activities', $insert_activity);
	}
	function get_mail_options($userid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_contact_method')->where('user_id = '.$userid);
		$options = $db->setQuery($query)->loadObject();
		return $options;
	}
	
	function include_to_network($other_user_id){
		$db = JFactory::getDbo();
		$activity = new JObject();
		$activity->user_id = JFactory::getUser()->id;
		$activity->other_user_id = $other_user_id;
		$activity->status = 1;
		return  $db->insertObject('#__request_network', $activity);
	}
	function suggest($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration ur')->leftJoin('#__users u ON u.email = ur.email')->where('ur.email = \''.$email.'\'');
		$db->setQuery($query);
		$user = $db->loadObject();
		$query = $db->getQuery(true);
		$conditions = array();
		$brokerage = ($user->brokerage)? "r.brokerage = ".$user->brokerage." OR" : "";
		$query->setQuery("
				SELECT	r.*, b.broker_name, u.*, c.countries_name, z.zone_code
				  FROM	#__user_registration r
			 LEFT JOIN	#__broker b
					ON	b.broker_id = r.brokerage
			 LEFT JOIN	#__users u
					ON	u.email = r.email
			 LEFT JOIN	#__countries c
					ON  c.countries_id = r.country
			 LEFT JOIN	#__zones z
					ON  r.state = z.zone_id
				 WHERE	(
							(".$brokerage."
							r.zip = ".$user->zip."
							OR	r.city = '".$user->city."')
							AND r.is_term_accepted = 1
						)
				   AND	u.id
						NOT IN	(
									SELECT user_id
									  FROM #__request_network
									 WHERE other_user_id =".$user->id."
								)");
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function get_listing($filters, $sticky = "AND"){
		$conditions = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__pocket_listing p');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." = '".$value."'";
				else{
					$explode = explode('-', $value);
					if(count($explode)>1){
						$conditions[] = $key."1 > ".$explode[0]." AND ".$key."2 < ".$explode[1];
					}
					else
						$conditions[] = $key."1 > ".$value;
				}
			}
		}
		if(count($conditions)>0)
			$query->where($conditions, $sticky);
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$db->setQuery($query);
		return $db->loadObjectList();
		die();
	}
	
	//count the number invitations created by the agent
	public function count_invitations() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$email = $user->email;	
		$query->setQuery("
			SELECT count( b.user_id ) as counter FROM 
				tbl_invitation_tracker a 
			LEFT JOIN 
				tbl_user_registration b 
			ON 
				a.agent_id = b.user_id 
			WHERE 
				b.email = '".$email."'");
		$db->setQuery($query);
		return $db->loadObject();
		die();	
	}
	
	//detect existing accounts and its status 
/*
SELECT 
	a.*, 
	b.*,
	count( a.user_id ) as user_id_count 
FROM 
	tbl_user_registration a
LEFT JOIN 
	tbl_invitation_tracker b
ON b.invited_email = a.email
WHERE 
	a.email= 'kate.middletonx@yopmail.com'
	
;
*/	
	
	function revalidateEmail( $email ){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT 
				email
			FROM 
				#__user_registration"
		);
		$db->setQuery($query);
		$email_array=$db->loadColumn();
		$to_return=$email;
		foreach($email_array as $value){
			if(strtolower($value)==strtolower($email))
				$to_return=$value;
		}
			
		return $to_return;
		die();
	}
	public function check_existing_accounts( $email ){
		$valid_email=$this->revalidateEmail($email);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT 
				email,
				COUNT( user_id ) as user_id_count 
			FROM 
				#__user_registration
			WHERE  
				email = '".$valid_email."'"
		);
		$db->setQuery($query);
		return $db->loadObject();
		die();
	}
	
	public function get_existing_details( $email ){
		$valid_email=$this->revalidateEmail($email);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT 
				a.*,
				b.*,
				
				u.*
			FROM 
				tbl_user_registration a
			LEFT JOIN
				tbl_invitation_tracker b
			ON
				b.invited_email = a.email
			
			LEFT JOIN
				tbl_users u
			ON
				u.email = a.email
			WHERE  
				a.email = '".$valid_email."'"
		);
		$db->setQuery($query);
		return $db->loadObject();
		die();
	}
	
	
	public function get_user_type( $email ){
		$valid_email=$this->revalidateEmail($email);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT user_type FROM #__user_registration WHERE email = '".$valid_email."'");
		$db->setQuery($query);
		return $db->loadObject();
		die();
	}
	
	public function get_security( $id ){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT uid FROM #__answers WHERE uid = ".$id);
		$db->setQuery($query);
		return $db->loadObject();
		die();
	}
	
	//for type 1, 2, 3 users
	/*public function insert_new_account( $agent_email, $email ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT * FROM #__user_registration WHERE email = '".$agent_email."'");
		$db->setQuery($query);
		$agent_id = $db->loadObject();		
		$session =& JFactory::getSession();
		$session->set( 'agent_id', $agent_id->user_id );
		
		//insert into invite table
		$insert_invitation = new JObject();
		$insert_invitation->agent_id = (int)$agent_id->user_id;
		$insert_invitation->invited_email =  $email;
		return $db->insertObject('#__invitation_tracker', $insert_invitation);
		die();
	}*/
	
	// for type 4 users
	public function insert_to_inviteusertype( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT * FROM #__user_registration WHERE email = '".$agent_email."'"); 
		$db->setQuery($query);
		$agent_id = $db->loadObject();
		$session =& JFactory::getSession();
		$session->set( 'agent_id', $agent_id->user_id );
		//insert into invite table
		$insert_invitation = new JObject();
		$insert_invitation->agent_id = (int)$agent_id->user_id;
		$insert_invitation->invited_email =  $email;
		$db->insertObject('#__invitation_tracker', $insert_invitation);
		
		$insert_usertype = new JObject();
		$insert_usertype->user_type = 4;
		$insert_usertype->email = $email;
		$db->updateObject('#__user_registration', $insert_usertype, 'email');
	}
	
	//for type 5 users
	public function insert_to_inviteusertypemanual( $agent_email, $email, $first_name, $last_name, $zip_code, $country_id  ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT * FROM #__user_registration WHERE email = '".$agent_email."'"); 
		$db->setQuery($query);
		$agent_id = $db->loadObject();
		$session =& JFactory::getSession();
		$session->set( 'agent_id', $agent_id->user_id );
		//insert into invite table
		$insert_invitation = new JObject();
		$insert_invitation->agent_id = (int)$agent_id->user_id;
		$insert_invitation->invited_email =  $email;
		$db->insertObject('#__invitation_tracker', $insert_invitation);
		
		$insert_usertype = new JObject();
		$insert_usertype->user_type = 5;
		$insert_usertype->email = $email;
		$db->updateObject('#__user_registration', $insert_usertype, 'email');
	}
	
	//get all invited emails
	public function get_invited_emails() {
		$session  =& JFactory::getSession();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$agent_email = JFactory::getUser()->email;
		$query->setQuery("SELECT * FROM #__user_registration WHERE LOWER(email) = LOWER('".$agent_email."')");
		$db->setQuery($query);
		$agent_id = $db->loadObject();		
		$session->set( 'agent_id', $agent_id->user_id );
		$agent_id = $agent_id->user_id;
	
		$query->setQuery("
			SELECT 
				* 
			FROM 
				#__invitation_tracker a
			LEFT JOIN
				#__user_registration b
			ON
				LOWER(b.email) = LOWER(a.invited_email)
			WHERE 
				a.agent_id = '".$agent_id."'
		"
		);
		$db->setQuery($query);
		return $db->loadObjectList();
		die();
	}
	
	public function insert_invited_emails( $invited_email, $email, $first_name, $last_name, $zip_code, $country_id ){
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		$insert_invitation = new JObject();
		$insert_invitation->email = $invited_email;
		$insert_invitation->firstname = $first_name;
		$insert_invitation->lastname = $last_name;
		$insert_invitation->user_type = 2;
		$insert_invitation->zip = $zip_code;
		$insert_invitation->is_premium = (int)1;
		$insert_invitation->country = (int)$country_id;
		$insert_invitation->registration_date = date('Y-m-d H:i:s',time());
		$db->insertObject('#__user_registration', $insert_invitation);
		
		// get inviter uid
		$query = $db->getQuery(true);
		$query->setQuery("SELECT * FROM #__user_registration WHERE email = '".$email."'");
		$db->setQuery($query);
		$agent_id = $db->loadObject();		
		$session =& JFactory::getSession();
		$session->set( 'agent_id', $agent_id->user_id );
		
		// get invitee uid
		$query = $db->getQuery(true);
		$query->setQuery("SELECT * FROM #__user_registration WHERE email = '".$invited_email."'");
		$db->setQuery($query);
		$own_id = $db->loadObject();		
		$session =& JFactory::getSession();
		$session->set( 'agent_id', $own_id->user_id );
		
		//insert into invite table
		$insert_invitation = new JObject();
		$insert_invitation->agent_id = (int)$agent_id->user_id;
		$insert_invitation->invited_email =  $invited_email;
		$db->insertObject('#__invitation_tracker', $insert_invitation);
		
		//insert into sales table
		$insert_sales = new JObject();
		$insert_sales->agent_id =  (int)$own_id->user_id;
		$insert_sales->volume_2012 =  NULL;
		$insert_sales->sides_2012 =  NULL;
		$insert_sales->ave_price_2012 =  NULL;
		$insert_sales->verified_2012 =  0;
		$insert_sales->volume_2013 =  NULL;
		$insert_sales->sides_2013 =  NULL;
		$insert_sales->ave_price_2013 =  NULL;
		$insert_sales->verified_2013 =  0;
		
		$insert_sales->volume_2014 =  NULL;
		$insert_sales->sides_2014 =  NULL;
		$insert_sales->ave_price_2014 =  NULL;
		$insert_sales->verified_2014 =  0;
		$db->insertObject('#__user_sales', $insert_sales);
			
	}
	
	
	public function notifyAdminInvitation ($agent_email, $invited_email, $first_name, $last_name, $zip_code) {
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from('#__user_registration')->where('email = \''.JFactory::getUser()->email.'\'');
		$db->setQuery($query);
		$first_name_inviter = $db->loadObject()->firstname;	
		$last_name_inviter = $db->loadObject()->lastname;
		$mailSender = &JFactory::getMailer();
		$body .= "Hello Admin,<br /><br />";
    	$body .= "You have a new invitation.<br /><br />";
    	$body .= "<table>
			    		<tr>
							<td>First Name: </td>
							<td>".$first_name."</td>
			    		</tr>
			    		<tr>
							<td>Last Name: </td>
							<td>".$last_name."</td>
			    		</tr>
			    		<tr>
							<td>Email: </td>
							<td>".$invited_email."</td>
			    		</tr>
			    		<tr>
							<td>Zip Code: </td>
							<td>".$zip_code."</td>
			    		</tr>
						<tr>
							<td>Invited By: </td>
							<td>".$first_name_inviter." ".$last_name_inviter."</td>
			    		</tr>
    				</table>";
    	$body .= "<br /><br />";
    	echo $body .= "To approve the application please log in to <a href=\"".JUri::root()."administrator/index.php?option=com_useractivation\">".JUri::root()."administrator/index.php?option=com_useractivation</a>";
    		
    	//$email = array('rebekah.roque@keydiscoveryinc.com', 'dwant@agentbridge.com', 'redler@agentbridge.com', 'ebooker@agentbridge.com' );
    	$email = array('support@agentbridge.com');
    	$subject = 'New Invitation - ' . $first_name . ' ' . $last_name;
			
    	
    	//$mailSender->addRecipient( $email );
    	//$mailSender->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
    	//$mailSender->setSubject( $subject );
    	$mailSender->isHTML( true);
    	//$mailSender->setBody( $body ); true, $cc, $bcc
		//$mailSender->Send();
		$cc = "rebekah.roque@keydiscoveryinc.com";
		$bcc = "mark.obre@keydiscoveryinc.com";
		$mailSender->sendMail('no-reply@agentbridge.com', 'AgentBridge', $email, $subject, $body,  true, $cc, $bcc);
	
	}
	
	public function get_current_invites_count(){
		$user = JFactory::getUser();
		$agent_email = $user->email;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->setQuery("
			SELECT count(b.email) as current_invite_count FROM 
				#__invitation_tracker a 
			LEFT JOIN 
				#__user_registration b 
			ON 
				a.agent_id = b.user_id
			WHERE b.email = '".$agent_email."'
			"
		);
		$db->setQuery($query);
		return $db->loadObject();
	}
	//get videos
	public function getListVideos(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(
			array(
					'*'
			)
		);
		$query->from('#__video_management vm');
		$query->order('order_number ASC');
		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		$query->where('page="Support"');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}
	//check if all pops is updated 
	public function checkUpdatedAll($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(
			array(
					'ur.is_updated_all'
			)
		);
		$query->from('#__user_registration ur');
		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		$query->where('email="'.$email.'"');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}
	//update is_updated_all
	public function updateUpdatedAll($email){
		$db = JFactory::getDbo();
		$update = new JObject();
		$update->is_updated_all = 1;
		$update->email = $email;
		$upd = $db->updateObject('#__user_registration', $update, 'email', false);	
		return $upd;
	}
}
?>