<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT . '/controller.php' );
 
// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 if(!JFactory::getUser()->id){
 	if (isset($_GET['webservice']) && $_GET['webservice']=="1") {
	
	} else
  	  JFactory::getApplication()->redirect('index.php?illegal=1&returnurl='.base64_encode(JURI::current().$_SERVER['REQUEST_URI']));
 	
 }
// Create the controller
$classname    = 'UserProfileController'.$controller;
$controller   = new $classname( );
  
// Perform the Request task
$controller->execute( JRequest::getWord( 'task' ) );
  
// Redirect if set by the controller
$controller->redirect();


?>

