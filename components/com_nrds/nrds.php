<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// Require the base controller
 
require_once( JPATH_COMPONENT . '/controller.php' );
 
// Require specific controller if requested
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}
 
// Create the controller
$classname    = 'NrdsController'.$controller;
$controller   = new $classname( );

if(JFactory::getUser()->id && ($_GET['task'] !== 'actusersales' && $_GET['task'] !== 'setUpdateSalesSession')){
	$url = (isset($_GET['returnurl'])) ? base64_decode($_GET['returnurl']) : 'index.php?option=com_activitylog'; 
	JFactory::getApplication()->redirect($url);
}
  
// Perform the Request task
$controller->execute( JRequest::getWord( 'task' ) );
  
// Redirect if set by the controller
$controller->redirect();

?>

