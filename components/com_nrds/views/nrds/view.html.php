<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 
/**
 * HTML View class for the HelloWorld Component
 *
 * @package    HelloWorld
 */
 
class NrdsViewNrds extends JViewLegacy
{
    function display($tpl = null)
    {
        parent::display($tpl);
    }
}

?>