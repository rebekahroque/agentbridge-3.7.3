<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.formvalidation');
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	$user =& JFactory::getUser();	

	
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl;?>/templates/agentbridge/plugins/fancybox/jquery.fancybox-1.3.4.css">
<style type="text/css">
ul.nav.nav-tabs.nav-stacked a {
  color: white;
  /* margin: 10px 3px 3px 3px; */
}

.login-form {
    /* padding-top: 12px; */
    float: none;
    margin-bottom: 0px;
}
</style>
<script type="text/javascript">
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery(document).ready(function(){
	jQuery('button[type="submit"]').live("click", function(){

		jQuery(".jform_field1.error_msg").hide();
		jQuery(".jform_field2.error_msg.error1").hide();
		jQuery("#username").removeClass("glow-required");

		var error=0;

		if(!jQuery("#volume_2016").val()){
			jQuery("#volume_2016").addClass("glow-required");
			jQuery(".jform_field1.error_msg.error1").show();
			error++;
		}

		if(!jQuery("#sides_2016").val()){
			jQuery("#sides_2016").addClass("glow-required");
			jQuery(".jform_field2.error_msg.error1").show();
			error++;
		}

		if(error>0){
			return false;
		} else {
			jQuery("#updatesales").submit();
		}
 		
	});

	jQuery("#fancybox-close").live("click",function(){
		window.location = "<?php echo $this->baseurl?>";
	});
});
	
</script>

<div class="banner-row" style="padding-bottom:40px; min-height:800px">
<div style="display:none;">
<div style="width:90%; margin:0 auto; padding-top:40px" class="login<?php echo $this->pageclass_sfx?>">

</div>
<!--<div>
	<ul class="nav nav-tabs nav-stacked">
		<li>
			<a style="color:#000" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
		</li>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
		</li>
		<?php endif; ?>
	</ul>
</div>-->
</div>
</div>

<div class="overlay"></div>
<div class="overlay-container">
	<div class="form-container">
		<div class="formlogin">
			<a id="fancybox-close" style="display:block"></a>
			<form id="updatesales" style="width:100%; background-color:#ffffff; border-radius:10px; padding:10px; margin:0px auto; color:#000000;position:relative" action="<?php echo JRoute::_('index.php?option=com_nrds&task=usersalessubmit'); ?>" method="POST" class="form-validate form-horizontal well">
				<div class="login-logo"></div>
				<div class="login-field"><input type="text" name="volume_2016" id="volume_2016" value="" class="validate-password required big-field" tabindex="0" placeholder="2016 Sales Volume" /></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field"><input type="text" name="sides_2016" id="sides_2016" value="" class="validate-password required big-field" tabindex="0" placeholder="2016 Sides" /></div>
				<p class="jform_field1 error_msg error1" style="display:none;">This field is required</p>
				<div class="login-field" style="padding-bottom: 20px">
					<button type="submit" class="button submit-btn big-submit">Update Profile</button>
				</div>
				<input type="hidden" name="return" value="<?php echo $_GET['returnurl']; ?>"/>
				<input type="hidden" name="h321" value="<?php echo $_GET['h321']; ?>"/>
			</form>
			<div class="bottom-signup">&nbsp;</div>
		</div>
	</div>
</div>
<script>

jQuery(document).ready(function(){
	jQuery(".hpform").hide();
	<?php //  var_dump($this->username);?>

	<?php if($this->username){?>
		jQuery("#username").val("<?php echo $this->user_email; ?>");
		jQuery("#username").hide();
		jQuery("#username").after("<div id='def_user'><?php echo $this->username; ?> <span id='close_def' >x</span></div>");
	<?php } ?>

	jQuery("#close_def").attr("onmouseover","ddrivetip('Not you?');hover_dd()");
	jQuery("#close_def").attr("onmouseout","hideddrivetip();hover_dd()");

	jQuery("#close_def").live("click", function(){
		jQuery("#def_user").remove();
		jQuery("#username").val("");
		jQuery("#username").show();
	});

	jQuery("#volume_2016").autoNumeric('init', {aSign:'<?php echo $this->user_currency_symbol; ?>', mDec: '0'});
	jQuery("#volume_2016").val("");
	jQuery("#volume_2016").autoNumeric('update', {aSign:'<?php echo $this->user_currency_symbol; ?>', mDec: '0'});
	
	jQuery("#sides_2016").autoNumeric('init', {mDec: '0', vMax: '9999'});
	
	
	removethis=" <?php echo $this->user_currency; ?>";
	addthis=" <?php echo $this->user_currency; ?>";
	
	jQuery("#volume_2016").blur(function(){
		if(jQuery(this).val().indexOf(addthis)==-1){
			console.log(addthis);
			jQuery(this).val(jQuery(this).val()+" "+addthis);
		}
	});

});

</script>
<style>
	.overlay {
		width:100%;
		height:100%;
		z-index:2000;
		opacity: 0.4;
		filter: alpha(opacity=40); /* msie */
		background-color: #000;
		position:fixed;
		top:0;
		left:0;
	}
	
	.overlay-container {
		display:table;
		position:fixed;
		width:100%;
		height:100%;
		z-index:2001;
		top:0;
	}
	
	.form-container {
		display:table-cell;
		vertical-align:middle;
	}
	
	.formlogin {
		margin: 0 auto;
		display:table;
		width:500px;
		height:370px;
		position:relative;
		background:#fff;
		border-radius:10px;
		box-sizing:border-box;
	}
	
	.login-logo {
		background:url("/~forqa/templates/agentbridge/images/main-logo.png") no-repeat;
		width:208px;
		height:49px;
		display:table;
		margin:0 auto 20px;
	}
	
	.login-field {
		width:400px;
		display:table;
		margin:10px auto;
	}
	
	.big-field {
		width:100%;
		height:40px;
		border: 2px solid #ccc;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	.big-submit {
		width:100%;
		height:40px;
		font-size:16px;
		color:#fff;
		background:#339900;
		border-radius:5px;
		padding:10px;
		box-sizing:border-box;
	}
	
	#remember-me-big {
		border:2px solid #ccc;
		width:15px;
		height:15px;
	}
	
	.chklabel {
		margin:0 0 0 5px;
		font-size:16px;
	}
	
	.bottom-signup {
		position:absolute;
		border-top:1px solid #ccc;
		text-align:right;
		padding:15px;
		font-size:16px;
		box-sizing:border-box;
		width:100%;
		bottom:0;
	}
	
	.bottom-signup a {
		font-size:16px;
	}
	
	.formlogin p {
		font-size:16px;width:400px;margin:0 auto;
	}
	
	.login-field a {
		color:#007bae;font-size:16px;
	}
	
	@media screen and (max-width: 500px) {
		.formlogin {
			margin: 0 auto;
			display:table;
			width:300px;
			height:330px;
			position:relative;
			background:#fff;
			border-radius:10px;
			box-sizing:border-box;
		}
		
		.login-logo {
			background:url("/~forqa/templates/agentbridge/images/main-logo.png") no-repeat;
			width:208px;
			height:49px;
			display:table;
			margin:0 auto 20px;
		}
		
		.login-field {
			width:250px;
			display:table;
			margin:10px auto;
		}
		
		.big-field {
			width:100%;
			height:30px;
			border: 2px solid #ccc;
			border-radius:5px;
			padding:10px;
			box-sizing:border-box;
		}
		
		.big-submit {
			width:100%;
			height:30px;
			font-size:16px;
			color:#fff;
			background:#339900;
			border-radius:5px;
			padding:5px;
			box-sizing:border-box;
		}
		
		#remember-me-big {
			border:2px solid #ccc;
			width:15px;
			height:15px;
		}
		
		.chklabel {
			margin:0 0 0 5px;
			font-size:14px;
		}
		
		.bottom-signup {
			position:absolute;
			border-top:1px solid #ccc;
			text-align:right;
			padding:15px;
			font-size:14px;
			box-sizing:border-box;
			width:100%;
			bottom:0;
		}
		
		.bottom-signup a {
			font-size:14px;  
		}
		
		.formlogin p {
			width:250px;
			font-size:14px;
		}
		
		.login-field a {
			color:#007bae;font-size:14px;
		}
	}
</style>
