<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.formvalidation');
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	$user =& JFactory::getUser();	
?>


<!--<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/dg_slider.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.qtip.custom/jquery.qtip.min.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/plugins/magnificPopup/magnific-popup.css"/>

<script src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/dg_slider.min.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/rwdimagemaps/jquery.rwdImageMaps.min.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.qtip.custom/jquery.qtip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/magnificPopup/magnificPopup2.js"></script>-->

<?php echo pushCSS($this->baseurl."/templates/agentbridge/css/dg_slider.css","all");?>
<?php echo pushCSS($this->baseurl."/templates/agentbridge/scripts/jquery.qtip.custom/jquery.qtip.min.css","all");?>
<?php echo pushCSS($this->baseurl."/templates/agentbridge/plugins/magnificPopup/magnific-popup.css");?>

<?php echo pushScript($this->baseurl."/templates/agentbridge/scripts/dg_slider.min.js"); ?>
<?php echo pushScript($this->baseurl."/templates/agentbridge/scripts/rwdimagemaps/jquery.rwdImageMaps.min.js"); ?>
<?php echo pushScript($this->baseurl."/templates/agentbridge/scripts/jquery.qtip.custom/jquery.qtip.min.js"); ?>
<?php echo pushScript($this->baseurl."/templates/agentbridge/scripts/magnificPopup/magnificPopup2.js"); ?>

<script type="text/javascript">
if(jQuery(window).width() > 773) {
	mixpanel.track("Home Page");
} else {
	mixpanel.track("Mobile Home Page");
}
</script>

<div class="ab-row">
	<div id="ab-hero">
		<div class="hero-container">
			<div class="hero-left">
				<div class="slogan"><?php echo JText::_('COM_HOME_HERO_SLOGAN') ?></div>
				<div class="slogan-landing"><?php echo JText::_('COM_HOME_LANDING_HERO_SLOGAN') ?></div>
				<div class="how-ab-works"><a id="video-how-ab-works" href="https://vimeo.com/103826473" class="mfp-iframe"><?php echo JText::_('COM_HOME_VIEW_HOW_AB_WORKS') ?></a></div>
			</div>
			<div class="hero-right">
				<div id="private-listings">
					<div class="amount">$8 <?php echo JText::_('COM_HOME_BILLION') ?></div>
					<div class="description"><?php echo JText::_('COM_HOME_IN_PRIVATE_LISTINGS') ?></div>
				</div>
				<div id="verified-amount">
					<div class="amount">$100 <?php echo JText::_('COM_HOME_BILLION') ?></div>
					<div class="description"><?php echo JText::_('COM_HOME_VERIFIABLE_AMOUNT_SOLD') ?></div>
				</div>
				<div id="ave-agent-sales">
					<div class="amount">$<?php echo $this->ave_volume; ?></div>
					<div class="description"><?php echo JText::_('COM_HOME_AVE_AGENT_YEARLY_SALES') ?></div>
				</div>
			</div>
		</div>
	</div>
	<div id="ab-hero-mobile">
		<div class="slogan"><?php echo JText::_('COM_HOME_LANDING_HERO_SLOGAN') ?></div>
		<div id="private-listings">
			<div class="amount">$8 <?php echo JText::_('COM_HOME_BILLION') ?></div>
			<div class="description"><?php echo JText::_('COM_HOME_IN_PRIVATE_LISTINGS') ?></div>
		</div>
		<div id="verified-amount">
			<div class="amount">$100 <?php echo JText::_('COM_HOME_BILLION') ?></div>
			<div class="description"><?php echo JText::_('COM_HOME_VERIFIABLE_AMOUNT_SOLD') ?></div>
		</div>
		<div id="ave-agent-sales">
			<div class="amount">$<?php echo $this->ave_volume; ?></div>
			<div class="description"><?php echo JText::_('COM_HOME_AVE_AGENT_YEARLY_SALES') ?></div>
		</div>
		<div id="worldwide">
			<div class="amount"><?php echo JText::_('COM_HOME_WORLDWIDE') ?></div>
			<div class="description"><?php echo JText::_('COM_HOME_REPRESENTATION') ?></div>
		</div>
	</div>
</div>
<div class="ab-row">
	<div class="about-bg">
		<div class="about-left"></div>
		<div class="about-right"></div>
	</div>
	<div class="about-ab">
		<div class="ab-who">
			<div class="title"><?php echo JText::_('COM_HOME_WHO') ?></div>
			<div class="content">
				<p><?php echo JText::_('COM_HOME_WHO_IS_AB') ?></p>
				<ul>
					<li><?php echo JText::_('COM_HOME_WHO_BULLET1') ?></li>
					<li><?php echo JText::_('COM_HOME_WHO_BULLET2') ?></li>
					<li><?php echo JText::_('COM_HOME_WHO_BULLET3') ?></li>
				</ul>
			</div>
		</div>
		<div class="ab-what">
			<div class="title"><?php echo JText::_('COM_HOME_WHAT') ?></div>
			<div class="content">
				<p><?php echo JText::_('COM_HOME_WHAT_DOES_AB_OFFER') ?></p>
				<ul>
					<li><?php echo JText::_('COM_HOME_WHAT_BULLET1') ?></li>
					<li><?php echo JText::_('COM_HOME_WHAT_BULLET2') ?></li>
				</ul>
			</div>
		</div>
		<div class="ab-how">
			<div class="title"><?php echo JText::_('COM_HOME_HOW') ?></div>
			<div class="content">
				<p><?php echo JText::_('COM_HOME_HOW_CAN_YOU_FIND') ?></p>
				<ul>
					<li><?php echo JText::_('COM_HOME_HOW_BULLET1') ?></li>
					<li><?php echo JText::_('COM_HOME_HOW_BULLET2') ?></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="about-ab-mobile">
		<div class="ab-who">
			<div class="title"><?php echo JText::_('COM_HOME_WHO_MOBILE') ?></div>
			<div class="content">
				<p class="small"><?php echo JText::_('COM_HOME_WHO_MOBILE_ONLY') ?></p>
				<p class="top1"><?php echo JText::_('COM_HOME_WHO_ONE_PERCENT') ?></p>
				<p class="small"><?php echo JText::_('COM_HOME_WHO_AGENTS_QUALIFY') ?></p>
			</div>
			<div class="testimonial">
				<div class="slide-img">
					<div class="slide-bg2"></div>
					<div class="slide-photo"></div>
				</div>
			</div>
			<div class="testimonial-text">
				<p class="quote"><?php echo JText::_('COM_HOME_TESTIMONIAL1') ?></p>
				<p class="quote-from"><?php echo JText::_('COM_HOME_TESTIMONIAL1_OWNER') ?></p>
			</div>
		</div>
		<div class="ab-what">
			<div class="title"><?php echo JText::_('COM_HOME_WHAT_MOBILE') ?></div>
			<div class="content">
				<p class="small"><?php echo JText::_('COM_HOME_WHAT_MOBILE_ACCESS') ?></p>
				<p class="big-green"><?php echo JText::_('COM_HOME_WHAT_MOBILE_CONFIDENTIAL') ?></p>
				<p class="small" style="margin-bottom:20px;"><?php echo JText::_('COM_HOME_WHAT_MOBILE_PROPERTIES') ?></p>
				<div class="ab-what-map"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ab-website-fnl/ab-what-map.png" /></div>
				<p class="small"><?php echo JText::_('COM_HOME_WHAT_BEST_DEALS') ?></p>
				<p class="big-green"><?php echo JText::_('COM_HOME_WHAT_PRIVATE_PROCESS') ?></p>
			</div>
		</div>
		<div class="ab-how">
			<div class="title"><?php echo JText::_('COM_HOME_HOW_MOBILE') ?></div>
			<div class="content">
				<div class="founder-badge"></div>
				<p class="small"><?php echo JText::_('COM_HOME_HOW_MOBILE_LOOK_FOR') ?></p>
			</div>
		</div>
	</div>
</div>
<div class="ab-row">
	<div id="carousel">
		<div class="carousel-container">
			<ul class="slider1">
				<li>
					<div class="slide">
						<div class="slide-left">
							<div class="slide-bg"></div>
							<div class="slide-photo1"></div>
						</div>
						<div class="slide-right"><span><?php echo "&ldquo;" . JText::_('COM_NRDS_AGENT1_TEXT2') . "&rdquo;";?> - Rick Edler, <?php echo JText::_('COM_NRDS_AGENT1_TEXT1');?></span></div>
					</div>
				</li>
				<li>
					<div class="slide">
						<div class="slide-left">
							<div class="slide-bg2"></div>
							<div class="slide-photo"></div>
						</div>
						<div class="slide-right"><span><?php echo JText::_('COM_HOME_TESTIMONIAL1') ?> - Gary Elminoufi, <?php echo JText::_('COM_NRDS_AGENT2_TEXT1');?></span></div>
					</div>
				</li>
				<li>
					<div class="slide">
						<div class="slide-left">
							<div class="slide-bg"></div>
							<div class="slide-photo3"></div>
						</div>
						<div class="slide-right"><span><?php echo "&ldquo;" . JText::_('COM_NRDS_AGENT3_TEXT2') . "&rdquo;";?> - Lily Liang, <?php echo JText::_('COM_NRDS_AGENT3_TEXT1');?></span></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="ab-row map">
	<div id="map">
		<div class="map-title"><?php echo JText::_('COM_HOME_CONNECT_TOP_AGENTS') ?></div>
		<div class="rollover-tip"><?php echo JText::_('COM_HOME_ROLLOVER') ?></div>
		<img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ab-website-fnl/ab-map-no-text.jpg" usemap="#ab-cities" />
		<map name="ab-cities">
			<area shape="rect" coords="226,449,241,459" id="map-los-angeles" alt="Los Angeles">
			<area shape="rect" coords="231,460,246,475" id="map-san-diego" alt="San Diego">
			<area shape="rect" coords="218,434,235,449" id="map-san-francisco" alt="San Francisco">
			<area shape="rect" coords="204,406,219,421" id="map-seattle" alt="Portland">
			<area shape="rect" coords="208,389,223,404" id="map-vancouver" alt="Seattle">
			<area shape="rect" coords="243,406,258,421" id="map-sun-valley" alt="Sun Valley">
			<area shape="rect" coords="269,427,284,442" id="map-denver" alt="Denver">
			<area shape="rect" coords="246,448,261,463" id="map-phoenix" alt="Phoenix">
			<area shape="rect" coords="274,448,289,463" id="map-austin" alt="Austin">
			<area shape="rect" coords="280,443,295,458" id="map-houston" alt="Houston">
			<area shape="rect" coords="280,485,295,500" id="map-mexico-city" alt="Mexico City">
			<area shape="rect" coords="307,376,322,391" id="map-chicago" alt="Chicago">
			<area shape="rect" coords="312,435,327,450" id="map-atlanta" alt="Atlanta">
			<area shape="rect" coords="327,461,342,476" id="map-miami" alt="Miami">
			<area shape="rect" coords="338,379,353,394" id="map-toronto" alt="Toronto">
			<area shape="rect" coords="365,398,380,405" id="map-new-york" alt="New York">
			<area shape="rect" coords="357,400,372,408" id="map-washington-dc" alt="Washington D.C.">
			<area shape="rect" coords="360,404,375,412" id="map-boston" alt="Boston">
			<area shape="rect" coords="439,611,454,626" id="map-rio-de-janeiro" alt="Rio de Janeiro">
			<area shape="rect" coords="401,651,416,656" id="map-buenos-aires" alt="Buenos Aires">
			<area shape="rect" coords="621,651,636,656" id="map-capetown" alt="Capetown">
			<area shape="rect" coords="654,620,669,635" id="map-johannesburg" alt="Johannesburg">
			<area shape="rect" coords="547,356,562,371" id="map-dublin" alt="Dublin">
			<area shape="rect" coords="564,362,579,378" id="map-london" alt="London">
			<area shape="rect" coords="572,377,587,393" id="map-paris" alt="Paris">
			<area shape="rect" coords="556,413,571,428" id="map-madrid" alt="Madrid">
			<area shape="rect" coords="588,367,601,382" id="map-amsterdam" alt="Amsterdam">
			<area shape="rect" coords="594,394,609,409" id="map-geneva" alt="Geneva">
			<area shape="rect" coords="603,405,618,420" id="map-milan" alt="Milan">
			<area shape="rect" coords="620,371,635,378" id="map-munich" alt="Munich">
			<area shape="rect" coords="616,378,631,388" id="map-frankfurt" alt="Frankfurt">
			<area shape="rect" coords="612,389,626,404" id="map-zurich" alt="Zurich">
			<area shape="rect" coords="627,390,642,405" id="map-berlin" alt="Berlin">
			<area shape="rect" coords="709,325,724,340" id="map-moscow" alt="Moscow">
			<area shape="rect" coords="681,400,696,415" id="map-istanbul" alt="Istanbul">
			<area shape="rect" coords="679,435,694,450" id="map-tel-aviv" alt="Tel Aviv">
			<area shape="rect" coords="724,460,739,475" id="map-dubai" alt="Dubai">
			<area shape="rect" coords="783,488,798,503" id="map-mumbai" alt="Mumbai">
			<area shape="rect" coords="906,407,921,422" id="map-beijing" alt="Beijing">
			<area shape="rect" coords="975,427,990,442" id="map-tokyo" alt="Tokyo">
			<area shape="rect" coords="920,422,935,447" id="map-shanghai" alt="Shanghai">
			<area shape="rect" coords="942,459,957,474" id="map-taipei" alt="Taipei">
			<area shape="rect" coords="907,470,922,485" id="map-hong-kong" alt="Hong Kong">
			<area shape="rect" coords="866,529,881,544" id="map-kuala-lumpur" alt="Kuala Lumpur">
			<area shape="rect" coords="873,540,888,555" id="map-singapore" alt="Singapore">
			<area shape="rect" coords="884,567,899,582" id="map-jakarta" alt="Jakarta">
			<area shape="rect" coords="1007,643,1022,658" id="map-sydney" alt="Sydney">
			<area shape="rect" coords="1077,656,1092,671" id="map-auckland" alt="Auckland">
		</map>
	</div>
</div>
<div class="founder-bottom"></div>
<div class="ab-row ab-footer-row">
	<div class="ab-logo-footer"></div>
	<div id="authnet_container">
		<!-- (c) 2005, 2016. Authorize.Net is a registered trademark of CyberSource Corporation --> 
		<div class="AuthorizeNetSeal"> 
			<script type="text/javascript" language="javascript">var ANS_customer_id="d5d311e7-6787-4b9d-af1f-b84ee97494a3";</script>
			<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> 
			<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank" style="display:none;">Merchant Services</a> 
		</div>   
	</div>
	<div class="ab-footer"><?php echo JText::_('COM_HOME_NEED_HELP') ?>
		<p class="copyright"><?php echo JText::sprintf('COM_AB_COPYRIGHT',date("Y")) ?></p>
	</div>
	
	<div class="ab-footer-mobile">
		<p class="contact-title"><?php echo JText::_('COM_HOME_CONTACT_US') ?></p>
		<p>310-870-1231</p>
		<p><a href="mailto:support@agentbridge.com">support@agentbridge.com</a></p>
		<div class="ab-logo-footer-mobile"></div>
		<p class="copyright"><?php echo JText::sprintf('COM_AB_COPYRIGHT',date("Y")) ?></p>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		
		jQuery.getJSON('https://ipinfo.io/json', function (data) { 
			if(data.country == "PH" || data.country == "GB" || data.country == "US") {
				if(!getCookie("AB_download_app")) { 
					var ua = navigator.userAgent.toLowerCase();
					var isAndroid = ua.indexOf("android") > -1;
					var isIPhone = ua.indexOf("iphone") > -1;
					var isIPad = ua.indexOf("ipad") > -1;
					var isIPod = ua.indexOf("ipod") > -1;
					
					if(isAndroid) {
						jQuery(".app-prompt-overlay").show();
						
						jQuery("#download-app").click(function(){
							document.location = "https://play.google.com/store/apps/details?id=eng.cucumber.agentbridge";
						});
					}
					
					if(isIPhone || isIPad || isIPod) {
						jQuery(".app-prompt-overlay").show();
						
						
						
						jQuery("#download-app").click(function(){
							document.location = "https://itunes.apple.com/us/app/agentbridge-real-estate-network/id882675097";
						});
					}
					
					jQuery("#fancybox-close, #continue-web").click(function(){
						jQuery(".app-prompt-overlay").slideUp();
					});
					
					setCookie("AB_download_app", "1", 1);
				}
			}
		});
		
		
		
		jQuery('input[type="submit"]').live("click", function(){
			jQuery(".jform_field1.error_msg").hide();
			jQuery(".jform_field2.error_msg.error1").hide();
			jQuery(".errorbr").hide();
			jQuery("#username").removeClass("glow-required");

			var error=0;

			if(!jQuery("#username").val()){
				jQuery("#username").addClass("glow-required");
				jQuery(".jform_field1.error_msg.error1").show();
				error++;
			} else {
				if(IsEmail(jQuery("#username").val())==false){
					jQuery("#username").addClass("glow-required");
					jQuery(".jform_field1.error_msg.error1").hide();
					jQuery(".jform_field1.error_msg.error2").show();
					error++;
				}
			}

			if(!jQuery("#password").val()){
				jQuery("#password").addClass("glow-required");
				jQuery(".jform_field2.error_msg.error1").show();
				jQuery(".errorbr").show();
				error++;
			}

			if(error>0){
				return false;
			}
			
		});
		
		
		jQuery('.slider1').DG_Slider({
			auto: true, 
			pause: 5000,
			controls: true,
			prevText: "<",
			nextText: ">",
		});
		
		jQuery('img[usemap]').rwdImageMaps();
		
		jQuery('area').qtip({
			content: {
				attr: 'alt'
			},
			position: {
				my: 'top left',
				at: 'bottom right'
			}
		});
		
		jQuery("#video-how-ab-works").magnificPopup();

		
		//jQuery("select").not("'#ab-home-lang'").select2();
		
	});
	
	function setCookie(c_name,value,exdays) {
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + 
			((exdays==null) ? "" : ("; expires="+exdate.toUTCString()));
		document.cookie=c_name + "=" + c_value;
	}

	function getCookie(c_name) {
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name) {
				return unescape(y);
			}
		}
	}
	
	jQuery(window).scroll(function (event) {
		var scroll = jQuery(window).scrollTop();
		// Do something
		if(scroll >= jQuery(".app-prompt-overlay").height() && jQuery(".app-prompt-overlay").is(":visible")) {
			jQuery(".app-prompt-overlay").slideUp();
			jQuery('html, body').scrollTop(jQuery('.ab-header').offset().top);
		}
	});
</script>
<style>
	.DG-prev {
		position:absolute;
		width:75px;
		height:140px;
		color:#fff;
		font-size:40px !important;
		font-weight:bold;
		font-family:'Roboto', sans-serif;
		text-align:center;
		line-height:140px;
		top:0;
		left:0;
		text-indent:0;
	}
	.DG-next{
		position:absolute;
		width:75px;
		height:140px;
		color:#fff;
		font-size:40px !important;
		font-weight:bold;
		font-family:'Roboto', sans-serif;
		text-align:center;
		line-height:140px;
		top:0;
		right:0;
		text-indent:0;
	}
</style>