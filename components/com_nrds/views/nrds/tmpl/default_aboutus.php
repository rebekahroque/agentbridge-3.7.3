<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.formvalidation');
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	
?>
<div class="texture-overlay move-up"></div>
<div id="background"><img class="move-up" style="z-index:-1000" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/home-main-image4.jpg" /></div>
		<div class="aboutcontainer" style="margin-bottom:50px">
	<div class="aboutcontent">
    	<div class="topbg">
        	<div class="clear-float clear right" style="margin-top:20px; padding-right:20px"><a href="" class="text-link right" style="font-size:12px; font-weight:bold"> X</a></div>
        	<div class="toptxt"><?php echo JText::_('COM_NRDS_ABT_CONNECT');?></div>
            <div style="clear:both"></div>
            <div class="toptxt-a"><?php echo JText::_('COM_NRDS_ABT_MATCH');?></div>
        </div>
        
        <div class="contwholeleft"> <!-------------left--------------------->
        <div class="contleft">
           	<span><strong><?php echo JText::_('COM_NRDS_BYAGENTS');?></strong></span>
            <p><?php echo JText::_('COM_NRDS_PIONEER');?> </p>
            <br />
            <span><img src="https://www.agentbridge.com/templates/agentbridge/images/check2.png" /><strong><?php echo JText::_('COM_NRDS_ABT_SECURE');?></strong></span>
            <p><strong><?php echo JText::_('COM_NRDS_ABT_POPS');?></strong><br /><?php echo JText::_('COM_NRDS_ABT_POPS_TEXT');?>
            <br /><strong><?php echo JText::_('COM_NRDS_ABT_REF');?></strong><br /><?php echo JText::_('COM_NRDS_ABT_REF_TEXT');?>
            </p>
            <br />
        	<span><img src="https://www.agentbridge.com/templates/agentbridge/images/check2.png" /><strong><?php echo JText::_('COM_NRDS_ABT_CTRL');?></strong></span>
            <ul>
            <li><?php echo JText::_('COM_NRDS_ABT_CTRL1');?></li>
            <li><?php echo JText::_('COM_NRDS_ABT_CTRL2');?></li>
            <li><?php echo JText::_('COM_NRDS_ABT_CTRL3');?></li>
            </ul>
        </div>
     	<div class="bottomleft">
        	<div class="img"><img src="images/people.png" width="478" height="137" /></div>
            <div class="bottombg">
            
            	<div class="quote-a">
                	<div class="quotea"><strong>"<?php echo JText::_('COM_NRDS_ABT_AGENT1');?>"</strong></div>
                	<div class="quoteb"><strong>— Rick Edler</strong> <br /> <span style="font-size:10px;">Top 250 USA Realtor</span></div>
                </div>
           
                
                <div class="quote-c">
                	<div class="quotea"><strong>"<?php echo JText::_('COM_NRDS_ABT_AGENT2');?>"</strong></div>
                	<div class="quoteb"><strong>— Lily Liang</strong> <br /> <span style="font-size:10px;">Top 250 USA Realtor</span></div>
                </div>
                
            </div>
        </div>
		</div>  <!-------------left--------------------->  
        
      <div class="contright">
        
        <span class="rightheadtxt"><?php echo JText::_('COM_NRDS_ABT_SALES');?></span>
        <br />
		<p class="righttxt"><span style="color:#fff;"><?php echo JText::_('COM_NRDS_ABT_SALES1');?></span><br />
        <?php echo JText::_('COM_NRDS_ABT_SALES1_TEX');?>
        </p>
        <div class="imgright"><img src="images/right_img1.png" width="215" height="69" border="0" /></div>
        
		<p class="righttxt"><span style="color:#fff;"><?php echo JText::_('COM_NRDS_ABT_SALES2');?></span><br />
        <?php echo JText::_('COM_NRDS_ABT_SALES2_TEXT');?>
        </p>
        <div class="imgright"><img src="images/right_img2.png" width="215" height="59" border="0" /></div>
                
		<p class="righttxt"><span style="color:#fff;"><?php echo JText::_('COM_NRDS_ABT_SALES3');?></span><br />
        <?php echo JText::_('COM_NRDS_ABT_SALES3_TEXT');?>
        </p>
        <div class="imgright"><img src="images/right_img3.png" width="215" height="62" border="0" /></div>
        
		<p class="righttxt"><span style="color:#fff;"><?php echo JText::_('COM_NRDS_ABT_SALES4');?></span><br />
        <?php echo JText::_('COM_NRDS_ABT_SALES4_TEXT');?>
        </p>
        <div class="imgright"><img src="images/right_img4.png" width="215" height="62" border="0" /></div>        
        <br/>
        <br/>
      </div>
      <div class="clear-float clear right" style="margin-top:20px; padding-right:20px"><a href="" class="text-link right" style="font-size:12px; font-weight:bold"> X</a></div>
    </div>
  </div>
</div>
