<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.formvalidation');
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	$user =& JFactory::getUser();	

	
?>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/lib/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/src/js/jquery.swipebox.js"/></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/src/css/swipebox.css"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/dg_slider.css" type="text/css" media="all" />
<script src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/dg_slider.min.js" type="text/javascript"></script>


<script type="text/javascript">
mixpanel.track("Home Page");
</script>

<?php if($_GET['success']=="true"){ ?>
		<div class="alert alert-thank you">
			<h4 class="alert-heading"><?php echo JText::_('COM_NRDS_THANKYOU');?></h4>
			<div>
					<p><?php echo JText::_('COM_NRDS_APPRECEIVED');?></p>
			</div>
		</div>
<?php } ?>

<?php if(isset($_GET['utype'])){ ?>
	<div class="row banner-row chartertext"> <!-- Start Banner Row --> 
		<input id="utype" type="hidden" value="<?php echo $_GET['utype'] ?>" />
		<input id="vid" type="hidden" value="<?php echo $_GET['vid'] ?>" />
        <span class="glow"></span>
        <h2><?php echo JText::_('COM_NRDS_EARNED');?></h2>
        <p style="line-height:32px"><?php echo JText::_('COM_CHARTER_TEXT');?></p>
		<a 
		<?php if (isset($_GET['useregid'])) { ?>			
			href="<?php echo JRoute::_('index.php?option=com_setpass&action=activated&useregid='. $_GET['useregid'].'&jgadeprasdium='.$_GET['jgadeprasdium'])?>"
		<?php } else { ?>
			href="<?php echo JRoute::_('index.php/component/nrds/?task=manual' )?>"
		<?php } ?> class="join-btn"><?php echo JText::_('COM_NRDS_BANNER_CLICKJOIN');?></a>
    </div> <!-- End Bannr Row -->
<?php } else { ?>	
	<div class="row banner-row noncharter"> <!-- Start Banner Row --> 
		<input id="utype" type="hidden" value="<?php echo $_GET['utype'] ?>" />
		<input id="vid" type="hidden" value="<?php echo $_GET['vid'] ?>" />
        <span class="glow"></span>
        <h2><?php echo JText::_('COM_NRDS_BANNER_SLOGAN_F');?></h2>
        <p><?php echo JText::_('COM_NRDS_BANNER_SLOGAN_S');?></p>
		<a 
		<?php if (isset($_GET['useregid'])) { ?>			
			href="<?php echo JRoute::_('index.php?option=com_setpass&action=activated&useregid='. $_GET['useregid'].'&jgadeprasdium='.$_GET['jgadeprasdium'])?>"
		<?php } else { ?>
			href="<?php echo JRoute::_('index.php/component/nrds/?task=manual' )?>"
		<?php } ?> class="join-btn"><?php echo JText::_('COM_NRDS_BANNER_CLICKJOIN');?></a>
    </div> <!-- End Banner Row -->
<?php } ?>	
	<div class="row ticker-row"> <!-- Start Ticker Row -->
        <div class="wrapper" style="text-align:center">
            <div class="ticker-container">
               <span class="us-flg" style="margin-top: -9px;">		     
                    <img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/us-flag.png">		
                    <img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ca-flag.png">
                    <img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/uk-flag.png">
                    <img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ie-flag.png">
                    <img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/au-flag.png">
					<img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/fr-flag.png">					
					<img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/de-flag.png">
					<img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/mc-flag.png">
					<img style="width:45px" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ch-flag.png">
                </span><br/>
                <span class="ticker clear-float" style="margin-top:5px">
				<?php echo "$".number_format($this->total, 0, '.', ',') ?></span><br/>
				<span class="ave-head clear-float"><?php echo JText::_('COM_NRDS_AVEVOLUME');?></span>
               
            </div>
        </div>
    </div> <!-- End Ticker Row -->
	
    <div class="row agent-bridge-row"> <!-- Start Agent Bridge -->
        <div class="wrapper-home">
            <h1><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/agentlogo-head.png"></h1>
            <div class="columns">
                <div class="column-m3">
                    <span class="agent-bridge-avatar">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ab-avatar1.jpg" alt="Rick Edler">
                    </span>
                    <h2><?php echo JText::_('COM_NRDS_WHOWEARE');?></h2>
                    <p><strong>Rick Edler <br/><?php echo JText::_('COM_NRDS_AGENT1_TEXT1');?></strong> <?php echo JText::_('COM_NRDS_AGENT1_TEXT2');?></p>
                </div>
                <div class="column-m3">
                     <span class="agent-bridge-avatar">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ab-avatar2.jpg"  alt="Gary Elminoufi ">
                    </span> 
                    <h2><?php echo JText::_('COM_NRDS_WHATWEDO');?></h2>
                    <p><strong>Gary Elminoufi <br/><?php echo JText::_('COM_NRDS_AGENT2_TEXT1');?></strong> <?php echo JText::_('COM_NRDS_AGENT2_TEXT2');?></p>
                </div>
                <div class="column-m3">
                    <span class="agent-bridge-avatar">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/ab-avatar3.jpg"  alt="Lily Liang">
                    </span>
                    <h2><?php echo JText::_('COM_NRDS_HOWWEDOIT');?></h2>
                    <p><strong>Lily Liang <br/><?php echo JText::_('COM_NRDS_AGENT3_TEXT1');?></strong><?php echo JText::_('COM_NRDS_AGENT3_TEXT2');?></p>
                </div>
            </div>

        </div>
    </div> <!-- End Agent Bridge -->

    <div class="row private-listings-row">  <!-- Start Private Listings Row -->
        <div class="wrapper-home">
            <a  class="view-video swipebox" id="larger" href="https://vimeo.com/103826473"><?php echo JText::_('COM_NRDS_HOWDOESITWORK');?></a>
            <h1><?php echo JText::_('COM_NRDS_PRIVATELISTING');?></h1>
            <div class="columns">
                <div class="column-m3">
                    <h2><?php echo JText::_('COM_NRDS_VERIFYAGENT');?></h2>
                    <p><?php echo JText::_('COM_NRDS_VERIFYAGENT_TEXT');?></p>
                    <img width="320" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/verified-agent-profiles.jpg"  alt="Verified Agent Profiles">
                </div>
        
                <div class="column-m3">
                    <h2><?php echo JText::_('COM_NRDS_MATCHES');?><br/><br/></h2>
                    <p><?php echo JText::_('COM_NRDS_MATCHES_TEXT');?></p>
                    <img width="320" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/matches.jpg"  alt="Matches">
                </div>

                <div class="column-m3">
                    <h2><?php echo JText::_('COM_NRDS_HOWYOUWORK');?></h2>
                    <p><?php echo JText::_('COM_NRDS_HOWYOUWORK_TEXT');?></p>
                    <img width="320" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-how-you-work.jpg"  alt="Works How You Work">
                </div>
            </div>
        </div>
    </div>  <!-- End Private Listings Row -->

    <div class="row agent-bridge-works"> <!-- Start Agent Bridge Works -->
        <h1><?php echo JText::_('COM_NRDS_ABWORKS');?></h1>
        <div class="slider-works">
           <span class="blue-overlay"></span>
            <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-bg.jpg" class="works-bg">
            <div class="wrapper">
			  <ul id="slider1">
                  <li>
                    <div class="works-avatar">
                      <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-avatar1.jpg"/>
                    </div>
                    <div class="works-content">
                      <div class="quote"><span class="quote-left quotes"></span><blockquote><?php echo JText::_('COM_NRDS_AGENT1_QUOTE');?></blockquote><span class="quote-right quotes"></span></div>

                      <div class="work-name">Tony Accardo</div>
                      <div class="tagname"><?php echo JText::_('COM_NRDS_AGENT1_BROKER');?></div>
                    </div>
                  </li>
                  <li>
                    <div class="works-avatar">
                      <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-avatar2.jpg"/>
                    </div>
                    <div class="works-content">
                      <div class="quote"><span class="quote-left quotes"></span><blockquote><?php echo JText::_('COM_NRDS_AGENT2_QUOTE');?> </blockquote><span class="quote-right  quotes"></span></div>
                      <div class="work-name">John Cain</div>
                      <div class="tagname"><?php echo JText::_('COM_NRDS_AGENT2_BROKER');?> </div>
                    </div>
                  </li>
                  <li>
                    <div class="works-avatar">
                      <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-avatar3.jpg"/>
                    </div>
                    <div class="works-content">
                      <div class="quote"><span class="quote-left quotes"></span><blockquote><?php echo JText::_('COM_NRDS_AGENT3_QUOTE');?> </blockquote><span class="quote-right  quotes"></span></div>
                      <div class="work-name">Brett Ellis</div>
                      <div class="tagname"><?php echo JText::_('COM_NRDS_AGENT3_BROKER');?></div>
                    </div>
                  </li>
				  <li>
                    <div class="works-avatar">
                      <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/works-avatar4.jpg"/>
                    </div>
                    <div class="works-content">
                      <div class="quote"><span class="quote-left quotes"></span><blockquote> <?php echo JText::_('COM_NRDS_AGENT4_QUOTE');?></i> </blockquote><span class="quote-right  quotes"></span></div>
                      <div class="work-name">Gary Elminoufi</div>
                      <div class="tagname"><?php echo JText::_('COM_NRDS_AGENT4_BROKER');?></div>
                    </div>
                  </li>
                </ul>
                
            </div>
        </div>
    </div> <!-- End Agent Bridge Works -->
	
	

    <div class="row secure-referral-row"> <!-- Start Secure Referral -->
        <div class="wrapper-home">
            <h1><?php echo JText::_('COM_NRDS_EASYREFERRALS');?></h1>
            <span class="arrow-process"></span>
            <div class="columns">
                <div class="column-sm4">
                    <h2><?php echo JText::_('COM_NRDS_PENDINGREFERRAL');?></h2>
                    <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/referral1.jpg">
                </div>
                <div class="column-sm4">
                    <h2><?php echo JText::_('COM_NRDS_ACTIVEREFERRAL');?></h2>
                    <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images//referral2.jpg">

                </div>
                <div class="column-sm4">
                    <h2><?php echo JText::_('COM_NRDS_UNDERCONTRACT');?></h2>
                    <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/referral3.jpg">

                </div>
                <div class="column-sm4">
                    <h2><?php echo JText::_('COM_NRDS_CLOSEDREFERRAL');?></h2>
                    <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/referral4.jpg">

                </div>
            </div>
        </div>
    </div>  <!-- End Secure Referral -->

    <div class="row get-start-row"> <!-- Start Get Started -->
        <div class="wrapper">
            <h2><?php echo JText::_('COM_NRDS_GETSTARTED');?></h2>
            <p><?php echo JText::_('COM_NRDS_GETSTARTED_TEXT');?> </p>
            <p class="last-p"><?php echo JText::_('COM_NRDS_COMPLETETRANSACTIONS');?></p>
            <a <?php if (isset($_GET['useregid'])) { ?>			
					href="<?php echo JRoute::_('index.php?option=com_setpass&action=activated&useregid='. $_GET['useregid'].'&jgadeprasdium='.$_GET['jgadeprasdium'])?>"
				<?php } else { ?>
					href="<?php echo JRoute::_('index.php/component/nrds/?task=manual' )?>"
				<?php } ?> class="join-btn"><?php echo JText::_('COM_NRDS_BANNER_CLICKJOIN');?></a>
            <div class="contact-info">
                <span class="info-head1"><?php echo JText::_('COM_NRDS_NEEDHELP');?></span>
                <span class="info-head2"><?php echo JText::_('COM_NRDS_ANSWERPHONES');?></span>
                <p><?php echo JText::_('COM_NRDS_DEDICATEDTEAM');?></p>
                <div class="contacts"><?php echo JText::_('COM_NRDS_PHONENUMBER');?> <a href="mailto:support@agentbridge.com"><?php echo JText::_('COM_NRDS_EMAIL');?></a></div>
            </div>
        </div>
    </div> <!-- End Get Started -->

	
<script>


jQuery(document).ready(function(){
	jQuery(".swipebox").swipebox();
	jQuery('#slider1').DG_Slider({
	      auto: true, pager: true
	    });
	loadVideo();
});

function loadVideo() {
	var vid = jQuery("#vid").val();
	if (vid==1) 
        jQuery(".view-video").trigger("click");
}


</script>
