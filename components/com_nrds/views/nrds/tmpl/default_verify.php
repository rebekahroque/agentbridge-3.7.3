<?php

// No direct access

defined('_JEXEC') or die('Restricted access');

$result = $this->result;
?>
<div class="texture-overlay move-up"></div>
<div id="background"><img class="move-up" style="z-index:-1000" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/home-main-image4.jpg" /></div>
<form id="register_nrds" action="<?php echo JRoute::_('index.php?option=com_nrds&task=registerNRDS'); ?>"
	method="post" class="form-validate form-horizontal">
	<input type="hidden" name="jform[form]" value="nrds_registration" />

	<div class="full-radius wide-container account-setup">
		<!-- start account setup -->
		<div class="forms-head">
			<h2>AgentBridge Account Setup</h2>
			<p class="sub-head">Is this you?</p>
			<p>Please verify and update your information.</p>
		</div>
		<div class="user clear">
			<span class="user-thumb clear"> <img
				src="<?php echo $result->imagelink; ?>"
				alt="<?php echo $result->name; ?>" /> <input type="hidden"
				name="jform[imagelink]" value="<?php echo $result->imagelink; ?>" />
			</span> <span class="user-name"> <?php echo $result->name; ?> <input
				type="hidden" name="jform[name]"
				value="<?php echo $result->name; ?>" />
			</span> <em class="license"> <span>Licence:</span> <?php echo $result->licence; ?>
				<input type="hidden" name="jform[licence]"
				value="<?php echo $result->licence; ?>" />
			</em> <em class="em-pending hidden"> <span>Pending as of:</span> <?php
			preg_match('/(\d\d\d\d)-(\d\d)-(\d\d)/', $result->pending, $date);
			echo @date('F d, Y', mktime(0, 0, 0, $date[2], $date[3], $date[1]));
			?> <input type="hidden" name="jform[pending]"
				value="<?php echo $result->pending; ?>" />
			</em>
		</div>

		<div class="user-details">
			<div class="details-left left">Work Phone:</div>
			<div class="details-right left">
				<?php echo $result->wphone; ?>
				<input type="hidden" name="jform[wphone][value]" value="<?php echo $result->wphone;?>" />
				<input type="hidden" value="1" name="jform[wphone][main]"/>
				<input type="hidden" value="1" name="jform[wphone][show]"/>
			</div>

			<div class="details-left left">Work Fax:</div>
			<div class="details-right left">
				<?php echo $result->wfax; ?>
				<input type="hidden" name="jform[wfax][value]" value="<?php echo $result->wfax;?>" />
				<input type="hidden" value="1" name="jform[wfax][main]"/>
				<input type="hidden" value="1" name="jform[wfax][show]"/>
				<?php
				//echo isValid("/\([0-9]{1,}\) ?[0-9]+-[0-9]+/", $result->wfax, "wfax");
				?>
			</div>
			<div class="detail-full">
				<!--a href="#" class="text-link">Add Information</a-->
			</div>

			<div class="details-left left">Email:</div>
			<div class="details-right left">
				<a href="<?php echo $result->email; ?>" class="text-link-dark"><?php echo $result->email; ?>
				</a> <input type="hidden" name="jform[email]"
					value="<?php echo $result->email; ?>" />
			</div>

			<div class="details-left left">Blog:</div>
			<div class="details-right left">
				<a href="<?php echo $result->blog; ?>" class="text-link-dark"><?php echo $result->blog; ?>
				</a> <input type="hidden" name="jform[blog][value]"
					value="<?php echo $result->blog; ?>" />
					<input type="hidden" value="1" name="jform[blog][main]"/>
				<input type="hidden" value="1" name="jform[blog][show]"/>
			</div>

			<div class="details-left left">Address:</div>
			<div class="details-right left">
				<?php echo $result->waddress; ?>
				&nbsp; <input type="hidden" name="jform[waddress]"
					value="<?php echo $result->waddress; ?>" />

				<?php echo $result->suburb; ?>
				<input type="hidden" name="jform[suburb]"
					value="<?php echo $result->suburb; ?>" />

				<?php echo $result->city; ?>
				,&nbsp; <input type="hidden" name="jform[city]"
					value="<?php echo $result->city; ?>" />

				<?php echo $result->state; ?>
				&nbsp; <input type="hidden" name="jform[state]"
					value="<?php echo $result->state; ?>" />

				<?php echo $result->zip; ?>
				&nbsp; <input type="hidden" name="jform[zip]"
					value="<?php echo $result->zip; ?>" />

				<?php echo $result->country; ?>
				<input type="hidden" name="jform[country]"
					value="<?php echo $result->country; ?>" />
			</div>
			<div class="detail-full">
				<!--a href="#" class="text-link">Add New Address</a-->
			</div>


			<div class="details-left left">Social Site:</div>
			<div class="details-right left">
				<a href="<?php echo $result->socialsite; ?>" class="text-link-dark"><?php echo $result->socialsite; ?>
				</a> <input type="hidden" name="jform[socialsite][value]"
					value="<?php echo $result->socialsite; ?>" />
					<input type="hidden" value="1" name="jform[socialsite][main]"/>
				<input type="hidden" value="1" name="jform[socialsite][show]"/>
			</div>
			<div class="detail-full">
				<!--a href="#" class="text-link">Add Social Site</a-->
			</div>
		</div>

		<input type="hidden" name="jform[status]" value="1" />

		<div class="account-setup-bottom">
			<a
				id="confbut" href="javascript: void(document.getElementById('register_nrds').submit())"
				class="button gradient-blue left">Confirm Account Setup</a> <a
				href="index.php?notme=1" class="text-link left">This is not me</a>
		</div>
		
	</div>
	<!-- end account setup -->
</form>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<script>
window.onload = function(){
	//jQuery("#brokerageInput").on('focus', function(){ jQuery('body').scrollTop(jQuery("#registerButton").position().top); });
	//jQuery("#jform_straddress").on('focus', function(){ jQuery('body').scrollTop(jQuery("#jform_straddress").position().top); });
	//window.scrollTo(0,document.body.scrollHeight);
}
jQuery(document).ready(function(){
	document.getElementById('confbut').scrollIntoView(true);
	//window.scrollTo(0,document.body.scrollHeight);
});
</script>
