<?php

// No direct access

defined('_JEXEC') or die('Restricted access');
	if(isset($_GET['lang']) && $_GET['lang']!=""){

                        $language =& JFactory::getLanguage();

                        $extension = 'com_nrds';

                        $base_dir = JPATH_SITE;

                        $language_tag = $_GET['lang'];

                        $language->load($extension, $base_dir, $language_tag, true);

        

        } else {



                  $language = JFactory::getLanguage();

                  $extension = 'com_nrds';

                  $base_dir = JPATH_SITE;

                  $language_tag = "english-US";

                  $language->load($extension, $base_dir, $language_tag, true);


          }

?>


<script type="text/javascript">
mixpanel.track("Apply");
</script>

<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.Autocomplete.css" type="text/css" media="screen" charset="utf-8"/>


<script>
	var $ajax = jQuery.noConflict();
	function get_state(cID){
		$ajax("#state").html('');
		$ajax.ajax({
			url: '<?php echo $this->baseurl; ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID },
			success: function(e){
				$ajax("#state").html(e);	
				jQuery("#jform_state").select2({width: "200px"});
				jQuery("#s2id_jform_state a span").text(jQuery("#stateLabel").text());

			}
		});
	}
	
	function get_broker(b){
		var $country = $ajax("#jform_country").val();
		var $state = $ajax("#jform_state").val();
		var $city = $ajax("#jform_city").val();
		
		if(b.length > 0){ 
			$ajax("#broker").html("searching...");
			$ajax.ajax({
				url: '<?php echo $this->baseurl; ?>/custom/_get_broker.php',
				type: 'POST',
				data: { 'broker': b, 'cID': $country, 'state': $state, 'city': $city },
				success: function(e){
					$ajax("#broker").html(e);
				}
			});
		}
	}
	
</script>	
<style>
.outer-wrapper section.main-content{
	height: auto !important;
}
.countCode{
	    display: inline-block;
    float: left;
    margin-top: 30px;
    margin-right: 10px;
}
#s2id_citySelect{
	margin-top: 20px;
}
</style>

<div class="banner-row" style="padding-bottom:40px; color:#000000">
	<form 
		id="register_nrds"
		enctype="multipart/form-data"
		style="text-align:left"
	>

	<input type="hidden" name="jform[form]" value="nrds_registration" /> <input
		type="hidden" name="jform[manual]" value="true" />
	<?php 
		$session =& JFactory::getSession();
	?>

	<div class="full-radius wide-container account-setup">
		<!-- start account setup -->
		<div class="forms-head">
			<h3 style="font-size:24px; font-weight:bold; padding-bottom:20px; border-bottom:1px dotted #bfbfbf"><?php echo JText::_('COM_NRDS_APPLICATIONFORM');?></h3>
		</div>
		<div>
			<div class="account-setup-bottom">
				<br />
				<div class="acct_setup_holder">
				
					<label><?php echo JText::_('COM_NRDS_FNAME');?></label> 
					<input 
                    	onMouseover="ddrivetip('Enter your first name as it appears on your license');hover_dd()"
                        onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="jform_fname" 
						class="text-input" 
						name="jform[fname]"
						value="<?php 
							if($session->get( 'fname' )) { 
								echo $session->get( 'fname' ); 
							} else { 
								echo $_POST['jform']['fname']; 
							} 
							?>" 
					/> 
						
					<div class="clear-float"></div>
					<p class="jform_fname error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>
					
					<label><br/><?php echo JText::_('COM_NRDS_LNAME');?></label> 
					<input 
                    	onMouseover="ddrivetip('Enter your last name as it appears on your license');hover_dd()"
                        onMouseout="hideddrivetip();hover_dd()" 
						type="text" 
						id="jform_lname"
						class="text-input"
                        style="margin-top:20px"
						name="jform[lname]"
						value="<?php 
							if($session->get( 'lname' )) { 
								echo $session->get( 'lname' ); 
							} else { 
								echo $_POST['jform']['lname']; 
							} ?>" 
					/> 
					<div class="clear-float"></div>
					<p class="jform_lname error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>   <br /> <br /></p> 
					

					<label><br/><?php echo JText::_('COM_NRDS_GENDER');?></label> 
					<div style="width:auto;margin-top:20px">
						<span style="width: 80px;display: inline-block;"><input checked style="margin-right:10px;width: auto;display: inline-block;" type="radio" name="jform[gender]" value="Male"> <?php echo JText::_('COM_USERACTIV_HEADERMR');?> </span>
						<span style="width: 80px;display: inline-block;"><input style="margin-right:10px;width: auto;display: inline-block;" type="radio" name="jform[gender]" value="Female"> <?php echo JText::_('COM_USERACTIV_HEADERMRS');?> </span>
					</div>
					<div class="clear-float"></div>
								

					<label><br/><?php echo JText::_('COM_NRDS_LICENSENO');?></label> 
					<input 
                    	onMouseover="ddrivetip('AgentBridge is only open to currently licensed real estate professionals. <br />For verification purposes, please enter your real estate license number');hover_dd()"
                        onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="jform_licence"
						class="text-input" 
                        style="margin-top:20px"
						name="jform[licence]"
						value="<?php 
							if($session->get( 'licence' )) { 
								echo $session->get( 'licence' ); 
							} else { 
								echo $_POST['jform']['licence']; 
							} 
							?>"
					/> 
					
					<div class="clear-float"></div>
					<p class="jform_licence error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>   <br /> <br /></p> 
					
					<label><br/><?php echo JText::_('COM_NRDS_FORMEMAIL');?></label>
					<input
                    	onMouseover="ddrivetip('Enter your preferred email address');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="jform_email" 
						class="text-input"
                        style="margin-top:20px"
						name="jform[email]"
						value="<?php 
							if($session->get( 'email' )) { 
								echo $session->get( 'email' ); 
							} else { 
								echo $_POST['jform']['email']; 
							} 
						?>"
					/> 
					<div class="clear-float"></div>
					<p class="jform_email error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>   <br /> <br /></p>  
					
					<label class="clear-float"><br/><?php echo JText::_('COM_NRDS_FORMWORK');?></label>
					<span class="countCode">+1</span> 
					<input
                    	onMouseover="ddrivetip('Enter your work phone number');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text"
						id="jform_wphone" 
						class="text-input mobilenos numbermask2"
						name="jform[wphone][value]"
						style="width: 150px; margin-top:20px"
						value="<?php 
							if($session->get( 'wphone' )) { 
								echo $session->get( 'wphone' ); 
							} else { 
								echo $_POST['jform']['wphone'][0]; 
							} ?>"
						 
					/>
					<input type="hidden" value="1" name="jform[wphone][main]"/><input type="hidden" value="1" name="jform[wphone][show]"/> 
					
                    <div class="clear-float"></div>
					<p class="jform_wphone error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>   <br /> <br /></p>  
					
					<label class="clear-float "><br/><?php echo JText::_('COM_NRDS_FORMMOBILE');?> </label> 
					<span class="countCode">+1</span>
					<input type="text"
                   	 	onMouseover="ddrivetip('Enter your mobile phone number');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						id="jform_mphone" 
						class="text-input mobilenos numbermask"
						name="jform[mphone][value]"
						value="<?php 
							if($session->get( 'mphone' )) { 
								echo $session->get( 'mphone' ); 
							} else { 
								echo $_POST['jform']['mphone'][0]; 
							} 
						?>"
						style="width: 150px; margin-top:20px" />
					<input type="hidden" value="1" name="jform[mphone][main]"/><input type="hidden" value="1" name="jform[mphone][show]"/><br /> 
                    
					 <div class="clear-float"></div>
					<p class="jform_mphone error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>   <br /> <br /></p>  
					<div style="margin-top:20px">
					<label class="clear-float"><?php echo JText::_('COM_NRDS_FORMCOUNTRY');?></label>
						<select 
							id="jform_country" 
                            style="margin-top:20px" 
							name="jform[country]"
							>
						<option value="">-<?php echo JText::_('COM_USERPROF_CC_SELCTRY');?>-</option>
						<?php 
						
						foreach($this->countries as $cArr){
						
								echo "<option data=\"".$cArr->countries_iso_code_2."\" value=\"".$cArr->countries_id."\" >".$cArr->countries_name."</option>";
							}
							?>
						</select> <br /> 
                         </div>
                         
                         
					<label class="clear-float"><br/><?php echo JText::_('COM_NRDS_FORMZIP');?></label> 
					<input
                    	onMouseover="ddrivetip('Enter Brokerage Zip Code');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						maxlength=5
						id="jform_zip" 
						class="text-input" 
						style="width: 100px; margin-top:20px" 
						name="jform[zip]"
						value="<?php 
							if($session->get( 'zip' )) { 
								echo $session->get( 'zip' ); 
							} else { 
								echo $_POST['jform']['zip']; 
							} ?>"
						
					/>
                   	<div class="clear-float"></div>
					<p class="jform_zip error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>  
					<p class="jform_zip_2 error_msg" style="margin-left:100px;display:none; font-size:11px"> Zip code must be at least 5 digits <br /> <br /></p> 
					<p class="jform_zip_ca error_msg" style="margin-left:100px;display:none; font-size:11px"> Invalid Zip Code <br /> <br /></p>					
					
					<label class="clear-float"><br/><?php echo JText::_('COM_NRDS_ADDRESS1');?></label> 
					<input
                    	onMouseover="ddrivetip('Enter Brokerage Address');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="jform_straddress" 
						class="text-input"
                        style="margin-top:20px"
						name="jform[straddress]"
						value="<?php 
							if($session->get( 'straddress' )) { 
								echo $session->get( 'straddress' ); 
							} else { 
								echo $_POST['jform']['straddress']; 
							} 
							?>"
					/> 
                   	<div class="clear-float"></div>
					<p class="jform_straddress error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>   
					
					<label><br/><?php echo JText::_('COM_NRDS_ADDRESS2');?></label> 
					<input 
                    	onMouseover="ddrivetip('Enter Brokerage Address');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="jform_suburb" 
						class="text-input"
                        style="margin-top:20px"
						name="jform[suburb]"
						value="<?php 
							if($session->get( 'suburb' )) { 
								echo $session->get( 'suburb' ); 
							} else { 
								echo $_POST['jform']['suburb']; 
						} ?>"
					/> 
					<br />
					
					<label id="cityLabel" class="clear-float"><br /><?php echo JText::_('COM_NRDS_FORMCITY');?></label> 
					
					<input type="text"
                    	onMouseover="ddrivetip('Enter Brokerage City');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						id="jform_city" 
						class="text-input" 
                        style="margin-top:20px"
						name="jform[city]"
						value="<?php 
							if($session->get( 'city' )) { 
								echo $session->get( 'city' ); 
							} else { 
								echo $_POST['jform']['city']; 
						} ?>"
					/> 
					<div class="clear-float"></div>
					<p class="jform_city error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p> 
					
					<label class="clear-float" id="stateLabel"><br/><?php echo JText::_('COM_NRDS_FORMSTATE');?></label>
				    	<div id="state" style="margin-top:15px;margin-left:0" class="state_dropdown">
						<input 
                        	onMouseover="ddrivetip('State you are licensed in');hover_dd()"
							onMouseout="hideddrivetip();hover_dd()"
							type="text" 
							id="jform_state" 
                            style="margin-top:20px"
							class="text-input"
						    name="jform[state]"
							value="<?php echo $_POST['jform']['state'] ?>" />
			
						</div>
						<div>
							<div class="clear-float"></div>
							<p class="jform_state error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p> 
						</div>
			
					<label class="clear-float"><br/><br/><?php echo JText::_('COM_NRDS_FORMBROKER');?></label>
					<div id="holder-nrds" class="text-input-small-nrds" style="margin-top:20px">
						<div class="clear">
							<ul id="selecteddesigs" style="float: left">
							</ul>
							<input type="text" id="brokerageInput" style="float: left"
                            	onMouseover="ddrivetip('Select from our list of Brokerage names. Type in the first few letters <br/>and we will find a match.');hover_dd()"
								onMouseout="hideddrivetip();hover_dd()"
								placeholder="<?php echo JText::_('COM_NRDS_FORMAUTO');?>"
                                class="text-input" /> <img id="imageloading"
								style="padding-top: 15px; position: relative; left: -20px; display: none;"
								src="<?php echo $this->baseurl."/images/ajax-loader.gif"?>" />
						</div>
					</div>
				
					
							<div class="clear-float"></div>
							<p class="brokerageInput error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> </p> 
						
				
                    <div style="clear: both;"></div>
                    <div class="clear" style="margin-top:10px">
					<label style='margin-top:0; line-height:16px'><br/><?php echo JText::_('COM_NRDS_FORMBROKERLICENSE');?></label>
					<input
                    	onMouseover="ddrivetip('Enter Brokerage license number');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="brokerage_license" 
						class="text-input"
                        style="margin-top:20px"
						name="jform[brokerage_license]"
						value="<?php 
							if($session->get( 'brokerage_license' )) { 
								echo $session->get( 'brokerage_license' ); 
							} else { 
								echo $_POST['jform']['brokerage_license']; 
							} ?>"
					/>
					<div class="clear-float"></div>
					<p class="brokerage_license error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>
					</div>
                    <div style="clear: both;"></div>
					<!--
                    <div class="clear" style="margin-top:10px">
					<label style='margin-top:0; line-height:16px'>Average Sale Price </label>
					
					
					<input 
						type="text" 
						id="average_price" 
						class="required text-input pricevalue numberperiodcomma" type="text" placeholder="$0" required="required"
						name="jform[average_price]"
						value="<?php 
							if($session->get( 'average_price' )) { 
								echo $session->get( 'average_price' ); 
							} else { 
								echo $_POST['jform']['average_price']; 
							} 
						?>" 
						
						
					/> 
					
						<br /></div>
					-->
					<div id="s2016" class="clear text-link" style="margin-top:15px;float:left; font-size:12px" onclick="show2016()"><strong>+2016</strong></div>
					<div id="h2016" class="clear text-link" style="margin-top:15px;float:left; font-size:12px; display:none" onclick="hide2016()"><strong>-2016</strong></div>
					<div style="display:none" id="2016">
                    <div class="clear" style="margin-top:5px; clear:both">
					<label style='line-height:16px'><br/><?php echo JText::_('COM_NRDS_FORMTOTALVOLUME');?></label>
					<input 
                    	onMouseover="ddrivetip('Enter your total sales volume closed in the indicated year');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="volume_2016"
						class="text-input pricevalue numberperiodcomma" 
                        style="margin-top:20px"
						type="text" 
						placeholder="$0 USD"
						name="jform[volume_2016]"
						value="<?php 
							if($session->get( 'volume_2016' )) { 
								echo $session->get('volume_2016'); 
							} else { 
								echo $_POST['jform']['volume_2016']; 
							} ?>" /> 
				
						</div>	
						
					<label style='line-height:16px'><br/><?php echo JText::_('COM_NRDS_FORMTOTALSIDES');?></label>
					<input
                    	onMouseover="ddrivetip('Enter your total number of transactions closed in the indicated year');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="sides_2016" 
                        style="margin-top:20px"
						class="text-input  numberperiodcomma" 
						type="text" 
						placeholder="0"
						name="jform[sides_2016]"
						value="<?php 
							if($session->get( 'sides_2016' )) { 
								echo $session->get( 'sides_2016' ); 
							} else { 
								echo $_POST['jform']['sides_2016']; 
							} ?>" /> 
					
					</div>
					<div style="clear: both;"></div>
					<div style="clear: both; margin-top:20px"></div>
                    <div class="clear" style="margin-top:15px"><strong>2015</strong></div>

                    <div class="clear" style="margin-top:5px">
					<label style='line-height:16px'><br/><?php echo JText::_('COM_NRDS_FORMTOTALVOLUME');?></label>
					<input 
                    	onMouseover="ddrivetip('Enter your total sales volume closed in the indicated year');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="volume_2015"
						class="text-input pricevalue numberperiodcomma" 
                        style="margin-top:20px"
						type="text" 
						placeholder="$0 USD"
						name="jform[volume_2015]"
						value="<?php 
							if($session->get( 'volume_2015' )) { 
								echo $session->get('volume_2015'); 
							} else { 
								echo $_POST['jform']['volume_2015']; 
							} ?>" 
						/> 
						
						<div class="clear-float"></div>
						<p class="total_volume_4 error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>
						<p class="total_volume_4_zero error_msg" style="margin-left:100px;display:none; font-size:11px"> Please enter a valid Sales Volume for your application<br/> to be considered. <br /> <br /></p>
						</div>					

					<label style='line-height:16px'><br/><?php echo JText::_('COM_NRDS_FORMTOTALSIDES');?></label>
					<input
                    	onMouseover="ddrivetip('Enter your total number of transactions closed in the indicated year');hover_dd()"
						onMouseout="hideddrivetip();hover_dd()"
						type="text" 
						id="sides_2015" 
                        style="margin-top:20px"
						class="text-input pricevalue numberperiodcomma" 
						type="text" 
						placeholder="0"
						name="jform[sides_2015]"
						value="<?php 
							if($session->get( 'sides_2015' )) { 
								echo $session->get( 'sides_2015' ); 
							} else { 
								echo $_POST['jform']['sides_2015']; 
							} ?>" 
					/> 
					
					<div class="clear-float"></div>
					<p class="total_sides_4 error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMERROR');?>  <br /> <br /></p>
					<p class="total_sides_4_zero error_msg" style="margin-left:100px;display:none; font-size:11px"> <?php echo JText::_('COM_NRDS_FORMVOLUMEERROR');?> <br /> <br /></p>
					
					<input type="hidden" name="jform[verified_2016]" value=0 />
					<input type="hidden" name="jform[verified_2015]" value=0 />
					</div>
                

				</div>
			</div>
		
		<div class="apply-btn-form">
	
			<input 
				id="submit-register" 
				type="submit"
				class="submit-btn left" 
				style="padding-top:5px; height:30px; width:140px"
				value="<?php echo JText::_('COM_NRDS_FORMAPPLY');?>"				
			/>
			<a href="" class="text-link left" style="padding-top:10px; padding-left:10px; padding-right:10px"> <?php echo JText::_('COM_NRDS_FORMCANCEL');?></a>
            <img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
		</div>
		<div class="clear-float"></div> <div class="clear-float"></div> 
		</div>
	</div>
	<!-- end account setup -->

</form>
</div>


<script>



function show2016() {
	jQuery("#2016").show();
	jQuery("#s2016").hide();
	jQuery("#h2016").show();
}

function hide2016() {
	jQuery("#2016").hide();
	jQuery("#s2016").show();
	jQuery("#h2016").hide();
}

function removeItemB(id){
	jQuery("#"+id).remove();
    jQuery("#brokerageInput").show();
	//console.log(id);
}

function set_masks(){

	jQuery(".numbermask").inputmask({mask:"(999) 999-9999","clearIncomplete":true});
	jQuery(".numbermask2").inputmask({mask:"(999) 999-9999","clearIncomplete":true});

}

function keyUpFunc(e) {
  if (e.keyCode == 27) { functionZzy(); }
}

function reverse(s) {
  var o = '';
  for (var i = s.length - 1; i >= 0; i--)
    o += s[i];
  return o;
}



jQuery(document).ready(function(){

	jQuery(".footer").css("display","none");


	jQuery("#volume_2016").autoNumeric('init', {aSign:'$', mDec: '0'});
	jQuery("#volume_2015").autoNumeric('init', {aSign:'$', mDec: '0'});
	
	jQuery("#sides_2016").autoNumeric('init', {mDec: '0', vMax: '9999'});
	jQuery("#sides_2015").autoNumeric('init', {mDec: '0', vMax: '9999'});
	
	jQuery("#sides_2015").focus(function(){jQuery("#sides_2015").val('');});
	jQuery("#volume_2015").focus(function(){jQuery("#volume_2015").val('');});
	jQuery("#sides_2016").focus(function(){jQuery("#sides_2016").val('');});
	jQuery("#volume_2016").focus(function(){jQuery("#volume_2016").val('');});


	set_masks();
	
	jQuery("#holder").click(function(){
		jQuery("#brokerageInput").focus();
	});
	

	jQuery("#jform_image").change(function(){
		var filename= jQuery(this).val().replace("C:\\fakepath\\", "");
		jQuery("#fakefile").val(filename);
		var i;
		var len = this.files.length;
		var	formdata = false;
		if(window.FormData){
			formdata = new FormData();
		}
		for	(i=0	;	i	<	len;	i++	)	{
			file	=	this.files[i];
			if	(!!file.type.match(/image.*/))	{
				if	(	window.FileReader	)	{
					reader	=	new	FileReader();
					reader.onloadend	=	function	(e)	{	
					//showUploadedItem(e.target.result,	file.fileName);
					};
					reader.readAsDataURL(file);
				}
				if	(formdata)	{
					formdata.append("image", file);
				}
				jQuery.ajax({
					url:	"../../../file_upload_dus.php",
					type:	"POST",
					data:	formdata,
					processData:	false,
					contentType:	false,
					success:	function	(response)	{
						imgsrc = "<?php echo $this->baseurl; ?>/uploads/"+response;
						jQuery('#selectedimage').attr('src',imgsrc);
					}
				});
			}
			else{
				alert('Not a vaild image!');
			}		
		}
	});




	jQuery("#jform_country").val("223");

	jQuery("#brokerageInput").on('focus', function(){ document.getElementById('registerButton').scrollIntoView(true); })//jQuery('body').scrollTop(jQuery("#registerButton").position().top); });


	
	var sel_c='US';
	var state_variable = "adminCode1";

		
	get_state(223);

	var removethis = " USD";
	var addthis = " USD";

	var baseurl = "<?php echo $this->baseurl?>";


	
	


	jQuery(".submit-btn").click(function(){

    	jQuery("#volume_2015").val(jQuery("#volume_2015").val().replace(removethis, ""));
		jQuery("#volume_2016").val(jQuery("#volume_2016").val().replace(removethis, ""));
    	
		jQuery("#volume_2015").val(jQuery("#volume_2015").autoNumeric('get'));
		jQuery("#volume_2016").val(jQuery("#volume_2016").autoNumeric('get'));
	
	});

	applyAddressData(baseurl,sel_c,jQuery("#jform_country").val());

	jQuery("#jform_country").change(function(){


			get_state(jQuery(this).val());

			jQuery("#jform_zip").unbind("keyup");
			jQuery("#jform_zip").val("");
			jQuery('#jform_zip').inputmask("remove");
			jQuery('#jform_city').val("");
			
			jQuery('.numbermask').val("");
			jQuery('.numbermask2').val("");

			jQuery('.numbermask').inputmask("remove");
			jQuery('.numbermask2').inputmask("remove");


			sel_c = jQuery(this).find(':selected').attr('data');

			console.log(sel_c);
				
			applyAddressData(baseurl,sel_c,jQuery(this).val());

	});
	
	jQuery("#brokerageInput").autocomplete({
		source: function( request, response ) {
			autoFocus: true,
	        jQuery.ajax({
	        	url: '<?php echo $this->baseurl; ?>/custom/_get_broker.php',
				type: 'POST',
				data: {
					'broker': jQuery("#brokerageInput").val(),
					'cID': jQuery("#jform_country").val(),
					'state': jQuery("#jform_state").val(),
					'city': jQuery("#jform_city").val()
				},
	          success: function( data ) {
		          console.log("data is: "+data);
		          var temp = jQuery.parseJSON(data);
		         	if(data!=""){
			        	response( jQuery.map( temp.brokers , function( item ) {
			        		return item;
			        	}));
	          		}
					else{
						console.log('ola');
					}
	          }
	        });
	      },
	    minLength: 1,
	    open: function( event, ui ) {
  			jQuery("#imageloading").hide();
		    jQuery(".ui-autocomplete").css('width', jQuery("#holder").width()+"!important");
		    jQuery(".ui-autocomplete").css('left', jQuery("#holder").position().left+"!important");
	    },
	    search: function( event, ui ) {
			jQuery("#imageloading").show();
					
		},
	    select: function(event, ui) {
	    	var terms = split( this.value );
	          // remove the current input
	          terms.pop();
	          // add the selected item
	          terms.push( ui.item.label );
	          // add placeholder to get the comma-and-space at the end
	          terms.push( "" );
	          this.value = terms.join( ", " );
			console.log(ui);
			html="<li id=\"listitem"+ui.item.value+"\" class=\"topseven textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable \">"+ui.item.label+"<a href=\"javascript:void(0)\" onclick=removeItemB(\"listitem"+ui.item.value+"\") class=\"textboxlist-bit-box-deletebutton\"></a>";
			html+="<input type=\"hidden\" required=\"required\" value=\""+ui.item.value+"\" name=\"jform[brokerage]\"/></li>";
			jQuery("#holder").prepend();
	        jQuery("#selecteddesigs").append(html); 
	        jQuery("#brokerageInput").val('');
	        jQuery("#brokerageInput").hide();
	        return false;
	    }
	});
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}

	function jsonpCallback(data){
		console.log(data);
		if(data.postalcodes[0].adminCode1){
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		jQuery("#jform_city").val(data.postalcodes[0].placeName);
		} else {
			//alert("Invalid Zip");
			jQuery("#jform_zip").val("");
		}
	}

});
</script>
