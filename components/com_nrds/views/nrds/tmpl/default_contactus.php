<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.formvalidation');
	jimport('joomla.application.component.controller');
	jimport('joomla.user.helper');
	if(isset($_GET['lang']) && $_GET['lang']!=""){

                        $language =& JFactory::getLanguage();

                        $extension = 'com_nrds';

                        $base_dir = JPATH_SITE;

                        $language_tag = $_GET['lang'];

                        $language->load($extension, $base_dir, $language_tag, true);

        

        } else {



                  $language = JFactory::getLanguage();

                  $extension = 'com_nrds';

                  $base_dir = JPATH_SITE;

                  $language_tag = "english-US";

                  $language->load($extension, $base_dir, $language_tag, true);


          }
?>
<link type="text/css" href="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/css/jplayer-skin/jplayer.agentbridge.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/scripts/jplayer/jquery.jplayer.min.js"/></script>
<script type="text/javascript">
var $ = jQuery;
    $(document).ready(function(){
        <?php foreach($this->video_datas as $i=>$data){?>
          $("#jquery_jplayer_<?php echo $data->id ?>").jPlayer({
            ready: function () {
              $(this).jPlayer("setMedia", {
                title: "<?php echo $data->video_name;?>",
                //mp4: "/test/media/the.mp4",
                m4v: "<?php echo str_replace('/administrator', "", $this->baseurl); ?>/videos/us/<?php echo $data->video_url;?>",
              //  ogv: "http://www.jplayer.org/video/ogv/Big_Buck_Bunny_Trailer.ogv",
              poster: "<?php echo $this->baseurl; ?>/videos/us/<?php echo $data->poster;?>"
              });
            },
            cssSelectorAncestor: '#jp_container_<?php echo $data->id ?>',
            play: function() { // To avoid multiple jPlayers playing together.
                $(this).jPlayer("pauseOthers");
            },
            swfPath: '<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/scripts/jplayer',
            supplied: "m4v, ogv",
            size: { width: "750px", height: "423px"}
          });

              $("#jquery_jplayer_<?php echo $data->id ?>").on("click",function(){
                $("#jquery_jplayer_<?php echo $data->id ?>").jPlayer("pause");
                $("#jp_container_<?php echo $data->id ?> .jp-video-play").css("display","block");
              });

              $("#jp_container_<?php echo $data->id ?> .jp-video-play").on("click",function(){
                 $("#jquery_jplayer_<?php echo $data->id ?>").jPlayer("play");
                $("#jp_container_<?php echo $data->id ?> .jp-video-play").css("display","none");
              });

                $("#jp_container_<?php echo $data->id ?> .jp-interface").css("display","none");
        <?php }?>
               });
  </script>
<style>
div.jp-video-270p div.jp-video-play {
margin-top: -413px;
height: 413px;
}
div.jp-video div.jp-interface {
border-top: 0; 
}

video{
    border:1px solid #dedede
}
</style>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/lib/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/src/js/jquery.swipebox.js"/></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/swipebox/src/css/swipebox.css"/>
    <div class="row faq-row"> <!-- Start FAQ Row -->
        <div class="wrapper">
		   
		   <div id="referraltable" style="display:none; font-size:14px" class="referraltable">
		   <h2 style="margin-bottom:20px; padding-bottom:10px; border-bottom:1px solid #dbdbdb"><?php echo JText::_('COM_NRDS_AGNT2AGNTREF');?></h2>
		   <p><?php echo $this->fee[0]->ref_parag ?><br/><br/></p>
		   <p><?php echo JText::_('COM_NRDS_REFCHARGES');?><br/>
		     <div class="feetable">
				<div class="feetitle left">
					<div class="feerowsuba clear-float" ><strong><?php echo JText::_('COM_NRDS_POPSSALEP');?></strong></div>
					<div class="feerowsuba clear-float" style="text-align:left; padding-left:10px" ><?php echo "$".number_format($this->fee[0]->tier1)." - $".number_format($this->fee[0]->tier2) ?></div>
					<div class="feerowsuba clear-float" style="text-align:left; padding-left:10px"><?php echo "$".number_format($this->fee[1]->tier1)." - $".number_format($this->fee[1]->tier2) ?></div>
					<div class="feerowsuba clear-float" style="text-align:left; padding-left:10px"><?php echo "$".number_format($this->fee[2]->tier1)." - $".number_format($this->fee[2]->tier2) ?></div>
					<div class="feerowsuba clear-float" style="text-align:left; padding-left:10px"><?php echo "$".number_format($this->fee[3]->tier1)." - $".number_format($this->fee[3]->tier2) ?></div>
					<div class="feerowsubc clear-float" style="text-align:left; padding-left:10px"><?php echo "$".number_format($this->fee[4]->tier1)." - $".number_format($this->fee[4]->tier2) ?></div>
				</div>
				<div class="feerowa left">
					<div class="feerowsub clear-float" ><strong><?php echo JText::_('COM_NRDS_SAGENTFEE');?></strong></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[0]->r1_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[1]->r1_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[2]->r1_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[3]->r1_fee) ?></div>
					<div class="feerowsubd clear-float" ><?php echo "$".number_format($this->fee[4]->r1_fee) ?></div>
				</div>
				<div class="feerowb left">
					<div class="feerowsub clear-float" ><strong><?php echo JText::_('COM_NRDS_RAGENTFEE');?></strong></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[0]->r2_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[1]->r2_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[2]->r2_fee) ?></div>
					<div class="feerowsub clear-float" ><?php echo "$".number_format($this->fee[3]->r2_fee) ?></div>
					<div class="feerowsubd clear-float" ><?php echo "$".number_format($this->fee[4]->r2_fee) ?></div>
				</div>
				</div>
				</p>
		   </div>
		   
		   <div id="faqtable">
            <div class="column">
                <div class="column-m3">
                    <div class="support-head" id="main">
                        <h2><?php echo JText::_('COM_NRDS_SUPPORTOPICS');?></h2>
                    </div>
                    <div class="support-content topics">
                        <ul>
                            <li id="nav1" class="notactive" style="padding-top:20px"><?php echo JText::_('COM_NRDS_ADDINGPOPS');?></li>
							<li id="nav2" class="notactive"><?php echo JText::_('COM_NRDS_HDOESITWORK');?></li>
                            <li id="nav3" class="notactive"><?php echo JText::_('COM_NRDS_CONTACTREFERRAL');?></li>
                            <li id="nav4" class="notactive"><?php echo JText::_('COM_NRDS_MYNETWORK');?></li>
                            <li id="nav5" class="notactive"><?php echo JText::_('COM_NRDS_PRIVACY');?></li>
							<li id="nav6" class="notactive"><?php echo JText::_('COM_NRDS_CONTACTREGISTRATION');?></li>
							<li id="nav7" class="text-link"><?php echo JText::_('COM_NRDS_CONTACTREFFEES');?></li>
                        </ul>
                    </div>
                </div>
                <div class="column-m3" id="helpyou">
                    <div class="help-head">
                        <h2><?php echo JText::_('COM_NRDS_WANT2HELP');?></h2>
                    </div>
                    <div class="support-content help">
                        <ul>
                           <li onclick="hy1()" id="seehy1" style="padding-top:20px"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q1');?></a></li>
						   <li onclick="hidehy1()" id="hidehy1" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q1');?></a></li>
						   <div id="hy1" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A1');?><br/></div>
						   <li onclick="hy2()" id="seehy2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q2');?></a></li>
						   <li onclick="hidehy2()" id="hidehy2" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q2');?></a></li>
						   <div id="hy2" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A2');?></div>
						   <li onclick="hy3()" id="seehy3"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q3');?></a></li>
						   <li onclick="hidehy3()" id="hidehy3" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q3');?></a></li>
						   <div id="hy3" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A3');?> </div>
						   <li onclick="hy4()" id="seehy4"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q4');?></a></li>
						   <li onclick="hidehy4()" id="hidehy4" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q4');?></a></li>
						   <div id="hy4" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A4');?> </div>
						   <li onclick="hy5()" id="seehy5"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q5');?></a></li>
						   <li onclick="hidehy5()" id="hidehy5" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q5');?></a></li>
						   <div id="hy5" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A5');?></div>
						   <li onclick="hy6()" id="seehy6"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_WANT2HELP_Q6');?></a></li>
						   <li onclick="hidehy6()" id="hidehy6" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_WANT2HELP_Q6');?></a></li>
						   <div id="hy6" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_WANT2HELP_A6');?></div>

                        </ul>
                    </div>
                </div>
                <div class="column-m3" id="quickfixes">
                    <div class="quick-head">
                        <h2><?php echo JText::_('COM_NRDS_QF');?></h2>
                    </div>
                    <div class="support-content fixes">
                        <ul>
                            <li onclick="qf1()" id="seeqf1" style="padding-top:20px"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_QF_Q1');?></a></li>
							<li onclick="hideqf1()" id="hideqf1" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_QF_Q1');?></a></li>
							<div id="qf1" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A1');?> </div>
                            <li onclick="qf2()" id="seeqf2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_QF_Q2');?></a></li>
							<li onclick="hideqf2()" id="hideqf2" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_QF_Q2');?></a></li>
							<div id="qf2" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A2');?></div>
							<li onclick="qf3()" id="seeqf3"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_QF_Q3');?></a></li>
							<li onclick="hideqf3()" id="hideqf3" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_QF_Q3');?></a></li>
							<div id="qf3" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A3');?></div>
							<li onclick="qf4()" id="seeqf4"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_QF_Q4');?></li>
							<li onclick="hideqf4()" id="hideqf4" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_QF_Q4');?></a></li>
							<div id="qf4" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A4');?> </div>
							<li onclick="qf5()" id="seeqf5"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_QF_Q5');?></li>
							<li onclick="hideqf5()" id="hideqf5" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_QF_Q5');?></a></li>
							<div id="qf5" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A5');?></div>
							<li onclick="qf6()" id="seeqf6"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_QF_Q6');?></li>
							<li onclick="hideqf6()" id="hideqf6" style="padding-top:20px; display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_QF_Q6');?></a></li>
							<div id="qf6" class="answer2 clear-float" style="display:none"><?php echo JText::_('COM_NRDS_QF_A6');?></div>

					   </ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="addpops" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_AP');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
							<li onclick="ap1()" id="seeap1"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_AP_Q1');?></a></li>
							<li onclick="hideap1()" id="hideap1" style="display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_AP_Q1');?></a></li>
							<div id="ap1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_AP_A1');?><br/><br/></div>
                            <li onclick="ap2()" id="seeap2"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_AP_Q2');?></a></li>
							<li onclick="hideap2()" id="hideap2" style="display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_AP_Q2');?></a></li>
							<div id="ap2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_AP_A2');?><br/><br/></div>
							<li onclick="ap3()" id="seeap3"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_AP_Q3');?></a></li>
							<li onclick="hideap3()" id="hideap3" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_AP_Q3');?></a></li>
							<div id="ap3" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_AP_A3');?><br/><br/></div>
							<li onclick="ap4()" id="seeap4"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_AP_Q4');?></a></li>
							<li onclick="hideap4()" id="hideap4" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_AP_Q4');?></a></li>
							<div id="ap4" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_AP_A4');?><br/><br/></div>
						</ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="howto" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_HDOESITWORK');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
                            <li onclick="hw1()" id="seehw1"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_HW_Q1');?></a></li>
							<li onclick="hidehw1()" id="hidehw1" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_HW_Q1');?></a></li>
							<div id="hw1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_HW_A1');?><br/><br/></div>
                            <li onclick="hw2()" id="seehw2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_HW_Q2');?></a></li>
							<li onclick="hidehw2()" id="hidehw2" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_HW_Q2');?></a></li>
							<div id="hw2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_HW_A2');?> <br/><br/></div>
							<li onclick="hw3()" id="seehw3"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_HW_Q3');?></a></li>
							<li onclick="hidehw3()" id="hidehw3" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_HW_Q3');?></a></li>
							<div id="hw3" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_HW_A3');?> <br/><br/></div>
							<li onclick="hw4()" id="seehw4"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_HW_Q4');?></a></li>
							<li onclick="hidehw4()" id="hidehw4" style="display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_HW_Q4');?></a></li>
							<div id="hw4" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_HW_A4');?> <br/><br/></div>

						</ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="referrals" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_CONTACTREFERRAL');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
                            <li onclick="ref1()" id="seeref1"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_REF_Q1');?></a></li>
							<li onclick="hideref1()" id="hideref1" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_REF_Q1');?></a></li>
							<div id="ref1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_REF_A1');?><br><br/></div>
                            <li onclick="ref2()" id="seeref2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_REF_Q2');?></a></li>
							<li onclick="hideref2()" id="hideref2" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_REF_Q2');?></a></li>
							<div id="ref2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_REF_A2');?> <br/><br/></div>
                            <li onclick="ref3()" id="seeref3"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_REF_Q3');?></a></li>
							<li onclick="hideref3()" id="hideref3" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_REF_Q3');?></a></li>
							<div id="ref3" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_REF_A3');?><br/><br/></div>
							<li onclick="ref4()" id="seeref4"><a href="javascript:void(0)" class="h-text-link">+<?php echo JText::_('COM_NRDS_REF_Q4');?></a></li>
							<li onclick="hideref4()" id="hideref4" style="display:none"><a href="javascript:void(0)" class="h-text-link">-<?php echo JText::_('COM_NRDS_REF_Q4');?></a></li>
							<div id="ref4" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_REF_A4');?><br/><br/></div>

						</ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="network" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_MYNETWORK');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
                            <li onclick="net1()" id="seenet1"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_MN_Q1');?></a></li>
							<li onclick="hidenet1()" id="hidenet1" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_MN_Q1');?></a></li>
							<div id="net1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_MN_A1');?> <br><br/></div>
                            <li onclick="net2()" id="seenet2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_MN_Q2');?></a></li>
							<li onclick="hidenet2()" id="hidenet2" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_MN_Q2');?></a></li>
							<div id="net2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_MN_A2');?><br/><br/></div>
                        </ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="privacy" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_PRIVACY');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
                            <li onclick="priv1()" id="seepriv1"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_PV_Q1');?></a></li>
							<li onclick="hidepriv1()" id="hidepriv1" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_PV_Q1');?></a></li>
							<div id="priv1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_PV_A1');?><br/><br/></div>
                            <li onclick="priv2()" id="seepriv2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_PV_Q2');?></a></li>
							<li onclick="hidepriv2()" id="hidepriv2" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_PV_Q2');?></a></li>
							<div id="priv2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_PV_A2');?> <br/><br/></div>
                        </ul>
                    </div>
                </div>
				<div class="column-contact-m3" id="registration" style="display:none">
                    <div class="help-head-sub">
                        <h2><?php echo JText::_('COM_NRDS_CONTACTREGISTRATION');?></h2>
                    </div>
                    <div class="support-content-sub help-sub">
                        <ul>
                            <li onclick="reg1()" id="seereg1"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_RG_Q1');?></a></li>
							<li onclick="hidereg1()" id="hidereg1" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_RG_Q1');?></a></li>
							<div id="reg1" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_RG_A1');?> <br/><br/></div>
                            <li onclick="reg2()" id="seereg2"><a href="javascript:void(0)" class="text-link">+<?php echo JText::_('COM_NRDS_RG_Q2');?></a></li>
							<li onclick="hidereg2()" id="hidereg2" style="display:none"><a href="javascript:void(0)" class="text-link">-<?php echo JText::_('COM_NRDS_RG_Q2');?></a></li>
							<div id="reg2" style="display:none" class="answer clear-float"><?php echo JText::_('COM_NRDS_RG_A2');?><br/><br/></div>
                        </ul>
                    </div>
                </div>
            </div>
		</div>
      </div>
   </div> <!-- End FAQ Row -->

    <div class="row other-features-row"> <!-- Start Other Features -->
        <div class="wrapper">
            <ul class="other-feature-nav">
                <li>
                    <a class="watch" href="#video-tutorial">
                        <span class="icon watch-icon"></span>
                        <span class="label"><?php echo JText::_('COM_NRDS_WATCH_VID');?></span>
                    </a>
                </li>
                <li>
                    <a class="feature" href="mailto:support@agentbridge.com?Subject=I%20would%20like%20to%20suggest..." target="_top">
                        <span class="icon feature-icon"></span>
                        <span class="label"><?php echo JText::_('COM_NRDS_SUGGEST');?></span>
                    </a>
                </li>
                <li>
                    <a class="agent" href="mailto:?Subject=I%20would%20like%20to%20invite%20you%20to%20join%20AgentBridge...&body=Dear%20%20%20%20,%0A%0AI%20want%20to%20share%20my%20pocket%20listings%20with%20you%20via%20AgentBridge,%20the%20most%20powerful%20global%20network%20of%20real%20estate%20professionals.%20By%20agents%20and%20for%20agents,%20AgentBridge%20provides%20us%20a%20secure%20place%20to%20privately%20share%20information%20on%20properties%20and%20clients.%20You%20always%20have%20total%20control%20of%20who%20views%20your%20private%20information.%20%0A%0ARepresenting%20the%20best%20of%20the%20best,%20the%20membership%20of%20AgentBridge%20transacted%20over%20$100%20Billion%20in%20deals%20last%20year.%20<br/><br/>
I%20hope%20you%20will%20choose%20to%20join%20me.%20We%20need%20this.%0A%0ASincerely," target="_top">
                        <span class="icon agent-icon"></span>
                        <span class="label"><?php echo JText::_('COM_NRDS_WELC_AGENT');?></span>
                    </a>
                </li>
                <li>
                    <a class="stories" href="mailto:support@agentbridge.com?Subject=I%20would%20like%20to%20share%20my%20success%20story..." target="_top">
                        <span class="icon story-icon"></span>
                        <span class="label"><?php echo JText::_('COM_NRDS_SHARE');?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div> <!-- End Other Features -->

    <div class="row video-dropdown">
        <div class="wrapper">
            <ul class="video-list">
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-thumb.jpg">
                        <span class="play-icon"></span>
                        <span class="video-title"><?php echo JText::_('COM_NRDS_GETSTART');?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row tutorial-row"> <!-- Start Video Tutorials -->
        <div class="wrapper" id="video-tutorial">
            <h1><?php echo JText::_('COM_NRDS_TUTS');?></h1>
               <?php foreach($this->video_datas as $i=>$data){?>
                   <!-- 
                        <div class="embed-video">
                        <a  class="swipebox" id="larger" href="https://vimeo.com/103826473"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/video-sample.jpg"></a>
                        </div>
-->                     
                    <div class="video-tuts-container">
                        <div id="jp_container_<?php echo $data->id?>" class="jp-video " style="width: 750px;border:0;position:relative">
                            <div class="jp-type-single">
                              <div id="jquery_jplayer_<?php echo $data->id?>" class="jp-jplayer"></div>
                                <div class="jp-gui">
                                    <div class="jp-video-play">
                                      <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
                                    </div>
                                </div>
                            
                          <!--  <div class="jp-interface">
                                <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                          <div class="jp-play-bar"></div>
                                        </div>
                                      </div>
                            </div>-->
                                <div class="jp-interface" style="position: absolute;bottom:0px;">
                                  <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                      <div class="jp-play-bar"></div>
                                    </div>
                                  </div>
                                  <div class="jp-current-time"></div>
                                  <div class="jp-duration"></div>
                                  <div class="jp-controls-holder">
                                    <ul class="jp-controls">
                                      <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
                                      <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
                                      <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
                                      <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
                                      <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
                                      <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
                                    </ul>
                                   
                                    <ul class="jp-toggles">
                                      <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
                                      <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
                                      <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
                                      <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
                                    </ul>

                                     <div class="jp-volume-bar">
                                      <div class="jp-volume-bar-value"></div>
                                    </div>
                                  </div>

                                </div>
                                </div>
                         </div>
                         <div class="video-title-holder">
                                    <span class="video-check"></span>
                                    <span class="video-title"><?php echo $data->video_name?></span>
                         </div>
                     </div>
                             
                            
                <?php }?>
         
        </div>
    </div> <!-- End Video Tutorials -->
	
<script>

jQuery("#nav1").mouseover(function() {
	jQuery("#addpops").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav1:hover").css('background',"#FFFFFF");
	jQuery("#nav1:hover").css('font-weight',"bold");
	jQuery("#nav1").removeClass('notactive');
	jQuery("#nav2,#nav3,#nav4,#nav5,#nav6").addClass('notactive');
	jQuery("#network").hide();
	jQuery("#referrals").hide();
	jQuery("#howto").hide();
	jQuery("#privacy").hide();
	jQuery("#registration").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav2").mouseover(function() {
	jQuery("#howto").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav2:hover").css('background',"#FFFFFF");
	jQuery("#nav2:hover").css('font-weight',"bold");
	jQuery("#nav2").removeClass('notactive');
	jQuery("#nav1,#nav3,#nav4,#nav5,#nav6").addClass('notactive');
	jQuery("#network").hide();
	jQuery("#referrals").hide();
	jQuery("#addpops").hide();
	jQuery("#privacy").hide();
	jQuery("#registration").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav3").mouseover(function() {
	jQuery("#referrals").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav3:hover").css('background',"#FFFFFF");
	jQuery("#nav3:hover").css('font-weight',"bold");
	jQuery("#nav3").removeClass('notactive');
	jQuery("#nav1,#nav2,#nav4,#nav5,#nav6").addClass('notactive');
	jQuery("#howto").hide();
	jQuery("#network").hide();
	jQuery("#addpops").hide();
	jQuery("#privacy").hide();
	jQuery("#registration").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav4").mouseover(function() {
	jQuery("#network").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav4:hover").css('background',"#FFFFFF");
	jQuery("#nav4:hover").css('font-weight',"bold");
	jQuery("#nav4").removeClass('notactive');
	jQuery("#nav1,#nav2,#nav3,#nav5,#nav6").addClass('notactive');
	jQuery("#howto").hide();
	jQuery("#referrals").hide();
	jQuery("#addpops").hide();
	jQuery("#privacy").hide();
	jQuery("#registration").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav5").mouseover(function() {
	jQuery("#privacy").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav5:hover").css('background',"#FFFFFF");
	jQuery("#nav5:hover").css('font-weight',"bold");
	jQuery("#nav5").removeClass('notactive');
	jQuery("#nav1,#nav2,#nav3,#nav4,#nav6").addClass('notactive');
	jQuery("#howto").hide();
	jQuery("#referrals").hide();
	jQuery("#network").hide();
	jQuery("#registration").hide();
	jQuery("#addpops").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav6").mouseover(function() {
	jQuery("#registration").show();
	jQuery("#helpyou").hide();
	jQuery("#quickfixes").hide();
	jQuery("#nav6:hover").css('background',"#FFFFFF");
	jQuery("#nav6:hover").css('font-weight',"bold");
	jQuery("#nav6").removeClass('notactive');
	jQuery("#nav1,#nav2,#nav4,#nav5,#nav3").addClass('notactive');
	jQuery("#howto").hide();
	jQuery("#referrals").hide();
	jQuery("#network").hide();
	jQuery("#privacy").hide();
	jQuery("#addpops").hide();
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
jQuery("#nav7").click(function() {
	jQuery("#referraltable").dialog(
			{
				modal:true, 
				width: 950,
				title: "Referral Fees",
				});
	//jQuery("#referraltable").show();
	//jQuery("#faqtable").hide();
	});
jQuery("#returntohelp").click(function() {
	jQuery("#referraltable").hide();
	jQuery("#faqtable").show();
	});
jQuery("#main").mouseover(function() {
	jQuery("#helpyou").show();
	jQuery("#quickfixes").show();
	jQuery("#howto").hide();
	jQuery("#privacy").hide();
	jQuery("#referrals").hide();
	jQuery("#network").hide();
	jQuery("#registration").hide();
	jQuery("#addpops").hide();
	jQuery("#nav1,#nav2,#nav3,#nav4,#nav5,#nav6").addClass('notactive');
	jQuery(".notactive").css('background',"transparent");
	jQuery(".notactive").css('font-weight',"normal");
	});
function ap1() {
	jQuery("#ap1").show();
	jQuery("#hideap1").show();
	jQuery("#seeap1").hide();
}
function hideap1() {
	jQuery("#ap1").hide();
	jQuery("#hideap1").hide();
	jQuery("#seeap1").show();
}
function ap2() {
	jQuery("#ap2").show();
	jQuery("#hideap2").show();
	jQuery("#seeap2").hide();
}
function hideap2() {
	jQuery("#ap2").hide();
	jQuery("#hideap2").hide();
	jQuery("#seeap2").show();
}
function ap3() {
	jQuery("#ap3").show();
	jQuery("#hideap3").show();
	jQuery("#seeap3").hide();
}
function hideap3() {
	jQuery("#ap3").hide();
	jQuery("#hideap3").hide();
	jQuery("#seeap3").show();
}
function ap4() {
	jQuery("#ap4").show();
	jQuery("#hideap4").show();
	jQuery("#seeap4").hide();
}
function hideap4() {
	jQuery("#ap4").hide();
	jQuery("#hideap4").hide();
	jQuery("#seeap4").show();
}
function hw1() {
	jQuery("#hw1").show();
	jQuery("#hidehw1").show();
	jQuery("#seehw1").hide();
}
function hidehw1() {
	jQuery("#hw1").hide();
	jQuery("#hidehw1").hide();
	jQuery("#seehw1").show();
}
function hw2() {
	jQuery("#hw2").show();
	jQuery("#hidehw2").show();
	jQuery("#seehw2").hide();
}
function hidehw2() {
	jQuery("#hw2").hide();
	jQuery("#hidehw2").hide();
	jQuery("#seehw2").show();
}
function hw3() {
	jQuery("#hw3").show();
	jQuery("#hidehw3").show();
	jQuery("#seehw3").hide();
}
function hidehw3() {
	jQuery("#hw3").hide();
	jQuery("#hidehw3").hide();
	jQuery("#seehw3").show();
}
function hw4() {
	jQuery("#hw4").show();
	jQuery("#hidehw4").show();
	jQuery("#seehw4").hide();
}
function hidehw4() {
	jQuery("#hw4").hide();
	jQuery("#hidehw4").hide();
	jQuery("#seehw4").show();
}
function ref1() {
	jQuery("#ref1").show();
	jQuery("#hideref1").show();
	jQuery("#seeref1").hide();
}
function hideref1() {
	jQuery("#ref1").hide();
	jQuery("#hideref1").hide();
	jQuery("#seeref1").show();
}
function ref2() {
	jQuery("#ref2").show();
	jQuery("#hideref2").show();
	jQuery("#seeref2").hide();
}
function hideref2() {
	jQuery("#ref2").hide();
	jQuery("#hideref2").hide();
	jQuery("#seeref2").show();
}
function ref3() {
	jQuery("#ref3").show();
	jQuery("#hideref3").show();
	jQuery("#seeref3").hide();
}
function hideref3() {
	jQuery("#ref3").hide();
	jQuery("#hideref3").hide();
	jQuery("#seeref3").show();
}
function ref4() {
	jQuery("#ref4").show();
	jQuery("#hideref4").show();
	jQuery("#seeref4").hide();
}
function hideref4() {
	jQuery("#ref4").hide();
	jQuery("#hideref4").hide();
	jQuery("#seeref4").show();
}
function net1() {
	jQuery("#net1").show();
	jQuery("#hidenet1").show();
	jQuery("#seenet1").hide();
}
function hidenet1() {
	jQuery("#net1").hide();
	jQuery("#hidenet1").hide();
	jQuery("#seenet1").show();
}
function net2() {
	jQuery("#net2").show();
	jQuery("#hidenet2").show();
	jQuery("#seenet2").hide();
}
function hidenet2() {
	jQuery("#net2").hide();
	jQuery("#hidenet2").hide();
	jQuery("#seenet2").show();
}
function priv1() {
	jQuery("#priv1").show();
	jQuery("#hidepriv1").show();
	jQuery("#seepriv1").hide();
}
function hidepriv1() {
	jQuery("#priv1").hide();
	jQuery("#hidepriv1").hide();
	jQuery("#seepriv1").show();
}
function priv2() {
	jQuery("#priv2").show();
	jQuery("#hidepriv2").show();
	jQuery("#seepriv2").hide();
}
function hidepriv2() {
	jQuery("#priv2").hide();
	jQuery("#hidepriv2").hide();
	jQuery("#seepriv2").show();
}
function reg1() {
	jQuery("#reg1").show();
	jQuery("#hidereg1").show();
	jQuery("#seereg1").hide();
}
function hidereg1() {
	jQuery("#reg1").hide();
	jQuery("#hidereg1").hide();
	jQuery("#seereg1").show();
}
function reg2() {
	jQuery("#reg2").show();
	jQuery("#hidereg2").show();
	jQuery("#seereg2").hide();
}
function hidereg2() {
	jQuery("#reg2").hide();
	jQuery("#hidereg2").hide();
	jQuery("#seereg2").show();
}
function hy1() {
	jQuery("#hy1").show();
	jQuery("#hidehy1").show();
	jQuery("#seehy1").hide();
}
function hidehy1() {
	jQuery("#hy1").hide();
	jQuery("#hidehy1").hide();
	jQuery("#seehy1").show();
}
function hy2() {
	jQuery("#hy2").show();
	jQuery("#hidehy2").show();
	jQuery("#seehy2").hide();
}
function hidehy2() {
	jQuery("#hy2").hide();
	jQuery("#hidehy2").hide();
	jQuery("#seehy2").show();
}
function hy3() {
	jQuery("#hy3").show();
	jQuery("#hidehy3").show();
	jQuery("#seehy3").hide();
}
function hidehy3() {
	jQuery("#hy3").hide();
	jQuery("#hidehy3").hide();
	jQuery("#seehy3").show();
}
function hy4() {
	jQuery("#hy4").show();
	jQuery("#hidehy4").show();
	jQuery("#seehy4").hide();
}
function hidehy4() {
	jQuery("#hy4").hide();
	jQuery("#hidehy4").hide();
	jQuery("#seehy4").show();
}
function hy5() {
	jQuery("#hy5").show();
	jQuery("#hidehy5").show();
	jQuery("#seehy5").hide();
}
function hidehy5() {
	jQuery("#hy5").hide();
	jQuery("#hidehy5").hide();
	jQuery("#seehy5").show();
}
function hy6() {
	jQuery("#hy6").show();
	jQuery("#hidehy6").show();
	jQuery("#seehy6").hide();
}
function hidehy6() {
	jQuery("#hy6").hide();
	jQuery("#hidehy6").hide();
	jQuery("#seehy6").show();
}
function qf1() {
	jQuery("#qf1").show();
	jQuery("#hideqf1").show();
	jQuery("#seeqf1").hide();
}
function hideqf1() {
	jQuery("#qf1").hide();
	jQuery("#hideqf1").hide();
	jQuery("#seeqf1").show();
}
function qf2() {
	jQuery("#qf2").show();
	jQuery("#hideqf2").show();
	jQuery("#seeqf2").hide();
}
function hideqf2() {
	jQuery("#qf2").hide();
	jQuery("#hideqf2").hide();
	jQuery("#seeqf2").show();
}
function qf3() {
	jQuery("#qf3").show();
	jQuery("#hideqf3").show();
	jQuery("#seeqf3").hide();
}
function hideqf3() {
	jQuery("#qf3").hide();
	jQuery("#hideqf3").hide();
	jQuery("#seeqf3").show();
}
function qf4() {
	jQuery("#qf4").show();
	jQuery("#hideqf4").show();
	jQuery("#seeqf4").hide();
}
function hideqf4() {
	jQuery("#qf4").hide();
	jQuery("#hideqf4").hide();
	jQuery("#seeqf4").show();
}
function qf5() {
	jQuery("#qf5").show();
	jQuery("#hideqf5").show();
	jQuery("#seeqf5").hide();
}
function hideqf5() {
	jQuery("#qf5").hide();
	jQuery("#hideqf5").hide();
	jQuery("#seeqf5").show();
}
function qf6() {
	jQuery("#qf6").show();
	jQuery("#hideqf6").show();
	jQuery("#seeqf6").hide();
}
function hideqf6() {
	jQuery("#qf6").hide();
	jQuery("#hideqf6").hide();
	jQuery("#seeqf6").show();
}
jQuery(document).ready(function(){
	jQuery(".swipebox").swipebox();
	jQuery(".watch").click(function(e) {
		 var currentAttrValue = jQuery(this).attr('href');
	    
	    jQuery('html,body').animate({
	        scrollTop:jQuery(currentAttrValue).offset().top},
	        'slow');
	     e.preventDefault();
	});
});
	
</script>