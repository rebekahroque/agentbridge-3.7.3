<?php
	
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
jimport('joomla.user.helper');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class NrdsController extends JControllerLegacy
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display($tpl = null)
    {   
            
		if(isset($_GET['lang']) && $_GET['lang']!=""){
			$language =& JFactory::getLanguage();
			$extension = 'com_nrds';
			$base_dir = JPATH_SITE;
			$language_tag = $_GET['lang'];
			$language->load($extension, $base_dir, $language_tag, true);

			setcookie('languageSet', $_GET['lang']);

		} else {

			$language = JFactory::getLanguage();
			$extension = 'com_nrds';
			$base_dir = JPATH_SITE;
			$language_tag = "english-US";
			$language->load($extension, $base_dir, $language_tag, true);

			// echo "<pre>";
			// var_dump( $language );
			// echo "</pre>";
		}
		
		$userprofilemodel2 = JPATH_ROOT.'/components/'.'com_propertylisting/models';

        JModelLegacy::addIncludePath( $userprofilemodel2 );

        $model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

        $ex_rates = $model2->getExchangeRates_indi();

        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;

        $model = $this->getModel($this->getName());
		
		$aveVolData = $model->get_AveMemberVolume();
        $start_year = 2012;
        $prev_year = (int) date("Y",strtotime("-1 year", time()));
        $count_ActUser=0;
        $count_TotalUser=0;
        $totalVols = 0;
        $verified_users = array();

        while ($prev_year >= $start_year) {
            # code...                 
            $str_prev_year =strval($prev_year);
            foreach ($aveVolData as $key => $value) {
                // var_dump($value->verified_."$str_prev_year");
                if($value->{"verified_".$str_prev_year} && !in_array($value->user_id, $verified_users) && $value->{"volume_".$str_prev_year} >= 2 ){
                  //  echo "<pre>";
                  //  var_dump($str_prev_year." - ".$value->agent_id."-".$value->{"verified_".$str_prev_year});
                  //  echo "</pre>";
                    $currency = ($value->user_currency == "" || $value->country == 0) ? "USD" :  $value->user_currency;
					
					if($value->country!="223"){
						$convertedV=$value->{"volume_".$str_prev_year} / $ex_rates->rates->{$currency};
					} else {
						$convertedV=$value->{"volume_".$str_prev_year};
					}
				  
                    $totalVols += $convertedV;
                    $count_ActUser++;
                    $verified_users[]=$value->user_id;
                }
              // $count_TotalUser++;                
            }
            $prev_year--;
        }
       // var_dump($count_ActUser);
      //  var_dump($count_TotalUser);
       // var_dump($totalVols/$count_ActUser);

        $total = $totalVols/$count_ActUser;
		
		$round_total = round($total);
		
		switch(strlen($round_total)) {
			case 4:
			case 7:
			case 10:
				$trimmed = substr($round_total,0,1);
				break;
			case 5:
			case 8:
			case 11:
				$trimmed = substr($round_total,0,2);
				break;
			case 6:
			case 9:
			case 12:
				$trimmed = substr($round_total,0,3);
				break;
		}
		
		if(strlen($round_total)<= 6 && strlen($total) >=4) {
			$suffix = "THOUSAND";
		} else if(strlen($round_total)<= 9 && strlen($total) >=7) {
			$suffix = "MILLION";
		} else if (strlen($round_total)<= 12 && strlen($total) >= 10) {
			$suffix = "BILLION";
		}
		
		$ave_volume = $trimmed . " " . $suffix;
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('ave_volume', $ave_volume);
		$view->display("newhome");	
			/*
			$mainframe=JFactory::getApplication();
            $url=$_GET['returnurl'];
            $h321=$_GET['h321'];
            if ($_GET['emailnotif']) {
                $mainframe->redirect(JRoute::_('index.php?option=com_users&view=login').'&returnurl='.$url."&h321=".$h321);
            } else {
                
                if($user->id > 0){
                $mainframe->redirect(JRoute::_('index.php?option=com_userprofile&task=profile'));
                }
            
                if(isset($_GET['illegal'])){
                    
                     $mainframe->redirect(JRoute::_(JURI::base()."index.php/component/users/"));
              
                }

                if(isset($_GET['lang']) && $_GET['lang']!=""){
                                $language =& JFactory::getLanguage();
                                $extension = 'com_nrds';
                                $base_dir = JPATH_SITE;
                                $language_tag = $_GET['lang'];
                                $language->load($extension, $base_dir, $language_tag, true);

                                 setcookie('languageSet', $_GET['lang']);
                
                } else {

                          $language = JFactory::getLanguage();
                          $extension = 'com_nrds';
                          $base_dir = JPATH_SITE;
                          $language_tag = "english-US";
                          $language->load($extension, $base_dir, $language_tag, true);

                       
                  }
            }

        $userprofilemodel2 = JPATH_ROOT.'/components/'.'com_propertylisting/models';

        JModelLegacy::addIncludePath( $userprofilemodel2 );

        $model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

        $ex_rates = $model2->getExchangeRates_indi();

        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;

        $model = $this->getModel($this->getName());
		/*$is2012 = $model->get_totalave2012();
		$is2013 = $model->get_totalave2013();
		$is2014 = $model->get_totalave2014();
        $is2015 = $model->get_totalave2015();

		$sum2012 = 0;
		foreach ($is2012 as $value) {
            if($value->currency)
            if($value->country!="223"){
                $convertedV=$value->volume_2012 / $ex_rates->rates->{$value->currency};
                //$convertedV = $model2->getExchangeRates($value->volume_2012,"USD",$value->currency);
                $sum2012 += $convertedV;
            } else {
                $sum2012 += $value->volume_2012; 
            } 
		}
		$sum2013 = 0;
		foreach ($is2013 as $value) {
           if($value->currency)			 
           if($value->country!="223"){
            $convertedV=$value->volume_2013 / $ex_rates->rates->{$value->currency};
               //$convertedV = $model2->getExchangeRates($value->volume_2013,"USD",$value->currency);
                $sum2013 += $convertedV;
            } else {
                $sum2013 += $value->volume_2013; 
            }
		}
		$sum2014 = 0;
		foreach ($is2014 as $value) {
               if($value->currency)
            if($value->country!="223"){
                $convertedV=$value->volume_2014 / $ex_rates->rates->{$value->currency};
               // $convertedV = $model2->getExchangeRates($value->volume_2014,"USD",$value->currency);
                $sum2014 += $convertedV;
            } else {
                $sum2014 += $value->volume_2014; 
            }			 
		}
        $sum2015 = 0;
        foreach ($is2015 as $value) {
            if($value->currency)
                if($value->country!="223"){
                    $convertedV=$value->volume_2015 / $ex_rates->rates->{$value->currency};
                   // $convertedV = $model2->getExchangeRates($value->volume_2014,"USD",$value->currency);
                    $sum2015 += $convertedV;
                } else {
                    $sum2015 += $value->volume_2015; 
                }             
        }

		$totalave2012 = $sum2012/count($is2012);
		$totalave2013 = $sum2013/count($is2013);
		$totalave2014 = $sum2014/count($is2014);
        $totalave2015 = $sum2015/count($is2015);

        
        if($totalave2015)
		  $total = ($totalave2012+$totalave2013+$totalave2014+$totalave2015)/4;
        else if($totalave2014)
          $total = ($totalave2012+$totalave2013+$totalave2014)/3;
        else
          $total = ($totalave2012+$totalave2013)/2;*//*

        # Average Member Volume Computation

        $aveVolData = $model->get_AveMemberVolume();
        $start_year = 2012;
        $prev_year = (int) date("Y",strtotime("-1 year", time()));
        $count_ActUser=0;
        $count_TotalUser=0;
        $totalVols = 0;
        $verified_users = array();

        while ($prev_year >= $start_year) {
            # code...                 
            $str_prev_year =strval($prev_year);
            foreach ($aveVolData as $key => $value) {
          
                if($value->{"verified_".$str_prev_year} && !in_array($value->user_id, $verified_users) && $value->{"volume_".$str_prev_year} >= 2 ){
               
                    $currency = ($value->user_currency == "" || $value->country == 0) ? "USD" :  $value->user_currency;
					
					if($value->country!="223"){
						$convertedV=$value->{"volume_".$str_prev_year} / $ex_rates->rates->{$currency};
					} else {
						$convertedV=$value->{"volume_".$str_prev_year};
					}
				  
                    $totalVols += $convertedV;
                    $count_ActUser++;
                    $verified_users[]=$value->user_id;
                }
                    
            }
            $prev_year--;
        }
    

        $total = $totalVols/$count_ActUser;
		
        $view = &$this->getView($this->getName(), 'html');
		$view->assignRef('total', $total);
		parent::display($tpl);
		*/

    }

  
	
	function contactus()
    {
        $model = $this->getModel($this->getName());
        $video_data = $model->getListVideos();
		$fee = $model->getReferralTable();
		$view = &$this->getView($this->getName(), 'html');
        $view->assignRef('video_datas', $video_data);
		$view->assignRef('fee', $fee);
        $view->display($this->getTask());
    }
	
	function aboutus()
    {
		$view = &$this->getView($this->getName(), 'html');
        $view->display($this->getTask());
    }
		
	
	
    function nrds(){
    	$view = &$this->getView($this->getName(), 'html');
    	$view->assignRef('countries', $countries);
    	$view->display($this->getTask());
    }
    
    function manual(){
    	// INITIALIZE DATABASE CONNECTION
    	$model = $this->getModel($this->getName());
    	$countries = $model->get_countries();
    	
        $getCountryLangs = $this->getCountryLangs();

    	$view = &$this->getView($this->getName(), 'html');
    	$view->assignRef('countries', $getCountryLangs);
    	$view->display($this->getTask());
    }
    

    function getCountryLangs(){

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->setQuery("
            SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id  ORDER BY cl.countries_name");
        $db->setQuery($query);      
        return $db->loadObjectList();
    }
    
    function verify(){
	    $xml = simplexml_load_file(JUri::base()."/agents.xml");
	   	//print_r($xml);
		$result = array();
		
		foreach($xml->agent as $agent){
			if($agent->licence==$_POST['licence']){
				$result = $agent;
			}
			else{
				continue;
			}
		}
		if(!empty($result)){
			$view = &$this->getView($this->getName(), 'html');
    		$view->assignRef('result', $result);
    		$view->display($this->getTask());
		}
		else{
			$msg = 'Not found';
			$link = 'index.php?option=com_nrds';
			$this->setRedirect($link, $msg);
		}
    }
    
    function registerNRDS(){
    	$model = $this->getModel($this->getName());
    	if($_POST['jform']['form']=="nrds_registration"){
    		$data = array();
    		foreach($_POST['jform'] as $key=>$value){
    			$data[$key] = $value;
    		}
    		$name = explode(" ", $data['name']);
    		$data['fname'] = $name[0];
    		unset($name[0]);
    		$data['lname'] = implode(' ', $name);
    		$data['country'] = $model->get_country_id($data['country']);
    		$data['state'] = ($model->numeric_zone($data['country'])) ? $model->get_state_id($data['state']): $data['state'];
    		$data['straddress'] = $data['waddress'];
    		$data['image'] = $data['imagelink']; 
    		unset($data['name']);
    		unset($data['imagelink']);
    		unset($data['waddress']);
    		$this->register($data, true);	
    		$this->setRedirect(JRoute::_('index.php'));
    	}
    }
    
    function registerManual() {
    	$model = $this->getModel($this->getName());
    	if($_POST['jform']['form']=="nrds_registration"){
    		$data = array();
    		foreach($_POST['jform'] as $key=>$value){
    			if(is_array($value)){
    				foreach($value as $k=>$v){
    					$data[$key][$k] = trim(addslashes($v));
    				}
    			}
    			else{
    				$data[$key] = trim(addslashes($value));
    			}
    		}
			

    		$this->register($data);	
    	}
    }
    
    function register($data, $let = false){
    	$mainframe = JFactory::getApplication();
    	$error = false;
	
    	$model = $this->getModel($this->getName());
    	
    	@$duplicate = $model->check_duplicate($data['email']);

		$session =& JFactory::getSession();
		
		(isset($data['email']) && !empty($data['email'])) ? $session->set( 'email', $data['email'] ) : "";
		(isset($data['fname']) && !empty($data['fname'])) ? $session->set( 'fname', $data['fname'] ) : "";
		(isset($data['lname']) && !empty($data['lname'])) ? $session->set( 'lname', $data['lname'] ) : "";
        (isset($data['gender']) && !empty($data['gender'])) ? $session->set( 'gender', $data['gender'] ) : $session->set( 'gender', "Male" );
		(isset($data['licence']) && !empty($data['licence'])) ? $session->set( 'licence', $data['licence'] ) : "";
		(isset($data['wphone']['value']) && !empty($data['wphone']['value'])) ? $session->set( 'wphone', $data['wphone']['value'] ) : "";
		(isset($data['mphone']['value']) && !empty($data['mphone']['value'])) ? $session->set( 'mphone', $data['mphone']['value'] ) : "";
		(isset($data['zip']) && !empty($data['zip'])) ? $session->set( 'zip', $data['zip'] ) : "";
		(isset($data['straddress']) && !empty($data['straddress'])) ? $session->set( 'straddress', $data['straddress'] ) : "";
		(isset($data['suburb']) && !empty($data['suburb'])) ? $session->set( 'suburb', $data['suburb'] ) : "";
		(isset($data['city']) && !empty($data['city'])) ? $session->set( 'city', $data['city'] ) : "";
		(isset($data['brokerage_license']) && !empty($data['brokerage_license'])) ? $session->set( 'brokerage_license', $data['brokerage_license'] ) : "";
		(isset($data['volume_2015']) && !empty($data['volume_2015'])) ? $session->set( 'volume_2015', $data['volume_2015'] ) : "";
		(isset($data['sides_2015']) && !empty($data['sides_2015'])) ? $session->set( 'sides_2015', $data['sides_2015'] ) : "";
		(isset($data['volume_2016']) && !empty($data['volume_2016'])) ? $session->set( 'volume_2016', $data['volume_2016'] ) : "";
		(isset($data['sides_2016']) && !empty($data['sides_2016'])) ? $session->set( 'sides_2016', $data['sides_2016'] ) : "";
		(isset($data['verified_2015']) && !empty($data['verified_2015'])) ? $session->set( 'verified_2015', $data['verified_2015'] ) : "";
		(isset($data['verified_2016']) && !empty($data['verified_2016'])) ? $session->set( 'verified_2016', $data['verified_2016'] ) : "";
		((!empty($data['volume_2016']) && !empty($data['sides_2016'])) || ($data['volume_2016']!=0 && $data['sides_2016']!=0) ) ? $session->set( 'ave_price_2016', $data['volume_2016']/$data['sides_2016'] ) : "0";
		$session->set( 'ave_price_2015', $data['volume_2015']/$data['sides_2015'] );
		

    	if(!$duplicate){


			
				$id = $model->register($data);
				$session->set( 'email', "" );
				$session->set( 'fname', "" );
				$session->set( 'lname', "" );
                $session->set( 'gender', "Male" );
		        $session->set( 'licence', "" );
				$session->set( 'wphone', "" );
				$session->set( 'mphone', "" );
				$session->set( 'zip', "" );
				$session->set( 'straddress', "");
			    $session->set( 'suburb', "" );
				$session->set( 'city', "" );
				$session->set( 'brokerage_license', "" );
				$session->set( 'volume_2015', "" );
				$session->set( 'sides_2015', "" );
				$session->set( 'ave_price_2015', "" );
				$session->set( 'volume_2016', "" );
				$session->set( 'sides_2016', "" );
				$session->set( 'ave_price_2016', "" );		
    			echo $this->sendmail($data, $id);
				die();
				
    	} else {
    		// JFactory::getApplication()->enqueueMessage('Click on Forget Password to retrieve your password.', "Duplicate Entry" );
			// $mainframe->redirect(JRoute::_("index.php?option=com_nrds&task=manual"));
			echo "duplicate_entry";
			die();
    	}
		
		die();
    }
    
    function sendmail($data, $id) {
    	$mainframe = JFactory::getApplication();
		$model = $this->getModel($this->getName());
    	if($data['manual']) {
		
			if ((!empty($data['volume_2016'])) || (!empty($data['sides_2016'])) || $data['volume_2016']!=0 || $data['sides_2016']!=0 || ($data['volume_2016'])<($data['sides_2016'])) {
		
				$ave_price_2016 =number_format( (int) preg_replace("/[^0-9,.]/", "", (str_replace(",","",$data['volume_2016'])))/ (int) str_replace(",", "", $data['sides_2016']));
		
			} else {
		
				$ave_price_2016 = 0;
			
			}
		

			$ave_price_2015 = number_format(((int) preg_replace("/[^0-9,.]/", "", (str_replace(",","",$data['volume_2015'])))/ (int) str_replace(",", "", $data['sides_2015'])),0, '.', ',');
			
			if (!empty($volume2015)) {
				$volume2015 = number_format($data['volume_2015']);
				} else {
				$volume2015=$data['volume_2015'];}
			if (!empty($volume2016)) {
				$volume2016 = number_format($data['volume_2016']);
				} else {
				$volume2016=$data['volume_2016'];}
			
			$currency = $model->getCurrency($data['country']);
			
    		$body .= "Hello Admin,<br /><br />";
    		$body .= "You have a new membership application.<br /><br />";
    		$body .= "<table>
			    		<tr>
			    		<td>First Name: </td>
			    		<td>".stripslashes_all($data['fname'])."</td>
			    		</tr>
			    		<tr>
			    		<td>Last Name: </td>
			    		<td>".stripslashes_all($data['lname'])."</td>
			    		</tr>
                        <tr>
                        <td>Gender: </td>
                        <td>".stripslashes_all($data['gender'])."</td>
                        </tr>
			    		<tr>
			    		<td>Licence: </td>
			    		<td>".$data['licence']."</td>
			    		</tr>
			    		<tr>
			    		<td>Work Phone: </td>
			    		<td>".$data['wphone']['value']."</td>
			    		</tr>
			    		<tr>
			    		<td>Mobile: </td>
			    		<td>".$data['mphone']['value']."</td>
			    		</tr>
    					<tr>
	    					<td>Email: </td>
	    					<td>".$data['email']."</td>
    					</tr>
    					<tr>
	    					<td>Address Line 1: </td>
	    					<td>".$data['straddress']."</td>
    					</tr>
    					<tr>
	    					<td>Address Line 2: </td>
	    					<td>".$data['suburb']."</td>
    					</tr>
    					<tr>
	    					<td>City: </td>
	    					<td>".$data['city']."</td>
    					</tr>
    					<tr>
	    					<td>Zip/ Postal Code: </td>
	    					<td>".$data['zip']."</td>
    					</tr>
    					<tr>
	    					<td>State: </td>
	    					<td>".$model->get_state($data['state'])."</td>
    					</tr>
    					<tr>
	    					<td>Country: </td>
	    					<td>".$model->get_country($data['country'])."</td>
    					</tr>
    					<tr>
	    					<td>Brokerage: </td>
	    					<td>".$model->get_broker($data['brokerage'])."</td>
    					</tr>
    					<tr>
	    					<td>Brokerage License Number: </td>
	    					<td>".$data['brokerage_license']."</td>
    					</tr>
    					
						<tr><td colspan='2'><strong>2016</strong></td></tr>
						<tr>
	    					<td>Average Price: </td>
	    					<td>".$currency->symbol.$ave_price_2016." ".$currency->currency."</td>
    					</tr>
    					<tr>
	    					<td>Total Volume: </td>
	    					<td>".$currency->symbol.$volume2016." ".$currency->currency."</td>
    					</tr>
    					<tr>
	    					<td>Total Sides: </td>
	    					<td>".$data['sides_2016']."</td> 
    					</tr>
						<tr><td colspan='2'><strong>2015</strong></td></tr>
						<tr>
	    					<td>Average Price: </td>
	    					<td>".$currency->symbol.$ave_price_2015." ".$currency->currency."</td>
    					</tr>
    					<tr>
	    					<td>Total Volume: </td>
	    					<td>".$currency->symbol.$volume2015." ".$currency->currency."</td>
    					</tr>
    					<tr>
	    					<td>Total Sides: </td>
	    					<td>".$data['sides_2015']."</td>
    					</tr>
    				</table>";
    		$body .= "<br /><br />";
    		$body .= "To approve the application please log in to <a href=\"".JUri::root()."administrator/index.php?option=com_useractivation\">".JUri::root()."administrator/index.php?option=com_useractivation</a>";
    		
    		$email = array('rroque@agentbridge.com','focon@agentbridge.com','mark.obre@keydiscoveryinc.com' );
    		$subject = 'New Membership Application - ' . stripslashes_all($data['fname']) . ' ' . stripslashes_all($data['lname']);
    	}
    	
    	
    	$mailSender = &JFactory::getMailer();
    	$mailSender->addRecipient( $email );
    	$mailSender->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
		$mailSender->addBCC("redler@agentbridge.com");
    	$mailSender->setSubject( $subject );
    	$mailSender->isHTML(  true );
    	$mailSender->setBody(  $body );
    	if ($mailSender->Send()){
    		
				return 'registration_complete';
		
    			// JFactory::getApplication()->enqueueMessage('Thank you for your interest in joining AgentBridge. Please check your email for further instructions.', "thank you");
    			// $mainframe->redirect('index.php');
    		
    	}
    	else{
    		JError::raiseWarning( 100, 'Email not sent!' );
    	}
    }


    //for reset locked account

    function resetLockedAccount(){

        $username = $_GET['email'];

        $defpassword = JUserHelper::getCryptedPassword("AgentBridge1", $salt);

        $db     = JFactory::getDbo();
        $query   = $db->getQuery(true);
        $fields = array(
            $db->quoteName('password') . ' = ' . $db->quote($defpassword),
            $db->quoteName('is_locked') . ' = 1'                           
        );
         
        // Conditions for which records should be updated.
        $conditions = array(
            $db->quoteName('username') . ' = '.$db->Quote($username)                            
        );

        $query->update($db->quoteName('#__users'))->set($fields)->where($conditions);

        $db->setQuery($query);

        $result = $db->query();

        echo $result;

    }

    //for login web service

    function loginWebService(){

        $email = $_POST['email'];
        $password = $_POST['password'];

        $credentials = array( 'username' => $email, 'password' => $password);
        $login_site  =& JFactory::getApplication();

        $num = $login_site->login($credentials);

        $model = $this->getModel($this->getName());

        $response = array();
        if($num == 0) {

            $db = JFactory::getDbo();
        
            $query = $db->getQuery(true);            
            $query->setQuery("

                SELECT email, is_locked FROM #__users WHERE email='$email'"

            );

            $db->setQuery($query);

            $rows = array();

           // $rows[0] = $db->loadAssocList();       
           // $data = $db->loadRow();
            $data = $db->loadAssocList();

            if($data){
                $response = array('status'=>$credentials, 'message'=>"Login Failed", 'email'=>$data);
            } else {
                $response = array('status'=>$credentials, 'message'=>"Login Failed", 'data'=>array());
            }

            
        }
        else {           

            $db = JFactory::getDbo();
        
            $query = $db->getQuery(true);            
            $query->setQuery("

                SELECT * FROM #__users WHERE email='$email'"

            );

            $db->setQuery($query);

            $rows = array();

           // $rows[0] = $db->loadAssocList();       
			$data = $db->loadAssocList();
			
			//$model = $this->getModel($this->getName());
			$session_id = $model->addUserSession();
			//$session_id = $this->addUserSession();
			//session_id = "";
            
            $response = array('status'=>$credentials, 'message'=>"Login Successful", 'data'=>$data, 'session_id'=>"$session_id");
        } 
		
		header("Content-Type: application/json");
		
        echo json_encode($response);

        exit();
        
    }
    
    function changePasswordWS(){


        $userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';

        JModelLegacy::addIncludePath( $userprofilemodel );

        $model =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');

        //$model = $this->getModel('UserProfile');

        $userId = $_POST['userId'];
        $oldpassword = $_POST['oldpassword'];
        $newpassword = $_POST['newpassword'];

        // Get a database object
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('id, password')
            ->from('#__users')
            ->where('id =' . $db->quote($userId));

        $db->setQuery($query);
        $result = $db->loadObject();

        $match = JUserHelper::verifyPassword($oldpassword, $result->password, $result->id);

        $response = array();
        if($match){
            if($newpassword){
                $model->insertUpdatePassword($result->id);
                $new_user = new JObject();
                $new_user->id = $result->id;
                $new_user->password = JUserHelper::hashPassword($newpassword);
                $result = JFactory::getDbo()->updateObject('#__users', $new_user, 'id');

                $response = array('status'=>1, 'message'=>"Change Password Successful", 'data'=>array("success"));
            } else {
                $response = array('status'=>3, 'message'=>"Missing New Password ", 'data'=>array("failed"));
            }

        }
        else{
                $response = array('status'=>0, 'message'=>"Change Password Failed", 'data'=>array());
        }


        echo json_encode($response);

        exit();
        
    }
	
	function newhome() {
		if(isset($_GET['lang']) && $_GET['lang']!=""){
			$language =& JFactory::getLanguage();
			$extension = 'com_nrds';
			$base_dir = JPATH_SITE;
			$language_tag = $_GET['lang'];
			$language->load($extension, $base_dir, $language_tag, true);

			setcookie('languageSet', $_GET['lang']);

		} else {

			$language = JFactory::getLanguage();
			$extension = 'com_nrds';
			$base_dir = JPATH_SITE;
			$language_tag = "english-US";
			$language->load($extension, $base_dir, $language_tag, true);

			// echo "<pre>";
			// var_dump( $language );
			// echo "</pre>";
		}
		
		$userprofilemodel2 = JPATH_ROOT.'/components/'.'com_propertylisting/models';

        JModelLegacy::addIncludePath( $userprofilemodel2 );

        $model2 =& JModelLegacy::getInstance('PropertyListing', 'PropertyListingModel');

        $ex_rates = $model2->getExchangeRates_indi();

        $ex_rates_CAD = $ex_rates->rates->CAD;
        $ex_rates_USD = $ex_rates->rates->USD;

        $model = $this->getModel($this->getName());
		
		$aveVolData = $model->get_AveMemberVolume();
        $start_year = 2012;
        $prev_year = (int) date("Y",strtotime("-1 year", time()));
        $count_ActUser=0;
        $count_TotalUser=0;
        $totalVols = 0;
        $verified_users = array();

        while ($prev_year >= $start_year) {
            # code...                 
            $str_prev_year =strval($prev_year);
            foreach ($aveVolData as $key => $value) {
                // var_dump($value->verified_."$str_prev_year");
                if($value->{"verified_".$str_prev_year} && !in_array($value->user_id, $verified_users) && $value->{"volume_".$str_prev_year} >= 2 ){
                  //  echo "<pre>";
                  //  var_dump($str_prev_year." - ".$value->agent_id."-".$value->{"verified_".$str_prev_year});
                  //  echo "</pre>";
                    $currency = ($value->user_currency == "" || $value->country == 0) ? "USD" :  $value->user_currency;
					
					if($value->country!="223"){
						$convertedV=$value->{"volume_".$str_prev_year} / $ex_rates->rates->{$currency};
					} else {
						$convertedV=$value->{"volume_".$str_prev_year};
					}
				  
                    $totalVols += $convertedV;
                    $count_ActUser++;
                    $verified_users[]=$value->user_id;
                }
              // $count_TotalUser++;                
            }
            $prev_year--;
        }
       // var_dump($count_ActUser);
      //  var_dump($count_TotalUser);
       // var_dump($totalVols/$count_ActUser);

        $total = $totalVols/$count_ActUser;
		
		$round_total = round($total);
		
		switch(strlen($round_total)) {
			case 4:
			case 7:
			case 10:
				$trimmed = substr($round_total,0,1);
				break;
			case 5:
			case 8:
			case 11:
				$trimmed = substr($round_total,0,2);
				break;
			case 6:
			case 9:
			case 12:
				$trimmed = substr($round_total,0,3);
				break;
		}
		
		if(strlen($round_total)<= 6 && strlen($total) >=4) {
			$suffix = "THOUSAND";
		} else if(strlen($round_total)<= 9 && strlen($total) >=7) {
			$suffix = "MILLION";
		} else if (strlen($round_total)<= 12 && strlen($total) >= 10) {
			$suffix = "BILLION";
		}
		
		$ave_volume = $trimmed . " " . $suffix;
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef('ave_volume', $ave_volume);
		$view->display($this->getTask());
		
		
	}
	
	/*
	function sales() {
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$userModel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');
		
		if(isset($_GET['h321']) && $_GET['h321']!=""){
			$regid= base64_decode($_GET['h321']);
			$userdets = $userModel->get_user_details("*",$regid);
			$username = $userdets->firstname." ".$userdets->lastname;
			$user_email = $userdets->email;
			
			
			//echo $this->getName(); exit;
			$view = &$this->getView($this->getName(), 'html');
			print_r($userdets);
			//echo $this->getTask();
			$view->assignRef('username', $username);
			$view->assignRef('user_email', $user_email);
			$view->display($this->getTask());
		} else {
			$this->setRedirect(JRoute::_('index.php'));
		}
		
		
	}
	*/
	
	function usersales() {
		$app = JFactory::getApplication();
		
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$userModel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');	
		
		if(isset($_GET['h321']) && $_GET['h321']!=""){
			$regid= base64_decode($_GET['h321']);
			$userdets = $userModel->get_user_details("ur.*, conc.currency, conc.symbol",$regid);
			$usersales = $userModel->get_user_sales($regid);
			if($usersales[0]->volume_2016 > 0 && $usersales[0]->sides_2016 > 0) {
				$app->enqueueMessage( "You've already updated your Sales Volume and Sides for 2016.", "Warning");
				$this->setRedirect(JRoute::_('index.php'));
			}
			
			//echo $this->getName(); exit;
			$view = &$this->getView($this->getName(), 'html');
			//echo $this->getTask();
			$view->assignRef('agent_id', $userdets->user_id);
			$view->assignRef('user_currency', $userdets->currency);
			$view->assignRef('user_currency_symbol', $userdets->symbol);
			$view->display($this->getTask());
		} else {
			$this->setRedirect(JRoute::_('index.php'));
		}
	}
	
	function usersalessubmit() {
		$app = JFactory::getApplication();
		
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$userModel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');	
		
		if(count($_POST)) {
			$regid= base64_decode($_POST['h321']);
			$userdets = $userModel->get_user_details("*",$regid);
			$usersales = $userModel->get_user_sales($regid);
			
			$update_sales['agent_id'] = base64_decode($_POST['h321']);
			$update_sales['volume_2016'] = (int)preg_replace("/[^0-9]/", "", $_POST['volume_2016']);
			$update_sales['sides_2016'] = $_POST['sides_2016'];
			$update_sales['ave_price_2016'] = (int)($update_sales['volume_2016'] / $update_sales['sides_2016']);
			$update_sales['verified_2016'] = 0;
			
			$userModel->update_user_sales($update_sales);
			
			$body .= "Hello Admin,<br /><br />";
    		$body .= "A member has updated his/her Sales Volume and Sides for 2016:<br /><br />";
    		$body .= "<table>
			    		<tr>
			    		<td>Agent ID: </td>
			    		<td>".stripslashes_all($update_sales['agent_id'])."</td>
			    		</tr>
						<tr>
			    		<td>First Name: </td>
			    		<td>".stripslashes_all($userdets->firstname)."</td>
			    		</tr>
			    		<tr>
			    		<td>Last Name: </td>
			    		<td>".stripslashes_all($userdets->lastname)."</td>
			    		</tr>
                        <tr>
                        <td>2016 Sales Volume: </td>
                        <td>".stripslashes_all($_POST['volume_2016'])."</td>
                        </tr>
			    		<tr>
                        <td>2016 Sales Volume: </td>
                        <td>".stripslashes_all($update_sales['sides_2016'])."</td>
                        </tr>
			    		<tr>
    				</table>";
    		$body .= "<br /><br />";
    		$body .= "To verify the numbers please log in to <a href=\"".JUri::root()."administrator/index.php?option=com_useractivation\">".JUri::root()."administrator/index.php?option=com_useractivation</a>";
    		
    		$email = array('francis.alincastre@keydiscoveryinc.com','rebekahroque@gmail.com','socon.agentbridge@gmail.com','mark.obre@keydiscoveryinc.com' );
			//$email = array('francis.alincastre@keydiscoveryinc.com','mark.obre@keydiscoveryinc.com', );
    		$subject = 'Agent Sales Volume and Sides Update for 2016 - ' . stripslashes_all($userdets->firstname) . ' ' . stripslashes_all($userdets->lastname);
       	
			$mailSender = &JFactory::getMailer();
			$mailSender->addRecipient( $email );
			$mailSender->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
			$mailSender->setSubject( $subject );
			$mailSender->isHTML(  true );
			$mailSender->setBody(  $body );
			
			$mailSender->Send();
			
			$app->enqueueMessage( "You've successfully updated your Sales Volume and Sides for 2016.", "Success");
			
			$this->setRedirect(JRoute::_('index.php'));
		}
	}
	
	function actusersales() {
		$app = JFactory::getApplication();
		
		$userprofilemodel = JPATH_ROOT.'/components/'.'com_userprofile/models';
		JModelLegacy::addIncludePath( $userprofilemodel );
		$userModel =& JModelLegacy::getInstance('UserProfile', 'UserProfileModel');	
		
		if(count($_POST)) {
			$regid= $_POST['profile_id'];
			$userdets = $userModel->get_user_details("*",$regid);
			$usersales = $userModel->get_user_sales($regid);
			
			$update_sales['agent_id'] = $_POST['profile_id'];
			$update_sales['volume_2016'] = (int)preg_replace("/[^0-9]/", "", $_POST['volume_2016']);
			$update_sales['sides_2016'] = $_POST['sides_2016'];
			$update_sales['ave_price_2016'] = (int)($update_sales['volume_2016'] / $update_sales['sides_2016']);
			$update_sales['verified_2016'] = 0;
			
			$userModel->update_user_sales($update_sales);
			
			$body .= "Hello Admin,<br /><br />";
    		$body .= "A member has updated his/her Sales Volume and Sides for 2016:<br /><br />";
    		$body .= "<table>
			    		<tr>
			    		<td>Agent ID: </td>
			    		<td>".stripslashes_all($update_sales['agent_id'])."</td>
			    		</tr>
						<tr>
			    		<td>First Name: </td>
			    		<td>".stripslashes_all($userdets->firstname)."</td>
			    		</tr>
			    		<tr>
			    		<td>Last Name: </td>
			    		<td>".stripslashes_all($userdets->lastname)."</td>
			    		</tr>
                        <tr>
                        <td>2016 Sales Volume: </td>
                        <td>".stripslashes_all($_POST['volume_2016'])."</td>
                        </tr>
			    		<tr>
                        <td>2016 Sales Volume: </td>
                        <td>".stripslashes_all($update_sales['sides_2016'])."</td>
                        </tr>
			    		<tr>
    				</table>";
    		$body .= "<br /><br />";
    		$body .= "To verify the numbers please log in to <a href=\"".JUri::root()."administrator/index.php?option=com_useractivation\">".JUri::root()."administrator/index.php?option=com_useractivation</a>";
    		
    		$email = array('rroque@agentbridge.com','focon@agentbridge.com','mark.obre@keydiscoveryinc.com' );
			//$email = array('francis.alincastre@keydiscoveryinc.com','mark.obre@keydiscoveryinc.com', );
    		$subject = 'Agent Sales Volume and Sides Update for 2016 - ' . stripslashes_all($userdets->firstname) . ' ' . stripslashes_all($userdets->lastname);
       	
			$mailSender = &JFactory::getMailer();
			$mailSender->addRecipient( $email );
			$mailSender->setSender( array(  'ebooker@agentbridge.com' , 'AgentBridge') );
			$mailSender->setSubject( $subject );
			$mailSender->isHTML(  true );
			$mailSender->setBody(  $body );
			
			$mailSender->Send();
			
			$_SESSION['update_sales'] = 1;
		}
	}
	
	function setUpdateSalesSession() {
		$_SESSION['update_sales'] = 1;
	}
	
	function getGeoLocation() {
		$url = "http://api.wipmania.com/jsonp"; 
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		// Get the data:
		$json = curl_exec($ch);
		curl_close($ch);
		echo $json;
		//$result = json_decode($json);
		
		print_r($result);
	}
}
	
?>