<?php
/********************************************************************************
* This script is brought to you by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: info@vasplus.info
*********************************************************************************/

$uploaded_files_location = '../agent_bridge/uploads/'; //This is the directory where uploaded files are saved on your server

$name = str_replace(' ', '', basename($_FILES['file_to_upload']['name']));

$final_location = $uploaded_files_location . $name; 
 
if (move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $final_location)) 
{
	//Here you can save the uploaded file to your database if you wish before the success message below	
	echo "file_uploaded_successfully"; 
} 
else 
{
	echo "file_upload_was_unsuccessful";
}

?>