(function( $ ){

  $.fn.formchecker = function() {
  	
	var message;
	
	var error = false;
	
	var error_message = $("<div class=\"error_msg\"></div>");
	
	$(".error_msg").remove();
	$("input").removeClass("error_notifi");
	
   this.each(function() {
	
		var x = $(this).offset();
		var message_icon = "<img width=\"14\" height=\"14\" style=\"margin-left:" + ( x.left + 20 ) + "px; margin-top:5px;\" src=\"images/error_icon.jpg\"> ";
			
		if( $(this).hasClass('isemail') == true ){
						
				if( $(this).val().search(/[a-z0-9-_.]+@[a-z0-9-_]+\.[a-z0-9.]{2,}/i) == -1 ){
					
					message = "Invalid Email Address!";
					error = true;
					
				}
				
		}
				
		if( $(this).hasClass('isnumber') ){

				if( $(this).val().search(/[0-9]+/) == -1 ){
				
					message = "Only numbers are allowed in this input!";
					error = true;
				
				}
				
		}
				
		if( $(this).hasClass('isrequired') ){
				
				if( $(this).val() == "" ){
				
					message = "This field is required!";
					error = true;
				
				}
				
		}	
			
		if(error){	
			$(this).addClass("error_notifi");
			error_message.html( message_icon + message );
			error_message.css({
				"position": "absolute",
				"top" : x.top + ($(this).height() + 20),
				"left": x.left,
			})
			
			$("body").append(error_message);
			return false;
		}
		
    });
		
		return (error==true) ? false : true;
		
  };
  

})( jQuery );