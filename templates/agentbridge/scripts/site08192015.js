var $jqnocon = jQuery.noConflict();

var Landing = {
	// / FORMAT THE HOMEPAGE
	// / Format includes initial var declarations
	// / and window resize functions
	format : function() {

		var path = window.location.pathname.substring(1);
		var pathx=path+window.location.search;
		if( path=="~forqa/index.php/component/activitylog/" || 
			path=="index.php/component/activitylog/" ||
			path=="component/activitylog/" ||
			path=="~forqa/index.php/component/securityquestion/" ||
			pathx=="~forqa/index.php?option=com_securityquestion" ||
			pathx=="~forqa/index.php/component/userprofile/?task=aboutus" ||
			pathx=="~forqa/index.php/component/userprofile/?task=membership" ||
			pathx=="~forqa/index.php/component/userprofile/?task=contactus" ||
			pathx=="~forqa/index.php/component/nrds/?task=contactus"	||
			pathx=="~forqa/index.php/component/nrds/component/nrds/?task=manual"	||
			pathx=="~forqa/"	||
			pathx=="~forqa/index.php"
			) {
				$jqnocon("section.main-content").css('min-height', '700px');
				if(
					pathx!="~forqa/index.php/component/nrds/?task=contactus" && 
					pathx!="~forqa/index.php/component/nrds/component/nrds/?task=manual" && 
					pathx!="~forqa/" && 
					pathx!="~forqa/index.php" &&
					pathx!="~forqa/index.php/component/securityquestion/" &&
					pathx!="~forqa/index.php?option=com_securityquestion")
				$jqnocon("section.main-content").height($jqnocon(document).height());
			
			} else {
				pathw=path+window.location.search;
				if(pathw!="~forqa/index.php/component/propertylisting/?task=view" && pathw!="~forqa/index.php/component/propertylisting/?task=buyers" && pathw!="~forqa/index.php/component/propertylisting/?task=referrals"){

					console.log(pathw);
					var prxy = this; 
					$jqnocon("section.main-content").css('min-height', $jqnocon("section.main-content #content").height()+'px');
					$jqnocon("section.main-content").css('height', '100%');
					//$jqnocon("section.main-content").css('height', ($jqnocon("section.main-content").height())+"px");
					var docuheight=$jqnocon("section.main-content #content").height()-(jQuery("section.header").height()+jQuery("section.footer").height());
					if(docuheight<0){
						var docuheight=$jqnocon("section.main-content #content .profile").height()-(jQuery("section.header").height()+jQuery("section.footer").height());
					}
					var windheight=$jqnocon(window).height()-(jQuery("section.header").height()+jQuery("section.footer").height());
					
					console.log(docuheight);
					console.log(windheight);
					console.log(docuheight-windheight);

					if(docuheight<windheight){
						$jqnocon("section.main-content").height(windheight-68);
					}else {
						$jqnocon("section.main-content").height($jqnocon(document).height()-(jQuery("section.header").height()+jQuery("section.footer").height()));
					}

					//$jqnocon("section.main-content").height($jqnocon(document).height()-(jQuery("section.header").height()+jQuery("section.footer").height()));
					//$jqnocon("section.main-content").height(
					//$jqnocon(document).height());
						//	- $jqnocon("section.header").height()
						//- $jqnocon("section.footer").height());

				}
			}			

	}
}

$jqnocon(window).load(function() {
    //jQuery.placeholder.shim();
	Landing.format();
});
$jqnocon(window).resize(function() {
    //jQuery.placeholder.shim();
	Landing.format();
});

$jqnocon(document).ready(function() {
	$jqnocon("#accsettings").click(function() {
		if ($jqnocon(".user-menu-low").is(":visible")) {
			$jqnocon(".user-menu-low").hide();
			$jqnocon("#accsettings").css('font-weight', '');
		} else {
			$jqnocon(".user-menu-low").show();
			$jqnocon("#accsettings").css('font-weight', 'bold');
		}
	});
	$jqnocon(".texture-overlay").height($jqnocon(document).height());
	$jqnocon("#searchdown").click(function() {
		$jqnocon("#searchdropdown").attr('size', '3');
	});
	$jqnocon("#searchdropdown").change(function() {
		$jqnocon("#searchdropdown").attr('size', '1');
	});
	$jqnocon("select").select2();
	/*$jqnocon("a.user-avatar img").each(function(){
		var parent = $jqnocon(this).parent();
		if($jqnocon(this).width()>$jqnocon(this).height()){
			$jqnocon(this).css('height',parent.height());
			$jqnocon(this).css('margin-left', -($jqnocon(this).width()-parent.width())/2);

		}
		else{
			$jqnocon(this).css('width',parent.width());
			$jqnocon(this).css('margin-top', -($jqnocon(this).height()-parent.height())/2);
		}
	});*/
});

		(function($) {
			$.fn.getHiddenDimensions = function(includeMargin) {
				var $item = this, props = {
					position : 'absolute',
					visibility : 'hidden',
					display : 'block'
				}, dim = {
					width : 0,
					height : 0,
					innerWidth : 0,
					innerHeight : 0,
					outerWidth : 0,
					outerHeight : 0
				}, $hiddenParents = $item.parents().andSelf().not(':visible'), includeMargin = (includeMargin == null) ? false
						: includeMargin;

				var oldProps = [];
				$hiddenParents.each(function() {
					var old = {};

					for ( var name in props) {
						old[name] = this.style[name];
						this.style[name] = props[name];
					}

					oldProps.push(old);
				});

				dim.width = $item.width();
				dim.outerWidth = $item.outerWidth(includeMargin);
				dim.innerWidth = $item.innerWidth();
				dim.height = $item.height();
				dim.innerHeight = $item.innerHeight();
				dim.outerHeight = $item.outerHeight(includeMargin);

				$hiddenParents.each(function(i) {
					var old = oldProps[i];
					for ( var name in props) {
						this.style[name] = old[name];
					}
				});

				return dim;
			}
		}(jQuery));
		
function addparams(part){
	url = location.href;
	return (url.indexOf("?") != -1 ? url.split("?")[0]+"?"+part+"&"+url.split("?")[1] : (url.indexOf("#") != -1 ? url.split("#")[0]+"?"+part+"#"+ url.split("#")[1] : url+'?'+part));
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, ((jQuery(window).height() - jQuery(this).outerHeight()) / 2) + 
    									jQuery(window).scrollTop()) + "px");
    this.css("left", Math.max(0, ((jQuery(window).width() - jQuery(this).outerWidth()) / 2) + 
    									jQuery(window).scrollLeft()) + "px");
    return this;
}

jQuery.expr[':'].regex = function(elem, index, match) {
	var matchParams = match[3].split(','), validLabels = /^(data|css):/, attr = {
		method : matchParams[0].match(validLabels) ? matchParams[0].split(':')[0]
				: 'attr',
		property : matchParams.shift().replace(validLabels, '')
	}, regexFlags = 'ig', regex = new RegExp(matchParams.join('').replace(
			/^\s+|\s+$/g, ''), regexFlags);
	return regex.test($jqnocon(elem)[attr.method](attr.property));
}