/********************************************************************************
* This script is brought to you by x Programming Blog
* Website: www.vasplus.info
* Email: info@vasplus.info
*********************************************************************************/

//This does the file Uploads
jQuery(document).ready(function()
{
	var uploaded_files_location = "uploads";
	new AjaxUpload(jQuery('#vpb_upload_button'),
	{
		action: appname+'/uploader.php',
		name: 'file_to_upload',
		onSubmit: function(file, file_extensions)
		{
			jQuery('.vpb_main_demo_wrapper').show(); //This is the main wrapper for the uploaded items which is hidden by default

			//pdf|docx|doc|rtf|txt
			//Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.
			if (!(file_extensions && /^(jpg|jpeg)$/.test(file_extensions)))
			{
				//If file format is not allowed then, display an error message to the user
				jQuery('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, you can only upload JPG files Thanks...</div>');
				return false;
			}
			else
			{
				//jQuery('#vpb_uploads_error_displayer').html('<div class="uplading_image">Uploading</div>');
			  return true;
			}
		},
		onComplete: function(file, response)
		{
			jQuery('.vpb_main_demo_wrapper').hide();

			if(response === "file_uploaded_successfully")
			{
				jQuery('#vpb_uploads_error_displayer').html(''); //Empty the error message box

				file = file.replace( ' ', '' );
				//Check the type of file uploaded and display it rightly on the screen to the user and that's cool man
				var type_of_file_uploaded = file.substring(file.lastIndexOf('.') + 1); //Get files extensions

				var files_name_without_extensions = file.substr(0, file.lastIndexOf('.')) || file;
				vpb_file_names = files_name_without_extensions.replace(/[^a-z0-9 \s]/gi, '').replace(/[_\s]/g, '');

				if(type_of_file_uploaded == "gif" || type_of_file_uploaded == "GIF" || type_of_file_uploaded == "JPEG" || type_of_file_uploaded == "jpeg" || type_of_file_uploaded == "jpg" || type_of_file_uploaded == "JPG" || type_of_file_uploaded == "png" || type_of_file_uploaded == "PNG")
				{
					/* $('#vpb_uploads_displayer').append('<div class="vpb_image_wrappers" id="fileID'+vpb_file_names+'" align="center"><div class="vpb_image_names">'+file+'</div><img src="/agentbridge/'+uploaded_files_location+'/'+file+'" class="vpb_image_design" />'+
					'<input type="hidden" name="jform_image[]" value="' + file + '" />'
					+'<div id="vpb_remove_button'+vpb_file_names+'" class="vpb_remove_button" onclick="remove_unwanted_file(\''+vpb_file_names+'\',\''+file+'\');">Remove</div></div>'); */
					//alert('asdf');
					jQuery('#add-photo-dynamic').parent()
						.remove();
						//.promise()
						//.done(function(){
					//jQuery('#imagegallery').append('<li class="nah"><img style="position: absolute; width: 75px; display:none" src="http://microsofthelpnow.com/wp-content/uploads/2011/10/delete-icon.png" /><a class=\"add-photo\" id="fileID'+vpb_file_names+'"><img src="/agentbridge/'+uploaded_files_location+'/'+file+'" height=\"77\" width=\"77\" /> <input type="hidden" name="jform_image[]" value="' + file + '" /></a></li>')
						//		.promise()
						//		.done(function(){
					//				jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
					jQuery("#image_to_crop").width('');
					jQuery("#image_to_crop").attr('src', '');
					jQuery("#image_to_crop").unbind('load');
					jQuery("#image_to_crop").attr('src', appname+uploaded_files_location+'/'+file);
					//console.log('call me '+ appname+uploaded_files_location+'/'+file);
					jQuery("#image_to_crop").load(function(){
						var dimensions = jQuery("#image_to_crop").getHiddenDimensions();
						console.log(dimensions);
						jQuery("input[name=\"origwidth\"]").val(dimensions.width);
						jQuery("input[name=\"origheight\"]").val(dimensions.height);
						if(dimensions.width > dimensions.height){
							if(dimensions.width > 1000){
								jQuery("#image_to_crop").width('800');
								jQuery("input[name=\"resized\"]").val('true');
							}
							else{
								jQuery("input[name=\"resized\"]").val('');
							}
						}
						else{
							if(dimensions.height > 1000){
								jQuery("#image_to_crop").height('800');
								jQuery("input[name=\"resized\"]").val('true');
							}
							else{
								jQuery("input[name=\"resized\"]").val('');
							}
						}
						jQuery("#cropimage").dialog({
							modal:true,
							width: 'auto',
							close: function( event, ui ) {
										jQuery("#image_to_crop").width('');
										jQuery("#image_to_crop").height('');
										jQuery("input[name=\"resized\"]").val('');
										jQuery('#image_to_crop').imgAreaSelect({remove:true});
										jQuery(".submitform").unbind('click');
										jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
									},
							dragStart: function( event, ui ) {
										jQuery('#image_to_crop').imgAreaSelect({remove:true});
									},
							dragStop: function(){
								jQuery('#image_to_crop').imgAreaSelect({
									x1: 0,
									y1: 0,
									x2: 100,
									y2: 100,
									onSelectEnd: function (img, selection) {
													jQuery('input[name="x1"]').val(selection.x1);
													jQuery('input[name="y1"]').val(selection.y1);
													jQuery('input[name="x2"]').val(selection.x2);
													jQuery('input[name="y2"]').val(selection.y2);
													jQuery('input[name="src"]').val(jQuery(img).attr('src'));

										        	handles: true;
									        	}
							})
							},
							open: function(){
								jQuery('#image_to_crop').imgAreaSelect({
									x1: 0,
									y1: 0,
									x2: 100,
									y2: 100,
									onSelectEnd: function (img, selection) {
													jQuery('input[name="x1"]').val(selection.x1);
													jQuery('input[name="y1"]').val(selection.y1);
													jQuery('input[name="x2"]').val(selection.x2);
													jQuery('input[name="y2"]').val(selection.y2);
													jQuery('input[name="src"]').val(jQuery(img).attr('src'));

										        	handles: true;
									        	}
								});
							}
						});
					});
					jQuery(".submitform").click(function(){
						console.log(jQuery("#cropform").serialize());
						jQuery.post(
							jQuery("#cropform").attr('action'),
							jQuery("#cropform").serialize(),
							function(data){
								console.log('called'+data);
								jQuery('#image_to_crop').imgAreaSelect({remove:true});
								var filename = data.replace(/^.*[\\\/]/, '');
								jQuery('#imagegallery').append('<li class="nah"><img style="position: absolute; width: 27px; display:none" src="images/delete-icon.png" /><a class=\"add-photo\" id="fileID'+filename+'"><img src="'+data+'" height=\"77\" width=\"77\" /> <input type="hidden" name="jform_image[]" value="' + filename.trim() + '" /></a></li>')
								jQuery(".submitform").unbind('click');
								jQuery("li.nah a.add-photo").hover(function(){
									jQuery(this).css('opacity', '0.4');
									jQuery(this).prev().show();
									console.log(jQuery(this).prev());
								});
								jQuery("li.nah a.add-photo").mouseout(function(){
									jQuery(this).css('opacity', '');
									jQuery(this).prev().hide();
								});
								jQuery("li.nah a.add-photo").click(function(){
									jQuery(this).parent().remove();
								});
								jQuery("#cropimage").dialog("close");
							}
						);
					});
				}
				else if(type_of_file_uploaded == "doc" || type_of_file_uploaded == "docx" || type_of_file_uploaded == "rtf" || type_of_file_uploaded == "DOC" || type_of_file_uploaded == "DOCX" || type_of_file_uploaded == "RTF")
				{
					jQuery('#vpb_uploads_displayer').append("<div class='vpb_image_wrappers' id='fileID"+vpb_file_names+"' style='padding-top:60px;padding-bottom:70px;' align='center'><span class='ccc'><a href='"+appname+uploaded_files_location+"/"+file+"' target='_blank'>Microsoft Word Document<br><br>Click here to download it</a></span><div id='vpb_remove_button"+vpb_file_names+"' class='vpb_remove_button' onclick='remove_unwanted_file(\""+vpb_file_names+"\",\""+file+"\");'>Remove</div></div>");
				}
				else if(type_of_file_uploaded == "pdf" || type_of_file_uploaded == "PDF")
				{
					jQuery('#vpb_uploads_displayer').append("<div class='vpb_image_wrappers' id='fileID"+vpb_file_names+"' style='padding-top:60px;padding-bottom:70px;' align='center'><span class='ccc'><a href='"+appname+uploaded_files_location+"/"+file+"' target='_blank'>PDF Document<br><br>Click here to view it</a></span><div id='vpb_remove_button"+vpb_file_names+"' class='vpb_remove_button' onclick='remove_unwanted_file(\""+vpb_file_names+"\",\""+file+"\");'>Remove</div></div>");
				}
				else if(type_of_file_uploaded == "txt" || type_of_file_uploaded == "TXT")
				{
					jQuery('#vpb_uploads_displayer').append("<div class='vpb_image_wrappers' id='fileID"+vpb_file_names+"' style='padding-top:60px;padding-bottom:70px;' align='center'><span class='ccc'><a href='"+appname+uploaded_files_location+"/"+file+"' target='_blank'>Text File Document<br><br>Click here to view it</a></span><div id='vpb_remove_button"+vpb_file_names+"' class='vpb_remove_button' onclick='remove_unwanted_file(\""+vpb_file_names+"\",\""+file+"\");'>Remove</div></div>");
				}
				else
				{
					jQuery('#vpb_uploads_displayer').append("<div class='vpb_image_wrappers' id='fileID"+vpb_file_names+"' style='padding-top:80px;padding-bottom:90px;' align='center'>"+file+" uploaded<br clear='all'><div id='vpb_remove_button"+vpb_file_names+"' class='vpb_remove_button' onclick='remove_unwanted_file(\""+vpb_file_names+"\",\""+file+"\");'>Remove</div></div>");
				}
			}
			else
			{
				jQuery('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, your file upload was unsuccessful. Please reduce the size of your file and try again or contact this site admin to report this error message if the problem persist. Thanks...</div>');
			}
		}
	});

});


//This removes all unwanted files
function remove_unwanted_file(id,file)
{
	if(confirm("If you are sure that you really want to remove the file "+file+" then click on OK otherwise, Cancel it."))
	{
		var dataString = "file_to_remove=" + file;
		jQuery.ajax({
			type: "POST",
			url: appname+"remove_unwanted_files.php",
			data: dataString,
			cache: false,
			beforeSend: function()
			{
				jQuery("#vpb_remove_button"+id).html('<img src="'+appname+'templates/agentbridge/images/loadings.gif" align="absmiddle" />');
			},
			success: function(response)
			{
				jQuery('div#fileID'+id).fadeOut('slow');
			}
		});
	}
	return false;
}