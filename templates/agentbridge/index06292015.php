<? ob_start("ob_gzhandler"); ?> 

<?php
defined('_JEXEC') or die;
// Getting params from templatetop
//var_dump(JFactory::getSession()->getState());
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}



// Add Stylesheets
// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);
// Add current user information
$user = JFactory::getUser();
// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}
// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JURI::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
$classes = "";
if((trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_propertylisting" || trim($_REQUEST['option'])=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){
	$classes.="main ";
}
if(trim($_REQUEST['option'])=="com_customsearch" || (trim($_REQUEST['option']) == "com_propertylisting" && trim($_REQUEST['task']) == "agreement") || (trim($_REQUEST['option']) == "com_userprofile" && (trim($_REQUEST['task']) == "help" || trim($_REQUEST['task']) == "mobilehelp")  )){
	$classes.="searchbody ";
}

//detect mobile device
$useragent=$_SERVER['HTTP_USER_AGENT'];
$is_mobile=0;
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
	$is_mobile=1;
//detect mobile device

if($is_mobile==1){
	$url_support= $this->baseurl."/index.php/component/userprofile/?task=mobilehelp";
} else{
	$url_support= $this->baseurl."/index.php/component/userprofile/?task=help";
}


function getCountryLanguages(){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("

			SELECT 

				cl.*, ct.countries_iso_code_2,ct.countries_name

			FROM 

				#__country_languages AS cl

			LEFT JOIN #__countries ct ON ct.countries_id = cl.country

			ORDER BY cl.pkID
			
			"

		);

		$db->setQuery($query);
		
		return $db->loadObjectList();
}



                  


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
>
<head>
<jdoc:include type="head" />
<link rel="icon" type="image/png" href="favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=414, initial-scale=0, maximum-scale=1.0, user-scalable=yes">
<meta name="description" content="AgentBridge is the most powerful global network of real estate professionals. By agents and for agents, AgentBridge provides a secure place to privately share information on properties and clients." />
<meta name="keywords" content="Real Estate Network, Pocket Listings, listings" />
<script>
var $jquery = jQuery;
<?php 
$user = JFactory::getUser();
$status = $user->guest;

if($status != 1){ ?>

//Your timing variables in number of seconds
 
//total length of session in seconds
var sessionLength = (30*60) <?php  // echo JFactory::getSession()->getExpire();?>;
//var sessionLength = 10;
//time warning shown (10 = warning box shown 10 seconds before session starts)
var warning = 10; 
//time redirect forced (10 = redirect forced 10 seconds after session ends)    
var forceRedirect = 10;
 
//time session started
var pageRequestTime = new Date();
 
//session timeout length
var timeoutLength = sessionLength*1000;
 
//set time for first warning, ten seconds before session expires
var warningTime = timeoutLength - (warning*1000);
 
//force redirect to log in page length (session timeout plus 10 seconds)
var forceRedirectLength = timeoutLength + (forceRedirect*1000);
 
//set number of seconds to count down from for countdown ticker
var countdownTime = warning;
 
//warning dialog open; countdown underway
var warningStarted = false;

function CheckForSession() {

	//get time now
    var timeNow = new Date();
     
    //event create countdown ticker variable declaration
    var countdownTickerEvent;  
     
    //difference between time now and time session started variable declartion
    var timeDifference = 0;
     
    timeDifference = timeNow - pageRequestTime;
          
    if (timeDifference > forceRedirectLength)
        {   
           //clear (stop) checksession event
            clearInterval(checkSessionTimeEvent);
 			$jquery("#logout_link").click();
            //force relocation
           // window.location="login.php?expired=true";
        }

}

var checkSessionTimeEvent;

$jquery(document).ready(function(){
	//alert('test response');
	checkSessionTimeEvent = setInterval("CheckForSession()", 30*60*1000);

});

<?php }?>



var appname = "<?php echo JUri::root(); ?>";
var hover_dd= function(){
	$jquery("#dhtmlpointer").css("display","none");
	$jquery("#dhtmltooltip").css("display","none");
	if (!$jquery("#dhtmltooltip").css('visibility') !== 'hidden') {
		$jquery("#dhtmlpointer").fadeIn("slow");
		$jquery("#dhtmltooltip").fadeIn("slow");
	}
}
//NS
function SiteSeal(img,type){
if(window.location.protocol.toLowerCase()=="https:"){var mode="https:";} else {var mode="http:";}
var host=location.host;
var baseURL=mode+"//seals.networksolutions.com/siteseal_seek/siteseal?v_shortname="+type+"&v_querytype=W&v_search="+host+"&x=5&y=5";
document.write('<a href="#" onClick=\'window.open("'+baseURL+'","'+type+'","width=450,height=500,toolbar=no,location=no,directories=no,\ status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no");return false;\'>\<img src="'+img+'" style="border:none;" oncontextmenu="alert(\'This SiteSeal is protected\');return false;"></a>');}
//TOOLTIP
var offsetfromcursorX=-20 //Customize x offset of tooltip
var offsetfromcursorY=10 //Customize y offset of tooltip
var offsetdivfrompointerX=10//Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY=10 //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).
document.write('<div id="dhtmltooltip"></div>') //write out tooltip DIV
document.write('<img id="dhtmlpointer">') //write out pointer image
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""
var pointerobj=document.all? document.all["dhtmlpointer"] : document.getElementById? document.getElementById("dhtmlpointer") : ""
function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function ddrivetip(thetext, thewidth, thecolor){
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}
function positiontip(e){
if (enabletip){
var nondefaultpos=false
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var winwidth=ie&&!window.opera? ietruebody().clientWidth : window.innerWidth-20
var winheight=ie&&!window.opera? ietruebody().clientHeight : window.innerHeight-20
var rightedge=ie&&!window.opera? winwidth-event.clientX-offsetfromcursorX : winwidth-e.clientX-offsetfromcursorX
var bottomedge=ie&&!window.opera? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY
var leftedge=(offsetfromcursorX<0)? offsetfromcursorX*(-1) : -1000
//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<tipobj.offsetWidth){
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=curX-tipobj.offsetWidth+"px"
nondefaultpos=true
}
else if (curX<leftedge)
tipobj.style.left="5px"
else{
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetfromcursorX-offsetdivfrompointerX+"px"
pointerobj.style.left=curX+offsetfromcursorX+"px"
}
//same concept with the vertical position
if (bottomedge<tipobj.offsetHeight){
tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px"
nondefaultpos=true
}
else{
tipobj.style.top=curY+offsetfromcursorY+offsetdivfrompointerY+"px"
pointerobj.style.top=curY+offsetfromcursorY+"px"
}
tipobj.style.visibility="visible"
if (!nondefaultpos)
pointerobj.style.visibility="visible"
else
pointerobj.style.visibility="hidden"
}
}
function hideddrivetip(){
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
pointerobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}
document.onmousemove=positiontip
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/skin.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/base.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/fonts.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/media-query.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/jqueryui-custom.css"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/jqtransform.css" type="text/css" media="all"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/select2/select2.css"/>
<style>

 #choose-lang{
 	cursor: pointer;
 	background-color:#414141;
 }

 #sel-lang {
 	cursor: pointer;
	padding-left: 10px;
 }
 
  #choose-lang2{
 	cursor: pointer;
 	background-color:#009BD0;
 }

 #sel-lang2 {
 	cursor: pointer;
	padding-left: 5px;
	padding-right:5px;
	font-size:12px;
  font-weight: bold;
  margin-top: -2px;	
 }

 

</style>


<script>

<?php 
	
	if(JFactory::getUser()->email){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*')->from('#__user_registration ur_u')->where('email = \''.JFactory::getUser()->email.'\'');

		$query->leftJoin('#__country_currency curc ON curc.country = ur_u.country');

		$db->setQuery($query);

		$userinfo = $db->loadObject();

		$user_currency = $userinfo->currency;
	
?>

function format(num){
	var num = Math.round(num);
    var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
    if(str.indexOf(".") > 0) {
        parts = str.split(".");
        str = parts[0];
    }
    str = str.split("").reverse();
    for(var j = 0, len = str.length; j < len; j++) {
        if(str[j] != ",") {
            output.push(str[j]);
            if(i%3 == 0 && j < (len - 1)) {
                output.push(",");
            }
            i++;
        }
    }
    formatted = output.reverse().join("");
    return("$" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
};



<?php } ?>

	jQuery(document).ready(function(){
	
    //#loading-image_custom_question

    jQuery("form").submit(function (e) {
       // e.preventDefault();
      //  var formId = this.id;  // "this" is a reference to the submitted form
      jQuery("#loading-image_custom_question").show();
    });


    jQuery('#sel-lang').click(function(){
        
        var status = jQuery(this).attr('data');
      
        jQuery('#changelanguage').dialog(
            {
              title: "Choose Language",
               width:'200'
            });


    });

     jQuery('#sel-lang2').click(function(){
        
        var status = jQuery(this).attr('data');
      
        jQuery('#changelanguage').dialog(
            {
              title: "Choose Language",
               width:'200'
            });


    });

		/*jQuery("#sel-lang").click(function(){
		jQuery ( ".modalDialog" ).css( {"opacity":"1","pointer-events":"auto"});
		});

    jQuery("#sel-lang2").click(function(){
      jQuery( ".modalDialog" ).css( {"opacity":"1","pointer-events":"auto"});
    });*/

    jQuery("#closemodal").click(function(){
      jQuery( ".modalDialog" ).css( {"opacity":"0","pointer-events":"none"});
      jQuery("#ch-cntry").animate({"margin-left":"0px"},"very slow");
      jQuery("#ch-lang div.langs").css("display","none");
      return false;
    });


   <?php  if($status == 1){?>
            <?php   if(isset($_GET['lang']) && $_GET['lang'] != "") {?>

              <?php $lang_Exp = explode("-", $_GET['lang']);?>
		
              jQuery("#sel-lang").prepend(jQuery("#<?php echo $_GET['lang'] ?>").html());
              jQuery("#sel-lang").addClass("<?php echo $_GET['lang'] ?>");
              jQuery("#sel-lang").attr("onmouseover","ddrivetip('<?php echo $lang_Exp[1]?> <?php echo ucfirst($lang_Exp[0])?>');hover_dd()");
              jQuery("#sel-lang").attr("onmouseout","hideddrivetip();hover_dd()");


              jQuery("#<?php echo $_GET['lang'] ?>").css("display","none");
              jQuery("#lang-id").val("<?php echo $_GET['lang'] ?>");
              

            <?php } else {?>
              jQuery("#sel-lang").prepend('<img class="ctry-flag" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/images/us-flag-lang.png">');
              jQuery("#sel-lang").attr("onmouseover","ddrivetip('US English');hover_dd()");
              jQuery("#sel-lang").attr("onmouseout","hideddrivetip();hover_dd()");
              
              jQuery("#sel-lang").addClass("english-US");
              jQuery("#english-US").css("display","none");
              jQuery("#lang-id").val("english-US");

             <?php }?>
        <?php } else {


                  $user = JFactory::getUser();
                  $session = JFactory::getSession();
                  $session->set('user', new JUser($user->id));
                  
                  $user = JFactory::getUser(); 
                  $jinput = JFactory::getApplication()->input;

                  $language = JFactory::getLanguage();
                  $extension = $jinput->get('option');
                  $base_dir = JPATH_SITE;
                  $language_tag = $user->currLanguage;

                 
                  $language->load($extension, $base_dir, $language_tag, true);
              ?>

              <?php $lang_Exp = explode("-", $user->currLanguage);?>

              jQuery("#sel-lang2").prepend(jQuery("#<?php echo $user->currLanguage ?>").html());
              jQuery("#sel-lang2").attr("onmouseover","ddrivetip('<?php echo $lang_Exp[1]?> <?php echo ucfirst($lang_Exp[0])?>');hover_dd()");
              jQuery("#sel-lang2").attr("onmouseout","hideddrivetip();hover_dd()");
              jQuery("#sel-lang2").addClass("<?php echo $user->currLanguage ?>");

        <?php }?>


        jQuery(".ch-cnt").click(function(){


			    jQuery( "#ch-cntry" ).hide();
			   	jQuery( "#ch-lang" ).show();

        		var thisclass=jQuery(this).attr("id");

        		
        		jQuery("."+thisclass).css("display","block");
        		jQuery(".arrow-right").attr("id",thisclass);

        });

        jQuery(".arrow-right").click(function(){
				jQuery( "#ch-cntry" ).show();
				jQuery( "#ch-lang" ).hide();

        		var thisclass=jQuery(this).attr("id");

        	
        		jQuery("."+thisclass).css("display","none");

        });

		jQuery(".lang-options").click(function(){

    <?php  if($status != 1){ $jinput = JFactory::getApplication()->input;?>
          var this_langId= jQuery(this).attr("id");
              jQuery.ajax({
                url: "<?php echo $this->baseurl?>/index.php?option=com_users&task=setLanguage&format=raw",
                type: "POST",
                data: {compName:"<?php echo $jinput->get('option')?>",langId:this_langId},
                success: function (data){
                 location.reload();
                }
              });


    <?php  }else{?>

         
        window.location = "<?php echo $this->baseurl; ?>/?lang="+jQuery(this).attr("id");
    <?php   }?>
			
		});

	});
</script>

</head>
<body class="<?php echo $classes ?>">
	<div id="loading" style="display:none">
		<div class="loading-bg"></div>
		<div class="icon-wrap icon-effect">
		            <div class="icon">Settings</div>
		            		<div class="loading-img">  </div>
		        </div>

	</div>
	<div class="outer-wrapper">
		<!-- start outer wrapper -->
		<?php if((isset($_REQUEST['view']) && $_REQUEST['view']=="article") && (isset($_REQUEST['id']) &&  $_REQUEST['id']=="1")){ ?>
		<div class="row header-row"> <!-- Start Header Row -->
			<div class="header1"><a href="<?php echo JRoute::_(""); ?>" class="agent-bridge-logo"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/main-logo.png"></a></div>
		</div> <!-- End Header Row -->
		<?php } ?>
		<?php if((trim($_REQUEST['option'])=="com_hikashop" || trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_propertylisting" || trim($_REQUEST['option'])=="com_home" || trim($_REQUEST['option'])=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){ ?>
		<section class="header">
		<!-- start header -->
		<div class="wrapper">
			<a href="<?php echo JRoute::_(""); ?>" class="logo ab-logo left" alt="Agent Bridge"></a>
			<div class="top-right right">
			<?php $task = JRequest::getVar('task'); ?>
			<?php if ( 
				($task!="set_pass") &&
				($task!="payments") &&
				($task!="continue_complete") &&
				($task!="term_acceptance")
				
			) { 
				 if(isset($_SESSION['selected']) && $_SESSION['selected']=="POPs"){
				 	$sel_p="selected";
				 	$sel_a=" ";
				 } else {
				 	$sel_a="selected";
				 	$sel_p=" ";
				 }
				?>
			
			<div class="search-container left">
				<form
					action="<?php echo JRoute::_('index.php?option=com_customsearch');?>"
					method="get" 
					class="form-inline">
					<div id="searchdropcontainer">
						<select id="searchdropdown" class="left select_nostyle" name="task">
							<option disabled="" value="">Select</option>
							<option <?php echo $sel_p;?> value="searchlisting">POPs&#153;</option>
							<option <?php echo $sel_a;?> value="searchagent">Agent</option>
						</select>
					</div>
						<input name="searchword" type="text" placeholder="Search" class="search left" id="searchValue"/> 
						<input type="hidden" name="option" value="com_customsearch" />
						<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
						<input name="" type="submit" class="button left" />
				</form>
			</div>			
			<?php } ?>		

			<div class="right-nav left">
				<ul>
					<li>	
					<?php if ( 
					($task!="set_pass") &&
					($task!="payments") &&
					($task!="continue_complete") &&
					($task!="term_acceptance") ) { ?><a href="<?php echo JRoute::_('index.php/component/activitylog/'); ?>" class="username"><?php echo strlen($user->name)<=15 ? stripslashes($user->name) : stripslashes($userinfo->firstname[0]).". ".stripslashes($userinfo->lastname); ?></a>
					<?php } ?>
					</li>
					<li style="padding:0px;width:30px">
							<div class="lang-selection2" >
								<div id="sel-lang2"></div>
								<div class="this-overflow2">
								<?php
								        $cl_list = getCountryLanguages();
					        		   foreach($cl_list as $value){		
								?>
									<div id="<?php echo strtolower($value->lang)."-".$value->countries_iso_code_2; ?>" class="lang-options2"><img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png"></div>									
								<?php }?>
								</div>
							</div>
					</li>
					<li style="margin-top:4px">
						<form 
							id="user_logout" 
							class="form-horizontal jqtransformdone"
							action="<?php echo JRoute::_("index.php?option=com_users&task=user.logout"); ?>"
							method="post">
							<a id='logout_link' href="javascript: void(0);" onclick="document.getElementById('user_logout').submit();">Logout</a>
							<?php echo JHtml::_('form.token');?></form>
					</li>
				</ul>
			</div>
		</div>
	</div>
		</section>
		<!-- end header -->
		
		<?php }else{  

			   if(isset($_GET['lang']) && $_GET['lang']!=""){
			            $language =& JFactory::getLanguage();
			            $extension = 'com_nrds';
			            $base_dir = JPATH_SITE;
			            $language_tag = $_GET['lang'];
			            $language->load($extension, $base_dir, $language_tag, true);
			    } else {

                  $language = JFactory::getLanguage();
                  $extension = 'com_nrds';
                  $base_dir = JPATH_SITE;
                  $language_tag = "english-US";
                  $language->load($extension, $base_dir, $language_tag, true);

                 // echo "<pre>";
                 // var_dump( $language );
                 // echo "</pre>";
          }



			?>	
		<div class="row header-row" style="background-color:#ffffff"> <!-- Start Header Row -->
			<div class="header1">
				<div class="left" id="hplogo" style="margin-bottom:20px"><a href="<?php echo $this->baseurl; ?>" class="agent-bridge-logo"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/main-logo.png"></a>
				</div>
				<div class="hpform" >
					<form class="login-form right" action="<?php echo JRoute::_('index.php'); ?>" method="post" id="login-form" >
					<div class="hpform1"><input id="modlgn-username" size="27" type="text" name="username" class="text-field" tabindex="0"  required="required" placeholder="Enter Your Email" /></div>
					<div class="hpform2"><input id="modlgn-passwd" size="27" type="password" name="password" class="text-field"  required="required" tabindex="0"  placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" /><br/>
					<a href="index.php/component/users/?view=reset" class="text-link left" style="font-size:11px;margin-top:5px;margin-left:5px"><?php echo JText::_('COM_NRDS_HEAD_FORGOTPASSWORD');?></a></div>
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
          <input type="hidden" id="lang-id" name="language" value="en-US" />
					<input type="hidden" name="return" value="<?php echo (isset($_GET['returnurl'])) ? $_GET['returnurl'] : base64_encode('index.php/component/activitylog/'); ?>" />
					<?php echo JHtml::_('form.token'); ?>
					<div class="hpform3" style="position: relatve;">
					  <input style="margin-top:2px;margin-left:0" type="submit" class="submit-btn" value="<?php echo JText::_('COM_NRDS_HEAD_LOGIN');?>">
            <img height="20px" style="position: absolute;display: none;top: 46px;right: 15px;"id="loading-image_custom_question" src="<?php echo $this->baseurl; ?>/images/ajax_loader.gif" alt="Loading..." />
          </div>
					</form>
				</div>
			</div>
			<div class="clear-float"></div>
			<div class="header2">
				<ul>
				<li style="margin:0px">
					<div class="lang-selection"   style="margin-top:-2px">
						<div id="sel-lang"></div>
						<div class="this-overflow">
						<?php
								        $cl_list = getCountryLanguages();
					        		   foreach($cl_list as $value){		
								?>
									<div  id="<?php echo strtolower($value->lang)."-".$value->countries_iso_code_2; ?>" class="lang-options">
									<img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png"/></div>									
								<?php }?></div>
					</div>
				</li>

                <!--<li><a href="<?php echo JRoute::_('index.php/component/nrds/?task=contactus')?>"><?php echo JText::_('COM_NRDS_HEAD_SUPPORTLINK');?></a></li>-->
                <li><a href="<?php echo JRoute::_('index.php/component/nrds/?task=manual')?>"><?php echo JText::_('COM_NRDS_HEAD_SIGNUPLINK');?></a></li>
				</ul>
			</div>

		</div> <!-- End Header Row -->

		<?php } ?>
		
	
		<section class="main-content"><!-- start main content --> 
				<div id="content" class="<?php echo $span;?>">
			<!-- Begin Content -->
				<jdoc:include type="modules" name="position-3" style="xhtml" />
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				<jdoc:include type="modules" name="position-2" style="none" />
			<!-- End Content -->
				</div>
		</section><!-- end main content --> 
		
		
		<?php if((trim($_REQUEST['option'])=="com_hikashop") || (trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_home" || trim($_REQUEST['option'])=="com_propertylisting" ||  trim(trim($_REQUEST['option']))=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){ ?>
		
		<?php $componenent = 'userprofile'; ?>
		<section class="footer"><!-- start footer -->
		<div class="wrapper">
			<p class="left"><a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=aboutus">About Us</a> | <a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=contactus">Contact Us</a> | <a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=help">Support</a></p>
			<p class="right">© 2014 AgentBridge. All rights reserved.</p>
			</div></section>
				<!-- start footer-link -->
		<?php } else{ ?>
			<?php $componenent = 'nrds'; ?>
					<!-- start footer-link -->
					
					 <div class="row footer-row"> <!-- Start Footer -->
						<div class="wrapper"><div class="footer-logo"><span class="footer-logo-txt">About</span></div>
						<span class="footer-tagline">The most powerful global network of real estate professionals</span>
						<span class="copyright">© 2014 AgentBridge</span></div>
					</div> <?php } ?><!-- End Footer -->
	<!-- end outer wrapper -->


	<!-- modal country language-->
<!--	<div id="openModal" class="modalDialog">
	    <div class="main" >	
	    <a href="#" title="Close" id="closemodal" class="close">X</a>
	    	<div class="mainDialog">	
	    		<div>
                          <div id="ch-cntry">
                   <h2>Choose Country</h2>
             <hr/>
						 <div style="padding-bottom:10px">
							<?php
							       $prev_val=null;
							       $cl_list = getCountryLanguages();				    
				        		   foreach($cl_list as $value){		
							?>
								<?php if($prev_val!=$value->countries_iso_code_2){?>
							    	<div class="ch-cnt" id="div-<?php echo $value->countries_iso_code_2?>">
							    	<h4><img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png"> 
							    		<?php echo $value->countries_name;?> </h4><br/>
					           		</div>
					            <?php }?>
							<?php 
									$prev_val = $value->countries_iso_code_2;
							}?>
						</div>
					</div>
					<div id="ch-lang" style="display:none">
						<div>
							<div class="arrow-right"></div>	
							<h2>Choose Language</h2>				
							<hr/>							   		
							<?php
						       $prev_val=null;
						       $cl_list = getCountryLanguages();
						    
			        		   foreach($cl_list as $value){		
							?>
								<div class="langs div-<?php echo $value->countries_iso_code_2?>" style="display:none">
				            	 <div id="<?php echo strtolower($value->lang)."-".$value->countries_iso_code_2; ?>" class="lang-options"><?php echo $value->lang; ?></div>  
				             	</div>
							<?php 
								$prev_val = $value->countries_iso_code_2;
							}?>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
	 modal country language-->

    <!-- modal country language2-->
    <div id="changelanguage" style="display: none; padding:10px">
              <div class="mainDialog">  
                <div>
                  <div id="ch-cntry">
                   <div style="margin-bottom: -20px;">
                    <?php
                           $prev_val=null;
                           $cl_list = getCountryLanguages();            
                             foreach($cl_list as $value){   
                    ?>
                      <?php if($prev_val!=$value->countries_iso_code_2){?>
                          <div class="ch-cnt" id="div-<?php echo $value->countries_iso_code_2?>">
                          <h4><img class="ctry-flag" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png"> 
                            <?php echo $value->countries_name;?> </h4><br/>
                              </div>
                            <?php }?>
                    <?php 
                        $prev_val = $value->countries_iso_code_2;
                    }?>
                  </div>
                </div>
                <div id="ch-lang" style="display:none">
                  <div>
                    <div class="arrow-right"></div>                
                    <?php
                         $prev_val=null;
                         $cl_list = getCountryLanguages();
                      
                           foreach($cl_list as $value){ 
                                   if(strtolower($value->lang)!="french")  {
                            ?>
                              <div class="langs div-<?php echo $value->countries_iso_code_2?>" style="display:none">
                                     <div id="<?php echo strtolower($value->lang)."-".$value->countries_iso_code_2; ?>" class="lang-options"><?php echo $value->lang; ?></div>  
                                    </div>
                            <?php 
                          }
                      $prev_val = $value->countries_iso_code_2;
                    }?>
                  </div>
                </div>
              </div>
          </div>
   </div>
 
  <!-- modal country language2-->

</body>

<?php
// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript($this->baseurl."/templates/agentbridge/scripts/jquery.html5-placeholder-shim.js");
?>
<script	src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/mask.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/custom/custom.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.jqtransform.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/site.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/select2/select2.js"></script>
<script type='text/javascript' src='//cdn.jsdelivr.net/qtip2/2.0.1/basic/jquery.qtip.js'></script>
</html>
