<? ob_start("ob_gzhandler"); ?> 

<?php
defined('_JEXEC') or die;
// Getting params from templatetop
//var_dump(JFactory::getSession()->getState());
$params = JFactory::getApplication()->getTemplate(true)->params;
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}
// Add Stylesheets
// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);
// Add current user information
$user = JFactory::getUser();
// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span6";
}
elseif ($this->countModules('position-7') && !$this->countModules('position-8'))
{
	$span = "span9";
}
elseif (!$this->countModules('position-7') && $this->countModules('position-8'))
{
	$span = "span9";
}
else
{
	$span = "span12";
}
// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="'. JURI::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
$classes = "";
if((trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_propertylisting" || trim($_REQUEST['option'])=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){
	$classes.="main ";
}
if(trim($_REQUEST['option'])=="com_customsearch" || (trim($_REQUEST['option']) == "com_propertylisting" && trim($_REQUEST['task']) == "agreement") || (trim($_REQUEST['option']) == "com_userprofile" && trim($_REQUEST['task']) == "help")){
	$classes.="searchbody ";
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
>
<head>
<jdoc:include type="head" />
<link rel="icon" type="image/png" href="favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=414, initial-scale=0, maximum-scale=1.0, user-scalable=yes">
<meta name="description" content="AgentBridge is the most powerful global network of real estate professionals. By agents and for agents, AgentBridge provides a secure place to privately share information on properties and clients." />
<meta name="keywords" content="Real Estate Network, Pocket Listings, listings" />
<script>
var $jquery = jQuery;
<?php 
$user = JFactory::getUser();
$status = $user->guest;

if($status != 1){ ?>

//Your timing variables in number of seconds
 
//total length of session in seconds
var sessionLength = <?php // echo JFactory::getSession()->getExpire();?> (30*60);
//var sessionLength = 10;
//time warning shown (10 = warning box shown 10 seconds before session starts)
var warning = 10; 
//time redirect forced (10 = redirect forced 10 seconds after session ends)    
var forceRedirect = 10;
 
//time session started
var pageRequestTime = new Date();
 
//session timeout length
var timeoutLength = sessionLength*1000;
 
//set time for first warning, ten seconds before session expires
var warningTime = timeoutLength - (warning*1000);
 
//force redirect to log in page length (session timeout plus 10 seconds)
var forceRedirectLength = timeoutLength + (forceRedirect*1000);
 
//set number of seconds to count down from for countdown ticker
var countdownTime = warning;
 
//warning dialog open; countdown underway
var warningStarted = false;

function CheckForSession() {
	//get time now
    var timeNow = new Date();
     
    //event create countdown ticker variable declaration
    var countdownTickerEvent;  
     
    //difference between time now and time session started variable declartion
    var timeDifference = 0;
     
    timeDifference = timeNow - pageRequestTime;
          
    if (timeDifference > forceRedirectLength)
        {   
           //clear (stop) checksession event
            clearInterval(checkSessionTimeEvent);
 			$jquery("#logout_link").click();
            //force relocation
           // window.location="login.php?expired=true";
        }

}

var checkSessionTimeEvent;

$jquery(document).ready(function(){
	//alert('test response');
	checkSessionTimeEvent = setInterval("CheckForSession()", (30*60*1000));

});

<?php }?>



var appname = "<?php echo JUri::root(); ?>";
var hover_dd= function(){
	$jquery("#dhtmlpointer").css("display","none");
	$jquery("#dhtmltooltip").css("display","none");
	if (!$jquery("#dhtmltooltip").css('visibility') !== 'hidden') {
		$jquery("#dhtmlpointer").fadeIn("slow");
		$jquery("#dhtmltooltip").fadeIn("slow");
	}
}
//NS
function SiteSeal(img,type){
if(window.location.protocol.toLowerCase()=="https:"){var mode="https:";} else {var mode="http:";}
var host=location.host;
var baseURL=mode+"//seals.networksolutions.com/siteseal_seek/siteseal?v_shortname="+type+"&v_querytype=W&v_search="+host+"&x=5&y=5";
document.write('<a href="#" onClick=\'window.open("'+baseURL+'","'+type+'","width=450,height=500,toolbar=no,location=no,directories=no,\ status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no");return false;\'>\<img src="'+img+'" style="border:none;" oncontextmenu="alert(\'This SiteSeal is protected\');return false;"></a>');}
//TOOLTIP
var offsetfromcursorX=-20 //Customize x offset of tooltip
var offsetfromcursorY=10 //Customize y offset of tooltip
var offsetdivfrompointerX=10//Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY=10 //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).
document.write('<div id="dhtmltooltip"></div>') //write out tooltip DIV
document.write('<img id="dhtmlpointer">') //write out pointer image
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""
var pointerobj=document.all? document.all["dhtmlpointer"] : document.getElementById? document.getElementById("dhtmlpointer") : ""
function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function ddrivetip(thetext, thewidth, thecolor){
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}
function positiontip(e){
if (enabletip){
var nondefaultpos=false
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var winwidth=ie&&!window.opera? ietruebody().clientWidth : window.innerWidth-20
var winheight=ie&&!window.opera? ietruebody().clientHeight : window.innerHeight-20
var rightedge=ie&&!window.opera? winwidth-event.clientX-offsetfromcursorX : winwidth-e.clientX-offsetfromcursorX
var bottomedge=ie&&!window.opera? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY
var leftedge=(offsetfromcursorX<0)? offsetfromcursorX*(-1) : -1000
//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<tipobj.offsetWidth){
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=curX-tipobj.offsetWidth+"px"
nondefaultpos=true
}
else if (curX<leftedge)
tipobj.style.left="5px"
else{
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetfromcursorX-offsetdivfrompointerX+"px"
pointerobj.style.left=curX+offsetfromcursorX+"px"
}
//same concept with the vertical position
if (bottomedge<tipobj.offsetHeight){
tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px"
nondefaultpos=true
}
else{
tipobj.style.top=curY+offsetfromcursorY+offsetdivfrompointerY+"px"
pointerobj.style.top=curY+offsetfromcursorY+"px"
}
tipobj.style.visibility="visible"
if (!nondefaultpos)
pointerobj.style.visibility="visible"
else
pointerobj.style.visibility="hidden"
}
}
function hideddrivetip(){
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
pointerobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}
document.onmousemove=positiontip
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/reset.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/skin.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/base.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/fonts.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/media-query.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/jqueryui-custom.css"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/css/jqtransform.css" type="text/css" media="all"/>
<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/select2/select2.css"/>
<style>
.alert,.notification_message {
	padding: 15px 40px 8px 20px;
	/* margin-bottom: 18px; */
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	background: #effaff;
	border: 1px solid #dadada;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	color: #333333;
	width: 480px;
	margin: 25px auto 0px 280px;
	font: 12px 'OpenSansRegular';
	position: absolute;
	right:610px;
	top:0px;
}
.alert h4,.notification_message h1 {
	font: bold 16px 'Conv_GothamHTF-Book 3';
	color: #000000;
}
.alert,.notification_message p {
	font: 12px 'Conv_GothamHTF-Book 3';
}

</style>


</head>
<body class="<?php echo $classes ?>">
	<div id="loading" style="display:none">
		<div class="loading-bg"></div>
		<div class="icon-wrap icon-effect">
		            <div class="icon">Settings</div>
		            		<div class="loading-img">  </div>
		        </div>

	</div>
	<div class="outer-wrapper">
		<!-- start outer wrapper -->
		<?php if((isset($_REQUEST['view']) && $_REQUEST['view']=="article") && (isset($_REQUEST['id']) &&  $_REQUEST['id']=="1")){ ?>
		<div class="row header-row"> <!-- Start Header Row -->
			<div class="header1"><a href="<?php echo JRoute::_(""); ?>" class="agent-bridge-logo"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/main-logo.png"></a></div>
		</div> <!-- End Header Row -->
		<?php } ?>
		<?php if((trim($_REQUEST['option'])=="com_hikashop" || trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_propertylisting" || trim($_REQUEST['option'])=="com_home" || trim($_REQUEST['option'])=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){ ?>
		<section class="header">
		<!-- start header -->
		<div class="wrapper">
			<a href="<?php echo JRoute::_(""); ?>" class="logo ab-logo left" alt="Agent Bridge"></a>
			<div class="top-right right">
			<?php $task = JRequest::getVar('task'); ?>
			<?php if ( 
				($task!="set_pass") &&
				($task!="payments") &&
				($task!="continue_complete") &&
				($task!="term_acceptance")
				
			) { 
				 if(isset($_SESSION['selected']) && $_SESSION['selected']=="POPs"){
				 	$sel_p="selected";
				 	$sel_a=" ";
				 } else {
				 	$sel_a="selected";
				 	$sel_p=" ";
				 }
				?>
			
			<div class="search-container left">
				<form
					action="<?php echo JRoute::_('index.php?option=com_customsearch');?>"
					method="get" 
					class="form-inline">
					<div id="searchdropcontainer">
						<select id="searchdropdown" class="left select_nostyle" name="task">
							<option disabled="" value="">Select</option>
							<option <?php echo $sel_p;?> value="searchlisting">POPs&#153;</option>
							<option <?php echo $sel_a;?> value="searchagent">Agent</option>
						</select>
					</div>
						<input name="searchword" type="text" placeholder="Search" class="search left" id="searchValue"/> 
						<input type="hidden" name="option" value="com_customsearch" />
						<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
						<input name="" type="submit" class="button left" />
				</form>
			</div>			
			<?php } ?>		
			
			<div class="right-nav left">
				<ul>
					<li>	
					<?php if ( 
					($task!="set_pass") &&
					($task!="payments") &&
					($task!="continue_complete") &&
					($task!="term_acceptance") ) { ?><a href="<?php echo JRoute::_('index.php?option=com_userprofile&task=profile'); ?>" class="username"><?php echo stripslashes($user->name); ?></a>
					<?php } ?>
					</li>
					<li style="margin-left:25px; margin-top:3px">
						<form 
							id="user_logout" 
							class="form-horizontal jqtransformdone"
							action="<?php echo JRoute::_("index.php?option=com_users&task=user.logout"); ?>"
							method="post">
							<a id='logout_link' href="javascript: void(0);" onclick="document.getElementById('user_logout').submit();">Logout</a>
							<?php echo JHtml::_('form.token');?></form>
					</li>
				</ul>
			</div>
		</div>
	</div>
		</section>
		<!-- end header -->
		
		<?php }else{  ?>	
		<div class="row header-row" style="background-color:#ffffff"> <!-- Start Header Row -->
			<div class="header1">
				<div class="left" id="hplogo" style="margin-bottom:20px"><a href="<?php echo $this->baseurl; ?>" class="agent-bridge-logo"><img src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/main-logo.png"></a>
				</div>
				<div class="hpform" >
					<form class="login-form right" action="<?php echo JRoute::_('index.php'); ?>" method="post" id="login-form" >
					<div class="hpform1"><input id="modlgn-username" size="27" type="text" name="username" class="text-field" tabindex="0"  required="required" placeholder="Enter Your Email" /></div>
					<div class="hpform2"><input id="modlgn-passwd" size="27" type="password" name="password" class="text-field"  required="required" tabindex="0"  placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" /><br/>
					<a href="index.php/component/users/?view=reset" class="text-link left" style="font-size:11px;margin-top:5px;margin-left:5px"> Forgot Your Password?</a></div>
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo (isset($_GET['returnurl'])) ? $_GET['returnurl'] : base64_encode('index.php?option=com_userprofile'); ?>" />
					<?php echo JHtml::_('form.token'); ?>
					<div class="hpform3 right">
					<input style="margin-top:2px" type="submit" class="submit-btn right" value="Log in"></div>
					</form>
				</div>
			</div>
			<div class="clear-float"></div>
			<div class="header2">
				<ul>
                <li><a href="<?php echo JRoute::_('index.php/component/nrds/?task=contactus')?>">Support</a></li>
                <li><a href="<?php echo JRoute::_('index.php/component/nrds/?task=manual')?>">Sign Up</a></li>
				</ul>

			</div>
		</div> <!-- End Header Row -->

		<?php } ?>
		
	
		<section class="main-content"><!-- start main content --> 
				<div id="content" class="<?php echo $span;?>">
			<!-- Begin Content -->
				<jdoc:include type="modules" name="position-3" style="xhtml" />
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				<jdoc:include type="modules" name="position-2" style="none" />
			<!-- End Content -->
				</div>
		</section><!-- end main content --> 
		
		
		<?php if((trim($_REQUEST['option'])=="com_hikashop") || (trim($_REQUEST['option'])=="com_userprofile") || trim($_REQUEST['option'])=="com_home" || trim($_REQUEST['option'])=="com_propertylisting" ||  trim(trim($_REQUEST['option']))=="com_customsearch" || trim($_REQUEST['option'])=="com_activitylog"){ ?>
		
		<?php $componenent = 'userprofile'; ?>
		<section class="footer"><!-- start footer -->
		<div class="wrapper">
			<p class="left"><a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=aboutus">About Us</a> | <a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=contactus">Contact Us</a> | <a href="<?php echo $this->baseurl ?>/index.php/component/userprofile/?task=help">Support</a></p>
			<p class="right">© 2014 AgentBridge. All rights reserved.</p>
			</div></section>
				<!-- start footer-link -->
		<?php } else{ ?>
			<?php $componenent = 'nrds'; ?>
					<!-- start footer-link -->
					
					 <div class="row footer-row"> <!-- Start Footer -->
						<div class="wrapper"><div class="footer-logo"><span class="footer-logo-txt">About</span></div>
						<span class="footer-tagline">The most powerful global network of real estate professionals</span>
						<span class="copyright">© 2014 AgentBridge</span></div>
					</div> <?php } ?><!-- End Footer -->
	<!-- end outer wrapper -->

</body>

<?php
// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript($this->baseurl."/templates/agentbridge/scripts/jquery.html5-placeholder-shim.js");
?>
<script	src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/agentbridge/scripts/mask.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/custom/custom.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/jquery.jqtransform.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/site.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/scripts/autoNumeric.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/agentbridge/plugins/select2/select2.js"></script>
<script type='text/javascript' src='//cdn.jsdelivr.net/qtip2/2.0.1/basic/jquery.qtip.js'></script>
</html>
