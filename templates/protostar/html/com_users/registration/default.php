<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

?>

<jdoc:include type="message" />

<?php
	if(count($_POST) > 0){
		require_once  JPATH_ROOT.'/components/com_users/models/registration.php';
		$model = new UsersModelRegistration();
		jimport('joomla.mail.helper');
		jimport( 'joomla.application.component.controller' );
		$uParams = JComponentHelper::getParams('com_users');
		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$error = false;
		$data = array();
		
		$jController = new UsersController;
		

		// IMAGE UPLOAD
		if(isset($_FILES['jform_image']['name'])){
			$image = str_replace(" ", "_", $_FILES['jform_image']['name']);
			$path = "uploads/";
			$extention = array(".jpg", ".jpeg", ".png", ".gif");
			$ext = strstr($image, ".");
			$imagePath = $path . $image;
			
			if(!in_array($ext, $extention)){
				JError::raiseWarning( 100, 'Invalid File Type for Image!' );
				$error = true;
			}
			else{
				if($_FILES['jform_image']['size'] > 200000){
					JError::raiseWarning( 100, 'File size exceeds limit.' );
					$error = true;
				}
				else{
					if(!move_uploaded_file($_FILES['jform_image']['tmp_name'], $imagePath)){
						JError::raiseWarning( 100, 'Error in uploading image.' );
						$error = true;
					}
				}
			}
			
			
		}
		
		// PREPARE POST VARIABLES
		foreach($_POST['jform'] as $key=>$value){
			if(is_array($value)){
				foreach($value as $k=>$v){
					$data[$key][$k] = trim(addslashes($v));
				}
			}
			else{
				$data[$key] = trim(addslashes($value));
			}
		}
				
		extract($data);
		
		if($error == false){
		
		$name = $fname . ' ' . $lname;
		
		// Attempt to save the data.
		jimport('joomla.user.helper');
		$data = array(  'username'=>$username,'name'=>$name,'email1'=>$email1,'password1'=>$password1, 'password2'=>$password2, 'block'=>1 );
		$return = $model->register($data);
		
		if ($return === false)
		{
			// Redirect back to the homepage.
			JError::raiseWarning( 100, $model->getError());
			$error = true;
		}
				
		$db = JFactory::getDbo();
		
		// get the last record from the user table
		$query = $db->getQuery(true);
		
		$query->select(array('id'));
		$query->from('#__users');
		//$query->where('profile_key LIKE \'custom.%\'');
		//$query->order('ordering ASC');		
		$db->setQuery($query);
		
		$results = $db->loadObjectList();
				
		//--------------------------//
			
			if($error == false){

				$query = $db->getQuery(true);
			
				// SAVE TO USER INFO			
				$insert = new JObject();
				
				$insert->image = $imagePath;
				$insert->user_id = (int)$results[0]->id;
				$insert->first_name = $fname;
				$insert->last_name = $lname;
				$insert->agent = $agent;
				$insert->street_address = $address;
				$insert->suburb = $suburb;
				$insert->city = $city;
				$insert->state = $state;
				$insert->country = $country;
				$insert->cell = serialize($cell);
				$insert->phone = serialize($phone);
				$insert->fax = serialize($fax);
				$insert->s_languages = serialize($languages);
				$insert->about = $about;
				$insert->designations = serialize($designation);
													
				//Insert new record into user_info table.
				$ret = $db->insertObject('#__user_info', $insert);
					
					// Redirect to the profile screen.
					if ($return === 'adminactivate'){
						JFactory::getApplication()->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_VERIFY'));
						$jController->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
					} elseif ($return === 'useractivate')
					{
						JFactory::getApplication()->enqueueMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE'));
						$jController->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
					}
					else
					{
						JFactory::getApplication()->enqueueMessage(JText::_('COM_USERS_REGISTRATION_SAVE_SUCCESS'));
						$jController->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
					} 
			
				
			}
		
		}
		
	}
?>

<script type="text/javascript">

	function add_field(type){
		var $af = jQuery.noConflict();
		
		var addmore, el;
		var desig = parseInt($af("#add_more_designation input").length) + 1;		
		var lang = parseInt($af("#add_more_language input").length) + 1;		
		var cell = parseInt($af("#add_more_cee input").length) + 1;		
		var phone = parseInt($af("#add_more_phone input").length) + 1;		
		var fax = parseInt($af("#add_more_fax input").length) + 1;		
		var remove = parseInt($af(".remove").length) + 1;		
				
			
						  
		switch (type){
			// CELL
			case 0:
				addmore = "cell"; 
				el = "#add_more_cell";
				increment = cell;
				break;
			// PHONE
			case 1:
				addmore = "phone"; 
				el = "#add_more_phone";
				increment = phone;
				break;
			// FAX
			case 2:
				addmore = "fax"; 
				el = "#add_more_fax";
				increment = fax;
				break;
			// LANGUAGE
			case 3:
				addmore = "languages"; 
				el = "#add_more_language";
				increment = lang;
				break;
			// DESIGNATION
			case 4: 
				addmore = "designation"; 
				el = "#add_more_designation";
				increment = desig;
				break;
		}		
		
		var field = "<div class=\"control-group\">" + 
							"<div class=\"control-label\"></div>" +
							"<div class=\"controls\">" +
								"<input type=\"text\" size=\"60\" value=\"\" class=\"\" id=\"jform_" + addmore + increment + "\" name=\"jform[" + addmore + "][]\" /> "	+	
								"<a href=\"javascript: add_field(" + type + ");\"><img src=\"<?php echo $this->baseurl ?>/templates/protostar/images/add.png\" width=\"27\" height=\"27\" border=\"0\" /></a>" +			
								"<a href=\"javascript: void(0)\" onclick=\"remover('remove_btn_" + remove + "')\" class=\"remove\" id=\"remove_btn_" + remove + "\"><img src=\"<?php echo $this->baseurl ?>/templates/protostar/images/remove.jpg\" border=\"0\" /></a>" + 	
							"</div>" +
						"</div>";
		
		$af(el).append(field);
		
	}

	function remover(e){
		var $rem = jQuery.noConflict();
		$rem(document).ready(function(){
			
			$rem("#" + e).parent().parent().remove();
			
		});
	} 
	
		
</script>

<div class="registration<?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
	<div class="page-header">
		<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
	</div>
<?php endif; ?>
	
	
	<?php #echo JRoute::_('index.php?option=com_users&task=abregistrationcontroller.register'); ?>
	
	<form id="member-registration" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=registration" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
<?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
	<?php $fields = $this->form->getFieldset($fieldset->name);?>
	<?php if (count($fields)):?>
		<fieldset>
		<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
		?>
			<legend><?php echo JText::_($fieldset->label);?></legend>
		<?php endif;?>
		
		<?php  /* foreach ($fields as $field) :// Iterate through the fields in the set and display them.?>
			<?php if ($field->hidden):// If the field is hidden, just display the input.?>
				<?php echo $field->input;?>
			<?php else:?>
				<div class="control-group">
					<div class="control-label">
					<?php echo $field->label; ?>
					<?php if (!$field->required && $field->type != 'Spacer') : ?>
						<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL');?></span>
					<?php endif; ?>
					</div>
					<div class="controls">
						<?php echo $field->input;?>
					</div>
				</div>
			<?php endif;?>
		<?php endforeach; */  ?>
		
		<div class="control-group">
			<div class="control-label">
				<span class="spacer">
					<span class="before"></span>
					<span class="text"><label class="" id="jform_spacer-lbl"><strong class="red">*</strong> Required field</label></span>
					<span class="after"></span>
				</span>										
			</div>
			<div class="controls"></div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Enter your first name" class="hasTip required" for="jform_fname" id="jform_fname-lbl">First Name:<span class="star">&nbsp;*</span></label>							
			</div>
			<div class="controls">
				<input type="text" size="30" class="required" value="<?php echo $_POST['jform']['fname'] ?>" id="jform_fname" name="jform[fname]" aria-required="true" required="required">					
			</div>
		</div>
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your last name" class="hasTip required" for="jform_lname" id="jform_lname-lbl">Last Name:<span class="star">&nbsp;*</span></label>							
			</div>
			<div class="controls">
				<input type="text" size="30" class="required" value="<?php echo $_POST['jform']['lname'] ?>" id="jform_lname" name="jform[lname]" aria-required="true" required="required">					
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Enter your desired username" class="hasTip required" for="jform_username" id="jform_username-lbl">Username:<span class="star">&nbsp;*</span></label>					
			</div>
			<div class="controls">
				<input type="text" size="30" class="validate-username required" value="<?php echo $_POST['jform']['email2'] ?>" id="jform_username" name="jform[username]" aria-required="true" required="required">
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Enter your desired password<br />Minimum of 4 characters" class="hasTip required" for="jform_password1" id="jform_password1-lbl">Password:<span class="star">&nbsp;*</span></label>				
			</div>
			<div class="controls">
				<input type="password" size="30" class="validate-password required" autocomplete="off" value="" id="jform_password1" name="jform[password1]" aria-required="true" required="required">					
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Confirm your password" class="hasTip required" for="jform_password2" id="jform_password2-lbl">Confirm Password:<span class="star">&nbsp;*</span></label>		
			</div>
			<div class="controls">
				<input type="password" size="30" class="validate-password required" autocomplete="off" value="" id="jform_password2" name="jform[password2]" aria-required="true" required="required">					
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Enter your email address" class="hasTip required" for="jform_email1" id="jform_email1-lbl">Email Address:<span class="star">&nbsp;*</span></label>		
			</div>
			<div class="controls">
				<input type="email" size="30" value="<?php echo $_POST['jform']['email1'] ?>" id="jform_email1" class="validate-email required" name="jform[email1]" aria-required="true" required="required">
			</div>
		</div>

		<div class="control-group">
			<div class="control-label">
				<label title="Confirm your email address" class="hasTip required" for="jform_email2" id="jform_email2-lbl">Confirm email Address:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="email" size="30" value="<?php echo $_POST['jform']['email2'] ?>" id="jform_email2" class="validate-email required" name="jform[email2]" aria-required="true" required="required">					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your agent's name" class="hasTip required" for="jform_agent" id="jform_agent-lbl">Agent:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="30" value="<?php echo $_POST['jform']['agent'] ?>" id="jform_agent" class="required" name="jform[agent]" aria-required="true" required="required">					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter brokerage" class="hasTip required" for="jform_brokerage" id="jform_brokerage-lbl">Brokerage:<span class="star">&nbsp;</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="30" value="<?php echo $_POST['jform']['brokerage'] ?>" id="jform_brokerage" class="required" name="jform[brokerage]" aria-required="true" required="required">					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label title="Upload a picture" class="hasTip required" for="jform_image" id="jform_image-lbl">Image:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="file" size="30" value="" class="required" id="jform_image" name="jform_image" aria-required="true" required="required" />				
			</div>
		</div> 
				
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your street address" class="hasTip required" for="jform_address" id="jform_address-lbl">Street Address:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['address'] ?>" id="jform_address" class="required" name="jform[address]" aria-required="true" required="required" style="width: 420px;">					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label for="jform_suburb" id="jform_suburb-lbl">Address Line 2:<span class="star">&nbsp;</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['suburb'] ?>" id="jform_suburb" name="jform[suburb]" />					
			</div>
		</div> 
				
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your city" class="hasTip required" for="jform_city" id="jform_city-lbl">City:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['city'] ?>" id="jform_city" class="required" name="jform[city]" aria-required="true" required="required" />					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your state" class="hasTip required" for="jform_state" id="jform_state-lbl">State:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['state'] ?>" id="jform_state" class="required" name="jform[state]" aria-required="true" required="required" />					
			</div>
		</div> 
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your country" class="hasTip required" for="jform_country" id="jform_country-lbl">Country:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['country'] ?>" id="jform_country" class="required" name="jform[country]" aria-required="true" required="required" style="width: 320px;" />					
			</div>
		</div> 
				
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your cellphone number" class="hasTip required" for="jform_cell" id="jform_cell-lbl">Cell:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['cell'][0] ?>" id="jform_cell" class="required" name="jform[cell][]" aria-required="true" required="required" />  <a href="javascript: add_field(0);"><img src="<?php echo $this->baseurl ?>/templates/protostar/images/add.png" width="27" height="27" border="0" /></a>						
			</div>
		</div> 
		
		<div id="add_more_cell"></div>
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your phone number" class="hasTip required" for="jform_phone" id="jform_phone-lbl">Phone:<span class="star">&nbsp;</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['phone'][0] ?>" id="jform_phone" name="jform[phone][]" /> <a href="javascript: add_field(1);"><img src="<?php echo $this->baseurl ?>/templates/protostar/images/add.png" width="27" height="27" border="0" /></a>								
			</div>
		</div> 
		
		<div id="add_more_phone"></div>
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your fax number" class="hasTip required" for="jform_fax" id="jform_phone-lbl">Fax:<span class="star">&nbsp;</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['fax'][0] ?>" id="jform_fax" name="jform[fax][]" /> <a href="javascript: add_field(2);"><img src="<?php echo $this->baseurl ?>/templates/protostar/images/add.png" width="27" height="27" border="0" /></a>										
			</div>
		</div> 
		
		<div id="add_more_fax"></div>
		
		<div class="control-group">
			<div class="control-label">
				<label title="Enter your spoken languages" class="hasTip required" for="jform_languages0" id="jform_languages0-lbl">Spoken Languages:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['languages'][0] ?>" id="jform_languages0" class="required" name="jform[languages][]" aria-required="true" required="required"  /> <a href="javascript: add_field(3);"><img src="<?php echo $this->baseurl ?>/templates/protostar/images/add.png" width="27" height="27" border="0" /></a>			
								
			</div>
		</div> 
		
		<div id="add_more_language"></div>
		
		<div class="control-group">
			<div class="control-label">
				<label title="Say something about yourself" class="hasTip required" for="jform_about" id="jform_about-lbl">About:<span class="star">&nbsp;*</span></label>	
			</div>
			<div class="controls">
				<textarea type="text" size="60" value="" id="jform_about" class="required" name="jform[about]" aria-required="true" required="required" style="width: 420px; height: 120px;"><?php echo $_POST['jform']['about'] ?></textarea>					
			</div>
		</div> 
				
		<div class="control-group" style="margin-bottom: 0;">
			<div class="control-label">
				<label title="Enter your designations and certification" class="hasTip required" for="jform_designation0" id="jform_designation0-lbl">Designations and Certification:<span class="star">&nbsp;</span></label>	
			</div>
			<div class="controls">
				<input type="text" size="60" value="<?php echo $_POST['jform']['designation'][0] ?>" class="required" id="jform_designation0" name="jform[designation][]" /> 
				<a href="javascript: add_field(4);"><img src="<?php echo $this->baseurl ?>/templates/protostar/images/add.png" width="27" height="27" border="0" /></a>					
			</div>
		</div> 		
		
		<div id="add_more_designation"></div>
				
		
		</fieldset>
	<?php endif;?>
<?php endforeach;?>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JREGISTER');?></button>
			<a class="btn" href="<?php echo JRoute::_('');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
			<input type="hidden" name="option" value="com_users" />
			<!--input type="hidden" name="task" value="registration.register" /-->
			<?php echo JHtml::_('form.token');?>
		</div>
	</form>
</div>
