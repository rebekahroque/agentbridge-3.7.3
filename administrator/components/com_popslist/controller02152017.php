<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class PopsListController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('PopsList');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

				'
				 b.buyer_type,
				 
				 pl.closed,

				 pl.listing_id,

				 pl.property_type, 

				 ur.user_type,

				 pl.property_name, 

				 pt.type_name,

				 ps.name AS sub_type_name,

				 pl.date_expired,

				 u.name AS agent_name,

				 u.id AS agent_id,

				 pl.zip, 

				 pl.city, 

				 z.zone_name, 

				 pp.price_type,

				 pp.price2,		

				 pp.price1,

				 pl.user_id,
				 
				 pl.currency',

			)

		);



		$query->from('#__pocket_listing pl');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = pl.sub_type');

		$query->leftJoin('#__users u ON u.id = pl.user_id');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = pl.state');

		$query->leftJoin('#__buyer_needs bne ON bne.listing_id = pl.listing_id');

		$query->leftJoin('#__buyer b ON b.buyer_id = bne.buyer_id');

	//	$query->where('closed!=1');
				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();

		//var_dump($results);


		//Agent with active pops and buyers
		$query2 = $db->getQuery(true);

		$query2->select(

			array(

					'
					 u.id
					'

			)

		);

		$query2->from('#__users u');

		$query2->leftJoin('#__pocket_listing pl ON pl.user_id = u.id');

		$query2->leftJoin('#__buyer b ON b.agent_id = u.id');
		

		$query2->where('(b.buyer_type!="Inactive" AND b.buyer_type!="") AND pl.closed!=1');

        $db->setQuery($query2);

		$results2 = $db->loadObjectList();


		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );	

		//Assignref for Agent with active pops and buyers
		$view->assignRef( 'agent_datas', $results2 );	

		$application = JFactory::getApplication();

		$task = JRequest::getVar('task');

		switch ($task):

			case "export_csv":
				generateCSV();
			break;
		
		endswitch;

        parent::display();

    }

    function export_csv()

	{
	
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;

		die();
	}

		   ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateCSV(){
	    $language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language->load($extension, $base_dir, "english-US", true);
		  
	      ## Make DB connections
	      $db    = JFactory::getDBO();
	       
			$query = $db->getQuery(true);

			$query->select(

				array(

					'pl.listing_id,

					 pl.property_name, 

					 pt.type_name AS property_type,

					 ps.name AS property_sub_type,

					 u.name AS agent,

					 ur.email,

					 umn.value AS contact_number,

					 ur.user_type,

					 pl.zip AS zip_code, 

					 pl.city, 

					 z.zone_name, 

					 pp.price1 AS price_1,

					 pp.price2 AS price_2,

					 pl.currency,
					 
					 pl.date_expired AS expiry_date'

				)

			);



			$query->from('#__pocket_listing pl');

			$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');

			$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');

			$query->leftJoin('#__property_sub_type ps ON ps.sub_id = pl.sub_type');

			$query->leftJoin('#__users u ON u.id = pl.user_id');
			
			$query->leftJoin('#__user_registration ur ON ur.email = u.email');

			$query->leftJoin('#__user_mobile_numbers umn ON umn.user_id = ur.user_id');

			$query->leftJoin('#__zones z ON z.zone_id = pl.state');

			$query->group('pl.listing_id');
	      
	      $db->setQuery($query);
	      $rows = $db->loadAssocList();

	       $time = time();
	      
	      ## If the query doesn't work..
	      if (!$db->query() ){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      
	      $fields = (array_keys($rows[0]));
	      
	      ## Count all fields(will be the collumns
	      $columns = count($fields); 
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	       if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	}else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      
	      $data .= "\n";
	      
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
			 foreach($row as $key => $value) {
				$row[$key] = str_replace(array("\\","/",","), "",  $value);
			 }
			 
			 $row['property_type'] = JText::_($row['property_type']);
			 $row['property_sub_type'] = JText::_($row['property_sub_type']);
			 if(!$row['price_2']) {
				 $row['price_2'] = $row['price_1'];
				 $row['price_1'] = "";
			 }
			 
			 $line = implode(",", $row);
			 
			 
	         ## Now replace several things for MS Excel
	         /*
			 foreach ($row as $key=>$value) {
	         	if($key=="price_type"){
	         	 //$value = $value == 1 ? "Price Range" : "Exact Price";  
				 $price_type = $value;
	         	} else if($key=="price1" || $key=="price2"){
	         	 $value = $value!=NULL ? "$".$value : "";
				} else if($key=="property_type" || $key=="property_sub_type"){
				 $value = JText::_($value);
	         	} else
	         	 $value = str_replace(array("\\","/",","), "",  $value);

	           $line .=   $value .  ",";
	         }
			 */
	         $data .= trim($line)."\n";
	      }
	      
	      $data = str_replace("\r","",$data);
	      
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      
	      ## Push the report now!
	      $this->name = 'POPs™-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }

}



?>