<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 

class BrokerageViewBrokerage extends JViewLegacy
{
    function display($tpl = null)
    {
       	   
		JToolBarHelper::title( 'Brokerage', 'generic.png' );  
		   
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();

		$query = $db->getQuery(true);
		$query->select(array('b.*', 'z.*'));
		$query->from('#__broker as b');
		$query->join('LEFT', '#__zones AS z ON (b.zone_id = z.zone_id)');
		
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		$this->assignRef( 'datas', $results );
		
		$task = JRequest::getVar('task');
		
		// ASSIGN TOOLBAR
		if($task==""){
		
			JToolBarHelper::addNew();
		//	JToolBarHelper::editList();
		//	JToolBarHelper::deleteList();
		
			$tpl = null;
		}
		else{
		//	JToolBarHelper::save();
		//	JToolBarHelper::cancel();
		}
		
		// PERFORM ACTION
		switch ($task){
		
			// ADD
			case 'add':

			$tpl = 'add';

			 //$application->redirect('index.php?option=com_brokerage&task=add');
		/*			
				if(count($_POST['jform']) > 0){
					$profile = new stdClass();
					
					$profile->broker_name=$_POST['jform']['name'];
					$profile->country_id=(int)$_POST['jform']['country'];
					 
					try {
						$result = JFactory::getDbo()->insertObject('#__broker', $profile);
							if($result) $application->redirect('index.php?option=com_brokerage');
					} catch (Exception $e) {
						JError::raiseWarning( 100, $e->getMessage() );
					}
				}
			*/	
				break;
			
			// EDIT
			case 'edit':
			
				if(count($_POST['jform']) > 0){
					$query = $db->getQuery(true); 
					$fields = array(
						'broker_name=\''.$_POST['jform']['name'].'\'',
						'country_id=\''.(int)$_POST['jform']['country'].'\'');
					 
					$conditions = array('broker_id=\''.(int)$_GET['bid'].'\'');
					 
					$query->update($db->quoteName('#__broker'))->set($fields)->where($conditions);
					 
					$db->setQuery($query);
					 
					try {
						$result = $db->query(); 
							if($result) $application->redirect('index.php?option=com_brokerage');
					} catch (Exception $e) {
						JError::raiseWarning( 100, $e->getMessage() );
					}
				}
				
				break;
			
		}
		
		//$this->state = $this->get('State');
        parent::display($tpl);
    }
}

?>