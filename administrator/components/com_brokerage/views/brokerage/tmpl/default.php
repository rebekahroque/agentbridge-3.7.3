<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');

$loggeduser = JFactory::getUser();

$model = &$this->getModel('Brokerage');

?>
<script type="text/javascript" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/scripts/jquery.tablesorter.js"/></script>
<script type="text/javascript" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/scripts/jquery.tablesorter.widgets.js"/></script>
<script>
var $ = jQuery;

$(document).ready(function() 
    { 
    	
	   $("table").tablesorter({
	   	widthFixed : true,
	   	widgets: ["zebra", "filter"],
	   });

	   $("#brokerage_table tr:nth-child(10n)").addClass("alt");

       $(".header .tablesorter-header-inner").append('<i class="icon-arrow-up-3" style="display:none"></i>');

       $("#brokerage_table").bind("sortStart",function() { 
        //do your magic here
       $("#brokerage_table tr").removeClass("alt");

       $(".header .tablesorter-header-inner").not(".headerSortUp").find("i").css("display","none");
       $(".header .tablesorter-header-inner").not(".headerSortDown").find("i").css("display","none");

	    }).bind("sortEnd",function() { 
	        //when done finishing do other magic things
	        $("#brokerage_table tr:nth-child(10n)").addClass("alt");
	     $(".header.tablesorter-headerAsc .tablesorter-header-inner i").removeClass("icon-arrow-up-3");
         $(".header.tablesorter-headerAsc .tablesorter-header-inner i").addClass("icon-arrow-down-3");
         $(".header.tablesorter-headerAsc .tablesorter-header-inner i").css("display","inline-block");

         $(".header.tablesorter-headerDesc .tablesorter-header-inner i").removeClass("icon-arrow-down-3");
         $(".header.tablesorter-headerDesc .tablesorter-header-inner i").addClass("icon-arrow-up-3");
         $(".header.tablesorter-headerDesc .tablesorter-header-inner i").css("display","inline-block");

	    });
    } 
);
</script>
<style>
#brokerage_table th.header{
	background: none;
	color: #08c;
	text-decoration: none;
}
#brokerage_table th.header:hover{
	color:#005580;
	text-decoration: underline;
	cursor: pointer;
}
input{
	width: 80%;
}
</style>

<form action="<?php echo JRoute::_('index.php?option=com_brokerage');?>" method="post" name="adminForm" id="adminForm">
	<table id="brokerage_table" class="table table-striped">
		<thead>
			<tr>
				<!--<th width="1%" class="nowrap center">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>-->
				<th class="header left" style="width:2%">
					<?php //echo JHtml::_('grid.sort', 'ID', 'a.broker_id', $listDirn, $listOrder); ?> 
					ID
				</th>
				<th class="header left">
					<?php //echo JHtml::_('grid.sort', 'Name', 'a.broker_name', $listDirn, $listOrder); ?> 
					Name
				</th>
				<th class="header left">
					<?php //echo JHtml::_('grid.sort', 'City', 'a.city', $listDirn, $listOrder); ?> 
					City
				</th>
				<th class="header left">
					<?php //echo JHtml::_('grid.sort', 'State', 'a.zone_id', $listDirn, $listOrder); ?> 
					State
				</th>
				<th class="header left">
					<?php //echo JHtml::_('grid.sort', 'Country', 'a.country_id', $listDirn, $listOrder); ?> 
					Country
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody>
			<?php 
				foreach($this->datas as $i=>$data):
			?>
			<tr  class="row<?php  echo $i % 2; ?>">
				<!--<td class="center">
					<?php // echo JHtml::_('grid.id', $i, $data->broker_id); ?>
				</td>-->
				<td>
					<?php echo $this->escape($data->broker_id); ?>
				</td>
				<td>
					<?php echo $this->escape($data->broker_name); ?>
				</td>
				<td>
					<?php echo $this->escape($data->city); ?>
				</td>
				<td>
					<?php echo $this->escape($data->zone_name); ?>
				</td>
				<td>
					<?php echo $model->get_country_name($this->escape($data->country_id)); ?>
				</td>
			</tr>
			<?php 
				endforeach;
			?>
		</tbody>
	</table>
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
</form>
