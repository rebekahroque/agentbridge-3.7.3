<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');


$model = &$this->getModel('Brokerage');
$country_with_states = $model->get_countries_based_on_states();
$countries_states=array_unique($country_with_states);
$countries = $model->get_countries_list($countries_states);


?>

<form action="<?php echo JRoute::_('index.php?option=com_brokerage&task=save');?>" method="post" name="adminForm" id="adminForm">
	
<table>
	<tr>
		<td colspan=2 ><h4>Add Broker</h4></td>
	</tr>
	<tr>
		<td>Name*</td>
		<td>:</td>
		<td><input type="text" id="b_name" name="jform[name]" value="" /> <span id="e_b_name" style="display:none;color:red">Please enter a name</span></td>
	</tr>
	<tr>
		<td>City*</td>
		<td>:</td>
		<td><input type="text" id="c_name" name="jform[city]" value="" /> <span id="e_c_name" style="display:none;color:red">Please enter city name</span></td>
	</tr>
		<tr>
		<td>State</td>
		<td>:</td>
		<td id="td_b_states">
			
		</td>
	</tr>
	<tr>
		<td>Country</td>
		<td>:</td>
		<td>
			<select id="b_country" name="jform[country]">
			<?php 
				foreach($countries as $i=>$country):
			?>
				<?php if($country->countries_id==223) {?>
					<option value="<?php echo $country->countries_id ?>" <?php echo $selected; ?> selected><?php echo $country->countries_name ?></option>
				<?php } else {?>
					<option value="<?php echo $country->countries_id ?>" <?php echo $selected; ?>><?php echo $country->countries_name ?></option>
				<?php } ?>
			<?php 
				endforeach;
			?>
			</select>
		</td>
	</tr>
</table>

<br />

	<button type="submit" class="btn btn-primary validate"><?php echo JText::_('Save and Close');?></button>
	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_brokerage');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
	<?php echo JHtml::_('form.token');?>

</form>
<script>
jQuery(document).ready(function(){

	jQuery.ajax({
			url: '<?php echo $this->baseurl; ?>/index.php?option=com_brokerage&task=getStatesCountry&format=raw',
			type: 'POST',
			data: {	countryid: jQuery("#b_country").val() },
			success: function( data ) {
				jQuery("#td_b_states").append('<select id="b_states" name="jform[state]"></select>');
				jQuery("#b_states").append(data);
				jQuery('#b_states').chosen({
						disable_search_threshold : 10,
						allow_single_deselect : true
					});
			},
		});

	jQuery("#b_country").on("change",function(){
		 jQuery.ajax({
			url: '<?php echo $this->baseurl; ?>/index.php?option=com_brokerage&task=getStatesCountry&format=raw',
			type: 'POST',
			data: {	countryid: jQuery("#b_country").val() },
			success: function( data ) {
				jQuery("#b_states").remove();
				jQuery("#b_states_chzn").remove();
				jQuery("#td_b_states").append('<select id="b_states" name="jform[state]"></select>');
				jQuery("#b_states").append(data);
				jQuery('#b_states').chosen({
						disable_search_threshold : 10,
						allow_single_deselect : true
					});
			},
		});
	});



	jQuery("button[type=submit]").on("click",function(){
		var f_error=0;
		if(!jQuery.trim(jQuery("#b_name").val()).length) { // zero-length string AFTER a trim
            jQuery("#e_b_name").show();
            f_error++;

            jQuery("#b_name").on("keyup blur",function(){
            	jQuery("#e_b_name").hide();
            });
     	} else {
     		jQuery("#e_b_name").hide();
     	}
     	if(!jQuery.trim(jQuery("#c_name").val()).length){
     		jQuery("#e_c_name").show();
     		f_error++;

     		jQuery("#c_name").on("keyup blur",function(){
            	jQuery("#e_c_name").hide();
            });
     	}

     	if(f_error>=1){
     		return false;
     	}
	});

});
</script>