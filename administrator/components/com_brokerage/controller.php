<?php
	
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class BrokerageController extends JControllerLegacy
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();	
		$application = JFactory::getApplication();
		
		$task = JRequest::getVar('task');
		
		switch ($task):

			case 'add':
				
				//$application->redirect(JRoute::_("index.php?option=com_brokerage"));
				break;

			case 'edit':
				
				if(count($_POST['cid']) > 1){					
					JError::raiseWarning( 100, 'You cannot edit two entries at the same time!' );					
				}
				else{
					$application->redirect('index.php?option=com_brokerage&views=edit&bid=' . $_POST['cid'][0]);
				}				
				break;

			case 'remove':

				$query = $db->getQuery(true);
 
				$conditions = array(
				'broker_id IN ("'.implode('", "', $_POST['cid']).'")');			
				 
				$query->delete($db->quoteName('#__broker'));
				$query->where($conditions);				 
				$db->setQuery($query);
				
				try {
				   $result = $db->query(); 
				} catch (Exception $e) {
				   // catch the error.
				}
				break;
				
		endswitch;
				
        parent::display($task);
    }


    function save(){

    	$application = JFactory::getApplication();

    	$profile = new stdClass();
					
		$profile->broker_name=$_POST['jform']['name'];
		$profile->city=$_POST['jform']['city'];
		$profile->zone_id=(int)$_POST['jform']['state'];
		$profile->country_id=(int)$_POST['jform']['country'];
		 
		try {

			if($profile->broker_name!='' && $profile->city!='')
				$result = JFactory::getDbo()->insertObject('#__broker', $profile);
			else{
				$application->enqueueMessage('Please fill in the required fields', 'Warning');	
			}

			if($result) {
				$application->enqueueMessage('You have successfully added '.$profile->broker_name);	
				$application->redirect('index.php?option=com_brokerage');
			} 
		} catch (Exception $e) {
			JError::raiseWarning( 100, $e->getMessage() );
		}

		

	//	$application->redirect(JRoute::_("index.php?option=com_brokerage"));
    }

    function getStatesCountry(){

    	$model = &$this->getModel('Brokerage');

		$country_id= $_POST['countryid'];
    	$states = $model->get_state_list($country_id);
    	$state_option='';
    	foreach($states as $key=>$state_value){
    		$state_option .= '<option value='.$state_value->zone_id.'>'.$state_value->zone_name.'</option>';
    	}

    	echo $state_option;

    }
 
}
	
?>