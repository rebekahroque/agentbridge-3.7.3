<?php
	
	defined('_JEXEC') or die;
	
	class BrokerageModelBrokerage extends JModelLegacy{
		
		public function get_country_name($id){
			
			// INITIALIZE DATABASE CONNECTION		
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('countries_id', 'countries_name'));
			$query->from('#__countries');
			$query->where("countries_id='$id'");				 
			$db->setQuery($query);
			$countries = $db->loadObjectList();
			
			return $countries[0]->countries_name;
			
		}
		
		public function get_broker_data($id){
			// INITIALIZE DATABASE CONNECTION		
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('*'));
			$query->from('#__broker');
			$query->where("broker_id='$id'");				 
			$db->setQuery($query);
			$broker = $db->loadObjectList();
			
			return $broker[0];
		}
		
		public function get_countries_list($country_states){
			// INITIALIZE DATABASE CONNECTION		
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('countries_id', 'countries_name'));

			$i=0;
			$count_arr=count($country_states);
			foreach($country_states as $key=>$value){
				if($count_arr<=1)
					$query->where('countries_id = \''.$value.'\'');
				else 
					$query->where('countries_id = \''.$value.'\'', "OR");
				$i++;
			} 
			
			$query->from('#__countries');	 
			$db->setQuery($query);
			$countries = $db->loadObjectList();
			
			return $countries;
		}

		public function get_state_id($name){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('zone_id'));
			$query->from('#__zones');
			$query->where('zone_name = \''.$name.'\'');
			$db->setQuery($query);
			$result = $db->loadObject();

			return $result->zone_id;

		}

		public function get_state($state_id){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('zone_name'));
			$query->from('#__zones');
			$query->where('zone_id = \''.$state_id.'\'');
			$db->setQuery($query);
			$result = $db->loadObject();

			return $result->zone_name;

		}

		public function get_state_list($country_id){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('zone_id','zone_name'));
			$query->from('#__zones');
			$query->where('zone_country_id = \''.$country_id.'\'');
			$db->setQuery($query);
			$result = $db->loadObjectList();

			return $result;

		}

		public function get_countries_based_on_states(){

			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(array('zone_country_id'));
			$query->from('#__zones');
			$db->setQuery($query);
			$result = $db->loadColumn();

			return $result;

		}
		
	}
	
?>	