<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class ReferralsFeeController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {

    	
		$model = $this->getModel('ReferralsFee');

		$ref_result= $model->getRefData();	

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

					'
					 r.status,
					 
					 r.referral_id,

					 b.name AS client_name,

					 u.name AS agent_name_a,

					 ur.name AS agent_name_b,

					 r.price_1,

					 r.price_2,

					 r.referral_fee,

					 b.agent_id,

					 rs.label

					 '

			)

		);



		$query->from('#__referral r');

		$query->leftJoin('#__buyer b ON b.buyer_id = r.client_id');

		$query->leftJoin('#__users u ON u.id = r.agent_a');

		$query->leftJoin('#__users ur ON ur.id = r.agent_b');

		$query->leftJoin('#__referral_status rs ON rs.status_id = r.status');

		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		//$query->where('buyer_type!="Inactive" AND buyer_type!=""');

        $db->setQuery($query);

		$results = $db->loadObjectList();



		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );
		$view->assignRef( 'ref_datas', $ref_result );		

		$application = JFactory::getApplication();

		$task = JRequest::getVar('task');

		switch ($task):

			case "export_csv":
				generateCSV();
			break;
		
		endswitch;

        parent::display();

    }

    function saveData(){

    	$this_fee=$_POST['id'];
    	$this_sa=str_replace(array("$",","), "", $_POST['thisSA']);
    	$this_ra=str_replace(array("$",","), "", $_POST['thisRA']);
    	//$this_ra=$_POST['thisRA'];

    	$db = JFactory::getDbo();	

		$ref_fee_data = new JObject();

		$ref_fee_data->fee_id = $this_fee;

		$ref_fee_data->r1_fee = $this_sa;

		$ref_fee_data->r2_fee = $this_ra;

		return $db->updateObject('#__referral_fee', $ref_fee_data, 'fee_id');

    }

    function saveParag(){

    	$refParag=nl2br($_POST['refParag']);
    	//$this_ra=$_POST['thisRA'];

    	$db = JFactory::getDbo();	

		$ref_fee_data = new JObject();

		$ref_fee_data->fee_id = 1;

		$ref_fee_data->ref_parag = $refParag;


		return $db->updateObject('#__referral_fee', $ref_fee_data, 'fee_id');

    }
}



?>