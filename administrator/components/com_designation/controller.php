<?php
	
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class DesignationController extends JControllerLegacy
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();	
		$application = JFactory::getApplication();
		
		$task = JRequest::getWord( 'task' );

		switch ($task):

			case 'add':
				$application->redirect('index.php?option=com_designation&views=add');
				break;
			case 'edit':
				
				if(count($_POST['cid']) > 1){					
					JError::raiseWarning( 100, 'You cannot edit two entries at the same time!' );					
				}
				else{
					$application->redirect('index.php?option=com_designation&views=edit&id=' . $_POST['cid'][0]);
				}
				
				break;
			case 'remove':
				$query = $db->getQuery(true);
 
				$conditions = array(
						'id IN ("'.implode('", "', $_POST['cid']).'")');			
				 
				$query->delete($db->quoteName('#__designations'));
				$query->where($conditions);				 
				$db->setQuery($query);
				 
				try {
				   $result = $db->query(); 
				} catch (Exception $e) {
				   // catch the error.
				}
				break;
				
		endswitch;
						
        parent::display();
    }
 
}
	
?>