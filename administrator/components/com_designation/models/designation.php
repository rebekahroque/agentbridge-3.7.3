<?php
	
	defined('_JEXEC') or die;
	
	class DesignationModelDesignation extends JModelLegacy{
		
		public function get_country_name($id){
			
			// INITIALIZE DATABASE CONNECTION		
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('countries_id', 'countries_name'));
			$query->from('#__countries');
			$query->where("countries_id='$id'");				 
			$db->setQuery($query);
			$countries = $db->loadObjectList();
			
			return $countries[0]->countries_name;
			
		}
				
		public function get_designations_data($id){
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('*'));
			$query->from('#__designations');
			$query->where("id='$id'");				 
			$db->setQuery($query);
			$designations = $db->loadObjectList();
			
			return $designations[0];
		}		
				
		public function get_countries_list(){
			// INITIALIZE DATABASE CONNECTION		
			$db = JFactory::getDbo();	
			$query = $db->getQuery(true);		
			$query->select(array('countries_id', 'countries_name'));
			$query->from('#__countries');	 
			$db->setQuery($query);
			$countries = $db->loadObjectList();
			
			return $countries;
		}
		
	}
	
?>	