<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 

class DesignationViewDesignation extends JViewLegacy
{
    function display($tpl = null)
    {
       	   
		JToolBarHelper::title( 'Designation and Certificates', 'generic.png' );  
		   
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();
		
			JToolBarHelper::addNew();
			JToolBarHelper::editList();
			JToolBarHelper::deleteList();
		
        parent::display($tpl);
    }
}

?>