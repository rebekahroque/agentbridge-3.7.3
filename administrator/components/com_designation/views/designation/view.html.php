<?php

// no direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.view');
 

class DesignationViewDesignation extends JViewLegacy
{
    function display($tpl = null)
    {
       	   
		JToolBarHelper::title( 'Designation and Certificates', 'generic.png' );  
		   
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$application = JFactory::getApplication();

			$query = $db->getQuery(true);	
			$query->select(array('*'));
			$query->from('#__designations');
			$db->setQuery($query);
			$results = $db->loadObjectList();	
						
			$this->assignRef( 'datas', $results );
			
		$tpl = JRequest::getVar('views');
		
		// ASSIGN TOOLBAR
		if($tpl==""){
		
			JToolBarHelper::addNew();
			JToolBarHelper::editList();
			JToolBarHelper::deleteList();
		
			$tpl = null;
		}
		else{
			JToolBarHelper::save();
			JToolBarHelper::cancel();
		}
		
		// PERFORM ACTION
		switch ($tpl){
		
			// ADD
			case 'add':
					
				if(count($_POST['jform']) > 0){
					$profile = new stdClass();
					
					$profile->designations=$_POST['jform']['name'];
					$profile->country_available=(int)$_POST['jform']['country'];
					 
					try {
						$result = JFactory::getDbo()->insertObject('#__designations', $profile);
							if($result) $application->redirect('index.php?option=com_designation');
					} catch (Exception $e) {
						JError::raiseWarning( 100, $e->getMessage() );
					}
				}
				
				break;
			
			// EDIT
			case 'edit':
			
				if(count($_POST['jform']) > 0){
					$query = $db->getQuery(true); 
					$fields = array(
						'designations=\''.$_POST['jform']['name'].'\'',
						'country_available=\''.(int)$_POST['jform']['country'].'\'');
					 
					$conditions = array('id=\''.(int)$_GET['id'].'\'');
					 
					$query->update($db->quoteName('#__designations'))->set($fields)->where($conditions);
					 
					$db->setQuery($query);
					 
					try {
						$result = $db->query(); 
							if($result) $application->redirect('index.php?option=com_designation');
					} catch (Exception $e) {
						JError::raiseWarning( 100, $e->getMessage() );
					}
				}
				
				break;
			
		}
		
        parent::display($tpl);
    }
}

?>