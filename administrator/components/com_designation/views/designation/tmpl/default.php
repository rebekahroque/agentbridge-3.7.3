<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');

$loggeduser = JFactory::getUser();

$model = &$this->getModel('Designation');

?>
<form action="<?php echo JRoute::_('index.php?option=com_designation');?>" method="post" name="adminForm" id="adminForm">

	<table class="table table-striped">
		<thead>
			<tr>
				<th width="1%" class="nowrap center">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="left">
					<?php echo JHtml::_('grid.sort', 'ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<th class="left">
					<?php echo JHtml::_('grid.sort', 'Designation & Certificates', 'a.designations', $listDirn, $listOrder); ?>
				</th>
				<th class="left">
					<?php echo JHtml::_('grid.sort', 'Country', 'a.country_available', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody>
			<?php 
				foreach($this->datas as $i=>$data):
			?>
			<tr  class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $data->id); ?>
				</td>
				<td>
					<?php echo $this->escape($data->id); ?>
				</td>
				<td>
					<?php echo $this->escape($data->designations); ?>
				</td>
				<td>
					<?php echo $model->get_country_name($this->escape($data->country_available)); ?>
				</td>
			</tr>
			<?php 
				endforeach;
			?>
		</tbody>
	</table>
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>

</form>

