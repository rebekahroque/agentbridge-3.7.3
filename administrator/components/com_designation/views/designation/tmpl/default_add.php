<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');

$model = &$this->getModel('Designation');
$countries = $model->get_countries_list();

?>

<form action="<?php echo JRoute::_('index.php?option=com_designation&views=add&added=1');?>" method="post" name="adminForm" id="adminForm">
	
<table>
	<tr>
		<td>Name</td>
		<td>:</td>
		<td><input type="text" name="jform[name]" value="" /></td>
	</tr>
	<tr>
		<td>Country</td>
		<td>:</td>
		<td>
			<select name="jform[country]">
				<option value=""></option>
			<?php 
				foreach($countries as $i=>$country):
			?>
				<option value="<?php echo $country->countries_id ?>" <?php echo $selected; ?>><?php echo $country->countries_name ?></option>
			<?php 
				endforeach;
			?>
			</select>
		</td>
	</tr>
</table>

<br />

	<button type="submit" class="btn btn-primary validate"><?php echo JText::_('Save and Close');?></button>
	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_designation');?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
	<?php echo JHtml::_('form.token');?>

</form>