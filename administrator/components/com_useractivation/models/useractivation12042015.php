<?php

defined('_JEXEC') or die;



class UserActivationModelUserActivation extends JModelLegacy 



{	



	public function __construct($config = array())

        {   

                

				$config['filter_fields'] = array(

                        'ur.user_id',

						'ur.firstname',

                        'ur.lastname',

						'LOWER(ur.email)',

						'ur.user_type',

						'ur.licence',

						'ur.brokerage_license',

						'us.verified_2012',

						'us.verified_2013'

 

                );

                parent::__construct($config);

				

        }

	

	protected function populateState($ordering = null, $direction = null) {

       	 

	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');

	    $this->setState('list.ordering', $orderCol);



		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');

		$this->setState('list.direction', $listOrder);

	   

	    parent::populateState('ur.user_id', 'ASC');



	   }

	

	

		

	public function getListQuery() {

          

	   $db = JFactory::getDbo();



	   $query = $db->getQuery(true);

	   

	   $query->select(

			$this->getState(

				'list.select',

				'ur.*'

			)

		);



		$query->from($db->quoteName('#__user_registration').' AS ur');



        $query->order($db->escape($this->getState('list.ordering', 'ur.user_id')).' '.

                

				$db->escape($this->getState('list.direction', 'ASC')));

 

        return $query;

		

	

	}

	

	

	function getOrigEmail ($uid) {

		$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('email');

		$query->from('#__user_registration');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		return $db->loadObject()->email;

	

	}

	

	

	function updateUserName ($orig_email, $data) {

		$db = JFactory::getDbo();	

		$us_object = new JObject();

		$email = $data['email'];

		$us_object->email = $orig_email;

		$us_object->username = $email;

		$db->updateObject('#__users', $us_object, 'email');

    } 

	

	function updateUserEmail ($data) {

		$db = JFactory::getDbo();	

		$urname_object = new JObject();

		$email = $data['email'];

		$urname_object->username = $email;

		$urname_object->email = $email;

		return $db->updateObject('#__users', $urname_object, 'username');

	

    } 





	function updateUserRegEmail ($data) {

		$db = JFactory::getDbo();	

		$ur_object = new JObject();

		$uid = $data['uid'];

		$email = $data['email'];

		$ur_object->user_id = $uid ;

		$ur_object->email = $email;

		return $db->updateObject('#__user_registration', $ur_object, 'user_id');

			

    } 

    function updateUserEmailInvitation($orig_email, $data) {

		$db = JFactory::getDbo();
 
		$query = $db->getQuery(true);
		 
		// Fields to update.
		$fields = array(
		   'invited_email = ' . "'" .$data['email']. "'",
		);
		 
		// Conditions for which records should be updated.
		$conditions = array(
		    'invited_email  = ' . "'" .$orig_email. "'"
		);
		 
		$query->update($db->quoteName('#__invitation_tracker'))->set($fields)->where($conditions);
		 
		$db->setQuery($query);
		 
		$result = $db->execute();

		return $result;

    } 

    function updateWorkZips($uid, $data) {

    	$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__user_zips_workaround');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		$result1 = $db->loadObjectList();

		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->zip_workaround=strtoupper($data['w_zips']);
			$a_details->zip_country=strtoupper($data['wz_country']);

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_zips_workaround', $a_details);

		} else {

			// Create an object for the record we are going to update.
			$object = new stdClass();
			 
			// Must be a valid primary key value.
			$object->user_id = $uid;

			if($data['w_zips']){
				$object->zip_workaround = strtoupper($data['w_zips']);
			}

			$object->zip_country = strtoupper($data['wz_country']);

			 
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_zips_workaround', $object, 'user_id');

		}

    } 


    function updateAssistantDetails($uid, $data) {

    	$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__user_assistant');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		$result1 = $db->loadObjectList();

		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->a_fname=$data['a_fname'];
			$a_details->a_lname=$data['a_lname'];
			$a_details->a_title=$data['a_title'];
			$a_details->a_email=$data['a_email'];
			$a_details->a_pnumber=$data['a_pnumber'];
			if($data['cc_all']){
				$a_details->cc_all = 1;
			}
			$a_details->date_created=time();

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_assistant', $a_details);

		} else {

			// Create an object for the record we are going to update.
			$object = new stdClass();
			 
			// Must be a valid primary key value.
			$object->user_id = $uid;

			if($data['a_fname']){
				$object->a_fname = $data['a_fname'];
			}
			if($data['a_lname']){
				$object->a_lname = $data['a_lname'];
			}
			if($data['a_title']){
				$object->a_title = $data['a_title'];
			}
			if($data['a_email']){
				$object->a_email = $data['a_email'];
			}
			if($data['a_pnumber']){
				$object->a_pnumber = $data['a_pnumber'];
			}

			if($data['cc_all']){
				$object->cc_all = 1;
			}
			 
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_assistant', $object, 'user_id');

		}

    } 



	function updateUserInfo ($data) {



		$db = JFactory::getDbo();

		

		$uid = $data['uid'];

	

		$update = new JObject();

		

		$updatesales = new JObject();

		

		$updatename = new JObject();

		

		$namearr = array($data['firstname'],$data['lastname']);

		

		$name = implode(" ",$namearr);

		

		$updatename->name = $name;

		

		$updatename->email = $data['email'];

		

		$db->updateObject('#__users', $updatename, 'email');

		

		//UPDATE REGISTRATION TABLE



		$update->user_id = $uid;

		

		$updatesales->agent_id = $uid;

		

		$update->firstname = $data['firstname'];

		

		$update->lastname = $data['lastname'];

		$update->user_type = $data['user_type'];
		

		if ($data['brokerlicense']) {

			$update->brokerage_license = $this->encrypt($data['brokerlicense']);

		} else {
			
			$update->brokerage_license = '';
		}
		
		if ($data['agentlicense']) {

			$update->licence = $this->encrypt($data['agentlicense']);

		} else {
			
			$update->licence = '';
		}
		
	

		$updatesales->volume_2014 = (int) str_replace(",", "", $data['volume_2014']);
		
		
		$updatesales->volume_2013 = (int) str_replace(",", "", $data['volume_2013']);

		

		$updatesales->volume_2012 = (int) str_replace(",", "", $data['volume_2012']);

		
		$updatesales->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
		
		
		$updatesales->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);

		

		$updatesales->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);

		

		$updatesales->ave_price_2012 = $updatesales->volume_2012 / $updatesales->sides_2012;

		

		$updatesales->ave_price_2013 = $updatesales->volume_2013 / $updatesales->sides_2013;
		
		
		$updatesales->ave_price_2014= $updatesales->volume_2014 / $updatesales->sides_2014;

		

		$updatesales->verified_2013 = $data['verified_2013'];

		

		$updatesales->verified_2012 = $data['verified_2012'];
		
		
		$updatesales->verified_2014 = $data['verified_2014'];

		

		$result = $db->updateObject('#__user_registration', $update, 'user_id');

		

		$result2 = $db->updateObject('#__user_sales', $updatesales, 'agent_id');

		

		

    }

	

	function encrypt($plain_text) {

	

		$key = 'password to (en/de)crypt';

		

		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));

		

		return $encrypted;

	}

	

		public function get_designations($countryID){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id as value', 'designations as label'));

		$query->from('#__designations');

		$query->where('country_available LIKE \''.$countryID.'\'');

		$db->setQuery($query);

		return  $db->loadObjectList();

	}
	
	public function get_user_designations($userid, $own = false){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id','designations'));

		$query->from('#__designations');

		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

		return $db->setQuery($query)->loadObjectList();

	}
	
	public function updateActivationDate($email){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$update = new JObject();
		
		$date = date('Y-m-d H:i:s',time());
		
		$update->email = $email;
		
		$update->sendEmail_date = $date;

		$db->updateObject('#__user_registration', $update, 'email');

	}

	public function getUsersForCSV(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('user_id','firstname','lastname','user_type','city','c.zone_name','is_premium','registration_date','sendEmail_date','activation_date', 'registerDate','completeReg_date','is_term_accepted'));

		$query->join('LEFT', $db->quoteName('#__zones', 'c') . ' ON (' . $db->quoteName('c.zone_id') . ' = ' . $db->quoteName('#__user_registration.state') . ')');
		
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.email') . ' = ' . $db->quoteName('#__user_registration.email') . ')');

		$query->from('#__user_registration');

		$db->setQuery($query);

	    return $db->loadAssocList();

		//$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

	}


}

?>