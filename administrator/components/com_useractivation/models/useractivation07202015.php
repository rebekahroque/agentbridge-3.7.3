<?php

defined('_JEXEC') or die;



class UserActivationModelUserActivation extends JModelLegacy 



{	



	public function __construct($config = array())

        {   

                

				$config['filter_fields'] = array(

                        'ur.user_id',

						'ur.firstname',

                        'ur.lastname',

						'LOWER(ur.email)',

						'ur.user_type',

						'ur.licence',

						'ur.brokerage_license',

						'us.verified_2012',

						'us.verified_2013'

 

                );

                parent::__construct($config);

				

        }

	

	protected function populateState($ordering = null, $direction = null) {

       	 

	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');

	    $this->setState('list.ordering', $orderCol);



		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');

		$this->setState('list.direction', $listOrder);

	   

	    parent::populateState('ur.user_id', 'ASC');



	   }

	

	

		

	public function getListQuery() {

          

	   $db = JFactory::getDbo();



	   $query = $db->getQuery(true);

	   

	   $query->select(

			$this->getState(

				'list.select',

				'ur.*'

			)

		);



		$query->from($db->quoteName('#__user_registration').' AS ur');



        $query->order($db->escape($this->getState('list.ordering', 'ur.user_id')).' '.

                

				$db->escape($this->getState('list.direction', 'ASC')));

 

        return $query;

		

	

	}

	

	

	function getOrigEmail ($uid) {

		$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('email');

		$query->from('#__user_registration');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		return $db->loadObject()->email;

	

	}

	

	

	function updateUserName ($orig_email, $data) {

		$db = JFactory::getDbo();	

		$us_object = new JObject();

		$email = $data['email'];

		$us_object->email = $orig_email;

		$us_object->username = $email;

		$db->updateObject('#__users', $us_object, 'email');

	

    } 

	

	function updateUserEmail ($data) {

		$db = JFactory::getDbo();	

		$urname_object = new JObject();

		$email = $data['email'];

		$urname_object->username = $email;

		$urname_object->email = $email;

		return $db->updateObject('#__users', $urname_object, 'username');

	

    } 



	function updateUserRegEmail ($data) {

		$db = JFactory::getDbo();	

		$ur_object = new JObject();

		$uid = $data['uid'];

		$email = $data['email'];

		$ur_object->user_id = $uid ;

		$ur_object->email = $email;

		return $db->updateObject('#__user_registration', $ur_object, 'user_id');

			

    } 



	function updateUserInfo ($data) {



		$db = JFactory::getDbo();

		

		$uid = $data['uid'];

	

		$update = new JObject();

		

		$updatesales = new JObject();

		

		$updatename = new JObject();

		

		$namearr = array($data['firstname'],$data['lastname']);

		

		$name = implode(" ",$namearr);

		

		$updatename->name = $name;

		

		$updatename->email = $data['email'];

		

		$db->updateObject('#__users', $updatename, 'email');

		

		//UPDATE REGISTRATION TABLE



		$update->user_id = $uid;

		

		$updatesales->agent_id = $uid;

		

		$update->firstname = $data['firstname'];

		

		$update->lastname = $data['lastname'];

		$update->user_type = $data['user_type'];

		$update->brokerage_license = $this->encrypt($data['brokerlicense']);

		

		$update->licence = $this->encrypt($data['agentlicense']);

		

		$updatesales->volume_2014 = (int) str_replace(",", "", $data['volume_2014']);
		
		
		$updatesales->volume_2013 = (int) str_replace(",", "", $data['volume_2013']);

		

		$updatesales->volume_2012 = (int) str_replace(",", "", $data['volume_2012']);

		
		$updatesales->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
		
		
		$updatesales->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);

		

		$updatesales->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);

		

		$updatesales->ave_price_2012 = $updatesales->volume_2012 / $updatesales->sides_2012;

		

		$updatesales->ave_price_2013 = $updatesales->volume_2013 / $updatesales->sides_2013;
		
		
		$updatesales->ave_price_2014= $updatesales->volume_2014 / $updatesales->sides_2014;

		

		$updatesales->verified_2013 = $data['verified_2013'];

		

		$updatesales->verified_2012 = $data['verified_2012'];
		
		
		$updatesales->verified_2014 = $data['verified_2014'];

		

		$result = $db->updateObject('#__user_registration', $update, 'user_id');

		

		$result2 = $db->updateObject('#__user_sales', $updatesales, 'agent_id');

		

		

    }

	

	function encrypt($plain_text) {

	

		$key = 'password to (en/de)crypt';

		

		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));

		

		return $encrypted;

	}

	

		public function get_designations($countryID){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id as value', 'designations as label'));

		$query->from('#__designations');

		$query->where('country_available LIKE \''.$countryID.'\'');

		$db->setQuery($query);

		return  $db->loadObjectList();

	}
	
	public function get_user_designations($userid, $own = false){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id','designations'));

		$query->from('#__designations');

		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

		return $db->setQuery($query)->loadObjectList();

	}
	
	public function updateActivationDate($email){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$update = new JObject();
		
		$date = date('Y-m-d H:i:s',time());
		
		$update->email = $email;
		
		$update->sendEmail_date = $date;

		$db->updateObject('#__user_registration', $update, 'email');

	}

	public function getUsersForCSV(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('user_id','firstname','lastname','user_type','city','c.zone_name','is_premium','registration_date','sendEmail_date','activation_date', 'registerDate','completeReg_date','is_term_accepted'));

		$query->join('LEFT', $db->quoteName('#__zones', 'c') . ' ON (' . $db->quoteName('c.zone_id') . ' = ' . $db->quoteName('#__user_registration.state') . ')');
		
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.email') . ' = ' . $db->quoteName('#__user_registration.email') . ')');

		$query->from('#__user_registration');

		$db->setQuery($query);

	    return $db->loadAssocList();

		//$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

	}


}

?>