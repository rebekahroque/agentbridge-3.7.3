<?php
defined('_JEXEC') or die;
class UserActivationModelUserActivation extends JModelLegacy 
{	
	public function __construct($config = array())
        {   
				$config['filter_fields'] = array(
                        'ur.user_id',
						'ur.firstname',
                        'ur.lastname',
						'LOWER(ur.email)',
						'ur.user_type',
						'ur.licence',
						'ur.brokerage_license',
						'us.verified_2012',
						'us.verified_2013'
                );
                parent::__construct($config);
        }
	protected function populateState($ordering = null, $direction = null) {
	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');
	    $this->setState('list.ordering', $orderCol);
		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');
		$this->setState('list.direction', $listOrder);
	    parent::populateState('ur.user_id', 'ASC');
	   }
	public function getListQuery() {
	   $db = JFactory::getDbo();
	   $query = $db->getQuery(true);
	   $query->select(
			$this->getState(
				'list.select',
				'ur.*'
			)
		);
		$query->from($db->quoteName('#__user_registration').' AS ur');
        $query->order($db->escape($this->getState('list.ordering', 'ur.user_id')).' '.
				$db->escape($this->getState('list.direction', 'ASC')));
        return $query;
	}
	function getOrigEmail ($uid) {
		$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('email');
		$query->from('#__user_registration');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		return $db->loadObject()->email;
	}
	
	function getUserAddress ($uid) {
		$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('ur.city, z.zone_code, ur.zip, ur.is_term_accepted, c.countries_iso_code_2, c.countries_iso_code_3, ucm.contact_options, ur.email as uemail');
		$query->from('#__user_registration ur');
		$query->leftJoin('#__countries c ON c.countries_id = ur.country');
		$query->leftJoin('#__users u ON u.email = ur.email');
		$query->leftJoin('#__user_contact_method ucm ON ucm.user_id = u.id');
		$query->leftJoin('#__zones z ON z.zone_id = ur.state');
		$query->where('ur.user_id = '.$uid);
		$db->setQuery($query);
		return $db->loadObject();
	}


	function updateUserName ($orig_email, $data) {
		$db = JFactory::getDbo();	
		$us_object = new JObject();
		$email = $data['email'];
		$us_object->email = $orig_email;
		$us_object->username = $email;
		$db->updateObject('#__users', $us_object, 'email');
    } 
	function updateUserEmail ($data) {
		$db = JFactory::getDbo();	
		$urname_object = new JObject();
		$email = $data['email'];
		$urname_object->username = $email;
		$urname_object->email = $email;
		return $db->updateObject('#__users', $urname_object, 'username');
    } 
	function updateUserRegEmail ($data) {
		$db = JFactory::getDbo();	
		$ur_object = new JObject();
		$uid = $data['uid'];
		$email = $data['email'];
		$ur_object->user_id = $uid ;
		$ur_object->email = $email;
		return $db->updateObject('#__user_registration', $ur_object, 'user_id');
    } 
    function updateUserEmailInvitation($orig_email, $data) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		// Fields to update.
		$fields = array(
		   'invited_email = ' . "'" .$data['email']. "'",
		);
		// Conditions for which records should be updated.
		$conditions = array(
		    'invited_email  = ' . "'" .$orig_email. "'"
		);
		$query->update($db->quoteName('#__invitation_tracker'))->set($fields)->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();
		return $result;
    } 
    function getZipWorkaround($uid){
    	$db = JFactory::getDbo();	
    	$query2 = $db->getQuery(true);
		$query2->select('*');
		$query2->from('#__user_zips_workaround uz');
		$query2->leftJoin('#__country_validations cv ON cv.country = uz.zip_country');
		$query2->where('user_id = '.$uid);
		$db->setQuery($query2);
		$result1 = $db->loadObjectList();
		return $result1;
    }
    function updateWorkZips($uid, $data) {
    	$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_zips_workaround');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		$result1 = $db->loadObjectList();
		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->zip_workaround=strtoupper($data['w_zips']);
			$a_details->zip_country=strtoupper($data['wz_country']);
			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_zips_workaround', $a_details);
		} else {
			// Create an object for the record we are going to update.
			$object = new stdClass();
			// Must be a valid primary key value.
			$object->user_id = $uid;
			if($data['w_zips']){
				$object->zip_workaround = strtoupper($data['w_zips']);
			}
			$object->zip_country = strtoupper($data['wz_country']);
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_zips_workaround', $object, 'user_id');
		}
    } 
    function updateAssistantDetails($uid, $data) {
    	$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_assistant');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		$result1 = $db->loadObjectList();
		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->a_fname=$data['a_fname'];
			$a_details->a_lname=$data['a_lname'];
			$a_details->a_title=$data['a_title'];
			$a_details->a_email=$data['a_email'];
			$a_details->a_pnumber=$data['a_pnumber'];
			if($data['cc_all']){
				$a_details->cc_all = 1;
			}
			$a_details->date_created=time();
			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_assistant', $a_details);
		} else {
			// Create an object for the record we are going to update.
			$object = new stdClass();
			// Must be a valid primary key value.
			$object->user_id = $uid;
			if($data['a_fname']){
				$object->a_fname = $data['a_fname'];
			}
			if($data['a_lname']){
				$object->a_lname = $data['a_lname'];
			}
			if($data['a_title']){
				$object->a_title = $data['a_title'];
			}
			if($data['a_email']){
				$object->a_email = $data['a_email'];
			}
			if($data['a_pnumber']){
				$object->a_pnumber = $data['a_pnumber'];
			}
			if($data['cc_all']){
				$object->cc_all = 1;
			}
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_assistant', $object, 'user_id');
		}
    } 
	function updateUserInfo ($data) {
		$db = JFactory::getDbo();
		$uid = $data['uid'];
		$update = new JObject();
		$updatesales = new JObject();
		$updatename = new JObject();
		$namearr = array($data['firstname'],$data['lastname']);
		$name = implode(" ",$namearr);
		$updatename->name = $name;
		$updatename->email = $data['email'];
		$db->updateObject('#__users', $updatename, 'email');
		//UPDATE REGISTRATION TABLE
		$update->user_id = $uid;
		$updatesales->agent_id = $uid;
		$update->firstname = $data['firstname'];
		$update->lastname = $data['lastname'];
		$update->user_type = $data['user_type'];
		$update->gender = $data['gender'];
		$update->street_address = $data['street_address']; 
		$update->suburb = $data['suburb']; 
		$update->city = $data['city']; 
		$update->zip = $data['zip']; 
		$update->state = $data['state']; 
		$update->country = $data['country']; 

		if(count($data['phone']))
			$message .= $this->update_work_number($data['phone'], $uid);
		if(count($data['cell']))
			$message .= $this->update_mobile_number($data['cell'], $uid);
		if(count($data['fax']))
			$message .= $this->update_fax_number($data['fax'], $uid);


		if ($data['brokerlicense']) {
			$update->brokerage_license = $this->encrypt($data['brokerlicense']);
		} else {
			$update->brokerage_license = '';
		}
		if ($data['agentlicense']) {
			$update->licence = $this->encrypt($data['agentlicense']);
		} else {
			$update->licence = '';
		}
		/*
		$updatesales->volume_2015 = (int) str_replace(",", "", $data['volume_2015']);
		$updatesales->volume_2014 = (int) str_replace(",", "", $data['volume_2014']);
		$updatesales->volume_2013 = (int) str_replace(",", "", $data['volume_2013']);
		$updatesales->volume_2012 = (int) str_replace(",", "", $data['volume_2012']);
		*/
		$updatesales->volume_2016 = preg_replace('/[^\d.]/', '', $data['volume_2016']);
		$updatesales->volume_2015 = preg_replace('/[^\d.]/', '', $data['volume_2015']);
		$updatesales->volume_2014 = preg_replace('/[^\d.]/', '', $data['volume_2014']);
		$updatesales->volume_2013 = preg_replace('/[^\d.]/', '', $data['volume_2013']);
		$updatesales->volume_2012 = preg_replace('/[^\d.]/', '', $data['volume_2012']);
		
		$updatesales->sides_2016 = (int) str_replace(",", "", $data['sides_2016']);
		$updatesales->sides_2015 = (int) str_replace(",", "", $data['sides_2015']);
		$updatesales->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
		$updatesales->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);
		$updatesales->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);
		$updatesales->ave_price_2012 = $updatesales->volume_2012 / $updatesales->sides_2012;
		$updatesales->ave_price_2013 = $updatesales->volume_2013 / $updatesales->sides_2013;
		$updatesales->ave_price_2014= $updatesales->volume_2014 / $updatesales->sides_2014;
		$updatesales->ave_price_2015= $updatesales->volume_2015 / $updatesales->sides_2015;
		$updatesales->ave_price_2015= $updatesales->volume_2016 / $updatesales->sides_2016;
		$updatesales->verified_2013 = $data['verified_2013'];
		$updatesales->verified_2012 = $data['verified_2012'];
		$updatesales->verified_2014 = $data['verified_2014'];
		$updatesales->verified_2015 = $data['verified_2015'];
		$updatesales->verified_2016 = $data['verified_2016'];
		$result = $db->updateObject('#__user_registration', $update, 'user_id');
		$result2 = $db->updateObject('#__user_sales', $updatesales, 'agent_id');
    }

    public function get_mobile_numbers($email, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_mobile_numbers');
		if(!$own)
			$query->where('`show` = 1');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE user_id = \''.$email.'\')');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function get_fax_numbers($email, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_fax_numbers');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE user_id = \''.$email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function get_work_numbers($email, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_work_numbers');
		$query->where('user_id = (SELECT user_id FROM #__user_registration WHERE user_id = \''.$email.'\')');
		if(!$own)
			$query->where('`show` = 1');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function update_work_number($dataList, $regid, $add = false){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT clv.countryCode FROM #__user_registration ur LEFT JOIN #__country_validations clv ON clv.country = ur.country WHERE ur.user_id = ".$regid);
		$db->setQuery($query);		
		$countryCode = $db->loadObject()->countryCode;


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_work_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_work_numbers')->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_work_numbers', $number_object);
			}
		}
		//return $this->set_message($orig_info, $dataList, 'work numbers', $add, $countryCode);
	}
	public function update_mobile_number($dataList, $regid, $add = false){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT clv.countryCode FROM #__user_registration ur LEFT JOIN #__country_validations clv ON clv.country = ur.country WHERE ur.user_id = ".$regid);
		$db->setQuery($query);		
		$countryCode = $db->loadObject()->countryCode;


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_mobile_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_mobile_numbers');
			$query->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_mobile_numbers', $number_object);
			}
		}
		//return $this->set_message($orig_info, $dataList, 'mobile numbers', $add, $countryCode);
	}
	public function update_fax_number($dataList, $regid, $add = false){


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT clv.countryCode FROM #__user_registration ur LEFT JOIN #__country_validations clv ON clv.country = ur.country WHERE ur.user_id = ".$regid);
		$db->setQuery($query);		
		$countryCode = $db->loadObject()->countryCode;


		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('value')->from('#__user_fax_numbers')->where('user_id = '.$regid);
		$db->setQuery($query);
		$orig_info = array_map(function($obj){ return $obj->value; }, $db->loadObjectList());
		if(!$add){
			$query = $db->getQuery(true);
			$query->delete('#__user_fax_numbers');
			$query->where('user_id = '.$regid);
			$db->setQuery($query);
			$db->execute();
		}
		foreach($dataList as $data){
			$number_object = new JObject();
			$number_object->value = $data;
			$number_object->main = 0;
			$number_object->show = 1;
			$number_object->user_id = $regid;
			if($number_object->value){
				$db->insertObject('#__user_fax_numbers', $number_object);
			}
		}
		//return $this->set_message($orig_info, $dataList, 'fax numbers', $add, $countryCode);
	}

	function encrypt($plain_text) {
		$key = 'password to (en/de)crypt';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
	}
		public function get_designations($countryID){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id as value', 'designations as label'));
		$query->from('#__designations');
		$query->where('country_available LIKE \''.$countryID.'\'');
		$db->setQuery($query);
		return  $db->loadObjectList();
	}
	public function get_user_designations($userid, $own = false){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id','designations'));
		$query->from('#__designations');
		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');
		return $db->setQuery($query)->loadObjectList();
	}




	public function get_user_brokerage($userid, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("
			b.broker_name,  
			b.broker_id ");
		$query->from('#__users u');
		$query->where('u.id = '.$userid);
		$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
		return $db->setQuery($query)->loadObjectList();
	}

	public function updateActivationDate($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$update = new JObject();
		$date = date('Y-m-d H:i:s',time());
		$update->email = $email;
		$update->sendEmail_date = $date;
		$db->updateObject('#__user_registration', $update, 'email');
	}
	public function getUsersForCSV(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('user_id','firstname','lastname','user_type','city','c.zone_name','is_premium','registration_date','sendEmail_date','activation_date', 'registerDate','completeReg_date','is_term_accepted'));
		$query->join('LEFT', $db->quoteName('#__zones', 'c') . ' ON (' . $db->quoteName('c.zone_id') . ' = ' . $db->quoteName('#__user_registration.state') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.email') . ' = ' . $db->quoteName('#__user_registration.email') . ')');
		$query->from('#__user_registration');
		$db->setQuery($query);
	    return $db->loadAssocList();
		//$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');
	}
	public function updateProfileImage($data){		$db = JFactory::getDbo();				$user_object = new JObject();				$uid = $data['uid'];		$email = $data['email'];		$user_object->user_id = $uid;		$user_object->email = $email;				$user_object->image = $data['image'] ;				return $db->updateObject('#__user_registration', $user_object, 'email');	}
	public function getAllPropertyTypes() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_type');
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getAllSubPropTypes($ptype_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type');
		$query->where('property_id ='.$ptype_id);
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getCountry($email) {
		$db = JFactory::getDbo();
		//GET COUNTRY
		$query = $db->getQuery(true);
		$query->select('ur.country, countries_iso_code_2, curr.currency, curr.symbol');
		$query->from('#__user_registration ur');
		$query->leftJoin('#__countries ON countries_id = ur.country');
		$query->leftJoin('#__country_currency curr ON curr.country = ur.country');
		$query->where('email = \''.$email.'\'');
		$db->setQuery($query);
		$country = $db->loadObjectList();
		return $country;
	}
	public function getPropSubTypeByName($subtype, $poptype) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type st');
		$query->leftJoin('#__property_type pt ON pt.type_id = st.property_id');
		$query->where('pt.type_name LIKE \''.$poptype.'\' AND st.name LIKE \''.$subtype.'\'');
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function updatePropertyExpiry($lid, $newval){
		$edit2 = new JObject();
		$edit2->listing_id = $lid;
		$edit2->date_expired = $newval;
		$edit2->closed= 0;
		$result2 = JFactory::getDbo()->updateObject('#__pocket_listing', $edit2, 'listing_id');
	}
	public function getSelectedListing($listing_id, $fallback = false) {
		if(!$fallback)
			return $this->get_listing(array('p.listing_id'=>$listing_id));
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'pl.*', 'pp.*', 'ps.*', 'con.*', 'v.*','conc.country as concode','conc.*', 'pa.*', 'pst.*', 'pt.*','cvs.*'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		$query->join('LEFT', '(select pocket_id, group_concat(address separator "-----") as address from #__pocket_address group by pocket_id) as pa ON (pl.listing_id = pa.pocket_id)');
		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = pl.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = pl.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = pl.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = pl.sub_type');
		$query->leftJoin('#__permission_setting ps ON ps.listing_id = pl.listing_id');
		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = pl.listing_id');
		$query->where('pl.listing_id = \''.$listing_id.'\'');
		$query->order('pl.listing_id DESC LIMIT 1');
		$db->setQuery($query);
		$individual = $db->loadObjectList();
		return $individual;
	}
	public function get_property_image($lID){
		$db		= $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__pocket_images');
		$query->where('listing_id LIKE \''. $lID .'\' 
			ORDER BY CASE 
		    WHEN order_image = 0 THEN image_id
		    ELSE order_image
			END ASC'
		);
		//$query->order('order_image ASC');
		$db->setQuery($query);
		$images = $db->loadObjectList();
		return $images;
	}
	public function get_listing($filters){
		$conditions = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('p.*,ps.*,pp.*,pa.*,pi.*,v.*,re.*,viewers.*,pt.*,pst.*, conc.symbol, conc.currency, cvs.*,
			p.listing_id as listing_id,
			con.countries_iso_code_2 AS conIso,
			con.countries_id AS conCode,
			p.property_name as property_name
		');
		$query->from('#__pocket_listing p');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." > ".$value;
				else{
					$explode = explode('-', $value);
					if(count($explode)>1){
						$conditions[] = $key."1 >= ".$explode[0]." AND ".$key."2 =< ".$explode[1];
					}
					else
						$conditions[] = $key."1 > ".$value;
				}
			}
		}
		if(count($conditions)>0)
			$query->where($conditions);
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image ORDER BY image_id ASC) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed
							FROM #__request_access racs
							WHERE permission = 1
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');
		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = p.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = p.sub_type');
		$query->order('p.setting desc');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function printPropertyState($state_id) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where('zone_id = \''.$state_id.'\'');
		$db->setQuery($query);
		$state = $db->loadObject();
		return $state;
	}
	
	function getMCcities(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('cities');
		$query->from('#__country_monaco_cities');
		$query->where('country_id = 141');
		$db->setQuery($query);
		$mccities = $db->loadColumn();
		return $mccities;
	}
	
	function getCCities($country_id, $postcode){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('cities');
		$query->from('#__country_postcode_cities');
		$query->where('country = '.$country_id.' AND postcode = '.$postcode);
		$db->setQuery($query);
		$mccities = $db->loadColumn();
		return $mccities;
	}


	function updatebrokerage_info($user_id, $country_id, $val){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$registration_object = new JObject();
		$registration_object->brokerage = $val;
		$registration_object->user_id = $user_id;
		$res = $db->updateObject('#__user_registration', $registration_object, 'user_id');
		$query->select('countries_name')->from('#__countries')->where("countries_id = ".$country_id );
		$db->setQuery($query);
		return $res;
	}
	
	//Form Fields

	function getFieldsBedrooms($reqfield = "", $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_BEDS").'</label>
				<select
					id="' . $editPrefix . 'jform_bedroom"
					name="bedroom" class="'.$reqfield.'">
					<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				for($i=1; $i<=5; $i++){
					//	if($i==$data->bedroom) $selected = "selected";
					//	else $selected = "";
						$fieldBody.= "<option value=\"$i\"".$selected.">".$i."</option>";
					}

		$fieldBody.='<option value="6+">6+</option>
			</select>
			<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsBathrooms($reqfield = "", $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_BATHS").'</label>
				<select
					id="' . $editPrefix . 'jform_bathroom"
					name="bathroom" class="'.$reqfield.'">
					<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				for($i=1; $i<=5; $i++){
					//	if($i==$data->bathroom) $selected = "selected";
					//	else $selected = "";
						$fieldBody.= "<option value=\"$i\"".$selected.">".$i."</option>";
					}

		$fieldBody.='<option value="6+">6+</option>
			</select>
			<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsBldgSqft($getMeasurement,$defsqft,$bldgsuffix="",$reqfield="", $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft.'</label>
				<select id="' . $editPrefix . 'jform_bldgsqft"
			name="bldgsqft" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['bldg'.$bldgsuffix] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsView($reqfield="", $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		
		$fieldBody='
		 <div class="left ys push-top2">
			<label>'.JText::_("COM_POPS_TERMS_VIEW").'</label> <select id="' . $editPrefix . 'jform_view" name="view" class="'.$reqfield.'">
				<option value="">--- '.JText::_("COM_POPS_TERMS_DEFAULT_SELECT").' ---</option>
				<option value="None">'.JText::_("COM_POPS_TERMS_NONE").'</option>
				<option value="Panoramic">'.JText::_("COM_POPS_TERMS_PANORAMIC").'</option>
				<option value="City">'.JText::_("COM_POPS_TERMS_CITY").'</option>
				<option value="Mountains/Hills">'.JText::_("COM_POPS_TERMS_MOUNT").'</option>
				<option value="Coastline">'.JText::_("COM_POPS_TERMS_COAST").'</option>
				<option value="Water">'.JText::_("COM_POPS_TERMS_WATER").'</option>
				<option value="Ocean">'.JText::_("COM_POPS_TERMS_OCEAN").'</option>
				<option value="Lake/River">'.JText::_("COM_POPS_TERMS_LAKE").'</option>
				<option value="Landmark">'.JText::_("COM_POPS_TERMS_LANDMARK").'</option>
				<option value="Desert">'.JText::_("COM_POPS_TERMS_DESERT").'</option>
				<option value="Bay">'.JText::_("COM_POPS_TERMS_BAY").'</option>
				<option value="Vineyard">'.JText::_("COM_POPS_TERMS_VINE").'</option>
				<option value="Golf">'.JText::_("COM_POPS_TERMS_GOLF").'</option>
				<option value="Other">'.JText::_("COM_POPS_TERMS_OTHER").'</option>
			</select>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">'.JText::_("COM_NRDS_FORMERROR").'</p>
		 </div>';

		return $fieldBody;

	}

	function getFieldsLotSqFt($getMeasurement,$defsqft_l,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft_l.'</label>
				<select id="' . $editPrefix . 'jform_lotsqft"
			name="lotsqft" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['lot'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_lotsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsCondition($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		 <div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CONDITION').'</label> <select id="' . $editPrefix . 'jform_condition"
				name="condition" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>
				<option value="Fixer">'.JText::_('COM_POPS_TERMS_FIX').'</option>
				<option value="Good">'.JText::_('COM_POPS_TERMS_GOOD').'</option>
				<option value="Excellent">'.JText::_('COM_POPS_TERMS_EXCELLENT').'</option>
				<option value="Remodeled">'.JText::_('COM_POPS_TERMS_REMODEL').'</option>
				<option value="New Construction">'.JText::_('COM_POPS_TERMS_NEWCONSTRUCT').'</option>
				<option value="Under Construction">'.JText::_('COM_POPS_TERMS_UNDERCONSTRUCT').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsStyle($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		 <div class="left ys push-top2" style="margin-top: 24x;">
			<label>'.JText::_('COM_POPS_TERMS_STYLE').'</label> 
			<select id="' . $editPrefix . 'jform_style" style="width:200px" name="style" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="American Farmhouse">'.JText::_('COM_POPS_TERMS_AMERICANFARM').'</option>
				<option value="Art Deco">'.JText::_('COM_POPS_TERMS_ARTDECO').'</option>
				<option value="Art Modern/Mid Century">'.JText::_('COM_POPS_TERMS_MIDCENT').'</option>
				<option value="Cape Cod">'.JText::_('COM_POPS_TERMS_CAPECOD').'</option>
				<option value="Colonial Revival">'.JText::_('COM_POPS_TERMS_COLONIAL').'</option>
				<option value="Contemporary">'.JText::_('COM_POPS_TERMS_CONTEMPORARY').'</option>
				<option value="Craftsman">'.JText::_('COM_POPS_TERMS_CRAFTSMAN').'</option>
				<option value="French">'.JText::_('COM_POPS_TERMS_FRENCH').'</option>
				<option value="Italian/Tuscan">'.JText::_('COM_POPS_TERMS_ITALIAN').'</option>
				<option value="Prairie Style">'.JText::_('COM_POPS_TERMS_PRAIRIE').'</option>
				<option value="Pueblo Revival">'.JText::_('COM_POPS_TERMS_PUEBLO').'</option>
				<option value="Ranch">'.JText::_('COM_POPS_TERMS_RANCH').'</option>
				<option value="Spanish/Mediterranean">'.JText::_('COM_POPS_TERMS_SPANISH').'</option>
				<option value="Swiss Cottage">'.JText::_('COM_POPS_TERMS_SWISS').'</option>
				<option value="Tudor">'.JText::_('COM_POPS_TERMS_TUDOR').'</option>
				<option value="Victorian">'.JText::_('COM_POPS_TERMS_VICTORIAN').'</option>
				<option value="Historic">'.JText::_('COM_POPS_TERMS_HISTORIC').'</option>
				<option value="Architecturally Significant">'.JText::_('COM_POPS_TERMS_ARCHITECTURE').'</option>
				<option value="Green">'.JText::_('COM_POPS_TERMS_GREEN').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsGarage($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2" style="margin-top: 24px;">
		<label>'.JText::_('COM_POPS_TERMS_GARAGE').'</label> <select id="' . $editPrefix . 'jform_garage" name="garage" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=1;$i<=7;$i++){
					$fieldBody.="<option value=\"$i\">$i</option>";
				}

		$fieldBody.='
			<option value="8+">8+</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsYearBuilt($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_YEAR_BUILT').'</label> 
			<select id="' . $editPrefix . 'jform_yearbuilt"
				name="yearbuilt" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="2016">2016</option>
				<option value="2015">2015</option>
				<option value="2014">2014</option>
				<option value="2013">2013</option>
				<option value="2012">2012</option>
				<option value="2011">2011</option>
				<option value="2010">2010</option>
				<option value="2009-2000">2009-2000</option>
				<option value="1999-1990">1999-1990</option>
				<option value="1989-1980">1989-1980</option>
				<option value="1979-1970">1979-1970</option>
				<option value="1969-1960">1969-1960</option>
				<option value="1959-1950">1959-1950</option>
				<option value="1949-1940">1949-1940</option>
				<option value="1939-1930">1939-1930</option>
				<option value="1929-1920">1929-1920</option>
				<option value="< 1919">< 1919</option>
				<option value="COM_POPS_TERMS_UNDISC">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
		</div>';

		return $fieldBody;

	}


	function getFieldsPoolSpa($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_POOL_SPA').'</label> <select id="' . $editPrefix . 'jform_poolspa" class="'.$reqfield.'"
			name="poolspa">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="None">'.JText::_('COM_POPS_TERMS_NONE').'</option>
			<option value="Pool">'.JText::_('COM_POPS_TERMS_POOL').'</option>
			<option value="Pool/Spa">'.JText::_('COM_POPS_TERMS_POOL_SPA').'</option>
			<option value="Spa">'.JText::_('COM_POPS_TERMS_OTHER').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select id="' . $editPrefix . 'jform_'.$id.'"
			name="'.$id.'" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="One Story">'.JText::_('COM_POPS_TERMS_ONESTORY').'</option>
			<option value="Two Story">'.JText::_('COM_POPS_TERMS_TWOSTORY').'</option>
			<option value="Three Story">'.JText::_('COM_POPS_TERMS_THREESTORY').'</option>
			<option value="Water Access">'.JText::_('COM_POPS_TERMS_WATERACCESS').'</option>
			<option value="Horse Property">'.JText::_('COM_POPS_TERMS_HORSE').'</option>
			<option value="Golf Course">'.JText::_('COM_POPS_TERMS_GOLFCOURSE').'</option>
			<option value="Walkstreet">'.JText::_('COM_POPS_TERMS_WALKSTREET').'</option>
			<option value="Media Room">'.JText::_('COM_POPS_TERMS_MEDIA').'</option>
			<option value="Guest House">'.JText::_('COM_POPS_TERMS_GUESTHOUSE').'</option>
			<option value="Wine Cellar">'.JText::_('COM_POPS_TERMS_WINECELLAR').'</option>
			<option value="Tennis Court">'.JText::_('COM_POPS_TERMS_TENNISCOURT').'</option>
			<option value="Den/Library">'.JText::_('COM_POPS_TERMS_DENLIB').'</option>
			<option value="Green Const.">'.JText::_('COM_POPS_TERMS_GREENCONSTRUCT').'</option>
			<option value="Basement">'.JText::_('COM_POPS_TERMS_BASEMENT').'</option>
			<option value="RV/Boat Parking">'.JText::_('COM_POPS_TERMS_RV').'</option>
			<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
		</select>
		</div>';

		return $fieldBody;

	}


	function getFieldsCondoFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
			name="'.$id.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="Gym" >'. JText::_('COM_POPS_TERMS_GYM').'</option>
			<option value="Security">'.JText::_('COM_POPS_TERMS_SECURITY').'</option>
			<option value="Tennis Court">'.JText::_('COM_POPS_TERMS_TENNISCOURT').'</option>
			<option value="Doorman" >'.JText::_('COM_POPS_TERMS_DOORMAN').'</option>
			<option value="Penthouse" >'.JText::_('COM_POPS_TERMS_PENTHOUSE').'</option>
			<option value="One Story" >'.JText::_('COM_POPS_TERMS_ONESTORY').'</option>
			<option value="Two Story" >'.JText::_('COM_POPS_TERMS_TWOSTORY').'</option>
			<option value="Three Story">'.JText::_('COM_POPS_TERMS_THREESTORY').'</option>
			<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsLandFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
			name="'.$id.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="Sidewalks">'.JText::_('COM_POPS_TERMS_SIDEWALKS').'</option>
			<option value="Utilities">'.JText::_('COM_POPS_TERMS_UTILITIES').'</option>
			<option value="Curbs">'.JText::_('COM_POPS_TERMS_CURBS').'</option>
			<option value="Horse Trails">'.JText::_('COM_POPS_TERMS_HORSETRAILS').'</option>
			<option value="Rural">'.JText::_('COM_POPS_TERMS_RURAL').'</option>
			<option value="Urban">'.JText::_('COM_POPS_TERMS_URBAN').'</option>
			<option value="Suburban">'.JText::_('COM_POPS_TERMS_SUBURBAN').'</option>
			<option value="Permits">'.JText::_('COM_POPS_TERMS_PERMITS').'</option>
			<option value="HOA">'.JText::_('COM_POPS_TERMS_HOA').'</option>
			<option value="Sewer">'.JText::_('COM_POPS_TERMS_SEWER').'</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">'.JText::_('COM_POPS_TERMS_COASTAL').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsMultiFamFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
				name="'.$id.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Rent Control">'.JText::_('COM_POPS_TERMS_RENTCONTROL').'</option>
				<option value="Senior">'.JText::_('COM_POPS_TERMS_SENIOR').'</option>
				<option value="Assoc-Pool">Assoc-'.JText::_('COM_POPS_TERMS_POOL').'</option>
				<option value="Assoc-Spa">'.JText::_('COM_POPS_TERMS_ASSOCSPA').'</option>
				<option value="Assoc-Tennis">'.JText::_('COM_POPS_TERMS_ASSOCTENNIS').'</option>
				<option value="Assoc-Other">'.JText::_('COM_POPS_TERMS_ASSOCOTHER').'</option>
				<option value="Section 8">'.JText::_('COM_POPS_TERMS_SECTION8').'</option>
				<option value="25% Occupied">'.JText::_('COM_POPS_TERMS_25OCCUPIED').'</option>
				<option value="50% Occupied">'.JText::_('COM_POPS_TERMS_50OCCUPIED').'</option>
				<option value="75% Occupied">'.JText::_('COM_POPS_TERMS_75OCCUPIED').'</option>
				<option value="100% Occupied">'.JText::_('COM_POPS_TERMS_100OCCUPIED').'</option>
				<option value="Cash Cow">'.JText::_('COM_POPS_TERMS_CASHCOW').'</option>
				<option value="Value Add">'.JText::_('COM_POPS_TERMS_VALUEADD').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsOfficeFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
				name="'.$id.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Mixed use">'.JText::_('COM_POPS_TERMS_MIXEDUSE').'</option>
				<option value="Single Tenant">'.JText::_('COM_POPS_TERMS_SINGLETENANT').'</option>
				<option value="Multiple Tenant">'.JText::_('COM_POPS_TERMS_MULTIPLETENANT').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
				<option value="Net-Leased">'.JText::_('COM_POPS_TERMS_NETLEASED').'</option>
				<option value="Owner User">'.JText::_('COM_POPS_TERMS_OWNERUSER').'</option>
				<option value="Vacant">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsIndusFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
				name="'.$id.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Mixed use">'.JText::_('COM_POPS_TERMS_MIXEDUSE').'</option>
				<option value="Single Tenant">'.JText::_('COM_POPS_TERMS_SINGLETENANT').'</option>
				<option value="Multiple Tenant">'.JText::_('COM_POPS_TERMS_MULTIPLETENANT').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
				<option value="Net-Leased">'.JText::_('COM_POPS_TERMS_NETLEASED').'</option>
				<option value="Owner User">'.JText::_('COM_POPS_TERMS_OWNERUSER').'</option>
				<option value="Vacant">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsHotelFeatures($id,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_FEATURES').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_'.$id.'"
				name="'.$id.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT') .' ---</option>
				<option value="Restaurant">'.JText::_('COM_POPS_TERMS_RESTAURANT').'</option>
				<option value="Bar">'.JText::_('COM_POPS_TERMS_BAR').'</option>
				<option value="Pool">'.JText::_('COM_POPS_TERMS_POOL').'</option>
				<option value="Banquet Room">'.JText::_('COM_POPS_TERMS_BANQUETROOM').'</option>
				<option value="Seller Carry">'.JText::_('COM_POPS_TERMS_SELLERCARRY').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsUnitSqft($getMeasurement,$defsqft_u,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		
		$fieldBody='
		<div class="left ys push-top2">
			<label class="changebycountry">'.$defsqft_u.'</label>
				<select id="' . $editPrefix . 'jform_unitsqft"
			name="unitsqft" class="'.$reqfield.'">
								<option value="">---'.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'---</option>';
	
				foreach ($getMeasurement['bldg'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_unitsqft error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsBldgType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_BLDG_TYPE').'</label> <select class="'.$reqfield.'" id="' . $editPrefix . 'jform_bldgtype"
			name="bldgtype">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
			<option value="North Facing">'.JText::_('COM_POPS_TERMS_NORTH').'</option>
			<option value="South Facing">'.JText::_('COM_POPS_TERMS_SOUTH').'</option>
			<option value="East Facing">'.JText::_('COM_POPS_TERMS_EAST').'</option>
			<option value="West Facing">'.JText::_('COM_POPS_TERMS_WEST').'</option>
			<option value="Low Rise">'.JText::_('COM_POPS_TERMS_LOWRISE').'</option>
			<option value="Mid Rise">'.JText::_('COM_POPS_TERMS_MIDRISE').'</option>
			<option value="High Rise">'.JText::_('COM_POPS_TERMS_HIGHRISE').'</option>
			<option value="Co-Op">'.JText::_('COM_POPS_TERMS_COOP').'</option>
		</select>
		</div>';

		return $fieldBody;

	}

	function getFieldLotSize($getMeasurement,$defsqft_ls,$reqfield=''){
		$editPrefix = ($isEdit) ? "edit_" : "";

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.$defsqft_ls.'</label> <select id="' . $editPrefix . 'jform_lotsize" 
			name="lotsize" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				foreach ($getMeasurement['lot'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_lotsize error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;
	}


	function getFieldsZoned($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_ZONED').'</label> <select
			class="'.$reqfield.'"
			id="' . $editPrefix . 'jform_zoned"
			name="zoned">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1">1 '.JText::_('COM_POPS_TERMS_UNITONE').'</option>
				<option value="2">2 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="3-4">3-4 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="5-20">5-20 '.JText::_('COM_POPS_TERMS_UNIT').'</option>
				<option value="20">20+ '.JText::_('COM_POPS_TERMS_UNIT').'</option>
			</select>
			<p class="jform_zoned error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsTerm($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TERM').'</label> <select id="' . $editPrefix . 'jform_term" name="term" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Short Term">'.JText::_('COM_POPS_TERMS_SHORTTERM').'</option>
				<option value="M to M">'.JText::_('COM_POPS_TERMS_M2M').'</option>
				<option value="Year Lease">'.JText::_('COM_POPS_TERMS_YEARLEASE').'</option>
				<option value="Multi Year Lease">'.JText::_('COM_POPS_TERMS_MULTIYEARLEASE').'</option>
				<option value="Lease Option">'.JText::_('COM_POPS_TERMS_LEASEOPTION').'</option>
			</select>
			<div>
				<p class="jform_term error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
			</div>
		</div>';

		return $fieldBody;

	}

	function getFieldsTermPossesion($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_POSSESSION').'</label> <select id="' . $editPrefix . 'jform_possession"
				name="possession" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Immediately">'.JText::_('COM_POPS_TERMS_IMMEDIATELY').'</option>
				<option value="Within 30 days">'.JText::_('COM_POPS_TERMS_30DAYS').'</option>
				<option value="Within 60 days">'.JText::_('COM_POPS_TERMS_60DAYS').'</option>
				<option value="Within 90 days">'.JText::_('COM_POPS_TERMS_90DAYS').'</option>
				<option value="Within 180 days">'.JText::_('COM_POPS_TERMS_180DAYS').'</option>
			</select>
			<p class="jform_possession error_msg" style="display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsPet($isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys" style="margin-top: 30px;width:96px">
			<label>'.JText::_('COM_POPS_TERMS_PET').'</label> 
			<a href="javascript: void(0)" style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1">'.JText::_('COM_POPS_TERMS_PETYES').'</a> 
			<a href="javascript: void(0)" class="left gradient-gray no pet" rel="0">'.JText::_('COM_POPS_TERMS_PETNO').'</a>
			<input type="hidden" value="1" id="' . $editPrefix . 'jform_pet" name="pet" class="text-input reqfield" />
		</div>';

		return $fieldBody;

	}

	function getFieldsFurnished($isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys" style="margin-top: 30px;width:96px">
			<label>'.JText::_('COM_POPS_TERMS_FURNISHED').'</label> 
			<a href="javascript: void(0)" style="margin-right:1px" class="left gradient-gray no furnished" rel="1" value="1">'.JText::_('COM_POPS_TERMS_PETYES').'</a> 
			<a href="javascript: void(0)" class="left gradient-blue-toggle yes furnished" rel="0" value="0">'.JText::_('COM_POPS_TERMS_PETNO').'</a>
			<input type="hidden" value="0" id="' . $editPrefix . 'jform_furnished" name="furnished" class="text-input reqfield" />
		</div>';

		return $fieldBody;

	}

	function getFieldsTermUnits($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_UNIT').'</label> <select id="' . $editPrefix . 'jform_units" name="units" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="2">'.JText::_('COM_POPS_TERMS_DUPLEX').'</option>
				<option value="3">'.JText::_('COM_POPS_TERMS_TRIPLEX').'</option>
				<option value="4">'.JText::_('COM_POPS_TERMS_QUAD').'</option>
				<option value="5-9">5-9</option>
				<option value="10-15">10-15</option>
				<option value="16-29">16-29</option>
				<option value="30-50">30-50</option>
				<option value="50-100">50-100</option>
				<option value="101-150">101-150</option>
				<option value="151-250">151-250</option>
				<option value="251">251+</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_units error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsTermCAP($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CAP').'</label>
				<select id="' . $editPrefix . 'jform_cap" name="cap" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="0-.9">0-.9</option>
				<option value="1-1.9">1-1.9</option>
				<option value="2-2.9">2-2.9</option>
				<option value="3-3.9">3-3.9</option>
				<option value="4-4.9">4-4.9</option>
				<option value="5-5.9">5-5.9</option>
				<option value="6-6.9">6-6.9</option>
				<option value="7-7.9">7-7.9</option>
				<option value="8-8.9">8-8.9</option>
				<option value="9-9.9">9-9.9</option>
				<option value="10-10.9">10-10.9</option>
				<option value="11-11.9">11-11.9</option>
				<option value="12-12.9">12-12.9</option>
				<option value="13-13.9">13-13.9</option>
				<option value="14-14.9">14-14.9</option>
				<option value="15">15+</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_cap error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsTermGRM($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_GRM').'</label> <select id="' . $editPrefix . 'jform_grm" name="grm" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="0">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
				<option value="1-2">1-2</option>
				<option value="3-4">3-4</option>
				<option value="4-5">4-5</option>
				<option value="5-6">5-6</option>
				<option value="6-7">6-7</option>
				<option value="7-8">7-8</option>
				<option value="8-9">8-9</option>
				<option value="9-10">9-10</option>
				<option value="10-11">10-11</option>
				<option value="11-12">11-12</option>
				<option value="12-13">12-13</option>
				<option value="13-14">13-14</option>
				<option value="14-15">14-15</option>
				<option value="15-16">15-16</option>
				<option value="16-17">16-17</option>
				<option value="17-18">17-18</option>
				<option value="18-19">18-19</option>
				<option value="19-20">19-20</option>
				<option value="20">20+</option>
			</select>	
			<p class="jform_grm error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldOccupancy($reqfield='', $isEdit = false){

		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_OCCUPANCY').'</label> <select id="' . $editPrefix . 'jform_occupancy" class="'.$reqfield.'"
			name="occupancy">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=100; $i>=0; $i=$i-5){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldOfficeType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label>
				<select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Office">'.JText::_('COM_POPS_TERMS_OFFICE').'</option>
				<option value="Institutional">'.JText::_('COM_POPS_TERMS_INSTITUTIONAL').'</option>
				<option value="Medical">'.JText::_('COM_POPS_TERMS_MEDICAL').'</option>
				<option value="Warehouse">'.JText::_('COM_POPS_TERMS_WAREHOUSE').'</option>
				<option value="Condo">'.JText::_('COM_POPS_TERMS_CONDO').'</option>
				<option value="R&D">R&D</option>
				<option value="Business Park">'.JText::_('COM_POPS_TERMS_BUSINESSPARK').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldOfficeClass($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_CLASS').'</label>
				<select id="' . $editPrefix . 'jform_class" name="class" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="4">A</option>
				<option value="3">B</option>
				<option value="2">C</option>
				<option value="1">D</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_class error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsParkingRatio($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_PARKING_RATIO').'</label> <select id="' . $editPrefix . 'jform_parking"
				name="parking" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1/1000">1/1000</option>
				<option value="1.5/1000">1.5/1000</option>
				<option value="2/1000">2/1000</option>
				<option value="2.5/1000">2.5/1000</option>
				<option value="3/1000">3/1000</option>
				<option value="3.5/1000">3.5/1000</option>
				<option value="4/1000">4/1000</option>
				<option value="4.5/1000">4.5/1000</option>
				<option value="5/1000">5/1000</option>
				<option value="other">'.JText::_('COM_POPS_TERMS_OTHER').'</option>
			</select>
		</div>';

		return $fieldBody;

	}

	function getFieldsIndusType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Flex Space">Flex Space</option>
				<option value="Business Park">'.JText::_('COM_POPS_TERMS_BUSINESSPARK').'</option>
				<option value="Condo">'.JText::_('COM_POPS_TERMS_CONDO').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
				<option value="Manufacturing">'.JText::_('COM_POPS_TERMS_MANUFACTURING').'</option>
				<option value="Office Showroom">'.JText::_('COM_POPS_TERMS_OFFICESHOWROOM').'</option>
				<option value="R&D">R&D</option>
				<option value="Self/Mini Storage">'.JText::_('COM_POPS_TERMS_SELFSTORAGE').'</option>
				<option value="Truck Terminal/Hub">'.JText::_('COM_POPS_TERMS_TRUCK').'</option>
				<option value="Warehouse">'.JText::_('COM_POPS_TERMS_WAREHOUSE').'</option>
				<option value="Distribution">'.JText::_('COM_POPS_TERMS_DISTRIBUTION').'</option>
				<option value="Cold Storage">'.JText::_('COM_POPS_TERMS_COLDSTORAGE').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldCeiling($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_CEILING_HEIGHT').'</label> <select id="' . $editPrefix . 'jform_ceiling"
			name="ceiling" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=12; $i<=34; $i=$i+2){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			<option value="36+">36+</option>
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldStories($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";

		$fieldBody='
		<div class="left ys push-top2">
		<label>'.JText::_('COM_POPS_TERMS_STORIES').'</label> <select id="' . $editPrefix . 'jform_stories"
			name="stories" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				for($i=1; $i<=5; $i++){
					$fieldBody.="<option value=\"$i\">$i</option>";	
				}

		$fieldBody.='
			<option value="6+">6+</option>
			<option value="COM_POPS_TERMS_VACANT">'.JText::_('COM_POPS_TERMS_VACANT').'</option>
			</select>
		</div>';

		return $fieldBody;
	}

	function getFieldsRetailType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Community Center">'.JText::_('COM_POPS_TERMS_COMMUNITYCENTER').'</option>
				<option value="Strip Center">'.JText::_('COM_POPS_TERMS_STRIPCENTER').'</option>
				<option value="Outlet Center">'.JText::_('COM_POPS_TERMS_OUTLETCENTER').'</option>
				<option value="Power Center">'.JText::_('COM_POPS_TERMS_POWERCENTER').'</option>
				<option value="Anchor">'.JText::_('COM_POPS_TERMS_ANCHOR').'</option>
				<option value="Restaurant">'.JText::_('COM_POPS_TERMS_RESTAURANT').'</option>
				<option value="Service Station">'.JText::_('COM_POPS_TERMS_SERVICESTATION').'</option>
				<option value="Retail Pad">'.JText::_('COM_POPS_TERMS_RETAILPAD').'</option>
				<option value="Free Standing">'.JText::_('COM_POPS_TERMS_FREESTANDING').'</option>
				<option value="Day Care/Nursery">'.JText::_('COM_POPS_TERMS_DAYCARE').'</option>
				<option value="Post Office">'.JText::_('COM_POPS_TERMS_POSTOFFICE').'</option>
				<option value="Vehicle">'.JText::_('COM_POPS_TERMS_VEHICLE').'</option>
			</select>
			<p class="jform_type error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsHotelType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Economy">'.JText::_('COM_POPS_TERMS_ECONOMY').'</option>
				<option value="Full Service">'.JText::_('COM_POPS_TERMS_FULLSERVICE').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsRoomCount($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_ROOM_COUNT').'</label> <select id="' . $editPrefix . 'jform_roomcount" name="roomcount" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="1-9">1-9</option>
				<option value="10-19">10-19</option>
				<option value="20-29">20-29</option>
				<option value="30-39">30-39</option>
				<option value="40-49">40-49</option>
				<option value="50-99">50-99</option>
				<option value="100-149">100-149</option>
				<option value="150-199">150-199</option>
				<option value="200">200+</option>
			</select>
			<p class="jform_roomcount error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}


	function getFieldsAssistType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2').'</label> <select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').'--- </option>
				<option value="Assisted">'.JText::_('COM_POPS_TERMS_ASSISTED').'</option>
				<option value="Acute Care">'.JText::_('COM_POPS_TERMS_ACUTECARE').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p style="display:none" class="jform_type error_msg"> '. JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}

	function getFieldsSpecialType($reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE2') .'</label> <select id="' . $editPrefix . 'jform_type" name="type" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="Golf">'. JText::_('COM_POPS_TERMS_GOLF').'</option>
				<option value="Marina">'.JText::_('COM_POPS_TERMS_MARINA').'</option>
				<option value="Theater">'.JText::_('COM_POPS_TERMS_THEATER').'</option>
				<option value="Religious">'.JText::_('COM_POPS_TERMS_RELIGIOUS').'</option>
				<option value="Land">'.JText::_('COM_POPS_TERMS_LAND').'</option>
			</select>
			<p class="jform_type error_msg" style="display:none;">'.JText::_('COM_NRDS_FORMERROR').' </p>
		</div>';

		return $fieldBody;

	}

	function getFieldsAvailSqFt($getMeasurement,$defsqft_a,$reqfield='', $isEdit = false){
		$editPrefix = ($isEdit) ? "edit_" : "";

		$fieldBody='
		<div class="left ys push-top2">
		<label class="changebycountry">'.$defsqft_a.'</label> <select id="' . $editPrefix . 'jform_available" name="available" class="'.$reqfield.'">
			<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>';
	
				foreach ($getMeasurement['bldg'] as $key => $value) {
					$fieldBody.= "<option value=".$value[1].">".$value[0]."</option>";
				}

		$fieldBody.='
			</select>
			<p class="jform_available error_msg" style="margin-left:0px;display:none">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;
	}


	function getFieldsOfficeTypeLease($reqfield='', $isEdit = false){ 
		$editPrefix = ($isEdit) ? "edit_" : "";
		$fieldBody='
		<div class="left ys push-top2">
			<label>'.JText::_('COM_POPS_TERMS_TYPE_LEASE').'</label> <select id="' . $editPrefix . 'jform_typelease" name="typelease" class="'.$reqfield.'">
				<option value="">--- '.JText::_('COM_POPS_TERMS_DEFAULT_SELECT').' ---</option>
				<option value="NNN">'.JText::_('COM_POPS_TERMS_NNN').'</option>
				<option value="FSG">'.JText::_('COM_POPS_TERMS_FSG').'</option>
				<option value="MG">'.JText::_('COM_POPS_TERMS_MG').'</option>
				<option value="Modified Net">'.JText::_('COM_POPS_TERMS_MODIFIEDNET').'</option>
				<option value="Not Disclosed">'.JText::_('COM_POPS_TERMS_UNDISC').'</option>
			</select>
			<p class="jform_typelease error_msg" style="display:none;">'.JText::_('COM_NRDS_FORMERROR').'</p>
		</div>';

		return $fieldBody;

	}
	
	function getSqMeasureByCountry($country){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('sqftMeasurement')->from('#__country_validations')->where(
			"country = ".$db->quote($country)
		);
		$db->setQuery($query);
		$sqmeasure = $db->loadResult();

		if($sqmeasure=="sq. ft."){
			$getColumn = "feet,feet";
		} else {
			$getColumn = "meter_round,feet";
		}

		$returnMeasure = array();

		$returnMeasure['sqmeasure'] = $sqmeasure;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_bldg');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['bldg'] = $db->loadRowList();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_lot');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['lot'] = $db->loadRowList();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($getColumn)->from('#__measurements_bldg_wide');
		$query->order('id ASC');
		$db->setQuery($query);
		$returnMeasure['bldg_wide'] = $db->loadRowList();

		return $returnMeasure;

	}
	
	
	
	
	public function checkUserChimp($user_details) {
		$apikey = 'df5c601248a76b681350af7249052b3d-us11';
		$email=$user_details['email'];
		$country = $user_details['country'];		
		$state = $user_details['state'];
		
		$type = "GET";
		//$target = "lists/5fd2cb4303/members/".md5($email);
		if($country == "USA") {
			if($state == "CA") {
				switch($city){
					case "San Diego":
						$target = "lists/5fd2cb4303/members";
						break;
					case "Orange":
						$target = "lists/5fd2cb4303/members";
						break;
					case "Ventura":
					case "Santa Barbara":
						$target = "lists/5fd2cb4303/members";
						break;
					case "San Francisco":
						$target = "lists/5fd2cb4303/members";
						break;
					default:
						$target = "lists/5fd2cb4303/members";
						break;
				}
			} else {
				$target = "lists/57a992eca9/members";
			}
		} else {
			$target = "lists/5fd2cb4303/members";
		}
		
		$target .= "/".md5($email);
		
		$api=array("login"=>"dwant@agentbridge.com","key"=>'df5c601248a76b681350af7249052b3d-us11',"url"=>'https://us11.api.mailchimp.com/3.0/');

		$result = $this->mc_request($api,$type,$target,$data);
		
		return $result;
	}


	public function addUserToChimp($user_details, $process="add"){

		$apikey = 'df5c601248a76b681350af7249052b3d-us11';
		$email=$user_details['email'];

		if($process=="add"){
			
			$fname=$user_details['fname'];
	        $lname=$user_details['lname'];
	        $broker=$user_details['broker'];
	        $state=$user_details['state'];
	        $country=$user_details['country'];
	        $zip=$user_details['zip'];
	        $city=$user_details['city'];


			
			$type = "POST";
			//$target = "lists/3f34696141/members";
			$target = "lists/5fd2cb4303/members";
		    
			/*
			if($country == "USA") {
				if($state == "CA") {
					switch($city){
						case "San Diego":
							$target = "lists/5fd2cb4303/members";
							break;
						case "Orange":
							$target = "lists/5fd2cb4303/members";
							break;
						case "Ventura":
						case "Santa Barbara":
							$target = "lists/5fd2cb4303/members";
							break;
						case "San Francisco":
							$target = "lists/5fd2cb4303/members";
							break;
						default:
							$target = "lists/5fd2cb4303/members";
							break;
					}
				} else {
					$target = "lists/5fd2cb4303/members";
				}
			} else {
				$target = "lists/5fd2cb4303/members";
			}
			*/		
			
			
			$data = array(
                'apikey'        => $apikey,
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => array(
                    'FNAME' => $fname,
                    'LNAME' => $lname,
                    //'BROKER' => $broker,
                    'STATE' => $state,
                    'COUNTRY' => $country,
                    //'ZIP' => $zip,
                    //'CITY' => $city,
					'COUNTY' => $city,
                )
            );

		} else if($process=="subs_change"){

			$fname=$user_details['fname'];
			$lname=$user_details['lname'];
			$broker=$user_details['broker'];
			$state=$user_details['state'];
			$country=$user_details['country'];
			$zip=$user_details['zip'];
			$city=$user_details['city'];
			$email=$user_details['email'];		
			$subs_val = $user_details['subs_val'];
				
			$type = "PATCH";
			//$target = "lists/3f34696141/members/".md5($email);
			$target = "lists/5fd2cb4303/members/".md5($email);
				
			
				
			//$target .= "/".md5($email);
			
			$data = array(
				'apikey'        => $apikey,
				'email_address' => $email,
				'status'        => $subs_val,	              
			);
		} else if($process=="update"){

			$email=$user_details['email']; 
			$fname=$user_details['fname'];
	        $lname=$user_details['lname'];
	        //$broker=$user_details['broker'];
	        $state=$user_details['state'];
	        $country=$user_details['country'];
	        //$zip=$user_details['zip'];
	        $city=$user_details['city'];

			$type = "PATCH";
			//$target = "lists/3f34696141/members/".md5($email);
			$target = "lists/5fd2cb4303/members/".md5($email);
			
			/*
			if($country == "USA") {
				if($state == "CA") {
					switch($city){
						case "San Diego":
							$target = "lists/5fd2cb4303/members";
							break;
						case "Orange":
							$target = "lists/5fd2cb4303/members";
							break;
						case "Ventura":
						case "Santa Barbara":
							$target = "lists/5fd2cb4303/members";
							break;
						case "San Francisco":
							$target = "lists/5fd2cb4303/members";
							break;
						default:
							$target = "lists/5fd2cb4303/members";
							break;
					}
				} else {
					$target = "lists/57a992eca9/members";
				}
			} else {
				$target = "lists/9823fbcc82/members";
			}
				
			$target .= "/".md5($email);
			*/
				
			$data = array(
				'apikey'        => $apikey,
				'email_address' => $email,
				'status'        => 'subscribed',
				'merge_fields'  => array(
					'FNAME' => $fname,
					'LNAME' => $lname,
					//'BROKER' => $broker,
					'STATE' => $state,
					'COUNTRY' => $country,
					//'ZIP' => $zip,
					'COUNTY' => $city,
				)              
			);
		}

        $api=array("login"=>"dwant@agentbridge.com","key"=>'df5c601248a76b681350af7249052b3d-us11',"url"=>'https://us11.api.mailchimp.com/3.0/');

		$result = $this->mc_request($api,$type,$target,$data);

		$mailSender =& JFactory::getMailer();
		$mailSender ->addRecipient( "francis.alincastre@keydiscoveryinc.com" );
		$mailSender ->setSubject( $subject );
		$mailSender ->isHTML(  true );
		$mailSender ->setBody(  $result.json_encode($user_details) );
		$mailSender ->Send();

		return $result;
		var_dump($result); exit;
	}
	
	function mc_request( $api, $type, $target, $data = false )
	{
		$ch = curl_init( $api['url'] . $target );

		curl_setopt( $ch, CURLOPT_HTTPHEADER, array
		(
			'Content-Type: application/json', 
			'Authorization: ' . $api['login'] . ' ' . $api['key'],
	//		'X-HTTP-Method-Override: ' . $type,
		) );

	//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );

		if( $data )
			curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );

		$response = curl_exec( $ch );
		curl_close( $ch );

		return $response;
	}
	
	
	public function getCounty($zip, $countries_iso_code2) {
		#echo $countries_iso_code2; exit;
		$zip = str_replace(" ", "%20", $zip);
		if($countries_iso_code2 == "IE") {
			$url = "http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/" . $zip . "?format=json";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);
			
			$county_details = json_decode($json);
			#echo $url;
			#print_r($json); exit;
			#print_r($county_details); exit;
			if($county_details[0]) {
				if(isset($county_details[0]->posttown)) {
					return $county_details[0]->posttown;
				} else {
					return $county_details[0]->dependentlocality;
				}
			} else {
				return "";
			}

		} else {
			$url = "https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode=" . $zip . "&country=" . $countries_iso_code2 . "&username=damianwant33"; 
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			// Get the data:
			$json = curl_exec($ch);
			curl_close($ch);
			
			$result = json_decode($json);
			$county_details = $result->postalcodes;

			if($county_details[0]) {
				if($countries_iso_code2 == "FR") {
					$france_citycode = explode(" ", $county_details[0]->placeName);					
					if(isset($france_citycode[1])) {
						$pariscity = $france_citycode[0];
						if($france_citycode[1] == "20") {
							$newparisnum = $france_citycode[1];
						} else {
							$newparisnum = str_replace("0", "", $france_citycode[1]);
						}
						
						return $pariscity . " " . $newparisnum . "&#7497;";
					}
				} else {
					if(isset($county_details[0]->adminName2)) {
						return $county_details[0]->adminName2;
					} else {
						return $county_details[0]->placeName;
					}
				}
			} else {
				return "";
			}
		}
	}
	
	public function getUser($email) {
		$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__users');
		$query->where("email = '" . $email . "'");
		$db->setQuery($query);
		
		return $db->loadObjectList();
	}


	public function update_single_designation( $designation_id, $user_id ) {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__user_designations')->where("user_id = ".$user_id." AND desig_id=".$designation_id );
		$db->setQuery($query);
		if( $db->loadObject()->count==0 ){
			$desig_object = new JObject();
			$desig_object->desig_id = $designation_id;
			$desig_object->user_id = $user_id;
			$db->insertObject('#__user_designations', $desig_object);
		}
		
		
	}
	
	public function delete_single_designation( $designation_id,$user_id ) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('count(1) as count')->from('#__user_designations')->where("user_id = ".$user_id." AND desig_id=".$designation_id );
		$db->setQuery($query);
		$db->execute();
		if( $db->loadObject()->count==1 ){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->delete('#__user_designations');
			$query->where("desig_id = ".$designation_id." AND user_id = ".$user_id);
			$db->setQuery($query);
			$db->execute();
		}
	}
	
}
?>