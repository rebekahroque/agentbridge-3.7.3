<?php
defined('_JEXEC') or die;
class UserActivationModelUserActivation extends JModelLegacy 
{	
	public function __construct($config = array())
        {   
				$config['filter_fields'] = array(
                        'ur.user_id',
						'ur.firstname',
                        'ur.lastname',
						'LOWER(ur.email)',
						'ur.user_type',
						'ur.licence',
						'ur.brokerage_license',
						'us.verified_2012',
						'us.verified_2013'
                );
                parent::__construct($config);
        }
	protected function populateState($ordering = null, $direction = null) {
	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');
	    $this->setState('list.ordering', $orderCol);
		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');
		$this->setState('list.direction', $listOrder);
	    parent::populateState('ur.user_id', 'ASC');
	   }
	public function getListQuery() {
	   $db = JFactory::getDbo();
	   $query = $db->getQuery(true);
	   $query->select(
			$this->getState(
				'list.select',
				'ur.*'
			)
		);
		$query->from($db->quoteName('#__user_registration').' AS ur');
        $query->order($db->escape($this->getState('list.ordering', 'ur.user_id')).' '.
				$db->escape($this->getState('list.direction', 'ASC')));
        return $query;
	}
	function getOrigEmail ($uid) {
		$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('email');
		$query->from('#__user_registration');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		return $db->loadObject()->email;
	}
	function updateUserName ($orig_email, $data) {
		$db = JFactory::getDbo();	
		$us_object = new JObject();
		$email = $data['email'];
		$us_object->email = $orig_email;
		$us_object->username = $email;
		$db->updateObject('#__users', $us_object, 'email');
    } 
	function updateUserEmail ($data) {
		$db = JFactory::getDbo();	
		$urname_object = new JObject();
		$email = $data['email'];
		$urname_object->username = $email;
		$urname_object->email = $email;
		return $db->updateObject('#__users', $urname_object, 'username');
    } 
	function updateUserRegEmail ($data) {
		$db = JFactory::getDbo();	
		$ur_object = new JObject();
		$uid = $data['uid'];
		$email = $data['email'];
		$ur_object->user_id = $uid ;
		$ur_object->email = $email;
		return $db->updateObject('#__user_registration', $ur_object, 'user_id');
    } 
    function updateUserEmailInvitation($orig_email, $data) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		// Fields to update.
		$fields = array(
		   'invited_email = ' . "'" .$data['email']. "'",
		);
		// Conditions for which records should be updated.
		$conditions = array(
		    'invited_email  = ' . "'" .$orig_email. "'"
		);
		$query->update($db->quoteName('#__invitation_tracker'))->set($fields)->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();
		return $result;
    } 
    function getZipWorkaround($uid){
    	$db = JFactory::getDbo();	
    	$query2 = $db->getQuery(true);
		$query2->select('*');
		$query2->from('#__user_zips_workaround uz');
		$query2->leftJoin('#__country_validations cv ON cv.country = uz.zip_country');
		$query2->where('user_id = '.$uid);
		$db->setQuery($query2);
		$result1 = $db->loadObjectList();
		return $result1;
    }
    function updateWorkZips($uid, $data) {
    	$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_zips_workaround');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		$result1 = $db->loadObjectList();
		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->zip_workaround=strtoupper($data['w_zips']);
			$a_details->zip_country=strtoupper($data['wz_country']);
			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_zips_workaround', $a_details);
		} else {
			// Create an object for the record we are going to update.
			$object = new stdClass();
			// Must be a valid primary key value.
			$object->user_id = $uid;
			if($data['w_zips']){
				$object->zip_workaround = strtoupper($data['w_zips']);
			}
			$object->zip_country = strtoupper($data['wz_country']);
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_zips_workaround', $object, 'user_id');
		}
    } 
    function updateAssistantDetails($uid, $data) {
    	$db = JFactory::getDbo();		
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__user_assistant');
		$query->where('user_id = '.$uid);
		$db->setQuery($query);
		$result1 = $db->loadObjectList();
		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->a_fname=$data['a_fname'];
			$a_details->a_lname=$data['a_lname'];
			$a_details->a_title=$data['a_title'];
			$a_details->a_email=$data['a_email'];
			$a_details->a_pnumber=$data['a_pnumber'];
			if($data['cc_all']){
				$a_details->cc_all = 1;
			}
			$a_details->date_created=time();
			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_assistant', $a_details);
		} else {
			// Create an object for the record we are going to update.
			$object = new stdClass();
			// Must be a valid primary key value.
			$object->user_id = $uid;
			if($data['a_fname']){
				$object->a_fname = $data['a_fname'];
			}
			if($data['a_lname']){
				$object->a_lname = $data['a_lname'];
			}
			if($data['a_title']){
				$object->a_title = $data['a_title'];
			}
			if($data['a_email']){
				$object->a_email = $data['a_email'];
			}
			if($data['a_pnumber']){
				$object->a_pnumber = $data['a_pnumber'];
			}
			if($data['cc_all']){
				$object->cc_all = 1;
			}
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_assistant', $object, 'user_id');
		}
    } 
	function updateUserInfo ($data) {
		$db = JFactory::getDbo();
		$uid = $data['uid'];
		$update = new JObject();
		$updatesales = new JObject();
		$updatename = new JObject();
		$namearr = array($data['firstname'],$data['lastname']);
		$name = implode(" ",$namearr);
		$updatename->name = $name;
		$updatename->email = $data['email'];
		$db->updateObject('#__users', $updatename, 'email');
		//UPDATE REGISTRATION TABLE
		$update->user_id = $uid;
		$updatesales->agent_id = $uid;
		$update->firstname = $data['firstname'];
		$update->lastname = $data['lastname'];
		$update->user_type = $data['user_type'];
		if ($data['brokerlicense']) {
			$update->brokerage_license = $this->encrypt($data['brokerlicense']);
		} else {
			$update->brokerage_license = '';
		}
		if ($data['agentlicense']) {
			$update->licence = $this->encrypt($data['agentlicense']);
		} else {
			$update->licence = '';
		}
		$updatesales->volume_2015 = (int) str_replace(",", "", $data['volume_2015']);
		$updatesales->volume_2014 = (int) str_replace(",", "", $data['volume_2014']);
		$updatesales->volume_2013 = (int) str_replace(",", "", $data['volume_2013']);
		$updatesales->volume_2012 = (int) str_replace(",", "", $data['volume_2012']);
		$updatesales->sides_2015 = (int) str_replace(",", "", $data['sides_2015']);
		$updatesales->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
		$updatesales->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);
		$updatesales->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);
		$updatesales->ave_price_2012 = $updatesales->volume_2012 / $updatesales->sides_2012;
		$updatesales->ave_price_2013 = $updatesales->volume_2013 / $updatesales->sides_2013;
		$updatesales->ave_price_2014= $updatesales->volume_2014 / $updatesales->sides_2014;
		$updatesales->ave_price_2015= $updatesales->volume_2015 / $updatesales->sides_2015;
		$updatesales->verified_2013 = $data['verified_2013'];
		$updatesales->verified_2012 = $data['verified_2012'];
		$updatesales->verified_2014 = $data['verified_2014'];
		$updatesales->verified_2015 = $data['verified_2015'];
		$result = $db->updateObject('#__user_registration', $update, 'user_id');
		$result2 = $db->updateObject('#__user_sales', $updatesales, 'agent_id');
    }
	function encrypt($plain_text) {
		$key = 'password to (en/de)crypt';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
	}
		public function get_designations($countryID){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id as value', 'designations as label'));
		$query->from('#__designations');
		$query->where('country_available LIKE \''.$countryID.'\'');
		$db->setQuery($query);
		return  $db->loadObjectList();
	}
	public function get_user_designations($userid, $own = false){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('id','designations'));
		$query->from('#__designations');
		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');
		return $db->setQuery($query)->loadObjectList();
	}	
	public function updateActivationDate($email){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$update = new JObject();
		$date = date('Y-m-d H:i:s',time());
		$update->email = $email;
		$update->sendEmail_date = $date;
		$db->updateObject('#__user_registration', $update, 'email');
	}
	public function getUsersForCSV(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('user_id','firstname','lastname','user_type','city','c.zone_name','is_premium','registration_date','sendEmail_date','activation_date', 'registerDate','completeReg_date','is_term_accepted'));
		$query->join('LEFT', $db->quoteName('#__zones', 'c') . ' ON (' . $db->quoteName('c.zone_id') . ' = ' . $db->quoteName('#__user_registration.state') . ')');
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.email') . ' = ' . $db->quoteName('#__user_registration.email') . ')');
		$query->from('#__user_registration');
		$db->setQuery($query);
	    return $db->loadAssocList();
		//$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');
	}
	public function updateProfileImage($data){		$db = JFactory::getDbo();				$user_object = new JObject();				$uid = $data['uid'];		$email = $data['email'];		$user_object->user_id = $uid;		$user_object->email = $email;				$user_object->image = $data['image'] ;				return $db->updateObject('#__user_registration', $user_object, 'email');	}
	public function getAllPropertyTypes() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_type');
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getAllSubPropTypes($ptype_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type');
		$query->where('property_id ='.$ptype_id);
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function getCountry($email) {
		$db = JFactory::getDbo();
		//GET COUNTRY
		$query = $db->getQuery(true);
		$query->select('ur.country, countries_iso_code_2, curr.currency, curr.symbol');
		$query->from('#__user_registration ur');
		$query->leftJoin('#__countries ON countries_id = ur.country');
		$query->leftJoin('#__country_currency curr ON curr.country = ur.country');
		$query->where('email = \''.$email.'\'');
		$db->setQuery($query);
		$country = $db->loadObjectList();
		return $country;
	}
	public function getPropSubTypeByName($subtype, $poptype) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__property_sub_type st');
		$query->leftJoin('#__property_type pt ON pt.type_id = st.property_id');
		$query->where('pt.type_name LIKE \''.$poptype.'\' AND st.name LIKE \''.$subtype.'\'');
		$db->setQuery($query);
		$ptype = $db->loadObjectList();
		return $ptype;
	}
	public function updatePropertyExpiry($lid, $newval){
		$edit2 = new JObject();
		$edit2->listing_id = $lid;
		$edit2->date_expired = $newval;
		$edit2->closed= 0;
		$result2 = JFactory::getDbo()->updateObject('#__pocket_listing', $edit2, 'listing_id');
	}
	public function getSelectedListing($listing_id, $fallback = false) {
		if(!$fallback)
			return $this->get_listing(array('p.listing_id'=>$listing_id));
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id as lid', 'pl.*', 'pp.*', 'ps.*', 'con.*', 'v.*','conc.country as concode','conc.*', 'pa.*', 'pst.*', 'pt.*','cvs.*'));
		$query->from('#__pocket_listing as pl');
		$query->join('LEFT', '#__property_price as pp ON (pl.listing_id = pp.pocket_id)');
		$query->join('LEFT', '(select pocket_id, group_concat(address separator "-----") as address from #__pocket_address group by pocket_id) as pa ON (pl.listing_id = pa.pocket_id)');
		$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = pl.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = pl.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = pl.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = pl.sub_type');
		$query->leftJoin('#__permission_setting ps ON ps.listing_id = pl.listing_id');
		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = pl.listing_id');
		$query->where('pl.listing_id = \''.$listing_id.'\'');
		$query->order('pl.listing_id DESC LIMIT 1');
		$db->setQuery($query);
		$individual = $db->loadObjectList();
		return $individual;
	}
	public function get_property_image($lID){
		$db		= $this->getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__pocket_images');
		$query->where('listing_id LIKE \''. $lID .'\' 
			ORDER BY CASE 
		    WHEN order_image = 0 THEN image_id
		    ELSE order_image
			END ASC'
		);
		//$query->order('order_image ASC');
		$db->setQuery($query);
		$images = $db->loadObjectList();
		return $images;
	}
	public function get_listing($filters){
		$conditions = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('p.*,ps.*,pp.*,pa.*,pi.*,v.*,re.*,viewers.*,pt.*,pst.*, conc.symbol, conc.currency, cvs.*,
			p.listing_id as listing_id,
			con.countries_iso_code_2 AS conIso,
			con.countries_id AS conCode,
			p.property_name as property_name
		');
		$query->from('#__pocket_listing p');
		foreach ($filters as $key => $value){
			if($value!="" && $value){
				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id")
					$conditions[] = $key." = ".$value;
				else if($key!="pp.price")
					$conditions[] = $key." > ".$value;
				else{
					$explode = explode('-', $value);
					if(count($explode)>1){
						$conditions[] = $key."1 >= ".$explode[0]." AND ".$key."2 =< ".$explode[1];
					}
					else
						$conditions[] = $key."1 > ".$value;
				}
			}
		}
		if(count($conditions)>0)
			$query->where($conditions);
		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');
		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(image ORDER BY image_id ASC) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');
		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');
		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed
							FROM #__request_access racs
							WHERE permission = 1
							GROUP BY racs.property_id
							) as viewers on viewers.id = p.listing_id');
		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');
		$query->leftJoin('#__countries con ON con.countries_id = p.country');
		$query->leftJoin('#__country_currency conc ON conc.currency = p.currency');
		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');
		$query->leftJoin('#__property_sub_type pst ON pst.sub_id = p.sub_type');
		$query->order('p.setting desc');
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	public function printPropertyState($state_id) {
		$db = JFactory::getDbo();
		// GET SELECTED LISTING
		$query = $db->getQuery(true);
		$query->select('zone_name');
		$query->from('#__zones');
		$query->where('zone_id = \''.$state_id.'\'');
		$db->setQuery($query);
		$state = $db->loadObject();
		return $state;
	}
}
?>