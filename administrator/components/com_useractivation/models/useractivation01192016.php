<?php

defined('_JEXEC') or die;



class UserActivationModelUserActivation extends JModelLegacy 



{	



	public function __construct($config = array())

        {   

                

				$config['filter_fields'] = array(

                        'ur.user_id',

						'ur.firstname',

                        'ur.lastname',

						'LOWER(ur.email)',

						'ur.user_type',

						'ur.licence',

						'ur.brokerage_license',

						'us.verified_2012',

						'us.verified_2013'

 

                );

                parent::__construct($config);

				

        }

	

	protected function populateState($ordering = null, $direction = null) {

       	 

	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');

	    $this->setState('list.ordering', $orderCol);



		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');

		$this->setState('list.direction', $listOrder);

	   

	    parent::populateState('ur.user_id', 'ASC');



	   }

	

	

		

	public function getListQuery() {

          

	   $db = JFactory::getDbo();



	   $query = $db->getQuery(true);

	   

	   $query->select(

			$this->getState(

				'list.select',

				'ur.*'

			)

		);



		$query->from($db->quoteName('#__user_registration').' AS ur');



        $query->order($db->escape($this->getState('list.ordering', 'ur.user_id')).' '.

                

				$db->escape($this->getState('list.direction', 'ASC')));

 

        return $query;

		

	

	}

	

	

	function getOrigEmail ($uid) {

		$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('email');

		$query->from('#__user_registration');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		return $db->loadObject()->email;

	

	}

	

	

	function updateUserName ($orig_email, $data) {

		$db = JFactory::getDbo();	

		$us_object = new JObject();

		$email = $data['email'];

		$us_object->email = $orig_email;

		$us_object->username = $email;

		$db->updateObject('#__users', $us_object, 'email');

    } 




	

	function updateUserEmail ($data) {

		$db = JFactory::getDbo();	

		$urname_object = new JObject();

		$email = $data['email'];

		$urname_object->username = $email;

		$urname_object->email = $email;

		return $db->updateObject('#__users', $urname_object, 'username');

	

    } 





	function updateUserRegEmail ($data) {

		$db = JFactory::getDbo();	

		$ur_object = new JObject();

		$uid = $data['uid'];

		$email = $data['email'];

		$ur_object->user_id = $uid ;

		$ur_object->email = $email;

		return $db->updateObject('#__user_registration', $ur_object, 'user_id');

			

    } 

    function updateUserEmailInvitation($orig_email, $data) {

		$db = JFactory::getDbo();
 
		$query = $db->getQuery(true);
		 
		// Fields to update.
		$fields = array(
		   'invited_email = ' . "'" .$data['email']. "'",
		);
		 
		// Conditions for which records should be updated.
		$conditions = array(
		    'invited_email  = ' . "'" .$orig_email. "'"
		);
		 
		$query->update($db->quoteName('#__invitation_tracker'))->set($fields)->where($conditions);
		 
		$db->setQuery($query);
		 
		$result = $db->execute();

		return $result;

    } 


    function getZipWorkaround($uid){

    	$db = JFactory::getDbo();	

    	$query2 = $db->getQuery(true);

		$query2->select('*');

		$query2->from('#__user_zips_workaround uz');

		$query2->leftJoin('#__country_validations cv ON cv.country = uz.zip_country');

		$query2->where('user_id = '.$uid);

		$db->setQuery($query2);

		$result1 = $db->loadObjectList();

		return $result1;

    }

    function updateWorkZips($uid, $data) {

    	$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__user_zips_workaround');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		$result1 = $db->loadObjectList();

		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->zip_workaround=strtoupper($data['w_zips']);
			$a_details->zip_country=strtoupper($data['wz_country']);

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_zips_workaround', $a_details);

		} else {

			// Create an object for the record we are going to update.
			$object = new stdClass();
			 
			// Must be a valid primary key value.
			$object->user_id = $uid;

			if($data['w_zips']){
				$object->zip_workaround = strtoupper($data['w_zips']);
			}

			$object->zip_country = strtoupper($data['wz_country']);

			 
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_zips_workaround', $object, 'user_id');

		}

    } 


    function updateAssistantDetails($uid, $data) {

    	$db = JFactory::getDbo();		

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__user_assistant');

		$query->where('user_id = '.$uid);

		$db->setQuery($query);

		$result1 = $db->loadObjectList();

		if(!$result1){
			// Create and populate an object.
			$a_details = new stdClass();
			$a_details->user_id = $uid;
			$a_details->a_fname=$data['a_fname'];
			$a_details->a_lname=$data['a_lname'];
			$a_details->a_title=$data['a_title'];
			$a_details->a_email=$data['a_email'];
			$a_details->a_pnumber=$data['a_pnumber'];
			if($data['cc_all']){
				$a_details->cc_all = 1;
			}
			$a_details->date_created=time();

			// Insert the object into the user profile table.
			$result = JFactory::getDbo()->insertObject('#__user_assistant', $a_details);

		} else {

			// Create an object for the record we are going to update.
			$object = new stdClass();
			 
			// Must be a valid primary key value.
			$object->user_id = $uid;

			if($data['a_fname']){
				$object->a_fname = $data['a_fname'];
			}
			if($data['a_lname']){
				$object->a_lname = $data['a_lname'];
			}
			if($data['a_title']){
				$object->a_title = $data['a_title'];
			}
			if($data['a_email']){
				$object->a_email = $data['a_email'];
			}
			if($data['a_pnumber']){
				$object->a_pnumber = $data['a_pnumber'];
			}

			if($data['cc_all']){
				$object->cc_all = 1;
			}
			 
			// Update their details in the users table using id as the primary key.
			$result = JFactory::getDbo()->updateObject('#__user_assistant', $object, 'user_id');

		}

    } 



	function updateUserInfo ($data) {



		$db = JFactory::getDbo();

		

		$uid = $data['uid'];

	

		$update = new JObject();

		

		$updatesales = new JObject();

		

		$updatename = new JObject();

		

		$namearr = array($data['firstname'],$data['lastname']);

		

		$name = implode(" ",$namearr);

		

		$updatename->name = $name;

		

		$updatename->email = $data['email'];

		

		$db->updateObject('#__users', $updatename, 'email');

		

		//UPDATE REGISTRATION TABLE



		$update->user_id = $uid;

		

		$updatesales->agent_id = $uid;

		

		$update->firstname = $data['firstname'];

		

		$update->lastname = $data['lastname'];

		$update->user_type = $data['user_type'];
		

		if ($data['brokerlicense']) {

			$update->brokerage_license = $this->encrypt($data['brokerlicense']);

		} else {
			
			$update->brokerage_license = '';
		}
		
		if ($data['agentlicense']) {

			$update->licence = $this->encrypt($data['agentlicense']);

		} else {
			
			$update->licence = '';
		}
		
	
		$updatesales->volume_2015 = (int) str_replace(",", "", $data['volume_2015']);
		
		
		$updatesales->volume_2014 = (int) str_replace(",", "", $data['volume_2014']);
		
		
		$updatesales->volume_2013 = (int) str_replace(",", "", $data['volume_2013']);
	

		$updatesales->volume_2012 = (int) str_replace(",", "", $data['volume_2012']);

		
		$updatesales->sides_2015 = (int) str_replace(",", "", $data['sides_2015']);
		
		
		$updatesales->sides_2014 = (int) str_replace(",", "", $data['sides_2014']);
		
		
		$updatesales->sides_2013 = (int) str_replace(",", "", $data['sides_2013']);


		$updatesales->sides_2012 = (int) str_replace(",", "", $data['sides_2012']);

		
		$updatesales->ave_price_2012 = $updatesales->volume_2012 / $updatesales->sides_2012;

		
		$updatesales->ave_price_2013 = $updatesales->volume_2013 / $updatesales->sides_2013;
		
		
		$updatesales->ave_price_2014= $updatesales->volume_2014 / $updatesales->sides_2014;
		
		
		$updatesales->ave_price_2015= $updatesales->volume_2015 / $updatesales->sides_2015;


		$updatesales->verified_2013 = $data['verified_2013'];
		

		$updatesales->verified_2012 = $data['verified_2012'];
		
		
		$updatesales->verified_2014 = $data['verified_2014'];
		
		
		$updatesales->verified_2015 = $data['verified_2015'];

		

		$result = $db->updateObject('#__user_registration', $update, 'user_id');

		

		$result2 = $db->updateObject('#__user_sales', $updatesales, 'agent_id');

		

		

    }

	

	function encrypt($plain_text) {

	

		$key = 'password to (en/de)crypt';

		

		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));

		

		return $encrypted;

	}

	

		public function get_designations($countryID){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id as value', 'designations as label'));

		$query->from('#__designations');

		$query->where('country_available LIKE \''.$countryID.'\'');

		$db->setQuery($query);

		return  $db->loadObjectList();

	}
	
	public function get_user_designations($userid, $own = false){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('id','designations'));

		$query->from('#__designations');

		$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

		return $db->setQuery($query)->loadObjectList();

	}	
	
	public function updateActivationDate($email){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$update = new JObject();
		
		$date = date('Y-m-d H:i:s',time());
		
		$update->email = $email;
		
		$update->sendEmail_date = $date;

		$db->updateObject('#__user_registration', $update, 'email');

	}

	public function getUsersForCSV(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('user_id','firstname','lastname','user_type','city','c.zone_name','is_premium','registration_date','sendEmail_date','activation_date', 'registerDate','completeReg_date','is_term_accepted'));

		$query->join('LEFT', $db->quoteName('#__zones', 'c') . ' ON (' . $db->quoteName('c.zone_id') . ' = ' . $db->quoteName('#__user_registration.state') . ')');
		
		$query->join('LEFT', $db->quoteName('#__users', 'u') . ' ON (' . $db->quoteName('u.email') . ' = ' . $db->quoteName('#__user_registration.email') . ')');

		$query->from('#__user_registration');

		$db->setQuery($query);

	    return $db->loadAssocList();

		//$query->where('id IN (SELECT desig_id FROM #__user_designations WHERE user_id = '.$userid.')');

	}
	public function updateProfileImage($data){		$db = JFactory::getDbo();				$user_object = new JObject();				$uid = $data['uid'];		$email = $data['email'];		$user_object->user_id = $uid;		$user_object->email = $email;				$user_object->image = $data['image'] ;				return $db->updateObject('#__user_registration', $user_object, 'email');	}
	
	
	
	public function getAllPropertyTypes() {

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('*'));

		$query->from('#__property_type');

		$db->setQuery($query);

		$ptype = $db->loadObjectList();

		return $ptype;

	}
	
	public function getAllSubPropTypes($ptype_id) {

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('*'));

		$query->from('#__property_sub_type');

		$query->where('property_id ='.$ptype_id);

		$db->setQuery($query);

		$ptype = $db->loadObjectList();

		return $ptype;

	}
	
	public function getCountry($email) {

		$db = JFactory::getDbo();

		//GET COUNTRY
		$query = $db->getQuery(true);

		$query->select('ur.country, countries_iso_code_2, curr.currency, curr.symbol');

		$query->from('#__user_registration ur');

		$query->leftJoin('#__countries ON countries_id = ur.country');
		$query->leftJoin('#__country_currency curr ON curr.country = ur.country');

		$query->where('email = \''.$email.'\'');

		$db->setQuery($query);

		$country = $db->loadObjectList();

		return $country;

	}
	
	
	public function getStateIdByName($state_name) {

		$db = JFactory::getDbo();

		// GET SELECTED LISTING

		$query = $db->getQuery(true);

		$query->select('zone_id');

		$query->from('#__zones');

		$query->where('zone_name = \''.$state_name.'\'');

		$db->setQuery($query);

		$state = $db->loadResult();

		return $state;

	}
	
public function getPtypeByName($ptype_name) {



		$db = JFactory::getDbo();



		// GET SELECTED LISTING



		$query = $db->getQuery(true);



		$query->select('type_id');



		$query->from('#__property_type');



		$query->where('type_name = \''.$ptype_name.'\'');



		$db->setQuery($query);



		$ptype_name = $db->loadResult();



		return $ptype_name;



	}



	public function getSubTypeByName($ptype_id,$subtype_name) {



		$db = JFactory::getDbo();



		// GET SELECTED LISTING



		$query = $db->getQuery(true);



		$query->select('sub_id');



		$query->from('#__property_sub_type');



		$query->where('name = \''.$subtype_name.'\' AND property_id='.$ptype_id);



		$db->setQuery($query);



		$subtype_name = $db->loadResult();



		return $subtype_name;



	}
	
	public function insertPocket($user_id, $data) {



		$db = JFactory::getDbo();



		$insert = new JObject();



		$insert->user_id = $user_id;



		$insert->property_name = $db->escape($data['property_name']);



		$insert->city = $data['city'];



		$insert->zip = $data['zip'];



		$insert->state = $data['state'];



		$insert->country = $data['country'];



		$insert->currency = $data['currency'];



		$insert->property_type = $data['ptype'];



		$insert->sub_type = $data['stype'];



		$insert->bedroom = $data['bedroom'];



		$insert->bathroom = $data['bathroom'];



		$insert->unit_sqft = $data['unitsqft'];



		$insert->view = $data['view'];



		$insert->style = $data['style'];



		$insert->year_built = $data['yearbuilt'];



		$insert->pool_spa = $data['poolspa'];



		$insert->condition = $data['condition'];



		$insert->garage = $data['garage'];



		$insert->units = $data['units'];



		$insert->cap_rate = $data['cap'];



		$insert->grm = $data['grm'];



		$insert->occupancy = $data['occupancy'];



		$insert->type = $data['type'];



		$insert->listing_class = $data['class'];



		$insert->parking_ratio = $data['parking'];



		$insert->ceiling_height = $data['ceiling'];



		$insert->stories = $data['stories'];



		$insert->room_count = $data['roomcount'];



		$insert->type_lease = $data['typelease'];



		$insert->type_lease2 = $data['typelease2'];



		$insert->available_sqft = $data['available'];



		$insert->lot_sqft = $data['lotsqft'];



		$insert->lot_size = $data['lotsize'];



		$insert->bldg_sqft = $data['bldgsqft'];



		$insert->term = $data['term'];



		$insert->furnished = $data['furnished'];



		$insert->pet = $data['pet'];



		$insert->zoned = $data['zoned'];



		$insert->possession = $data['possession'];



		$insert->bldg_type = $data['bldgtype'];



		$insert->features1 = $data['features1'];



		$insert->features2 = $data['features2'];



		$insert->features3 = $data['features3'];



		$insert->setting = $data['settings'];



		$insert->description = $data['desc'];



		$insert->date_created = date("Y-m-d");



		$insert->date_expired = date('Y-m-d', strtotime(date('Y-m-d'). ' + 90 days'));



		$ret = $db->insertObject('#__pocket_listing', $insert);



		//$ret = $db->insertid();



		return $ret;



	}
	
	public function insertPocketAddress($listing_id, $addr) {







		$db = JFactory::getDbo();







		$insert3 = new JObject();







		$insert3->address = $addr;







		$insert3->pocket_id = $listing_id;







		$ret3 = $db->insertObject('#__pocket_address', $insert3);







		return $ret3;







	}







	public function insertPocketImages($listing_id, $v, $k) {







		$db = JFactory::getDbo();







		$insert4 = new JObject();







		if (strpos($v, 'uploads/') === FALSE) {







			$insert4->image = JUri::base(). "uploads/" . $v;







		} else {







			$insert4->image = $v;





		}



		$insert4->order_image = $k;





		$insert4->listing_id = $listing_id;







		$ret4 = $db->insertObject('#__pocket_images', $insert4);







		return $ret4;







	}
	
	public function getListing($user_id) {







		$db = JFactory::getDbo();







		$query = $db->getQuery(true);







		$query->select('listing_id')







		->from('#__pocket_listing')







		->where('user_id = '.$user_id)







		->order('listing_id DESC LIMIT 1');







		$db->setQuery($query);







		$listing = $db->loadObjectList();







		return $listing;







	}
	
	public function insertNewPermissionSetting($listing_id, $data) {







		$db = JFactory::getDbo();







		$insert = new JObject();







		$insert->selected_permission = (int)$data['setting'];







			if( (int)$data['setting']==3 ) {



			



				$psetting1 = str_replace(",", "", $data['psetting1']);







				$insert->values =  (int)(str_replace("$", "", $psetting1));







			} else if ( (int)$data['setting']==4 ) {







				$psetting2 = str_replace(",", "", $data['psetting2']);



				



				$insert->values =  (int)(str_replace("$", "", $psetting1));







			} else if ( (int)$data['setting']==5 ) {







				$insert->values =  (int)($data['psetting3']);







			} else {







				$insert->values = "";







			}











			$insert->listing_id = $listing_id;







			$db->insertObject('#__permission_setting', $insert);







	}

	public function insertPropertyPrice($listing_id, $data) {







		$db = JFactory::getDbo();







		$insert2 = new JObject();







		$insert2->price_type = $data['pricerange'];







		$insert2->price1 = $data['price1'];







		$insert2->price2 = $data['price2'];







		$insert2->disclose = $data['disclose'];







		$insert2->pocket_id = $listing_id;







		$ret2 = $db->insertObject('#__property_price', $insert2);







		return $ret2;







	}
	
	public function insertPropertyActivity($listing_id, $activity) {







		$db = JFactory::getDbo();







		







		#print_r($activity); die();







		#print_r($activity); exit;







		$insert_activity = new JObject();







		$insert_activity->activity_type = (int)$activity['activity_type'];







		$activity_type = (int)$activity['activity_type'];







		$insert_activity->activity_id = (int)$listing_id;







		$insert_activity->user_id = (int)$activity['user_id'];







		$insert_activity->date = $activity['date'];







		#$insert_activity->other_user_id = (int)$activity['user_id'];







		$insert_activity->other_user_id = (isset($activity['other_user_id'])) ? (int)$activity['other_user_id'] : (int)$activity['user_id'];







		if($activity_type == 23 || $activity_type == 24 || $activity_type == 25 || $activity_type == 26){







			$insert_activity->buyer_id = (int)$activity['buyer_id'];







		}	







		$ret = $db->insertObject('#__activities', $insert_activity);







		return $ret;







	}

	
		public function getBuyerAll(){



		$db = JFactory::getDbo();



		$query = $db->getQuery(true);



		



		$query->select('*')->from('#__buyer');



		$buyers = $db->setQuery($query)->loadObjectList();



		//



		foreach ($buyers as $buyer){



			$query = $db->getQuery(true);



			$query->select('*')->from('#__buyer_needs')->where('buyer_id = '.$buyer->buyer_id);



			$buyer->needs = $db->setQuery($query)->loadObjectList();



		}



		//echo "<pre>"; print_r($buyers); die();



		#echo "<pre>";  print_r($buyers); die();



		return $buyers;



	}
	
	public function get_pocket_listing_indi($filters){



		$conditions = array();



		#echo ""; print_r($buyers);



		$db = JFactory::getDbo();



		$query = $db->getQuery(true);



		$query->select('*');



		$query->from('#__pocket_listing p');



		foreach ($filters as $key => $value){



			if($value!="" && $value){



				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id")



					$conditions[] = $key." = ".$value;



				else if($key!="pp.price")



					$conditions[] = $key." > ".$value;



				else{



					//$explode = explode('-', $value);



					//if(count($explode)>1){



						//$conditions[] = $key."1 <= ".$explode[1] ." AND ". $key."2 >= ".$explode[0];



					//}



					//else



					//	$conditions[] = $key."1 >= ".$value;



				}



			}



		}



		if(count($conditions)>0)



			$query->where($conditions);



		$query->leftJoin('#__property_price pp on pp.pocket_id =  p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');



		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');



		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid



							FROM #__request_access racs



							GROUP BY racs.property_id



							) as viewers on viewers.id = p.listing_id');



		$query->leftJoin('#__property_type ON type_id = p.property_type');



		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');



		$query->order('p.setting desc');



		$query->order('p.listing_id desc');



		//$query->order('racs.permission desc');



		$db->setQuery($query);



		return $db->loadObjectList();



		die();



	}
	
		function getExchangeRates_indi(){



			$file = 'latest.json';

			$appId = 'd73f8525552048a7a39aaac9977299fd';



			// Open CURL session:

			$ch = curl_init("https://openexchangerates.org/api/{$file}?app_id={$appId}");

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



			// Get the data:

			$json = curl_exec($ch);

			curl_close($ch);



			// Decode JSON response:

			$exchangeRates = json_decode($json);



			// You can now access the rates inside the parsed object, like so:

			/*printf(

			    "1 %s in GBP: %s (as of %s)",

			    $exchangeRates->base,

			    $exchangeRates->rates->GBP,

			    date('H:i jS F, Y', $exchangeRates->timestamp)

			);*/



			return $exchangeRates;



			



		}
		
	public function resetBuyerListings($bid, $type){



		$db = JFactory::getDbo();



		$update = new JObject();



		$update->buyer_id = $bid;



		$update->reset_new = 1;



		$update->listingcount_new = 0;



		if($type == 0){	



			$update->hasnew = 0;



			$update->hasnew_2 = 0;



		} else if($type == 1){



			$update->hasnew = 0;



		} else if($type == 2){



			$update->hasnew_2 = 0;



		}		



		$db->updateObject('#__buyer', $update, 'buyer_id', false);



	}
	
	public function countlisting($buyer_id) {



		$model = $this;

	



		$buyers = $model->getBuyer($buyer_id);



		$search_key = $buyers[0]->needs[0]->zip;



		$searchreturn = $model->searchZip($search_key);



		$pocketlistings = array();



		$pocketlistings_0 = array();



		$pocketlistings_1 = array();



		$ctr = 1;



		$pointctr = 0;



		$listing = $model->get_pocket_listing_indi_filter(array('pp.price'=>$buyers[0]->price_value), $buyers);



		$price = explode('-', $buyers[0]->price_value);



		$usethis = 1;



		if($price[1] == ''){$usethis = 0;}



		#print_r($listing); die();



		foreach($listing as $search){	



			if($search->property_type == 1){



				if($search->sub_type == 1){



					



					if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->condition == $buyers[0]->needs[0]->condition){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					} if($search->features3 == $buyers[0]->needs[0]->features3){



						$pointctr++;



					}



					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 2){



					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){



						$pointctr++;



					}  if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					



					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 3){



					if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){



						$pointctr++;



					}  if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 4){



					if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



			}



			if($search->property_type == 2){



				if($search->sub_type == 5){



					if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->condition == $buyers[0]->needs[0]->condition){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 6){



				 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){



						$pointctr++;



					}  if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 7){



					 if($search->bldg_type == $buyers[0]->needs[0]->bldg_type){



						$pointctr++;



					} if($search->view == $buyers[0]->needs[0]->view){



						$pointctr++;



					} if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					} if($search->pool_spa == $buyers[0]->needs[0]->pool_spa){



						$pointctr++;



					} if($search->garage == $buyers[0]->needs[0]->garage){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 8){



					 if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



			}



			if($search->property_type == 3){



				if($search->sub_type == 9){



					 if($search->style == $buyers[0]->needs[0]->style){



						$pointctr++;



					}  if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}



					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 10){



					 if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 11){



					if($search->ceiling_height == $buyers[0]->needs[0]->ceiling_height){



						$pointctr++;



					} if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){



						$pointctr++;



					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 12){



					if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->occupancy == $buyers[0]->needs[0]->occupancy){



						$pointctr++;



					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 13){



					if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 14){



				if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					}



					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 15){



					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



			}



			if($search->property_type == 4){



				if($search->sub_type == 16){



					if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 17){



					if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->ceiling_height == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



				if($search->sub_type == 18){



				 if($search->stories == $buyers[0]->needs[0]->stories){



						$pointctr++;



					} if($search->year_built == $buyers[0]->needs[0]->year_built){



						$pointctr++;



					} if($search->parking_ratio == $buyers[0]->needs[0]->parking_ratio){



						$pointctr++;



					} if($search->features1 == $buyers[0]->needs[0]->features1){



						$pointctr++;



					} if($search->features2 == $buyers[0]->needs[0]->features2){



						$pointctr++;



					}

					$search->abcd = $pointctr;



					array_push($pocketlistings_0, $search);

					$pointctr = 0;



				}



			}



		}



		#die();



		usort($pocketlistings_0, function($a, $b){



			if($a->abcd == $b->abcd){



				if($a->price1 > $b->price1){



					return 1;



				} else {



					return -1;



				}



				if($a->price1 == $b->price1){



					if($a->listing_id > $b->listing_id){



						return 1;



					} else {



						return -1;



					}



				}



			}

			if(($a->abcd < $b->abcd)){



				return 1;



			} else {



				return -1;



			}



		});



		

		foreach($pocketlistings_0 as $indi){



		

				array_push($pocketlistings, $indi);

					

		}



		

		return count($pocketlistings);



	}
	
	public function insertToBuyerListings($bid, $pid, $listingAll, $pops_trans=0){



		$db = JFactory::getDbo();





		$query = $db->getQuery(true);

		$query

		->select('*')

		->from('#__buyer_new_listing')

		->where('listing_id = ' . $pid. ' AND buyer_id='.$bid);

		$db->setQuery($query);

		$objects = $db->loadRow();



		if(count($objects)==0){



			$insert = new JObject();



			



			$insert->buyer_id = $bid;



			$insert->listing_id = $pid;



			



			$ret = $db->insertObject('#__buyer_new_listing', $insert);



			$refid = $db->insertid();



			$query0 = $db->getQuery(true);



			//$query0 = "SELECT COUNT(DISTINCT listing_id) as 'bnl_count' FROM #__buyer_new_listing WHERE buyer_id='".$bid."'";''



			$query0 = "SELECT COUNT( listing_id ) AS 'bnl_count', COUNT( * ) - COUNT( DISTINCT listing_id ) AS  'dup_list' FROM tbl_buyer_new_listing WHERE buyer_id =".$bid." GROUP BY listing_id HAVING dup_list =0";



			$db->setQuery($query0);



			$bnl = $db->loadObjectList();

			

			$bnl_count = $listingAll;



			$bnl_new = count($bnl);



			$query1 = $db->getQuery(true);



			$query1 = "SELECT listingcount FROM #__buyer WHERE buyer_id='".$bid."'";



			$db->setQuery($query1);



			$bl = $db->loadObjectList();



			$bl_count = $bl[0]->listingcount;



			



			



			if($bnl_new && $pops_trans){



				$query3 = $db->getQuery(true);



				$query3 = "SELECT listingcount_new FROM #__buyer WHERE buyer_id='".$bid."' AND hasnew_2=1";



				$db->setQuery($query3);



				$listing_news = $db->loadObjectList();



				$listing_news = $listing_news[0]->listingcount_new;





				if($pops_trans==1){

					$bnl_new = $listing_news+1;

				} 





				$update = new JObject();



				$update->listingcount = $bnl_count;



				$update->buyer_id = $bid;



				$update->hasnew = 1;



				$update->listingcount_new = $bnl_new;



				$update->hasnew_2 = 1;



				$upd = $db->updateObject('#__buyer', $update, 'buyer_id', false);



				echo $bnl_count." > ".$bl_count;



			} else {



				echo "none";



			}



		}



		$_SESSION['viewed_buyerspage'] = 0;



		$_SESSION['viewed_buyerspage_2'] = 0;



		return $ret;



	}
	
	public function getBuyer($buyerid){

	



		$db = JFactory::getDbo();



		$query = $db->getQuery(true);	



		$query->select('*')->from('#__buyer')->where(array('buyer_id = '.$buyerid));



		$buyers = $db->setQuery($query)->loadObjectList();



		#print_r($buyers); die();



		foreach ($buyers as $buyer){



			$query = $db->getQuery(true);



			$query->select('bn.*,conc.country,  cvs.*, conc_c.currency, conc_c.symbol')->from('#__buyer_needs bn')

			->leftJoin('#__country_currency conc_c ON conc_c.currency = bn.currency')

			->leftJoin('#__country_validations cvs ON bn.country = cvs.country')

			->leftJoin('#__country_currency conc ON conc.country = bn.country')

			->where('bn.buyer_id = '.$buyerid);



			$buyer->needs = $db->setQuery($query)->loadObjectList();



		}



		return $buyers;



	}
	
	public function searchZip($searchval){



		$db = JFactory::getDbo();



		$query = $db->getQuery(true);



		#$query = "SELECT * FROM #__pocket_listing WHERE user_id='".$user."' AND zip LIKE '".$searchval."%'";



		$query = "SELECT * FROM #__pocket_listing WHERE zip LIKE '".$searchval."%'";



		$db->setQuery($query);



		//echo "<pre>"; print_r($db->loadObjectList()); die();



		return $db->loadObjectList();



	}
	
	public function get_pocket_listing_indi_filter($filters, $buyers){



		

		$conditions = array();



		$db = JFactory::getDbo();



		$today = date ('Y-m-d');

		

		//get listing details

		$query = $db->getQuery(true);



		$query->select("*, pp.price_type, p.listing_id, currcon.*, IFNULL(pp.price2,'exact_price') as abcd, cvs.*");



		$query->from('#__pocket_listing p');

		

		$query->leftJoin('#__country_currency currcon ON currcon.currency = p.currency');



		$query->leftJoin('#__country_validations cvs ON cvs.country = p.country');



		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		

		$query->where('date_expired > \''. $today .'\' AND sold = 0');



		foreach ($filters as $key => $value){



			if($value!="" && $value){



				if($key=="p.property_type" || $key=="p.user_id" || $key=="p.listing_id") {



					if($key=="p.user_id") {



						$user_id = $value;	



					}					



					$conditions[] = $key." = ".$value;



				} else if($key!="pp.price") {



					$conditions[] = $key." > ".$value;



				} else{



					$property_type = $buyers[0]->needs[0]->property_type;



					$sub_type = $buyers[0]->needs[0]->sub_type;		



					$country = 	$buyers[0]->needs[0]->country;			



					// zip extract



					$zip = explode(",",$buyers[0]->needs[0]->zip);



					$zip_cnt=0;



					$zip_condition = "";



					foreach($zip as $row){



						



						if($zip_cnt<count($zip)){



							if($country==222){



								$zip_cnt++;



								$zip_condition .= "p.zip LIKE '".trim($row)."%'";



							} else {



								$zip_cnt++;



								$zip_condition .= "p.zip ='".$row."'";



							}



						}





						if($zip_cnt<count($zip)){



							$zip_condition .=" OR ";



							



						}



					}



					



					//if(count($zip)>=2){



						$conditions[] = "(".$zip_condition.") AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";



					//} else {



					//	$conditions[] = "p.zip='".$buyers[0]->needs[0]->zip."' AND p.property_type='".$buyers[0]->needs[0]->property_type."' AND p.sub_type='".$buyers[0]->needs[0]->sub_type."' AND p.country ='".$country."'";



					//}



					



					switch($property_type){



						case 1:



						if($sub_type == 1){



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";		



						} else if($sub_type == 2){



							$unitrange = $buyers[0]->needs[0]->unit_sqft;



							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array



							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";			



						} else if($sub_type == 3){



							$unitrange = $buyers[0]->needs[0]->unit_sqft;



							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array



							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."'";				



						} else if($sub_type == 4){



							$zrange = $buyers[0]->needs[0]->zoned;



							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array



							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer



							$lotszrange = $buyers[0]->needs[0]->lot_size;



							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array



							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer



							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."'";		



						}



						break;



						case 2:



						if($sub_type == 5){



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.pet >='".$buyers[0]->needs[0]->pet."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."' AND p.possession >='".$buyers[0]->needs[0]->possession."'";



						} else if($sub_type == 6){



							$unitrange = $buyers[0]->needs[0]->unit_sqft;



							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array



							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."'  AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";



						} else if($sub_type == 7){



							$unitrange = $buyers[0]->needs[0]->unit_sqft;



							$unitexplode = explode("-", $unitrange); // split the ranged value into two, returning an array



							$unitlow = str_replace(",", "", $unitexplode[0]); //lower range for buyer



							$conditions[] = "p.bedroom >='".$buyers[0]->needs[0]->bedroom."' AND p.bathroom >='".$buyers[0]->needs[0]->bathroom."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.unit_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitlow."' AND p.term >='".$buyers[0]->needs[0]->term."' AND p.furnished >='".$buyers[0]->needs[0]->furnished."'  AND p.pet >='".$buyers[0]->needs[0]->pet."'";



						} else if($sub_type == 8){



							$zrange = $buyers[0]->needs[0]->zoned;



							$zexplode = explode("-", $zrange); // split the ranged value into two, returning an array



							$zlow = str_replace(",", "", $zexplode[0]); //lower range for buyer



							$lotszrange = $buyers[0]->needs[0]->lot_size;



							$lotszexplode = explode("-", $lotszrange); // split the ranged value into two, returning an array



							$lotszlow = str_replace(",", "", $lotszexplode[0]); //lower range for buyer



							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.lot_size,',','') , '-' ,1), UNSIGNED INTEGER) >='".$lotszlow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.zoned,',','') , '-' ,1), UNSIGNED INTEGER) >='".$zlow."' AND p.view ='".$buyers[0]->needs[0]->view."'";



						}



						break;



						case 3:



						if($sub_type == 9){



							$unitsrange = $buyers[0]->needs[0]->units;



							$unitsexplode = explode("-", $unitsrange); // split the ranged value into two, returning an array



							$unitslow = str_replace(",", "", $unitsexplode[0]); //lower range for buyer



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "CONVERT(SUBSTRING_INDEX(REPLACE(p.units,',','') , '-' ,1), UNSIGNED INTEGER) >='".$unitslow."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";



						} else if($sub_type == 10){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";



						} else if($sub_type == 11){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";



						} else if($sub_type == 12){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";



						} else if($sub_type == 13){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$roomrange = $buyers[0]->needs[0]->room_count;



							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array



							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer



							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";



						} else if($sub_type == 14){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$roomrange = $buyers[0]->needs[0]->room_count;



							$roomexplode = explode("-", $roomrange); // split the ranged value into two, returning an array



							$roomlow = str_replace(",", "", $roomexplode[0]); //lower range for buyer



							$conditions[] = "p.type ='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.room_count,',','') , '-' ,1), UNSIGNED INTEGER) >='".$roomlow."'";



						} else if($sub_type == 15){



							$caprange = $buyers[0]->needs[0]->cap_rate;



							$capexplode = explode("-", $caprange); // split the ranged value into two, returning an array



							$conditions[] = "p.type='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.cap_rate,'.','') , '-' ,1), UNSIGNED INTEGER) >='".$capexplode[0]."'";



						}



						break;



					case 4:



						if($sub_type == 16){



							$avlbrange = $buyers[0]->needs[0]->available_sqft;



							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array



							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer



							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.listing_class >='".$buyers[0]->needs[0]->listing_class."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";



						} else if($sub_type == 17){



							$avlbrange = $buyers[0]->needs[0]->available_sqft;



							$avlbexplode = explode("-", $avlbrange); // split the ranged value into two, returning an array



							$avlblow = str_replace(",", "", $avlbexplode[0]); //lower range for buyer



							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND p.type_lease >='".$buyers[0]->needs[0]->type_lease."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.available_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$avlblow."'";



						} else if($sub_type == 18){



							$bldgrange = $buyers[0]->needs[0]->bldg_sqft;



							$bldgexplode = explode("-", $bldgrange); // split the ranged value into two, returning an array



							$bldglow = str_replace(",", "", $bldgexplode[0]); //lower range for buyer



							$conditions[] = "p.type >='".$buyers[0]->needs[0]->type."' AND CONVERT(SUBSTRING_INDEX(REPLACE(p.bldg_sqft,',','') , '-' ,1), UNSIGNED INTEGER) >='".$bldglow."'";



						}



						break;



					}



					$explode = explode('-', $value);

					

					//	if($value->currency != $buyers[0]->needs[0]->currency){

					//		$explode[1] = $this->getExchangeRates($explode[1],$buyers[0]->needs[0]->currency,$value->currency);

					

					//}



					//	$conditions[]= $key."1 <= ".$explode[1];

					

		

				}



			} 



		}

		



		if(count($conditions)>0) {



			$query->where($conditions);



		}



		$query->leftJoin('#__permission_setting ps on ps.listing_id =  p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(address) as address, pocket_id from #__pocket_address group by pocket_id) pa on pa.pocket_id =  p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(image) as images, listing_id from #__pocket_images group by listing_id) pi on p.listing_id =  pi.listing_id');



		$query->leftJoin('(SELECT COUNT( DISTINCT (user_id) ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');



		$query->leftJoin('(SELECT GROUP_CONCAT(CONCAT(agent_a,\'-\',status)) as referrers, property_id from #__referral group by property_id) re on re.property_id =  p.listing_id');



		$query->leftJoin('(SELECT racs.property_id as id, GROUP_CONCAT(racs.user_b) as allowed, racs.permission as per, racs.pkId as pkid



							FROM #__request_access racs



							WHERE racs.user_b = ' . $buyers[0]->agent_id . '



							GROUP BY racs.property_id



							) as viewers on viewers.id = p.listing_id'); 



		$query->leftJoin('#__property_type ON type_id = p.property_type');



		$query->leftJoin('#__property_sub_type ON sub_id = p.sub_type');



		$db->setQuery($query);



		$original_array=$db->loadObjectList();



		$i=0;



		$ex_rates = $this->getExchangeRates_indi();


		foreach ($original_array as $key => $value) {





				if($value->user_id != JFactory::getUser()->id){

					if($value->currency != $buyers[0]->needs[0]->currency){

						

						$ex_rates_con = $ex_rates->rates->{$value->currency};

						if(!$buyers[0]->needs[0]->currency){

							$buyers[0]->needs[0]->currency ="USD";

						}

						$ex_rates_can = $ex_rates->rates->{$buyers[0]->needs[0]->currency};

					

						if($value->currency=="USD"){					

							$value->price1=$value->price1 * $ex_rates_can;

						} else {

							$value->price1=($value->price1 / $ex_rates_con) * $ex_rates_can;							

						}



						if(($value->price2 != null || $value->price2 != "")){

							if($value->currency=="USD"){					

								$value->price2=$value->price2 * $ex_rates_can;

							} else {

								$value->price2=($value->price2 / $ex_rates_con) * $ex_rates_can;							

							}

						}

						

					

						if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){

						unset($original_array[$i]);

						}

					}

				} else {

					if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){

						unset($original_array[$i]);

						}

				}

				

				if(($value->price1 > $explode[1] && ($value->price2 != null || $value->price2 != "")) || (( $value->price2 < $explode[0]) && ($value->price2 != null || $value->price2 != ""))  || (($value->price2 == null || $value->price2 == "") && ($value->price1 < $explode[0] || $value->price1 > $explode[1]))){

					unset($original_array[$i]);

				}









				$i++;

		}



		return $original_array;

		

		die();



	}

}

?>