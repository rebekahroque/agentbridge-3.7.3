<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class UserActivationController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */



	public function update_single_designation(){

		$model 	= $this->getModel('UserProfile');

		if( JRequest::getVar('desgination_id')!="" ) {

			$designation_id = JRequest::getVar('desgination_id');
			$user_id =  JRequest::getVar('user_id');
			$user_id = (int)$user_id;

			$result = $model->update_single_designation( $designation_id, $user_id );

		}

		echo json_encode((int)$user_id );
		
		die();
	}
	
	public function delete_single_designation(){

		$model 	= $this->getModel('UserProfile');

		if( JRequest::getVar('desgination_id')!="" ) {

			$designation_id = JRequest::getVar('desgination_id');
			$user_id =  JRequest::getVar('user_id');
			$user_id = (int)$user_id;

			$result = $model->delete_single_designation( $designation_id, $user_id );

		}

		echo json_encode((int)$user_id );
		
		die();
	}
	
	function upload_agent()

	{

		$view = &$this->getView($this->getName(), 'html');

		$view->display($this->getTask());

	}

	

	function encrypt($plain_text) {

	

		$key = 'password to (en/de)crypt';

		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));

		return $encrypted;

	}	

	

	function process_csv()

	{

	

		// INITIALIZE DATABASE CONNECTION

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$application = JFactory::getApplication();



		if ($_FILES[csv][size] > 0) {



			//get the csv file

			$file = $_FILES[csv][tmp_name];

			$handle = fopen($file,"r");

			

			//loop through the csv file and insert into database

			do {

				if ($data[0]) {



					$insert_temp_agent = new JObject();

					$insert_temp_agent->agent_bridge_num	= $data[0];

					$insert_temp_agent->first_name 		 	= $data[1];

					$insert_temp_agent->last_name 		 	= $data[2];

					$insert_temp_agent->company 			= $data[3];

					$insert_temp_agent->mobile 			 	= $data[4];

					$insert_temp_agent->work 				= $data[5];

 					$insert_temp_agent->fax 				= $data[6];

					$insert_temp_agent->email 			 	= $data[7];

					$insert_temp_agent->country			 	= $data[8];

					$insert_temp_agent->address_1 		 	= $data[9];

					$insert_temp_agent->address_2 		 	= $data[10];

					$insert_temp_agent->city 				= $data[11];

					$insert_temp_agent->states 			 	= $data[12];

					$insert_temp_agent->zip_code 			= $data[13];

					$insert_temp_agent->al_number 		 	= $data[14];

					$insert_temp_agent->bl_number 		 	= $data[15];

					$insert_temp_agent->brand 				= $data[16];

					$insert_temp_agent->year_licensed 	 	= $data[17];

					$insert_temp_agent->volume_2012 		= (int) $data[18];

					$insert_temp_agent->sides_2012 			= (int) $data[19];

					$insert_temp_agent->ave_price_2012  	= (int) $data[20];

					$insert_temp_agent->verified_2012 	 	= (int) $data[21];

					$insert_temp_agent->volume_2013 		= (int) $data[22];

					$insert_temp_agent->sides_2013 			= (int) $data[23];

					$insert_temp_agent->ave_price_2013 		= (int) $data[24];

					$insert_temp_agent->verified_2013	  	= (int) $data[25];
					
					$insert_temp_agent->volume_2014 		= (int) $data[26];

					$insert_temp_agent->sides_2014			= (int) $data[27];

					$insert_temp_agent->ave_price_2014 		= (int) $data[28];

					$insert_temp_agent->verified_2014	  	= (int) $data[29];

					$insert_temp_agent->website 			= $data[30];

					$insert_temp_agent->user_type	 		= (int) $data[31];

					$db->insertObject('#__temp_agent_list', $insert_temp_agent);

				}

			} while ($data = fgetcsv($handle,1000,",","'"));

		

		

			$query = $db->getQuery(true);



 			$query->select('*')->from('#__temp_agent_list')->where('is_processed = 0');

			

			$db->setQuery($query);



			$result = $db->loadObjectList();

			foreach($result as $row){

			

				$queryx = $db->getQuery(true);

				$queryx->select('*')->from('#__countries')->where("countries_iso_code_3 = '".$row->country."' LIMIT 1");

				$db->setQuery( $queryx );

				$country_id = $db->loadObject()->countries_id;

				

				$queryz = $db->getQuery(true);

				$queryz->select('*')->from('#__zones')->where("zone_code = '".$row->states."' LIMIT 1");

				$db->setQuery( $queryz );

				$zone_id = $db->loadObject()->zone_id;

				

				$querya = $db->getQuery(true);

				$querya->select('*')->from('#__broker')->where("broker_name like '%".$row->company."%' LIMIT 1");

				$db->setQuery( $querya );

				$broker_id = $db->loadObject()->broker_id;

				

				//INSERT NEW TABLES in tbl_user_registration

				$insert_new_agent = new JObject();

				$insert_new_agent->firstname 		 = $row->first_name;

				$insert_new_agent->lastname 		 = $row->last_name;

				$insert_new_agent->email 			 = $row->email;

				$insert_new_agent->city 			 = $row->city;

				$insert_new_agent->zip 				 = $row->zip_code;

				$insert_new_agent->licence 			 = $row->al_number;

				$insert_new_agent->brokerage_license = $row->bl_number;

				$insert_new_agent->street_address	 = $row->address_1;

				$insert_new_agent->suburb	 		 = $row->address_2;

				$insert_new_agent->registration_date = date('Y-m-d H:i:s',time());

				$insert_new_agent->country 			 = $country_id;

				$insert_new_agent->state 			 = $zone_id;

				$insert_new_agent->brokerage 		 = $broker_id;

				$insert_new_agent->user_type 		 = $row->user_type;			

				$db->insertObject('#__user_registration', $insert_new_agent);

				

			

				$new_user_id = $db->insertid();

			

				$insert_sales = new JObject();

				$insert_sales->agent_id = $new_user_id;

				$insert_sales->volume_2012 = $row->volume_2012;

				$insert_sales->volume_2013 = $row->volume_2013;
				
				$insert_sales->volume_2014 = $row->volume_2014;

				$insert_sales->sides_2012  = $row->sides_2012;

				$insert_sales->sides_2013  = $row->sides_2013;
				
				$insert_sales->sides_2014  = $row->sides_2014;

				$insert_sales->ave_price_2012  = $row->ave_price_2012;

				$insert_sales->ave_price_2013  = $row->ave_price_2013;
				
				$insert_sales->ave_price_2014  = $row->ave_price_2014;

				$insert_sales->verified_2012   = $row->verified_2012;

				$insert_sales->verified_2013  = $row->verified_2013;
				
				$insert_sales->verified_2014  = $row->verified_2014;

				$db->insertObject('#__user_sales', $insert_sales);

								

				$insert_phone = new JObject();

				$insert_phone->user_id = $new_user_id;

				$insert_phone->value = $row->mobile;

				$insert_phone->main = 1;

				$insert_phone->show = 1;

				$db->insertObject('#__user_mobile_numbers', $insert_phone);

				

				$insert_work = new JObject();

				$insert_work->user_id = $new_user_id;

				$insert_work->value = $row->work;

				$insert_work->main = 1;

				$insert_work->show = 1;

				$db->insertObject('#__user_work_numbers', $insert_work);				

				

				$insert_fax = new JObject();

				$insert_fax->user_id = $new_user_id;

				$insert_fax->value = $row->fax;

				$insert_fax->main = 1;

				$insert_fax->show = 1;

				$db->insertObject('#__user_fax_numbers', $insert_fax);

				

				$is_processed = new JObject();

				$is_processed->is_processed = 1;

				$is_processed->agent_bridge_num =  $row->agent_bridge_num;;

				JFactory::getDbo()->updateObject('#__temp_agent_list', $is_processed, 'agent_bridge_num');

			}

			$application->redirect(JRoute::_("index.php?option=com_useractivation"));

			

		}	

	}

	 

	 

	function save() {

	

		

		$model = $this->getModel('UserActivation');

		

		$application = JFactory::getApplication();



		foreach($_POST['jform'] as $key=>$value){



				if(is_array($value)){



					foreach($value as $k=>$v){



						$data[$key][$k] = trim(addslashes($v));



					}



				}



				else{



					$data[$key] = trim(addslashes($value));



				}



			}



		extract($data);

		

		$uid = $data['uid'];

		

		$orig_email = $model->getOrigEmail($uid);

		

		$model->updateUserName($orig_email, $data);

		

  		$model->updateUserRegEmail($data);

		

		$model->updateUserEmail($data);

		

		$model->updateUserInfo($data);

		

		$application->enqueueMessage('You have successfully edited '.$data['firstname'].'\'s profile', 'Success');

		

		$application->redirect(JRoute::_("index.php?option=com_useractivation"));

		

		

	

	}

	

	function edit() {	

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		
		$model = $this->getModel('UserActivation');

		$uid = $_POST['cid'][0];

		$query->select('*, ur.email AS notact_email');

		$query->from('#__user_registration ur');

		$query->where('ur.user_id = '.$uid);
		
		$query->leftJoin('#__users u ON u.email = ur.email');

		$db->setQuery($query);

		$results = $db->loadObjectList();

		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );
		
		$view->assignRef( 'designationAuto', $model->get_designations($results[0]->country));

		if ($results[0]->activation_status==1) {
			$view->assignRef('designations', $model->get_user_designations($results[0]->id));
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__users');
		$query->where('email = \''.$results[0]->email.'\'');
		$db->setQuery($query);
		$users_id = $db->loadObjectList();

		$userid = $users_id[0]->id;

		$view->assignRef('users_id', $userid);

		$view->display($this->getTask());

		



	}

	 

    function display()



    {

    	
		$model = $this->getModel('UserActivation');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

				'ur.user_id,

				 ur.firstname, 

				 ur.lastname, 

				 ur.licence, 

				 ur.email, 

				 ur.brokerage_license, 

				 ur.is_premium, 

				 ur.status,

				 ur.brokerage,

				 ur.activation_status,
				 
				 ur.is_term_accepted,
				 
				 ur.city,
				 
				 ur.state,

				 ur.user_type'

			)

		);



		$query->from('#__user_registration ur');

		$query->leftJoin('#__user_sales u ON u.agent_id = ur.user_id');
				
		$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();

		//var_dump($results);



		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );	

		$application = JFactory::getApplication();

		$task = JRequest::getVar('task');

		

		switch ($task):



			case 'add':



				break;



			case 'edit':

			

				$application->redirect(JRoute::_("index.php?option=com_useractivation").'&uid='.$_POST['cid'][0]);



				break;



			case 'remove':



				$query = $db->getQuery(true);



 				$query->select('ur.user_id as reg_id, u.id, u.email')->from('#__user_registration ur')



 					->leftJoin('#__users u on u.email = ur.email')



 					->where('ur.user_id in ("'.implode('", "', $_POST['cid']).'")');



 	

				$user_info = $db->setQuery($query)->loadObjectList();



				$todelete['email'] = array_map(function($object){ return $object->email; }, $user_info);



				$todelete['id'] = array_map(function($object){ return $object->id; }, $user_info);



				$todelete['reg_id'] = array_map(function($object){ return $object->reg_id; }, $user_info);





				//delete from user_registration



				$query = $db->getQuery(true);



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query->delete($db->quoteName('#__user_registration'));



				$query->where($conditions);				 



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {





				}





				//delete from users



				$query = $db->getQuery(true);



				$conditions = 'id IN ("'.implode('","',$todelete['id']).'")';



				$query->delete($db->quoteName('#__users'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from activities



				$query = $db->getQuery(true);



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'") OR other_user_id IN ("'.implode('","',$todelete['id']).'")';



				$query->delete($db->quoteName('#__activities'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}





				//delete from answers



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__answers'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from permission_setting



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__permission_setting'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from pocket listing



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__pocket_listing'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from request network



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'") OR other_user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__request_network'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user designation



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_designations'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user fax numbers



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_fax_numbers'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user lang spoken



				$conditions = 'user_id IN ("'.implode('","',$todelete['id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_lang_spoken'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user mobile numbers



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_mobile_numbers'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				



				//delete from user social sites



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_social_sites'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user websites



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_websites'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				//echo $query;



				



				//delete from user work numbers



				$conditions = 'user_id IN ("'.implode('","',$todelete['reg_id']).'")';



				$query = $db->getQuery(true);



				$query->delete($db->quoteName('#__user_work_numbers'));



				$query->where($conditions);



				$db->setQuery($query);



				try {



				   $result = $db->query(); 



				} catch (Exception $e) {



				   // catch the error.



				}



				break;



			case 'activate':	


				//collect all the agents that is premium



				$is_premium .="0 ";



				foreach($_POST['premium'] as $key => $value) {



					$is_premium .= ", ".$key; 



				}



					$query4 = $db->getQuery(true);		 



					$fields = array(



						'status=\'1\'');			 



					$conditions = array(



						'user_id IN ("'.implode('", "', $_POST['cid']).'")');			 



					$query4->update($db->quoteName('#__user_registration'))->set($fields)->where($conditions);		



					$db->setQuery($query4);



					$result = $db->query(); 



					//update all premiums



					$query5 = $db->getQuery(true);		 



					$fields = array(



						'is_premium=\'1\'');			 



					$conditions = array(



						"user_id IN (".$is_premium.")");			 



					$query5->update($db->quoteName('#__user_registration'))->set($fields)->where($conditions);		



					$db->setQuery($query5);					



					try {



						$result = $db->query(); 



						foreach($_POST['cid'] as $ids):



							$query = $db->getQuery(true);			



							$query->select(

								array(

									'ur.user_id', 

									'ur.firstname', 

									'ur.lastname', 

									'ur.email',

									'c.countries_iso_code_2',  

									'ur.is_premium',

									'ur.user_type',

									'it.invited_email',

									'it.agent_id'

								)

							);



							$query->from('

									#__user_registration ur 

								LEFT JOIN 

									#__invitation_tracker it								

								ON

									it.invited_email = ur.email

								LEFT JOIN 

									#__countries c 

								ON

									c.countries_id = ur.country

							');



							$query->where('ur.user_id LIKE \''.$ids.'\'');



							$db->setQuery($query);



							$results = $db->loadObjectList();

							

							$body = "";



							//copy to is_premium variable



							$is_premium = $results[0]->is_premium;

							$user_type = $results[0]->user_type;

							$profile_id = $results[0]->user_id;

							$invited_email = $results[0]->invited_email;

							$inviter_email = $results2[0]->email;

							$vid=1;

						

							$user 	 = JFactory::getUser($inviter_email);

						  $language = JFactory::getLanguage();
		                  $extension = 'com_nrds';
		                  $base_dir = JPATH_SITE;
		                  $language_tag = "english-US";
		                  $language->load($extension, $base_dir, $language_tag, true);						



							if ($user_type==3){


								$subject = JText::_('COM_USERACTIV_SUB_UT3');

								$body .= "<p style='padding-top:20px'>".JText::_('COM_USERACTIV_HEADER')." ".stripslashes($results[0]->firstname).",<br/><br/></p>";

								$body .= JText::_('COM_USERACTIV_BODYA_UT3');

								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."'>". JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."</a>.<br /><br/>";

								$body .= JText::_('COM_USERACTIV_BODYB_UT3');

								$body .= JText::_('COM_USERACTIV_CLOSING');

								$sender = array(  'no-reply@agentbridge.com' , 'AgentBridge');



							} else if ($user_type==1){

						

								$subject = JText::_('COM_USERACTIV_SUB_UT1');

								$body .= JText::_('COM_USERACTIV_HEADER'). " ".stripslashes($results[0]->firstname).",<br/><br/>";

								$body .= JText::_('COM_USERACTIV_BODYA_UT1');

								$body .= JText::_('COM_USERACTIV_BODYB_UT1');
								
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."'>". JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."</a>.<br /><br/>";

								$body .= JText::_('COM_USERACTIV_BODYC_UT1');		

								$body .= JText::_('COM_USERACTIV_CLOSING');
								
								$body .= JText::_('COM_USERACTIV_BODYD_UT1');

								$body .= JText::_('COM_USERACTIV_BODYE_UT1');

								$sender = array(  'redler@vistasir.com' , 'Rick Edler');

								

							} else if  ($user_type==4) {

															

								$sponsor = $results[0]->agent_id;

								$query2 = $db->getQuery(true);

						

								$query2->select(

									array( 

										'firstname', 

										'lastname'

									)

								);



								$query2->from('

										#__user_registration

								');

								$query2->where("user_id = $sponsor");

								$db->setQuery($query2);

								$results3 = $db->loadObjectList();	

								

								$subject = JText::_('COM_USERACTIV_SUB_UT4'). " ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname);

								$body .= JText::_('COM_USERACTIV_BODYA_UT4'). " ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname)."<br/><br/>";


								$body .= JText::_('COM_USERACTIV_HEADER'). " ".stripslashes($results[0]->firstname).",<br/><br/>";

								$body .= JText::_('COM_USERACTIV_BODYB_UT4');

								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."'>". JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."</a>.<br /><br/>";

								$body .= JText::_('COM_USERACTIV_BODYC_UT1');

								$body .= stripslashes($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." <a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&utype=".$user_type."&jgadeprasdium=".$is_premium."'>".JText::_('COM_USERACTIV_BODYD_UT4')."</a><br/><br/>" ;

								$body .= JText::_('COM_USERACTIV_BODYD_UT1');
								
								$body .= JText::_('COM_USERACTIV_BODYE_UT1');

								$sender = array(  'no-reply@agentbridge.com' , 'AgentBridge');

								

							}	else if  ($user_type==5) {

															

								$sponsor = $results[0]->agent_id;

								$query2 = $db->getQuery(true);

						

								$query2->select(

									array( 

										'firstname', 

										'lastname'

									)

								);



								$query2->from('

										#__user_registration

								');

								$query2->where("user_id = $sponsor");

								$db->setQuery($query2);

								$results3 = $db->loadObjectList();	

								

								$subject = JText::_('COM_USERACTIV_SUB_UT5'). " ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname);

								$body .= JText::_('COM_USERACTIV_SUB_UT5'). " ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname)."<br/><br/>";

								$body .= JText::_('COM_USERACTIV_HEADER'). " ".stripslashes($results[0]->firstname).",<br/><br/>";

								$body .= JText::_('COM_USERACTIV_BODYA_UT5');

								$body .= JText::_('COM_USERACTIV_CONFIRM')." <br /><br />";

								$body .= "<a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."'>". JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."</a>.<br /><br/>";

								$body .= JText::_('COM_USERACTIV_BODYB_UT5');

								$body .= JText::_('COM_USERACTIV_BODYC_UT1');

								$body .= stripslashes($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." ".JText::_('COM_USERACTIV_BODYD_UT4')."<br/><br/>" ;

								$body .= "<table width='80%'><tr><td style='align:center'><strong>".JText::_('COM_USERACTIV_BODYC_UT5').":</strong></td></tr> <br/>
										  <tr><td style='align:center'><a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&jgadeprasdium=".$is_premium."&vid=".$vid."'><img src='".JUri::root()."/images/video-icon.jpg' style='border:none'/></a></td></tr></table><br/>";

								$sender = array(  'no-reply@agentbridge.com' , 'AgentBridge');

								

							}	else if  ($user_type==2) {

															

								$sponsor = $results[0]->agent_id;

								$query2 = $db->getQuery(true);

						

								$query2->select(

									array( 

										'firstname', 

										'lastname'

									)

								);



								$query2->from('

										#__user_registration

								');

								$query2->where("user_id = $sponsor");

								$db->setQuery($query2);

								$results3 = $db->loadObjectList();	

								

								$subject = JText::_('COM_USERACTIV_SUB_UT4')." ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname);

								$body .= JText::_('COM_USERACTIV_BODYA_UT4')." ".stripslashes($results3[0]->firstname)." ".stripslashes($results3[0]->lastname)."<br/><br/>";

								$body .= JText::_('COM_USERACTIV_HEADER')." ".stripslashes($results[0]->firstname).",<br/><br/>";
								
								$body .= JText::_('COM_USERACTIV_BODYA_UT2');

								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."'>". JUri::root()."index.php?option=com_setpass&action=activated&useregid=".$ids."&jgadeprasdium=".$is_premium."</a>.<br /><br/>";

								$body .= JText::_('COM_USERACTIV_BODYC_UT1');

								$body .= stripslashes($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." <a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&utype=".$user_type."&jgadeprasdium=".$is_premium."'>".JText::_('COM_USERACTIV_BODYD_UT4')."</a><br/><br/>" ;

								$body .= JText::_('COM_USERACTIV_BODYD_UT1');
													
								$body .= JText::_('COM_USERACTIV_BODYE_UT1');

								$sender = array(  'no-reply@agentbridge.com' , 'AgentBridge');

								

							}		

							

							$email = $results[0]->email;

							$bcc = ('support@agentbridge.com');

							$bcc2 = ('rebekah.roque@keydiscoveryinc.com');



							$mailSender =& JFactory::getMailer();

							$mailSender ->addRecipient( $email );

							$mailSender ->addBCC( $bcc );

							$mailSender ->addBCC( $bcc2 );

							$mailSender ->setSender( $sender );

							$mailSender ->setSubject( $subject );

							$mailSender ->isHTML(  true );

							$mailSender ->setBody(  $body );
							
							$mailSender ->Encoding = 'base64';
							
							$mailSender ->wordWrap = 50;



							if ($mailSender ->Send()) {



								JFactory::getApplication()->enqueueMessage('An Email has been set to ' . $results[0]->email . "...");

							} else {



								JError::raiseWarning( 100, 'An error occured while sending an email to ' .  $results[0]->email );

							}
							
							$model->updateActivationDate($results[0]->email);



						endforeach;

					}



					catch ( Exception $e ) {



						// Catch the error.

					}



				break;


			case 'download':

				$this->generateUserCSV();
				break;


		endswitch;



        parent::display();

    }

    ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateUserCSV(){
	      
		  
	      ## Make DB connections

		  $model = $this->getModel($this->getName());
	 	
	      $rows = $model->getUsersForCSV();

	      date_default_timezone_set('America/Los_Angeles');

	      $time = time();      

	      ## If the query doesn't work..
	      if (!$rows){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      
	      $fields = (array_keys($rows[0]));
	      
	      ## Count all fields(will be the collumns
	      $columns = count($fields);
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	      	if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	} else if($fields[$i]=="is_premium"){
	      		$data .= "Charter".$sep;
	      	} else if($fields[$i]=="sendEmail_date"){
	      		$data .= "Date Email Activation sent".$sep;
	      	} else if($fields[$i]=="activation_date"){
	      		$data .= "Date User Activated link".$sep;
	      	} else if($fields[$i]=="completeReg_date"){
	      		$data .= "Date Terms Accepted".$sep;
	      	} else if($fields[$i]=="registration_date"){
	      		$data .= "Date added to Database".$sep;
	      	} else if($fields[$i]=="registerDate"){
	      		$data .= "Date of Password Setup".$sep;
	      	} else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      
	      $data .= "\n";
	      
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="is_premium"){
	         	 $value = $value == 1 ? "Yes" : "No";  
	         	} elseif($key=="zip_code"){
					$value = str_replace(array("\\","/",","), "-",  $value);
	         	} elseif($key==sendEmail_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==activation_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==completeReg_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==registration_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==registerDate){
					 if ($value!="") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 } else {
					 $value="0000-00-00 00:00:00";
					 }
				} elseif($key==is_term_accepted){
					 if ($value==1) {
					 $value = "Yes";
					 } else {
					 $value="No";
					 }
				}else
	         	 $value = str_replace(array("\\","/",","), "",  $value);

	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      
	      $data = str_replace("\r","",$data);
	      
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      
	      ## Push the report now!
	      $this->name = 'User-Registration-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }

}



?>