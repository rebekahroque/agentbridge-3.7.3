<?php

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.

JHtml::_('behavior.tooltip');

JHtml::_('behavior.multiselect');

JHtml::_('behavior.modal');

JHtml::_('formbehavior.chosen', 'select');

function get_salesV2012($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2012 = $db->loadObjectList();
		return $sales2012[0];
	}

function get_salesV2013($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2013 = $db->loadObjectList();
		return $sales2013[0];
	}
function get_salesV2014($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2014 = $db->loadObjectList();
		return $sales2014[0];
	}

function get_broker($broker_id){
	if(!empty($broker_id)) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('broker_name')->from('#__broker')->where('broker_id = '.$broker_id);
			$db->setQuery($query);
			return $db->loadObject()->broker_name;
		}
	}
	


function get_desig_name($id){
$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select(array('*'));
	$query->from('#__designations');
	$query->where('id LIKE \''.$id.'\'');
	$db->setQuery($query);
	$designame = $db->loadObjectList();
	return $designame[0]->designations;
	}
	
function decrypt($encrypted_text){
	$key = 'password to (en/de)crypt';
	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	return $decrypted;
}

?>

<link rel="stylesheet" href="templates/isis/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.Autocomplete.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/imagearea/css/imgareaselect-default.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/css/jqueryui-custom.css">
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/inputmask.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/mask.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="templates/isis/scripts/autoNumeric.js"></script>
<style>
.ui-helper-hidden-accessible{
	display: none;
}
.ui-menu a{
	font-family: 'OpenSansRegular', Arial, sans-serif;
}

.ui-menu a:hover{
	font-family: 'OpenSansRegular', Arial, sans-serif;
}

</style>
<form action="<?php echo JRoute::_('index.php?option=com_useractivation&task=save')?>" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
	
	<?php foreach($this->datas as $i=>$data)?>
		<input type="hidden" name="boxchecked" value="1" />
		<input type="hidden" name="jform[uid]" value="<?php echo $data->user_id ?>" />
	<fieldset>
		<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#details">User Profile</a></li>
	    <li><a data-toggle="tab" href="#brokerdesig">Brokerage & Designation</a></li>
		<li><a data-toggle="tab" href="#sales">Sales Numbers</a></li>
		<li><a data-toggle="tab" href="#assistant">Assistant Details</a></li>
		<li><a data-toggle="tab" href="#zipworkaround">User Work Around Zips</a></li>
		</ul>
		<div class="tab-content">
			<div id="details" class="tab-pane active">
				<table width="600">
					<tr>
						<td style="padding-bottom:15px" colspan="2">User ID: <?php echo $data->user_id ?></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px" colspan="2">Activation Status: <?php echo (($data->activation_status)==0) ? "Not Activated ": $data->activation_status ?></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>User Type</label></td>
						<td style="padding-bottom:15px">
							<input type="hidden" id="this_usertype" value="<?php echo stripslashes_all($data->user_type);?>"/>
							<?php 
								if($data->user_type==1){
									$one="selected";
								} else if ($data->user_type==2){
									$two="selected";
								} else if ($data->user_type==3){
									$three="selected";
								} else if ($data->user_type==4){
									$four="selected";
								} else if ($data->user_type==5){
									$five="selected";
								} else if ($data->user_type==86){
									$eightsix="selected";
								}
							?>
							<select name="jform[user_type]" >
								<option value=""></option>
								<option value=1 <?php echo $one ?>>1</option>
								<option value=2 <?php echo $two ?>>2</option>
								<option value=3 <?php echo $three ?>>3</option>
								<option value=4 <?php echo $four ?>>4</option>
								<option value=5 <?php echo $five ?>>5</option>
								<option value=86 <?php echo $eightsix ?>>86</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>First Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[firstname]" value="<?php echo stripslashes_all($data->firstname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Last Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[lastname]" value="<?php echo stripslashes_all($data->lastname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Email</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[email]" value="<?php echo $data->activation_status == 1 ? $data->email : $data->notact_email; ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Agent License</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[agentlicense]" value="<?php echo $this->escape(decrypt($data->licence)) ?>"/></td>
					</tr>
				</table>
			</div>
			
			<div id="brokerdesig" class="tab-pane">
			<table>
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Broker License</label></td>
				<td style="padding-bottom:15px"><input type="text"  name="jform[brokerlicense]" value="<?php echo $this->escape(decrypt($data->brokerage_license))?>"/></td>
				</tr>
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Brokerage</label></td>
				<td style="padding-bottom:15px"><?php echo stripslashes_all(get_broker($data->brokerage)) ?></td>
				</tr>
		
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Designation</label></td>
				<td style="padding-bottom:15px">
							<div id="holder" class="text-input-small" style="margin-top:20px;">
								<div class="clear">
									<div id="selecteddesigs" style="float: left">
										<?php
										
										$designations = $this->designations;
										foreach($designations as $d):
										?>
										<div id="listitem<?php echo $d->id; ?>"
											style="width:250px;margin-bottom:10px; font-size:12px"
											class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable "><?php echo $d->designations; ?><a
											href="javascript:void(0)"
											onclick="removeDesignation('listitem<?php echo $d->id; ?>')"
											class="textboxlist-bit-box-deletebutton"></a><input
											type="hidden" value="<?php echo $d->id; ?>"
											name="jform[designations][]">
										</div>
										<?php
										endforeach;
										?>
										<input type="text" id="designationInput" style="float: left"
										class="transparent-text" />
									</div>
								</div>
							</div>
					<div id="menu-container" style="position:absolute; width: 100px;"></div>
					<div id="designations" style="display:none;width:100%">
						<label class="clear-float">Brokerage</label>
						<div id="holder-nrds" class="text-input-small-nrds">
							<div class="clear">
								<ul id="selecteddesigs" style="float: left">
								</ul>
								<input type="text" id="brokerageInput" style="float: left"
									class="text-input" /> <img id="imageloading"
									style="padding-top: 4px; position: relative; left: -20px; display: none;"
									src="<?php echo str_replace('/administrator', "", $this->baseurl)."/images/ajax-loader.gif"?>" />
								<input type="hidden" id="jform_country" value="<?php echo $this->user->country?>"/>
								<input type="hidden" id="jform_state" value="<?php echo $this->user->state?>"/>
								<input type="hidden" id="jform_city" value="<?php echo $this->user->city?>"/>
							</div>
						</div>
						<a href="javascript:void(0)" class="button gradient-blue" style="padding:5px; position:absolute" id="savebroker"> Save </a>
					</div>

				</td>
				</tr>
			
			</table>
			</div>
			
			<div id="sales" class="tab-pane">
				<table>
					<tr><td colspan="2"><strong>2014</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text" id="volume_2014" name="jform[volume_2014]" value="<?php echo number_format(get_salesV2014($data->user_id)->volume_2014)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text" id="sides_2014" name="jform[sides_2014]"  value="<?php echo number_format(get_salesV2014($data->user_id)->sides_2014)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2014($data->user_id)->ave_price_2014)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2014]"  value="<?php echo get_salesV2014($data->user_id)->verified_2014 ?>"/></td>
					</tr>
					<tr><td colspan="2"><strong>2013</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text" id="volume_2013" name="jform[volume_2013]" value="<?php echo number_format(get_salesV2013($data->user_id)->volume_2013)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text" id="sides_2013" name="jform[sides_2013]"  value="<?php echo number_format(get_salesV2013($data->user_id)->sides_2013)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2013($data->user_id)->ave_price_2013)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2013]"  value="<?php echo get_salesV2013($data->user_id)->verified_2013 ?>"/></td>
					</tr>
					<tr><td colspan="2"><strong>2012</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text"  id="volume_2012"  name="jform[volume_2012]"  value="<?php echo number_format(get_salesV2012($data->user_id)->volume_2012)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text"  id="sides_2012" name="jform[sides_2012]"  value="<?php echo number_format(get_salesV2012($data->user_id)->sides_2012)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2012($data->user_id)->ave_price_2012)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2012]"  value="<?php echo get_salesV2012($data->user_id)->verified_2012 ?>"/></td>
					</tr>
				</table>
			</div>
			<div id="assistant" class="tab-pane ">
				<table width="600">
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Copy on All Communication</label></td>
						<td style="padding-bottom:15px"><input type="checkbox"  id="cc_check" name="jform[cc_all]" value="1" <?php echo $data->cc_all ? "checked" : "" ?> /> <span id='cc_val'>No</span></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>First Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[a_fname]" value="<?php echo stripslashes_all($data->a_fname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Last Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[a_lname]" value="<?php echo stripslashes_all($data->a_lname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Email</label></td>
						<td style="padding-bottom:15px"><input type="email"  name="jform[a_email]" value="<?php echo $data->a_email ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Phone number</label></td>
						<td style="padding-bottom:15px"><input placeholder="(999) 999-9999" id="assist_tel" class="numbermask" type="text"   name="jform[a_pnumber]" value="<?php echo $data->a_pnumber ?>"/></td>
					</tr>
				</table>
			</div>
			<div id="zipworkaround" class="tab-pane ">
				<table width="600">
				<?php 
					//foreach ($this->zip_workarounds as $key => $value) {
						# code...
						//var_dump($value);

						$masks_zips = $this->zip_workarounds[0]->zip_format;
						$wzip = explode(",",$this->zip_workarounds[0]->zip_workaround);
						$def_country = $this->zip_workarounds[0]->country;
						if(!$def_country){
							$def_country = $data->country;
						} 
						if(count($wzip)<=0){
							$wzip="";
						}
					 ?>


					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Country:</label></td>
						<td style="padding-bottom:15px"><select 
							id="jform_wz_country" 
                            style="margin-top:20px" 
							name="jform[wz_country]"
							>
							<?php 
								foreach($this->countries as $cArr){
									$sel_country = "";
									if($cArr->countries_id == $def_country){
										$sel_country = "selected='selected'";
									}
									echo "<option data=\"".$cArr->countries_iso_code_2."\" value=\"".$cArr->countries_id."\" ".$sel_country.">".$cArr->countries_name."</option>";
								}
							?>

							</select></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Works Around Zips:</label></td>
						<td style="padding-bottom:15px">
						<div id="zips_list" style="  font-size: 12px;text-decoration: none;color: #006699;margin-bottom: 10px; "><span style="color:black;margin-right: 5px;">Zip/Postcodes:</span></div>	
						<div><input placeholder="Enter Zip" id="jform_zip" type="text"  onkeyup="javascript:this.value=this.value.toUpperCase();"  name="jform[w_zips]" value="" style="margin:0;width:110px;"/>
						<span id="add_zips" class="button gradient-blue" style="color:#007bae;font-size: 13px;text-decoration: none;cursor:pointer;padding:5px;">+ Add Zip/Postcode</span>
						<img height="20px" id="checkzipload" style="display: none;position: absolute;width: 20px;"id="loading-image_custom_question" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/images/ajax_loader.gif" alt="Loading..." />
						</div></td>
					</tr>
				
				<?php	//}
				?>
				
					
				</table>
			</div>
		
		
		</div>
	</fieldset>
	
	<a href="<?php echo JRoute::_('index.php?option=com_useractivation');?>"><button type="submit" class="btn btn-primary validate"><?php echo JText::_('Save and Close');?></button></a>
	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_useractivation');?>" title="<?php echo JText::_('JSAVE');?>"><?php echo JText::_('JCANCEL');?></a>
	<?php echo JHtml::_('form.token');?>

	</form>
	
<script>


	<?php


		echo "var zips_array = ".json_encode($wzip).";";
		echo "var zip_count = ".count($wzip).";";
	?>


	function removeItem(id){
		jQuery("#"+id).remove();
		if(id.indexOf("broker") !== -1){
			jQuery("#brokerageInput").val('');
			jQuery("#brokerageInput").show();
		}

		//console.log(id);
	}

	function removeZip(id){
		jQuery("#jform_zip").val("");
		jQuery("#"+id).remove();

		if(id.indexOf("-")){
			id = id.replace(/-/g, ' ');
		}

		var index = zips_array.indexOf(id);

		if(index>=0){

		   zips_array.splice(index, 1);
		   zip_count--;
		}
	}

	function set_masks(){
		jQuery(".numbermask").mask("(999) 999-9999");
	}
	

		function removeDesignation(id){
		jQuery("#"+id).remove();

		var designation_id = id.split("listitem");
	
		// delete designation 
		jQuery.ajax({
			url: '<?php echo JRoute::_('index.php');?>?option=com_userprofile&task=delete_single_designation&format=raw',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'desgination_id': designation_id[1],
				'user_id': '<?php echo $this->users_id?>'
			},
		  success: function( data ) {
				
		  }
		});
		//console.log(id);
	}
	function removeDesignation(id){
		jQuery("#"+id).remove();

		var designation_id = id.split("listitem");
	
		// delete designation 
		jQuery.ajax({
			url: '<?php echo JRoute::_('index.php');?>?option=com_useractivation&task=delete_single_designation&format=raw',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'desgination_id': designation_id[1],
				'user_id': '<?php echo $this->users_id?>'
			},
		  success: function( data ) {
				
		  }
		});
		//console.log(id);
	}

	function logArrayElements(element, index, array) {
		if(element)
	  jQuery("#zips_list").append("<div id='"+element.replace(/\s/g , "-")+"' class='textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable' style='float:none;display: inline-block;'>"+element+'<a href="javascript:void(0)" onclick="removeZip(\''+element.replace(/\s/g , "-")+'\')" class="textboxlist-bit-box-deletebutton"></a></div>');		
	}


	function checkGBString(inputElem) {

		/*GB Format guide
		a9,a99,aa9,aa99,aa9a,a9 9aa,aa9a 9aa,a9a 9aa,a99 9aa,aa9 9aa,aa99 9aa
		*/

		//a9
		var regex1 = new RegExp("^[A-Z]{1}[0-9]{1}$", "i");
		//a99
		var regex2 = new RegExp("^[A-Z]{1}[0-9]{2}$", "i");
		//aa9
		var regex3 = new RegExp("^[A-Z]{2}[0-9]{1}$", "i");
		//aa99
		var regex4 = new RegExp("^[A-Z]{2}[0-9]{2}$", "i");
		//aa9a
		var regex5 = new RegExp("^[A-Z]{2}[0-9]{1}[A-Z]{1}$", "i");
		//a9 9aa
		var regex6 = new RegExp("^[A-Z]{1}[0-9]{1} [0-9]{1}[A-Z]{2}$", "i");
		//aa9a 9aa
		var regex7 = new RegExp("^[A-Z]{2}[0-9]{1}[A-Z]{1} [0-9]{1}[A-Z]{2}$", "i");
		//a9a 9aa
		var regex8 = new RegExp("^[A-Z]{1}[0-9]{1}[A-Z]{1} [0-9]{1}[A-Z]{2}$", "i");
		//a99 9aa
		var regex9 = new RegExp("^[A-Z]{1}[0-9]{2} [0-9]{1}[A-Z]{2}$", "i");
		//aa9 9aa
		var regex10 = new RegExp("^[A-Z]{2}[0-9]{1} [0-9]{1}[A-Z]{2}$", "i");
		//aa99 9aa
		var regex11 = new RegExp("^[A-Z]{2}[0-9]{2} [0-9]{1}[A-Z]{2}$", "i");

	   // var regex = new RegExp("^[A-Z]{2}[0-9]{4}-[0-9]{5}$", "i");
	    var searchText = inputElem;
	   	
	   	var invalid_gb_zip = 1;
	    
	    if ((searchText.length > 0)) {
	    	//alert(searchText);
	    	if(regex1.test(searchText)){
	    		return 0;
	    	} else if(regex2.test(searchText)){
	    		return 0;
	    	} else if(regex3.test(searchText)){
	    		return 0;
	    	} else if(regex4.test(searchText)){
	    		return 0;
	    	} else if(regex5.test(searchText)){
	    		return 0;
	    	} else if(regex6.test(searchText)){
	    		return 0;
	    	} else if(regex7.test(searchText)){
	    		return 0;
	    	} else if(regex8.test(searchText)){
	    		return 0;
	    	} else if(regex9.test(searchText)){
	    		return 0;
	    	} else if(regex10.test(searchText)){
	    		return 0;
	    	} else if(regex11.test(searchText)){
	    		return 0;
	    	} else {
	    		return 1;
	    	}
	        
	    }

	   
	}

jQuery(document).ready(function(){
	set_masks();
	//var assist_tel_v = document.getElementById("assist_tel");
	//assist_tel_v.setCustomValidity("Invalid format. Please follow the format 999-999-9999.");


	if(jQuery("#cc_check").is(":checked")){
			jQuery("#cc_val").text("Yes");
		} else {
			jQuery("#cc_val").text("No");
		}
		
	jQuery("#cc_check").live("change",function(){
		if(jQuery(this).is(":checked")){
			jQuery("#cc_val").text("Yes");
		} else {
			jQuery("#cc_val").text("No");
		}
	});

	//jQuery("select").chosen('destroy');
	//jQuery('select').val(jQuery("#this_usertype").val());
    jQuery("select").chosen();
  //  jQuery("select").trigger("liszt:updated");
	var list = <?php echo json_encode($this->designationAuto); ?>;
	console.log(list);
	jQuery("#designationInput").autocomplete({
		autoFocus: true,
	    source: list,
	    minLength: 1,
	    open: function( event, ui ) {
		    jQuery(".ui-autocomplete").css('width', jQuery("#holder").width()+"!important");
		    jQuery(".ui-autocomplete").css('left', jQuery("#holder").position().left+"!important");
	    },
	    select: function(event, ui) {
	    	jQuery.ajax({
						url: '<?php echo JRoute::_("index.php");?>?option=com_useractivation&task=update_single_designation&format=raw',
						type: 'POST',
						dataType: 'JSON',
						data: {
							'desgination_id': ui.item.value,
							'user_id': '<?php echo $this->users_id?>'
						},
					  success: function( data ) {
							
					  },
					  error: function (data){
					  	alert(JSON.stringify(data));
					  }
					});
	    	var terms = split( this.value );
	          // remove the current input
	          terms.pop();
	          // add the selected item
	          terms.push( ui.item.label );
	          // add placeholder to get the comma-and-space at the end
	          terms.push( "" );
	          this.value = terms.join( ", " );
			html="<li id=\"listitem"+ui.item.value+"\" class=\"textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable \">"+ui.item.label+"<a href=\"javascript:void(0)\" onclick=removeItem(\"listitem"+ui.item.value+"\") class=\"textboxlist-bit-box-deletebutton\"></a>";
			html+="<input type=\"hidden\" value=\""+ui.item.value+"\" name=\"jform[designations][]\"/></li>";
			jQuery("#holder").prepend();
	        jQuery("#selecteddesigs").append(html);
	        jQuery("#designationInput").val('');
	        return false;
	    }
	// Format the list menu output of the autocomplete
	});/*.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	    return $( "<li></li>" )
	        .data( "item.autocomplete", item )
	        .append( "<a>" + item.itemCode + " - " + item.itemDesc + "</a>" )
	        .appendTo( ul );
	};*/
	jQuery.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + jQuery.ui.autocomplete.escapeRegex(term), "i");
        return jQuery.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}


	zips_array.forEach(logArrayElements);




	jQuery("#holder").css('width', jQuery("#jform_about").width()+5);

	jQuery("#volume_2014").keyup(function(){
		jQuery("#volume_2014").autoNumeric('init', {mDec: '0'});
		});
	jQuery("#volume_2013").keyup(function(){
		jQuery("#volume_2013").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#volume_2012").keyup(function(){
		jQuery("#volume_2012").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#sides_2014").keyup(function(){
		jQuery("#sides_2014").autoNumeric('init', {mDec: '0'});
		});
	
	jQuery("#sides_2013").keyup(function(){
		jQuery("#sides_2013").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#sides_2012").keyup(function(){
		jQuery("#sides_2012").autoNumeric('init', {mDec: '0'});
		});

	var invalid;
	var clicked=0;


	jQuery("#jform_wz_country").live("change",function(){

			jQuery("#jform_zip").val("");
			jQuery('#jform_zip').inputmask("remove");

			var this_id = jQuery(this).val();
			zips_array.length = 0;
			jQuery.ajax({
				url: "<?php echo str_replace('/administrator', "", $this->baseurl); ?>/index.php?option=com_propertylisting&task=changeCountryData&format=raw",
				type: "POST",
				data: {country:this_id},
				success: function (data){
					//jQuery("#pocketform").prepend(data);

					var datas = JSON.parse(data);

					jQuery("#jform_zip").inputmask({mask:datas.zip_format.split(','),placeholder:"",});

					jQuery("#jform_zip").blur(function(){

					  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

					});
					
					jQuery("#jform_zip").attr("maxlength",datas.zipMaxLength);
					jQuery("#jform_zip").attr("placeholder",datas.zipLabel);
					jQuery("#zips_list").html('<span style="color:black;margin-right: 5px;">Zip/Postcodes:</span>');
					
				}

			});
	});

	jQuery("#jform_zip").inputmask({
		mask:'<?php echo $masks_zips ?>'.split(','),placeholder:"",
		onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toUpperCase();
            return pastedValue;
        },
        definitions: {
            '*': {
                casing: "upper"
            }
        }
    });

	jQuery("#add_zips").live("click",function(){
		
			invalid=0;

				var zip = jQuery.trim(jQuery("#jform_zip").val());
				
				if(jQuery("#jform_wz_country").find(':selected').attr('data')=="IE"){

					jQuery.ajax({
			                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
			                async:false,
			                success: function(data){
			                	if(data[0]){

								} else {
									invalid=1;
									jQuery("#jform_zip").val("");
									
								}
			                }
			        }); 


				} else if(jQuery("#jform_wz_country").find(':selected').attr('data')!="GB") {
					jQuery.ajax({
			                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+jQuery("#jform_wz_country").find(':selected').attr('data')+'&username=damianwant33',
			                async:false,
			                success: function(data){
			                	console.log(data.postalcodes.length);
			                	if(data.postalcodes[0]){		                		

								} else {
									invalid=1;
									jQuery("#jform_zip").val("");
									
								}
			                }
			        }); 
				} else if(jQuery("#jform_wz_country").find(':selected').attr('data')=="GB") {
					invalid = checkGBString(jQuery("#jform_zip").val());
					
				}


				if(!invalid ){
					if(jQuery("#jform_zip").val()){			
						if(jQuery.inArray( jQuery("#jform_zip").val(), zips_array ) == -1){
							zips_array.push(jQuery("#jform_zip").val());

							if(clicked){
								//jQuery("#zips_list").append(",");
							}

							jQuery("#zips_list").append("<div id='"+jQuery("#jform_zip").val().replace(/\s/g , "-")+"' class='textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable' style='float:none;display: inline-block;'>"+jQuery("#jform_zip").val()+'<a href="javascript:void(0)" onclick="removeZip(\''+jQuery("#jform_zip").val().replace(/\s/g , "-")+'\')" class="textboxlist-bit-box-deletebutton"></a></div>');

							clicked=1;
							zip_count++;
						}
						
					}

					jQuery("#jform_zip").val("");
				} else {
					alert("Invalid Zip/Postcode/Eircode Entered.");
				}
			
			
		});

	jQuery(".btn").live("click",function(){

		jQuery("#jform_zip").inputmask("remove");
	 	var zips = zips_array.join(",");
	 	jQuery("#jform_zip").val(zips);
	});

		
	});

</script>


