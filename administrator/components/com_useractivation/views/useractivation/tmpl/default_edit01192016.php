<?php

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

// Load the tooltip behavior.

JHtml::_('behavior.tooltip');

JHtml::_('behavior.multiselect');

JHtml::_('behavior.modal');

//JHtml::_('formbehavior.chosen', 'select');

function get_salesV2012($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2012 = $db->loadObjectList();
		return $sales2012[0];
	}

function get_salesV2013($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2013 = $db->loadObjectList();
		return $sales2013[0];
	}
function get_salesV2014($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2014 = $db->loadObjectList();
		return $sales2014[0];
	}

function get_salesV2015($user_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('*'));
		$query->from('#__user_sales');
		$query->where('agent_id LIKE \''.$user_id.'\'');
		$db->setQuery($query);
		$sales2015 = $db->loadObjectList();
		return $sales2015[0];
	}

function get_broker($broker_id){
	if(!empty($broker_id)) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('broker_name')->from('#__broker')->where('broker_id = '.$broker_id);
			$db->setQuery($query);
			return $db->loadObject()->broker_name;
		}
	}
	


function get_desig_name($id){
$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select(array('*'));
	$query->from('#__designations');
	$query->where('id LIKE \''.$id.'\'');
	$db->setQuery($query);
	$designame = $db->loadObjectList();
	return $designame[0]->designations;
	}
	
function decrypt($encrypted_text){
	$key = 'password to (en/de)crypt';
	$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted_text), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	return $decrypted;
}

?>

<link rel="stylesheet" href="templates/isis/css/template.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/css/fonts.css"/>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/TextboxList/Source/TextboxList.Autocomplete.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/imagearea/css/imgareaselect-default.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/css/jqueryui-custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/system/css/admin_skin.css">
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/inputmask.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/mask.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/plugins/imagearea/scripts/jquery.imgareaselect.js"></script>
<script type="text/javascript" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/templates/agentbridge/scripts/custom/custom2.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="templates/isis/scripts/autoNumeric.js"></script> 
<style>
.ui-helper-hidden-accessible{
	display: none;
}
.ui-menu a{
	font-family: 'OpenSansRegular', Arial, sans-serif;
}

.ui-menu a:hover{
	font-family: 'OpenSansRegular', Arial, sans-serif;
}

</style>

<script>
	var tablecols = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','property_type','sub_type','unit_sqft','year_built','pool_spa','cap_rate','listing_class','parking_ratio','ceiling_height','room_count','type_lease','type_lease2','available_sqft','lot_sqft','lot_size','bldg_sqft','bldg_type','description'];
	var formnames = ['bedroom','bathroom','garage','view','style','condition','grm','occupancy','type','stories','term','furnished','pet','possession','zoned','features1','features2','features3','setting','ptype','stype','unitsqft','yearbuilt','poolspa','cap','class','parking','ceili','roomcount','typelease','typelease2','available','lotsqft','lotsize','bldgsqft','bldgtype','desc'];
	var array = <?php echo json_encode($this->data)?>;
	
	var $nocon = jQuery.noConflict();
	
	var ptype = 0;
	var stype = 0;
	
	function bindUpload(id){

		jQuery('input[type=file]').trigger('click');

		//});

	}
	
	function redirect(){
		window.location = "<?php echo JRoute::_('index.php?option=com_propertylisting'); ?>";
	}
	function jsonpCallback(data){
		
		jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
				
		jQuery("#jform_city").val(data.postalcodes[0].placeName);
		jQuery("#s2id_state").remove();
		setTimeout(function() { 
				try{
					jQuery("#jform_state").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());					
				}catch(e){}
		}, 1000);
	}
	var hidden_fields;
	function set_selects(v){
		var ptype_s = jQuery("#jform_ptype").val();
		var stype_s = jQuery("#jform_stype").val();
		var stype_v=v;
		switch(ptype_s){
			case "1":
				if(stype_s==1){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_ceiling'];
				} else if(stype_s==2){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_parking','jform_view','jform_bldgtype'];
				} else if(stype_s==3){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_yearbuilt','jform_parking','jform_view', 'jform_bldgtype'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "2":
				if(stype_s==5){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_view'];
				} else if(stype_s==6){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype'];
				} else if(stype_s==7){
					hidden_fields = ['jform_features1','jform_features2','jform_poolspa','jform_garage','jform_style','jform_view', 'jform_possession', 'jform_bldgtype','jform_yearbuilt'];
				} else {
					hidden_fields = ['jform_features1','jform_features2'];
				}
				break;
			case "3":
				if(stype_s==9){
					hidden_fields = ['jform_features1','jform_features2','jform_style','jform_yearbuilt','jform_occupancy','jform_view'];
				} else if(stype_s==10){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking'];
				} else if(stype_s==11){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_ceiling','jform_stories'];
				} else if(stype_s==12){
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_occupancy','jform_parking','jform_stories'];
				} else if(stype_s==13){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_stories'];
				} else if(stype_s==14){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_yearbuilt','jform_stories'];
				} else {
					hidden_fields = [];
				}
				break;
			case "4":
				if(stype_s==16){
					hidden_fields = ['jform_bldgsqft','jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking'];
				} else if(stype_s==17){
					hidden_fields = ['jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_ceiling','jform_stories'];
				} else {
					hidden_fields = ['jform_lotsqft','jform_features1','jform_features2','jform_yearbuilt','jform_parking','jform_stories'];
				}
				break;
				
			default:
				hidden_fields = ['jform_features1','jform_features2','jform_features3','jform_poolspa','jform_condition','jform_garage','jform_style','jform_yearbuilt','jform_possesion','jform_ceiling','jform_stories','jform_occupancy','jform_parking', 'jform_bldgtype','jform_view'];
		}
		load_form(stype_v);
	}

	function progressHandlingFunction(e){
	    if(e.lengthComputable){
	        //jQuery('progress').attr({value:e.loaded,max:e.total});
	        jQuery("#imageloading").show();
	    }
	}
	
	function setButtons(){
		jQuery(".pet").click(function(){
				jQuery(".pet").removeClass("yes");
				jQuery(".pet").removeClass("gradient-blue-toggle");
				jQuery(".pet").removeClass("gradient-gray");
				jQuery(".pet").addClass("no");
				jQuery(".pet").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_pet").val(jQuery(this).attr("rel"));
			})
			jQuery(".furnished").click(function(){
				jQuery(".furnished").removeClass("yes");
				jQuery(".furnished").removeClass("gradient-blue-toggle");
				jQuery(".furnished").removeClass("gradient-gray");
				jQuery(".furnished").addClass("no");
				jQuery(".furnished").addClass("gradient-gray");
				jQuery(this).removeClass("gradient-gray");
				jQuery(this).removeClass("no");
				jQuery(this).addClass("yes");
				jQuery(this).addClass("gradient-blue-toggle");
				jQuery("#jform_furnished").val(jQuery(this).attr("rel"));
			})
	}
		function select_price_type(e){			
		
			jQuery("#jform_price2, #jform_price1").removeClass("glow-required");
			jQuery(".jform_price1, .jform_price2").hide();
			
			jQuery(".qtip").remove();
			
			if(e==2) {
				document.getElementById('jform_price2').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1").show();	
					
			} else if(e==1) {				
				document.getElementById('jform_price1').disabled=false;				
				jQuery("#jform_price1, .hl-label").show();							
				document.getElementById('jform_price2').disabled=false;				
				jQuery("#jform_price2, .hl-label").show();		
			} else {
				document.getElementById('jform_price2').disabled=true;				
				document.getElementById('jform_price1').disabled=true;				
				jQuery("#jform_price2").hide();				
				jQuery("#jform_price2").removeAttr('required');				
				jQuery("#jform_price1").hide();				
				jQuery("#jform_price1").removeAttr('required');				
				jQuery("#jform_price2, .hl-label").hide();			
			}		
		}	
	
	function get_sub(f){
		jQuery("#loading-image_custom_question").show();
		ptype=f;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_useractivation'); ?>&task=subtype',
			type: 'get',
			data: { 'ptype': f },
			success: function(msg){
				jQuery("#subtype").show();
				jQuery("#loading-image_custom_question").hide();
				var html="<option value=\"\">--- Select ---</option>";
				var x = $nocon.parseJSON(msg);
				for(var i=0; i<x.length; i++){
					html+="<option value=\""+x[i].sub_id+"\">"+x[i].name+"</option>";
				}
				jQuery("#jform_stype").html(html).promise().done(function(){
					jQuery("#jform_stype").val(<?php echo $this->sub_type; ?>);
					});
			}
		});
	}
	function load_form(v){
		jQuery("#loading-image_custom_question2").show();
		stype=v;
		$nocon.ajax({
			url: '<?php echo JRoute::_('index.php?option=com_useractivation'); ?>&task=form&country='+jQuery("#jform_country2").val(),
			type: 'POST',
			data: { 'ptype': ptype, 'stype': stype },
			success: function(msg){
				jQuery("#loading-image_custom_question2").hide();
				var temp = $nocon.trim(msg.replace('\n',''));
				temp = temp.replace('<div class="pocket-form">', '');
				temp = jQuery('<div/>').html(temp);
				if($nocon.trim(temp)!="&lt;/div&gt;"){
					jQuery("#formdiv").html(msg);
					jQuery("#formdiv").show();
					jQuery("#prop_features").show();
					if (jQuery(window).width() <= 600) {
								jQuery("#prop_features").removeAttr('onmouseover');	
					}
					jQuery("#formdiv").css('border-bottom', '1px solid #dedede');
					jQuery("section.main-content").css('min-height', '700px');
					jQuery("section.main-content").height(jQuery(document).height()-jQuery("section.header").height()-jQuery("section.footer").height());
					setButtons();
					if(array!=null)
						jQuery(formnames).each(function(index){
							jQuery("select[name='jform["+formnames[index]+"]']").val(array[tablecols[index]]);
						})
						jQuery(jQuery("#formdiv").children()[0]).append('<a id="showhidefield" href="javascript:void(0)" onclick="toggleHiddenFields()">More Fields</a>');
					
					jQuery('#jform_features1').change(function(){
						if(jQuery('#jform_features1').val()){
							jQuery('#jform_features2 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
							jQuery('#jform_features3 option[value=\''+jQuery('#jform_features1').val()+'\']').remove();
						}
					});
					jQuery('#jform_features2').change(function(){
						if(jQuery('#jform_features2').val()){
							jQuery('#jform_features1 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
							jQuery('#jform_features3 option[value=\''+jQuery('#jform_features2').val()+'\']').remove();
						}
					});
					jQuery('#jform_features3').change(function(){
						if(jQuery('#jform_features3').val()){
							jQuery('#jform_features1 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
							jQuery('#jform_features2 option[value=\''+jQuery('#jform_features3').val()+'\']').remove();
						}
					});
					jQuery('select').promise().done(function(){jQuery(hidden_fields).each(function(index){ jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');})});
				}
			}
		});
	}

	var $ajax = jQuery.noConflict();

	function get_state(cID){
		$ajax("#jform_state2").html('');
		$ajax.ajax({
			url: '<?php echo str_replace("/administrator", "", $this->baseurl); ?>/custom/_get_state.php',
			type: 'POST',
			data: { 'cID': cID },
			success: function(e){
				$ajax("#jform_state2").html(e);
			}
		});


	}

	function toggleHiddenFields(){
		if(jQuery('#showhidefield').html()=='More Fields')
			jQuery('#showhidefield').html('Less Fields')
		else
			jQuery('#showhidefield').html('More Fields')
			
			
			console.log(jQuery('#showhidefield').html()=='More Fields'+"|"+jQuery('#showhidefield').html())
		jQuery(hidden_fields).each(function(index){
			jQuery("#"+hidden_fields[index]).parent().toggleClass('hidden');
		});
		
	}
	function showPrivate(){
		jQuery("#loremipsum").dialog({
			modal: true,
			title: "Set to Private",
			});
	}
	function showSettings(){
		jQuery("#settingsform").dialog({
				modal: true,
				width: 'auto',
				minHeight: '300px',
				title: "Change Custom Settings",
				open: function(){
					jQuery('input[type=\'radio\']').click(function(){
						var selectedVal = jQuery('input[name=\'jform[setting]\']:checked').val();
						console.log(selectedVal);
					});
				}
		});
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_useractivation&task=save')?>" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
	
	<?php foreach($this->datas as $i=>$data)?>
		<input type="hidden" name="boxchecked" value="1" />
		<input type="hidden" name="jform[uid]" value="<?php echo $data->user_id ?>" />
	<fieldset>
		<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#details" class="showbuttons">User Profile</a></li>		
		<li><a data-toggle="tab" href="#profileimage" class="showbuttons">Profile Image</a></li>
	    <li><a data-toggle="tab" href="#brokerdesig" class="showbuttons">Brokerage & Designation</a></li>
		<li><a data-toggle="tab" href="#sales" class="showbuttons">Sales Numbers</a></li>
		<li><a data-toggle="tab" href="#assistant" class="showbuttons">Assistant Details</a></li>
		<li><a data-toggle="tab" href="#zipworkaround" class="showbuttons">User Work Around Zips</a></li>
		<li><a id="openaddpops" href="#" style="cursor:pointer;border:1px solid #ccc;background:#3071a9;color:#fff;">Add POPs&#8482;</a></li>
		</ul>
		<div class="tab-content">
			<div id="details" class="tab-pane active">
				<table width="600">
					<tr>
						<td style="padding-bottom:15px" colspan="2">User ID: <?php echo $data->user_id ?></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px" colspan="2">Activation Status: <?php echo (($data->activation_status)==0) ? "Not Activated ": $data->activation_status ?></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>User Type</label></td>
						<td style="padding-bottom:15px">
							<input type="hidden" id="this_usertype" value="<?php echo stripslashes_all($data->user_type);?>"/>
							<?php 
								if($data->user_type==1){
									$one="selected";
								} else if ($data->user_type==2){
									$two="selected";
								} else if ($data->user_type==3){
									$three="selected";
								} else if ($data->user_type==4){
									$four="selected";
								} else if ($data->user_type==5){
									$five="selected";
								} else if ($data->user_type==86){
									$eightsix="selected";
								}
							?>
							<select name="jform[user_type]" >
								<option value=""></option>
								<option value=1 <?php echo $one ?>>1</option>
								<option value=2 <?php echo $two ?>>2</option>
								<option value=3 <?php echo $three ?>>3</option>
								<option value=4 <?php echo $four ?>>4</option>
								<option value=5 <?php echo $five ?>>5</option>
								<option value=86 <?php echo $eightsix ?>>86</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>First Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[firstname]" value="<?php echo stripslashes_all($data->firstname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Last Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[lastname]" value="<?php echo stripslashes_all($data->lastname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Email</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[email]" value="<?php echo $data->activation_status == 1 ? $data->email : $data->notact_email; ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Agent License</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[agentlicense]" value="<?php echo $this->escape(decrypt($data->licence)) ?>"/></td>
					</tr>
				</table>
			</div>						
			
			<div id="profileimage" class="tab-pane">			
				<table>
					<tr>
						<td>
							<?php if ($data->image!=="") { ?>
								<img id="selectedimage" src="<?php echo $data->image?>" width="230px"/>
							<?php } else { ?>
								<img id="selectedimage" src="../templates/agentbridge/images/temp/blank-image.jpg" width="230px"/>
							<?php }?>
						</td>
					</tr>
					<tr>
						<td>
							<input type="hidden" name="jform[imagePath]" value="<?php echo $data->image; ?>" id="imagehiddeninput"/>
							<span style="font-size:11px;">(File must be less than 8MB)</span>
							<div style="clear: both;">
								<a id="newimage" class="btn btn-primary" style="cursor: pointer;">Upload Profile Image</a>
								<img id="imageloading" style="position: relative; margin-left: 5px; margin-top:-5px; display: none" src="../images/ajax-loader.gif" />	
							</div>
							<input type="file" name="newimagefile" id="inputfile" style="opacity: 0" />
						</td>
					</tr>
				</table>
			</div>
			
			<div id="brokerdesig" class="tab-pane">
			<table>
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Broker License</label></td>
				<td style="padding-bottom:15px"><input type="text"  name="jform[brokerlicense]" value="<?php echo $this->escape(decrypt($data->brokerage_license))?>"/></td>
				</tr>
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Brokerage</label></td>
				<td style="padding-bottom:15px"><?php echo stripslashes_all(get_broker($data->brokerage)) ?></td>
				</tr>
		
				<tr>
				<td style="padding-bottom:15px; width:150px"><label>Designation</label></td>
				<td style="padding-bottom:15px">
							<div id="holder" class="text-input-small" style="margin-top:20px;">
								<div class="clear">
									<div id="selecteddesigs" style="float: left">
										<?php
										
										$designations = $this->designations;
										foreach($designations as $d):
										?>
										<div id="listitem<?php echo $d->id; ?>"
											style="width:250px;margin-bottom:10px; font-size:12px"
											class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable "><?php echo $d->designations; ?><a
											href="javascript:void(0)"
											onclick="removeDesignation('listitem<?php echo $d->id; ?>')"
											class="textboxlist-bit-box-deletebutton"></a><input
											type="hidden" value="<?php echo $d->id; ?>"
											name="jform[designations][]">
										</div>
										<?php
										endforeach;
										?>
										<input type="text" id="designationInput" style="float: left"
										class="transparent-text" />
									</div>
								</div>
							</div>
					<div id="menu-container" style="position:absolute; width: 100px;"></div>
					<div id="designations" style="display:none;width:100%">
						<label class="clear-float">Brokerage</label>
						<div id="holder-nrds" class="text-input-small-nrds">
							<div class="clear">
								<ul id="selecteddesigs" style="float: left">
								</ul>
								<input type="text" id="brokerageInput" style="float: left"
									class="text-input" /> <img id="imageloading"
									style="padding-top: 4px; position: relative; left: -20px; display: none;"
									src="<?php echo str_replace('/administrator', "", $this->baseurl)."/images/ajax-loader.gif"?>" />
								<input type="hidden" id="jform_country" value="<?php echo $this->user->country?>"/>
								<input type="hidden" id="jform_state" value="<?php echo $this->user->state?>"/>
								<input type="hidden" id="jform_city" value="<?php echo $this->user->city?>"/>
							</div>
						</div>
						<a href="javascript:void(0)" class="button gradient-blue" style="padding:5px; position:absolute" id="savebroker"> Save </a>
					</div>

				</td>
				</tr>
			
			</table>
			</div>
			
			<div id="sales" class="tab-pane">
				<table>
				<tr><td colspan="2"><strong>2015</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text" id="volume_2015" name="jform[volume_2015]" value="<?php echo number_format(get_salesV2015($data->user_id)->volume_2015)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text" id="sides_2015" name="jform[sides_2015]"  value="<?php echo number_format(get_salesV2015($data->user_id)->sides_2015)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2015($data->user_id)->ave_price_2015)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2015]"  value="<?php echo get_salesV2015($data->user_id)->verified_2015 ?>"/></td>
					</tr>
					<tr><td colspan="2"><strong>2014</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text" id="volume_2014" name="jform[volume_2014]" value="<?php echo number_format(get_salesV2014($data->user_id)->volume_2014)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text" id="sides_2014" name="jform[sides_2014]"  value="<?php echo number_format(get_salesV2014($data->user_id)->sides_2014)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2014($data->user_id)->ave_price_2014)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2014]"  value="<?php echo get_salesV2014($data->user_id)->verified_2014 ?>"/></td>
					</tr>
					<tr><td colspan="2"><strong>2013</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text" id="volume_2013" name="jform[volume_2013]" value="<?php echo number_format(get_salesV2013($data->user_id)->volume_2013)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text" id="sides_2013" name="jform[sides_2013]"  value="<?php echo number_format(get_salesV2013($data->user_id)->sides_2013)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2013($data->user_id)->ave_price_2013)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2013]"  value="<?php echo get_salesV2013($data->user_id)->verified_2013 ?>"/></td>
					</tr>
					<tr><td colspan="2"><strong>2012</strong></td></tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sales Volume</label></td>
					<td style="padding-bottom:15px"><input type="text"  id="volume_2012"  name="jform[volume_2012]"  value="<?php echo number_format(get_salesV2012($data->user_id)->volume_2012)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Total Sides</label></td>
					<td style="padding-bottom:15px"><input type="text"  id="sides_2012" name="jform[sides_2012]"  value="<?php echo number_format(get_salesV2012($data->user_id)->sides_2012)?>"/></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Average Price</label></td>
					<td style="padding-bottom:15px"><?php echo '$'.number_format(get_salesV2012($data->user_id)->ave_price_2012)?></td>
					</tr>
					<tr>
					<td style="padding-bottom:15px; width:150px"><label>Verified:</label></td>
					<td style="padding-bottom:15px"><input style="width:20px" type="text"  name="jform[verified_2012]"  value="<?php echo get_salesV2012($data->user_id)->verified_2012 ?>"/></td>
					</tr>
				</table>
			</div>
			<div id="assistant" class="tab-pane ">
				<table width="600">
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Copy on All Communication</label></td>
						<td style="padding-bottom:15px"><input type="checkbox"  id="cc_check" name="jform[cc_all]" value="1" <?php echo $data->cc_all ? "checked" : "" ?> /> <span id='cc_val'>No</span></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>First Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[a_fname]" value="<?php echo stripslashes_all($data->a_fname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Last Name</label></td>
						<td style="padding-bottom:15px"><input type="text"  name="jform[a_lname]" value="<?php echo stripslashes_all($data->a_lname) ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Email</label></td>
						<td style="padding-bottom:15px"><input type="email"  name="jform[a_email]" value="<?php echo $data->a_email ?>"/></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Phone number</label></td>
						<td style="padding-bottom:15px"><input placeholder="(999) 999-9999" id="assist_tel" class="numbermask" type="text"   name="jform[a_pnumber]" value="<?php echo $data->a_pnumber ?>"/></td>
					</tr>
				</table>
			</div>
			<div id="zipworkaround" class="tab-pane ">
				<table width="600">
				<?php 
					//foreach ($this->zip_workarounds as $key => $value) {
						# code...
						//var_dump($value);

						$masks_zips = $this->zip_workarounds[0]->zip_format;
						$wzip = explode(",",$this->zip_workarounds[0]->zip_workaround);
						$def_country = $this->zip_workarounds[0]->country;
						if(!$def_country){
							$def_country = $data->country;
						} 
						if(count($wzip)<=0){
							$wzip="";
						}
					 ?>


					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Country:</label></td>
						<td style="padding-bottom:15px"><select 
							id="jform_wz_country" 
                            style="margin-top:20px" 
							name="jform[wz_country]"
							>
							<?php 
								foreach($this->countries as $cArr){
									$sel_country = "";
									if($cArr->countries_id == $def_country){
										$sel_country = "selected='selected'";
									}
									echo "<option data=\"".$cArr->countries_iso_code_2."\" value=\"".$cArr->countries_id."\" ".$sel_country.">".$cArr->countries_name."</option>";
								}
							?>

							</select></td>
					</tr>
					<tr>
						<td style="padding-bottom:15px; width:150px"><label>Works Around Zips:</label></td>
						<td style="padding-bottom:15px">
						<div id="zips_list" style="  font-size: 12px;text-decoration: none;color: #006699;margin-bottom: 10px; "><span style="color:black;margin-right: 5px;">Zip/Postcodes:</span></div>	
						<div><input placeholder="Enter Zip" id="jform_zip" type="text"  name="jform[w_zips]" value="" style="margin:0;width:110px;"/>
						<span id="add_zips" class="button gradient-blue" style="color:#007bae;font-size: 13px;text-decoration: none;cursor:pointer;padding:5px;">+ Add Zip/Postcode</span>
						<img height="20px" id="checkzipload" style="display: none;position: absolute;width: 20px;"id="loading-image_custom_question" src="<?php echo str_replace('/administrator', "", $this->baseurl); ?>/images/ajax_loader.gif" alt="Loading..." />
						</div></td>
					</tr>
				
				<?php	//}
				?>
				
					
				</table>
			</div>
		</div>
	</fieldset>
	<div class="buttons">
		<a href="<?php echo JRoute::_('index.php?option=com_useractivation');?>"><button type="submit" class="btn btn-primary validate"><?php echo JText::_('Save and Close');?></button></a>
		<a class="btn" href="<?php echo JRoute::_('index.php?option=com_useractivation');?>" title="<?php echo JText::_('JSAVE');?>"><?php echo JText::_('JCANCEL');?></a>
	</div>
	<?php echo JHtml::_('form.token');?>
	
	</form>
	<input type="hidden" name="country" id="country" value="<?php echo $this->countryIso; ?>"/>
	
<div id="addpops" style="display:none;">
<form id="pocket-listingx" class="oooooform-validateooooo form-horizontal" enctype="multipart/form-data" >
	<input type="hidden" name="user_id" value="<?php echo $this->users_id; ?>" />
	<input id="buyer_form_type" type="hidden" name="jform[form]" value="add_pocket" />
	<input type="hidden" id="jform_country2" name="jform[country]" value="<?php echo $this->countryId; ?>"/>
	<input type="hidden" name="jform[currency]" id="jform_currency" value="<?php echo $this->countryCurrency; ?>"/>
	<div class="pocket-form" style="padding-bottom: 0">
		<div class="c200" style="width:150px; margin-right:30px">
			<input id="jform_zip2" name="jform[zip]"
				maxlength="5" class="text-input" type="text"  
				onMouseover="ddrivetip('Enter <?php echo $ziptext_php; ?> code of POPs&trade; location.');hover_dd()"
				onMouseout="hideddrivetip();hover_dd()" placeholder="Postal Code" />
			<div id="clickchangecountry" class="change_country"><a href="#" onclick="return false;"> Change country </a></div>
			<div>
				<p class="jform_zip error_msg" style="margin-top:12px;margin-left:2px;display:none"> This field is required <br /></p> 
				<p class="jform_zip_2 error_msg" style="margin-top:12px;margin-left:2px;display:none; line-height:14px"> Zip code must be at least 5 digits. <br /></p>
			</div>
		</div>
		<div id="p_type" class="c200" style="width:220px" onMouseover="ddrivetip('Select type of property: Purchase or lease. Residential or Commercial.');hover_dd()" onMouseout="hideddrivetip();hover_dd()" >
			<select style="width:195px" 
				id="jform_ptype" 
				name="jform[ptype]" 
				onchange="get_sub(this.value)">
				<option value="">Property Type</option>
				<?php
				foreach($this->ptype as $ptype){
					if($ptype->type_id==$this->property_type) $selected = "selected='selected'";
					else $selected = "";
					echo "<option value=\"".$ptype->type_id."\" ". $selected .">".$ptype->type_name."</option>";
				}
				?>
			</select>
			 <img id="loading-image_custom_question" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
			<div>
				<p class="jform_ptype error_msg" style="display:none"> This field is required <br /></p> 
			</div>
		</div>
		<div id="s_type" class="c200" onMouseover="ddrivetip('Choose sub type of property.');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
			<div id="subtype" style="display:none">
				<select style="width:195px" id="jform_stype" name="jform[stype]"
					onchange="set_selects(this.value);">
					<option value="">Property Sub-Type</option>
					<?php
					foreach($this->sub_types as $stype){
						if($stype->sub_id==$this->sub_type) $selected = "selected='selected'";
						else $selected = "";
						echo "<option value=\"".$stype->sub_id."\" ".$selected.">".$stype->name."</option>";
					}
					?>
				</select>
				
				 <img id="loading-image_custom_question2" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:20px" />
				<div>
					<p class="jform_stype error_msg" style="display:none"> This field is required <br /></p> 
				</div>
			</div>
		</div>
		<div class="clear-float"></div>
		<div style="font-size: 12px;">
			<div id="completeadd" class="popsaddress"
				style="margin-top: 10px">
				<!--<label style="margin-bottom: 10px;">Address 1</label> 
				<input
					id="jform_address1"
					onMouseover="ddrivetip('For your use only. Other agents do not see the address')"
					onMouseout="hideddrivetip();hover_dd();hover_dd()" 
					name="jform[address][]" 
					class="text-input" 
					type="text" style="width: 450px; margin-left: 10px;" />
				<div class="clear-float"></div>
				<label style="margin-bottom: 10px;">Address 2</label> 
				<input
					type="text" 
					id="jform_address2" 
					name="jform[address][]" 
					class="text-input"
					style="width: 450px; margin-left: 10px;" />
				<div class="clear-float"></div>-->
				<div class="left">
					<label id="cityLabel" style="margin-bottom: 10px;">City</label> 
					<input
						id="jform_city2" 
						name="jform[city]" 
						class="text-input-city"
						type="text"
						placeholder="City" 
					/>
					<div>
						<p class="jform_city error_msg" style="margin-top:12px;margin-left:65px;display:none"> This field is required <br /></p> 
					</div>
					<div class="clear-float"></div>
				</div>
				<div class="left popsstate">
					<label class="left" id="stateLabel" style="margin-bottom: 10px; margin-top:10px"><?php echo $statetext_php; ?></label>
					<div id="stateDiv" class="state_dropdown">
						<select 
							id="jform_state2" 
							name="jform[state]">
						</select>
						<div><p class="jform_state error_msg" style="margin-top:12px;margin-left:65px;display:none"> This field is required </p></div> 
					</div>
					
				</div>
				<div class="clear-float"></div>
			</div>
		</div>
		<div class="clear-float"></div>
	</div>
	<div class="pocket-form clear-float popsformdisplay" >
		<h2>Display Name</h2>
		<div class="c200" style="width:90%">
			<input 
				id="jform_propertyname"
				onMouseover="ddrivetip('Name that can be viewed by other agents that describes your property i.e. Gated Equestrian Estate');hover_dd()"
				onMouseout="hideddrivetip();hover_dd()"
				name="jform[property_name]"
				class="text-input-propertyname" 
				type="text" 
				placeholder="Name" />
			<div><p class="jform_propertyname error_msg" style="margin-top:10px;display:none;"> This field is required </p></div>
		</div>
		<div class="clear-float"></div>
		<h2>Property Price</h2>
		<div class="rangeError" style='font-size:10px;margin-top:-10px;margin-left:160px;margin-bottom:15px;font-size:10px;display:none' ></div>
		<?php 
			$currency_def = "USD";
			$currency_sym = "$";
			
			if($this->countryCurrency){
				//$curreData = getCountryCurrencyData($this->data->country);
				$currency_def = $this->countryCurrency;
				$currency_sym = $this->countryCurrSymbol;
			}

		?>
		<div id="clickchangecurrency" class="<?php echo $currency_def; ?> def_<?php echo $currency_sym; ?>" style="display:none;font-size: 13px;margin-bottom: 15px;cursor: pointer;"><a href="#" onclick="return false;"> Currency is <?php echo $currency_sym; ?><?php echo $currency_def; ?>. Change to <span id="chosenCurr"></span>.</a> </div>	
					
		<div id="p_range" class="c200 p_range" style="width:150px; padding-top:3px" onMouseover="ddrivetip('Choose price range for buyer')" onMouseout="hideddrivetip();hover_dd()" >
			<select 
				style="width:140px" 
				id="jform_pricerange"
				onchange="select_price_type(this.value)" 
				name="jform[pricerange]" >
				<option value="1"
				<?php echo (($this->data->price_type==1) ? "selected" : ""); ?>>Price
					Range</option>
				<option value="2"
				<?php echo (($this->data->price_type==2) ? "selected" : ""); ?>>Exact</option>
			</select>
		</div>
		<div class="left ys price_row">
			<label class="hl-label">Low</label>
			<input 
				onMouseover="ddrivetip('Low price range for your property')"
				onMouseout="hideddrivetip();hover_dd()"
				id="jform_price1" 
				name="jform[price1]"							
				class="text-input pricevalue "
				type="text"
				placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" />
			<div><p class="jform_price1 error_msg" style="margin-top:10px;display:none;"> This field is required </p></div> 
		</div>
		<div class="left ys price_row">
			<label class="hl-label">High</label>
			<input 
				onMouseover="ddrivetip('Upper price range for your property')"
				onMouseout="hideddrivetip();hover_dd()"
				id="jform_price2"
				name="jform[price2]"
				class="text-input pricevalue" 
				type="text"
				placeholder="<?php echo $currency_sym; ?>0 <?php echo $currency_def; ?>" />
			<div><p class="jform_price2 error_msg" style="margin-top:10px;display:none;"> This field is required </p></div> 
		</div>
		<div id="p_disclose" class="left ys price_row" onMouseover="ddrivetip('Disclose price? If not disclosed will display at end of searches')" onMouseout="hideddrivetip();hover_dd()">
			<label>Disclose</label>
			<a href="javascript: void(0)" class="left <?php echo ($this->data->disclose || !isset($this->data->disclose)) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> disclose" style="margin-right:1px" rel="1">Yes</a>
			<a href="javascript: void(0)" class="left <?php echo (isset($this->data->disclose) && !$this->data->disclose) ? "gradient-blue-toggle yes" : "gradient-gray no"; ?> disclose" style="margin-right:1px" rel="0">No</a>
				<input 
				type="hidden" 
				value="<?php echo (isset($this->data->disclose)) ? $this->data->disclose : 1; ?>" id="jform_disclose" name="jform[disclose]"
				onMouseover="ddrivetip('Disclose price? If not disclosed will display at end of searches')"
				onMouseout="hideddrivetip();hover_dd()"
				class="text-input"/>
		</div>
		<div class="clear-float"></div>
	</div>
	<div style="display:none" id="formdiv" onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results');hover_dd()" onMouseout="hideddrivetip();hover_dd()" ></div>
	<div class="pocket-form popsformpermission">
		<div class="c480">
			<h2>Permission Settings</h2>
			<input 
				id="jform_settings" 
				checked="true" 
				name="jform[settings]"
				type="radio" 
				value="1"
				/>
			<label class="permissionlbl">&nbsp;&nbsp;<a style="color: #333333; cursor: default">Change Custom Settings</a>
			</label>
			<input 
				id="jform_settings_private" 
				name="jform[settings]"
				type="radio" 
				value="2"
				onclick="disablePermission()"
				/>
			<label class="permissionlbl">&nbsp;&nbsp;<a
				style="color: #333333; cursor: default">Set to Private</a>
			</label> <br /> 
			<a id="settings_desc" class="whatsthis" onMouseover="ddrivetip('The default settings is set to viewable by all agents. Click on What&lsquo;s this to change.');hover_dd()" onMouseout="hideddrivetip();hover_dd()"href="javascript:showSettings()" data="general" class="custom">what's this</a>
		</div>
	</div>
	<div class="pocket-form popsnotes">
		<div class="c480" >
			<h2>Description</h2>
			<textarea 
				id="jform_desc" 
				name="jform[desc]" 
				onMouseover="ddrivetip('Enter POPs&trade; description. This will be viewed by other agents based on Permission settings');hover_dd()"
				onMouseout="hideddrivetip();hover_dd()" class="required text-input text-area"></textarea>
		</div>
	</div>
	<div class="pocket-form">
		<h2>Photo Gallery</h2>
		<!-- Upload Form Starts Here -->
		<div id="a_photo" onMouseover="ddrivetip('Add a photo or allow system to use default image');hover_dd()" onMouseout="hideddrivetip();hover_dd()">
			<div id="try_hard_upload" class="text-link">+ Add photo. File must be less than 8MB.</div>
		</div>
		<!-- Upload Form Ends Here -->
			<ul class="gallery-addphoto" id="imagegallery">
			<?php
			if(count($images) > 0):
			foreach($images as $image):
			echo "<li class='nah'> <img style=\"position: absolute; width: 27px; display:none\" src=\"../images/delete-icon.png\" /> <a class=\"add-photo\"><img src=\"".trim($image->image)."\" height=\"100\" width=\"150\" /></a> <input type=\"hidden\" name=\"jform_image[]\" value=\"".$image->image."\" /></li>";
			endforeach;
			endif;
			?>
		</ul>
		<img id="imageloading"
				style="position: relative; margin-left: 5px; margin-top:20px; display: none"
				src="<?php echo $this->baseurl."/images/ajax-loader-big.gif"?>" />
		<div id="image_error" class="clear-float" style="display:none; color:#007bae;  font-size:11px; margin-bottom:10px">Please upload a JPEG, GIF or PNG file.</div>	
		<div id="savebuttons" style="clear: both"
			class="<?php echo (isset($_GET['saved']))? "gray": ""?>">
			<input name="submitformpops" id="submitformbuttonx" value="Save POPs&trade;" type="button" class="sub_button button gradient-blue validate" /> 

			<input name="" value="Cancel" type="button" class="button gradient-gray cancel closePopDialog" />
			  <img id="loading-image_custom_question3" src="https://www.agentbridge.com/images/ajax_loader.gif" style="display:none; height:26px; margin-bottom:-10px" />
		</div>
		<?php echo JHtml::_('form.token');?>
		<div class="clear-float"
			style="margin-top: 20px; margin-bottom: 40px;"></div>
	</div>
	</form>
	<div style="width:1px; height:1px overflow:hidden">
		<input type="file" name="newimagefile[]" id="inputfile2" style="opacity: 0" multiple />
	</div>
</div>

<div id="cropimage" style="display: none">
	<div style="clear:left"><input type="button" class="btn btn-primary submitform crop_image" value="Crop Image" /></div><br/>
	<img id="image_to_crop" src="" alt="" title="" />
	<form action="<?php echo JRoute::_('index.php?option=com_useractivation&task=crop');?>" method="post" id="cropform"> 
		<input type="hidden" name="x1" value="" /> 
		<input type="hidden" name="y1" value="" /> 
		<input type="hidden" name="x2" value="" /> 
		<input type="hidden" name="y2" value="" /> <input type="hidden" id="imagetopost" name="src"	value="" />
		<input type="hidden" name="resized" value="" />
		<input type="hidden" name="origheight" value="" />
		<input type="hidden" name="origwidth" value="" />
		<br />
		<input type="button" class="btn btn-primary submitform crop_image" value="Crop Image" />
	</form>
</div>	

<!-- Updated Dialog Country -->
<div id="changecountry" style="display: none; padding:10px">
	<?php
		$cl_list = $this->countries;
		$fav_countries = array(
		       					"Canada",
		       					"United States",
		       					"United Kingdom",
		       					"France",
		       					"China",
		       					"Germany",
		       					"Australia",
		       					"Ireland",
		       					"Mexico",
		       					"Italy",
		       					"Spain");
	?>
		<div id="country_modal_left">
			<?php 
			$i=0;
				  foreach($cl_list as $value){	
			?>
				<?php if(in_array($value->countries_name,$fav_countries)){?>
						<div class="<?php echo $value->countries_iso_code_2;?> choose-country"  id="<?php echo $value->countries_id?>">
				    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
				    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">
				    		<img class="ctry-flag" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">
				    		<span style="line-height:1em"><?php echo $value->countries_name;?></span>			           	
		           		</div>
				<?php //unset($cl_list[$i]);
				 }  ?>	
				<?php $i++?>
			<?php } ?>
		</div>
		<div id="country_modal_right">	
			<div class="tabs">
			    <ul class="tab-links">
			        <?php 
							    $capital_letter="A";			    
							    $once=1;
							    $x=1;
			        ?>
			        <?php foreach($cl_list as $value){	?>
			        			<?php 	if($capital_letter!=(substr($value->countries_name,0,1)) ){
				        					$capital_letter=substr($value->countries_name, 0, 1);
				        					$once=1;
				        				} else {
				        					$once=0;
				        				}

				        				if($once==1 || $x==1) { ?>

				        			<li><a href="#country_<?php echo $capital_letter; ?>" class="country_<?php echo $capital_letter; ?>" ><?php echo $capital_letter; ?></a></li>

				        		<?php $x++; }	?>
			        <?php }?>
			    </ul>
			</div>
			 <div class="tab-content" >
			 		<?php 
						    $capital_letter="A";			    
						    $once=1;
						    $x=1;
						    $def_Active="country_active";
						    $t=1;
			        ?>
			        <?php foreach($cl_list as $key=>$value){	?>


			        <?php if($capital_letter!=(substr($value->countries_name,0,1))){ echo $t!=1 ? "</div>":"";$once=1;$t=1;}  ?>  
			        <?php $capital_letter=substr($value->countries_name, 0, 1);?>
			        <?php if($once==1){ $once=0;?>
			        	<?php echo $x==1 ? "":"</div>";?>
			        	<div id="country_<?php echo $capital_letter; ?>" class="tab <?php echo $def_Active; ?>">
			        <?php  } $def_Active=""; ?>

	        			<?php if($t==1 || $t==10 || $t==20){ ?>
	        					<?php echo $t==1 ? "":"</div>";?>
				        		<div style="display:inline-block;vertical-align:top;width:150px">
				        <?php   } 	?>

				        	<?php if($capital_letter==(substr($value->countries_name,0,1))){?>
			            		<div class="<?php echo $value->countries_iso_code_2;?> choose-country"  id="<?php echo $value->countries_id?>">
						    		<input type="hidden" class="this_currency" value="<?php echo $value->currency;?>">
						    		<input type="hidden" class="this_symbol" value="<?php echo $value->symbol;?>">
						    	<!--<img class="ctry-flag-48" src="<?php echo $this->baseurl; ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lng-48.png">-->
						    		<img class="ctry-flag" src="<?php echo str_replace("/administrator", "", $this->baseurl); ?>/templates/agentbridge/images/<?php echo strtolower($value->countries_iso_code_2);?>-flag-lang.png">
						    		<?php echo $value->countries_name;?>
				           		</div>
			           		<?php  } ?>
			       		 <?php $t++; ?>
				    
				    <?php $x++; } ?>
			 </div>				
	</div>
</div>
<!-- Updated Dialog Country -->

<script>


	<?php


		echo "var zips_array = ".json_encode($wzip).";";
		echo "var zip_count = ".count($wzip).";";
	?>


	function removeItem(id){
		jQuery("#"+id).remove();
		if(id.indexOf("broker") !== -1){
			jQuery("#brokerageInput").val('');
			jQuery("#brokerageInput").show();
		}

		//console.log(id);
	}

	function removeZip(id){
		jQuery("#jform_zip").val("");
		jQuery("#"+id).remove();

		if(id.indexOf("-")){
			id = id.replace(/-/g, ' ');
		}

		var index = zips_array.indexOf(id);

		if(index>=0){

		   zips_array.splice(index, 1);
		   zip_count--;
		}
	}

	function set_masks(){
		jQuery(".numbermask").mask("(999) 999-9999");
	}
	

		function removeDesignation(id){
		jQuery("#"+id).remove();

		var designation_id = id.split("listitem");
	
		// delete designation 
		jQuery.ajax({
			url: '<?php echo JRoute::_('index.php');?>?option=com_userprofile&task=delete_single_designation&format=raw',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'desgination_id': designation_id[1],
				'user_id': '<?php echo $this->users_id?>'
			},
		  success: function( data ) {
				
		  }
		});
		//console.log(id);
	}
	function removeDesignation(id){
		jQuery("#"+id).remove();

		var designation_id = id.split("listitem");
	
		// delete designation 
		jQuery.ajax({
			url: '<?php echo JRoute::_('index.php');?>?option=com_useractivation&task=delete_single_designation&format=raw',
			type: 'POST',
			dataType: 'JSON',
			data: {
				'desgination_id': designation_id[1],
				'user_id': '<?php echo $this->users_id?>'
			},
		  success: function( data ) {
				
		  }
		});
		//console.log(id);
	}

	function logArrayElements(element, index, array) {
		if(element)
	  jQuery("#zips_list").append("<div id='"+element.replace(/\s/g , "-")+"' class='textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable' style='float:none;display: inline-block;'>"+element+'<a href="javascript:void(0)" onclick="removeZip(\''+element.replace(/\s/g , "-")+'\')" class="textboxlist-bit-box-deletebutton"></a></div>');		
	}


jQuery(document).ready(function(){
	jQuery("#openaddpops").click(function(e){
		e.preventDefault();
		jQuery("#addpops").dialog({
			modal:true,
			width: '733px',
			title: "Add POPs&#8482;",
			open: function(){
				console.log("opened");
			}
		});
	});
	
	jQuery(".closePopDialog").click(function(){
		jQuery("#addpops").dialog("close");
	});
	
	
	set_masks();
	//var assist_tel_v = document.getElementById("assist_tel");
	//assist_tel_v.setCustomValidity("Invalid format. Please follow the format 999-999-9999.");


	if(jQuery("#cc_check").is(":checked")){
			jQuery("#cc_val").text("Yes");
		} else {
			jQuery("#cc_val").text("No");
		}
		
	jQuery("#cc_check").live("change",function(){
		if(jQuery(this).is(":checked")){
			jQuery("#cc_val").text("Yes");
		} else {
			jQuery("#cc_val").text("No");
		}
	});

	//jQuery("select").chosen('destroy');
	//jQuery('select').val(jQuery("#this_usertype").val());
    //jQuery("select").chosen();
  //  jQuery("select").trigger("liszt:updated");
	var list = <?php echo json_encode($this->designationAuto); ?>;
	console.log(list);
	jQuery("#designationInput").autocomplete({
		autoFocus: true,
	    source: list,
	    minLength: 1,
	    open: function( event, ui ) {
		    jQuery(".ui-autocomplete").css('width', jQuery("#holder").width()+"!important");
		    jQuery(".ui-autocomplete").css('left', jQuery("#holder").position().left+"!important");
	    },
	    select: function(event, ui) {
	    	jQuery.ajax({
						url: '<?php echo JRoute::_("index.php");?>?option=com_useractivation&task=update_single_designation&format=raw',
						type: 'POST',
						dataType: 'JSON',
						data: {
							'desgination_id': ui.item.value,
							'user_id': '<?php echo $this->users_id?>'
						},
					  success: function( data ) {
							
					  },
					  error: function (data){
					  	alert(JSON.stringify(data));
					  }
					});
	    	var terms = split( this.value );
	          // remove the current input
	          terms.pop();
	          // add the selected item
	          terms.push( ui.item.label );
	          // add placeholder to get the comma-and-space at the end
	          terms.push( "" );
	          this.value = terms.join( ", " );
			html="<li id=\"listitem"+ui.item.value+"\" class=\"textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable \">"+ui.item.label+"<a href=\"javascript:void(0)\" onclick=removeItem(\"listitem"+ui.item.value+"\") class=\"textboxlist-bit-box-deletebutton\"></a>";
			html+="<input type=\"hidden\" value=\""+ui.item.value+"\" name=\"jform[designations][]\"/></li>";
			jQuery("#holder").prepend();
	        jQuery("#selecteddesigs").append(html);
	        jQuery("#designationInput").val('');
	        return false;
	    }
	// Format the list menu output of the autocomplete
	});/*.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	    return $( "<li></li>" )
	        .data( "item.autocomplete", item )
	        .append( "<a>" + item.itemCode + " - " + item.itemDesc + "</a>" )
	        .appendTo( ul );
	};*/
	jQuery.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + jQuery.ui.autocomplete.escapeRegex(term), "i");
        return jQuery.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}


	zips_array.forEach(logArrayElements);




	jQuery("#holder").css('width', jQuery("#jform_about").width()+5);

	jQuery("#volume_2015").keyup(function(){
		jQuery("#volume_2015").autoNumeric('init', {mDec: '0'});
		});
	jQuery("#volume_2014").keyup(function(){
		jQuery("#volume_2014").autoNumeric('init', {mDec: '0'});
		});
	jQuery("#volume_2013").keyup(function(){
		jQuery("#volume_2013").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#volume_2012").keyup(function(){
		jQuery("#volume_2012").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#sides_2015").keyup(function(){
		jQuery("#sides_2015").autoNumeric('init', {mDec: '0'});
		});
	jQuery("#sides_2015").keyup(function(){
		jQuery("#sides_2015").autoNumeric('init', {mDec: '0'});
		});
	
	jQuery("#sides_2013").keyup(function(){
		jQuery("#sides_2013").autoNumeric('init', {mDec: '0'});
		});
		
	jQuery("#sides_2012").keyup(function(){
		jQuery("#sides_2012").autoNumeric('init', {mDec: '0'});
		});

	var invalid;
	var clicked=0;


	jQuery("#jform_wz_country").live("change",function(){

			jQuery("#jform_zip").val("");
			jQuery('#jform_zip').inputmask("remove");

			var this_id = jQuery(this).val();
			zips_array.length = 0;
			jQuery.ajax({
				url: "<?php echo str_replace('/administrator', "", $this->baseurl); ?>/index.php?option=com_useractivation&task=changeCountryData&format=raw",
				type: "POST",
				data: {country:this_id},
				success: function (data){
					//jQuery("#pocketform").prepend(data);

					var datas = JSON.parse(data);

					jQuery("#jform_zip").inputmask({mask:datas.zip_format.split(','),placeholder:"",});

					jQuery("#jform_zip").blur(function(){

					  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

					});
					
					jQuery("#jform_zip").attr("maxlength",datas.zipMaxLength);
					jQuery("#jform_zip").attr("placeholder",datas.zipLabel);
					jQuery("#zips_list").html('<span style="color:black;margin-right: 5px;">Zip/Postcodes:</span>');
					
				}

			});
	});

	jQuery("#jform_zip").inputmask({
		mask:'<?php echo $masks_zips ?>'.split(','),placeholder:"",
    });

	
    jQuery("#jform_zip").blur(function(){

	  jQuery("#jform_zip").val((jQuery("#jform_zip").val()).toUpperCase());

	});

	
	jQuery("#add_zips").live("click",function(){
		
			invalid=0;

				var zip = jQuery("#jform_zip").val().trim();
				
				if(jQuery("#jform_wz_country").find(':selected').attr('data')=="IE"){

					jQuery.ajax({
			                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
			                async:false,
			                success: function(data){
			                	if(data[0]){

								} else {
									invalid=1;
									jQuery("#jform_zip").val("");
									
								}
			                }
			        }); 


				}  else if(jQuery("#jform_wz_country").find(':selected').attr('data')=="GB") {

					if(zip.length < 4){
						jQuery.ajax({
			                url: "<?php echo str_replace('/administrator', "", $this->baseurl); ?>/index.php?option=com_useractivation&task=checkPartialGBCode&format=raw&webservice=1",
			                type: "POST",
			                data: {partialCode:zip},
			                async:false,
			                success: function(data){
			                	
			                	if(data!=0){		                		
			                		console.log(data);
								} else {
									invalid=1;
									jQuery("#jform_zip").val("");
								}
			                }
				        });

					} else {
						jQuery.ajax({
				                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+jQuery("#jform_wz_country").find(':selected').attr('data')+'&username=damianwant33',
				                async:false,
				                success: function(data){
				                	console.log(data.postalcodes.length);
				                	if(data.postalcodes[0]){		                		
		
									} else {
										invalid=1;
										jQuery("#jform_zip").val("");
									}
				                }
				        }); 
					}
				} else {
					jQuery.ajax({
			                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+jQuery("#jform_wz_country").find(':selected').attr('data')+'&username=damianwant33',
			                async:false,
			                success: function(data){
			                	console.log(data.postalcodes.length);
			                	if(data.postalcodes[0]){		                		

								} else {
									invalid=1;
									jQuery("#jform_zip").val("");
								}
			                }
			        }); 

				}



				if(!invalid ){
					if(jQuery("#jform_zip").val()){			
						if(jQuery.inArray( jQuery("#jform_zip").val(), zips_array ) == -1){
							zips_array.push(jQuery("#jform_zip").val());

							if(clicked){
								//jQuery("#zips_list").append(",");
							}

							jQuery("#zips_list").append("<div id='"+jQuery("#jform_zip").val().replace(/\s/g , "-")+"' class='textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable' style='float:none;display: inline-block;'>"+jQuery("#jform_zip").val()+'<a href="javascript:void(0)" onclick="removeZip(\''+jQuery("#jform_zip").val().replace(/\s/g , "-")+'\')" class="textboxlist-bit-box-deletebutton"></a></div>');

							clicked=1;
							zip_count++;
						}
						
					}

					jQuery("#jform_zip").val("");
				} else {
					alert("Invalid Zip/Postcode/Eircode Entered.");
				}
			
			
		});

	jQuery(".btn").live("click",function(){

		jQuery("#jform_zip").inputmask("remove");
	 	var zips = zips_array.join(",");
	 	jQuery("#jform_zip").val(zips);
	});
	
	jQuery("#newimage").click(function(){
		jQuery('input[type=file]').trigger('click');
	});
	
	jQuery("#inputfile").change(function(){
		
		var appname = "http://108.163.201.122/~forqa/"; 
		var filename= jQuery(this).val().replace("C:\\fakepath\\", "");

		jQuery("#fakefile").val(filename);

		jQuery("#imageloading").show();

		var i;

		var len = this.files.length;

		var	formdata = false;

		if(window.FormData){

			formdata = new FormData();

		}

		for	(i=0	;	i	<	len;	i++	)	{

			file	=	this.files[i];

			if	(!!file.type.match(/image\/(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF)/))	{

				if	(	window.FileReader	)	{

					reader	=	new	FileReader();

					reader.onloadend	=	function	(e)	{

					//showUploadedItem(e.target.result,	file.fileName);

					};

					reader.readAsDataURL(file);

				}

				if	(formdata)	{

					formdata.append("image", file);

				}

				jQuery.ajax({

					url:	"../file_upload_dus.php",

					type:	"POST",

					data:	formdata,

					processData:	false,

					contentType:	false,

					success:	function	(response)	{
									jQuery("#imageloading").hide();
									
									imgsrc = appname+"uploads/"+response;
									
									var formdata2 = new FormData();
									//formdata2.append("fullpath", imgsrc);
									formdata2.append("src", imgsrc);
									
									jQuery.ajax({
										url:			"index.php?option=com_useractivation&task=createthumb",
										type:			"POST",
										data:			formdata2,
										processData:	false,
										contentType:	false,
										success:		function(response){
											var filename = response.replace(/^.*[\\\/]/, '');
											jQuery('#selectedimage').attr('src', response);
											jQuery('#imagehiddeninput').val(response);											
										}
									});
									
									
									
									/*
									jQuery("#image_to_crop").attr('src', appname+'uploads/'+response);

									jQuery("#imageloading").show();

									jQuery("#image_to_crop").load(function(){

										jQuery("#imageloading").hide();
										
										var dimensions = jQuery("#image_to_crop").getHiddenDimensions();

										console.log(dimensions);

										jQuery("input[name=\"origwidth\"]").val(dimensions.width);

										jQuery("input[name=\"origheight\"]").val(dimensions.height);

										if(dimensions.width > dimensions.height){

											if(dimensions.width > 1000){

												jQuery("#image_to_crop").css("max-width","800px");

												jQuery("#image_to_crop").width((jQuery(window).width()-20));

												jQuery("input[name=\"resized\"]").val('true');

											}

											else{

												jQuery("input[name=\"resized\"]").val('');

											}

										}

										else{

											if(dimensions.height > 1000){

												jQuery("#image_to_crop").css("max-height","800px");

												jQuery("#image_to_crop").height((jQuery(window).height()-20));

												jQuery("input[name=\"resized\"]").val('true');

											}

											else{

												jQuery("input[name=\"resized\"]").val('');

											}

										}



										if(jQuery(window).width()<dimensions.width){

											jQuery("#resized_img").val('mobile');

											jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());

											jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());

										}



										jQuery("#cropimage").dialog({

											modal:true,

											width: 'auto',

											title: "Resize, Drag and Crop Image",

											close: function( event, ui ) {

														jQuery("#ias-select").remove();

														jQuery("#image_to_crop").width('');

														jQuery("#image_to_crop").height('');

														jQuery("input[name=\"resized\"]").val('');

														jQuery('#image_to_crop').imgAreaSelect({remove:true});

														jQuery(".submitform").unbind('click');

													},

											dragStart: function( event, ui ) {

												jQuery('#image_to_crop').imgAreaSelect({remove:true});

											},

											dragStop: function(){

												jQuery('#image_to_crop').imgAreaSelect({ persistent:true, aspectRatio: '21:25', handles: true });

											},

											open: function(){

												
												if(jQuery(window).width()<dimensions.width){

													console.log(jQuery(window).width()<dimensions.width);

													console.log(jQuery("#image_to_crop").width());

													jQuery("#resized_img").val('mobile');

													jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());

													jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());

												}

												jQuery('#image_to_crop').imgAreaSelect({

													x1: 0,

													y1: 0,

													x2: 80,

													y2: 95,

													aspectRatio: '21:25',

													persistent:true,

													parent: "#cropimage",

													onSelectEnd: function (img, selection) {

																	jQuery('input[name="x1"]').val(selection.x1);

																	jQuery('input[name="y1"]').val(selection.y1);

																	jQuery('input[name="x2"]').val(selection.x2);

																	jQuery('input[name="y2"]').val(selection.y2);

																	jQuery('input[name="src"]').val(jQuery(img).attr('src'));



																	handles: true;

																}

												});

											}

										});

									});

									jQuery(".submitform").click(function(){

										jQuery("#imagetopost").val(appname+'uploads/'+response);

										jQuery.post(

											jQuery("#cropform").attr('action'),

											jQuery("#cropform").serialize(),

											function(data){

												jQuery('#image_to_crop').imgAreaSelect({remove:true});

												jQuery('#image_error').hide();

												jQuery("#cropimage").dialog("close");

												var filename = data.replace(/^.*[\\\/]/, '');

												if( jQuery('input[name="x1"]').val()=='') 

												{jQuery('#selectedimage').attr('src', appname+'uploads/'+response)} 

												else {jQuery('#selectedimage').attr('src', data);};

												jQuery('#selectedimage').attr('src',data);

												jQuery('#imagehiddeninput').val(filename);

											}

										);

									});
									*/

					}

				});

			}

			else{

				jQuery('#image_error').show();

				jQuery('#imageloading').hide();

			}

		}

	});

	
	jQuery(".hidebuttons").click(function(){
		jQuery(".buttons").hide();
	});
	
	jQuery(".showbuttons").click(function(){
		jQuery(".buttons").show();
	});
	
	});

	
	jQuery(document).ready(function(){
		var order_img=0;
		var order_img_crop;
		if (jQuery(window).width() <= 600) {
			jQuery("#a_photo").removeAttr('onmouseover');
			jQuery("#formdiv").removeAttr('onmouseover');
			jQuery("#jform_zip").removeAttr('onmouseover');
			jQuery("#p_type").removeAttr('onmouseover');
			jQuery("#s_type").removeAttr('onmouseover');
			jQuery("#jform_propertyname").removeAttr('onmouseover');
			jQuery("#p_range").removeAttr('onmouseover');
			jQuery("#jform_price1").removeAttr('onmouseover');
			jQuery("#jform_price2").removeAttr('onmouseover');
			jQuery("#settings_desc").removeAttr('onmouseover');
			jQuery("#p_disclose").removeAttr('onmouseover');
			jQuery("#jform_desc").removeAttr('onmouseover');
			}
		jQuery(".pricevalue").autoNumeric('init', {aSign:'<?php echo $this->getCountryLangsInitial->symbol; ?>', mDec: '0'});
		jQuery(".pricevalue_2").autoNumeric('init', {mDec: '0'});
		jQuery("#jform_pricesetting2").keyup(function(){
			jQuery("#jform_pricesetting2").autoNumeric('init', {aSign:'<?php echo $this->getCountryLangsInitial->symbol; ?>', mDec: '0'});
		});
		jQuery('#try_hard_upload').click(function(){jQuery("#inputfile2").click();});

		jQuery("#inputfile2").change(function(){

			//if(this.files.length>20){
			//	alert("Only 20 simultaneous image upload allowed");
		//		return false;
			//}
			jQuery("#image_to_crop").attr("src","");
			console.log("called");
			order_img_crop=0;
			var filename= jQuery(this).val().replace("C:\\fakepath\\", "");
			jQuery("#fakefile").val(filename);
			jQuery("#imageloading").show();

			var i;
			var len = this.files.length;
			var	formdata = false;
			if(window.FormData){
				formdata = new FormData();
			}

			var img_srcs = [];

			for	(i=0	;	i	<	len;	i++	)	{
				
				file	=	this.files[i];
				file.name = file.name+i;
				console.log(file.name);
				
				if	(!!file.type.match(/image\/(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF)/))	{
					jQuery("#images_thumbs").append('<img id="img'+i+'" class="imgs_th" src="<?php echo $this->baseurl."/images/ajax-loader-big.gif"?>"/>');
					if	(	window.FileReader	)	{
							reader	=	new	FileReader();
							reader.onloadend	=	function	(e)	{
							//showUploadedItem(e.target.result,	file.fileName);
							};
							reader.readAsDataURL(file);
						}
						if	(formdata)	{
							formdata.append("image"+i, file);
						}
				}
			}


			//alert(JSON.stringify(this.files));


			jQuery.ajax({
				url:	"../file_upload_dus.php",
				type:	"POST",
				data:	formdata,
				//async: false,
				processData:	false,
				contentType:	false,
				dataType: 'json',
				xhr: function() {  // Custom XMLHttpRequest
		            var myXhr = jQuery.ajaxSettings.xhr();
		            if(myXhr.upload){ // Check if upload property exists
		                myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
		            }
		            return myXhr;
		        },
				success:	function	(response)	{
					imgsrc = appname+"uploads/"+response;
									
					var formdata2 = new FormData();
					//formdata2.append("fullpath", imgsrc);
					formdata2.append("src", imgsrc);
					
					jQuery.ajax({
							url:			"index.php?option=com_useractivation&task=createthumb",
							type:			"POST",
							data:			formdata2,
							processData:	false,
							contentType:	false,
							success:		function(response){
								var filename = response.replace(/^.*[\\\/]/, '');
								
								try{
									jQuery("#add-photo-dynamic").parent().remove();
								}catch(e){}
								
								
								jQuery('#imagegallery').append('<li class="nah" style="overflow:hidden;"><img style="position: absolute; width: 27px; display:none" src="../images/delete-icon.png" /><a class=\"add-photo\" id="fileID'+filename+'"><img src="'+response+'" height=\"98\" width=\"150\" /> <input type="hidden" name="jform_image_order[]" value="'+(++order_img)+'" /> <input type="hidden" name="jform_image[]" value="' + response.trim() + '" /></a></li>');
								
								jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');

								jQuery("li.nah a.add-photo").hover(function(){

									jQuery(this).css('opacity', '0.4');

									jQuery(this).prev().show();

									console.log(jQuery(this).prev());

								});

								jQuery("li.nah a.add-photo").mouseout(function(){

									jQuery(this).css('opacity', '');

									jQuery(this).prev().hide();

								});

								jQuery("li.nah a.add-photo").click(function(){

									jQuery(this).parent().remove();

								});
								
							}
						});
					
					
					/*
					
					//alert(appname+"uploads/"+response);
					jQuery("#cropimage").dialog({
									modal:true,
									width: 'auto',
									title: "Resize, Drag and Crop Image",
									open: function(){
										console.log("opened");
										jQuery('#imageloading').hide();
										
										
									}
					});
					console.log(response.length);
					jQuery.each(response, function(index, element) {

			           	jQuery("#img"+index).on('load', function() { console.log("image loaded correctly");}).attr("src",appname+"uploads/"+element);

						console.log(index);	
						jQuery("#imageloading").show();	
						//console.log(response);
					});
					var loaded = 0;

					jQuery(".imgs_th").load(function() {

				          // One more image has loaded
				        ++loaded;

				          // Only if ALL the images have loaded
				        if (loaded === response.length) {

				              // This will be executed ONCE after all images are loaded.
				            jQuery("#image_to_crop").attr('src', jQuery("#img0").attr("src"));
				        }
				    });

								
							jQuery("#image_to_crop").load(function(){
								jQuery("#imageloading").hide();
								var dimensions = jQuery("#image_to_crop").getHiddenDimensions();
								console.log(dimensions);
								jQuery("input[name=\"origwidth\"]").val(dimensions.width);
								jQuery("input[name=\"origheight\"]").val(dimensions.height);
								if(dimensions.width > dimensions.height){
									if(dimensions.width > 800){
										jQuery("#image_to_crop").css("max-width","800px");
										jQuery("#image_to_crop").width((jQuery(window).width()-20));
										jQuery("input[name=\"resized\"]").val('true');
									}
									else{
										jQuery("input[name=\"resized\"]").val('');
									}
								}
								else{
									if(dimensions.height > 800){
										jQuery("#image_to_crop").css("max-height","800px");
										jQuery("#image_to_crop").height((jQuery(window).height()-20));
										jQuery("input[name=\"resized\"]").val('true');
									}
									else{
										jQuery("input[name=\"resized\"]").val('');
									}
								}

								if(jQuery(window).width()<dimensions.width){
									jQuery("#resized_img").val('mobile');
									jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
									jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
								}
								jQuery("head").append("<style id='ias-select'>.imgareaselect-outer{ width:"+jQuery("#image_to_crop").width()+"px !important;height:"+jQuery("#image_to_crop").height()+"px !important}</style>");
								
										if(jQuery(window).width()<dimensions.width){
											console.log(jQuery(window).width()<dimensions.width);
											console.log(jQuery("#image_to_crop").width());
											jQuery("#resized_img").val('mobile');
											jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
											jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
										}	
										console.log(jQuery("#resized_img").val());												
										jQuery('#image_to_crop').imgAreaSelect({
											x1: 0,
											y1: 0,
											x2: 210,
											y2: 128,
											persistent: true,
											parent: "#cropimage",
											aspectRatio: '66:43',
											onSelectEnd: function (img, selection) {
															jQuery('input[name="x1"]').val(selection.x1);
															jQuery('input[name="y1"]').val(selection.y1);
															jQuery('input[name="x2"]').val(selection.x2);
															jQuery('input[name="y2"]').val(selection.y2);
															jQuery('input[name="src"]').val(jQuery(img).attr('src'));
												        	handles: true;
											        	}
										});

								jQuery("#cropimage").dialog({
									modal:true,
									width: 'auto',
									title: "Resize, Drag and Crop Image",
									dragStop: function(){

										jQuery('#image_to_crop').imgAreaSelect({
											x1: 0,
											y1: 0,
											x2: 210,
											y2: 128,
											persistent: true,
											aspectRatio: '66:43',
											onSelectEnd: function (img, selection) {
															jQuery('input[name="x1"]').val(selection.x1);
															jQuery('input[name="y1"]').val(selection.y1);
															jQuery('input[name="x2"]').val(selection.x2);
															jQuery('input[name="y2"]').val(selection.y2);
															jQuery('input[name="src"]').val(jQuery(img).attr('src'));
												        	handles: true;
											        	}
										});
									},
									open: function(){
										
									},
									close: function( event, ui ) {
												jQuery("#ias-select").remove();
												jQuery('.imgs_th').remove();
												jQuery("#image_to_crop").width('');
												jQuery("#image_to_crop").height('');
												jQuery("input[name=\"resized\"]").val('');
												jQuery('#image_to_crop').imgAreaSelect({remove:true});
												jQuery(".submitform").unbind('click');
												try{
													jQuery("#add-photo-dynamic").parent().remove();
												}catch(e){}
												jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
											},
									dragStart: function( event, ui ) {
										jQuery('#image_to_crop').imgAreaSelect({remove:true});
									}
								});
							});
					
					*/
				}
			});
			


			
		});

		jQuery(".imgs_th").live("click",function(){
			console.log("clicked imgthumb");
			jQuery("#ias-select").remove();
			jQuery("#image_to_crop").removeAttr("style");
			jQuery("#image_to_crop").attr('src', jQuery(this).attr("src"));
				var dimensions = jQuery("#image_to_crop").getHiddenDimensions();
				console.log(dimensions);
				jQuery("input[name=\"origwidth\"]").val(dimensions.width);
				jQuery("input[name=\"origheight\"]").val(dimensions.height);
				if(dimensions.width > dimensions.height){
					if(dimensions.width > 800){
						jQuery("#image_to_crop").css("max-width","800px");
						jQuery("#image_to_crop").width((jQuery(window).width()-20));
						jQuery("input[name=\"resized\"]").val('true');
					}
					else{
						jQuery("input[name=\"resized\"]").val('');
					}
				}
				else{
					if(dimensions.height > 800){
						jQuery("#image_to_crop").css("max-height","800px");
						jQuery("#image_to_crop").height((jQuery(window).height()-20));
						jQuery("input[name=\"resized\"]").val('true');
					}
					else{
						jQuery("input[name=\"resized\"]").val('');
					}
				}

				if(jQuery(window).width()<dimensions.width){
					jQuery("#resized_img").val('mobile');
					jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
					jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
				}
			jQuery("head").append("<style id='ias-select'>.imgareaselect-outer{ width:"+jQuery("#image_to_crop").width()+"px !important;height:"+jQuery("#image_to_crop").height()+"px !important}</style>");
				if(jQuery(window).width()<dimensions.width){
							console.log(jQuery(window).width()<dimensions.width);
							console.log(jQuery("#image_to_crop").width());
							jQuery("#resized_img").val('mobile');
							jQuery("input[name=\"origwidth\"]").val(jQuery("#image_to_crop").width());
							jQuery("input[name=\"origheight\"]").val(jQuery("#image_to_crop").height());
						}	
						console.log(jQuery("#resized_img").val());												
						jQuery('#image_to_crop').imgAreaSelect({
							x1: 0,
							y1: 0,
							x2: 210,
							y2: 128,
							persistent: true,
							parent: "#cropimage",
							aspectRatio: '66:43',
							onSelectEnd: function (img, selection) {
											jQuery('input[name="x1"]').val(selection.x1);
											jQuery('input[name="y1"]').val(selection.y1);
											jQuery('input[name="x2"]').val(selection.x2);
											jQuery('input[name="y2"]').val(selection.y2);
											jQuery('input[name="src"]').val(jQuery(img).attr('src'));
								        	handles: true;
							        	}
						});		
		});

		jQuery(".submitform").live("click",function(){
			jQuery("#imagetopost").val(jQuery("#image_to_crop").attr('src'));
			jQuery.post(
				jQuery("#cropform").attr('action'),
				jQuery("#cropform").serialize(),
				function(data){
					jQuery('#image_to_crop').imgAreaSelect({remove:true});
					jQuery('#image_error').hide();
					//jQuery("#cropimage").dialog("close");
					var filename = data.replace(/^.*[\\\/]/, '');
					jQuery('#imagegallery').append('<li class="nah"><img style="position: absolute; width: 27px; display:none" src="../images/delete-icon.png" /><a class=\"add-photo\" id="fileID'+filename+'"><img src="'+data+'" height=\"98\" width=\"150\" /> <input type="hidden" name="jform_image_order[]" value="'+(++order_img)+'" /> <input type="hidden" name="jform_image[]" value="' + filename.trim() + '" /></a></li>');
					try{
						jQuery("#add-photo-dynamic").parent().remove();
					}catch(e){}
					jQuery('#imagegallery').append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
					jQuery("li.nah a.add-photo").hover(function(){
						jQuery(this).css('opacity', '0.4');
						jQuery(this).prev().show();
						console.log(jQuery(this).prev());
					});
					jQuery("li.nah a.add-photo").mouseout(function(){
						jQuery(this).css('opacity', '');
						jQuery(this).prev().hide();
					});
					jQuery("li.nah a.add-photo").click(function(){
						jQuery(this).parent().remove();
					});
					
					if(jQuery('.imgs_th').length > 1){
						if(jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().length){
							console.log(jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().attr("id"));
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().click();
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").prev().remove();
						} else {
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").prev().click();
							jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").next().remove();
						}
						
					} else {
						jQuery("img[src='"+jQuery("#image_to_crop").attr('src')+"'].imgs_th").remove();
						jQuery("#cropimage").dialog("close");
					}
					//jQuery("#cropimage").dialog("close");
				}
			);
		});


		jQuery("#pocket_setting").on('submit',function(){
			return false;
		});




		<?php $ziptext_php = "Zip";
			  $statetext_php = "State";
			  if($this->this_lang_tag=="english-CA"){ 
				$ziptext_php = "Postal";
				$statetext_php = "Province";
				?> 
			  jQuery("#jform_zip").attr("placeholder","Postal Code");
			  jQuery(".jform_zip_2.error_msg").text("Postal code must be at least 5 digits.");		

		<?php }  else {?>
			  jQuery("#jform_zip").attr("placeholder","Zip Code");
			  jQuery(".jform_zip_2.error_msg").text("Zip code must be at least 5 digits.");
		<?php }?>

		
		var $ajax = jQuery.noConflict();
		$ajax.ajax({
			url: '<?php echo str_replace("/administrator", "", $this->baseurl); ?>/custom/_get_state2.php',
			type: 'POST',
			data: { 'cID': <?php echo (!empty($this->countryId)) ? $this->countryId: '""'; ?>, 'state': <?php echo (!empty($this->data->state)) ? $this->data->state: '""'; ?> },
			success: function(e){
				$ajax("#stateDiv").html(e)
				.promise()
				.done(
						function () {
							$ajax("#jform_state2").val(<?php echo $this->data->state ?>);
							try{	
								jQuery("#jform_state2").select2();
								$ajax("#jform_state2").select2("val","<?php echo $this->data->state ?>");
							}catch(e){}

							if(jQuery("#jform_country2").val() == 38){
							//	jQuery("#s2id_jform_state a span").text("Province");
								//jQuery(".select2-results li").first().closest("span").text("Province");
							} else {
							//	jQuery("#s2id_jform_state a span").text("State");
						//		//jQuery(".select2-results li").first().closest("span").text("State");
							}
							jQuery("#s2id_jform_state a span").text("<?php echo $this->getCountryLangsInitial->stateLabel; ?>");	

						//	jQuery("#s2id_jform_state a span").text("Province");
						//	jQuery(".select2-results li").first().closest("span").text("Province");
						}
					);
			}
		});
		jQuery(".hide").fadeOut();
		jQuery("#show_address").click(function(){
			jQuery(this).toggleClass("hideChild");
			if(jQuery(this).hasClass("hideChild")){
				jQuery(".hide").fadeOut();
				jQuery(this).html('+ Add Complete Address');
			}
			else{
				jQuery(".hide").fadeIn();
				jQuery(this).html('- Add Complete Address');
			}
		});
		jQuery(".disclose").click(function(){
			jQuery(".disclose").removeClass("yes");
			jQuery(".disclose").removeClass("gradient-blue-toggle");
			jQuery(".disclose").removeClass("gradient-gray");
			jQuery(".disclose").addClass("no");
			jQuery(".disclose").addClass("gradient-gray");
			
			jQuery(this).removeClass("gradient-gray");
			jQuery(this).removeClass("no");
			jQuery(this).addClass("yes");
			jQuery(this).addClass("gradient-blue-toggle");
			jQuery("#jform_disclose").val(jQuery(this).attr("rel"));
		})
		jQuery("#imagegallery").append('<li> <a onclick="bindUpload(\'add-photo-dynamic\')" class="add-photo" id="add-photo-dynamic"></a></li>');
		jQuery("input[type=\"file\"]").mouseenter(function(){
			console.log("hover");
		});
		

		var options =  function(){ 
			var zip = jQuery("#jform_zip2").val();


			if(jQuery("#country").val()=="IE"){

				jQuery.ajax({
		                url: 'http://ws.postcoder.com/pcw/PCWZY-BGDNL-JG89X-PM9BQ/address/ie/'+zip+'?format=json',
		                success: function(data){
		                	
		                	console.log(data[0]);
		                	if (data[0] == null) {
		                		alert("Eircode search reached limit. only 15 per day");
		                }
		                	jQuery("#jform_state2 option").filter(function() {

									if(this.text == data[0].county){
										jQuery("#jform_state2").select2({ width: 'resolve' });
										jQuery("#jform_state2").select2("val", this.value);
									}
								});				
							if(typeof data[0].posttown != 'undefined'){
								jQuery("#jform_city").val(data[0].posttown);
							} else {
								jQuery("#jform_city").val(data[0].dependentlocality);
							}
							jQuery("#s2id_state").remove();
		                }
		        }); 


			} else {
				jQuery.ajax({
		                url: 'https://ba-ws.geonames.net/postalCodeLookupJSON?postalcode='+zip+'&country='+jQuery("#country").val()+'&username=damianwant33',
		                success: function(data){
		                	if(jQuery("#country").val()=='GB'){
								jQuery("#jform_state2 option").filter(function() {

									if(this.text == data.postalcodes[0].adminName3){
										//jQuery("#jform_state2").select2({ width: 'resolve' });
										jQuery("#jform_state2").val(this.value);
									}
								});
		                	} else { 
		                		jQuery("#jform_state2").val(jQuery("#zone"+data.postalcodes[0].adminCode1).val());
		                	}
					
							jQuery("#jform_city2").val(data.postalcodes[0].placeName);
							jQuery("#s2id_state").remove();
		                }
		        }); 
			}

		};
		


		jQuery('#clickchangecountry').click(function(){
				
				var status = jQuery(this).attr('data');
			
				jQuery('#changecountry').dialog(
						{
						  title: "Change Country",
						  width:'auto'
						});


		});

		jQuery("#jform_zip2").inputmask({mask:'<?php echo strtolower($this->getCountryLangsInitial->zip_format); ?>'.split(','),oncomplete:options});


		jQuery("#jform_zip2").blur(function(){

		  jQuery("#jform_zip2").val(jQuery("#jform_zip2").val().toUpperCase());

		});
		
		jQuery("#jform_zip2").attr("maxlength","<?php echo $this->getCountryLangsInitial->zipMaxLength; ?>");
		jQuery("#cityLabel").text("<?php echo $this->getCountryLangsInitial->cityLabel; ?>");
		jQuery("#jform_city").attr("placeholder","<?php echo $this->getCountryLangsInitial->cityLabel; ?>");
		jQuery("#stateLabel").text("<?php echo $this->getCountryLangsInitial->stateLabel; ?>");

		jQuery("#jform_zip2").attr("placeholder","<?php echo $this->getCountryLangsInitial->zipLabel; ?>");
	    jQuery(".jform_zip_2.error_msg").text("<?php echo $this->getCountryLangsInitial->zipErrorMess; ?>");
	    jQuery("#jform_zip").attr("onMouseover","ddrivetip('<?php echo $this->getCountryLangsInitial->zipHoverMess; ?>');hover_dd()");
				
	    jQuery("#s2id_jform_state a span").text("<?php echo $this->getCountryLangsInitial->stateLabel; ?>");	


		jQuery(".choose-country").click(function(){

			var this_id = jQuery(this).attr("id");
			var this_class=jQuery(this).attr('class').split(' ');
			
			jQuery("#clickchangecountry").html("<a href='#' onclick='return false;'> <img class='ctry-flag' style='margin-right: 6px;' src='<?php echo str_replace("/administrator", "", $this->baseurl);?>/templates/agentbridge/images/"+this_class[0].toLowerCase()+"-flag-lang.png'>Change country</a>");
			
			jQuery("#jform_zip2").unbind("keyup");
			jQuery("#jform_zip2").val("");
			jQuery('#jform_zip2').inputmask('remove');

			jQuery.ajax({
				url: "<?php echo $this->baseurl?>/index.php?option=com_useractivation&task=changeCountryData&format=raw",
				type: "POST",
				data: {country:this_id},
				success: function (data){
					//jQuery("#pocketform").prepend(data);

					var datas = JSON.parse(data);

					var array_zip = datas.zip_format.split(',');
					var zip_f = "";
					if(array_zip.length>1){
						zip_f = datas.zip_format.split(',');
					} else {
						zip_f = datas.zip_format;
					}
					jQuery("#jform_zip2").inputmask({mask:array_zip,oncomplete:options});
					
					jQuery(".sqftbycountry").html(datas.sqftMeasurement);

					jQuery("#jform_zip2").blur(function(){

					  jQuery("#jform_zip2").val((jQuery("#jform_zip").val()).toUpperCase());

					});
					
					jQuery("#jform_zip2").attr("maxlength",datas.zipMaxLength);
					jQuery("#cityLabel").text(datas.cityLabel);
					jQuery("#jform_city").attr("placeholder",datas.cityLabel);
					jQuery("#stateLabel").text(datas.stateLabel);
					jQuery("#jform_zip2").attr("placeholder",datas.zipLabel);
				    jQuery(".jform_zip_2.error_msg").text(datas.zipErrorMess);
				    jQuery("#jform_zip").attr("onMouseover","ddrivetip('"+datas.zipHoverMess+"');hover_dd()");
							
				    jQuery("#s2id_jform_state a span").text(datas.stateLabel);
				}

			});

			jQuery("#country").val(this_class[0]);
			jQuery("#jform_country2").val(this_id);
			
			get_state(this_id);

			jQuery("#clickrevertcurrency").attr('id','clickchangecurrency');

			if(jQuery(this).find(".this_currency").val() != "<?php echo $this->countryCurrency;?>"){
				jQuery('#clickchangecurrency').css("display","block");

				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
				if(count_Ar.length > 2){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}

				
				jQuery('#clickchangecurrency').addClass(jQuery(this).find(".this_currency").val()+" cho_"+jQuery(this).find(".this_symbol").val());

				var this_class=jQuery("#clickchangecurrency").attr('class').split(' ');
				jQuery("#clickchangecurrency").html("<a href='#' onclick='return false;'> Currency is "+this_class[1].split('_')[1]+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].split('_')[1]+this_class[2]+"</span>.</a></a>");
				
				jQuery("#chosenCurr").html(jQuery(this).find(".this_symbol").val()+jQuery(this).find(".this_currency").val());

				
				//jQuery("#jform_price1").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");
				//jQuery("#jform_price2").attr("placeholder",jQuery(this).find(".this_symbol").val()+"0");

				jQuery("#jform_price1").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
			    jQuery("#jform_price2").attr("placeholder",this_class[1].split('_')[1]+"0 "+this_class[0]);
				jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].split('_')[1]});
				});
				
			
			} else {	

				var count_Ar = jQuery('#clickchangecurrency').attr('class').split(' ');
				
				jQuery("#jform_price1").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
			    jQuery("#jform_price2").attr("placeholder",count_Ar[1].split('_')[1]+"0 "+count_Ar[0]);
				jQuery(".pricevalue").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				jQuery("#jform_pricesetting2").keyup(function(){
					jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:count_Ar[1].split('_')[1]});
				});
				if(count_Ar.length == 4){
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				} else if(count_Ar.length == 3){					
					var lastClass = jQuery('#clickchangecurrency').attr('class').split(' ').pop();
					jQuery('#clickchangecurrency').removeClass(lastClass);
				}

				jQuery('#clickchangecurrency').css("display","none");
			}

			jQuery(this).css("display","none");

			

			jQuery('.choose-country').not(this).each(function(){
		         jQuery(this).css("display","block");
		    });



			jQuery(".choose-country."+jQuery(this).attr("class").split(' ')[0]).css("display","none");




			jQuery('#changecountry').dialog('close');



		});

		jQuery('#clickchangecurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[3].replace("cho_","")+this_class[2]+". Revert to "+this_class[1].replace("def_","")+this_class[0]+".</a>");
			jQuery("#jform_price1").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);
			jQuery("#jform_price2").attr("placeholder",this_class[3].replace("cho_","")+"0 "+this_class[2]);

 			
 			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[3].replace("cho_","")});
			});

			jQuery("#jform_currency").val(this_class[2]);

			jQuery(this).attr('id','clickrevertcurrency');

		});

		jQuery('#clickrevertcurrency').live("click",function(){
			var this_class=jQuery(this).attr('class').split(' ');

			jQuery(this).html("<a href='#' onclick='return false;'> Currency is "+this_class[1].replace("def_","")+this_class[0]+". Change to <span id='chosenCurr'>"+this_class[3].replace("cho_","")+this_class[2]+"</span>.</a></a>");
			jQuery("#jform_price1").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);
			jQuery("#jform_price2").attr("placeholder",this_class[1].replace("def_","")+"0 "+this_class[0]);

			jQuery("#jform_currency").val(this_class[0]);


			jQuery(".pricevalue").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			jQuery("#jform_pricesetting2").keyup(function(){
				jQuery("#jform_pricesetting2").autoNumeric('update', {aSign:this_class[1].replace("def_","")});
			});

			jQuery(this).attr('id','clickchangecurrency');

		});

		/* Updated Dialog Country */

		jQuery(".choose-country.<?php echo $this->countryIso;?>").css("display","none");

		  jQuery('.tabs .tab-links a').live('click', function(e)  {
		        var currentAttrValue = jQuery(this).attr('href');

		        // Show/Hide Tabs
		        jQuery(currentAttrValue).css("display","block");
			 	jQuery(currentAttrValue).siblings().hide();
		        // Change/remove current tab to active
		        jQuery(this).parent('li').addClass('country_activel').siblings().removeClass('country_activel');
		 
		        e.preventDefault();
		    });

		/* Updated Dialog Country */


		jQuery(".sub_button").live("click",function(){
			
			jQuery("#jform_price1").val(jQuery("#jform_price1").autoNumeric('get'));
			jQuery("#jform_price2").val(jQuery("#jform_price2").autoNumeric('get'));

		});


	});
	
	
	
(function($) {
	$.fn.getHiddenDimensions = function(includeMargin) {
		var $item = this, props = {
			position : 'absolute',
			visibility : 'hidden',
			display : 'block'
		}, dim = {
			width : 0,
			height : 0,
			innerWidth : 0,
			innerHeight : 0,					
			outerWidth : 0,
			outerHeight : 0,
			naturalWidth : 0,
			naturalHeight : 0
		}, $hiddenParents = $item.parents().andSelf().not(':visible'), includeMargin = (includeMargin == null) ? false
				: includeMargin;

		var oldProps = [];
		$hiddenParents.each(function() {
			var old = {};

			for ( var name in props) {
				old[name] = this.style[name];
				this.style[name] = props[name];
			}

			oldProps.push(old);
		});

		dim.width = $item.width();
		dim.outerWidth = $item.outerWidth(includeMargin);
		dim.innerWidth = $item.innerWidth();
		dim.height = $item.height();
		dim.innerHeight = $item.innerHeight();
		dim.outerHeight = $item.outerHeight(includeMargin);

		var h = document.querySelector('#image_to_crop');
		dim.width = h.naturalWidth;
		dim.height = h.naturalHeight;

		$hiddenParents.each(function(i) {
			var old = oldProps[i];
			for ( var name in props) {
				this.style[name] = old[name];
			}
		});

		return dim;
	}
}(jQuery));


var $jquery = jQuery;

var appname = "<?php echo JUri::root(); ?>";
var hover_dd= function(){
	$jquery("#dhtmlpointer").css("display","none");
	$jquery("#dhtmltooltip").css("display","none");
	if (!$jquery("#dhtmltooltip").css('visibility') !== 'hidden') {
		$jquery("#dhtmlpointer").fadeIn("slow");
		$jquery("#dhtmltooltip").fadeIn("slow");
	}
}
//NS
function SiteSeal(img,type){
if(window.location.protocol.toLowerCase()=="https:"){var mode="https:";} else {var mode="http:";}
var host=location.host;
var baseURL=mode+"//seals.networksolutions.com/siteseal_seek/siteseal?v_shortname="+type+"&v_querytype=W&v_search="+host+"&x=5&y=5";
document.write('<a href="#" onClick=\'window.open("'+baseURL+'","'+type+'","width=450,height=500,toolbar=no,location=no,directories=no,\ status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no");return false;\'>\<img src="'+img+'" style="border:none;" oncontextmenu="alert(\'This SiteSeal is protected\');return false;"></a>');}
//TOOLTIP
var offsetfromcursorX=-20 //Customize x offset of tooltip
var offsetfromcursorY=10 //Customize y offset of tooltip
var offsetdivfrompointerX=10//Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY=10 //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).
document.write('<div id="dhtmltooltip"></div>') //write out tooltip DIV
document.write('<img id="dhtmlpointer">') //write out pointer image
var ie=document.all
var ns6=document.getElementById && !document.all
var enabletip=false
if (ie||ns6)
var tipobj=document.all? document.all["dhtmltooltip"] : document.getElementById? document.getElementById("dhtmltooltip") : ""
var pointerobj=document.all? document.all["dhtmlpointer"] : document.getElementById? document.getElementById("dhtmlpointer") : ""
function ietruebody(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
function ddrivetip(thetext, thewidth, thecolor){
if (ns6||ie){
if (typeof thewidth!="undefined") tipobj.style.width=thewidth+"px"
if (typeof thecolor!="undefined" && thecolor!="") tipobj.style.backgroundColor=thecolor
tipobj.innerHTML=thetext
enabletip=true
return false
}
}
function positiontip(e){
if (enabletip){
var nondefaultpos=false
var curX=(ns6)?e.pageX : event.clientX+ietruebody().scrollLeft;
var curY=(ns6)?e.pageY : event.clientY+ietruebody().scrollTop;
//Find out how close the mouse is to the corner of the window
var winwidth=ie&&!window.opera? ietruebody().clientWidth : window.innerWidth-20
var winheight=ie&&!window.opera? ietruebody().clientHeight : window.innerHeight-20
var rightedge=ie&&!window.opera? winwidth-event.clientX-offsetfromcursorX : winwidth-e.clientX-offsetfromcursorX
var bottomedge=ie&&!window.opera? winheight-event.clientY-offsetfromcursorY : winheight-e.clientY-offsetfromcursorY
var leftedge=(offsetfromcursorX<0)? offsetfromcursorX*(-1) : -1000
//if the horizontal distance isn't enough to accomodate the width of the context menu
if (rightedge<tipobj.offsetWidth){
//move the horizontal position of the menu to the left by it's width
tipobj.style.left=curX-tipobj.offsetWidth+"px"
nondefaultpos=true
}
else if (curX<leftedge)
tipobj.style.left="5px"
else{
//position the horizontal position of the menu where the mouse is positioned
tipobj.style.left=curX+offsetfromcursorX-offsetdivfrompointerX+"px"
pointerobj.style.left=curX+offsetfromcursorX+"px"
}
//same concept with the vertical position
if (bottomedge<tipobj.offsetHeight){
tipobj.style.top=curY-tipobj.offsetHeight-offsetfromcursorY+"px"
nondefaultpos=true
}
else{
tipobj.style.top=curY+offsetfromcursorY+offsetdivfrompointerY+"px"
pointerobj.style.top=curY+offsetfromcursorY+"px"
}
tipobj.style.visibility="visible"
if (!nondefaultpos)
pointerobj.style.visibility="visible"
else
pointerobj.style.visibility="hidden"
}
}
function hideddrivetip(){
if (ns6||ie){
enabletip=false
tipobj.style.visibility="hidden"
pointerobj.style.visibility="hidden"
tipobj.style.left="-1000px"
tipobj.style.backgroundColor=''
tipobj.style.width=''
}
}
document.onmousemove=positiontip
</script>


