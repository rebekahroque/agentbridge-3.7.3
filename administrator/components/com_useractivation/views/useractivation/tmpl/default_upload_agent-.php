<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Import a CSV File with PHP & MySQL</title>
</head>

<body>

<?php if (!empty($_GET[success])) { echo "<b>Your file has been imported.</b><br><br>"; } //generic success notice ?>

<div style="margin-bottom:20px">
<strong>Guide on Uploading Agents</strong> <br/>
<strong>Last ID used:<?php echo $this->lastid; ?></strong><br/><br/>
<ul>
	<li><strong>NEW</strong>: For the Brokerage Column (column D), use the Broker ID instead of the Name. Search for the Broker name <a href="<?php echo $this->baseurl ?>/index.php?option=com_brokerage" target="blank">here</a> and use the number indicated in the ID column</li>
	<li>File format must be in .CSV</li>
	<li>There shouldn't be any commas in the file</li>
	<li>Use USA/CAN for Country Column (column I)</li>	
	<li>Phone number must be in this format (123) 456-7890</li>
</ul>
</div>
<form action="<?php echo JRoute::_('index.php?option=com_useractivation&task=process_csv')?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
  Choose your file: <br />
  <input name="csv" type="file" id="csv" />
  <input type="submit" class="btn btn-primary" name="Submit" value="Submit" />
</form>

</body>
</html>