<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
jimport('joomla.mail.helper'); 
class UserActivationController extends JControllerLegacy
{	

	public function update_brokerage(){
		$broker_id 	= $_POST['broker_id'];
		$country_id = $_POST['country_id'];
		$user_id = $_POST['user_id']; 
		$model 		= $this->getModel($this->getName());
		echo json_encode( $model->updatebrokerage_info( $user_id, $country_id,  $broker_id ) );
		die();
	}


	public function update_single_designation(){
		$model 	= $this->getModel('UserProfile');
		if( JRequest::getVar('user_id')!="" ) {
			$designation_id = JRequest::getVar('designation_id');
			$user_id =  JRequest::getVar('user_id');
			$user_id = (int)$user_id;
			$result = $model->update_single_designation( $designation_id, $user_id );
		}
		echo json_encode((int)$user_id );
		die();
	}
	public function delete_single_designation(){
		$model 	= $this->getModel('UserProfile');
		if( JRequest::getVar('desgination_id')!="" ) {
			$designation_id = JRequest::getVar('desgination_id');
			$user_id =  JRequest::getVar('user_id');
			$user_id = (int)$user_id;
			$result = $model->delete_single_designation( $designation_id, $user_id );
		}
		echo json_encode((int)$user_id );
		die();
	}
	function upload_agent() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('agent_bridge_num')->from('#__temp_agent_list')->order('agent_bridge_num DESC')->setLimit('1');
		$db->setQuery( $query );
		$lastid= $db->loadObject()->agent_bridge_num;
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'lastid', $lastid );
		$view->display($this->getTask());
	}
	function encrypt($plain_text) {
		$key = 'password to (en/de)crypt';
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $plain_text, MCRYPT_MODE_CBC, md5(md5($key))));
		return $encrypted;
	}


	function generateUserSalesCSV(){
	      
		  
	      ## Make DB connections
	      $db    = JFactory::getDBO();
	       
			$query = $db->getQuery(true);

			$query->select(

				array(

					'ur.user_id AS reg_ID,

					 ur.firstname AS first_name, 

					 ur.lastname AS last_name,

					 us.volume_2012,

					 us.sides_2012,

					 us.ave_price_2012,

					 us.verified_2012,

					 us.volume_2013,

					 us.sides_2013,

					 us.ave_price_2013,

					 us.verified_2013,

					 us.volume_2014,

					 us.sides_2014,

					 us.ave_price_2014,

					 us.verified_2014,

					 us.volume_2015,

					 us.sides_2015,

					 us.ave_price_2015,

					 us.verified_2015'

				)

			);



			$query->from('#__users u');
			
			$query->leftJoin('#__user_registration ur ON ur.email = u.email');

			$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');

			$query->where('ur.email NOT LIKE "%yopmail%" ');

			$query->group('ur.user_id');
	      
	      $db->setQuery($query);
	      $rows = $db->loadAssocList();

	       $time = time();
	      
	      ## If the query doesn't work..
	      if (!$db->query() ){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      
	      $fields = (array_keys($rows[0]));
	      
	      ## Count all fields(will be the collumns
	      $columns = count($fields);
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	       if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	}else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      
	      $data .= "\n";
	      
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="price_type"){
	         	 $value = $value == 1 ? "Price Range" : "Exact Price";  
	         	} else if($key=="price1" || $key=="price2"){
	         	 $value = $value!=NULL ? "$".$value : "";
	         	} else
	         	 $value = str_replace(array("\\","/",","), "",  $value);

	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      
	      $data = str_replace("\r","",$data);
	      
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      
	      ## Push the report now!
	      $this->name = 'User-Sales-Report-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }

	function process_csv() {
		// INITIALIZE DATABASE CONNECTION
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$application = JFactory::getApplication();
		if ($_FILES[csv][size] > 0) {
			//get the csv file
			$file = $_FILES[csv][tmp_name];
			$handle = fopen($file,"r");
			//loop through the csv file and insert into database
			do {
				if ($data[0]) {
					$insert_temp_agent = new JObject();
					$insert_temp_agent->agent_bridge_num	= $data[0];
					$insert_temp_agent->first_name 		 	= $data[1];
					$insert_temp_agent->last_name 		 	= $data[2];
					$insert_temp_agent->gender 		 		= $data[3];
					$insert_temp_agent->company 			= (int) $data[4];
					$insert_temp_agent->mobile 			 	= $data[5];
					$insert_temp_agent->work 				= $data[6];
 					$insert_temp_agent->fax 				= $data[7];
					$insert_temp_agent->email 			 	= $data[8];
					$insert_temp_agent->country			 	= $data[9];
					$insert_temp_agent->address_1 		 	= $data[10];
					$insert_temp_agent->address_2 		 	= $data[11];
					$insert_temp_agent->city 				= $data[12];
					$insert_temp_agent->states 			 	= $data[13];
					$insert_temp_agent->zip_code 			= $data[14];
					$insert_temp_agent->al_number 		 	= $data[15];
					$insert_temp_agent->bl_number 		 	= $data[16];
					$insert_temp_agent->volume_2012 		= (int) $data[17];
					$insert_temp_agent->sides_2012 			= (int) $data[18];
					$insert_temp_agent->ave_price_2012  	= (int) $data[19];
					$insert_temp_agent->verified_2012 	 	= (int) $data[20];
					$insert_temp_agent->volume_2013 		= (int) $data[21];
					$insert_temp_agent->sides_2013 			= (int) $data[22];
					$insert_temp_agent->ave_price_2013 		= (int) $data[23];
					$insert_temp_agent->verified_2013	  	= (int) $data[24];
					$insert_temp_agent->volume_2014 		= (int) $data[25];
					$insert_temp_agent->sides_2014			= (int) $data[26];
					$insert_temp_agent->ave_price_2014 		= (int) $data[27];
					$insert_temp_agent->verified_2014	  	= (int) $data[28];
					$insert_temp_agent->volume_2015 		= (int) $data[29];
					$insert_temp_agent->sides_2015			= (int) $data[30];
					$insert_temp_agent->ave_price_2015 		= (int) $data[31];
					$insert_temp_agent->verified_2015	  	= (int) $data[32];
					$insert_temp_agent->volume_2016 		= (int) $data[33];
					$insert_temp_agent->sides_2016			= (int) $data[34];
					$insert_temp_agent->ave_price_2016 		= (int) $data[35];
					$insert_temp_agent->verified_2016	  	= (int) $data[36];
					$insert_temp_agent->user_type	 		= (int) $data[37];
					$db->insertObject('#__temp_agent_list', $insert_temp_agent);
				}
			} while ($data = fgetcsv($handle,1000,",","'"));
			$query = $db->getQuery(true);
 			$query->select('*')->from('#__temp_agent_list')->where('is_processed = 0');
			$db->setQuery($query);
			$result = $db->loadObjectList();
			foreach($result as $row){
				$queryx = $db->getQuery(true);
				$queryx->select('*')->from('#__countries')->where("countries_iso_code_3 = '".$row->country."' LIMIT 1");
				$db->setQuery( $queryx );
				$country_id = $db->loadObject()->countries_id;
				$queryz = $db->getQuery(true);
				$queryz->select('*')->from('#__zones')->where("zone_country_id = '".$country_id."' AND zone_code = '".$row->states."' LIMIT 1");
				$db->setQuery( $queryz );
				$zone_id = $db->loadObject()->zone_id;
				$querya = $db->getQuery(true);
				$querya->select('*')->from('#__broker')->where('broker_id  ='.$row->company);
				$db->setQuery( $querya );
				$broker_id = $db->loadObject()->broker_id;
				//INSERT NEW TABLES in tbl_user_registration
				$insert_new_agent = new JObject();
				$insert_new_agent->firstname 		 = $row->first_name;
				$insert_new_agent->lastname 		 = $row->last_name;
				$insert_new_agent->gender 		 = $row->gender;
				$insert_new_agent->email 			 = $row->email;
				$insert_new_agent->city 			 = $row->city;
				$insert_new_agent->zip 				 = $row->zip_code;
				if($row->al_number){
					$insert_new_agent->licence 			 = $this->encrypt($row->al_number);
				} else {
					$insert_new_agent->licence 			 = "";
				}
				if($row->bl_number){
					$insert_new_agent->brokerage_license = $this->encrypt($row->bl_number);
				} else {
					$insert_new_agent->brokerage_license 			 = "";
				}
				$insert_new_agent->street_address	 = $row->address_1;
				$insert_new_agent->suburb	 		 = $row->address_2;
				$insert_new_agent->registration_date = date('Y-m-d H:i:s',time());
				$insert_new_agent->country 			 = $country_id;
				$insert_new_agent->state 			 = $zone_id;
				$insert_new_agent->brokerage 		 = $broker_id;
				$insert_new_agent->user_type 		 = $row->user_type;			
				$db->insertObject('#__user_registration', $insert_new_agent);
				$new_user_id = $db->insertid();
				$insert_sales = new JObject();
				$insert_sales->agent_id = $new_user_id;
				$insert_sales->volume_2012 = $row->volume_2012;
				$insert_sales->volume_2013 = $row->volume_2013;
				$insert_sales->volume_2014 = $row->volume_2014;
				$insert_sales->volume_2015 = $row->volume_2015;
				$insert_sales->volume_2016 = $row->volume_2016;
				$insert_sales->sides_2012  = $row->sides_2012;
				$insert_sales->sides_2013  = $row->sides_2013;
				$insert_sales->sides_2014  = $row->sides_2014;
				$insert_sales->sides_2015  = $row->sides_2015;
				$insert_sales->sides_2016  = $row->sides_2016;
				$insert_sales->ave_price_2012  = $row->ave_price_2012;
				$insert_sales->ave_price_2013  = $row->ave_price_2013;
				$insert_sales->ave_price_2014  = $row->ave_price_2014;
				$insert_sales->ave_price_2015  = $row->ave_price_2015;
				$insert_sales->ave_price_2016  = $row->ave_price_2016;
				$insert_sales->verified_2012   = $row->verified_2012;
				$insert_sales->verified_2013  = $row->verified_2013;
				$insert_sales->verified_2014  = $row->verified_2014;
				$insert_sales->verified_2015  = $row->verified_2015;
				$insert_sales->verified_2016  = $row->verified_2016;
				$db->insertObject('#__user_sales', $insert_sales);
				if ($row->mobile) {
					$insert_phone = new JObject();
					$insert_phone->user_id = $new_user_id;
					$insert_phone->value = $row->mobile;
					$insert_phone->main = 1;
					$insert_phone->show = 1;
					$db->insertObject('#__user_mobile_numbers', $insert_phone);
				}
				if ($row->work) {
					$insert_work = new JObject();
					$insert_work->user_id = $new_user_id;
					$insert_work->value = $row->work;
					$insert_work->main = 1;
					$insert_work->show = 1;
					$db->insertObject('#__user_work_numbers', $insert_work);				
				}
				if ($row->fax) {
					$insert_fax = new JObject();
					$insert_fax->user_id = $new_user_id;
					$insert_fax->value = $row->fax;
					$insert_fax->main = 1;
					$insert_fax->show = 1;
					$db->insertObject('#__user_fax_numbers', $insert_fax);
				}
				$is_processed = new JObject();
				$is_processed->is_processed = 1;
				$is_processed->agent_bridge_num =  $row->agent_bridge_num;;
				JFactory::getDbo()->updateObject('#__temp_agent_list', $is_processed, 'agent_bridge_num');
			}
			$application->redirect(JRoute::_("index.php?option=com_useractivation"));			
		}	
	}
	function save() {
		$model = $this->getModel('UserActivation');
		$application = JFactory::getApplication();
		foreach($_POST['jform'] as $key=>$value){
				if(is_array($value)){
					foreach($value as $k=>$v){
						$data[$key][$k] = trim(addslashes($v));
					}
				}
				else{
					$data[$key] = trim(addslashes($value));
				}
			}
		extract($data);
		$uid = $data['uid'];		
		$orig_email = $model->getOrigEmail($uid);
		
		$orig_address = $model->getUserAddress($uid);
		$county = $model->getCounty($orig_address->zip, $orig_address->countries_iso_code_2);
		$old_user_details = array(
			'email' => $orig_email, 
			//'fname' => $data['firstname'],
			//'lname' => $data['lastname'],
			//'state' => $orig_address->zone_code,
			//'country' => $orig_address->countries_iso_code_3,
			//'zip' => $orig_address->zip,
			//'city' => $county,
		);
		
		$old_chimp_status = json_decode($model->checkUserChimp($old_user_details));		
		if($orig_email != $data['email']) {
			#echo $old_chimp_status->status . "<br />";
			#exit;
			if($old_chimp_status->status == "subscribed") {
				$old_user_details = array(
					'email' => $orig_email, 
					//'fname' => $data['firstname'],
					//'lname' => $data['lastname'],
					//'state' => $orig_address->zone_code,
					//'country' => $orig_address->countries_iso_code_3,
					//'zip' => $orig_address->zip,
					//'city' => $county,
					'subs_val' => 'unsubscribed',
				);
				
				$chimp_result = $model->addUserToChimp($old_user_details, "subs_change");
			}
			
			$new_user_details = array(
				'email' => $data['email']
				//'state' => $orig_address->zone_code,
				//'country' => $orig_address->countries_iso_code_3,
				//'city' => $county,
			);
			
			$new_chimp_status = json_decode($model->checkUserChimp($new_user_details));
			#print_r($new_user_details);
			#echo $new_chimp_status->status;
			#exit;
			if($new_chimp_status->status == "404") {
				$new_user_details = array(
					'email' => $data['email'], 
					'fname' => $data['firstname'],
					'lname' => $data['lastname'],
					'state' => $orig_address->zone_code,
					'country' => $orig_address->countries_iso_code_3,
					'zip' => $orig_address->zip,
					'city' => $county,
					'subs_val' => 'subscribed',
				);
				//print_r($new_user_details); exit;
				$chimp_result = $model->addUserToChimp($new_user_details);
			} else if($new_chimp_status->status == "unsubscribed") {
				$new_user_details = array(
					'email' => $data['email'], 
					'subs_val' => 'subscribed',
				);
				//print_r($new_user_details); exit;
				$chimp_result = $model->addUserToChimp($new_user_details, "subs_change");
				$new_user_details = array(
					'email' => $data['email'], 
					'fname' => $data['firstname'],
					'lname' => $data['lastname'],
					'state' => $orig_address->zone_code,
					'country' => $orig_address->countries_iso_code_3,
					'zip' => $orig_address->zip,
					'city' => $county
				);
				$chimp_result = $model->addUserToChimp($new_user_details, "update");
			}
		}
		
		
		$model->updateUserName($orig_email, $data);
  		$model->updateUserRegEmail($data);
		$model->updateUserEmail($data);
		$model->updateUserInfo($data);
		if($data['imagePath']) {
			if(strpos($data['imagePath'], "http://qa.agentbridge.com/~forqa/uploads/") !== FALSE){
				$data['imagePath'] = str_replace("http://qa.agentbridge.com/~forqa/uploads/", "", $data['imagePath']);
			}
			$basepath = str_replace("/administrator", "", JURI::base()) . "uploads/";
			$data['image'] = str_replace($basepath, "", $data['imagePath']);
			$data['image'] = str_replace("/administrator", "", JURI::base()) . "uploads/" . $data['image'];
			$model->updateProfileImage($data);
		}

		$mailSender =& JFactory::getMailer();
        $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
        $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
        $mailSender ->setSubject( "save user activation" );
        $mailSender ->isHTML(  true );
        $mailSender ->setBody( print_r($data,true));
        $mailSender ->Send(); 


		$model->updateUserEmailInvitation($orig_email, $data);
		$model->updateAssistantDetails($uid, $data);
		$model->updateWorkZips($uid, $data);
		$application->enqueueMessage('You have successfully edited '.$data['firstname'].'\'s profile', 'Success');
		$application->redirect(JRoute::_("index.php?option=com_useractivation"));
	}
	function edit() {	
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
		$model = $this->getModel('UserActivation');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$model = $this->getModel('UserActivation');
		$uid = $_POST['cid'][0];
		if($_POST['addmsg']) {
			$application = JFactory::getApplication();
			$application->enqueueMessage("Successfully added POPs&trade;");
		}
		if($_POST['editmsg']) {
			$application = JFactory::getApplication();
			$application->enqueueMessage("Successfully edited POPs&trade;");
		}
		$query->select('*, ur.email AS notact_email');
		$query->from('#__user_registration ur');
		$query->where('ur.user_id = '.$uid);
		$query->leftJoin('#__users u ON u.email = ur.email');
		$query2 = $db->getQuery(true);
		$query2->select('*');
		$query2->from('#__user_assistant');
		$query2->where('user_id = '.$uid);
		$db->setQuery($query2);
		$result1 = $db->loadObjectList();
		if($result1){
			$query->leftJoin('#__user_assistant ua ON ua.user_id = ur.user_id');
		}
		
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'datas', $results );
		
		$_email = ($results[0]->email) ? $results[0]->email : $results[0]->notact_email;
		
		$country = $model->getCountry($_email);
		
		if($results[0]->user_type == 2 && !$results[0]->email) {
			$db3 = JFactory::getDbo();
			$query3 = $db3->getQuery(true);
			$query3->select('ur.city, ur.state');
			$query3->from('#__user_registration ur');
			$query3->leftJoin('#__invitation_tracker it ON it.agent_id = ur.user_id');
			$query3->where("it.invited_email = '".$_email."'");
			
			$db3->setQuery($query3);
			
			$result3 = $db3->loadObjectList();
			
			$is_invited = true;
			
			$view->assignRef( 'invited', $is_invited );
			$view->assignRef( 'invited_city', $result3[0]->city );
			$view->assignRef( 'invited_state', $result3[0]->state );
		}
		
		$view->assignRef( 'designationAuto', $model->get_designations($results[0]->country));
		if ($results[0]->activation_status==1) {
			$view->assignRef('designations', $model->get_user_designations($results[0]->id));
		}
		$getCountryLangs = $this->getCountryLangs();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__users');
		$query->where('email = \''.$_email.'\'');
		$db->setQuery($query);
		$users_id = $db->loadObjectList();

		if(count($users_id) && $users_id[0]->id !== "") {
			$userid = $users_id[0]->id;
			$arr_user_pops = $this->get_user_pops($userid);
		} else {
			$userid = "";
			$arr_user_pops = array();
		}
		$zip_workarounds = $model->getZipWorkaround($results[0]->user_id);
		$ptype = $model->getAllPropertyTypes();
		if($country[0]->country) {
			$getCountryLangsInitial = $this->getCountryDataByID($country[0]->country);
		} else {
			$getCountryLangsInitial = $this->getCountryDataByID(223);
		}
		
		$view->assignRef( 'getCountryLangsInitial', $getCountryLangsInitial );
		$view->assignRef('zip_workarounds', $zip_workarounds);
		$view->assignRef('users_id', $userid);
		$view->assignRef('countries', $getCountryLangs);
		$view->assignRef( 'countryId', $country[0]->country);
		$view->assignRef( 'countryIso', $country[0]->countries_iso_code_2);
		$view->assignRef( 'countryCurrency', $country[0]->currency);
		$view->assignRef( 'countryCurrSymbol', $country[0]->symbol);
		$view->assignRef('ptype', $ptype);
		$view->assignRef('user_pops', $arr_user_pops);
		$view->display($this->getTask());
	}


	function getCountryLangs(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->setQuery("
            SELECT cl.*,curr.*,clv.* FROM #__country_languages cla 
            LEFT JOIN #__country_validations clv ON clv.country = cla.country
            LEFT JOIN #__countries cl ON cla.country = cl.countries_id   
            LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id  
            ORDER BY cl.countries_name");
        $db->setQuery($query);      
        return $db->loadObjectList();
    }
    function display() {
		$model = $this->getModel('UserActivation');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(
			array(
				'ur.user_id,
				 ur.image,
				 ur.firstname, 
				 ur.lastname, 
				 ur.licence, 
				 ur.email, 
				 ur.brokerage_license, 
				 ur.is_premium, 
				 ur.status,
				 ur.brokerage,
				 ur.activation_status,
				 ur.is_term_accepted,
				 ur.city,
				 ur.state,
				 ur.user_type'
			)
		);
		$query->from('#__user_registration ur');
		$query->leftJoin('#__user_sales u ON u.agent_id = ur.user_id');
		$query->order('ur.firstname ASC');
		if(isset($_GET['page'])){
			$offsets=($_GET['page']-1)*500;
			$query->setLimit(500,$offsets);  
		} else {
			$query->setLimit(500); 
		}
		if(isset($_POST['search_users'])){
			if(isset($_POST['searchkey'])){
				if (strpos($_POST['searchkey'],"'") !== false) {
				   	$keys2 = str_replace("'", "\\\\\\\\\'", $_POST['searchkey']);
				//   	var_dump($keys2);
				$query->where("
					 ur.user_id LIKE '%".$keys2."%' OR
					 ur.firstname LIKE '%".$keys2."%' OR
					 ur.lastname LIKE '%".$keys2."%' OR
					 ur.licence LIKE '%".$keys2."%' OR
					 ur.email LIKE '%".$keys2."%' OR
					 ur.brokerage_license LIKE '%".$keys2."%' OR
					 b.broker_name LIKE '%".$keys2."%' OR
					 ur.city LIKE '%".$keys2."%' OR
					 z.zone_name LIKE '%".$keys2."%' OR
					 ur.user_type LIKE '%".$keys2."%'
				", "OR");
				}
				$keys=urldecode(addslashes($_POST['searchkey']));
				$query->where("
					 ur.user_id LIKE '%".$keys."%' OR
					 ur.firstname LIKE '%".$keys."%' OR
					 ur.lastname LIKE '%".$keys."%' OR
					 ur.licence LIKE '%".$keys."%' OR
					 ur.email LIKE '%".$keys."%' OR
					 ur.brokerage_license LIKE '%".$keys."%' OR
					 b.broker_name LIKE '%".$keys."%' OR
					 ur.city LIKE '%".$keys."%' OR
					 z.zone_name LIKE '%".$keys."%' OR
					 ur.user_type LIKE '%".$keys."%'
				");
				$query->leftJoin('#__zones z ON z.zone_id = ur.state');
				$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
			}
		}
        $db->setQuery($query);
		$results = $db->loadObjectList();
		$query = $db->getQuery(true);
		$query->select("COUNT(*)");
		$query->from('#__user_registration ur');
		if(isset($_POST['search_users'])){
			if(isset($_POST['searchkey'])){
				if (strpos($_POST['searchkey'],'\'') !== false) {
				   	$keys2 = str_replace("'", "\\\\\\\\\'", $_POST['searchkey']);
				//   	var_dump($keys2);
				$query->where("
					 ur.user_id LIKE '%".$keys2."%' OR
					 ur.firstname LIKE '%".$keys2."%' OR
					 ur.lastname LIKE '%".$keys2."%' OR
					 ur.licence LIKE '%".$keys2."%' OR
					 ur.email LIKE '%".$keys2."%' OR
					 ur.brokerage_license LIKE '%".$keys2."%' OR
					 b.broker_name LIKE '%".$keys2."%' OR
					 ur.city LIKE '%".$keys2."%' OR
					 z.zone_name LIKE '%".$keys2."%' OR
					 ur.user_type LIKE '%".$keys2."%'
				", "OR");
				}
				$keys=urldecode(addslashes($_POST['searchkey']));
				$query->where("
					 ur.user_id LIKE '%".$keys."%' OR
					 ur.firstname LIKE '%".$keys."%' OR
					 ur.lastname LIKE '%".$keys."%' OR
					 ur.licence LIKE '%".$keys."%' OR
					 ur.email LIKE '%".$keys."%' OR
					 ur.brokerage_license LIKE '%".$keys."%' OR
					 b.broker_name LIKE '%".$keys."%' OR
					 ur.city LIKE '%".$keys."%' OR
					 z.zone_name LIKE '%".$keys."%' OR
					 ur.user_type LIKE '%".$keys."%'
				");
				$query->leftJoin('#__zones z ON z.zone_id = ur.state');
				$query->leftJoin('#__broker b ON b.broker_id = ur.brokerage');
			}
		}
		$db->setQuery($query);
		$total_results = $db->loadResult();
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'datas', $results );	
		$view->assignRef( 'total_results', $total_results );	
		$application = JFactory::getApplication();
		$task = JRequest::getVar('task');
		switch ($task):
			case 'add':
				break;
			case 'edit':			
				$application->redirect(JRoute::_("index.php?option=com_useractivation").'&uid='.$_POST['cid'][0]);
				break;
			case 'activate':	
				$is_premium .="0 ";
				$date = date('Y-m-d H:i:s',time());
				date_default_timezone_set('America/Los_Angeles');
					try {
						foreach($_POST['cid'] as $ids):
						$intval_cid = (int)$ids;
						if(isset($_POST['premium'][$intval_cid]) && $_POST['premium'][$intval_cid]=="on"){
								$fields = array(
								$db->quoteName('is_premium') . '=1',
								$db->quoteName('status') . '=1',
								);
						} else {
								$fields = array(
									$db->quoteName('is_premium') . '=0',
									$db->quoteName('status') . '=1',
								);
						}		
						$query = $db->getQuery(true);
						$conditions = array(
							$db->quoteName('user_id') . '=\''.$intval_cid.'\'', 
						);
						$query->update($db->quoteName('#__user_registration'))->set($fields)->where($conditions);
						$db->setQuery($query);			 
						$result = $db->execute();
						$token_date = date('Y-m-d H:i:s',time());
						$token = crypt($intval_cid.$token_date);
						$query = $db->getQuery(true);
						$query->select('ur.email,ua.a_email,ua.cc_all');
						$query->from('#__user_registration ur');
						$query->leftJoin('#__user_assistant ua ON ua.user_id = ur.user_id');
						$query->where('ur.user_id'." = ".$intval_cid);
						$db->setQuery($query);
						$email = $db->loadObject()->email;
						$assistant_email="";
						if($db->loadObject()->cc_all && $db->loadObject()->cc_all == 1){
							$assistant_email = $db->loadObject()->a_email;
						}
						$query = $db->getQuery(true);
						$query->select('COUNT(*)');
						$query->from($db->quoteName('#__user_registration_tokens'));
						$query->where($db->quoteName('user_id')." = ".$db->quote($intval_cid));
						// Reset the query using our newly populated query object.
						$db->setQuery($query);
						$token_exist = $db->loadResult(); 
						if($token_exist == "0"){
							$columns = array("user_id","email","token","token_date");
							$values = array($intval_cid,"'".$email."'","'".$token."'","'".$token_date."'");
							// Prepare the insert query.
							$query = $db->getQuery(true);
							$query
							    ->insert($db->quoteName('#__user_registration_tokens'))
							    ->columns($db->quoteName($columns))
							    ->values(implode(',', $values));
							// Set the query using our newly populated query object and execute it.
							$db->setQuery($query);
							$db->execute();
						} else {
							$fields = array($db->quoteName('email') . '='.$db->quote($email).'',$db->quoteName('token') . '='.$db->quote($token).'',$db->quoteName('token_date') . '="'.$token_date.'"');							
							$conditions = array($db->quoteName('user_id') . '=\''.$intval_cid.'\'');							
							$query = $db->getQuery(true);
							$query->update($db->quoteName('#__user_registration_tokens'))->set($fields)->where($conditions);							 
							// Set the query using our newly populated query object and execute it.
							$db->setQuery($query);
							$db->execute();
						}
							$query = $db->getQuery(true);			
							$query->select(
								array(
									'ur.user_id', 
									'ur.firstname', 
									'ur.lastname', 
									'ur.email',
									'ur.gender',
									'c.countries_iso_code_2',  
									'ur.is_premium',
									'ur.user_type',
									'it.invited_email',
									'it.agent_id',
									'cl.lang'
								)
							);
							$query->from('
									#__user_registration ur 
								LEFT JOIN 
									#__invitation_tracker it								
								ON
									it.invited_email = ur.email
								LEFT JOIN 
									#__countries c 
								ON
									c.countries_id = ur.country
								LEFT JOIN 
									#__country_languages cl 
								ON
									cl.country = ur.country
							');
							$query->where('ur.user_id = \''.$ids.'\'');
							$db->setQuery($query);
							$results = $db->loadObjectList();
							$body = "";
							$is_premium = $results[0]->is_premium;
							$user_type = $results[0]->user_type;
							$profile_id = $results[0]->user_id;
							$invited_email = $results[0]->invited_email;
							$inviter_email = $results2[0]->email;
							$currLanguage = strtolower($results[0]->lang)."-".$results[0]->countries_iso_code_2;

							$vid=1;
							$user 	 = JFactory::getUser($inviter_email);
						  	$language = JFactory::getLanguage();
		                  	$extension = 'com_nrds';
		                  	$base_dir = JPATH_SITE;
		                  	$language_tag = $currLanguage;
		                  	$language->load($extension, $base_dir, $language_tag, true);		
		                  	
		                  	if($results[0]->gender == "Female"){
								$salut = JText::_('COM_USERACTIV_HEADERMRS')." ".stripslashes_all($results[0]->lastname);
							} else {
								$salut =  JText::_('COM_USERACTIV_HEADERMR')." ".stripslashes_all($results[0]->lastname);
							}

							if ($user_type==3){
								$subject = JText::_('COM_USERACTIV_SUB_UT3');
								$body .= "<p style='padding-top:20px'>".JText::_('COM_USERACTIV_HEADER')." ".stripslashes_all($salut).",<br/><br/></p>";
								$body .= JText::_('COM_USERACTIV_BODYA_UT3');
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&z=".$token."'>". JUri::root()."index.php?option=com_setpass&action=activated&&z=".$token."</a><br /><br/>";
								$body .= JText::_('COM_USERACTIV_BODYB_UT3');
								$body .= JText::_('COM_USERACTIV_CLOSING');
								$sender = array(  'socon.agentbridge@gmail.com' , 'AgentBridge');
							} else if ($user_type==1){
								$subject = JText::_('COM_USERACTIV_SUB_UT1');
								$body .= JText::_('COM_USERACTIV_HEADER'). " ".stripslashes_all($salut).",<br/><br/>";
								$body .= JText::_('COM_USERACTIV_BODYA_UT1');
								$body .= JText::_('COM_USERACTIV_BODYB_UT1');
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&z=".$token."'>". JUri::root()."index.php?option=com_setpass&action=activated&&z=".$token."</a><br /><br/>";
								$body .= JText::_('COM_USERACTIV_BODYC_UT1');		
								$body .= JText::_('COM_USERACTIV_CLOSING');
								$body .= JText::_('COM_USERACTIV_BODYD_UT1');
								$sender = array(  'redler@vistasir.com' , 'Rick Edler');
							} else if  ($user_type==4) {
								$sponsor = $results[0]->agent_id;
								$query2 = $db->getQuery(true);
								$query2->select(
									array( 
										'firstname', 
										'lastname',
										'gender'
									)
								);
								$query2->from('
										#__user_registration
								');
								$query2->where("user_id = $sponsor");
								$db->setQuery($query2);
								$results3 = $db->loadObjectList();	

								if($results3[0]->gender == "Female"){
									$salut3 = JText::_('COM_USERACTIV_HEADERMRS').stripslashes_all($results3[0]->firstname);
								} else {
									$salut3 = JText::_('COM_USERACTIV_HEADERMR').stripslashes_all($results3[0]->firstname);
								}


								$subject = JText::_('COM_USERACTIV_SUB_UT4'). " ".$salut3." ".stripslashes_all($results3[0]->lastname);
								$body .= JText::_('COM_USERACTIV_BODYA_UT4'). " ".$salut3." ".stripslashes_all($results3[0]->lastname)."<br/><br/>";
								$body .= JText::_('COM_USERACTIV_HEADER'). " ".$salut.",<br/><br/>";
								$body .= JText::_('COM_USERACTIV_BODYB_UT4');
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&z=".$token."'>". JUri::root()."index.php?option=com_setpass&action=activated&&z=".$token."</a><br /><br/>";
								$body .= JText::_('COM_USERACTIV_BODYC_UT1');
								$body .= stripslashes_all($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." <a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&utype=".$user_type."&jgadeprasdium=".$is_premium."'>".JText::_('COM_USERACTIV_BODYD_UT4')."</a><br/><br/>" ;
								$body .= JText::_('COM_USERACTIV_BODYD_UT1');
								$sender = array(  'socon.agentbridge@gmail.com' , 'AgentBridge');
							}	else if  ($user_type==5) {
								$sponsor = $results[0]->agent_id;
								$query2 = $db->getQuery(true);
								$query2->select(
									array( 
										'firstname', 
										'lastname',
										'gender'
									)
								);
								$query2->from('
										#__user_registration
								');
								$query2->where("user_id = $sponsor");
								$db->setQuery($query2);
								$results3 = $db->loadObjectList();	
								if($results3[0]->gender == "Female"){
									$salut3 = JText::_('COM_USERACTIV_HEADERMRS').stripslashes_all($results3[0]->firstname);
								} else {
									$salut3 = JText::_('COM_USERACTIV_HEADERMR').stripslashes_all($results3[0]->firstname);
								}

								$subject = JText::_('COM_USERACTIV_SUB_UT5'). " ".$salut3." ".stripslashes_all($results3[0]->lastname);
								$body .= JText::_('COM_USERACTIV_SUB_UT5'). " ".$salut3." ".stripslashes_all($results3[0]->lastname)."<br/><br/>";
								$body .= JText::_('COM_USERACTIV_HEADER'). " ".$salut.",<br/><br/>";
								$body .= JText::_('COM_USERACTIV_BODYA_UT5');
								$body .= JText::_('COM_USERACTIV_CONFIRM')." <br /><br />";
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&z=".$token."'>". JUri::root()."index.php?option=com_setpass&action=activated&&z=".$token."</a><br /><br/>";
								$body .= JText::_('COM_USERACTIV_BODYB_UT5');
								$body .= JText::_('COM_USERACTIV_BODYC_UT1');
								$body .= stripslashes_all($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." ".JText::_('COM_USERACTIV_BODYD_UT4')."<br/><br/>" ;
								$body .= "<table width='80%'><tr><td style='align:center'><strong>".JText::_('COM_USERACTIV_BODYC_UT5').":</strong></td></tr> <br/>
										  <tr><td style='align:center'><a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&jgadeprasdium=".$is_premium."&vid=".$vid."'><img src='".JUri::root()."/images/video-icon.jpg' style='border:none'/></a></td></tr></table><br/>";
								$sender = array(  'socon.agentbridge@gmail.com' , 'AgentBridge');
							}	else if  ($user_type==2) {

								
								$mailSender =& JFactory::getMailer();
						        $mailSender ->addRecipient( "mark.obre@keydiscoveryinc.com" );
						        $mailSender ->setSender( array(  'no-reply@agentbridge.com' , 'AgentBridge') );
						        $mailSender ->setSubject( "User Type 2" );
						        $mailSender ->isHTML(  true );
						        $mailSender ->setBody( print_r($results,true));
						        $mailSender ->Send(); 

								$sponsor = $results[0]->agent_id;
								$query2 = $db->getQuery(true);
								$query2->select(
									array( 
										'firstname', 
										'lastname',
										'gender'
									)
								);
								$query2->from('
										#__user_registration
								');
								$query2->where("user_id = ".$sponsor);
								$db->setQuery($query2);
								$results3 = $db->loadObjectList();	
								if($results3[0]->gender == "Female"){
									$salut3 = JText::_('COM_USERACTIV_HEADERMRS').stripslashes_all($results3[0]->firstname);
								} else {
									$salut3 = JText::_('COM_USERACTIV_HEADERMR').stripslashes_all($results3[0]->firstname);
								}

								$subject = JText::_('COM_USERACTIV_SUB_UT4')." ".$salut3." ".stripslashes_all($results3[0]->lastname);
								$body .= JText::_('COM_USERACTIV_BODYA_UT4')." ".$salut3." ".stripslashes_all($results3[0]->lastname)."<br/><br/>";
								$body .= JText::_('COM_USERACTIV_HEADER')." ".$salut.",<br/><br/>";
								$body .= JText::_('COM_USERACTIV_BODYB_UT4');
								$body .= JText::_('COM_USERACTIV_CONFIRM').": <a style='font-size:11px' href='".JUri::root()."index.php?option=com_setpass&action=activated&z=".$token."'>". JUri::root()."index.php?option=com_setpass&action=activated&&z=".$token."</a><br /><br/>";
								$body .= JText::_('COM_USERACTIV_BODYC_UT1');
								$body .= stripslashes_all($results3[0]->firstname). " ".JText::_('COM_USERACTIV_BODYC_UT4')." <a href='".JUri::root()."index.php?option=com_nrds&useregid=".$ids."&utype=".$user_type."&jgadeprasdium=".$is_premium."'>".JText::_('COM_USERACTIV_BODYD_UT4')."</a><br/><br/>" ;
								$body .= JText::_('COM_USERACTIV_BODYD_UT1');
								$sender = array(  'socon.agentbridge@gmail.com' , 'AgentBridge');


							}		
							$email = $results[0]->email;
							$bcc2 = ('rebekahroque@gmail.com');
							$bcc3 = ('mark.obre@keydiscoveryinc.com');
							$bcc4 = ('socon.agentbridge@gmail.com');
							$mailSender =& JFactory::getMailer();
							$mailSender ->addRecipient( $email );
							$mailSender ->addCC( $assistant_email );
							$mailSender ->addBCC( $bcc2 );
							$mailSender ->addBCC( $bcc3 );
							$mailSender ->addBCC( $bcc4 );
							$mailSender ->setSender( $sender );
							$mailSender ->setSubject( $subject );
							$mailSender ->isHTML(  true );
							$mailSender ->setBody(  $body );
							$mailSender ->Encoding = 'base64';
							$mailSender ->wordWrap = 50;
							if ($mailSender ->Send()) {
								JFactory::getApplication()->enqueueMessage('An Email has been set to ' . $results[0]->email . "...");
							} else {
								JError::raiseWarning( 100, 'An error occured while sending an email to ' .  $results[0]->email );
							}
							$model->updateActivationDate($results[0]->email);
						endforeach;
					}
					catch ( Exception $e ) {
						// Catch the error.
					}
				break;
			case 'download':
				$this->generateUserCSV();
				break;
			case 'generateUserSalesCSV':
				$this->generateUserSalesCSV();
				break;
		endswitch;
        parent::display();
    }
	function generateUserCSV(){
		  $model = $this->getModel($this->getName());
	      $rows = $model->getUsersForCSV();
	      date_default_timezone_set('America/Los_Angeles');
	      $time = time();      
	      ## If the query doesn't work..
	      if (!$rows){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      $fields = (array_keys($rows[0]));
	      ## Count all fields(will be the collumns
	      $columns = count($fields);
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	      	if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	} else if($fields[$i]=="is_premium"){
	      		$data .= "Charter".$sep;
	      	} else if($fields[$i]=="sendEmail_date"){
	      		$data .= "Date Email Activation sent".$sep;
	      	} else if($fields[$i]=="activation_date"){
	      		$data .= "Date User Activated link".$sep;
	      	} else if($fields[$i]=="completeReg_date"){
	      		$data .= "Date Terms Accepted".$sep;
	      	} else if($fields[$i]=="registration_date"){
	      		$data .= "Date added to Database".$sep;
	      	} else if($fields[$i]=="registerDate"){
	      		$data .= "Date of Password Setup".$sep;
	      	} else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      $data .= "\n";
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="is_premium"){
	         	 $value = $value == 1 ? "Yes" : "No";  
	         	} elseif($key=="zip_code"){
					$value = str_replace(array("\\","/",","), "-",  $value);
	         	} elseif($key==sendEmail_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==activation_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==completeReg_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==registration_date){
					if ($value!="0000-00-00 00:00:00") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 }
				} elseif($key==registerDate){
					 if ($value!="") {
					 $value = date("m/d/Y g:i:s A", strtotime($value)-28800);
					 } else {
					 $value="0000-00-00 00:00:00";
					 }
				} elseif($key==is_term_accepted){
					 if ($value==1) {
					 $value = "Yes";
					 } else {
					 $value="No";
					 }
				}else
	         	 $value = str_replace(array("\\","/",","), "",  $value);
	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      $data = str_replace("\r","",$data);
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      ## Push the report now!
	      $this->name = 'User-Registration-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	}		
	function createthumb() {
		//$fullpath = $_REQUEST['fullpath'];
		$src = $_REQUEST['src'];
		$imagename = $this->stripImageName($src);
		$filepath = '../uploads/'.$imagename;
		$geetimagesize = getimagesize($filepath);
		$mime_type = $geetimagesize['mime'];
		$width = $geetimagesize[0];
		$height = $geetimagesize[1];
		$targ_w = $geetimagesize[0];
		$targ_h = $geetimagesize[1];
		$thumbheight = round(60*$height/$width);
		switch($mime_type) {
			case "image/jpg":
			case "image/jpeg":
				$jpeg_quality = 90;
				$img_r = imagecreatefromjpeg($filepath);
				$images_fin = imagecreatetruecolor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$x1,$y1,$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagejpeg($dst_r,$destImage,$jpeg_quality);
				imagedestroy($dst_r);	
				$image_p = imagecreatetruecolor(60, $thumbheight);
				$image = imagecreatefromjpeg($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, $thumbheight, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagejpeg($image_p,$destImageT,$jpeg_quality);
				imagedestroy($image_p);
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;			
			case "image/png":				
				$png_quality = 9;
				$img_r = imagecreatefrompng($filepath);
				$images_fin = ImageCreateTrueColor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagepng($dst_r,$destImage,$png_quality);
				imagedestroy($dst_r);
				$image_p = imagecreatetruecolor(60, $thumbheight);
				$image = imagecreatefrompng($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, $thumbheight, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagepng($image_p,$destImageT,$png_quality);
				imagedestroy($image_p);
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;			
			case "image/gif":
				$img_r = imagecreatefromgif($filepath);
				$images_fin = ImageCreateTrueColor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagegif($dst_r,$destImage);
				imagedestroy($dst_r);
				$image_p = imagecreatetruecolor(60, $thumbheight);
				$image = imagecreatefromgif($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, $thumbheight, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagegif($image_p,$destImageT);
				imagedestroy($image_p);								
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;		
		}		
		exit;
	}
	function afteraddpop() {
		$application = JFactory::getApplication();
		$application->redirect(JRoute::_("index.php?option=com_useractivation").'&task=edit&uid='.$_POST['cid'][0]);		
	}
	function crop() {		
		if ($_REQUEST['x2']==$_REQUEST['x1'] || ($_REQUEST['x2']=='' && $_REQUEST['x1']=='') ) {
			$x1=0;
			$x2=$_REQUEST['origwidth'];
			$y1=0;			
			$y2=$_REQUEST['origheight'];
		} else {
			$x1=$_REQUEST['x1'];
			$x2=$_REQUEST['x2'];
			$y1=$_REQUEST['y1'];
			$y2=$_REQUEST['y2'];
		}
		$targ_w = $x2-$x1;
		$targ_h = $y2-$y1;
		$width = $_REQUEST['origwidth'];
		$height = $_REQUEST['origheight'];
		$jpeg_quality = 90;
		$src = $_REQUEST['src'];
		if($_REQUEST['resized']){
			if($_REQUEST['resized']!="mobile"){
				if($_REQUEST['origwidth'] > $_REQUEST['origheight']) {
					$width=800;
					$height=round($width*$_REQUEST['origheight']/$_REQUEST['origwidth']);
				} else {
					$height=800;
					$width=round($height*$_REQUEST['origwidth']/$_REQUEST['origheight']);
				}
			} else {
				if($_REQUEST['origwidth'] > $_REQUEST['origheight']){
					$width=$_REQUEST['origwidth'];
					$height=round($width*$_REQUEST['origheight']/$_REQUEST['origwidth']);
				} else {
					$height=$_REQUEST['origheight'];
					$width=round($height*$_REQUEST['origwidth']/$_REQUEST['origheight']);
				}		
			}		
		}
		$imagename = $this->stripImageName($src);
		$filepath = '../uploads/'.$imagename;
		$geetimagesize = getimagesize($filepath);
		$mime_type = $geetimagesize['mime'];
		switch($mime_type) {
			case "image/jpg":
			case "image/jpeg":
				$jpeg_quality = 90;
				$img_r = imagecreatefromjpeg($filepath);
				$images_fin = imagecreatetruecolor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$x1,$y1,$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagejpeg($dst_r,$destImage,$jpeg_quality);
				imagedestroy($dst_r);								
				$image_p = imagecreatetruecolor(60, 71);
				$image = imagecreatefromjpeg($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagejpeg($image_p,$destImageT,$jpeg_quality);
				imagedestroy($image_p);
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;			
			case "image/png":				
				$png_quality = 9;
				$img_r = imagecreatefrompng($filepath);
				$images_fin = ImageCreateTrueColor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagepng($dst_r,$destImage,$png_quality);
				imagedestroy($dst_r);
				$image_p = imagecreatetruecolor(60, 71);
				$image = imagecreatefrompng($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagepng($image_p,$destImageT,$png_quality);
				imagedestroy($image_p);
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;			
			case "image/gif":
				$img_r = imagecreatefromgif($filepath);
				$images_fin = ImageCreateTrueColor($width, $height);
				$xdaw = ImagesX($img_r);
				$ydaw = ImagesY($img_r);
				ImageCopyResampled($images_fin, $img_r, 0, 0, 0, 0, $width+1, $height+1, $xdaw, $ydaw);
				$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
				imagecopyresampled($dst_r,$images_fin,0,0,$_REQUEST['x1'],$_REQUEST['y1'],$targ_w,$targ_h,$targ_w,$targ_h);
				$base = new JConfig();
				$time = str_replace('.', '-', microtime(true));
				$destImage = $base->uploadsDir."/".JFactory::getUser()->id.$time."-".basename($src);
				imagegif($dst_r,$destImage);
				imagedestroy($dst_r);
				$image_p = imagecreatetruecolor(60, 71);
				$image = imagecreatefromgif($destImage);
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, 60, 71, $targ_w, $targ_h);
				$destImageT = $base->uploadsDir."/thumb_".JFactory::getUser()->id.$time."-".basename($src);
				imagegif($image_p,$destImageT);
				imagedestroy($image_p);								
				echo str_replace("/administrator", "", JUri::base())."uploads/".JFactory::getUser()->id.$time."-".basename($src);
				break;		
		}		
		exit;			
	}		
	function stripImageName($src) {		
		$url = urldecode($src);		
		$image_name = (stristr($url,'?',true))?stristr($url,'?',true):$url;
		$pos = strrpos($image_name,'/');
		$image_name = substr($image_name,$pos+1);
		$extension = stristr($image_name,'.');
		return $image_name;	
	}
	public function getSubPropertyTypes(){
		$proptype_id = $_GET['prop_id'];
		$model = $this->getModel($this->getName());
		$sptypes = $model->getAllSubPropTypes($proptype_id);
		echo json_encode($sptypes);
	}
	function subtype(){
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
		$ptype = $_GET['ptype'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('st.sub_id', 'st.name'));
		$query->from('#__property_sub_type st');
		$query->where('pt.type_name LIKE \''.$ptype.'\'');
		$query->leftJoin('#__property_type pt ON pt.type_id = st.property_id');
		$db->setQuery($query);
		$subtypes = $db->loadObjectList();
		foreach($subtypes as $key => $subtype) {
			$subtypes[$key]->name_translated = JText::_($subtype->name);
		}
		echo json_encode($subtypes);
		die();
	}
	function changeCountryData(){
		$country = $_POST['country'];
		$datas = $this->getCountryDataByID($country);
		echo json_encode($datas);
	}
	function getCountryDataByID($country){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("
			SELECT cl.*,curr.*,clv.* FROM #__country_languages cla LEFT JOIN #__country_validations clv ON clv.country = cla.country LEFT JOIN #__countries cl ON cla.country = cl.countries_id   LEFT JOIN #__country_currency AS curr ON curr.country = cl.countries_id WHERE cl.countries_id = ".$country." ORDER BY cl.countries_name");
		$db->setQuery($query);		
		return $db->loadObject();
	}
	function array_search_partial($arr,$keyword) { 
	     for ($i = 0; $i < count($arr); $i++) {
	     	$z_check_len = strlen(trim($arr[$i]));
	     	if(substr($keyword, 0, $z_check_len) == trim($arr[$i])){
	     		return true;
	     	}
			//if(strpos(strtolower($keyword),strtolower(trim($arr[$i]))) !== FALSE){
	        //    return true;
			//} 
		 }
		return false;
	}
	public function zipCheck(){
		$ZIPREG=array(
			"US"=>"^\d{5}([\-]?\d{4})?$",
			"UK"=>"^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$",
			"DE"=>"\b((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))\b",
			"CA"=>"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$",
			"FR"=>"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$",
			"IT"=>"^(V-|I-)?[0-9]{5}$",
			"AU"=>"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$",
			"IE"=>"^(A|[C-F]|H|K|N|P|R|T|[V-Y])([0-9])([0-9]|W)( )?([0-9]|A|[C-F]|H|K|N|P|R|T|[V-Y]){4}$",
			"NL"=>"^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$",
			"ES"=>"^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$",
			"DK"=>"^([D-d][K-k])?( |-)?[1-9]{1}[0-9]{3}$",
			"SE"=>"^(s-|S-){0,1}[0-9]{3}\s?[0-9]{2}$",
			"BE"=>"^[1-9]{1}[0-9]{3}$"
		);
		/*$country_code="US";
		$zip_postal="11111";		 
		if ($ZIPREG[$country_code]) {
			if (!preg_match("/".$ZIPREG[$country_code]."/i",$zip_postal)){
				//Validation failed, provided zip/postal code is not valid.
			} else {
				//Validation passed, provided zip/postal code is valid.
			}
		} else {
			//Validation not available
		}*/
		$country = $_POST['country'];
		$zip = $_POST['zip'];
		$valid_return_value = 'valid';
		$invalid_return_value = 'invalid';
		if($country==223){
			$length = strlen($zip);
			if($zip<5){
				echo $invalid_return_value;
			    die();
			} else {
				echo $valid_return_value;
			    die();
			}
		} else if($country==38) {
			if (!preg_match("/".$ZIPREG["CA"]."/i",$zip)){
				//Validation failed, provided zip/postal code is not valid.
				echo $invalid_return_value;
			    die();
			} else {
				//Validation passed, provided zip/postal code is valid.
				echo $valid_return_value;
			    die();
			}
		} else if($country==13) {
			echo $valid_return_value;
			die();
		} else if($country==222) {
			    // Start config
				$string = $zip;			    
			    $exceptions = array('BS981TL', 'BX11LT', 'BX21LB', 'BX32BB', 'BX55AT', 'CF101BH', 'CF991NA', 'DE993GG', 'DH981BT', 'DH991NS', 'E161XL', 'E202AQ', 'E202BB', 'E202ST', 'E203BS', 'E203EL', 'E203ET', 'E203HB', 'E203HY', 'E981SN', 'E981ST', 'E981TT', 'EC2N2DB', 'EC4Y0HQ', 'EH991SP', 'G581SB', 'GIR0AA', 'IV212LR', 'L304GB', 'LS981FD', 'N19GU', 'N811ER', 'NG801EH', 'NG801LH', 'NG801RH', 'NG801TH', 'SE18UJ', 'SN381NW', 'SW1A0AA', 'SW1A0PW', 'SW1A1AA', 'SW1A2AA', 'SW1P3EU', 'SW1W0DT', 'TW89GS', 'W1A1AA', 'W1D4FA', 'W1N4DJ');
			    // Add Overseas territories ?
			    array_push($exceptions, 'AI-2640', 'ASCN1ZZ', 'STHL1ZZ', 'TDCU1ZZ', 'BBND1ZZ', 'BIQQ1ZZ', 'FIQQ1ZZ', 'GX111AA', 'PCRN1ZZ', 'SIQQ1ZZ', 'TKCA1ZZ');
			    // End config
			    $string = strtoupper(preg_replace('/\s/', '', $string)); // Remove the spaces and convert to uppercase.
			    $exceptions = array_flip($exceptions);
			    if(isset($exceptions[$string])){return $valid_return_value;} // Check for valid exception
			    $length = strlen($string);
			    if($length < 5 || $length > 7){return $invalid_return_value;} // Check for invalid length
			    $letters = array_flip(range('A', 'Z')); // An array of letters as keys
			    $numbers = array_flip(range(0, 9)); // An array of numbers as keys
			    switch($length){
			        case 7:
			            if(!isset($letters[$string[0]], $letters[$string[1]], $numbers[$string[2]], $numbers[$string[4]], $letters[$string[5]], $letters[$string[6]])){break;}
			            if(isset($letters[$string[3]]) || isset($numbers[$string[3]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			        case 6:
			            if(!isset($letters[$string[0]], $numbers[$string[3]], $letters[$string[4]], $letters[$string[5]])){break;}
			            if(isset($letters[$string[1]], $numbers[$string[2]]) || isset($numbers[$string[1]], $letters[$string[2]]) || isset($numbers[$string[1]], $numbers[$string[2]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			        case 5:
			            if(isset($letters[$string[0]], $numbers[$string[1]], $numbers[$string[2]], $letters[$string[3]], $letters[$string[4]])){
			                echo $valid_return_value;
			                die();
			            }
			        break;
			    }
			    echo $invalid_return_value;
			    die();
		} else if($country==103){
			if (!preg_match("/".$ZIPREG["IE"]."/i",$zip)){
				//Validation failed, provided zip/postal code is not valid.
				echo $invalid_return_value;
			    die();
			} else {
				//Validation passed, provided zip/postal code is valid.
				echo $valid_return_value;
			    die();
			}
			die();
		}
	}
	function getsubpropid() {
		$subtype = $_POST['stype'];
		$poptype = $_POST['ptype'];
		$model = $this->getModel('UserActivation');
		$_subtype = $model->getPropSubTypeByName($subtype, $poptype);
		$stype = $_subtype[0]->sub_id;
		$ptype = $_subtype[0]->property_id;
		$arr_return = array(
			"stype" => $stype,
			"ptype" => $ptype
		);
		echo json_encode($arr_return); exit;
	}
	
	/*
	function form(){
		$defsqft = "Sq. Ft.";	
		if(isset($_GET['country'])){
			if($_GET['country']==13){
				//$defsqft = "square meters";
			} else if($_GET['country']==103){
				//$defsqft = "m²";
			}
		}
		$dataArray = json_decode(json_encode($_POST['data']));
		$data = $dataArray[0];
		$subtype = $_POST['stype'];
		$poptype = $_POST['ptype'];
		$model = $this->getModel('UserActivation');
		$_subtype = $model->getPropSubTypeByName($subtype, $poptype);
		$stype = $_subtype[0]->sub_id;
		?>
<div
	class="pocket-form">
	<div 
		id="prop_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2>Features</h2></div>
	<div 
		id="buyer_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your buyer needs. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2>Minimum Features</h2></div>
	<?php
	switch ($stype){
		// RESIDENTIAL PURCHASE + SFR
		case 1:
			?>
	<div class="left ys push-top2">
		<label>Bedrooms</label>
			<select
				id="jform_bedroom"
				name="bedroom" class="reqfield">
				<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option <?php echo ($data->bedroom=="6+") ? "selected":""?>value="6+">6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label>
		<select
			id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label>
			<select id="jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label> <select id="jform_condition"
			name="condition">
			<option value="">--- Select ---</option>
			<option value="Fixer">Fixer</option>
			<option value="Good">Good</option>
			<option value="Excellent">Excellent</option>
			<option value="Remodeled">Remodeled</option>
			<option value="New Construction">New Construction</option>
			<option value="Under Construction">Under Construction</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features3"
			name="features3">
			<option value="">--- Select ---</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + CONDO
case 2:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_unitsqft"
			name="unitsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_unitsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<!---------------------------------------------------------------- -->
        <div class="clear-float"></div>
    	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
		<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="jform_bldgtype"
			name="bldgtype">
			<option value="">--- Select ---</option>
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="jform_style" style="width:200px "name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE
case 3:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="jform_bedroom" required="required"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="unitsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    	<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="jform_bldgtype"
			name="bldgtype">
			<option value="">--- Select ---</option>
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Detached">Detached</option>
			<option value="Attached">Attached</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div style="clear: both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + LAND
case 4:
	?>
	<div class="left ys push-top2">
		<label>Lot Size</label> <select id="jform_lotsize" required="required"
			name="lotsize" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsize error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label> <select
		class="reqfield"
		id="jform_zoned"
		name="zoned">
			<option value="">--- Select ---</option>
			<option value="1">1 Unit</option>
			<option value="2">2 Units</option>
			<option value="3-4">3-4 Units</option>
			<option value="5-20">5-20 Units</option>
			<option value="20">20+ Units</option>
		</select>
		<p class="jform_zoned error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + SFR
case 5:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2" >
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="jform_term" name="term" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_term error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="jform_possession"
			name="possession" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
		<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys" style="margin-top: 30px;">
		<label>Pet</label> <a href="javascript: void(0)"
			style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1">Yes</a> <a
			href="javascript: void(0)" class="left gradient-gray no pet" rel="0">No</a>
		<input type="hidden" value="1" id="jform_pet" name="pet"
			class="text-input reqfield" />
	</div>
    <div class="left ys" style="margin-top: 30px;">
		<label>Furnished</label> <a href="javascript: void(0)"
			style="margin-right:1px" class="left gradient-gray no furnished" rel="1" value="1">Yes</a> <a
			href="javascript: void(0)" class="left gradient-blue-toggle yes furnished"
			rel="0" value="0">No</a> <input type="hidden" value="0" id="jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label> <select id="jform_condition"
			name="condition">
			<option value="">--- Select ---</option>
			<option value="Fixer">Fixer</option>
			<option value="Good">Good</option>
			<option value="Excellent">Excellent</option>
			<option value="Remodeled">Remodeled</option>
			<option value="New Construction">New Construction</option>
			<option value="Under Construction">Under Construction</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + Condo
case 6:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="unitsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="jform_term" name="term" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
		<p class="jform_term error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    <div class="left ys" style="margin-top: 30px;">
		<label>Furnished</label> <a href="javascript: void(0)"
			style="margin-right:1px" class="left gradient-gray no furnished" rel="1">Yes</a> <a
			href="javascript: void(0)" class="left gradient-blue-toggle yes furnished"
			rel="0">No</a> <input type="hidden" value="0" id="jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px;">
		<label>Pet</label> <a href="javascript: void(0)"
			style="margin-right:1px" class="left gradient-blue-toggle yes pet" rel="1">Yes</a> <a
			href="javascript: void(0)" class="left gradient-gray no pet" rel="0">No</a>
		<input type="hidden" value="1" id="jform_pet" name="pet"
			class="text-input reqfield" />
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="jform_possession"
			name="possession">
			<option value="">--- Select ---</option>
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="jform_bldgtype"
			name="bldgtype">
			<option value="">--- Select ---</option>
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Townhouse/Row House
case 7:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bedroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					if($i==$data->bathroom) $selected = "selected";
					else $selected = "";
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+">6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="unitsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
             <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="jform_term" name="term" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
			<p class="jform_term error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
		<div class="left ys" style="margin-top: 30px;">
		<label>Furnished</label> <a href="javascript: void(0)"
			style="margin-right:1px" class="left gradient-gray no furnished" rel="1">Yes</a> <a
			href="javascript: void(0)" class="left gradient-blue-toggle yes furnished"
			rel="0">No</a> <input onchange="get_pet_furnished" type="hidden" value="0" id="jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px;">
		<label>Pet</label>
        <a href="javascript: void(0)"
			style="margin-right:1px" id="yes" class="left gradient-blue-toggle yes pet" rel="1">Yes</a>
        <a
			href="javascript: void(0)" id="no" class="left gradient-gray no pet" rel="0">No</a>
		<input onchange="get_pet_furnished" type="hidden" value="2" id="jform_pet" name="pet"
			class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="jform_possession"
			name="possession">
			<option value="">--- Select ---</option>
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
		<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="jform_bldgtype"
			name="bldgtype">
			<option value="">--- Select ---</option>
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Detached">Detached</option>
			<option value="Attached">Attached</option>
		</select>
		<p class="jform_bldgtype error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div style="clear: both"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="jform_poolspa"
			name="poolspa">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
			for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="8+">8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Land
case 8:
	?>
	<div class="left ys push-top2">
		<label>Lot Size</label> <select id="jform_lotsize"
			name="lotsize" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_lotsize error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label> <select id="jform_zoned" name="zoned" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1">1 Unit</option>
			<option value="2">2 Units</option>
			<option value="3-4">3-4 Units</option>
			<option value="5-20">5-20 Units</option>
			<option value="20">20+ Units</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_zoned error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<div style="clear: both"></div>
		<div>
			<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
		</div>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Multi Family
case 9:
	?>
	<div class="left ys push-top2">
		<label>Units</label> <select required="required" id="jform_units" name="units" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="2">Duplex</option>
			<option value="3">TriPlex</option>
			<option value="4">Quad</option>
			<option value="5-9">5-9</option>
			<option value="10-15">10-15</option>
			<option value="16-29">16-29</option>
			<option value="30-50">30-50</option>
			<option value="50-100">50-100</option>
			<option value="101-150">101-150</option>
			<option value="151-250">151-250</option>
			<option value="251">251+</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_units error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label>
			<select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>GRM</label> <select id="jform_grm" name="grm" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0">Not Disclosed</option>
			<option value="1-2">1-2</option>
			<option value="3-4">3-4</option>
			<option value="4-5">4-5</option>
			<option value="5-6">5-6</option>
			<option value="6-7">6-7</option>
			<option value="7-8">7-8</option>
			<option value="8-9">8-9</option>
			<option value="9-10">9-10</option>
			<option value="10-11">10-11</option>
			<option value="11-12">11-12</option>
			<option value="12-13">12-13</option>
			<option value="13-14">13-14</option>
			<option value="14-15">14-15</option>
			<option value="15-16">15-16</option>
			<option value="16-17">16-17</option>
			<option value="17-18">17-18</option>
			<option value="18-19">18-19</option>
			<option value="19-20">19-20</option>
			<option value="20">20+</option>
		</select>	
		<p class="jform_grm error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    <div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft" name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
			<option value="Land Only">Land Only</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft" name="lotsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="jform_view" name="view">
			<option value="">--- Select ---</option>
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
		<p class="jform_view error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    <!---------------------------------------------------------------- -->
    <div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24x">
		<label>Style</label> 
		<select id="jform_style" style="width:200px" name="style">
			<option value="">--- Select ---</option>
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Architecturally Significant">Architecturally Significant</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Rent Control">Rent Control</option>
			<option value="Senior">Senior</option>
			<option value="Assoc-Pool">Assoc-Pool</option>
			<option value="Assoc-Spa">Assoc-Spa</option>
			<option value="Assoc-Tennis">Assoc-Tennis</option>
			<option value="Assoc-Other">Assoc-Other</option>
			<option value="Section 8">Section 8</option>
			<option value="25% Occupied">25% Occupied</option>
			<option value="50% Occupied">50% Occupied</option>
			<option value="75% Occupied">75% Occupied</option>
			<option value="100% Occupied">100% Occupied</option>
			<option value="Cash Cow">Cash Cow</option>
			<option value="Value Add">Senior</option>
			<option value="Senior">Value Add</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Rent Control">Rent Control</option>
			<option value="Senior">Senior</option>
			<option value="Assoc-Pool">Assoc-Pool</option>
			<option value="Assoc-Spa">Assoc-Spa</option>
			<option value="Assoc-Tennis">Assoc-Tennis</option>
			<option value="Assoc-Other">Assoc-Other</option>
			<option value="Section 8">Section 8</option>
			<option value="25% Occupied">25% Occupied</option>
			<option value="50% Occupied">50% Occupied</option>
			<option value="75% Occupied">75% Occupied</option>
			<option value="100% Occupied">100% Occupied</option>
			<option value="Cash Cow">Cash Cow</option>
			<option value="Value Add">Senior</option>
			<option value="Senior">Value Add</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Office
case 10:
	?>
	<div class="left ys push-top2">
		<label>Type</label>
			<select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Office">Office</option>
			<option value="Institutional">Institutional</option>
			<option value="Medical">Medical</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Condo">Condo</option>
			<option value="R&D">R&D</option>
			<option value="Business Park">Business Park</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Class</label>
			<select id="jform_class" name="class" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="4">A</option>
			<option value="3">B</option>
			<option value="2">C</option>
			<option value="1">D</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_class error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft" name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Industrial
case 11:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Flex Space">Flex Space</option>
			<option value="Business Park">Business Park</option>
			<option value="Condo">Condo</option>
			<option value="Land">Land</option>
			<option value="Manufacturing">Manufacturing</option>
			<option value="Office Showroom">Office Showroom</option>
			<option value="R&D">R&D</option>
			<option value="Self/Mini Storage">Self/Mini Storage</option>
			<option value="Truck Terminal/Hub">Truck Terminal/Hub</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Distribution">Distribution</option>
			<option value="Cold Storage">Cold Storage</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none">This field is required</p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft" name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Ceiling Height</label> <select id="jform_ceiling"
			name="ceiling">
			<option value="">--- Select ---</option>
			<?php
			for($i=12; $i<=34; $i=$i+2){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Retail
case 12:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Community Center">Community Center</option>
			<option value="Strip Center">Strip Center</option>
			<option value="Outlet Center">Outlet Center</option>
			<option value="Power Center">Power Center</option>
			<option value="Anchor">Anchor</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Service Station">Service Station</option>
			<option value="Retail Pad">Retail Pad</option>
			<option value="Free Standing">Free Standing</option>
			<option value="Day Care/Nursery">Day Care/Nursery</option>
			<option value="Post Office">Post Office</option>
			<option value="Vehicle">Vehicle</option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft" name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";
				}
				?>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Motel/Hotel
case 13:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Economy">Economy</option>
			<option value="Full Service">Full Service</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label> <select id="jform_roomcount" name="roomcount" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-9">1-9</option>
			<option value="10-19">10-19</option>
			<option value="20-29">20-29</option>
			<option value="30-39">30-39</option>
			<option value="40-49">40-49</option>
			<option value="50-99">50-99</option>
			<option value="100-149">100-149</option>
			<option value="150-199">150-199</option>
			<option value="200">200+</option>
		</select>
		<p class="jform_roomcount error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="bldgsqft">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Bar">Bar</option>
			<option value="Pool">Pool</option>
			<option value="Banquet Room">Banquet Room</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Bar">Bar</option>
			<option value="Pool">Pool</option>
			<option value="Banquet Room">Banquet Room</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Assisted Care
case 14:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Assisted">Assisted</option>
			<option value="Acute Care">Acute Care</option>
			<option value="Land">Land</option>
		</select>
		<p style="display:none" class="jform_type error_msg"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p style="display:none" class="jform_cap error_msg"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label> <select id="jform_roomcount" name="roomcount" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-9">1-9</option>
			<option value="10-19">10-19</option>
			<option value="20-29">20-29</option>
			<option value="30-39">30-39</option>
			<option value="40-49">40-49</option>
			<option value="50-99">50-99</option>
			<option value="100-149">100-149</option>
			<option value="150-199">150-199</option>
			<option value="200">200+</option>
		</select>
		<p style="display:none" class="jform_roomcount error_msg"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select
			id="jform_bldgsqft"
			name="bldgsqft">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p style="display:none" class="jform_bldgsqft error_msg"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Special Purpose
case 15:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Golf">Golf</option>
			<option value="Marina">Marina</option>
			<option value="Theater">Theater</option>
			<option value="Religious">Religious</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="jform_cap" name="cap" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + OFFICE
case 16:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Office">Office</option>
			<option value="Institutional">Institutional</option>
			<option value="Medical">Medical</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Condo">Condo</option>
			<option value="R&D">R&D</option>
			<option value="Business Park">Business Park</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Class</label> <select id="jform_class" name="class" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="4">A</option>
			<option value="3">B</option>
			<option value="2">C</option>
			<option value="1">D</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_class error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Available <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_available" name="available" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
            <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_available error_msg" style="display:none;">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="jform_typelease" name="typelease" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="bldgsqft">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
        <p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div style="clear:both"><div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + INDUSTRIAL
case 17:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Flex Space">Flex Space</option>
			<option value="Business Park">Business Park</option>
			<option value="Condo">Condo</option>
			<option value="Manufacturing">Manufacturing</option>
			<option value="Office Showroom">Office Showroom</option>
			<option value="R&D">R&D</option>
			<option value="Truck Terminal/Hub">Truck Terminal/Hub</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Distribution">Distribution</option>
			<option value="Cold Storage">Cold Storage</option>
			<option value="Land">Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="jform_typelease" name="typelease" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Available <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_available" name="available" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_available error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft" name="lotsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft" name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500-2,999">2,500-2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
             <option value="5,000-7,499">5,000-7,499</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Ceiling Height</label> <select id="jform_ceiling"
			name="ceiling">
			<option value="">--- Select ---</option>
			<?php
			for($i=12; $i<=34; $i=$i+2){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + RETAIL
case 18:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="jform_type" name="type" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="Community Center">Community Center</option>
			<option value="Strip Center">Strip Center</option>
			<option value="Outlet Center">Outlet Center</option>
			<option value="Power Center">Power Center</option>
			<option value="Anchor">Anchor</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Service Station">Service Station</option>
			<option value="Retail Pad">Retail Pad</option>
			<option value="Free Standing">Free Standing</option>
			<option value="Day Care/Nursery">Day Care/Nursery</option>
			<option value="Post Office ">Post Office</option>
			<option value="Vehicle">Vehicle</option>
		</select>
		<p class="jform_type error_msg" style="display:none;">This field is required </p>
	</div>	
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="1-999">1-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
            <option value="150,000-249,000">150,000-249,000</option>
            <option value="250,000-499,000">250,000-499,000</option>
			<option value="500,000">500,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="jform_typelease" name="typelease" class="reqfield">
			<option value="">--- Select ---</option>
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
				?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="jform_lotsqft"
			name="lotsqft">
			<option value="">--- Select ---</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="jform_yearbuilt"
			name="yearbuilt">
			<option value="">--- Select ---</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="< 1919">< 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<option value="">--- Select ---</option>
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features1"
			name="features1">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="jform_features2"
			name="features2">
			<option value="">--- Select ---</option>
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	}
	?>
</div>
<?php
die();
	}
*/

function form(){
	    $user = JFactory::getUser();	   
		$session = JFactory::getSession();
		$curr_user = $user->id ? $user->id : $_GET["user_id"];
		$session->set('user', new JUser($curr_user));
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language_tag = JFactory::getUser()->currLanguage;
		if(!$language_tag) $language_tag = "english-US";
        $language->load($extension, $base_dir, $language_tag, true);
		$defsqft = "Sq. Ft.";	
		//$model = $this->getModel($this->getName());
		//$getMeasurement = $model->getSqMeasureByCountry($_GET['country']);
		
		$subtype = $_POST['stype'];
		$poptype = $_POST['ptype'];
		$model = $this->getModel('UserActivation');
		$propSubType = $model->getPropSubTypeByName($subtype, $poptype);
		$_subtype = (isset($_POST['data'])) ? $subtype : $propSubType[0]->sub_id;
		$getMeasurement = $model->getSqMeasureByCountry($_GET['country']);
		
		/*if(isset($_GET['country'])){
			if($_GET['country']==13){
				$defsqft = "square meters";
			} else if($_GET['country']==103){
				$defsqft = "m²";
			}
		}*/

		if($getMeasurement['sqmeasure'] == 'sq. ft.'){
			$defsqft = JText::_('COM_POPS_TERMS_BLDG');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE');
		} else {
			$defsqft = JText::_('COM_POPS_TERMS_BLDG_M');
			$defsqft_a = JText::_('COM_POPS_TERMS_AVAIL_M');
			$defsqft_u = JText::_('COM_POPS_TERMS_UNITSQ_M');
			$defsqft_l = JText::_('COM_POPS_TERMS_LOT_M');
			$defsqft_ls = JText::_('COM_POPS_TERMS_LOT_SIZE_M');
		}
		
		$dataArray = json_decode(json_encode($_POST['data']));
		$data = isset($_POST['data']) ? $_POST['data'] : $dataArray[0];
		$isEdit = isset($_POST['data']);
		?>
<div
	class="pocket-form">
	<div 
		id="prop_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_FEATURES') ?></h2></div>
	<div 
		id="buyer_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your buyer needs. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2><?php echo JText::_('COM_POPS_TERMS_MINIMUM_FEATURES') ?></h2></div>
	<?php
	switch ($_subtype){

		// RESIDENTIAL PURCHASE + SFR
		case 1:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield", $isEdit);
			echo $model->getFieldsView("required reqfield", $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldsCondition('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsStyle('', $isEdit);
			echo $model->getFieldsGarage('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsFeatures("features1", '', $isEdit);
			echo $model->getFieldsFeatures("features2", '', $isEdit);
			echo $model->getFieldsFeatures("features3", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + CONDO
		case 2:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsView('', $isEdit);
			echo $model->getFieldsBldgType('', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsGarage('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsCondoFeatures("features1", '', $isEdit);
			echo $model->getFieldsCondoFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE
		case 3:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield", $isEdit);
			echo $model->getFieldsView('', $isEdit);
			echo $model->getFieldsBldgType('', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsGarage('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsCondoFeatures("features1", '', $isEdit);
			echo $model->getFieldsCondoFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL PURCHASE + LAND
		case 4:
			echo $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield", $isEdit);
			echo $model->getFieldsView("required reqfield", $isEdit);
			echo $model->getFieldsZoned("required reqfield", $isEdit);
			echo $model->getFieldsLandFeatures("features1", '', $isEdit);
			echo $model->getFieldsLandFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + SFR
		case 5:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsTerm("required reqfield", $isEdit);
			echo $model->getFieldsTermPossesion("required reqfield", $isEdit);
			echo $model->getFieldsPet('', $isEdit);
			echo $model->getFieldsFurnished('', $isEdit);	
			echo '<div style="clear:both"></div>';		
			echo $model->getFieldsView('', $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsCondition('', $isEdit);
			echo $model->getFieldsGarage('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsYearBuilt();
			echo $model->getFieldsFeatures("features1", '', $isEdit);
			echo $model->getFieldsFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Condo
		case 6:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield", $isEdit);
			echo $model->getFieldsTerm("required reqfield", $isEdit);
			echo $model->getFieldsPet('', $isEdit);
			echo $model->getFieldsFurnished('', $isEdit);	
			echo $model->getFieldsView('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsTermPossesion('', $isEdit);
			echo $model->getFieldsBldgType('', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsGarage('', $isEdit);
			echo $model->getFieldsCondoFeatures("features1", '', $isEdit);
			echo $model->getFieldsCondoFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Townhouse/Row House
		case 7:
			echo $model->getFieldsBedrooms("required reqfield", $isEdit);
			echo $model->getFieldsBathrooms("required reqfield", $isEdit);
			echo $model->getFieldsUnitSqft($getMeasurement,$defsqft_u,"required reqfield", $isEdit);
			echo $model->getFieldsTerm("required reqfield", $isEdit);
			echo $model->getFieldsPet('', $isEdit);
			echo $model->getFieldsFurnished('', $isEdit);	
			echo $model->getFieldsView('', $isEdit);
			echo $model->getFieldsTermPossesion('', $isEdit);
			echo $model->getFieldsBldgType('', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo $model->getFieldsPoolSpa('', $isEdit);
			echo $model->getFieldsGarage('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsCondoFeatures("features1", '', $isEdit);
			echo $model->getFieldsCondoFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// RESIDENTIAL LEASE + Land
		case 8:
			echo $model->getFieldLotSize($getMeasurement,$defsqft_ls,"required reqfield", $isEdit);
			echo $model->getFieldsView("required reqfield", $isEdit);
			echo $model->getFieldsZoned("required reqfield", $isEdit);
			echo $model->getFieldsLandFeatures("features1", '', $isEdit);
			echo $model->getFieldsLandFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Multi Family
		case 9:
			echo $model->getFieldsTermUnits("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsTermGRM("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"","required reqfield", $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield", $isEdit);
			echo $model->getFieldsView('', $isEdit);
			echo $model->getFieldsStyle('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldOccupancy('', $isEdit);
			echo $model->getFieldsMultiFamFeatures("features1", '', $isEdit);
			echo $model->getFieldsMultiFamFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;	
		// COMMERCIAL + Office
		case 10:
			echo $model->getFieldOfficeType("required reqfield", $isEdit);
			echo $model->getFieldOfficeClass("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield", $isEdit);			
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldOccupancy('', $isEdit);
			echo $model->getFieldsOfficeFeatures("features1", '', $isEdit);
			echo $model->getFieldsOfficeFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Industrial
		case 11:
			echo $model->getFieldsIndusType("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldCeiling('', $isEdit);
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldOccupancy('', $isEdit);
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo $model->getFieldsIndusFeatures("features1", '', $isEdit);
			echo $model->getFieldsIndusFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Retail
		case 12:
			echo $model->getFieldsRetailType("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo $model->getFieldOccupancy('', $isEdit);
			echo $model->getFieldsIndusFeatures("features1", '', $isEdit);
			echo $model->getFieldsIndusFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Motel/Hotel
		case 13:
			echo $model->getFieldsHotelType("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsRoomCount("required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide");
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsHotelFeatures("features1", '', $isEdit);
			echo $model->getFieldsHotelFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Assisted Care
		case 14:
			echo $model->getFieldsAssistType("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo $model->getFieldsRoomCount("required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"", '', $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL + Special Purpose
		case 15:
			echo $model->getFieldsSpecialType("required reqfield", $isEdit);
			echo $model->getFieldsTermCAP("required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + OFFICE
		case 16:
			echo $model->getFieldOfficeType("required reqfield", $isEdit);
			echo $model->getFieldOfficeClass("required reqfield", $isEdit);
			echo $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsOfficeTypeLease("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide", '', $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsOfficeFeatures("features1", '', $isEdit);
			echo $model->getFieldsOfficeFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + INDUSTRIAL
		case 17:
			echo $model->getFieldsIndusType("required reqfield", $isEdit);
			echo $model->getFieldsOfficeTypeLease("required reqfield", $isEdit);
			echo $model->getFieldsAvailSqFt($getMeasurement,$defsqft_a,"required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield", $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l,"required reqfield", $isEdit);
			echo $model->getFieldCeiling('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo $model->getFieldsIndusFeatures("features1");
			echo $model->getFieldsIndusFeatures("features2");
			echo '<div style="clear:both"></div>';
			break;
		// COMMERCIAL LEASE + RETAIL
		case 18:
			echo $model->getFieldsRetailType("required reqfield", $isEdit);
			echo $model->getFieldsBldgSqft($getMeasurement,$defsqft,"_wide","required reqfield", $isEdit);
			echo $model->getFieldsOfficeTypeLease("required reqfield", $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldStories('', $isEdit);
			echo $model->getFieldsLotSqFt($getMeasurement,$defsqft_l, '', $isEdit);
			echo $model->getFieldsYearBuilt('', $isEdit);
			echo '<div style="clear:both"></div>';
			echo $model->getFieldsParkingRatio('', $isEdit);
			echo $model->getFieldsOfficeFeatures("features1", '', $isEdit);
			echo $model->getFieldsOfficeFeatures("features2", '', $isEdit);
			echo '<div style="clear:both"></div>';
			break;
	}
	?>
</div>
<?php
die();
	}


	private function get_user_pops($user_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('pl.listing_id, pl.property_type, pl.sub_type, pl.property_name, pl.zip, pl.date_created, pl.date_expired, pi.image'));
		$query->from('#__pocket_listing pl');
		$query->leftJoin('#__pocket_images pi ON pi.listing_id = pl.listing_id');
		$query->where('pl.user_id = '.$user_id);
		$query->group($db->quoteName('pl.listing_id'));
		$query->order('pl.listing_id DESC , pi.image_id');
		$db->setQuery($query);
		$rows = $db->loadAssocList();
		foreach($rows as $key => $row) {
			if(!$row['image']) {
				$rows[$key]['image'] = $this->get_default_listing_image($row['property_type'], $row['sub_type'], false);
			}
		}
		return $rows;
	}
	private function get_default_listing_image($property_type, $sub_type, $bw = false) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$config = new JConfig();
		$query->select('type_name')->from('#__property_type')->where('type_id = '.$property_type);
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		$query->select('name')->from('#__property_sub_type')->where('type_id = '.$property_type.' AND sub_id='.$sub_type);
		$db->setQuery($query);
		$rows2 = $db->loadObjectList();
		//var_dump($rows2);
		if($rows2){
			$property_type = strtolower(str_replace(" ", "-", $rows2[0]->type_name));
			$sub_type = strtolower($rows2[0]->name);
		}
		switch($sub_type) {
			case "motel/hotel":
				$sub_type = "motel";
				break;
			case "townhouse/row house":
				$sub_type = "townhouse";
				break;
			case "assisted care":
				$sub_type = "assist";
				break;
			case "special purpose":
				$sub_type = "special";
				break;
			case "multi family":
				$sub_type = "multi-family";
				break;
		}
		$img_filename = ($bw) ? $property_type . "-" . $sub_type . "-bw.jpg" : $property_type . "-" . $sub_type . ".jpg";
		return str_replace("/administrator", "", JURI::base()) . "images/buyers/56x46/" . $img_filename;
	}
	public function updatelisting(){
		$model = $this->getModel('UserActivation');
		$date = date('Y-m-d', strtotime($_REQUEST['value']));
		$lisiting_id = $_REQUEST['id'];
		$ts1 = strtotime(date('Y-m-d'));
		$ts2 = strtotime($_REQUEST['value']);
		$seconds_diff = $ts2 - $ts1;
		$days = floor($seconds_diff/3600/24);
		if($days>0) {
			$model->updatePropertyExpiry(
				$lisiting_id , 
				$date
			);
			echo json_encode(array('code'=>1, 'days' => $days." ", 'value'=>'You have successfully updated the expiry of your listing to '.$days.' days'));
		} else {
			echo json_encode(array('code'=>0, 'value'=>'You cannot set the expiry to this date'));
		}
		die();
	}
function formedit(){
		$defsqft = "Sq. Ft.";	
		if(isset($_GET['country'])){
			if($_GET['country']==13){
				//$defsqft = "square meters";
			} else if($_GET['country']==103){
				//$defsqft = "m²";
			}
		}
		#$buyer_data = $_POST['buyerdata'];
		$data = $_POST['data'];
		?>
<div
	class="pocket-form">
	<div 
		id="prop_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your property. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2>Features</h2></div>
	<div 
		id="buyer_features" 
		style="display:none"
		onMouseover="ddrivetip('Select features to describe your buyer needs. The more features selected the better the matching results')" 
		onMouseout="hideddrivetip()"><h2>Minimum Features</h2></div>
	<?php
	switch ($_POST['stype']){
		// RESIDENTIAL PURCHASE + SFR
		case 1:
			?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select
			id="edit_jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bathroom = $data['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500-10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none"> This field is required </p>
	</div>
    	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view" class="reqfield">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label> <select id="edit_jform_condition"
			name="condition">
			<?php
				$selected_condition = $data['condition'];
			?>
			<option value="">--- Select ---</option>
			<option value="Fixer" <?php echo ($selected_condition == "Fixer" ? "selected" : ""); ?>>Fixer</option>
			<option value="Good" <?php echo ($selected_condition == "Good" ? "selected" : ""); ?>>Good</option>
			<option value="Excellent" <?php echo ($selected_condition == "Excellent" ? "selected" : ""); ?>>Excellent</option>
			<option value="Remodeled" <?php echo ($selected_condition == "Remodeled" ? "selected" : ""); ?>>Remodeled</option>
			<option value="New Construction" <?php echo ($selected_condition == "New Construction" ? "selected" : ""); ?>>New Construction</option>
			<option value="Under Construction" <?php ($selected_condition == "Under Construction" ? "selected" : ""); ?>>Under Construction</option>
			<option value="Not Disclosed" <?php echo ($selected_condition == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Water Access" <?php echo ($selected_features1 == "Water Access" ? "selected" : ""); ?>>Water Access</option>
			<option value="Horse Property" <?php echo ($selected_features1 == "Horse Property" ? "selected" : ""); ?>>Horse Property</option>
			<option value="Golf Course" <?php echo ($selected_features1 == "Golf Course" ? "selected" : ""); ?>>Golf Course</option>
			<option value="Walkstreet" <?php echo ($selected_features1 == "Walkstreet" ? "selected" : ""); ?>>Walkstreet</option>
			<option value="Media Room" <?php echo ($selected_features1 == "Media Room" ? "selected" : ""); ?>>Media Room</option>
			<option value="Guest House" <?php echo ($selected_features1 == "Guest House" ? "selected" : ""); ?>>Guest House</option>
			<option value="Wine Cellar" <?php echo ($selected_features1 == "Wine Cellar" ? "selected" : ""); ?>>Wine Cellar</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Den/Library" <?php echo ($selected_features1 == "Den/Library" ? "selected" : ""); ?>>Den/Library</option>
			<option value="Green Const." <?php echo ($selected_features1 == "Green Const." ? "selected" : ""); ?>>Green Const.</option>
			<option value="Basement" <?php echo ($selected_features1 == "Basement" ? "selected" : ""); ?>>Basement</option>
			<option value="RV/Boat Parking" <?php echo ($selected_features1 == "RV/Boat Parking" ? "selected" : ""); ?>>RV/Boat Parking</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Water Access" <?php echo ($selected_features2 == "Water Access" ? "selected" : ""); ?>>Water Access</option>
			<option value="Horse Property" <?php echo ($selected_features2 == "Horse Property" ? "selected" : ""); ?>>Horse Property</option>
			<option value="Golf Course" <?php echo ($selected_features2 == "Golf Course" ? "selected" : ""); ?>>Golf Course</option>
			<option value="Walkstreet" <?php echo ($selected_features2 == "Walkstreet" ? "selected" : ""); ?>>Walkstreet</option>
			<option value="Media Room" <?php echo ($selected_features2 == "Media Room" ? "selected" : ""); ?>>Media Room</option>
			<option value="Guest House" <?php echo ($selected_features2 == "Guest House" ? "selected" : ""); ?>>Guest House</option>
			<option value="Wine Cellar" <?php echo ($selected_features2 == "Wine Cellar" ? "selected" : ""); ?>>Wine Cellar</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Den/Library" <?php echo ($selected_features2 == "Den/Library" ? "selected" : ""); ?>>Den/Library</option>
			<option value="Green Const." <?php echo ($selected_features2 == "Green Const." ? "selected" : ""); ?>>Green Const.</option>
			<option value="Basement" <?php echo ($selected_features2 == "Basement" ? "selected" : ""); ?>>Basement</option>
			<option value="RV/Boat Parking" <?php echo ($selected_features2 == "RV/Boat Parking" ? "selected" : ""); ?>>RV/Boat Parking</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features3"
			name="features3">
			<?php
				$selected_features3 = $data['features3'];
			?>
			<option value="">--- Select ---</option>
			<option value="One Story" <?php echo ($selected_features3 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features3 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features3 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Water Access" <?php echo ($selected_features3 == "Water Access" ? "selected" : ""); ?>>Water Access</option>
			<option value="Horse Property" <?php echo ($selected_features3 == "Horse Property" ? "selected" : ""); ?>>Horse Property</option>
			<option value="Golf Course" <?php echo ($selected_features3 == "Golf Course" ? "selected" : ""); ?>>Golf Course</option>
			<option value="Walkstreet" <?php echo ($selected_features3 == "Walkstreet" ? "selected" : ""); ?>>Walkstreet</option>
			<option value="Media Room" <?php echo ($selected_features3 == "Media Room" ? "selected" : ""); ?>>Media Room</option>
			<option value="Guest House" <?php echo ($selected_features3 == "Guest House" ? "selected" : ""); ?>>Guest House</option>
			<option value="Wine Cellar" <?php echo ($selected_features3 == "Wine Cellar" ? "selected" : ""); ?>>Wine Cellar</option>
			<option value="Tennis Court" <?php echo ($selected_features3 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Den/Library" <?php echo ($selected_features3 == "Den/Library" ? "selected" : ""); ?>>Den/Library</option>
			<option value="Green Const." <?php echo ($selected_features3 == "Green Const." ? "selected" : ""); ?>>Green Const.</option>
			<option value="Basement" <?php echo ($selected_features3 == "Basement" ? "selected" : ""); ?>>Basement</option>
			<option value="RV/Boat Parking" <?php echo ($selected_features3 == "RV/Boat Parking" ? "selected" : ""); ?>>RV/Boat Parking</option>
			<option value="Senior" <?php echo ($selected_features3 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + CONDO
case 2:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="edit_jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
				<?php
			$selected_bathroom = $buyer_data[0]['needs'][0]['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_unitsqft"
			name="unitsqft" class="reqfield">
			<?php
				$selected_unitsqft = $data['unit_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_unitsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
    <div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="edit_jform_bldgtype"
			name="bldgtype">
			<?php
				$selected_bldgtype = $data['bldg_type'];
			?>
			<option value="">--- Select ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>>North Facing</option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>>South Facing</option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>>East Facing</option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>>West Facing</option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>>Low Rise</option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>>Mid Rise</option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>>High Rise</option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>>Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<option value="">--- Select ---</option>
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE
case 3:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select
			id="edit_jform_bedroom"
			name="bedroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bathroom = $data['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="unitsqft" class="reqfield">
			<?php
				$selected_unitsqft = $data['unit_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="left ys push-top2">
		<label>View</label>
			<select id="edit_jform_view" name="view">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		   </select>
			<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
    <div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="edit_jform_bldgtype"
			name="bldgtype">
			<?php
				$selected_bldgtype = $data['bldg_type'];
			?>
			<option value="">--- Select ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>>North Facing</option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>>South Facing</option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>>East Facing</option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>>West Facing</option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>>Low Rise</option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>>Mid Rise</option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>>High Rise</option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>>Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL PURCHASE + LAND
case 4:
	?>
	<div class="left ys push-top2">
		<label>Lot Size</label> <select id="edit_jform_lotsize"
			name="lotsize" class="reqfield">
			<?php
				$selected_lotsize = $data['lot_size'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsize == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsize == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsize == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsize == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsize == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsize == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsize == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsize == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsize == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsize == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsize == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsize error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view" class="reqfield">
			<?php
				$selected_view = $buyer_data[0]['needs'][0]['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label> <select id="edit_jform_zoned" class="reqfield" name="zoned">
			<?php
				$selected_zoned = $data['zoned'];
			?>
			<option value="">--- Select ---</option>
			<option value="1" <?php echo ($selected_zoned == "1" ? "selected" : ""); ?>>1 Unit</option>
			<option value="2" <?php echo ($selected_zoned == "2" ? "selected" : ""); ?>>2 Units</option>
			<option value="3-4" <?php echo ($selected_zoned == "3-4" ? "selected" : ""); ?>>3-4 Units</option>
			<option value="5-20" <?php echo ($selected_zoned == "5-20" ? "selected" : ""); ?>>5-20 Units</option>
			<option value="20" <?php echo ($selected_zoned == "20" ? "selected" : ""); ?>>20+ Units</option>
		</select>
		<p class="jform_zoned error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Sidewalks" <?php echo ($selected_features1 == "Sidewalks" ? "selected" : ""); ?>>Sidewalks</option>
			<option value="Utilities" <?php echo ($selected_features1 == "Utilities" ? "selected" : ""); ?>>Utilities</option>
			<option value="Curbs" <?php echo ($selected_features1 == "Curbs" ? "selected" : ""); ?>>Curbs</option>
			<option value="Horse Trails" <?php echo ($selected_features1 == "Horse Trails" ? "selected" : ""); ?>>Horse Trails</option>
			<option value="Rural" <?php echo ($selected_features1 == "Rural" ? "selected" : ""); ?>>Rural</option>
			<option value="Urban" <?php echo ($selected_features1 == "Urban" ? "selected" : ""); ?>>Urban</option>
			<option value="Suburban" <?php echo ($selected_features1 == "Suburban" ? "selected" : ""); ?>>Suburban</option>
			<option value="Permits" <?php echo ($selected_features1 == "Permits" ? "selected" : ""); ?>>Permits</option>
			<option value="HOA" <?php echo ($selected_features1 == "HOA" ? "selected" : ""); ?>>HOA</option>
			<option value="Sewer" <?php echo ($selected_features1 == "Sewer" ? "selected" : ""); ?>>Sewer</option>
			<option value="CC&Rs" <?php echo ($selected_features1 == "CC&Rs" ? "selected" : ""); ?>>CC&amp;Rs</option>
			<option value="Coastal" <?php echo ($selected_features1 == "Coastal" ? "selected" : ""); ?>>Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Sidewalks" <?php echo ($selected_features2 == "Sidewalks" ? "selected" : ""); ?>>Sidewalks</option>
			<option value="Utilities" <?php echo ($selected_features2 == "Utilities" ? "selected" : ""); ?>>Utilities</option>
			<option value="Curbs" <?php echo ($selected_features2 == "Curbs" ? "selected" : ""); ?>>Curbs</option>
			<option value="Horse Trails" <?php echo ($selected_features2 == "Horse Trails" ? "selected" : ""); ?>>Horse Trails</option>
			<option value="Rural" <?php echo ($selected_features2 == "Rural" ? "selected" : ""); ?>>Rural</option>
			<option value="Urban" <?php echo ($selected_features2 == "Urban" ? "selected" : ""); ?>>Urban</option>
			<option value="Suburban" <?php echo ($selected_features2 == "Suburban" ? "selected" : ""); ?>>Suburban</option>
			<option value="Permits" <?php echo ($selected_features2 == "Permits" ? "selected" : ""); ?>>Permits</option>
			<option value="HOA" <?php echo ($selected_features2 == "HOA" ? "selected" : ""); ?>>HOA</option>
			<option value="Sewer" <?php echo ($selected_features2 == "Sewer" ? "selected" : ""); ?>>Sewer</option>
			<option value="CC&Rs" <?php echo ($selected_features2 == "CC&Rs" ? "selected" : ""); ?>>CC&amp;Rs</option>
			<option value="Coastal" <?php echo ($selected_features2 == "Coastal" ? "selected" : ""); ?>>Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + SFR
case 5:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select
			class="reqfield"
			id="edit_jform_bedroom"
			name="bedroom">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bathroom = $data['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="edit_jform_term" name="term" class="reqfield">
			<?php
				$selected_term = $data['term'];
			?>
			<option value="">--- Select ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>>Short Term</option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>>M to M</option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>>Year Lease</option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>>Multi Year Lease</option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>>Lease Option</option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="edit_jform_possession"
			name="possession" class="reqfield">
			<?php
				$selected_possession = $data['possession'];
			?>
			<option value="">--- Select ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>>Immediately</option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>>Within 30 days</option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>>Within 60 days</option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>>Within 90 days</option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>>Within 180 days</option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label>Pet</label>
        <?php $selected_pet = $data['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="edit_pyes" style="margin-right:1px" class="left gradient-blue-toggle yes edit_pet" rel="1">Yes</a>
        <a href="javascript: void(0)" id="edit_pno" class="left gradient-gray no edit_pet" rel="0">No</a>
		<input type="hidden" value="" onchange="edit_get_pet(this.value)" id="edit_jform_pet" name="pet" class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px; width:100px">
		<label>Furnished</label>
        <?php $selected_furnished = $data['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="edit_fyes"
            class="left gradient-gray no edit_furnished" rel="1">Yes</a>
        <a
			href="javascript: void(0)"
            id="edit_fno"
            class="left gradient-blue-toggle yes edit_furnished"
			rel="0">No</a>
        <input onchange="edit_get_furnished(this.value)" value="" type="hidden" value="<?php echo $selected_furnished ?>" id="edit_jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view" >
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label> <select id="edit_jform_condition"
			name="condition">
			<?php
				$selected_condition = $data['condition'];
			?>
			<option value="">--- Select ---</option>
			<option value="Fixer" <?php echo ($selected_condition == "Fixer" ? "selected" : ""); ?>>Fixer</option>
			<option value="Good" <?php echo ($selected_condition == "Good" ? "selected" : ""); ?>>Good</option>
			<option value="Excellent" <?php echo ($selected_condition == "Excellent" ? "selected" : ""); ?>>Excellent</option>
			<option value="Remodeled" <?php echo ($selected_condition == "Remodeled" ? "selected" : ""); ?>>Remodeled</option>
			<option value="New Construction" <?php echo ($selected_condition == "New Construction" ? "selected" : ""); ?>>New Construction</option>
			<option value="Under Construction" <?php ($selected_condition == "Under Construction" ? "selected" : ""); ?>>Under Construction</option>
			<option value="Not Disclosed" <?php echo ($selected_condition == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
    <div class="clear-float"></div>
    <!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Water Access" <?php echo ($selected_features1 == "Water Access" ? "selected" : ""); ?>>Water Access</option>
			<option value="Horse Property" <?php echo ($selected_features1 == "Horse Property" ? "selected" : ""); ?>>Horse Property</option>
			<option value="Golf Course" <?php echo ($selected_features1 == "Golf Course" ? "selected" : ""); ?>>Golf Course</option>
			<option value="Walkstreet" <?php echo ($selected_features1 == "Walkstreet" ? "selected" : ""); ?>>Walkstreet</option>
			<option value="Media Room" <?php echo ($selected_features1 == "Media Room" ? "selected" : ""); ?>>Media Room</option>
			<option value="Guest House" <?php echo ($selected_features1 == "Guest House" ? "selected" : ""); ?>>Guest House</option>
			<option value="Wine Cellar" <?php echo ($selected_features1 == "Wine Cellar" ? "selected" : ""); ?>>Wine Cellar</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Den/Library" <?php echo ($selected_features1 == "Den/Library" ? "selected" : ""); ?>>Den/Library</option>
			<option value="Green Const." <?php echo ($selected_features1 == "Green Const." ? "selected" : ""); ?>>Green Const.</option>
			<option value="Basement" <?php echo ($selected_features1 == "Basement" ? "selected" : ""); ?>>Basement</option>
			<option value="RV/Boat Parking" <?php echo ($selected_features1 == "RV/Boat Parking" ? "selected" : ""); ?>>RV/Boat Parking</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Water Access" <?php echo ($selected_features2 == "Water Access" ? "selected" : ""); ?>>Water Access</option>
			<option value="Horse Property" <?php echo ($selected_features2 == "Horse Property" ? "selected" : ""); ?>>Horse Property</option>
			<option value="Golf Course" <?php echo ($selected_features2 == "Golf Course" ? "selected" : ""); ?>>Golf Course</option>
			<option value="Walkstreet" <?php echo ($selected_features2 == "Walkstreet" ? "selected" : ""); ?>>Walkstreet</option>
			<option value="Media Room" <?php echo ($selected_features2 == "Media Room" ? "selected" : ""); ?>>Media Room</option>
			<option value="Guest House" <?php echo ($selected_features2 == "Guest House" ? "selected" : ""); ?>>Guest House</option>
			<option value="Wine Cellar" <?php echo ($selected_features2 == "Wine Cellar" ? "selected" : ""); ?>>Wine Cellar</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Den/Library" <?php echo ($selected_features2 == "Den/Library" ? "selected" : ""); ?>>Den/Library</option>
			<option value="Green Const." <?php echo ($selected_features2 == "Green Const." ? "selected" : ""); ?>>Green Const.</option>
			<option value="Basement" <?php echo ($selected_features2 == "Basement" ? "selected" : ""); ?>>Basement</option>
			<option value="RV/Boat Parking" <?php echo ($selected_features2 == "RV/Boat Parking" ? "selected" : ""); ?>>RV/Boat Parking</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<?php
	break;
	// RESIDENTIAL LEASE + Condo
case 6:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select id="edit_jform_bedroom"
			name="bedroom"class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bathroom = $data['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="unitsqft" class="reqfield">
			<?php
				$selected_unitsqft = $data['unit_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="edit_jform_term" name="term" class="reqfield">
			<?php
				$selected_term = $data['term'];
			?>
			<option value="">--- Select ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>>Short Term</option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>>M to M</option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>>Year Lease</option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>>Multi Year Lease</option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>>Lease Option</option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label>Pet</label>
        <?php $selected_pet = $data['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="edit_pyes" style="margin-right:1px" class="left gradient-blue-toggle yes edit_pet" rel="1">Yes</a>
        <a href="javascript: void(0)" id="edit_pno" class="left gradient-gray no edit_pet" rel="0">No</a>
		<input type="hidden" value="" onchange="edit_get_pet(this.value)" id="edit_jform_pet" name="pet"
			class="text-input reqfield" />
	</div>
  	<div class="left ys" style="margin-top: 30px; width:100px">
		<label>Furnished</label>
        <?php $selected_furnished = $data['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="edit_fyes"
            class="left gradient-gray no edit_furnished" rel="1">Yes</a>
        <a
			href="javascript: void(0)"
            id="edit_fno"
            class="left gradient-blue-toggle yes edit_furnished"
			rel="0">No</a>
        <input onchange="edit_get_furnished(this.value)" value="" type="hidden" id="edit_jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view">			
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="edit_jform_possession"
			name="possession">
			<?php
				$selected_possession = $data['possession'];
			?>
			<option value="">--- Select ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>>Immediately</option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>>Within 30 days</option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>>Within 60 days</option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>>Within 90 days</option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>>Within 180 days</option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
    <div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="edit_jform_bldgtype" name="bldgtype">
			<?php
				$selected_bldgtype = $data['bldg_type'];
			?>
			<option value="">--- Select ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>>North Facing</option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>>South Facing</option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>>East Facing</option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>>West Facing</option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>>Low Rise</option>
			<option value="Mid Rise" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>>Mid Rise</option>
			<option value="High Rise" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>>High Rise</option>
			<option value="Co-Op" <?php echo ($selected_bldgtype == "Co-Op" ? "selected" : ""); ?>>Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">			
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Townhouse/Row House
case 7:
	?>
	<div class="left ys push-top2">
		<label>Bedrooms</label> <select
			class="reqfield"
			id="edit_jform_bedroom"
			name="bedroom">
			<option value="">--- Select ---</option>
			<?php
			$selected_bedroom = $data['bedroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bedroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bedroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bedroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bathrooms</label> <select id="edit_jform_bathroom"
			name="bathroom" class="reqfield">
			<option value="">--- Select ---</option>
			<?php
			$selected_bathroom = $data['bathroom'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_bathroom) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_bathroom == "6+" ? "selected" : ""); ?>>6+</option>
		</select>
		<p class="jform_bathroom error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Unit <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft" class="reqfield"
			name="unitsqft">
			<?php
				$selected_unitsqft = $data['unit_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_unitsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_unitsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_unitsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_unitsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_unitsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_unitsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_unitsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
			<option value="7,500 -10,000" <?php echo ($selected_unitsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_unitsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_unitsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_unitsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Term</label> <select id="edit_jform_term" name="term" class="reqfield">
			<?php
				$selected_term = $data['term'];
			?>
			<option value="">--- Select ---</option>
			<option value="Short Term" <?php echo ($selected_term == "Short Term" ?  "selected" : ""); ?>>Short Term</option>
			<option value="M to M" <?php echo ($selected_term == "M to M" ?  "selected" : ""); ?>>M to M</option>
			<option value="Year Lease" <?php echo ($selected_term == "Year Lease" ?  "selected" : ""); ?>>Year Lease</option>
			<option value="Multi Year Lease" <?php echo ($selected_term == "Multi Year Lease" ?  "selected" : ""); ?>>Multi Year Lease</option>
			<option value="Lease Option" <?php echo ($selected_term == "Lease Option" ?  "selected" : ""); ?>>Lease Option</option>
		</select>
		<p class="jform_term error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys" style="margin-top: 30px; width:90px">
		<label>Pet</label>
        <?php $selected_pet = $data['pet'];?>
        <?php $data_pet = $data['pet'];?>
        <a href="javascript: void(0)" id="edit_pyes" style="margin-right:1px" class="left gradient-blue-toggle yes edit_pet" rel="1">Yes</a>
        <a href="javascript: void(0)" id="edit_pno" class="left gradient-gray no edit_pet" rel="0">No</a>
		<input type="hidden" value="" onchange="edit_get_pet(this.value)" id="edit_jform_pet" name="pet"
			class="text-input reqfield" />
	</div>
	<div class="left ys" style="margin-top: 30px; width:100px">
		<label>Furnished</label>
        <?php $selected_furnished = $data['furnished'];?>
        <?php $data_furnished = $data['furnished'];?>
    	<a
        	href="javascript: void(0)"
			style="margin-right:1px"
            id="edit_fyes"
            class="left gradient-gray no edit_furnished" rel="1">Yes</a>
        <a
			href="javascript: void(0)"
            id="edit_fno"
            class="left gradient-blue-toggle yes edit_furnished"
			rel="0">No</a>
        <input onchange="edit_get_furnished(this.value)" value="" type="hidden" value="<?php echo $selected_furnished ?>" id="edit_jform_furnished"
			name="furnished" class="text-input reqfield" />
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear=float"></div>
	<div class="left ys push-top2">
		<label>Possession</label> <select id="edit_jform_possession"
			name="possession">
			<?php
				$selected_possession = $data['possession'];
			?>
			<option value="">--- Select ---</option>
			<option value="Immediately" <?php echo ($selected_possession == "Immediately" ?  "selected" : ""); ?>>Immediately</option>
			<option value="Within 30 days" <?php echo ($selected_possession == "Within 30 days" ?  "selected" : ""); ?>>Within 30 days</option>
			<option value="Within 60 days" <?php echo ($selected_possession == "Within 60 days" ?  "selected" : ""); ?>>Within 60 days</option>
			<option value="Within 90 days" <?php echo ($selected_possession == "Within 90 days" ?  "selected" : ""); ?>>Within 90 days</option>
			<option value="Within 180 days" <?php echo ($selected_possession == "Within 180 days" ?  "selected" : ""); ?>>Within 180 days</option>
		</select>
			<p class="jform_possession error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Type</label> <select id="edit_jform_bldgtype"
			name="bldgtype">
			<?php
				$selected_bldgtype = $data['bldg_type'];
			?>
			<option value="">--- Select ---</option>
			<option value="North Facing" <?php echo ($selected_bldgtype == "North Facing" ? "selected" : ""); ?>>North Facing</option>
			<option value="South Facing" <?php echo ($selected_bldgtype == "South Facing" ? "selected" : ""); ?>>South Facing</option>
			<option value="East Facing" <?php echo ($selected_bldgtype == "East Facing" ? "selected" : ""); ?>>East Facing</option>
			<option value="West Facing" <?php echo ($selected_bldgtype == "West Facing" ? "selected" : ""); ?>>West Facing</option>
			<option value="Low Rise" <?php echo ($selected_bldgtype == "Low Rise" ? "selected" : ""); ?>>Low Rise</option>
			<option value="Detached" <?php echo ($selected_bldgtype == "Mid Rise" ? "selected" : ""); ?>>Detached</option>
			<option value="Attached" <?php echo ($selected_bldgtype == "High Rise" ? "selected" : ""); ?>>Attached</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label> <select id="edit_jform_poolspa"
			name="poolspa">
			<?php
				$selected_poolspa = $data['pool_spa'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_poolspa == "None" ? "selected" : ""); ?>>None</option>
			<option value="Pool" <?php echo ($selected_poolspa == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Pool/Spa" <?php echo ($selected_poolspa == "Pool/Spa" ? "selected" : ""); ?>>Pool/Spa</option>
			<option value="Spa" <?php echo ($selected_poolspa == "Spa" ? "selected" : ""); ?>>Other</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label> <select id="edit_jform_garage" name="garage">
			<?php
				$selected_garage = $data['garage'];
			?>
			<option value="">--- Select ---</option>
			<?php
				for($i=1;$i<=7;$i++){
					if($i==$selected_garage) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="8+" <?php echo ($selected_garage == "8+" ? "selected" : ""); ?>>8+</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features1 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features1 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features1 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features1 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features1 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features1 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features1 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features1 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Gym" <?php echo ($selected_features2 == "Gym" ? "selected" : ""); ?>>Gym</option>
			<option value="Security" <?php echo ($selected_features2 == "Security" ? "selected" : ""); ?>>Security</option>
			<option value="Tennis Court" <?php echo ($selected_features2 == "Tennis Court" ? "selected" : ""); ?>>Tennis Court</option>
			<option value="Doorman" <?php echo ($selected_features2 == "Doorman" ? "selected" : ""); ?>>Doorman</option>
			<option value="Penthouse" <?php echo ($selected_features2 == "Penthouse" ? "selected" : ""); ?>>Penthouse</option>
			<option value="One Story" <?php echo ($selected_features2 == "One Story" ? "selected" : ""); ?>>One Story</option>
			<option value="Two Story" <?php echo ($selected_features2 == "Two Story" ? "selected" : ""); ?>>Two Story</option>
			<option value="Three Story" <?php echo ($selected_features2 == "Three Story" ? "selected" : ""); ?>>Three Story</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// RESIDENTIAL LEASE + Land
case 8:
	?>
	<div class="left ys push-top2">
		<label>Lot Size</label> <select
			class="reqfield"
			id="edit_jform_lotsize"
			name="lotsize">
			<?php
				$selected_lotsize = $data['lot_size'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsize == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsize == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsize == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsize == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsize == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsize == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsize == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsize == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsize == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsize == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsize == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsize error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label> <select  id="edit_jform_zoned" name="zoned" class="reqfield">
			<?php
				$selected_zoned = $data['zoned'];
			?>
			<option value="">--- Select ---</option>
			<option value="1" <?php echo ($selected_zoned == "1" ? "selected" : ""); ?>>1 Unit</option>
			<option value="2" <?php echo ($selected_zoned == "2" ? "selected" : ""); ?>>2 Units</option>
			<option value="3-4" <?php echo ($selected_zoned == "3-4" ? "selected" : ""); ?>>3-4 Units</option>
			<option value="5-20" <?php echo ($selected_zoned == "5-20" ? "selected" : ""); ?>>5-20 Units</option>
			<option value="20" <?php echo ($selected_zoned == "20" ? "selected" : ""); ?>>20+ Units</option>
		</select>
		<p class="jform_zoned error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view"class="reqfield">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Sidewalks" <?php echo ($selected_features1 == "Sidewalks" ? "selected" : ""); ?>>Sidewalks</option>
			<option value="Utilities" <?php echo ($selected_features1 == "Utilities" ? "selected" : ""); ?>>Utilities</option>
			<option value="Curbs" <?php echo ($selected_features1 == "Curbs" ? "selected" : ""); ?>>Curbs</option>
			<option value="Horse Trails" <?php echo ($selected_features1 == "Horse Trails" ? "selected" : ""); ?>>Horse Trails</option>
			<option value="Rural" <?php echo ($selected_features1 == "Rural" ? "selected" : ""); ?>>Rural</option>
			<option value="Urban" <?php echo ($selected_features1 == "Urban" ? "selected" : ""); ?>>Urban</option>
			<option value="Suburban" <?php echo ($selected_features1 == "Suburban" ? "selected" : ""); ?>>Suburban</option>
			<option value="Permits" <?php echo ($selected_features1 == "Permits" ? "selected" : ""); ?>>Permits</option>
			<option value="HOA" <?php echo ($selected_features1 == "HOA" ? "selected" : ""); ?>>HOA</option>
			<option value="Sewer" <?php echo ($selected_features1 == "Sewer" ? "selected" : ""); ?>>Sewer</option>
			<option value="CC&Rs" <?php echo ($selected_features1 == "CC&Rs" ? "selected" : ""); ?>>CC&amp;Rs</option>
			<option value="Coastal" <?php echo ($selected_features1 == "Coastal" ? "selected" : ""); ?>>Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Sidewalks" <?php echo ($selected_features2 == "Sidewalks" ? "selected" : ""); ?>>Sidewalks</option>
			<option value="Utilities" <?php echo ($selected_features2 == "Utilities" ? "selected" : ""); ?>>Utilities</option>
			<option value="Curbs" <?php echo ($selected_features2 == "Curbs" ? "selected" : ""); ?>>Curbs</option>
			<option value="Horse Trails" <?php echo ($selected_features2 == "Horse Trails" ? "selected" : ""); ?>>Horse Trails</option>
			<option value="Rural" <?php echo ($selected_features2 == "Rural" ? "selected" : ""); ?>>Rural</option>
			<option value="Urban" <?php echo ($selected_features2 == "Urban" ? "selected" : ""); ?>>Urban</option>
			<option value="Suburban" <?php echo ($selected_features2 == "Suburban" ? "selected" : ""); ?>>Suburban</option>
			<option value="Permits" <?php echo ($selected_features2 == "Permits" ? "selected" : ""); ?>>Permits</option>
			<option value="HOA" <?php echo ($selected_features2 == "HOA" ? "selected" : ""); ?>>HOA</option>
			<option value="Sewer" <?php echo ($selected_features2 == "Sewer" ? "selected" : ""); ?>>Sewer</option>
			<option value="CC&Rs" <?php echo ($selected_features2 == "CC&Rs" ? "selected" : ""); ?>>CC&amp;Rs</option>
			<option value="Coastal" <?php echo ($selected_features2 == "Coastal" ? "selected" : ""); ?>>Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Multi Family
case 9:
	?>
	<div class="left ys push-top2">
		<label>Units</label>
			<select id="edit_jform_units" name="units" class="reqfield">
				<?php
					$selected_units = $data['units'];
					$selected_units = $data['units'];
				?>
				<option value="">--- Select ---</option>
				<option value="2" <?php echo ($selected_units == "2" ? "selected" : ""); ?>>Duplex</option>
				<option value="3" <?php echo ($selected_units == "3" ? "selected" : ""); ?>>TriPlex</option>
				<option value="4" <?php echo ($selected_units == "4" ? "selected" : ""); ?>>Quad</option>
				<option value="5-9" <?php echo ($selected_units == "5-9" ? "selected" : ""); ?>>5-9</option>
				<option value="10-15" <?php echo ($selected_units == "10-15" ? "selected" : ""); ?>>10-15</option>
				<option value="16-29" <?php echo ($selected_units == "16-29" ? "selected" : ""); ?>>16-29</option>
				<option value="30-50" <?php echo ($selected_units == "30-50" ? "selected" : ""); ?>>30-50</option>
				<option value="50-100" <?php echo ($selected_units == "50-100" ? "selected" : ""); ?>>50-100</option>
				<option value="101-150" <?php echo ($selected_units == "101-150" ? "selected" : ""); ?>>101-150</option>
				<option value="151-250" <?php echo ($selected_units == "151-250" ? "selected" : ""); ?>>151-250</option>
				<option value="251" <?php echo ($selected_units == "251" ? "selected" : ""); ?>>251+</option>
				<option value="Land" <?php echo ($selected_units == "Land" ? "selected" : ""); ?>>Land</option>
			</select>
		<p class="jform_units error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>GRM</label> <select id="edit_jform_grm" name="grm" class="reqfield">
			<?php
				$selected_grm = $data['grm'];
			?>
			<option value="">--- Select ---</option>
			<option value="0" <?php echo ($selected_grm == "0" ? "selected" : ""); ?>>Not Disclosed</option>
			<option value="1-2" <?php echo ($selected_grm == "1-2" ? "selected" : ""); ?>>1-2</option>
			<option value="3-4" <?php echo ($selected_grm == "3-4" ? "selected" : ""); ?>>3-4</option>
			<option value="4-5" <?php echo ($selected_grm == "4-5" ? "selected" : ""); ?>>4-5</option>
			<option value="5-6" <?php echo ($selected_grm == "5-6" ? "selected" : ""); ?>>5-6</option>
			<option value="6-7" <?php echo ($selected_grm == "6-7" ? "selected" : ""); ?>>6-7</option>
			<option value="7-8" <?php echo ($selected_grm == "7-8" ? "selected" : ""); ?>>7-8</option>
			<option value="8-9" <?php echo ($selected_grm == "8-9" ? "selected" : ""); ?>>8-9</option>
			<option value="9-10" <?php echo ($selected_grm == "9-10" ? "selected" : ""); ?>>9-10</option>
			<option value="10-11" <?php echo ($selected_grm == "10-11" ? "selected" : ""); ?>>10-11</option>
			<option value="11-12" <?php echo ($selected_grm == "11-12" ? "selected" : ""); ?>>11-12</option>
			<option value="12-13" <?php echo ($selected_grm == "12-13" ? "selected" : ""); ?>>12-13</option>
			<option value="13-14" <?php echo ($selected_grm == "13-14" ? "selected" : ""); ?>>13-14</option>
			<option value="14-15" <?php echo ($selected_grm == "14-15" ? "selected" : ""); ?>>14-15</option>
			<option value="15-16" <?php echo ($selected_grm == "15-16" ? "selected" : ""); ?>>15-16</option>
			<option value="16-17" <?php echo ($selected_grm == "16-17" ? "selected" : ""); ?>>16-17</option>
			<option value="17-18" <?php echo ($selected_grm == "17-18" ? "selected" : ""); ?>>17-18</option>
			<option value="18-19" <?php echo ($selected_grm == "18-19" ? "selected" : ""); ?>>18-19</option>
			<option value="19-20" <?php echo ($selected_grm == "19-20" ? "selected" : ""); ?>>19-20</option>
			<option value="20" <?php echo ($selected_grm == "20" ? "selected" : ""); ?>>20+</option>
		</select>
		<p class="jform_grm error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
    <div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft" name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_bldgsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft" name="lotsqft" class="reqfield">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
    <div class="left ys push-top2">
		<label>View</label> <select id="edit_jform_view" name="view">
			<?php
				$selected_view = $data['view'];
			?>
			<option value="">--- Select ---</option>
			<option value="None" <?php echo ($selected_view == "None" ? "selected" : ""); ?>>None</option>
			<option value="Panoramic" <?php echo ($selected_view == "Panoramic" ? "selected" : ""); ?>>Panoramic</option>
			<option value="City" <?php echo ($selected_view == "City" ? "selected" : ""); ?>>City</option>
			<option value="Mountains/Hills" <?php echo ($selected_view == "Mountains/Hills" ? "selected" : ""); ?>>Mountains/Hills</option>
			<option value="Coastline" <?php echo ($selected_view == "Coastline" ? "selected" : ""); ?>>Coastline</option>
			<option value="Water" <?php echo ($selected_view == "Water" ? "selected" : ""); ?>>Water</option>
			<option value="Ocean" <?php echo ($selected_view == "Ocean" ? "selected" : ""); ?>>Ocean</option>
			<option value="Lake/River" <?php echo ($selected_view == "Lake/River" ? "selected" : ""); ?>>Lake/River</option>
			<option value="Landmark" <?php echo ($selected_view == "Landmark" ? "selected" : ""); ?>>Landmark</option>
			<option value="Desert" <?php echo ($selected_view == "Desert" ? "selected" : ""); ?>>Desert</option>
			<option value="Bay" <?php echo ($selected_view == "Bay" ? "selected" : ""); ?>>Bay</option>
			<option value="Vineyard" <?php echo ($selected_view == "Vineyard" ? "selected" : ""); ?>>Vineyard</option>
			<option value="Golf" <?php echo ($selected_view == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Other" <?php echo ($selected_view == "Other" ? "selected" : ""); ?>>Other</option>
		</select>
		<p class="jform_view error_msg" style="display:none;"> This field is required </p>
	</div>
<!---------------------------------------------------------------- -->
	<div style="clear:both"></div>
    <div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label> 
		<select id="edit_jform_style" style="width:200px" name="style">
			<?php
				$selected_style = $data['style'];
			?>
			<option value="">--- Select ---</option>
			<option value="American Farmhouse" <?php echo ($selected_style == "American Farmhouse" ? "selected" : ""); ?>>American Farmhouse</option>
			<option value="Art Deco" <?php echo ($selected_style == "Art Deco" ? "selected" : ""); ?>>Art Deco</option>
			<option value="Art Modern/Mid Century" <?php echo ($selected_style == "Art Modern/Mid Century" ? "selected" : ""); ?>>Art Modern/Mid Century</option>
			<option value="Cape Cod" <?php echo ($selected_style == "Cape Cod" ? "selected" : ""); ?>>Cape Cod</option>
			<option value="Colonial Revival" <?php echo ($selected_style == "Colonial Revival" ? "selected" : ""); ?>>Colonial Revival</option>
			<option value="Contemporary" <?php echo ($selected_style == "Contemporary" ? "selected" : ""); ?>>Contemporary</option>
			<option value="Craftsman" <?php echo ($selected_style == "Craftsman" ? "selected" : ""); ?>>Craftsman</option>
			<option value="French" <?php echo ($selected_style == "French" ? "selected" : ""); ?>>French</option>
			<option value="Italian/Tuscan" <?php echo ($selected_style == "Italian/Tuscan" ? "selected" : ""); ?>>Italian/Tuscan</option>
			<option value="Prairie Style" <?php echo ($selected_style == "Prairie Style" ? "selected" : ""); ?>>Prairie Style</option>
			<option value="Pueblo Revival" <?php echo ($selected_style == "Pueblo Revival" ? "selected" : ""); ?>>Pueblo Revival</option>
			<option value="Ranch" <?php echo ($selected_style == "Ranch" ? "selected" : ""); ?>>Ranch</option>
			<option value="Spanish/Mediterranean" <?php echo ($selected_style == "Spanish/Mediterranean" ? "selected" : ""); ?>>Spanish/Mediterranean</option>
			<option value="Swiss Cottage" <?php echo ($selected_style == "Swiss Cottage" ? "selected" : ""); ?>>Swiss Cottage</option>
			<option value="Tudor" <?php echo ($selected_style == "Tudor" ? "selected" : ""); ?>>Tudor</option>
			<option value="Victorian" <?php echo ($selected_style == "Victorian" ? "selected" : ""); ?>>Victorian</option>
			<option value="Historic" <?php echo ($selected_style == "Historic" ? "selected" : ""); ?>>Historic</option>
			<option value="Architecturally Significant" <?php echo ($selected_style == "Architecturally Significant" ? "selected" : ""); ?>>Architecturally Significant</option>
			<option value="Green" <?php echo ($selected_style == "Green" ? "selected" : ""); ?>>Green</option>
			<option value="Not Disclosed" <?php echo ($selected_style == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="edit_jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			$selected_occupancy = $data['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Rent Control" <?php echo ($selected_features1 == "Rent Control" ? "selected" : ""); ?>>Rent Control</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Senior</option>
			<option value="Assoc-Pool" <?php echo ($selected_features1 == "Assoc-Pool" ? "selected" : ""); ?>>Assoc-Pool</option>
			<option value="Assoc-Spa" <?php echo ($selected_features1 == "Assoc-Spa" ? "selected" : ""); ?>>Assoc-Spa</option>
			<option value="Assoc-Tennis" <?php echo ($selected_features1 == "Assoc-Tennis" ? "selected" : ""); ?>>Assoc-Tennis</option>
			<option value="Assoc-Other" <?php echo ($selected_features1 == "Assoc-Other" ? "selected" : ""); ?>>Assoc-Other</option>
			<option value="Section 8" <?php echo ($selected_features1 == "Section 8" ? "selected" : ""); ?>>Section 8</option>
			<option value="25% Occupied" <?php echo ($selected_features1 == "25% Occupied" ? "selected" : ""); ?>>25% Occupied</option>
			<option value="50% Occupied" <?php echo ($selected_features1 == "50% Occupied" ? "selected" : ""); ?>>50% Occupied</option>
			<option value="75% Occupied" <?php echo ($selected_features1 == "75% Occupied" ? "selected" : ""); ?>>75% Occupied</option>
			<option value="100% Occupied" <?php echo ($selected_features1 == "100% Occupied" ? "selected" : ""); ?>>100% Occupied</option>
			<option value="Cash Cow" <?php echo ($selected_features1 == "Cash Cow" ? "selected" : ""); ?>>Cash Cow</option>
			<option value="Value Add" <?php echo ($selected_features1 == "Value Add" ? "selected" : ""); ?>>Senior</option>
			<option value="Senior" <?php echo ($selected_features1 == "Senior" ? "selected" : ""); ?>>Value Add</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Rent Control" <?php echo ($selected_features2 == "Rent Control" ? "selected" : ""); ?>>Rent Control</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Senior</option>
			<option value="Assoc-Pool" <?php echo ($selected_features2 == "Assoc-Pool" ? "selected" : ""); ?>>Assoc-Pool</option>
			<option value="Assoc-Spa" <?php echo ($selected_features2 == "Assoc-Spa" ? "selected" : ""); ?>>Assoc-Spa</option>
			<option value="Assoc-Tennis" <?php echo ($selected_features2 == "Assoc-Tennis" ? "selected" : ""); ?>>Assoc-Tennis</option>
			<option value="Assoc-Other" <?php echo ($selected_features2 == "Assoc-Other" ? "selected" : ""); ?>>Assoc-Other</option>
			<option value="Section 8" <?php echo ($selected_features2 == "Section 8" ? "selected" : ""); ?>>Section 8</option>
			<option value="25% Occupied" <?php echo ($selected_features2 == "25% Occupied" ? "selected" : ""); ?>>25% Occupied</option>
			<option value="50% Occupied" <?php echo ($selected_features2 == "50% Occupied" ? "selected" : ""); ?>>50% Occupied</option>
			<option value="75% Occupied" <?php echo ($selected_features2 == "75% Occupied" ? "selected" : ""); ?>>75% Occupied</option>
			<option value="100% Occupied" <?php echo ($selected_features2 == "100% Occupied" ? "selected" : ""); ?>>100% Occupied</option>
			<option value="Cash Cow" <?php echo ($selected_features2 == "Cash Cow" ? "selected" : ""); ?>>Cash Cow</option>
			<option value="Value Add" <?php echo ($selected_features2 == "Value Add" ? "selected" : ""); ?>>Senior</option>
			<option value="Senior" <?php echo ($selected_features2 == "Senior" ? "selected" : ""); ?>>Value Add</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Office
case 10:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select  id="edit_jform_type" name="type" class="reqfield">
		<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Office" <?php echo ($selected_type == "Office" ? "selected" : ""); ?>>Office</option>
			<option value="Institutional" <?php echo ($selected_type == "Institutional" ? "selected" : ""); ?>>Institutional</option>
			<option value="Medical" <?php echo ($selected_type == "Medical" ? "selected" : ""); ?>>Medical</option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>>Warehouse</option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>>Condo</option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>>R&amp;D</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>>Business Park</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Class</label> <select id="edit_jform_class" name="class" class="reqfield">
			<?php
				$selected_class = $data['listing_class'];
			?>
			<option value="">--- Select ---</option>
			<option value="4" <?php echo ($selected_class == 4 ? "selected" : ""); ?>>A</option>
			<option value="3" <?php echo ($selected_class == 3 ? "selected" : ""); ?>>B</option>
			<option value="2" <?php echo ($selected_class == 2 ? "selected" : ""); ?>>C</option>
			<option value="1" <?php echo ($selected_class == 1 ? "selected" : ""); ?>>D</option>
			<option value="Not Disclosed" <?php echo ($selected_class == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_class error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft" name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
            <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="edit_jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			$selected_occupancy = $buyer_data[0]['needs'][0]['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Industrial
case 11:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Flex Space" <?php echo ($selected_type == "Flex Space" ? "selected" : ""); ?>>Flex Space</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>>Business Park</option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>>Condo</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
			<option value="Manufacturing" <?php echo ($selected_type == "Manufacturing" ? "selected" : ""); ?>>Manufacturing</option>
			<option value="Office Showroom" <?php echo ($selected_type == "Office Showroom" ? "selected" : ""); ?>>Office Showroom</option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>>R&amp;D</option>
			<option value="Self/Mini Storage" <?php echo ($selected_type == "Self/Mini Storage" ? "selected" : ""); ?>>Self/Mini Storage</option>
			<option value="Truck Terminal/Hub" <?php echo ($selected_type == "Truck Terminal/Hub" ? "selected" : ""); ?>>Truck Terminal/Hub</option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>>Warehouse</option>
			<option value="Distribution" <?php echo ($selected_type == "Distribution" ? "selected" : ""); ?>>Distribution</option>
			<option value="Cold Storage" <?php echo ($selected_type == "Cold Storage" ? "selected" : ""); ?>>Cold Storage</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select  id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
			<p class="jform_cap error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			 <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
			<p class="jform_bldgsqft error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Ceiling Height</label> <select id="edit_jform_ceiling"
			name="ceiling">
			<option value="">--- Select ---</option>
			<?php
			$selected_ceiling = $data['ceiling_height'];
			for($i=12; $i<=34; $i=$i+2){
					if($i==$selected_ceiling) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="edit_jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			$selected_occupancy = $data['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="edit_jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="datafeatures2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Retail
case 12:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Community Center" <?php echo ($selected_type == "Community Center" ? "selected" : ""); ?>>Community Center</option>
			<option value="Strip Center" <?php echo ($selected_type == "Strip Center" ? "selected" : ""); ?>>Strip Center</option>
			<option value="Outlet Center" <?php echo ($selected_type == "Outlet Center" ? "selected" : ""); ?>>Outlet Center</option>
			<option value="Power Center" <?php echo ($selected_type == "Power Center" ? "selected" : ""); ?>>Power Center</option>
			<option value="Anchor" <?php echo ($selected_type == "Anchor" ? "selected" : ""); ?>>Anchor</option>
			<option value="Restaurant" <?php echo ($selected_type == "Restaurant" ? "selected" : ""); ?>>Restaurant</option>
			<option value="Service Station" <?php echo ($selected_type == "Service Station" ? "selected" : ""); ?>>Service Station</option>
			<option value="Retail Pad" <?php echo ($selected_type == "Retail Pad" ? "selected" : ""); ?>>Retail Pad</option>
			<option value="Free Standing" <?php echo ($selected_type == "Free Standing" ? "selected" : ""); ?>>Free Standing</option>
			<option value="Day Care/Nursery" <?php echo ($selected_type == "Day Care/Nursery" ? "selected" : ""); ?>>Day Care/Nursery</option>
			<option value="Post Office" <?php echo ($selected_type == "Post Office" ? "selected" : ""); ?>>Post Office</option>
			<option value="Vehicle"<?php echo ($selected_type == "Vehicle" ? "selected" : ""); ?>>Vehicle</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select  id="edit_jform_bldgsqft"
			name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
		    <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
    <div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="edit_jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Occupancy</label> <select id="edit_jform_occupancy"
			name="occupancy">
			<option value="">--- Select ---</option>
			<option value="Undisclosed">Undisclosed</option>
			<?php
			$selected_occupancy = $data['occupancy'];
			for($i=100; $i>=0; $i=$i-5){
					if($i==$selected_occupancy) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Motel/Hotel
case 13:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Economy" <?php echo ($selected_type == "Economy" ? "selected" : ""); ?>>Economy</option>
			<option value="Full Service" <?php echo ($selected_type == "Full Service" ? "selected" : ""); ?>>Full Service</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label> <select id="edit_jform_roomcount" name="roomcount" class="reqfield">
			<?php
				$selected_roomcount = $data['room_count'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-9" <?php echo ($selected_roomcount == "1-9" ? "selected" : ""); ?>>1-9</option>
			<option value="10-19" <?php echo ($selected_roomcount == "10-19" ? "selected" : ""); ?>>10-19</option>
			<option value="20-29" <?php echo ($selected_roomcount == "20-29" ? "selected" : ""); ?>>20-29</option>
			<option value="30-39" <?php echo ($selected_roomcount == "30-39" ? "selected" : ""); ?>>30-39</option>
			<option value="40-49" <?php echo ($selected_roomcount == "40-49" ? "selected" : ""); ?>>40-49</option>
			<option value="50-99" <?php echo ($selected_roomcount == "50-99" ? "selected" : ""); ?>>50-99</option>
			<option value="100-149" <?php echo ($selected_roomcount == "100-149" ? "selected" : ""); ?>>100-149</option>
			<option value="150-199" <?php echo ($selected_roomcount == "150-199" ? "selected" : ""); ?>>150-199</option>
			<option value="200" <?php echo ($selected_roomcount == "200" ? "selected" : ""); ?>>200+</option>
		</select>
		<p class="jform_roomcount error_msg" style="margin-left:0px;display:none">This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Restaurant" <?php echo ($selected_features1 == "Restaurant" ? "selected" : ""); ?>>Restaurant</option>
			<option value="Bar" <?php echo ($selected_features1 == "Bar" ? "selected" : ""); ?>>Bar</option>
			<option value="Pool" <?php echo ($selected_features1 == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Banquet Room" <?php echo ($selected_features1 == "Banquet Room" ? "selected" : ""); ?>>Banquet Room</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Restaurant" <?php echo ($selected_features2 == "Restaurant" ? "selected" : ""); ?>>Restaurant</option>
			<option value="Bar" <?php echo ($selected_features2 == "Bar" ? "selected" : ""); ?>>Bar</option>
			<option value="Pool" <?php echo ($selected_features2 == "Pool" ? "selected" : ""); ?>>Pool</option>
			<option value="Banquet Room" <?php echo ($selected_features2 == "Banquet Room" ? "selected" : ""); ?>>Banquet Room</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Assisted Care
case 14:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Assisted" <?php echo ($selected_type == "Assisted" ? "selected" : ""); ?>>Assisted</option>
			<option value="Acute Care" <?php echo ($selected_type == "Acute Care" ? "selected" : ""); ?>>Acute Care</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label> <select id="edit_jform_roomcount"
			name="roomcount" class="reqfield">
			<?php
				$selected_roomcount = $data['room_count'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-9" <?php echo ($selected_roomcount == "1-9" ? "selected" : ""); ?>>1-9</option>
			<option value="10-19" <?php echo ($selected_roomcount == "10-19" ? "selected" : ""); ?>>10-19</option>
			<option value="20-29" <?php echo ($selected_roomcount == "20-29" ? "selected" : ""); ?>>20-29</option>
			<option value="30-39" <?php echo ($selected_roomcount == "30-39" ? "selected" : ""); ?>>30-39</option>
			<option value="40-49" <?php echo ($selected_roomcount == "40-49" ? "selected" : ""); ?>>40-49</option>
			<option value="50-99" <?php echo ($selected_roomcount == "50-99" ? "selected" : ""); ?>>50-99</option>
			<option value="100-149" <?php echo ($selected_roomcount == "100-149" ? "selected" : ""); ?>>100-149</option>
			<option value="150-199" <?php echo ($selected_roomcount == "150-199" ? "selected" : ""); ?>>150-199</option>
			<option value="200" <?php echo ($selected_roomcount == "200" ? "selected" : ""); ?>>200+</option>
		</select>
		<p class="jform_roomcount error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_bldgsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL + Special Purpose
case 15:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Golf" <?php echo ($selected_type == "Golf" ? "selected" : ""); ?>>Golf</option>
			<option value="Marina" <?php echo ($selected_type == "Marina" ? "selected" : ""); ?>>Marina</option>
			<option value="Theater" <?php echo ($selected_type == "Theater" ? "selected" : ""); ?>>Theater</option>
			<option value="Religious" <?php echo ($selected_type == "Religious" ? "selected" : ""); ?>>Religious</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label> <select id="edit_jform_cap" name="cap" class="reqfield">
			<?php
				$selected_caprate = $data['cap_rate'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-.9" <?php echo ($selected_caprate == "0-.9" ? "selected" : ""); ?>>0-.9</option>
			<option value="1-1.9" <?php echo ($selected_caprate == "1-1.9" ? "selected" : ""); ?>>1-1.9</option>
			<option value="2-2.9" <?php echo ($selected_caprate == "2-2.9" ? "selected" : ""); ?>>2-2.9</option>
			<option value="3-3.9" <?php echo ($selected_caprate == "3-3.9" ? "selected" : ""); ?>>3-3.9</option>
			<option value="4-4.9" <?php echo ($selected_caprate == "4-4.9" ? "selected" : ""); ?>>4-4.9</option>
			<option value="5-5.9" <?php echo ($selected_caprate == "5-5.9" ? "selected" : ""); ?>>5-5.9</option>
			<option value="6-6.9" <?php echo ($selected_caprate == "6-6.9" ? "selected" : ""); ?>>6-6.9</option>
			<option value="7-7.9" <?php echo ($selected_caprate == "7-7.9" ? "selected" : ""); ?>>7-7.9</option>
			<option value="8-8.9" <?php echo ($selected_caprate == "8-8.9" ? "selected" : ""); ?>>8-8.9</option>
			<option value="9-9.9" <?php echo ($selected_caprate == "9-9.9" ? "selected" : ""); ?>>9-9.9</option>
			<option value="10-10.9" <?php echo ($selected_caprate == "10-10.9" ? "selected" : ""); ?>>10-10.9</option>
			<option value="11-11.9" <?php echo ($selected_caprate == "11-11.9" ? "selected" : ""); ?>>11-11.9</option>
			<option value="12-12.9" <?php echo ($selected_caprate == "12-12.9" ? "selected" : ""); ?>>12-12.9</option>
			<option value="13-13.9" <?php echo ($selected_caprate == "13-13.9" ? "selected" : ""); ?>>13-13.9</option>
			<option value="14-14.9" <?php echo ($selected_caprate == "14-14.9" ? "selected" : ""); ?>>14-14.9</option>
			<option value="15"<?php echo ($selected_caprate == "15" ? "selected" : ""); ?>>15+</option>
			<option value="Not Disclosed" <?php echo ($selected_caprate == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_cap error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + OFFICE
case 16:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Office" <?php echo ($selected_type == "Office" ? "selected" : ""); ?>>Office</option>
			<option value="Institutional" <?php echo ($selected_type == "Institutional" ? "selected" : ""); ?>>Institutional</option>
			<option value="Medical" <?php echo ($selected_type == "Medical" ? "selected" : ""); ?>>Medical</option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>>Warehouse</option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>>Condo</option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>>R&amp;D</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>>Business Park</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Class</label> <select id="edit_jform_class" name="class" class="reqfield" >
			<?php
				$selected_class = $data['listing_class'];
			?>
			<option value="">--- Select ---</option>
			<option value="4" <?php echo ($selected_class == 4 ? "selected" : ""); ?>>A</option>
			<option value="3" <?php echo ($selected_class == 3 ? "selected" : ""); ?>>B</option>
			<option value="2" <?php echo ($selected_class == 2 ? "selected" : ""); ?>>C</option>
			<option value="1" <?php echo ($selected_class == 1 ? "selected" : ""); ?>>D</option>
			<option value="Not Disclosed" <?php echo ($selected_class == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_class error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Available <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_available" name="available" class="reqfield">
			<?php
				$selected_availablesqft = $data['available_sqft'];
			?>
			<option value="">--- Select ---</option>
            <option value="1-999" <?php echo ($selected_availablesqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_availablesqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_availablesqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_availablesqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_availablesqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_availablesqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_availablesqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_availablesqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_availablesqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_availablesqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_availablesqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_availablesqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_availablesqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_available error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="edit_jform_typelease" name="typelease" class="reqfield">
			<?php
				$selected_typelease = $data['type_lease'];
			?>
			<option value="">--- Select ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>>NNN</option>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> >FSG</option>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>>MG</option>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>>Modified Net</option>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft"
			name="bldgsqft">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
	</div>
	<div style="clear:both"></div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="edit_jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + INDUSTRIAL
case 17:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Flex Space" <?php echo ($selected_type == "Flex Space" ? "selected" : ""); ?>>Flex Space</option>
			<option value="Business Park" <?php echo ($selected_type == "Business Park" ? "selected" : ""); ?>>Business Park</option>
			<option value="Condo" <?php echo ($selected_type == "Condo" ? "selected" : ""); ?>>Condo</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
			<option value="Manufacturing" <?php echo ($selected_type == "Manufacturing" ? "selected" : ""); ?>>Manufacturing</option>
			<option value="Office Showroom" <?php echo ($selected_type == "Office Showroom" ? "selected" : ""); ?>>Office Showroom</option>
			<option value="R&D" <?php echo ($selected_type == "R&D" ? "selected" : ""); ?>>R&amp;D</option>
			<option value="Self/Mini Storage" <?php echo ($selected_type == "Self/Mini Storage" ? "selected" : ""); ?>>Self/Mini Storage</option>
			<option value="Truck Terminal/Hub" <?php echo ($selected_type == "Truck Terminal/Hub" ? "selected" : ""); ?>>Truck Terminal/Hub</option>
			<option value="Warehouse" <?php echo ($selected_type == "Warehouse" ? "selected" : ""); ?>>Warehouse</option>
			<option value="Distribution" <?php echo ($selected_type == "Distribution" ? "selected" : ""); ?>>Distribution</option>
			<option value="Cold Storage" <?php echo ($selected_type == "Cold Storage" ? "selected" : ""); ?>>Cold Storage</option>
			<option value="Land" <?php echo ($selected_type == "Land" ? "selected" : ""); ?>>Land</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="edit_jform_typelease" name="typelease" class="reqfield">
			<?php
				$selected_typelease = $data['type_lease'];
			?>
			<option value="">--- Select ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>>NNN</option>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> >FSG</option>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>>MG</option>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>>Modified Net</option>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Available <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_available" name="available" class="reqfield">
			<?php
				$selected_availablesqft = $data['available_sqft'];
			?>
			<option value="">--- Select ---</option>
            <option value="1-999" <?php echo ($selected_availablesqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_availablesqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_availablesqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_availablesqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_availablesqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_availablesqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_availablesqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_availablesqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_availablesqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_availablesqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_availablesqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_availablesqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_availablesqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_available error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft" name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="1-499" <?php echo ($selected_bldgsqft == "1-499" ?  "selected" : ""); ?> >1-499</option>
			<option value="500-999" <?php echo ($selected_bldgsqft == "500-999" ? "selected" : ""); ?>>500-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-2,499" <?php echo ($selected_bldgsqft == "2,000-2,499" ? "selected" : ""); ?>>2,000-2,499</option>
			<option value="2,500-2,999" <?php echo ($selected_bldgsqft == "2,500-2,999" ? "selected" : ""); ?>>2,500-2,999</option>
			<option value="3,000-3,999" <?php echo ($selected_bldgsqft == "3,000-3,999" ? "selected" : ""); ?>>3,000-3,999</option>
			<option value="4,000-4,999" <?php echo ($selected_bldgsqft == "4,000-4,999" ? "selected" : ""); ?>>4,000-4,999</option>
            <option value="5,000-7,499" <?php echo ($selected_bldgsqft == "5,000-7,499" ? "selected" : ""); ?>>5,000-7,499</option>
			<option value="7,500 -10,000" <?php echo ($selected_bldgsqft == "7,500 -10,000" ? "selected" : ""); ?>>7,500 -10,000</option>
			<option value="10,001-19,999" <?php echo ($selected_bldgsqft == "10,001-19,999" ? "selected" : ""); ?>>10,001-19,999</option>
			<option value="20,000" <?php echo ($selected_bldgsqft == "20,000" ? "selected" : ""); ?>>20,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft" name="lotsqft" class="reqfield">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Ceiling Height</label> <select id="edit_jform_ceiling"
			name="ceiling">
			<option value="">--- Select ---</option>
			<?php
			$selected_ceiling = $data['ceiling_height'];
			for($i=12; $i<=34; $i=$i+2){
					if($i==$selected_ceiling) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div style="clear:both"	></div>
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="edit_jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features1'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	// COMMERCIAL LEASE + RETAIL
case 18:
	?>
	<div class="left ys push-top2">
		<label>Type</label> <select id="edit_jform_type" name="type" class="reqfield">
			<?php
				$selected_type = $data['type'];
			?>
			<option value="">--- Select ---</option>
			<option value="Community Center" <?php echo ($selected_type == "Community Center" ? "selected" : ""); ?>>Community Center</option>
			<option value="Strip Center" <?php echo ($selected_type == "Strip Center" ? "selected" : ""); ?>>Strip Center</option>
			<option value="Outlet Center" <?php echo ($selected_type == "Outlet Center" ? "selected" : ""); ?>>Outlet Center</option>
			<option value="Power Center" <?php echo ($selected_type == "Power Center" ? "selected" : ""); ?>>Power Center</option>
			<option value="Anchor" <?php echo ($selected_type == "Anchor" ? "selected" : ""); ?>>Anchor</option>
			<option value="Restaurant" <?php echo ($selected_type == "Restaurant" ? "selected" : ""); ?>>Restaurant</option>
			<option value="Service Station" <?php echo ($selected_type == "Service Station" ? "selected" : ""); ?>>Service Station</option>
			<option value="Retail Pad" <?php echo ($selected_type == "Retail Pad" ? "selected" : ""); ?>>Retail Pad</option>
			<option value="Free Standing" <?php echo ($selected_type == "Free Standing" ? "selected" : ""); ?>>Free Standing</option>
			<option value="Day Care/Nursery" <?php echo ($selected_type == "Day Care/Nursery" ? "selected" : ""); ?>>Day Care/Nursery</option>
			<option value="Post Office" <?php echo ($selected_type == "Post Office" ? "selected" : ""); ?>>Post Office</option>
			<option value="Vehicle"<?php echo ($selected_type == "Vehicle" ? "selected" : ""); ?>>Vehicle</option>
		</select>
		<p class="jform_type error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Bldg. <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_bldgsqft" name="bldgsqft" class="reqfield">
			<?php
				$selected_bldgsqft = $data['bldg_sqft'];
			?>
			<option value="">--- Select ---</option>
            <option value="1-999" <?php echo ($selected_bldgsqft == "1-999" ?  "selected" : ""); ?> >1-999</option>
			<option value="1,000-1,999" <?php echo ($selected_bldgsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_bldgsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_bldgsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_bldgsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_bldgsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_bldgsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_bldgsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
            <option value="75,000-149,999" <?php echo ($selected_bldgsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
            <option value="150,000-249,000" <?php echo ($selected_bldgsqft == "150,000-249,000" ? "selected" : ""); ?>>150,000-249,000</option>
            <option value="250,000-499,000" <?php echo ($selected_bldgsqft == "250,000-499,000" ? "selected" : ""); ?>>250,000-499,000</option>
			<option value="500,000" <?php echo ($selected_bldgsqft == "500,000" ? "selected" : ""); ?>>500,000+</option>
			<option value="Undisclosed" <?php echo ($selected_bldgsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
		<p class="jform_bldgsqft error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label> <select id="edit_jform_typelease" name="typelease" class="reqfield">
			<?php
				$selected_typelease = $data['type_lease'];
			?>
			<option value="">--- Select ---</option>
			<option value="NNN" <?php echo ($selected_typelease == "NNN" ? "selected" : ""); ?>>NNN</option>
			<option value="FSG" <?php echo ($selected_typelease == "FSG" ? "selected" : ""); ?> >FSG</option>
			<option value="MG" <?php echo ($selected_typelease == "MG" ? "selected" : ""); ?>>MG</option>
			<option value="Modified Net" <?php echo ($selected_typelease == "Modified Net" ? "selected" : ""); ?>>Modified Net</option>
			<option value="Not Disclosed" <?php echo ($selected_typelease == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
		<p class="jform_typelease error_msg" style="display:none;"> This field is required </p>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Stories</label> <select id="edit_jform_stories"
			name="stories">
			<option value="">--- Select ---</option>
			<?php
			$selected_stories = $data['stories'];
			for($i=1; $i<=5; $i++){
					if($i==$selected_stories) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					echo "<option value=\"$i\" $selected>$i</option>";
				}
				?>
			<option value="6+" <?php echo ($selected_stories == "6+" ? "selected" : ""); ?>>6+</option>
			<option value="Vacant" <?php echo ($selected_stories == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label class="changebycountry">Lot <span class="sqftbycountry"><?php echo $defsqft ?></span></label> <select id="edit_jform_lotsqft"
			name="lotsqft">
			<?php
				$selected_lotsqft = $data['lot_sqft'];
			?>
			<option value="">--- Select ---</option>
			<option value="0-999" <?php echo ($selected_lotsqft == "0-999" ? "selected" : ""); ?>>0-999</option>
			<option value="1,000-1,999" <?php echo ($selected_lotsqft == "1,000-1,999" ? "selected" : ""); ?>>1,000-1,999</option>
			<option value="2,000-4,999" <?php echo ($selected_lotsqft == "2,000-4,999" ? "selected" : ""); ?>>2,000-4,999</option>
			<option value="5,000-9,999" <?php echo ($selected_lotsqft == "5,000-9,999" ? "selected" : ""); ?>>5,000-9,999</option>
			<option value="10,000-19,999" <?php echo ($selected_lotsqft == "10,000-19,999" ? "selected" : ""); ?>>10,000-19,999</option>
			<option value="20,000-29,999" <?php echo ($selected_lotsqft == "20,000-29,999" ? "selected" : ""); ?>>20,000-29,999</option>
			<option value="30,000-49,999" <?php echo ($selected_lotsqft == "30,000-49,999" ? "selected" : ""); ?>>30,000-49,999</option>
			<option value="50,000-74,999" <?php echo ($selected_lotsqft == "50,000-74,999" ? "selected" : ""); ?>>50,000-74,999</option>
			<option value="75,000-149,999" <?php echo ($selected_lotsqft == "75,000-149,999" ? "selected" : ""); ?>>75,000-149,999</option>
			<option value="150,000" <?php echo ($selected_lotsqft == "150,000" ? "selected" : ""); ?>>150,000+</option>
			<option value="Undisclosed" <?php echo ($selected_lotsqft == "Undisclosed" ? "selected" : ""); ?>>Undisclosed</option>
		</select>
        <p class="jform_lotsqft error_msg" style="display:none"> This field is required </p>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label> <select id="edit_jform_yearbuilt"
			name="yearbuilt">
			<?php
				$selected_yearbuilt = $data['year_built'];
			?>
			<option value="">--- Select ---</option>
			<option value="2016" <?php echo ($selected_yearbuilt == "2016" ? "selected" : ""); ?>>2016</option>
			<option value="2015" <?php echo ($selected_yearbuilt == "2015" ? "selected" : ""); ?>>2015</option>
			<option value="2014" <?php echo ($selected_yearbuilt == "2014" ? "selected" : ""); ?>>2014</option>
			<option value="2013" <?php echo ($selected_yearbuilt == "2013" ? "selected" : ""); ?>>2013</option>
			<option value="2012" <?php echo ($selected_yearbuilt == "2012" ? "selected" : ""); ?>>2012</option>
			<option value="2011" <?php echo ($selected_yearbuilt == "2011" ? "selected" : ""); ?>>2011</option>
			<option value="2010" <?php echo ($selected_yearbuilt == "2010" ? "selected" : ""); ?>>2010</option>
			<option value="2009-2000"<?php echo ($selected_yearbuilt == "2009-2000" ? "selected" : ""); ?>>2009-2000</option>
			<option value="1999-1990" <?php echo ($selected_yearbuilt == "1999-1990" ? "selected" : ""); ?>>1999-1990</option>
			<option value="1989-1980" <?php echo ($selected_yearbuilt == "1989-1980" ? "selected" : ""); ?>>1989-1980</option>
			<option value="1979-1970" <?php echo ($selected_yearbuilt == "1979-1970" ? "selected" : ""); ?>>1979-1970</option>
			<option value="1969-1960" <?php echo ($selected_yearbuilt == "1969-1960" ? "selected" : ""); ?>>1969-1960</option>
			<option value="1959-1950" <?php echo ($selected_yearbuilt == "1959-1950" ? "selected" : ""); ?>>1959-1950</option>
			<option value="1949-1940" <?php echo ($selected_yearbuilt == "1949-1940" ? "selected" : ""); ?>>1949-1940</option>
			<option value="1939-1930" <?php echo ($selected_yearbuilt == "1939-1930" ? "selected" : ""); ?>>1939-1930</option>
			<option value="1929-1920" <?php echo ($selected_yearbuilt == "1929-1920" ? "selected" : ""); ?>>1929-1920</option>
			<option value="< 1919" <?php echo ($selected_yearbuilt == "< 1919" ? "selected" : ""); ?>>< 1919</option>
			<option value="Not Disclosed" <?php echo ($selected_yearbuilt == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label> <select id="edit_jform_parking"
			name="parking">
			<?php
				$selected_parking = $data['parking_ratio'];
			?>
			<option value="">--- Select ---</option>
			<option value="1/1000" <?php echo ($selected_parking == "1/1000" ? "selected" : ""); ?>>1/1000</option>
			<option value="1.5/1000" <?php echo ($selected_parking == "1.5/1000" ? "selected" : ""); ?>>1.5/1000</option>
			<option value="2/1000" <?php echo ($selected_parking == "2/1000" ? "selected" : ""); ?>>2/1000</option>
			<option value="2.5/1000" <?php echo ($selected_parking == "2.5/1000" ? "selected" : ""); ?>>2.5/1000</option>
			<option value="3/1000" <?php echo ($selected_parking == "3/1000" ? "selected" : ""); ?>>3/1000</option>
			<option value="3.5/1000" <?php echo ($selected_parking == "3.5/1000" ? "selected" : ""); ?>>3.5/1000</option>
			<option value="4/1000" <?php echo ($selected_parking == "4/1000" ? "selected" : ""); ?>>4/1000</option>
			<option value="4.5/1000" <?php echo ($selected_parking == "4.5/1000" ? "selected" : ""); ?>>4.5/1000</option>
			<option value="5/1000" <?php echo ($selected_parking == "5/1000" ? "selected" : ""); ?>>5/1000</option>
			<option value="other" <?php echo ($selected_parking == "other" ? "selected" : ""); ?>>other</option>
		</select>
	</div>
	<!---------------------------------------------------------------- -->
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features1"
			name="features1">
			<?php
				$selected_features1 = $data['features'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features1 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features1 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features1 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features1 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features1 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features1 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features1 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label> <select id="edit_jform_features2"
			name="features2">
			<?php
				$selected_features2 = $data['features2'];
			?>
			<option value="">--- Select ---</option>
			<option value="Mixed use" <?php echo ($selected_features2 == "Mixed use" ? "selected" : ""); ?>>Mixed use</option>
			<option value="Single Tenant" <?php echo ($selected_features2 == "Single Tenant" ? "selected" : ""); ?>>Single Tenant</option>
			<option value="Multiple Tenant" <?php echo ($selected_features2 == "Multiple Tenant" ? "selected" : ""); ?>>Multiple Tenant</option>
			<option value="Seller Carry" <?php echo ($selected_features2 == "Seller Carry" ? "selected" : ""); ?>>Seller Carry</option>
			<option value="Net-Leased" <?php echo ($selected_features2 == "Net-Leased" ? "selected" : ""); ?>>Net-Leased</option>
			<option value="Owner User" <?php echo ($selected_features2 == "Owner User" ? "selected" : ""); ?>>Owner User</option>
			<option value="Vacant" <?php echo ($selected_features2 == "Vacant" ? "selected" : ""); ?>>Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2" style="display:none;">
		<label>Type Lease</label> <select id="jform_typelease2"
			name="typelease2">
			<?php
				$selected_typelease2 = $buyer_data[0]['needs'][0]['type_lease2'];
			?>
			<option value="">--- Select ---</option>
			<option value="NNN" <?php echo ($selected_typelease2 == "NNN" ? "selected" : ""); ?>>NNN</option>
			<option value="FSG" <?php echo ($selected_typelease2 == "FSG" ? "selected" : ""); ?> >FSG</option>
			<option value="MG" <?php echo ($selected_typelease2 == "MG" ? "selected" : ""); ?>>MG</option>
			<option value="Modified Net" <?php echo ($selected_typelease2 == "Modified Net" ? "selected" : ""); ?>>Modified Net</option>
			<option value="Not Disclosed" <?php echo ($selected_typelease2 == "Not Disclosed" ? "selected" : ""); ?>>Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!---------------------------------------------------------------- -->
	<?php
	break;
	}
	?>
</div>
<?php
die();
}
	public function editproperty() {
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
		$model = $this->getModel($this->getName());
		$individual = $model->getSelectedListing($_REQUEST['lID']);
		$user = JFactory::getUser();
		$user_info = $this->get_user_registration($individual[0]->user_id);
		$currency_data = $this->getCountryCurrencyData($user_info->country); 
		#$property_type = JRequest::getVar('ptype');
		#$property_subtype = JRequest::getVar('stype');
		#$country = $propertylisting_model->getCountry($user->id);		
		#$setting = $propertylisting_model->getPermissionSetting($_GET['lID']);		
		$state = $model->printPropertyState($individual[0]->state);
		$images = array();
		$images = $model->get_property_image($_GET['lID']);
		if(!empty($task)) $tpl = $data['task'];
		$getCountryLangsInitial = $this->getCountryDataByID($individual[0]->conCode);
		#echo "<pre>", print_r($images), "</pre>";
		#echo "<br />";
		$property = new JObject();
		$property->images = $images; 
		$property->pop = $individual;
		$property->pop[0]->type_name = JText::_($property->pop[0]->type_name);
		$property->pop[0]->name = JText::_($property->pop[0]->name);
		$property->state_text = $state->zone_name;
		$property->currency_data = $currency_data;
		$property->getCountryLangsInitial = $getCountryLangsInitial;
		echo json_encode($property);
		exit;
	}
	function getCountryCurrencyData($cID){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->setQuery("SELECT cur.* FROM #__country_currency AS cur WHERE country=".$cID);
		$db->setQuery($query);		
		return $db->loadObjectList();
	}
	public function get_user_registration($uid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('ur.*,us.*,u.*,conc.*,concva.countryCode');
		$query->from('#__users u');
		$query->where('u.id ='.$uid);
		$query->leftJoin('#__user_registration ur ON u.email = ur.email');
		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');
		$query->leftJoin('#__country_currency conc ON conc.country = ur.country');
		$query->leftJoin('#__country_validations concva ON concva.country = ur.country');
		$db->setQuery($query);
		$db->loadObject()->firstname = str_replace("\\", "", $db->loadObject()->firstname);
		$db->loadObject()->lastname = str_replace("\\", "", $db->loadObject()->lastname);
		return $db->loadObject();
	}
	
	function getMonacoCities(){
		$model = $this->getModel($this->getName());
		$datas = $model->getMCcities();
		$conv_datas = array();
		foreach ($datas as $value) {
			$conv_datas[] = array("city" => $value);
		}
		echo json_encode($datas);
	}
	function getCountryCities(){
		$country_id = $_POST['id_c'];
		$postcode = $_POST['postcode'];
		$model = $this->getModel($this->getName());
		$datas = $model->getCCities($country_id,$postcode);
		$conv_datas = array();
		foreach ($datas as $value) {
			$conv_datas[] = array("city" => $value);
		}
		echo json_encode($datas);
	}
	
	function getPartialGBCodeStates(){
		$postcode = $_POST['partialCode'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__country_partialszip_states'));
		$query->where($db->quoteName('postcode')." = ".$db->quote($postcode));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		$states_obj = $db->loadAssoc();
		$states_ret = array();
		$keys_not_included=array("postcode","country","id");
		foreach ($states_obj as $key => $value) {
			if(!in_array($key, $keys_not_included) && $value){
				$states_ret[]=$value;
			}
		}
		echo json_encode($states_ret);
	}
}
?>