<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class AbAnalyticsController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {
		
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		#echo $interval; exit;
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$sold_condition = ($only_active) ? " AND sold = 0" : "";

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('p.listing_id, p.property_name, pp.price_type, p.zip, p.state, p.country, pp.price1, pp.price2, pp.price_type, p.date_created, p.date_expired'));

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = p.sub_type');

		$query->leftJoin('#__users u ON u.id = p.user_id');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');
		
		$query->where('ugm.group_id NOT IN(7,8,11,12)' . $sold_condition);
		
		$query->order('p.date_created ASC');
	//	$query->where('closed!=1');

				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();
		
		$end_key = count($results) - 1;
		$start_date = $results[0]->date_created;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$end_date = $results[$end_key]->date_created;
		$str_end_totime = strtotime($end_date);
		
		$arr_stats = array();
		$previous_display_date = "";
		while($str_current_totime < $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
					}
				} else {
					break;
				}
			}
			
			$previous_display_date = $display_date;
			$str_current_totime = strtotime($current_date . $interval);
			
		}
		if($rformat == "weekly") {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
					}
				} else {
					break;
				}
			}
		}
		

		#echo $interval; exit;
		#echo "<pre>", print_r($arr_stats), "</pre>";
		

		
		$query2 = $db->getQuery(true);
		$expired_condition = ($only_active) ? " AND p.date_expired > NOW() AND p.sold = 0" : "";
		$query2->select(array('pt.type_name, pt.type_id, COUNT(p.listing_id) as pops_count'));
		$query2->from("#__pocket_listing p");
		$query2->leftJoin('#__users u ON u.id = p.user_id');
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin("#__property_type pt ON pt.type_id = p.property_type");
		$query2->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query2->group("p.property_type");
		
		$db->setQuery($query2);
		$count_per_type = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_type), "</pre>";
		
		$query3 = $db->getQuery(true);

		$query3->select(array('st.name, st.property_id, COUNT(p.listing_id) as pops_count'));
		$query3->from("#__pocket_listing p");
		$query3->leftJoin('#__users u ON u.id = p.user_id');
		$query3->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query3->leftjoin("#__property_sub_type st ON st.sub_id = p.sub_type");
		$query3->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query3->group("p.sub_type");
		
		$db->setQuery($query3);
		$count_per_subtype = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_subtype), "</pre>";
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'arr_stats', $arr_stats );
		$view->assignRef( 'only_active', $only_active );
		$view->assignRef( 'count_per_type', $count_per_type );
		$view->assignRef( 'count_per_subtype', $count_per_subtype );
		
		
		$task = JRequest::getVar('task');

		switch ($task):

			case "export_csv":
				generateCSV();
			break;
		
		endswitch;
		
        parent::display();

    }
	
	
	function userReport() {
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select(array("u.id, u.registerDate, ur.*, it.invited_email"));
		$query->from("#__users as u");
		$query->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query->leftjoin('#__invitation_tracker it ON it.invited_email = ur.email');
		$query->where('ugm.group_id NOT IN(7,8,11,12)');
		$query->order("u.registerDate ASC");
		
		$db->setQuery($query);

		$results = $db->loadObjectList();
		#echo "<pre>", print_r($results), "</pre>"; exit;
		$end_key = count($results) - 1;
		$start_date = $results[0]->registerDate;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$end_date = $results[$end_key]->registerDate;
		$str_end_totime = strtotime(date("Y-m-d"));

		$arr_stats = array();
		$previous_display_date = $str_current_totime;
		while($str_current_totime < $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			$arr_stats[$display_date]['invited_count'] = 0;
			$arr_stats[$display_date]['terms_accepted_count'] = 0;
			foreach($results as $key => $user) {
				$registration_date = strtotime($user->registerDate);
				$invited_date = strtotime($user->registerDate);
				$terms_accepted_date = strtotime($user->completeReg_date);
				
				if($registration_date <= $str_current_totime) {
					$arr_stats[$display_date]['member_count'] += 1;
				}
				
				if($previous_display_date == $str_current_totime) {
					if($user->invited_email && $invited_date <= $str_current_totime) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $user->is_term_accepted > 0 && $terms_accepted_date <= $str_current_totime) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				} else {
					if($user->invited_email && $invited_date <= $str_current_totime && $invited_date > $previous_date) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $registration_date > $previous_date && $user->is_term_accepted > 0  && $terms_accepted_date <= $str_current_totime  && $terms_accepted_date > $previous_date) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				}
				
				if($registration_date > $str_current_totime) {
					break;
				}
			}
			
			$previous_date = $str_current_totime;
			$str_current_totime = strtotime($current_date . $interval);
		}
		
		
		
		$query2 = $db->getQuery(true);
		$query2->select(array("COUNT(u.id) as user_count, c.countries_name, ur.country"));
		$query2->from("#__users as u");
		$query2->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin('#__countries c ON c.countries_id = ur.country');
		$query2->group("ur.country");
		$query2->where('ugm.group_id NOT IN(7,8,11,12)');
		
		$db->setQuery($query2);
		
		$results = $db->loadObjectList();
		
		$arr_user_count_per_country = array();
		foreach($results as $country) {
			if(!$country->country || $country->country == 223) {
				$arr_user_count_per_country['US'] += $country->user_count;
			} else {
				$arr_user_count_per_country['INTL'] += $country->user_count;
			}
		}
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'arr_stats', $arr_stats );
		$view->assignRef( 'only_active', $only_active );
		$view->assignRef( 'arr_user_count_per_country', $arr_user_count_per_country );
		$view->display($this->getTask());
		
	}
	
    function export_csv()

	{
	
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;

		die();
	}

		   ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateCSV(){		
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		#echo $interval; exit;
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$sold_condition = ($only_active) ? " AND sold = 0" : "";

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('p.listing_id, p.property_name, pp.price_type, p.zip, p.state, p.country, pp.price1, pp.price2, pp.price_type, p.date_created, p.date_expired'));

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = p.sub_type');

		$query->leftJoin('#__users u ON u.id = p.user_id');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');
		
		$query->where('ugm.group_id NOT IN(7,8,11,12)' . $sold_condition);
		
		$query->order('p.date_created ASC');
	//	$query->where('closed!=1');

				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();
		
		$end_key = count($results) - 1;
		$start_date = $results[0]->date_created;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$end_date = $results[$end_key]->date_created;
		$str_end_totime = strtotime($end_date);
		
		$arr_stats = array();
		$previous_display_date = "";
		while($str_current_totime < $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
					}
				} else {
					break;
				}
			}
			
			$previous_display_date = $display_date;
			$str_current_totime = strtotime($current_date . $interval);
			
		}
		if($rformat == "weekly") {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
					}
				} else {
					break;
				}
			}
		}
		

		#echo $interval; exit;
		#echo "<pre>", print_r($arr_stats), "</pre>";
		

		
		$query2 = $db->getQuery(true);
		$expired_condition = ($only_active) ? " AND p.date_expired > NOW() AND p.sold = 0" : "";
		$query2->select(array('pt.type_name, pt.type_id, COUNT(p.listing_id) as pops_count'));
		$query2->from("#__pocket_listing p");
		$query2->leftJoin('#__users u ON u.id = p.user_id');
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin("#__property_type pt ON pt.type_id = p.property_type");
		$query2->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query2->group("p.property_type");
		
		$db->setQuery($query2);
		$count_per_type = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_type), "</pre>";
		
		$query3 = $db->getQuery(true);

		$query3->select(array('st.name, st.property_id, COUNT(p.listing_id) as pops_count'));
		$query3->from("#__pocket_listing p");
		$query3->leftJoin('#__users u ON u.id = p.user_id');
		$query3->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query3->leftjoin("#__property_sub_type st ON st.sub_id = p.sub_type");
		$query3->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query3->group("p.sub_type");
		
		$db->setQuery($query3);
		$count_per_subtype = $db->loadAssocList();
		
		require_once dirname(__FILE__) . '/../../../Classes/PHPExcel.php';
		
		$objPHPExcel = new PHPExcel();
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$objWorksheet->getColumnDimension("A")->setAutoSize(true);
		$objWorksheet->getColumnDimension("B")->setAutoSize(true);
		$objWorksheet->getColumnDimension("C")->setAutoSize(true);
		$objWorksheet->getColumnDimension("D")->setAutoSize(true);
		
		
		
		$worksheet_table = array();
		$worksheet_table[] = array('Date', 'Number of POPs', 'Value of POPs', 'Average Price');
		$end_data = count($arr_stats)+1;
		foreach($arr_stats as $key => $stat) {
			$worksheet_table[] = array($key, $stat['pops_count'], $stat['price'], $stat['average']);
		}
		$objWorksheet->fromArray($worksheet_table);
		
		$index = 1;
		
		$pie_array = array();
		
		$pie_cells1 = "";
		$pie_cells2 = "";
		foreach($count_per_type as $type) {
			$objWorksheet->setCellValue("G$index", JText::_($type['type_name']));
			$objWorksheet->setCellValue("H$index", $type['pops_count']);
			
			$objWorksheet->getStyle("G$index:H$index")->getFill()->applyFromArray(array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'DDDDDD'
				)
			));
			$pie_cells1 .= ($pie_cells1) ? "," : "";
			$pie_cells1 .= 'Worksheet!$G$' . $index;
			
			$pie_cells2 .= ($pie_cells2) ? "," : "";
			$pie_cells2 .= 'Worksheet!$H$' . $index;
			
			
			$index=$index+1;
			foreach($count_per_subtype as $sub_type) {
				if($type['type_id'] == $sub_type['property_id']) {					
					$objWorksheet->setCellValue("G$index", JText::_($sub_type['name']));
					$objWorksheet->setCellValue("H$index", $sub_type['pops_count']);
					$index=$index+1;
				}
			}			
		}
		
		$objWorksheet->getColumnDimension("G")->setAutoSize(true);
		$objWorksheet->getColumnDimension("H")->setAutoSize(true);
		
		
		$objWorksheet->getStyle("B1:B$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objWorksheet->getStyle("C1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objWorksheet->getStyle("C2:C$end_data")->getNumberFormat()->setFormatCode("\$ #,##0.00");
		$objWorksheet->getStyle("D2:D$end_data")->getNumberFormat()->setFormatCode("\$ #,##0.00");
		
		$dataSeriesLabels = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1));
		$xAxisTickValues = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1));
		$dataSeriesValues = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$'.$end_data, NULL, 1));
		
		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dataSeriesValues)-1),			// plotOrder
			$dataSeriesLabels,								// plotLabel
			$xAxisTickValues,								// plotCategory
			$dataSeriesValues								// plotValues
		);
		
		//	Set the series in the plot area
		$plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
		//	Set the chart legend
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

		$title = new PHPExcel_Chart_Title('Value of Total POPs');
		$yAxisLabel = new PHPExcel_Chart_Title('Value of POPs');


		//	Create the chart
		$chart = new PHPExcel_Chart(
			'chart1',		// name
			$title,			// title
			$legend,		// legend
			$plotArea,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			$yAxisLabel		// yAxisLabel
		);

		//	Set the position where the chart should appear in the worksheet
		$chart->setTopLeftPosition('A' . intval($end_data + 3));
		$chart->setBottomRightPosition('K' . intval($end_data + 33));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart);
		
		
		
		#echo $pie_cells1;
		#echo $pie_cells2; exit;
		
		
		$dataSeriesLabels1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', '', NULL, 1),	//	2011
		);
		
		$xAxisTickValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', "(" . $pie_cells1 . ")", NULL, 1),	//	Q1 to Q4
		);
		
		$dataSeriesValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('Number', "(" . $pie_cells2 . ")", NULL, 1),
		);
		
		//	Build the dataseries
		$series1 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
			NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
			range(0, count($dataSeriesValues1)-1),					// plotOrder
			$dataSeriesLabels1,										// plotLabel
			$xAxisTickValues1,										// plotCategory
			$dataSeriesValues1										// plotValues
		);
		
		//	Set up a layout object for the Pie chart
		$layout1 = new PHPExcel_Chart_Layout();
		$layout1->setShowVal(TRUE);
		$layout1->setShowPercent(TRUE);

		//	Set the series in the plot area
		$plotArea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
		//	Set the chart legend
		$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title1 = new PHPExcel_Chart_Title('Property Types');


		//	Create the chart
		$chart1 = new PHPExcel_Chart(
			'chart1',		// name
			$title1,		// title
			$legend1,		// legend
			$plotArea1,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
		);

		//	Set the position where the chart should appear in the worksheet
		$chart1->setTopLeftPosition('L' . intval($end_data + 3));
		$chart1->setBottomRightPosition('X' . intval($end_data + 33));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart1);
		
		
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=file.xlsx");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("php://output");
		exit;
	
	}

}



?>