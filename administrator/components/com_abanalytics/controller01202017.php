<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class AbAnalyticsController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {
		
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		#echo $interval; exit;
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('p.listing_id, p.property_name, pp.price_type, p.zip, p.state, p.country, pp.price1, pp.price2, pp.price_type, p.date_created, p.date_expired'));

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = p.sub_type');

		$query->leftJoin('#__users u ON u.id = p.user_id');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');

		$query->where('ur.is_term_accepted = 1 AND ugm.group_id NOT IN(7,8,11,12)');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');
		
		$query->where('ugm.group_id NOT IN(7,8,11,12)');
		
		$query->order('p.date_created ASC');
	//	$query->where('closed!=1');

				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();
		
		$end_key = count($results) - 1;
		$start_date = $results[0]->date_created;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$end_date = $results[$end_key]->date_created;
		$str_end_totime = strtotime($end_date);
		
		$arr_stats = array();
		$previous_display_date = "";
		while($str_current_totime < $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
					}
				} else {
					break;
				}
			}
			
			$previous_display_date = $display_date;
			$str_current_totime = strtotime($current_date . $interval);
			
		}
		if($rformat == "weekly") {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'], 2);
					}
				} else {
					break;
				}
			}
		}
		

		#echo $interval; exit;
		#echo "<pre>", print_r($arr_stats), "</pre>";
		

		
		$query2 = $db->getQuery(true);
		$expired_condition = ($only_active) ? " AND p.date_expired > NOW()" : "";
		$query2->select(array('pt.type_name, pt.type_id, COUNT(p.listing_id) as pops_count'));
		$query2->from("#__pocket_listing p");
		$query2->leftJoin('#__users u ON u.id = p.user_id');
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin("#__property_type pt ON pt.type_id = p.property_type");
		$query2->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query2->group("p.property_type");
		
		$db->setQuery($query2);
		$count_per_type = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_type), "</pre>";
		
		$query3 = $db->getQuery(true);

		$query3->select(array('st.name, st.property_id, COUNT(p.listing_id) as pops_count'));
		$query3->from("#__pocket_listing p");
		$query3->leftJoin('#__users u ON u.id = p.user_id');
		$query3->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query3->leftjoin("#__property_sub_type st ON st.sub_id = p.sub_type");
		$query3->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition);
		$query3->group("p.sub_type");
		
		$db->setQuery($query3);
		$count_per_subtype = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_subtype), "</pre>";
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'arr_stats', $arr_stats );
		$view->assignRef( 'only_active', $only_active );
		$view->assignRef( 'count_per_type', $count_per_type );
		$view->assignRef( 'count_per_subtype', $count_per_subtype );
		
        parent::display();

    }

    function export_csv()

	{
	
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;

		die();
	}

		   ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateCSV(){
	    $language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language->load($extension, $base_dir, "english-US", true);
		  
	      ## Make DB connections
	      $db    = JFactory::getDBO();
	       
			$query = $db->getQuery(true);

			$query->select(

				array(

					'pl.listing_id,

					 pl.property_name, 

					 pt.type_name AS property_type,

					 ps.name AS property_sub_type,

					 u.name AS agent,

					 ur.email,

					 umn.value AS contact_number,

					 ur.user_type,

					 pl.zip AS zip_code, 

					 pl.city, 

					 z.zone_name, 

					 pp.price1 AS price_1,

					 pp.price2 AS price_2,

					 pl.currency,
					 
					 pl.date_expired AS expiry_date'

				)

			);



			$query->from('#__pocket_listing pl');

			$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');

			$query->leftJoin('#__property_type pt ON pt.type_id = pl.property_type');

			$query->leftJoin('#__property_sub_type ps ON ps.sub_id = pl.sub_type');

			$query->leftJoin('#__users u ON u.id = pl.user_id');
			
			$query->leftJoin('#__user_registration ur ON ur.email = u.email');

			$query->leftJoin('#__user_mobile_numbers umn ON umn.user_id = ur.user_id');

			$query->leftJoin('#__zones z ON z.zone_id = pl.state');

			$query->group('pl.listing_id');
	      
	      $db->setQuery($query);
	      $rows = $db->loadAssocList();

	       $time = time();
	      
	      ## If the query doesn't work..
	      if (!$db->query() ){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      
	      $fields = (array_keys($rows[0]));
	      
	      ## Count all fields(will be the collumns
	      $columns = count($fields); 
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	       if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	}else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      
	      $data .= "\n";
	      
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="price_type"){
	         	 $value = $value == 1 ? "Price Range" : "Exact Price";  
	         	} else if($key=="price1" || $key=="price2"){
	         	 $value = $value!=NULL ? "$".$value : "";
				} else if($key=="property_type" || $key=="property_sub_type"){
				 $value = JText::_($value);
	         	} else
	         	 $value = str_replace(array("\\","/",","), "",  $value);

	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      
	      $data = str_replace("\r","",$data);
	      
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      
	      ## Push the report now!
	      $this->name = 'POPs™-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }

}



?>