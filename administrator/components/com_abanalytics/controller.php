<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 

require_once dirname(__FILE__) . '/../../../Classes/PHPExcel.php';

 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class AbAnalyticsController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {
		
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$sold_condition = ($only_active) ? " AND sold = 0" : "";

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('p.listing_id, p.property_name, pp.price_type, p.zip, p.state, p.country, pp.price1, pp.price2, pp.price_type, p.date_created, p.date_expired'));

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = p.sub_type');

		$query->leftJoin('#__users u ON u.id = p.user_id');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');
		
		$query->where('ugm.group_id NOT IN(7,8,11,12)' . $sold_condition);
		
		$query->order('p.date_created ASC');
	//	$query->setLimit(20);
	//	$query->where('closed!=1');

				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();
		
		switch($rformat) {
			case "daily":
				$interval = " + 8 hours";
				$display_format = "Y-m-d H:i";
				$compare_format = "Y-m-d H:i";
				
				$select_date = $_GET['select_date'];
				$start_date = $_GET['select_date'] . "00:00";
				$end_date = $_GET['select_date'] . "23:59";
				break;
			case "weekly":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				
				$select_year = $_GET['select_year'];
				$select_week = $_GET['select_week'];
				
				$start_date = $select_week;
				$end_date = date("Y-m-d", strtotime($select_week . " + 6 days"));
				/*
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				*/
				break;
			case "monthly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				/*
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				*/
				
				$select_year = $_GET['select_year'];
				$select_month = $_GET['select_month'];
				
				$start_date = $select_year . "-" . $select_month . "-01";
				if(strtotime(date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"))) > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-t");
				} else {
					$end_date = date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"));
				}
				break;
			case "yearly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				/*
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				*/
				$select_year = $_GET['select_year'];
				
				$start_date = $select_year . "-01-01";
				if(strtotime($select_year . "-12-31") > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-d");
				} else {
					$end_date = $select_year . "-12-31";
				}
				
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				$start_date = $results[0]->date_created;
				$end_date = date("Y-m-d 23:59:59");
		}
		#echo $interval; exit;
		
		$end_key = count($results) - 1;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$str_end_totime = strtotime($end_date);
		
		$arr_stats = array();
		$previous_display_date = "";
		$first_zero = "";
		
		$arr_y_axis1 = array();
		$arr_y_axis2 = array();
		
		while($str_current_totime <= $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format(ceil($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count']));
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format(ceil($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count']));						
					}
					
					
				} else {
					break;
				}
			}
			
			if(!intval($arr_stats[$display_date]['pops_count'])) {
				$arr_stats[$display_date]['average'] = number_format(0, 2);
				if(!$first_zero) {
					$first_zero = $display_date;
				}
			}
			
			$arr_y_axis1[] = $arr_stats[$display_date]['price'];
			$arr_y_axis2[] = $arr_stats[$display_date]['pops_count'];
			
			$previous_display_date = $display_date;
			$str_current_totime = strtotime($current_date . $interval);
			
		}
		
		if($str_current_totime > strtotime($end_date)) {
			$current_date = date($compare_format, strtotime($end_date));
			$display_date = date($display_format, strtotime($end_date));
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = number_format(ceil($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count']));
							//$arr_stats[$display_date]['average'] = (!$arr_stats[$display_date]['average']) ? number_format("0", 2) : $arr_stats[$display_date]['average'];
							if(!$arr_stats[$display_date]['pops_count']) {
								$arr_stats[$display_date]['average'] = number_format(0);
							}
							if($previous_display_date) {
								if($arr_stats[$display_date]['price'] > $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "up";
								} else if($arr_stats[$display_date]['price'] < $arr_stats[$previous_display_date]['price']) {
									$arr_stats[$display_date]['diff'] = "down";
								} else {
									$arr_stats[$display_date]['diff'] = "even";
								}
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = number_format(ceil($arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count']));
					}
				} else {
					break;
				}
			}
			
			if(!intval($arr_stats[$display_date]['pops_count'])) {
				$arr_stats[$display_date]['average'] = number_format(0);
			}
			
			$arr_y_axis1[] = $arr_stats[$display_date]['price'];
			$arr_y_axis2[] = $arr_stats[$display_date]['pops_count'];
		}
		
		if($rformat !== "") {
			$max = max($arr_y_axis1);
			$min = min($arr_y_axis1);
			$diff_min_max = $max - $min;
			
			$_min_value = $min;
			
			$_max_value = substr($max, 0, 2);
			for($i=0; $i<strlen($max)-2; $i++) {
				$_max_value .= 0;
			}
			
			$minus = 1;
			while($_min_value < $diff_min_max) {
				$minus++;
				$diff_min_max = substr($diff_min_max, 1, strlen($diff_min_max));
			}
			
			if($rformat == "daily") {
				$_interval = 1000000;
				$_min_value = $min;
			
				$_max_value = $max + $_interval;

				$ticks = array();
				#echo "Min Max Value:" . $_min_value . "-" . $_max_value;
				while($_min_value <= $_max_value) {
					if(strlen($_min_value) >= 6 && strlen($_min_value) <= 9) {
						$multiplier = 0.000001;
						$_suffix = "M";
					} else if(strlen($_min_value) >= 10 && strlen($_min_value) <= 13) {
						$multiplier = 0.000000001;
						$_suffix = "B";
					}
					
					$current_f = number_format(($_min_value * $multiplier), 3);
					$_f = ($multiplier && $_suffix) ? $current_f . $_suffix : $_min_value;
					
					$ticks[] = array(
						"v" => $_min_value,
						"f" => $_f
					);
					$_min_value += $_interval;
				}
				
				$ticks = json_encode($ticks);
			} else if($rformat == "weekly") {
				$_interval = 5000000;
				
				$_min_value = $min;
			
				$_max_value = $max + $_interval;

				$ticks = array();
				#echo "Min Max Value:" . $_min_value . "-" . $_max_value;
				while($_min_value <= $_max_value) {
					if(strlen($_min_value) >= 6 && strlen($_min_value) <= 9) {
						$multiplier = 0.000001;
						$_suffix = "M";
					} else if(strlen($_min_value) >= 10 && strlen($_min_value) <= 13) {
						$multiplier = 0.000000001;
						$_suffix = "B";
					}
					
					$current_f = number_format(($_min_value * $multiplier), 4);
					$_f = ($multiplier && $_suffix) ? $current_f . $_suffix : $_min_value;
					
					$ticks[] = array(
						"v" => $_min_value,
						"f" => $_f
					);
					$_min_value += $_interval;
				}

				$ticks = json_encode($ticks);
			} else {
				$_interval = 1;
				for($i=0; $i<strlen($diff_min_max)-$minus; $i++) {
					$_interval .= 0;
				}
			}
		}
		
		#echo $interval; exit;
		#echo "<pre>", print_r($arr_stats), "</pre>";
		

		
		$query2 = $db->getQuery(true);
		
		if($rformat !== "") {
			$expired_condition = ($only_active) ? " AND p.date_expired > '" . $end_date . "' AND p.sold = 0" : "";
		} else {
			$expired_condition = ($only_active) ? " AND p.date_expired > NOW() AND p.sold = 0" : "";
		}
		
		$query2->select(array('pt.type_name, pt.type_id, COUNT(p.listing_id) as pops_count'));
		$query2->from("#__pocket_listing p");
		$query2->leftJoin('#__users u ON u.id = p.user_id');
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin("#__property_type pt ON pt.type_id = p.property_type");
		$query2->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition . " AND p.date_created <= '" . $end_date . "'");
		$query2->group("p.property_type");

		$db->setQuery($query2);
		/*
		$db->setQuery("
			SELECT COUNT(x.listing_id) as pops_count, x.type_name, x.type_id 
			FROM(
				SELECT p.listing_id, p.property_type, pt.type_name, pt.type_id
				FROM tbl_pocket_listing p
				LEFT JOIN tbl_users u ON u.id = p.user_id
				LEFT JOIN tbl_user_usergroup_map ugm ON ugm.user_id = u.id
				LEFT JOIN tbl_property_type pt ON pt.type_id = p.property_type
				WHERE ugm.group_id NOT IN ( 7, 8, 11, 12 ) $expired_condition 
				ORDER BY p.listing_id ASC
				LIMIT 0, 20
			) as x
			GROUP BY x.property_type
			"
		);
		*/
		$count_per_type = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_type), "</pre>";
		
		$query3 = $db->getQuery(true);

		$query3->select(array('st.name, st.property_id, COUNT(p.listing_id) as pops_count'));
		$query3->from("#__pocket_listing p");
		$query3->leftJoin('#__users u ON u.id = p.user_id');
		$query3->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query3->leftjoin("#__property_sub_type st ON st.sub_id = p.sub_type");
		$query3->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition . " AND p.date_created <= '" . $end_date . "'");
		$query3->group("p.sub_type");
		//$query3->setLimit(20);
		
		$db->setQuery($query3);
		/*
		$db->setQuery("
			SELECT x.name, x.property_id, COUNT(x.listing_id) as pops_count
			FROM(
				SELECT p.listing_id, st.name, st.property_id, p.sub_type
				FROM tbl_pocket_listing p
				LEFT JOIN tbl_users u ON u.id = p.user_id
				LEFT JOIN tbl_user_usergroup_map ugm ON ugm.user_id = u.id
				LEFT JOIN tbl_property_sub_type st ON st.sub_id = p.sub_type
				WHERE ugm.group_id NOT IN (7,8,11,12) $expired_condition
				ORDER BY p.listing_id ASC
				LIMIT 0, 20
			) as x
			GROUP BY x.sub_type
		");
		*/
		$count_per_subtype = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_subtype), "</pre>";
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'arr_stats', $arr_stats );
		$view->assignRef( 'only_active', $only_active );
		$view->assignRef( 'count_per_type', $count_per_type );
		$view->assignRef( 'count_per_subtype', $count_per_subtype );
		$view->assignRef( 'ticks', $ticks );
		
		
		$task = JRequest::getVar('task');

		switch ($task):

			case "export_csv":
				generateCSV();
			break;
		
		endswitch;
		
        parent::display();

    }
	
	
	function userReport() {
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		/*
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		*/
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select(array("u.id, u.registerDate, ur.*"));
		$query->from("#__users as u");
		$query->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query->where('ugm.group_id NOT IN(7,8,11,12) AND u.block = 0 AND ur.user_id IS NOT NULL');
		$query->order("u.registerDate ASC");
		
		$db->setQuery($query);

		$results = $db->loadObjectList();
		
		switch($rformat) {
			case "daily":
				$interval = " + 8 hours";
				$display_format = "Y-m-d H:i";
				$compare_format = "Y-m-d H:i";
				
				$select_date = $_GET['select_date'];
				$start_date = $_GET['select_date'] . "00:00:00";
				$end_date = $_GET['select_date'] . "23:59:59";
				
				$previous_date = strtotime($start_date . "-8 hours");
				break;
			case "weekly":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				
				$select_year = $_GET['select_year'];
				$select_week = $_GET['select_week'];
				
				$start_date = $select_week . " 00:00:00";
				$end_date = date("Y-m-d 23:59:59", strtotime($select_week . " + 6 days"));
				/*
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				*/
				
				$previous_date = strtotime($start_date . "-1 day");
				
				break;
			case "monthly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				/*
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				*/
				
				$select_year = $_GET['select_year'];
				$select_month = $_GET['select_month'];
				
				$start_date = $select_year . "-" . $select_month . "-01 00:00:00";
				if(strtotime(date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"))) > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-t 23:59:59");
				} else {
					$end_date = date("Y-m-t 23:59:59", strtotime($select_year . "-" . $select_month . "-01"));
				}
				
				$previous_date = strtotime($start_date . "-1 week");
				
				break;
			case "yearly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d 23:59:59";
				/*
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				*/
				$select_year = $_GET['select_year'];
				
				$start_date = $select_year . "-01-01 00:00:00";
				if(strtotime($select_year . "-12-31") > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-d H:i:s");
				} else {
					$end_date = $select_year . "-12-31 23:59:59";
				}
				
				$previous_date = strtotime($start_date . "-1 month");
				
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				$start_date = $results[0]->registerDate;
				$end_date = date("Y-m-d 23:59:59");
				$previous_date = $start_date;
		}
		
		#echo "<pre>", print_r($results), "</pre>"; exit;
		$end_key = count($results) - 1;
		#$start_date = $results[0]->registerDate;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		#$end_date = $results[$end_key]->registerDate;
		$str_end_totime = strtotime($end_date);

		$arr_stats = array();
		$previous_display_date = $previous_date;
		
		$arr_membercount = array();
		$arr_invites = array();
		$arr_terms_accepted = array();
		while($str_current_totime <= $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			$arr_stats[$display_date]['invited_count'] = 0;
			$arr_stats[$display_date]['terms_accepted_count'] = 0;
			foreach($results as $key => $user) {
				$registration_date = strtotime($user->registerDate);
				$invited_date = strtotime($user->sendEmail_date);
				$terms_accepted_date = strtotime($user->registerDate);
				
				if($registration_date <= $str_current_totime) {
					$arr_stats[$display_date]['member_count'] += 1;
				}
				
				if($previous_display_date == $str_current_totime) {
					if($invited_date <= $str_current_totime) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $user->is_term_accepted > 0 && $terms_accepted_date <= $str_current_totime) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				} else {
					if($invited_date <= $str_current_totime && $invited_date > $previous_date) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $registration_date > $previous_date && $user->is_term_accepted > 0  && $terms_accepted_date <= $str_current_totime  && $terms_accepted_date > $previous_date) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				}
				
				if($registration_date > $str_current_totime) {
					break;
				}
			}
			$arr_membercount[] = $arr_stats[$display_date]['member_count'];
			$arr_invites[] = $arr_stats[$display_date]['invited_count'];
			$arr_terms_accepted[] = $arr_stats[$display_date]['terms_accepted_count'];
			
			$previous_date = $str_current_totime;
			$str_current_totime = strtotime($current_date . $interval);
		}
		
		
		if($rformat == "weekly") {
			$membercount_min = min($arr_membercount);
			$membercount_max = max($arr_membercount);
			
			$membercount_ticks = array();
			while($membercount_min <= $membercount_max) {
				$membercount_ticks[] = $membercount_min;
				$membercount_min += 5;
			}
			$membercount_ticks[] = $membercount_min;
			$membercount_ticks = implode(",", $membercount_ticks);
			
			$invites_min = min($arr_invites);
			$invites_max = max($arr_invites);
			
			$invites_ticks = array();
			while($invites_min <= $invites_max) {
				$invites_ticks[] = $invites_min;
				$invites_min += 5;
			}
			$invites_ticks[] = $invites_min;
			$invites_ticks = implode(",", $invites_ticks);
			
			$terms_accepted_min = min($arr_terms_accepted);
			$terms_accepted_max = max($arr_terms_accepted);
			
			$terms_accepted_ticks = array();
			while($terms_accepted_min <= $terms_accepted_max) {
				$terms_accepted_ticks[] = $terms_accepted_min;
				$terms_accepted_min += 5;
			}
			$terms_accepted_ticks[] = $terms_accepted_min;
			$terms_accepted_ticks = implode(",", $terms_accepted_ticks);
			
		} else if($rformat == "daily") {
			$membercount_min = min($arr_membercount);
			$membercount_max = max($arr_membercount);
			
			$membercount_ticks = array();
			while($membercount_min <= $membercount_max) {
				$membercount_ticks[] = $membercount_min;
				$membercount_min += 2;
			}
			$membercount_ticks[] = $membercount_min;
			$membercount_ticks = implode(",", $membercount_ticks);
			
			$invites_min = min($arr_invites);
			$invites_max = max($arr_invites);
			
			$invites_ticks = array();
			while($invites_min <= $invites_max) {
				$invites_ticks[] = $invites_min;
				$invites_min += 2;
			}
			$invites_ticks[] = $invites_min;
			$invites_ticks = implode(",", $invites_ticks);
			
			$terms_accepted_min = min($arr_terms_accepted);
			$terms_accepted_max = max($arr_terms_accepted);
			
			$terms_accepted_ticks = array();
			while($terms_accepted_min <= $terms_accepted_max) {
				$terms_accepted_ticks[] = $terms_accepted_min;
				$terms_accepted_min += 2;
			}
			$terms_accepted_ticks[] = $terms_accepted_min;
			$terms_accepted_ticks = implode(",", $terms_accepted_ticks);
			
		} else {
			$membercount_ticks = "";
			$invites_ticks = "";
			$terms_accepted_ticks = "";
		}
		
		
		$query2 = $db->getQuery(true);
		$query2->select(array("COUNT(u.id) as user_count, c.countries_name, ur.country"));
		$query2->from("#__users as u");
		$query2->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin('#__countries c ON c.countries_id = ur.country');
		$query2->group("ur.country");
		$query2->where('ugm.group_id NOT IN(7,8,11,12) AND u.block = 0 AND ur.user_id IS NOT NULL AND u.registerDate <="' . $end_date . '"');
		
		$db->setQuery($query2);
		
		$results = $db->loadObjectList();
		
		$arr_user_count_per_country = array();
		foreach($results as $country) {
			if(!$country->country || $country->country == 223) {
				$arr_user_count_per_country['US'] += $country->user_count;
			} else {
				$arr_user_count_per_country['INTL'] += $country->user_count;
			}
		}
		
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'arr_stats', $arr_stats );
		$view->assignRef( 'only_active', $only_active );
		$view->assignRef( 'arr_user_count_per_country', $arr_user_count_per_country );
		$view->assignRef( 'membercount_ticks', $membercount_ticks );
		$view->assignRef( 'invites_ticks', $invites_ticks );
		$view->assignRef( 'terms_accepted_ticks', $terms_accepted_ticks );
		$view->display($this->getTask());
		
	}
	
    function export_csv()

	{
	
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;

		die();
	}

		   ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateCSV(){		
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$sold_condition = ($only_active) ? " AND sold = 0" : "";

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(array('p.listing_id, p.property_name, pp.price_type, p.zip, p.state, p.country, pp.price1, pp.price2, pp.price_type, p.date_created, p.date_expired'));

		$query->from('#__pocket_listing p');

		$query->leftJoin('#__property_price pp ON pp.pocket_id = p.listing_id');

		$query->leftJoin('#__property_type pt ON pt.type_id = p.property_type');

		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = p.sub_type');

		$query->leftJoin('#__users u ON u.id = p.user_id');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');

		$query->leftJoin('#__zones z ON z.zone_id = p.state');
		
		$query->where('ugm.group_id NOT IN(7,8,11,12)' . $sold_condition);
		
		$query->order('p.date_created ASC');
		
		//$query->setLimit(20);
	//	$query->where('closed!=1');

				
	//	$query->order($db->escape($model->getState('list.ordering', 'ur.user_id')).' '.$db->escape($model->getState('list.direction', 'ASC')));

        $db->setQuery($query);

		$results = $db->loadObjectList();
		
		switch($rformat) {
			case "daily":
				$interval = " + 8 hours";
				$display_format = "Y-m-d H:i";
				$compare_format = "Y-m-d H:i";
				
				$select_date = $_GET['select_date'];
				$start_date = $_GET['select_date'] . "00:00";
				$end_date = $_GET['select_date'] . "23:59";
				break;
			case "weekly":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				
				$select_year = $_GET['select_year'];
				$select_week = $_GET['select_week'];
				
				$start_date = $select_week;
				$end_date = date("Y-m-d", strtotime($select_week . " + 6 days"));
				/*
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				*/
				break;
			case "monthly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				/*
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				*/
				
				$select_year = $_GET['select_year'];
				$select_month = $_GET['select_month'];
				
				$start_date = $select_year . "-" . $select_month . "-01";
				if(strtotime(date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"))) > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-t");
				} else {
					$end_date = date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"));
				}
				break;
			case "yearly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				/*
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				*/
				$select_year = $_GET['select_year'];
				
				$start_date = $select_year . "-01-01";
				if(strtotime($select_year . "-12-31") > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-d");
				} else {
					$end_date = $select_year . "-12-31";
				}
				
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				$start_date = $results[0]->date_created;
				$end_date = date("Y-m-d");
		}

		$end_key = count($results) - 1;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		$str_end_totime = strtotime($end_date);
		
		$arr_stats = array();
		$previous_display_date = "";
		$first_zero = "";
		while($str_current_totime <= $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
							if($previous_display_date) {
								$arr_stats[$display_date]['diff'] = $arr_stats[$display_date]['price'] - $arr_stats[$previous_display_date]['price'];
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
					}
				} else {
					break;
				}
			}
			
			if(!intval($arr_stats[$display_date]['pops_count'])) {
				$arr_stats[$display_date]['average'] = 0;
				if(!$first_zero) {
					$first_zero = $display_date;
				}
			}
			
			$previous_display_date = $display_date;
			$str_current_totime = strtotime($current_date . $interval);
			
		}
		
		if($str_current_totime > strtotime($end_date)) {
			$current_date = date($compare_format, strtotime($end_date));
			$display_date = date($display_format, strtotime($end_date));
			$arr_stats[$display_date] = array();
			foreach($results as $key => $pops) {
				if(strtotime($pops->date_created) <= $str_current_totime) {
					if($only_active) {
						if(strtotime($pops->date_expired) > $str_current_totime) {
							$arr_stats[$display_date]['pops_count'] += 1;
							if($pops->price_type == 1) {
								$arr_stats[$display_date]['price'] += $pops->price2;
							} else {
								$arr_stats[$display_date]['price'] += $pops->price1;
							}
							$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
							if($previous_display_date) {
								$arr_stats[$display_date]['diff'] = $arr_stats[$display_date]['price'] - $arr_stats[$previous_display_date]['price'];
							} else {
								$arr_stats[$display_date]['diff'] = "";
							}
						}
					} else {
						$arr_stats[$display_date]['pops_count'] += 1;
						if($pops->price_type == 1) {
							$arr_stats[$display_date]['price'] += $pops->price2;
						} else {
							$arr_stats[$display_date]['price'] += $pops->price1;
						}
						$arr_stats[$display_date]['average'] = $arr_stats[$display_date]['price'] / $arr_stats[$display_date]['pops_count'];
					}
				} else {
					break;
				}
			}
			
			if(!intval($arr_stats[$display_date]['pops_count'])) {
				$arr_stats[$display_date]['average'] = 0;
			}
		}
		

		#echo $interval; exit;
		#echo "<pre>", print_r($arr_stats), "</pre>"; exit;
		

		
		$query2 = $db->getQuery(true);
		
		if($rformat !== "") {
			$expired_condition = ($only_active) ? " AND p.date_expired > '" . $end_date . "' AND p.sold = 0" : "";
		} else {
			$expired_condition = ($only_active) ? " AND p.date_expired > NOW() AND p.sold = 0" : "";
		}
		
		$query2->select(array('pt.type_name, pt.type_id, COUNT(p.listing_id) as pops_count'));
		$query2->from("#__pocket_listing p");
		$query2->leftJoin('#__users u ON u.id = p.user_id');
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin("#__property_type pt ON pt.type_id = p.property_type");
		$query2->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition . " AND p.date_created <= '" . $end_date . "'");
		$query2->group("p.property_type");
		/*
		$db->setQuery("
			SELECT COUNT(x.listing_id) as pops_count, x.type_name, x.type_id 
			FROM(
				SELECT p.listing_id, p.property_type, pt.type_name, pt.type_id
				FROM tbl_pocket_listing p
				LEFT JOIN tbl_users u ON u.id = p.user_id
				LEFT JOIN tbl_user_usergroup_map ugm ON ugm.user_id = u.id
				LEFT JOIN tbl_property_type pt ON pt.type_id = p.property_type
				WHERE ugm.group_id NOT IN ( 7, 8, 11, 12 ) $expired_condition 
				ORDER BY p.listing_id ASC
				LIMIT 0, 20
			) as x
			GROUP BY x.property_type
			"
		);
		*/
		$db->setQuery($query2);
		$count_per_type = $db->loadAssocList();
		
		#echo "<pre>", print_r($count_per_type), "</pre>";
		
		$query3 = $db->getQuery(true);

		$query3->select(array('st.name, st.property_id, COUNT(p.listing_id) as pops_count'));
		$query3->from("#__pocket_listing p");
		$query3->leftJoin('#__users u ON u.id = p.user_id');
		$query3->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query3->leftjoin("#__property_sub_type st ON st.sub_id = p.sub_type");
		$query3->where("ugm.group_id NOT IN (7,8,11,12)" . $expired_condition . " AND p.date_created <= '" . $end_date . "'");
		$query3->group("p.sub_type");
		/*
		$db->setQuery("
			SELECT x.name, x.property_id, COUNT(x.listing_id) as pops_count
			FROM(
				SELECT p.listing_id, st.name, st.property_id, p.sub_type
				FROM tbl_pocket_listing p
				LEFT JOIN tbl_users u ON u.id = p.user_id
				LEFT JOIN tbl_user_usergroup_map ugm ON ugm.user_id = u.id
				LEFT JOIN tbl_property_sub_type st ON st.sub_id = p.sub_type
				WHERE ugm.group_id NOT IN (7,8,11,12) $expired_condition
				ORDER BY p.listing_id ASC
				LIMIT 0, 20
			) as x
			GROUP BY x.sub_type
		");
		*/
		$db->setQuery($query3);
		$count_per_subtype = $db->loadAssocList();
		
		$objPHPExcel = new PHPExcel();
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$objWorksheet->getColumnDimension("A")->setAutoSize(true);
		$objWorksheet->getColumnDimension("B")->setAutoSize(true);
		$objWorksheet->getColumnDimension("C")->setAutoSize(true);
		$objWorksheet->getColumnDimension("D")->setAutoSize(true);
		if($only_active) {
			$objWorksheet->getColumnDimension("E")->setAutoSize(true);
		}
		
		
		
		$worksheet_table = array();
		if($only_active) {
			$worksheet_table[] = array('Date', 'Number of POPs', 'Value of POPs', 'Change in Value', 'Average Price');
		} else {
			$worksheet_table[] = array('Date', 'Number of POPs', 'Value of POPs', 'Average Price');
		}
		
		$end_data = count($arr_stats)+1;
		if($only_active) {
			foreach($arr_stats as $key => $stat) {
				$worksheet_table[] = array($key, $stat['pops_count'], $stat['price'], $stat['diff'], $stat['average']);
			}
		} else {
			foreach($arr_stats as $key => $stat) {
				$worksheet_table[] = array($key, $stat['pops_count'], $stat['price'], $stat['average']);
			}
		}
		$objWorksheet->fromArray($worksheet_table);
		#echo "<pre>", print_r($worksheet_table), "</pre>"; exit;
		$index = 1;
		
		$pie_array = array();
		
		$pie_cells1 = "";
		$pie_cells2 = "";
		foreach($count_per_type as $type) {
			$objWorksheet->setCellValue("G$index", JText::_($type['type_name']));
			$objWorksheet->setCellValue("H$index", $type['pops_count']);
			
			$objWorksheet->getStyle("G$index:H$index")->getFill()->applyFromArray(array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'DDDDDD'
				)
			));
			$pie_cells1 .= ($pie_cells1) ? "," : "";
			$pie_cells1 .= 'Worksheet!$G$' . $index;
			
			$pie_cells2 .= ($pie_cells2) ? "," : "";
			$pie_cells2 .= 'Worksheet!$H$' . $index;
			
			
			$index=$index+1;
			foreach($count_per_subtype as $sub_type) {
				if($type['type_id'] == $sub_type['property_id']) {					
					$objWorksheet->setCellValue("G$index", JText::_($sub_type['name']));
					$objWorksheet->setCellValue("H$index", $sub_type['pops_count']);
					$index=$index+1;
				}
			}			
		}
		
		$objWorksheet->getColumnDimension("G")->setAutoSize(true);
		$objWorksheet->getColumnDimension("H")->setAutoSize(true);
		
		
		$objWorksheet->getStyle("B1:B$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		if($only_active) {
			$objWorksheet->getStyle("C1:E1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		} else {
			$objWorksheet->getStyle("C1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		}		
		$objWorksheet->getStyle("C2:C$end_data")->getNumberFormat()->setFormatCode("\$ #,##0.00");
		if($only_active) {
			$phpFont = new PHPExcel_Style_Font();
			$phpFont->setBold(true);
			$phpFont->setName('Wingdings');
			$phpFont->setSize('15');
			$objWorksheet->getStyle("D2:D$end_data")->getNumberFormat()->setFormatCode('[Red][<0] "q";[Green][>0] "p";');
			$objWorksheet->getStyle("D2:D$end_data")->getFont()->setName("Wingdings 3");
			#$objWorksheet->getStyle("D2:D$end_data")->getNumberFormat()->setFormatCode("#,##0");
			$objWorksheet->getStyle("D2:D$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objWorksheet->getStyle("E2:E$end_data")->getNumberFormat()->setFormatCode("\$ #,##0.00");
		} else {
			$objWorksheet->getStyle("D2:D$end_data")->getNumberFormat()->setFormatCode("\$ #,##0.00");
		}
		
		
		$dataSeriesLabels = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1, array(), 'none'));
		$xAxisTickValues = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1, array(), 'none'));
		$dataSeriesValues = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$'.$end_data, NULL, 1, array(), 'none'));
		
		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dataSeriesValues)-1),			// plotOrder
			$dataSeriesLabels,								// plotLabel
			$xAxisTickValues,								// plotCategory
			$dataSeriesValues								// plotValues
		);
		
		//	Set the series in the plot area
		$plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
		//	Set the chart legend
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

		if($only_active) {
			$title = new PHPExcel_Chart_Title('Value of Active POPs');
			$yAxisLabel = new PHPExcel_Chart_Title('Value of POPs');
		} else {
			$title = new PHPExcel_Chart_Title('Value of Total POPs');
			$yAxisLabel = new PHPExcel_Chart_Title('Value of POPs');
		}
		


		//	Create the chart
		$chart = new PHPExcel_Chart(
			'chart2',		// name
			$title,			// title
			$legend,		// legend
			$plotArea,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			$yAxisLabel		// yAxisLabel
		);

		//	Set the position where the chart should appear in the worksheet
		if($end_data < 25) {
			$end_data = 25;
		}
		$chart->setTopLeftPosition('A' . intval($end_data + 3));
		$chart->setBottomRightPosition('K' . intval($end_data + 33));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart);
		
		
		
		if($only_active) {
			$dataSeriesLabels = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$B$1', NULL, 1, array(), 'none'));
			$xAxisTickValues = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1, array(), 'none'));
			$dataSeriesValues = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$2:$B$'.$end_data, NULL, 1, array(), 'none'));
			
			$series = new PHPExcel_Chart_DataSeries(
				PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
				PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
				range(0, count($dataSeriesValues)-1),			// plotOrder
				$dataSeriesLabels,								// plotLabel
				$xAxisTickValues,								// plotCategory
				$dataSeriesValues								// plotValues
			);
			
			//	Set the series in the plot area
			$plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
			//	Set the chart legend
			$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

			$title = new PHPExcel_Chart_Title('Active POPs');
			$yAxisLabel = new PHPExcel_Chart_Title('Active POPs');


			//	Create the chart
			$chart = new PHPExcel_Chart(
				'chart1',		// name
				$title,			// title
				$legend,		// legend
				$plotArea,		// plotArea
				true,			// plotVisibleOnly
				0,				// displayBlanksAs
				NULL,			// xAxisLabel
				$yAxisLabel		// yAxisLabel
			);
			//	Set the position where the chart should appear in the worksheet
			$chart->setTopLeftPosition('A' . intval($end_data + 35));
			$chart->setBottomRightPosition('K' . intval($end_data + 65));

			//	Add the chart to the worksheet
			$objWorksheet->addChart($chart);
		}
		
		
		
		#echo $pie_cells1;
		#echo $pie_cells2; exit;
		
		
		$dataSeriesLabels1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', '', NULL, 1),	//	2011
		);
		
		$xAxisTickValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', "(" . $pie_cells1 . ")", NULL, 1),	//	Q1 to Q4
		);
		
		$dataSeriesValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('Number', "(" . $pie_cells2 . ")", NULL, 1),
		);
		
		//	Build the dataseries
		$series1 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
			NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
			range(0, count($dataSeriesValues1)-1),					// plotOrder
			$dataSeriesLabels1,										// plotLabel
			$xAxisTickValues1,										// plotCategory
			$dataSeriesValues1										// plotValues
		);
		
		//	Set up a layout object for the Pie chart
		$layout1 = new PHPExcel_Chart_Layout();
		$layout1->setShowVal(TRUE);
		$layout1->setShowPercent(TRUE);

		//	Set the series in the plot area
		$plotArea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
		//	Set the chart legend
		$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title1 = new PHPExcel_Chart_Title('Property Types');


		//	Create the chart
		$chart1 = new PHPExcel_Chart(
			'chart1',		// name
			$title1,		// title
			$legend1,		// legend
			$plotArea1,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
		);

		//	Set the position where the chart should appear in the worksheet
		$chart1->setTopLeftPosition('L' . intval($end_data + 3));
		$chart1->setBottomRightPosition('X' . intval($end_data + 33));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart1);
		
		
		$insert_act = new JObject();
		$insert_act->activity_type_id = 6;
		$insert_act->user_id = JFactory::getUser()->id;
		$insert_act->activity_details = "Downloaded POPs Excel Report";
		$db->insertObject('#__admin_activities', $insert_act);
		
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=POPs.Report." . date("Y-m-d") . ".xlsx");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("php://output");
		exit;
	
	}
	
	public function generateMemberCSV() {
		$rformat = (isset($_GET['rformat'])) ? $_GET['rformat'] : "";
		$only_active = (isset($_GET['only_active'])) ? $_GET['only_active'] : false;
		/*
		switch($rformat) {
			case "daily":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "weekly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				break;
			case "monthly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				break;
			case "yearly":
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
		}
		*/
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        
        $language->load($extension, $base_dir, "english-US", true);
		
    	
		$model = $this->getModel('AbAnalytics');
		
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select(array("u.id, u.registerDate, ur.*"));
		$query->from("#__users as u");
		$query->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query->where('ugm.group_id NOT IN(7,8,11,12) AND u.block = 0 AND ur.user_id IS NOT NULL');
		$query->order("u.registerDate ASC");
		
		$db->setQuery($query);

		$results = $db->loadObjectList();
		
		switch($rformat) {
			case "daily":
				$interval = " + 8 hours";
				$display_format = "Y-m-d H:i";
				$compare_format = "Y-m-d H:i";
				
				$select_date = $_GET['select_date'];
				$start_date = $_GET['select_date'] . "00:00:00";
				$end_date = $_GET['select_date'] . "23:59:59";
				
				$previous_date = strtotime($start_date . "-8 hours");
				break;
			case "weekly":
				$interval = " + 1 day";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				
				$select_year = $_GET['select_year'];
				$select_week = $_GET['select_week'];
				
				$start_date = $select_week . " 00:00:00";
				$end_date = date("Y-m-d 23:59:59", strtotime($select_week . " + 6 days"));
				/*
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d";
				*/
				
				$previous_date = strtotime($start_date . "-1 day");
				
				break;
			case "monthly":
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				/*
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d";
				*/
				
				$select_year = $_GET['select_year'];
				$select_month = $_GET['select_month'];
				
				$start_date = $select_year . "-" . $select_month . "-01 00:00:00";
				if(strtotime(date("Y-m-t", strtotime($select_year . "-" . $select_month . "-01"))) > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-t 23:59:59");
				} else {
					$end_date = date("Y-m-t 23:59:59", strtotime($select_year . "-" . $select_month . "-01"));
				}
				
				$previous_date = strtotime($start_date . "-1 week");
				
				break;
			case "yearly":
				$interval = " + 1 month";
				$display_format = "Y-m";
				$compare_format = "Y-m-d 23:59:59";
				/*
				$interval = " + 1 year";
				$display_format = "Y";
				$compare_format = "Y-12-31";
				*/
				$select_year = $_GET['select_year'];
				
				$start_date = $select_year . "-01-01 00:00:00";
				if(strtotime($select_year . "-12-31") > strtotime(date("Y-m-d"))) {
					$end_date = date("Y-m-d");
				} else {
					$end_date = $select_year . "-12-31 23:59:59";
				}
				
				$previous_date = strtotime($start_date . "-1 year");
				
				break;
			default:
				$interval = " + 1 week";
				$display_format = "Y-m-d";
				$compare_format = "Y-m-d 23:59:59";
				$start_date = $results[0]->registerDate;
				$end_date = date("Y-m-d 23:59:59");
				$previous_date = $start_date;
		}
		
		#echo "<pre>", print_r($results), "</pre>"; exit;
		$end_key = count($results) - 1;
		#$start_date = $results[0]->registerDate;
		$str_current_totime = strtotime($start_date);
		$current_date = $start_date;
		#$end_date = date("Y-m-d");
		$str_end_totime = strtotime($end_date);

		$arr_stats = array();
		$previous_display_date = $previous_date;
		while($str_current_totime <= $str_end_totime) {
			$current_date = date($compare_format, $str_current_totime);
			$display_date = date($display_format, $str_current_totime);
			$arr_stats[$display_date] = array();
			$arr_stats[$display_date]['invited_count'] = 0;
			$arr_stats[$display_date]['terms_accepted_count'] = 0;
			foreach($results as $key => $user) {
				$registration_date = strtotime($user->registerDate);
				$invited_date = strtotime($user->sendEmail_date);
				$terms_accepted_date = strtotime($user->registerDate);
				
				if($registration_date <= $str_current_totime) {
					$arr_stats[$display_date]['member_count'] += 1;
				}
				
				if($previous_display_date == $str_current_totime) {
					if($invited_date <= $str_current_totime) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $terms_accepted_date <= $str_current_totime) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				} else {
					if($invited_date <= $str_current_totime && $invited_date > $previous_date) {
						$arr_stats[$display_date]['invited_count'] += 1;
					}
					
					if($registration_date <= $str_current_totime && $registration_date > $previous_date  && $terms_accepted_date <= $str_current_totime  && $terms_accepted_date > $previous_date) {
						$arr_stats[$display_date]['terms_accepted_count'] += 1;
					}
				}
				
				if($registration_date > $str_current_totime) {
					break;
				}
			}
			
			$arr_stats[$display_date]['terms_accepted_count'] = (!$arr_stats[$display_date]['terms_accepted_count']) ? "0" : $arr_stats[$display_date]['terms_accepted_count'];
			$arr_stats[$display_date]['invited_count'] = (!$arr_stats[$display_date]['invited_count']) ? "0" : $arr_stats[$display_date]['invited_count'];
			
			$previous_date = $str_current_totime;
			$str_current_totime = strtotime($current_date . $interval);
		}
		
		$query2 = $db->getQuery(true);
		$query2->select(array("COUNT(u.id) as user_count, c.countries_name, ur.country"));
		$query2->from("#__users as u");
		$query2->leftjoin("#__user_registration ur ON ur.email = u.email");
		$query2->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		$query2->leftjoin('#__countries c ON c.countries_id = ur.country');
		$query2->group("ur.country");
		$query2->where('ugm.group_id NOT IN(7,8,11,12) AND u.block = 0 AND ur.user_id IS NOT NULL AND u.registerDate <="' . $end_date . '"');
		
		$db->setQuery($query2);
		
		$results = $db->loadObjectList();
		
		$arr_user_count_per_country = array();
		foreach($results as $country) {
			if(!$country->country || $country->country == 223) {
				$arr_user_count_per_country['US'] += $country->user_count;
			} else {
				$arr_user_count_per_country['INTL'] += $country->user_count;
			}
		}
		
		$objPHPExcel = new PHPExcel();
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$objWorksheet->getColumnDimension("A")->setAutoSize(true);
		$objWorksheet->getColumnDimension("B")->setAutoSize(true);
		$objWorksheet->getColumnDimension("C")->setAutoSize(true);
		$objWorksheet->getColumnDimension("D")->setAutoSize(true);
		
		
		
		$worksheet_table = array();
		$worksheet_table[] = array('Date', 'Members', 'Invitations', 'Terms Accepted');
		$end_data = count($arr_stats)+1;
		foreach($arr_stats as $key => $stat) {
			$worksheet_table[] = array($key, $stat['member_count'], $stat['invited_count'], $stat['terms_accepted_count']);
		}
		$objWorksheet->fromArray($worksheet_table);
		
		$index = 1;
		
		$pie_array = array();
		
		$objWorksheet->setCellValue("G$index", "Country");
		$objWorksheet->setCellValue("H$index", "Members");
		$index += 1;
		foreach($arr_user_count_per_country as $key => $country_stats) {
			if($key == "US") {
				$objWorksheet->setCellValue("G$index", "U.S.A.");
			} else {
				$objWorksheet->setCellValue("G$index", "International");
			}
			
			$objWorksheet->setCellValue("H$index", $country_stats);
			
			$index=$index+1;
		}
		
		$objWorksheet->getColumnDimension("G")->setAutoSize(true);
		$objWorksheet->getColumnDimension("H")->setAutoSize(true);
		
		
		$objWorksheet->getStyle("B1:B$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objWorksheet->getStyle("C1:C$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objWorksheet->getStyle("C1:C$end_data")->getNumberFormat()->setFormatCode('#,##0');
		$objWorksheet->getStyle("D1:D$end_data")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objWorksheet->getStyle("D1:D$end_data")->getNumberFormat()->setFormatCode('#,##0');
		
		$dataSeriesLabels = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$B$1', NULL, 1, array(), 'none'));
		$xAxisTickValues = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1, array(), 'none'));
		$dataSeriesValues = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$2:$B$'.$end_data, NULL, 1, array(), 'none'));
		
		$series = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dataSeriesValues)-1),			// plotOrder
			$dataSeriesLabels,								// plotLabel
			$xAxisTickValues,								// plotCategory
			$dataSeriesValues								// plotValues
		);
		
		//	Set the series in the plot area
		$plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
		//	Set the chart legend
		$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

		
		$title = new PHPExcel_Chart_Title('Members');
		$yAxisLabel = new PHPExcel_Chart_Title('Members');		
		
		//	Create the chart
		$chart = new PHPExcel_Chart(
			'chart1',		// name
			$title,			// title
			$legend,		// legend
			$plotArea,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			$yAxisLabel		// yAxisLabel
		);

		//	Set the position where the chart should appear in the worksheet
		$chart->setTopLeftPosition('A' . intval($end_data + 3));
		$chart->setBottomRightPosition('K' . intval($end_data + 33));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart);
		
		
		$dataSeriesLabels1 = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1, array(), 'none'));
		$xAxisTickValues1 = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1, array(), 'none'));
		$dataSeriesValues1 = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$'.$end_data, NULL, 1, array(), 'none'));
		
		$series1 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dataSeriesValues1)-1),			// plotOrder
			$dataSeriesLabels1,								// plotLabel
			$xAxisTickValues1,								// plotCategory
			$dataSeriesValues1								// plotValues
		);
		
		//	Set the series in the plot area
		$plotArea1 = new PHPExcel_Chart_PlotArea(NULL, array($series1));
		//	Set the chart legend
		$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

		
		$title1 = new PHPExcel_Chart_Title('Invitations');
		$yAxisLabel1 = new PHPExcel_Chart_Title('Invitations');		
		
		//	Create the chart
		$chart2 = new PHPExcel_Chart(
			'chart2',		// name
			$title1,		// title
			$legend1,		// legend
			$plotArea1,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			$yAxisLabel1	// yAxisLabel
		);

		//	Set the position where the chart should appear in the worksheet
		$chart2->setTopLeftPosition('A' . intval($end_data + 35));
		$chart2->setBottomRightPosition('K' . intval($end_data + 65));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart2);
		
		$dataSeriesLabels2 = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$D$1', NULL, 1, array(), 'none'));
		$xAxisTickValues2 = array(new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.$end_data, NULL, 1, array(), 'none'));
		$dataSeriesValues2 = array(new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$D$2:$D$'.$end_data, NULL, 1, array(), 'none'));
		
		$series2 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
			PHPExcel_Chart_DataSeries::GROUPING_STACKED,	// plotGrouping
			range(0, count($dataSeriesValues2)-1),			// plotOrder
			$dataSeriesLabels2,								// plotLabel
			$xAxisTickValues2,								// plotCategory
			$dataSeriesValues2								// plotValues
		);
		
		//	Set the series in the plot area
		$plotArea2 = new PHPExcel_Chart_PlotArea(NULL, array($series2));
		//	Set the chart legend
		$legend2 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOPRIGHT, NULL, false);

		
		$title2 = new PHPExcel_Chart_Title('Terms Accepted');
		$yAxisLabel2 = new PHPExcel_Chart_Title('Terms Accepted');		
		
		//	Create the chart
		$chart3 = new PHPExcel_Chart(
			'chart3',		// name
			$title2,		// title
			$legend2,		// legend
			$plotArea2,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			$yAxisLabel2	// yAxisLabel
		);

		//	Set the position where the chart should appear in the worksheet
		$chart3->setTopLeftPosition('L' . intval($end_data + 35));
		$chart3->setBottomRightPosition('X' . intval($end_data + 65));

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart3);
		
		$insert_act = new JObject();
		$insert_act->activity_type_id = 5;
		$insert_act->user_id = JFactory::getUser()->id;
		$insert_act->activity_details = "Downloaded Members Excel Report";
		$db->insertObject('#__admin_activities', $insert_act);
		
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=Members.Report." . date("Y-m-d") . ".xlsx");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setIncludeCharts(TRUE);
		$objWriter->save("php://output");
		exit;
	}
	
	public function generateMonthList() {
		$year = $_GET['year'];
		$selected_month = $_GET['month'];
		if(date("Y") == $year) {
			$end_month = date("n");
		} else {
			$end_month = 12;
		}
		echo "<b>Month: </b><br />";
		echo "<select name='select_month'>";
		for($i=1;$i<=$end_month;$i++) {
			$selected = ($i == $selected_month) ? "selected" : "";
			echo "<option value='" . date("n", mktime(0,0,0,$i,1,$year)) . "'" . $selected . ">" . date("F", mktime(0,0,0,$i,1,$year)) . "</option>";
		}
		echo "</select>";
		exit;
	}
	
	public function generateYearList() {
		
		$selected_year = $_GET['year'];
		
		$end_year = 2014;
		
		echo "<b>Year: </b><br />";
		echo "<select name='select_year'>";
		for($i=date("Y"); $i>=$end_year; $i--) {
			$selected = ($i == $selected_year) ? "selected" : "";
			echo "<option value='" . date("Y", mktime(0,0,0,1,1,$i)) . "'" . $selected . ">" . date("Y", mktime(0,0,0,1,1,$i)) . "</option>";
		}
		echo "</select>";
		echo "
		<script>
			jQuery('#select_year select').change(function(){
				if(jQuery('#rformat').val() == 'monthly') {
					getMonth(1, jQuery('#select_year select').val());
				} else if(jQuery('#rformat').val() == 'weekly') {
					getWeek(jQuery('#select_year select').val() + '-01-01', jQuery('#select_year select').val());
				}				
			});
		</script>";
		exit;
	}
	
	public function generateWeekList() {
		$year = $_GET['year'];
		$selected_week = $_GET['week'];
		
		echo "<b>Week: </b><br />";
		echo "<select name='select_week'>";
		$start_day = $year . "-01-01";
		for($i=1; $i<=52; $i++) {
			$selected = (date("Y-m-d", strtotime($start_day)) == $selected_week) ? "selected" : "";
			echo '<option value="' . date("Y-m-d", strtotime($start_day)) . '"' . $selected . '>' . date("M j", strtotime($start_day)) . " - " . date("M j", strtotime($start_day . " + 6 days")) .'</option>';
			$start_day = date("Y-m-d", strtotime($start_day . " + 7 days"));
			if(strtotime($start_day) > strtotime(date("Y-m-d"))) break;
		}
		echo "</select>";
		
		exit;
	}

}



?>