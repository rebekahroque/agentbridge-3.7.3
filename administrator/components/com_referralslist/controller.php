<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class ReferralsListController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */




	 

    function display()



    {

    	
		$model = $this->getModel('ReferralsList');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

					'
					 r.status,
					 
					 r.referral_id,

					 b.name AS client_name,

					 u.name AS agent_name_a,

					 ur.name AS agent_name_b,
					 
					 b.price_type,

					 r.price_1,

					 r.price_2,

					 r.referral_fee,

					 b.agent_id,

					 rs.label,
					 
					 ba.currency

					 '

			)

		);



		$query->from('#__referral r');

		$query->leftJoin('#__buyer b ON b.buyer_id = r.client_id');
		
		$query->leftJoin('#__buyer_address ba ON ba.buyer_id = b.buyer_id');

		$query->leftJoin('#__users u ON u.id = r.agent_a');
		
		$query->leftJoin('#__user_usergroup_map ugma ON ugma.user_id = u.id');
		
		$query->leftJoin("#__user_registration usr ON u.email = usr.email");
		
		$query->leftJoin("#__country_currency cc ON usr.country = cc.country");

		$query->leftJoin('#__users ur ON ur.id = r.agent_b');
		
		$query->leftJoin('#__user_usergroup_map ugmb ON ugmb.user_id = ur.id');

		$query->leftJoin('#__referral_status rs ON rs.status_id = r.status');
		
		$query->where('ugma.group_id NOT IN (7,8,11,12) AND ugmb.group_id NOT IN (7,8,11,12)');

		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		//$query->where('buyer_type!="Inactive" AND buyer_type!=""');

        $db->setQuery($query);

		$results = $db->loadObjectList();



		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );	

		$application = JFactory::getApplication();

		$task = JRequest::getVar('task');

		switch ($task):

			case "export_csv":
				generateCSV();
			break;
		
		endswitch;

        parent::display();

    }

    function export_csv()

	{
	
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;

		die();
	}

		   ###############################################################
	## This function will generate a report of invoices XLS Format
	   
	function generateCSV(){
	    $language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language->load($extension, $base_dir, "english-US", true);  
		  
	      ## Make DB connections
	      $db    = JFactory::getDBO();

			$query = $db->getQuery(true);

			$query->select(

				array(

						'r.referral_id AS referral_ID,

						 b.name AS client_name,

						 u.name AS referred_by,

						 u.email as referred_by_email,

						 umn.value AS referred_by_contact_number,

						 ur.name AS referred_to,

						 ur.email AS referred_to_email,

						 umu.value AS referred_to_contact_number,
						 
						 b.price_type,
						 
						 r.price_1,

						 r.price_2,
						 
						 ba.currency,

						 r.referral_fee,
						 
						 rs.label AS status

						 '

				)

			);



			$query->from('#__referral r');

			$query->leftJoin('#__buyer b ON b.buyer_id = r.client_id');
			
			$query->leftJoin('#__buyer_address ba ON ba.buyer_id = b.buyer_id'); 

			$query->leftJoin('#__users u ON u.id = r.agent_a');
			
			$query->leftJoin('#__user_usergroup_map ugma ON ugma.user_id = u.id');

			$query->leftJoin('#__user_registration urg ON urg.email = u.email');

			$query->leftJoin('#__user_mobile_numbers umn ON umn.user_id = urg.user_id');

			$query->leftJoin('#__users ur ON ur.id = r.agent_b');
			
			$query->leftJoin('#__user_usergroup_map ugmb ON ugmb.user_id = ur.id');

			$query->leftJoin('#__user_registration ure ON ure.email = ur.email');

			$query->leftJoin('#__user_mobile_numbers umu ON umu.user_id = ure.user_id');

			$query->leftJoin('#__referral_status rs ON rs.status_id = r.status');
			
			$query->where('ugma.group_id NOT IN (7,8,11,12) AND ugmb.group_id NOT IN (7,8,11,12)');
			
			$query->group('r.referral_id');
		      
	      $db->setQuery($query);
	      $rows = $db->loadAssocList();

	       $time = time();
	      
	      ## If the query doesn't work..
	      if (!$db->query() ){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      
	      $fields = (array_keys($rows[0]));
	      
	      ## Count all fields(will be the collumns
	      $columns = count($fields);
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	      	if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	}else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      
	      $data .= "\n";
	      
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         $price_type = '';
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="price_type"){
	         	 $price_type = $value;
				 switch($value) {
					 case 1:
						$value = "Price Range";
						break;
					 case 2:
						$value = "Exact Price";
						break;
					 case 3:
						$value = "Unknown";
						break;
				 }
	         	} elseif($key=="zip"){
	         	 $value = str_replace(array("\\","/",","), "-",  $value);
	         	}elseif($key=="price_1" || $key=="price_2" ){
	         	 $value = ($price_type == 3) ? "" : "$".str_replace(array("\\","/",","), "",  $value);
				} else if($key=="status"){
				 $value = JText::_($value);
	         	}else
	         	 $value = str_replace(array("\\","/",","), "",  $value);

	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      
	      $data = str_replace("\r","",$data);
	      
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      
	      ## Push the report now!
	      $this->name = 'Referrals-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }

}



?>