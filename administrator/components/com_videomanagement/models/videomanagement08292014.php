<?php

defined('_JEXEC') or die;



class VideoManagementModelVideoManagement extends JModelLegacy 

{	


	public function __construct($config = array()){   

			$config['filter_fields'] = array(

                    'vm.id',

					'vm.video_name',

                    'vm.video_url',

					'vm.page',

					'vm.order_number',
            );

            parent::__construct($config);

        }

    protected function populateState($ordering = null, $direction = null) {

	    $orderCol   = JRequest::getCmd('filter_order', 'ur.user_id');

	    $this->setState('list.ordering', $orderCol);

		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');

		$this->setState('list.direction', $listOrder);

	    parent::populateState('vm.id', 'ASC');

	   }


	public function getListVideos(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

					'*'

			)

		);

		$query->from('#__video_management vm');
		$query->order('order_number ASC');
		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		//$query->where('buyer_type!="Inactive" AND buyer_type!=""');

        $db->setQuery($query);

		$results = $db->loadObjectList();

		return $results;
	}

	public function getMaxOrder($page){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select(

			array(

					'MAX(order_number) as last_count'

			)

		);



		$query->from('#__video_management vm');

		//$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		//$query->where('buyer_type!="Inactive" AND buyer_type!=""');

        $db->setQuery($query);

		$results = $db->loadObjectList();

		return $results;
	}



	function insertVideo($data) {

		$db = JFactory::getDbo();	

		$vid_object = new JObject();

		$vid_object->video_name = $data['video_name'];

		$vid_object->video_url = $data['video_url'];

		$vid_object->thumbnail = $data['thumbnail'];

		$vid_object->country = $data['country'];

		$vid_object->page = $data['page'];

		$vid_object->order_number = $data['order_number'];

		$db->insertObject('#__video_management', $vid_object);
    }

	function saveOrderToDB($data) {

		$db = JFactory::getDbo();	

		$vd_object = new JObject();

		$vd_object->id = $data['id'];

		$vd_object->order_number = $data['order_number'];

		$db->updateObject('#__video_management', $vd_object, 'id');
    } 

}

?>