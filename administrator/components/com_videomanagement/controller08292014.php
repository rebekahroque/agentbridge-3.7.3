<?php



	



defined( '_JEXEC' ) or die( 'Restricted access' );



 



jimport('joomla.application.component.controller');



jimport('joomla.mail.helper'); 



 



/**



 * Hello World Component Controller



 *



 * @package    Joomla.Tutorials



 * @subpackage Components



 */



class VideoManagementController extends JControllerLegacy



{



    /**



     * Method to display the view



     *



     * @access    public



     */


    function display(){

		$model = $this->getModel('VideoManagement');

		$results = $model->getListVideos();

		$view = &$this->getView($this->getName(), 'html');

		$view->assignRef( 'datas', $results );	

		$application = JFactory::getApplication();

		$task = JRequest::getVar('task');

        parent::display();

    }

	function UploaderVid(){

		$model = $this->getModel('VideoManagement');

		$j_abspath=JPATH_BASE;
		//$j_videopath=str_replace("administrator","",JPATH_BASE)."images/videos";
		$j_videopath=str_replace("administrator","",JPATH_BASE)."videos/us";

		require($j_abspath.'/components/com_videomanagement/includes/Uploader.php');

		$upload_dir = $j_videopath;
		$valid_extensions = array('mp4');

		$Upload = new FileUpload('uploadfile');
		$Upload->sizeLimit = (10485760*5);
		$result = $Upload->handleUpload($upload_dir, $valid_extensions);

		if (!$result) {
		    echo json_encode(array('success' => false, 'msg' => $Upload->getErrorMsg()));   
		} else {

			$data['video_url'] = $j_videopath."/".$Upload->getFileName();
			$data['video_name'] = urldecode($_GET['title']);

			$data['thumbnail'] = $_GET['thumbnail'];

			$data['page'] = "Support";
			$data['country'] = 12;

			$last_vid_count=$model->getMaxOrder($data['page']);
			$data['order_number'] = intval($last_vid_count[0]->last_count);

			$model->insertVideo($data);

		    echo json_encode(array('success' => true, 'file' => $Upload->getFileName()));
		}

	}  

	function UploaderThumb(){

		$model = $this->getModel('VideoManagement');

		$j_abspath=JPATH_BASE;
		//$j_videopath=str_replace("administrator","",JPATH_BASE)."images/videos";
		$j_imagepath=str_replace("administrator","",JPATH_BASE)."videos/us";

		require($j_abspath.'/components/com_videomanagement/includes/Uploader.php');

		$upload_dir = $j_imagepath;
		$valid_extensions = array('jpg','png','gif');

		$Upload = new FileUpload('uploadfile');
		$result = $Upload->handleUpload($upload_dir, $valid_extensions);

		if (!$result) {
		    echo json_encode(array('success' => false, 'msg' => $Upload->getErrorMsg()));   
		} else {
		    echo json_encode(array('success' => true, 'file' => $Upload->getFileName()));
		}

	} 

	function saveVideoOrder(){

		$model = $this->getModel('VideoManagement');

		$i = 0;

		foreach ($_POST['item'] as $value) {
		    // Execute statement:
		    // UPDATE [Table] SET [Position] = $i WHERE [EntityId] = $value
		    $data['id'] = $value;
		    $data['order_number'] = $i;
		    $model->saveOrderToDB($data);
		    $i++;
		}

	}  

	function checkFileExists(){

		$j_abspath=JPATH_BASE;
		$j_videopath=str_replace("administrator","",JPATH_BASE)."videos/us";

		$filename=$j_videopath."/".$_POST['filename'];

		if (file_exists($filename)) {
		   echo "1";
		} else {
		   echo "0";
		}

	}    
}



?>