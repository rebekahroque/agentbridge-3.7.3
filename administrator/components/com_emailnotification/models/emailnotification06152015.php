<?php



defined('_JEXEC') or die;







class EmailNotificationModelEmailNotification extends JModelLegacy 

{	

	function getUsers(){


		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->where('ur.is_term_accepted = 1');

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();
		
		return $users_email;


	}
	
	function getUsersByState($state){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->where('ur.is_term_accepted = 1 AND ur.state='.$state);

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();
		
		return $users_email;


	}

	function getStates(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__zones');
		
		//$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->where('zone_country_id=223');

		$db->setQuery($query);
		
		$state_list = $db->loadObjectList();
		
		return $state_list;
	}
	
	
	function get_totalave2012(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2012');

		$query->from('#__user_sales');
		
		$query->where('verified_2012 = 1');

		$db->setQuery($query);
		
		$totalave2012 = $db->loadObjectList();
		
		return $totalave2012;


	}
	
	function get_totalave2013(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2013');

		$query->from('#__user_sales');
		
		$query->where('verified_2013 = 1');

		$db->setQuery($query);
		
		$totalave2013 = $db->loadObjectList();
		
		return $totalave2013;


	}
	
	function get_totalave2014(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2014');

		$query->from('#__user_sales');
		
		$query->where('verified_2014 = 1');

		$db->setQuery($query);
		
		$totalave2014 = $db->loadObjectList();
		
		return $totalave2014;


	}
	
	function getvaluePOPsp2(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__pocket_listing pl');
		
		$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');
		
		$query->where('pl.closed=0 AND pp.price2 IS NOT NULL');

		$db->setQuery($query);
		
		$price2 = $db->loadObjectList();
		
		return $price2;


	}
	
	function getvaluePOPsp1(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		
		$query->select('*');

		$query->from('#__pocket_listing pl');
		
		$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');
		
		$query->where('pl.closed=0 AND pp.price2 IS NULL');

		$db->setQuery($query);
		
		$price1 = $db->loadObjectList();
		
		return $price1;


	}
	
	 
	 
	 
	 function getImageAndName($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'users.email'



			);



			$query->from($db->quoteName('#__users').' AS users'); 



			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("users.id = '$userid' ");



			$db->setQuery($query);



			$email = $db->loadObject()->email;



			$query = $db->getQuery(true);	   



		    $query->select(



				'user_regs.*'



			);



			$query->from($db->quoteName('#__user_registration').' AS user_regs'); 



			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("user_regs.email = '$email' ");



			$db->setQuery($query);



			return $db->loadObjectList();



	 }



	 function getBuyers($userid){


	 
	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(

				'SUM(buyer.listingcount_new) AS pops_matched'


			);



			$query->from($db->quoteName('#__buyer').' AS buyer'); 



			$query->where("buyer.agent_id = ".$userid." AND buyer.hasnew_2=1");


			$db->setQuery($query);



			return $db->loadObject()->pops_matched;
		


	 }
	 
	
	function getBuyersAll($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(

				'SUM(buyer.listingcount) AS pops_matched'


			);


			$query->from($db->quoteName('#__buyer').' AS buyer'); 



			$query->where("buyer.agent_id = ".$userid." AND buyer_type!='Inactive'");


			$db->setQuery($query);



			return $db->loadObject()->pops_matched;



	 }



	 function getReqtoViewPOPs($userid){



	 	    $db = JFactory::getDbo();



		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(req_access.pkId) AS req_access_count'



			);



			$query->from($db->quoteName('#__request_access').' AS req_access'); 



			$query->where("req_access.user_a = '$userid' AND (req_access.permission = 0 OR req_access.permission = 2)");



			$db->setQuery($query);



			$req_access_count = $db->loadObject()->req_access_count;



			$db = JFactory::getDbo();



		   	$query = $db->getQuery(true);	   



		   	$query->select(



				'COUNT(req_network.pk_id) AS req_network_count'



			);



			$query->from($db->quoteName('#__request_network').' AS req_network'); 



			$query->where("req_network.user_id = '$userid' AND (req_network.status = 0 OR req_network.status = 2)");



			$db->setQuery($query);



			$req_network_count = $db->loadObject()->req_network_count;



 

			$total = $req_network_count + $req_access_count;





			return $total;



	 }





	 function getPendReferrals($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(referral.referral_id) AS referral_count'



			);



			$query->from($db->quoteName('#__referral').' AS referral'); 



			$query->where("status = 7 AND (agent_a = '$userid' OR agent_b = '$userid')");



			$db->setQuery($query);



			return $db->loadObject()->referral_count;



	 }





	 function getPOPsExpiring($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_toexpired'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 



			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' ");



			$db->setQuery($query);



			return $db->loadObject()->pocket_count_toexpired;



	 }



	 function getDaysActivities($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'DATEDIFF(CURRENT_TIMESTAMP(),date) AS date_diff, date'



			);



			$query->from($db->quoteName('#__activities').' AS activities'); 



			$query->where("activities.user_id = '$userid' AND (activities.activity_type = 2 OR activities.activity_type = 23)");



			$query->order('date DESC');



			$db->setQuery($query);



			return $db->loadObject()->date_diff;



	 }



	 function getPOPsThisWeek($userid){


	 	   $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			/*$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);


	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_info);

	 	//	$where =$user_zips->zip;

	 		$where ="pockets.user_id = '$userid'";

				*/

		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where = "(pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }

	 function getPOPsListing($userid){


	 	   $user_info=$this->getImageAndName($userid);

	 	   $db = JFactory::getDbo();
		
		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where = "(pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$zip3 = substr($user_info[0]->zip, 0, 3);


	 		$where .=" AND pockets.zip LIKE '%".$zip3."%'";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }


	 function getPOPsThisWeekAREA($userid){


	 	   $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);


	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;

	 		$where = "";

	 		if($user_zips_count){
		 		$where = "(";
		 		$i=1;
		 		foreach ($user_zips as $value) {

		 			$where .= "pockets.zip = ".$value->zip;
		 			if($i<$user_zips_count){
		 				$where .= " OR ";
		 			}

		 			$i++;
		 		}

		 		$where .=")";
			} else {
				$where = "pockets.user_id = '$userid' ";
			}
	
				

		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where .= " AND (pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }

	 function getBuyersThisWeek($userid){


	 	    $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);

	 		

	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;

	 		if($user_zips_count){
		 		$where = "(";
		 		$i=1;
		 		foreach ($user_zips as $value) {

		 			$where .= "buyers_a.zip = ".$value->zip;
		 			if($i<$user_zips_count){
		 				$where .= " OR ";
		 			}

		 			$i++;
		 		}

	 			$where .=")";
			} else {
				$where = "buyer.agent_id = '$userid' ";
			}



		    $query = $db->getQuery(true);	   


		    $query->select(

				'COUNT(buyers_a.buyer_id) AS buyer_count_inarea'

			);



			$query->from($db->quoteName('#__buyer_needs').' AS buyers_a'); 

			$query->leftJoin("#__activities activities on activities.buyer_id = buyers_a.buyer_id"); 
			$query->leftJoin("#__buyer buyer on buyer.buyer_id = buyers_a.buyer_id"); 


			$where ="(activities.date BETWEEN  DATE_SUB( NOW(), INTERVAL 7 DAY) AND NOW() ) AND buyer.buyer_type!='Inactive' AND activities.activity_type=23";

			$zip3 = substr($user_info[0]->zip, 0, 3);

		
	 		$where .=" AND buyers_a.zip LIKE '%".$zip3."%'";

			//(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' 

			$query->where($where);

			$db->setQuery($query);

			$return=$db->loadObject()->buyer_count_inarea;

			return $return;



	 }

	 	 function getBuyersAllThisWeek($userid){


	 	    $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);

	 		

	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;



		    $query = $db->getQuery(true);	   


		    $query->select(

				'COUNT(buyers_a.buyer_id) AS buyer_count_inarea'

			);



			$query->from($db->quoteName('#__buyer_needs').' AS buyers_a'); 

			$query->leftJoin("#__activities activities on activities.buyer_id = buyers_a.buyer_id"); 
			$query->leftJoin("#__buyer buyer on buyer.buyer_id = buyers_a.buyer_id"); 

			$where ="(activities.date BETWEEN  DATE_SUB( NOW(), INTERVAL 6 DAY) AND NOW() ) AND buyer.buyer_type!='Inactive' AND activities.activity_type=23";

			//(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' 

			$query->where($where);



			$db->setQuery($query);

			$return=$db->loadObject()->buyer_count_inarea;

			return $return;



	 }


	function getPendReferralsInArea($userid,$zip){

		   

	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(referral.referral_id) AS referral_count'



			);



			$query->from($db->quoteName('#__referral').' AS referral'); 

			$query->leftJoin("#__buyer_address buyer_a on buyer_a.buyer_id = referral.client_id"); 


		//	$query->where("status = 7 AND buyer_a.zip = '$zip' ");
			$query->where("status = 7");



			$db->setQuery($query);



			return $db->loadObject()->referral_count;



	 }


}



?>