<?php



defined('_JEXEC') or die;







class EmailNotificationModelEmailNotification extends JModelLegacy 

{	

	function getUsers(){


		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, ua.*, uss.*');

		$query->from('#__user_registration ur');
		
		$query->leftJoin('#__user_sales uss ON uss.agent_id = ur.user_id');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		#$query->where('ur.is_term_accepted = 1 AND ugm.group_id NOT IN(7,8,11,12)');
		
		$query->where('ur.is_term_accepted = 1');

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();
		
		return $users_email;


	}
	
	function getUsersByState($state,$limit){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, ua.cc_all, ua.a_email');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		$query->where('ur.is_term_accepted = 1 AND ur.state='.$state.' AND ugm.group_id NOT IN(11,12)');

		$query->group($db->quoteName('ur.user_id'));


		if($limit){
			$limit_area  = explode(",", $limit);
			$query->setLimit($limit_area[0],$limit_area[1]);
		}


		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();


		
		return $users_email;

	}

	function getUsersByUserId($userId){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, ua.cc_all, ua.a_email');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		$query->where('ur.is_term_accepted = 1  AND ugm.group_id NOT IN(11,12) AND ur.user_id = '.$userId);

		/*$query->group($db->quoteName('ur.user_id'));


		if($limit){
			$limit_area  = explode(",", $limit);
			$query->setLimit($limit_area[0],$limit_area[1]);
		}
*/

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();


		
		return $users_email;

	}

	function getUsersByCountry($country){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, ua.cc_all, ua.a_email');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		$query->where('ur.is_term_accepted = 1 AND ur.country='.$country.' AND ugm.group_id NOT IN(11,12)');

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();
		
		return $users_email;


	}

	function getStates(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__zones');
		
		//$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->where('zone_country_id=223 OR zone_country_id=38');

		$db->setQuery($query);
		
		$state_list = $db->loadObjectList();


		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.email');

		$query->from('#__user_registration ur');
		
		$query->leftjoin('#__users us ON us.email = ur.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');

		$query->where('ur.is_term_accepted = 1 AND ur.state=12');

		$db->setQuery($query);
		
		//var_dump($db->loadColumn());

		$ca_user = count($db->loadColumn());


		$called_user = 100;
		$limit_ca_user =array($called_user.",0");
		$limit_first=0;
		$limit_second=$called_user;
		$orig_ca_user_count = $ca_user;


		while ($ca_user > 0) {
			if($ca_user>$called_user ){
				$limit_first=$called_user;				
				$remain = $orig_ca_user_count - $limit_second;
				$limit_ca_user[]=$limit_first.",".$limit_second;
				$limit_second=$limit_second+$called_user;
			} 
			$ca_user-=$called_user;
		}

		// ["zone_id"]=> string(2) "12" ["zone_country_id"]=> string(3) "223" ["zone_code"]=> string(2) "CA" ["zone_name"]=> string(10) "California"



		$ti = 1;

		foreach ($state_list as $key => $value) {
			# code...
			if($value->zone_code == "CA"){
				unset($state_list[$key]);
			}
		}

		foreach ($limit_ca_user as $key => $value) {
			# code...
			$obj_contain = new stdClass();
			$obj_contain->zone_id="12";
			$obj_contain->zone_country_id="223";
			$obj_contain->zone_code="CA";
			$obj_contain->zone_name="California Batch ".$ti;
			$obj_contain->zone_limit=$value;
			$obj_contain->zone_order=$ti;
			$state_list[] = $obj_contain;
			$ti++;
		}

		return $state_list;
	}
	
	
	public function get_totalave2012(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2012,ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');
		
		$query->where('verified_2012 = 1');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$db->setQuery($query);
		
		$totalave2012 = $db->loadObjectList();
		
		return $totalave2012;


	}
	
	public function get_totalave2013(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2013,ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->where('verified_2013=1');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$db->setQuery($query);
		
		$totalave2013 = $db->loadObjectList();
		
		return $totalave2013;


	}

	public function get_totalave2014(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('volume_2014, ur.country, c.currency, ur.user_id');

		$query->from('#__user_sales us');

		$query->where('verified_2014=1');

		$query->leftJoin('#__user_registration ur ON ur.user_id  = us.agent_id');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$db->setQuery($query);
		
		$totalave2014 = $db->loadObjectList();
		
		return $totalave2014;


	}

	
	public function get_AveMemberVolume(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('u.*, us.*, ur.*,c.currency as user_currency');

		$query->from('#__users u');

		$query->leftJoin('#__user_registration ur ON ur.email  = u.email');

		$query->leftJoin('#__country_currency c ON c.country  = ur.country');

		$query->leftJoin('#__user_sales us ON us.agent_id = ur.user_id');

		$query->leftJoin('#__user_usergroup_map ugmp ON ugmp.user_id = u.id');		

		$query->where('ugmp.group_id!=8 ');

		$db->setQuery($query);
		
		$totalave2015 = $db->loadObjectList();
		
		return $totalave2015;

	}
	
	function getvaluePOPsp2(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*');

		$query->from('#__pocket_listing pl');
		
		$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');
		
		$query->where('pl.closed=0 AND pp.price2 IS NOT NULL');

		$db->setQuery($query);
		
		$price2 = $db->loadObjectList();
		
		return $price2;


	}
	
	function getvaluePOPsp1(){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		
		$query->select('*');

		$query->from('#__pocket_listing pl');
		
		$query->leftJoin('#__property_price pp ON pp.pocket_id = pl.listing_id');
		
		$query->where('pl.closed=0 AND pp.price2 IS NULL');

		$db->setQuery($query);
		
		$price1 = $db->loadObjectList();
		
		return $price1;


	}
	
	 
	 
	 
	 function getImageAndName($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'users.email'



			);



			$query->from($db->quoteName('#__users').' AS users'); 
			



			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("users.id = '$userid' ");



			$db->setQuery($query);



			$email = $db->loadObject()->email;



			$query = $db->getQuery(true);	   



		    $query->select(



				'user_regs.*, ucm.contact_options'



			);



			$query->from($db->quoteName('#__user_registration').' AS user_regs'); 
			$query->leftJoin("#__users u on u.email = user_regs.email"); 
			$query->leftJoin("#__user_contact_method ucm on ucm.user_id = u.id"); 


			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("user_regs.email = '$email' ");



			$db->setQuery($query);



			return $db->loadObjectList();



	 }



	 function getBuyers($userid){


	 
	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(

				'SUM(buyer.listingcount_new) AS pops_matched'


			);



			$query->from($db->quoteName('#__buyer').' AS buyer'); 



			$query->where("buyer.agent_id = ".$userid." AND buyer.hasnew_2=1");


			$db->setQuery($query);



			return $db->loadObject()->pops_matched;
		


	 }
	 
	
	function getBuyersAll($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(

				'SUM(buyer.listingcount) AS pops_matched'


			);


			$query->from($db->quoteName('#__buyer').' AS buyer'); 



			$query->where("buyer.agent_id = ".$userid." AND buyer_type!='Inactive'");


			$db->setQuery($query);



			return $db->loadObject()->pops_matched;



	 }



	 function getReqtoViewPOPs($userid){



	 	    $db = JFactory::getDbo();



		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(req_access.pkId) AS req_access_count'



			);



			$query->from($db->quoteName('#__request_access').' AS req_access'); 



			$query->where("req_access.user_a = '$userid' AND (req_access.permission = 0 OR req_access.permission = 2)");



			$db->setQuery($query);



			$req_access_count = $db->loadObject()->req_access_count;



			$db = JFactory::getDbo();



		   	$query = $db->getQuery(true);	   



		   	$query->select(



				'COUNT(req_network.pk_id) AS req_network_count'



			);



			$query->from($db->quoteName('#__request_network').' AS req_network'); 



			$query->where("req_network.user_id = '$userid' AND (req_network.status = 0 OR req_network.status = 2)");



			$db->setQuery($query);



			$req_network_count = $db->loadObject()->req_network_count;



 

			$total = $req_network_count + $req_access_count;





			return $total;



	 }





	 function getPendReferrals($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(referral.referral_id) AS referral_count'



			);



			$query->from($db->quoteName('#__referral').' AS referral'); 



			$query->where("status = 7 AND (agent_a = '$userid' OR agent_b = '$userid')");



			$db->setQuery($query);



			return $db->loadObject()->referral_count;



	 }





	 function getPOPsExpiring($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_toexpired'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 



			//	$query->where("pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)");

			$query->where("(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' ");



			$db->setQuery($query);



			return $db->loadObject()->pocket_count_toexpired;



	 }


	 public function getViewPOPs($listing_id){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('DISTINCT (user_id) ');
		$query->from('#__views');
		$query->where('property_id='.$listing_id);
		$db->setQuery($query);
		$before_view =  count($db->loadColumn());

		return $before_view;

	}

	public function getRandViewPOPs($listing_id){

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('rand_view_count');
		$query->from('#__pocket_listing');
		$query->where('listing_id='.$listing_id);
		$db->setQuery($query);
		$rand_view_save =  $db->loadResult();

		return $rand_view_save;

	}

	 function getPOPsTotalView($userid){

	 	//pops table
	 	//views table

	 	$totalcount = 0;

	 	$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('p.rand_view_count,p.listing_id,p.date_created,v.views');
		$query->from('#__pocket_listing p');
		$query->leftJoin('(SELECT COUNT( user_id ) AS views, property_id FROM #__views GROUP BY property_id) v on v.property_id = p.listing_id');
		$query->where('user_id='.$userid);
		$db->setQuery($query);

		$data = $db->loadObjectList();

		foreach ($data as $pk) {
			if(  strtotime($pk->date_created) < strtotime('-3 days')  ) {
				if(!$pk->views ){
			  		$totalcount += 0;
			  	} else {
			  	//	$rand_count = $model->getRandViewPOPs($pk->listing_id);
					$totalcount += $pk->rand_view_count + $pk->views;
			  	}
			} else {
				if(!$pk->views ){
			  		$totalcount += 1;
			  	} else {
			  	//	$rand_count = $model->getRandViewPOPs($pk->listing_id);
					$totalcount += $pk->rand_view_count + $pk->views;
			  	}				
			}
		}

		return $totalcount;
	 }



	 function getDaysActivities($userid){



	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'DATEDIFF(CURRENT_TIMESTAMP(),date) AS date_diff, date'



			);



			$query->from($db->quoteName('#__activities').' AS activities'); 



			$query->where("activities.user_id = '$userid' AND (activities.activity_type = 2 OR activities.activity_type = 23)");



			$query->order('date DESC');



			$db->setQuery($query);



			return $db->loadObject()->date_diff;



	 }



	 function getPOPsThisWeek($userid){


	 	   $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			/*$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);


	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_info);

	 	//	$where =$user_zips->zip;

	 		$where ="pockets.user_id = '$userid'";

				*/

		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where = "(pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }

	 function getPOPsListing($userid){


	 	   $user_info=$this->getImageAndName($userid);

	 	   $db = JFactory::getDbo();
		
		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where = "(pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$zip3 = substr($user_info[0]->zip, 0, 3);


	 		$where .=" AND pockets.zip LIKE '%".$zip3."%'";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }


	 function getPOPsThisWeekAREA($userid){


	 	   $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);


	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;

	 		$where = "";

	 		if($user_zips_count){
		 		$where = "(";
		 		$i=1;
		 		foreach ($user_zips as $value) {

		 			$where .= "pockets.zip LIKE '".$value->zip."'";
		 			if($i<$user_zips_count){
		 				$where .= " OR ";
		 			}

		 			$i++;
		 		}

		 		$where .=")";
			} else {
				$where = "pockets.user_id = '$userid' ";
			}
	
				

		    $query = $db->getQuery(true);	   



		    $query->select(



				'COUNT(pockets.listing_id) AS pocket_count_inarea'



			);



			$query->from($db->quoteName('#__pocket_listing').' AS pockets'); 

			$where .= " AND (pockets.date_created BETWEEN DATE_SUB(NOW(), INTERVAL 6 DAY) AND NOW() ) AND pockets.closed=0";

			$query->where($where);


			$db->setQuery($query);



			return $db->loadObject()->pocket_count_inarea;



	 }

	 function getBuyersThisWeek($userid){


	 	    $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);

	 		

	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;

	 		if($user_zips_count){
		 		$where = "(";
		 		$i=1;
		 		foreach ($user_zips as $value) {

		 			$where .= "buyers_a.zip = ".$value->zip;
		 			if($i<$user_zips_count){
		 				$where .= " OR ";
		 			}

		 			$i++;
		 		}

	 			$where .=")";
			} else {
				$where = "buyer.agent_id = '$userid' ";
			}



		    $query = $db->getQuery(true);	   


		    $query->select(

				'COUNT(buyers_a.buyer_id) AS buyer_count_inarea'

			);



			$query->from($db->quoteName('#__buyer_needs').' AS buyers_a'); 

			$query->leftJoin("#__activities activities on activities.buyer_id = buyers_a.buyer_id"); 
			$query->leftJoin("#__buyer buyer on buyer.buyer_id = buyers_a.buyer_id"); 


			$where ="(activities.date BETWEEN  DATE_SUB( NOW(), INTERVAL 7 DAY) AND NOW() ) AND buyer.buyer_type!='Inactive' AND activities.activity_type=23";

			$zip3 = substr($user_info[0]->zip, 0, 3);

		
	 		$where .=" AND buyers_a.zip LIKE '%".$zip3."%'";

			//(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' 

			$query->where($where);

			$db->setQuery($query);

			$return=$db->loadObject()->buyer_count_inarea;

			return $return;



	 }

	 	 function getBuyersAllThisWeek($userid){


	 	    $user_info=$this->getImageAndName($userid);


	 	    $db = JFactory::getDbo();



			$query = $db->getQuery(true);



			$query->select('distinct(zip) as zip, count(zip) as count');



			$query->from('#__pocket_listing');



			$query->group('zip');



			$query->where("user_id = '$userid' ");



			$db->setQuery($query);

	 		

	 		$user_zips=$db->loadObjectList();

	 		$user_zips_count = count($user_zips);

	 	//	$where =$user_zips->zip;



		    $query = $db->getQuery(true);	   


		    $query->select(

				'COUNT(buyers_a.buyer_id) AS buyer_count_inarea'

			);



			$query->from($db->quoteName('#__buyer_needs').' AS buyers_a'); 

			$query->leftJoin("#__activities activities on activities.buyer_id = buyers_a.buyer_id"); 
			$query->leftJoin("#__buyer buyer on buyer.buyer_id = buyers_a.buyer_id"); 

			$where ="(activities.date BETWEEN  DATE_SUB( NOW(), INTERVAL 6 DAY) AND NOW() ) AND buyer.buyer_type!='Inactive' AND activities.activity_type=23";

			//(pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY) ) AND pockets.user_id = '$userid' 

			$query->where($where);



			$db->setQuery($query);

			$return=$db->loadObject()->buyer_count_inarea;

			return $return;



	 }


	function getPendReferralsInArea($userid,$zip){

		   

	 	   $db = JFactory::getDbo();



		   $query = $db->getQuery(true);	   



		   $query->select(



				'COUNT(referral.referral_id) AS referral_count'



			);



			$query->from($db->quoteName('#__referral').' AS referral'); 

			$query->leftJoin("#__buyer_address buyer_a on buyer_a.buyer_id = referral.client_id"); 


		//	$query->where("status = 7 AND buyer_a.zip = '$zip' ");
			$query->where("status = 7");



			$db->setQuery($query);



			return $db->loadObject()->referral_count;



	 }
	 
	function getBuyersWithinArea($zip, $country_id, $random_num) {

		var_dump("rand = ".$random_num);

		$partial_zip = substr($zip, 0, 3);
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("COUNT(b.buyer_id) as buyer_count");
		$query->from($db->quoteName('#__buyer').' as b'); 
		$query->leftJoin("#__buyer_needs as bn ON bn.buyer_id = b.buyer_id");
		if($country_id == 223) {
			$query->where("b.buyer_type != 'Inactive' AND bn.country = '" . $country_id . "' AND bn.zip LIKE '%" . $partial_zip . "%'");
		} else {
			$query->where("b.buyer_type != 'Inactive'");
		}
		
		$db->setQuery($query);

		var_dump("bcounts = ".$db->loadObject()->buyer_count);
		
		return ($country_id == 223) ? intval($db->loadObject()->buyer_count) + $random_num : intval($db->loadObject()->buyer_count);
		
	}
	
	function getUsersByStateOptimized($state,$limit){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select("
			u.id, u.name, u.email,
			ur.firstname, ur.lastname,
			CHAR_LENGTH(contact_options) as charlength,
			CASE CHAR_LENGTH(contact_options)
				WHEN 37 THEN SUBSTRING(contact_options FROM 31 FOR 1)
				WHEN 21 THEN SUBSTRING(contact_options FROM 15 FOR 1)
				WHEN 0 THEN contact_options
			END as 'is_subscribed',
			bpm.pops_matched as bpmatched,
			rvp.req_access_count as reqaccess,
			prfa.referral_count as referral_count_a,
			prfb.referral_count as referral_count_b,
			ptexp.pocket_count_toexpired,
			lastact.date_diff as date_difference
		");
		

		$query->from('#__users u');
		
		$query->leftjoin('#__user_registration ur ON u.email = ur.email ');
		
		$query->leftjoin('#__user_contact_method ucm ON ucm.user_id = u.id');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = u.id');
		
		$query->leftjoin(
			"(
				SELECT buyer.agent_id, SUM(buyer.listingcount_new) as pops_matched
				FROM #__buyer as buyer
				WHERE buyer.hasnew_2 = 1
				GROUP BY buyer.agent_id
			) as bpm ON bpm.agent_id = u.id
			"
		);
		
		$query->leftjoin(
			"(
				SELECT req_access.user_a, COUNT(req_access.pkId) as req_access_count
				FROM #__request_access as req_access
				WHERE req_access.permission = 0 OR req_access.permission = 2
				GROUP BY req_access.user_a
			) as rvp ON rvp.user_a = u.id"
		);
		
		$query->leftjoin(
			"(
				SELECT agent_a, COUNT(referral.referral_id) as referral_count
				FROM #__referral as referral
				WHERE status = 7
				GROUP BY agent_a
			) as prfa ON prfa.agent_a = u.id"
		);
		
		$query->leftjoin(
			"(
				SELECT agent_b, COUNT(referral.referral_id) as referral_count
				FROM #__referral as referral
				WHERE status = 7
				GROUP BY agent_b
			) as prfb ON prfb.agent_b = u.id"
		);
		
		$query->leftjoin(
			"(
				SELECT pockets.user_id, COUNT(pockets.listing_id) as pocket_count_toexpired
				FROM #__pocket_listing as pockets
				WHERE (pockets.date_expired BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY))
				GROUP BY pockets.user_id
			) as ptexp ON ptexp.user_id = u.id"
		);
		
		$query->leftjoin(
			"(
				SELECT user_id, DATEDIFF(CURRENT_TIMESTAMP(), date) as date_diff, date
				FROM #__activities as activities
				WHERE activities.activity_type = 2 OR activities.activity_type = 23
				GROUP BY user_id, activity_id
				ORDER BY date_diff ASC
			) as lastact ON lastact.user_id = u.id "
		);
		
		$query->where('
			ur.is_term_accepted = 1 
			AND ur.state='.$state.' 
			AND ugm.group_id NOT IN(11,12)
			AND CASE CHAR_LENGTH(contact_options)
					WHEN 37 THEN SUBSTRING(contact_options FROM 31 FOR 1) = 8
					WHEN 21 THEN SUBSTRING(contact_options FROM 15 FOR 1) = 8
					WHEN NULL THEN ISNULL(contact_options)
				END
		');

		$query->group($db->quoteName('ur.user_id'));
		$query->order("lastact.date_diff DESC");

		if($limit){
			$limit_area  = explode(",", $limit);
			$query->setLimit($limit_area[0],$limit_area[1]);
		}


		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();


		
		return $users_email;

	}
	
	function getStateUsers($state,$limit){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, us.email as email, ua.cc_all, ua.a_email');

		$query->from('#__users us');
		
		$query->leftjoin('#__user_registration ur ON ur.email = us.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		$query->where('ur.is_term_accepted = 1 AND ur.state='.$state.' AND ugm.group_id NOT IN(11,12)');

		$query->group($db->quoteName('ur.user_id'));


		if($limit){
			$limit_area  = explode(",", $limit);
			$query->setLimit($limit_area[0],$limit_area[1]);
		}


		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();


		
		return $users_email;

	}
	
	function getCountryUsers($country){

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('ur.*, us.*, us.email as email, ua.cc_all, ua.a_email');

		$query->from('#__users us');
		
		$query->leftjoin('#__user_registration ur ON ur.email = us.email ');

		$query->leftjoin('#__user_assistant ua ON ua.user_id = ur.user_id ');
		
		$query->leftjoin('#__user_usergroup_map ugm ON ugm.user_id = us.id');

		$query->where('ur.is_term_accepted = 1 AND ur.country='.$country.' AND ugm.group_id NOT IN(11,12)');

		$db->setQuery($query);
		
		$users_email = $db->loadObjectList();
		
		return $users_email;


	}



}



?>