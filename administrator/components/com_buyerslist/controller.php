<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.controller');
jimport('joomla.mail.helper'); 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class BuyersListController extends JControllerLegacy
{
    /**
     * Method to display the view
     *
     * @access    public
     */
    function display()
    {
		
		$model = $this->getModel('BuyerList');
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language->load($extension, $base_dir, "english-US", true);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(
			array(
					'
					 pl.closed,
					 b.name AS buyer_name,
					 b.buyer_type, 
					 b.home_buyer,
					 ur.user_type,
					 b.buyer_id,
					 pt.type_name AS property_name,
					 ps.name AS property_sub_type,
					 u.name AS agent_name, 					
					 bne.zip,
					 b.price_value,
					 a.date,
					 b.agent_id,
					 bne.currency
					 '
			)
		);
		$query->from('#__buyer b');
		$query->leftJoin('#__buyer_needs bne ON bne.buyer_id = b.buyer_id');
		$query->leftJoin('#__pocket_listing pl ON pl.listing_id = bne.listing_id');
		$query->leftJoin('#__property_type pt ON pt.type_id = bne.property_type');
		$query->leftJoin('#__property_sub_type ps ON ps.sub_id = bne.sub_type');
		$query->leftJoin('#__users u ON u.id = b.agent_id');
		$query->leftJoin('#__user_registration ur ON ur.email = u.email');
		$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
		$query->where('buyer_type!=""');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		//Agent with active pops and buyers
		$query2 = $db->getQuery(true);
		$query2->select(
			array(
					'
					 u.id
					'
			)
		);
		$query2->from('#__users u');
		$query2->leftJoin('#__pocket_listing pl ON pl.user_id = u.id');
		$query2->leftJoin('#__buyer b ON b.agent_id = u.id');
		$query2->where('(b.buyer_type!="Inactive" AND b.buyer_type!="") AND pl.closed!=1');
        $db->setQuery($query2);
		$results2 = $db->loadObjectList();
		$view = &$this->getView($this->getName(), 'html');
		$view->assignRef( 'datas', $results );	
		//Assignref for Agent with active pops and buyers
		$view->assignRef( 'agent_datas', $results2 );	
		$application = JFactory::getApplication();
		$task = JRequest::getVar('task');
		switch ($task):
			case "export_csv":
				generateCSV();
			break;
		endswitch;
        parent::display();
    }
    function export_csv()
	{
		$data='test1,test2,test3,';
		//$model = $this->getModel('listworkdownload');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=file.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print $data ;
		die();
	}
		   ###############################################################
	## This function will generate a report of invoices XLS Format
	function generateCSV(){
		$language = JFactory::getLanguage();
        $extension = 'com_nrds';
        $base_dir = JPATH_SITE;
        $language->load($extension, $base_dir, "english-US", true);
	      ## Make DB connections
	      $db    = JFactory::getDBO();
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select(
				array(
					'b.buyer_id,
					 b.buyer_type, 
					 b.home_buyer,
					 pt.type_name AS property_type,
					 ps.name AS property_sub_type,
					 u.name AS agent, 
					 u.email, 
					 umn.value AS contact_number,
					 ur.user_type,
					 bne.zip AS zip_code,		 
					 b.price_value AS price,
					 bne.currency,
					 a.date'
				)
			);
			$query->from('#__buyer b');
			$query->leftJoin('#__buyer_needs bne ON bne.buyer_id = b.buyer_id');
			$query->leftJoin('#__property_type pt ON pt.type_id = bne.property_type');
			$query->leftJoin('#__property_sub_type ps ON ps.sub_id = bne.sub_type');
			$query->leftJoin('#__users u ON u.id = b.agent_id');
			$query->leftJoin('#__user_registration ur ON ur.email = u.email');
			$query->leftJoin('#__user_mobile_numbers umn ON umn.user_id = ur.user_id');
			$query->leftJoin('#__activities a ON a.buyer_id = b.buyer_id AND a.activity_type = 23');
			$query->where('buyer_type!=""');
	      $db->setQuery($query);
	      $rows = $db->loadAssocList();
	      $time = time();
	      ## If the query doesn't work..
	      if (!$db->query() ){
	         echo "<script>alert('Please report your problem.');
	         window.history.go(-1);</script>\n";       
	      }   
	      ## Empty data vars
	      $data = "" ;
	      ## We need tabbed data
	      $sep = ","; 
	      $fields = (array_keys($rows[0]));
	      ## Count all fields(will be the collumns
	      $columns = count($fields);
	      ## Put the name of all fields to $out.  
	      for ($i = 0; $i < $columns; $i++) {
	      	if($fields[$i]=="zone_name"){
	      		$data .= "State".$sep;
	      	}else
	       		$data .= ucwords(str_replace("_", " ", $fields[$i])).$sep;
	      }
	      $data .= "\n";
	      ## Counting rows and push them into a for loop
	      for($k=0; $k < count( $rows ); $k++) {
	         $row = $rows[$k];
	         $line = '';
	         ## Now replace several things for MS Excel
	         foreach ($row as $key=>$value) {
	         	if($key=="price_type"){
	         	 $value = $value == 1 ? "Price Range" : "Exact Price";  
	         	} elseif($key=="zip_code"){
	         	 $value = str_replace(array("\\","/",","), "-",  $value);
	         	}elseif($key=="price"){
         		 $price_arr=explode("-", $value);
				 $price_styled="$".$price_arr[0]."- $".$price_arr[1];
	         	 $value = str_replace(array("\\","/",","), "",  $price_styled);
	         	} else if($key=="property_type" || $key=="property_sub_type"){
				 $value = JText::_($value);
	         	} else
	         	 $value = str_replace(array("\\","/",","), "",  $value);
	           $line .=   $value .  ",";
	         }
	         $data .= trim($line)."\n";
	      }
	      $data = str_replace("\r","",$data);
	      ## If count rows is nothing show o records.
	      if (count( $rows ) == 0) {
	        $data .= "\n(0) Records Found!\n";
	      }
	      ## Push the report now!
	      $this->name = 'Buyers-List-'.date("m-d-Y",$time);
	     // header("Content-type: application/octet-stream");
	     // header("Content-Disposition: attachment; filename=".$this->name.".xls");
	      header("Content-type: text/csv");
	      header("Content-Disposition: attachment; filename=".$this->name.".csv");
	      header("Pragma: no-cache");
	      header("Expires: 0");
	      //header("Location: excel.htm?id=yes");
	      print $data ;
	      die();   
	   }
}
?>