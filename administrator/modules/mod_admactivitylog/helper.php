<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_admactivity
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_admactivity
 *
 * @since  1.5
 */
abstract class ModAdmactivitylogHelper
{
	/**
	 * Get a list of logged users.
	 *
	 * @param   \Joomla\Registry\Registry  &$params  The module parameters.
	 *
	 * @return  mixed  An array of users, or false on error.
	 *
	 * @throws  RuntimeException
	 */
	public static function getList(&$params)
	{
		$db    = JFactory::getDbo();
		$user  = JFactory::getUser();
		$query = $db->getQuery(true)
			->select('u.id, a.activity_details, u.name, dcreated as time')
			->from('#__admin_activities AS a')
			->leftJoin('#__users AS u ON a.user_id = u.id')
			->order('dcreated DESC');
		$db->setQuery($query, 0, $params->get('count', 10));

		try
		{
			$results = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			throw $e;
		}
		/*
		foreach ($results as $k => $result)
		{
			$results[$k]->logoutLink = '';

			if ($user->authorise('core.manage', 'com_users'))
			{
				$results[$k]->editLink   = JRoute::_('index.php?option=com_users&task=user.edit&id=' . $result->id);
				$results[$k]->logoutLink = JRoute::_('index.php?option=com_login&task=logout&uid=' . $result->id . '&' . JSession::getFormToken() . '=1');
			}

			if ($params->get('name', 1) == 0)
			{
				$results[$k]->name = $results[$k]->username;
			}
		}
		*/
		return $results;
	}

	/**
	 * Get the alternate title for the module
	 *
	 * @param   \Joomla\Registry\Registry  $params  The module parameters.
	 *
	 * @return  string    The alternate title for the module.
	 */
	public static function getTitle($params)
	{
		return JText::plural('MOD_ADMACTIVITYLOG_TITLE', $params->get('count'));
	}
}
