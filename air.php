<?php

/*
* @copyright   Wireless Emporium (http://www.wirelessemporium.com)
*/

class Activity_In_Repo
{
    public $toEmail = array(
        'kesarweb@gmail.com',
		'dwant@agentbridge.com',
	);

    public $notCheckedNewFile = array(
        'air.php'
    );

    public $notCheckedFile = array(
        'test1.php',
        'test2.php'
    );

    public $notCheckedFileExtension = array(
        'txt',
        'out',
        'log',
        'gz',
        'csv',
		'zip'
    );

    public $notCheckedDirectory = array(
        'var/',
        'media2/'
    );

    protected function _validate()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            die('This script cannot be run from Browser');
        }
    }

    public function run()
    {
        $this->_validate();
        chdir(dirname(__FILE__));
        $string = shell_exec("git status");
        $body = '';
        if ($string) {
            $serverName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : gethostname();
            $data = str_replace("\t", "###", $string);
            $gitStatus = nl2br($data);
            $arr = explode("<br />", $gitStatus);
            $res = array();
            foreach ($arr as $value) {
                if (strpos($value, "###")) {
                    $res[] = str_replace("###", "", $value);
                }
            }
            $html = "<table style='border: 1px solid #000000'>
                        <tr>
                            <th style='border: 1px solid #000000'>Server Name</th>
                            <th style='border: 1px solid #000000'>Event Type</th>
                            <th style='border: 1px solid #000000'>List of files</th>
                            <th style='border: 1px solid #000000'>Execution Date</th>
                        </tr>";
            $list = '';
            foreach ($res as $val) {
                if (($this->checkedFile($val, $this->notCheckedFile) !== false)
                    && ($this->checkedFile($val, $this->notCheckedDirectory) !== false)
                    && ($this->checkedFileExtension($val, $this->notCheckedFileExtension) !== false)) {
                    $list = '';
                    if (strpos($val, "new file")) {
                        if ($this->checkedFile($val, $this->notCheckedNewFile) !== false) {
                            $event = 'New file';
                            $list = substr(strrchr($val, ":"), 1);
                        }
                    } elseif (strpos($val, "modified")) {
                        $event = 'Modified';
                        $list = substr(strrchr($val, ":"), 1);
                    } elseif (strpos($val, "deleted")) {
                        $event = 'Deleted';
                        $list = substr(strrchr($val, ":"), 1);
                    } elseif (strpos($val, "renamed")) {
                        $event = 'Renamed';
                        $list = substr(strrchr($val, ":"), 1);
                    } else {
                        $event = 'Others';
                        $list = $val;
                    }

                    if ($list !== '') {
                        $html .= "<tr>
                                 <td style='border: 1px solid #000000'>" . $serverName . "</td>
                                 <td style='border: 1px solid #000000'>" . $event . "</td>
                                 <td style='border: 1px solid #000000'>" . $list . "</td>
                                 <td style='border: 1px solid #000000'>" . date("d/m/Y H:i:s A") . "</td>
                             </tr>";
                    }
                }
            }
            $html .= '</table>';
            if (!$list == '') {
                $title = 'Activity in MP Joomla Repo. (ALERT)';
                $body = $html;
            } else {
                $title = 'No activity in MP Joomla Repo ' . $serverName;
            }
        } else {
            $title = 'Something went wrong along repo monitoring execution';
        }
        $this->sendRepoEmail($body, $title);
    }

    public function checkedFile($string, $array)
    {
        $result = true;
        if (count($array)) {
            foreach($array as $notAddFile) {
                if (strpos($string, $notAddFile)) {
                    $result = false;
                    break;
                }
            }
        }
        return $result;
    }

    public function checkedFileExtension($string, $array)
    {
        $result = true;
        if (count($array)) {
            foreach($array as $notAddFile) {
                $str = end(explode(".", $string));
                if (strpos($str, $notAddFile) === 0) {
                    $result = false;
                    break;
                }
            }
        }
        return $result;
    }

    public function sendRepoEmail($body, $title)
    {
        $email = '';
        foreach ($this->toEmail as $value) {
                $email .= $value . ', ';
        }
        $headers = "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: no-reply@agentbridge.com";

        try {
            mail($email, $title, $body, $headers);
        } catch (Exception $e) {
            $text = date("Y-m-d H:i:s") . " - Error send email (Activity in Repo)\n";
            $fp = fopen("var/log/air.log", "a");
            fwrite($fp, $text);
            fclose($fp);
        }
    }
}

$activity = new Activity_In_Repo;
$activity->run();