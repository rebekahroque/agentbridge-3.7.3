<div class="pocket-form">
<?php 
	
	switch ($this->sub_type){
		// RESIDENTIAL PURCHASE + SFR
		case 1:
?>

	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label>
		<select id="jform_condition" name="jform[condition]">
			<option value="Fixer">Fixer</option>
			<option value="Good">Good</option>
			<option value="Excellent">Excellent</option>
			<option value="Remodeled">Remodeled</option>
			<option value="New Construction">New Construction</option>
			<option value="Under Construction">Under Construction</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features3" name="jform[features3]">
			<option value=""></option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>

<?php	
			break;
	
		// RESIDENTIAL PURCHASE + CONDO
		case 2:
?>	

	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Unit Sq. Ft.</label>
		<select id="jform_unitsqft" name="jform[unitsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Type</label>
		<select id="jform_bldgtype" name="jform[bldgtype]">
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>

<?php		
			break;
		
		// RESIDENTIAL PURCHASE + TOWNHOUSE/ ROW HOUSE	
		case 3:
?>

	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Unit Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Type</label>
		<select id="jform_bldgtype" name="jform[bldgtype]">
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>

<?php
			break;
				
		// RESIDENTIAL PURCHASE + LAND	
		case 4:
?>

	<div class="left ys push-top2">
		<label>Lot Size</label>
		<select id="jform_lotsize" name="jform[lotsize]">
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label>
		<select id="jform_zoned" name="jform[zoned]">
			<option value="1 Unit">1 Unit</option>
			<option value="2 Units">2 Units</option>
			<option value="3-4 Units">3-4 Units</option>
			<option value="5-20 Units">5-20 Units</option>
			<option value="20+ Units">20+ Units</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>

<?php
			break;
			
		// RESIDENTIAL LEASE + SFR
		case 5:
?>
	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="clear-float"></div>
		<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Term</label>
		<select id="jform_term" name="jform[term]">
			<option value=""></option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Possession</label>
		<select id="jform_possesion" name="jform[possesion]">
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys" style="margin-top: 24px;">
		<label>Pet</label>
		<a href="javascript: void(0)" class="left gradient-blue yes pet" rel="1">Yes</a> 
		<a href="javascript: void(0)" class="left gradient-gray no pet" rel="0">No</a> 
		<input type="hidden" value="1" id="jform_pet" name="jform[pet]" class="text-input" />
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Condition</label>
		<select id="jform_condition" name="jform[condition]">
			<option value="Fixer">Fixer</option>
			<option value="Good">Good</option>
			<option value="Excellent">Excellent</option>
			<option value="Remodeled">Remodeled</option>
			<option value="New Construction">New Construction</option>
			<option value="Under Construction">Under Construction</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
		<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
		<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Water Access">Water Access</option>
			<option value="Horse Property">Horse Property</option>
			<option value="Golf Course">Golf Course</option>
			<option value="Walkstreet">Walkstreet</option>
			<option value="Media Room">Media Room</option>
			<option value="Guest House">Guest House</option>
			<option value="Wine Cellar">Wine Cellar</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Den/Library">Den/Library</option>
			<option value="Green Const.">Green Const.</option>
			<option value="Basement">Basement</option>
			<option value="RV/Boat Parking">RV/Boat Parking</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
<?php
			break;
			
		// RESIDENTIAL LEASE + Condo
		case 6:
?>
	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Unit Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Term</label>
		<select id="jform_term" name="jform[term]">
			<option value=""></option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Possession</label>
		<select id="jform_possesion" name="jform[possesion]">
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys" style="margin-top: 24px;">
		<label>Furnished</label>
		<a href="javascript: void(0)" class="left gradient-blue yes furnished" rel="1">Yes</a> 
		<a href="javascript: void(0)" class="left gradient-gray no furnished" rel="0">No</a> 
		<input type="hidden" value="1" id="jform_furnished" name="jform[furnished]" class="text-input" />
	</div>
	<div class="left ys" style="margin-top: 24px;">
		<label>Pet</label>
		<a href="javascript: void(0)" class="left gradient-blue yes pet" rel="1">Yes</a> 
		<a href="javascript: void(0)" class="left gradient-gray no pet" rel="0">No</a> 
		<input type="hidden" value="1" id="jform_pet" name="jform[pet]" class="text-input" />
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Type</label>
		<select id="jform_bldgtype" name="jform[bldgtype]">
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// RESIDENTIAL LEASE + Townhouse/Row House
		case 7:
?>
	<div class="left ys push-top2">
		<label>Bedrooms</label>
		<select id="jform_bedroom" name="jform[bedroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bedroom) $selected = "selected";
					else $selected = "";
					
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bathroom</label>
		<select id="jform_bathroom" name="jform[bathroom]">
			<?php 
				for($i=1; $i<=5; $i++){
					if($i==$this->bathroom) $selected = "selected";
					else $selected = "";
				
					echo "<option value=\"$i\" $selected>$i</option>";
				}
			?>
			<option value="6+">6+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Unit Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Term</label>
		<select id="jform_term" name="jform[term]">
			<option value=""></option>
			<option value="Short Term">Short Term</option>
			<option value="M to M">M to M</option>
			<option value="Year Lease">Year Lease</option>
			<option value="Multi Year Lease">Multi Year Lease</option>
			<option value="Lease Option">Lease Option</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Possession</label>
		<select id="jform_possesion" name="jform[possesion]">
			<option value="Immediately">Immediately</option>
			<option value="Within 30 days">Within 30 days</option>
			<option value="Within 60 days">Within 60 days</option>
			<option value="Within 90 days">Within 90 days</option>
			<option value="Within 180 days">Within 180 days</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Type</label>
		<select id="jform_bldgtype" name="jform[bldgtype]">
			<option value="North Facing">North Facing</option>
			<option value="South Facing">South Facing</option>
			<option value="East Facing">East Facing</option>
			<option value="West Facing">West Facing</option>
			<option value="Low Rise">Low Rise</option>
			<option value="Mid Rise">Mid Rise</option>
			<option value="High Rise">High Rise</option>
			<option value="Co-Op">Co-Op</option>
		</select>
	</div>
	<div class="left ys" style="margin-top: 24px;">
		<label>Furnished</label>
		<a href="javascript: void(0)" class="left gradient-blue yes furnished" rel="1">Yes</a> 
		<a href="javascript: void(0)" class="left gradient-gray no furnished" rel="0">No</a> 
		<input type="hidden" value="1" id="jform_furnished" name="jform[furnished]" class="text-input" />
	</div>
	<div class="left ys" style="margin-top: 24px;">
		<label>Pet</label>
		<a href="javascript: void(0)" class="left gradient-blue yes pet" rel="1">Yes</a> 
		<a href="javascript: void(0)" class="left gradient-gray no pet" rel="0">No</a> 
		<input type="hidden" value="1" id="jform_pet" name="jform[pet]" class="text-input" />
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Pool/Spa</label>
		<select id="jform_poolspa" name="jform[poolspa]">
			<option value="None">None</option>
			<option value="Pool">Pool</option>
			<option value="Pool/Spa">Pool/Spa</option>
			<option value="Spa">Other</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24px;">
		<label>Garage</label>
		<select id="jform_garage" name="jform[garage]">
			<?php 
				for($i=1;$i<=7;$i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="8+">8+</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="Gym">Gym</option>
			<option value="Security">Security</option>
			<option value="Tennis Court">Tennis Court</option>
			<option value="Doorman">Doorman</option>
			<option value="Penthouse">Penthouse</option>
			<option value="One Story">One Story</option>
			<option value="Two Story">Two Story</option>
			<option value="Three Story">Three Story</option>
			<option value="Senior">Senior</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// RESIDENTIAL LEASE + Land
		case 8:
?>
	<div class="left ys push-top2">
		<label>Lot Size</label>
		<select id="jform_lotsize" name="jform[lotsize]">
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Zoned</label>
		<select id="jform_zoned" name="jform[zoned]">
			<option value="1 Unit">1 Unit</option>
			<option value="2 Units">2 Units</option>
			<option value="3-4 Units">3-4 Units</option>
			<option value="5-20 Units">5-20 Units</option>
			<option value="20+ Units">20+ Units</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value=""></option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value=""></option>
			<option value="Sidewalks">Sidewalks</option>
			<option value="Utilities">Utilities</option>
			<option value="Curbs">Curbs</option>
			<option value="Horse Trails">Horse Trails</option>
			<option value="Rural">Rural</option>
			<option value="Urban">Urban</option>
			<option value="Suburban">Suburban</option>
			<option value="Permits">Permits</option>
			<option value="HOA">HOA</option>
			<option value="Sewer">Sewer</option>
			<option value="CC&Rs">CC&Rs</option>
			<option value="Coastal">Coastal</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Multi Family
		case 9:
?>
	<div class="left ys push-top2">
		<label>Units</label>
		<select id="jform_units" name="jform[units]">
			<option value="Duplex">Duplex</option>
			<option value="TriPlex">TriPlex</option>
			<option value="Quad">Quad</option>
			<option value="5-9">5-9</option>
			<option value="10-15">10-15</option>
			<option value="16-29">16-29</option>
			<option value="30-50">30-50</option>
			<option value="50-100">50-100</option>
			<option value="101-150">101-150</option>
			<option value="151-250">151-250</option>
			<option value="251+">251+</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>GRM</label>
		<select id="jform_grm" name="jform[grm]">
			<option value="Not Disclosed">Not Disclosed</option>
			<option value="1-2">1-2</option>
			<option value="3-4">3-4</option>
			<option value="4-5">4-5</option>
			<option value="5-6">5-6</option>
			<option value="6-7">6-7</option>
			<option value="7-8">7-8</option>
			<option value="8-9">8-9</option>
			<option value="9-10">9-10</option>
			<option value="10-11">10-11</option>
			<option value="11-12">11-12</option>
			<option value="12-13">12-13</option>
			<option value="13-14">13-14</option>
			<option value="14-15">14-15</option>
			<option value="15-16">15-16</option>
			<option value="16-17">16-17</option>
			<option value="17-18">17-18</option>
			<option value="18-19">18-19</option>
			<option value="19-20">19-20</option>
			<option value="20+">20+</option>
		</select>
	</div>
	<div class="left ys push-top2" style="margin-top: 24x;">
		<label>Style</label>
		<select id="jform_style" name="jform[style]">
			<option value="American Farmhouse">American Farmhouse</option>
			<option value="Art Deco">Art Deco</option>
			<option value="Art Modern/Mid Century">Art Modern/Mid Century</option>
			<option value="Cape Cod">Cape Cod</option>
			<option value="Colonial Revival">Colonial Revival</option>
			<option value="Contemporary">Contemporary</option>
			<option value="Craftsman">Craftsman</option>
			<option value="French">French</option>
			<option value="Italian/Tuscan">Italian/Tuscan</option>
			<option value="Prairie Style">Prairie Style</option>
			<option value="Pueblo Revival">Pueblo Revival</option>
			<option value="Ranch">Ranch</option>
			<option value="Spanish/Mediterranean">Spanish/Mediterranean</option>
			<option value="Swiss Cottage">Swiss Cottage</option>
			<option value="Tudor">Tudor</option>
			<option value="Victorian">Victorian</option>
			<option value="Historic">Historic</option>
			<option value="Artichitucaul Significnat">Artichitucaul Significnat</option>
			<option value="Green">Green</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
			<option value="Land Only">Land Only</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>View</label>
		<select id="jform_view" name="jform[view]">
			<option value="None">None</option>
			<option value="Panoramic">Panoramic</option>
			<option value="City">City</option>
			<option value="Mountains/Hills">Mountains/Hills</option>
			<option value="Coastline">Coastline</option>
			<option value="Water">Water</option>
			<option value="Ocean">Ocean</option>
			<option value="Lake/River">Lake/River</option>
			<option value="Landmark">Landmark</option>
			<option value="Desert">Desert</option>
			<option value="Bay">Bay</option>
			<option value="Vineyard">Vineyard</option>
			<option value="Golf">Golf</option>
			<option value="Other">Other</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Occupancy</label>
		<select id="jform_occupancy" name="jform[occupancy]">
			<option value="Undisclosed">Undisclosed</option>
			<?php 
				for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";	
				}
			?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Rent Control">Rent Control</option>
			<option value="Senior">Senior</option>
			<option value="Assoc-Pool">Assoc-Pool</option>
			<option value="Assoc-Spa">Assoc-Spa</option>
			<option value="Assoc-Tennis">Assoc-Tennis</option>
			<option value="Assoc-Other">Assoc-Other</option>
			<option value="Section 8">Section 8</option>
			<option value="25% Occupied">25% Occupied</option>
			<option value="50% Occupied">50% Occupied</option>
			<option value="75% Occupied">75% Occupied</option>
			<option value="100% Occupied">100% Occupied</option>
			<option value="Cash Cow">Cash Cow</option>
			<option value="Value Add">Senior</option>
			<option value="Senior">Value Add</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Rent Control">Rent Control</option>
			<option value="Senior">Senior</option>
			<option value="Assoc-Pool">Assoc-Pool</option>
			<option value="Assoc-Spa">Assoc-Spa</option>
			<option value="Assoc-Tennis">Assoc-Tennis</option>
			<option value="Assoc-Other">Assoc-Other</option>
			<option value="Section 8">Section 8</option>
			<option value="25% Occupied">25% Occupied</option>
			<option value="50% Occupied">50% Occupied</option>
			<option value="75% Occupied">75% Occupied</option>
			<option value="100% Occupied">100% Occupied</option>
			<option value="Cash Cow">Cash Cow</option>
			<option value="Value Add">Senior</option>
			<option value="Senior">Value Add</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Office
		case 10:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Office">Office</option>
			<option value="Institutional">Institutional</option>
			<option value="Medical">Medical</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Condo">Condo</option>
			<option value="R&D">R&D</option>
			<option value="Business Park">Business Park</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Class</label>
		<select id="jform_class" name="jform[class]">
			<option value="A">A</option>
			<option value="B">B</option>
			<option value="C">C</option>
			<option value="D">D</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->	
	<div class="left ys push-top2">
		<label>Occupancy</label>
		<select id="jform_occupancy" name="jform[occupancy]">
			<option value="Undisclosed">Undisclosed</option>
			<?php 
				for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";	
				}
			?>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Industrial
		case 11:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Flex Space">Flex Space</option>
			<option value="Business Park">Business Park</option>
			<option value="Condo">Condo</option>
			<option value="Land">Land</option>
			<option value="Manufacturing">Manufacturing</option>
			<option value="Office Showroom">Office Showroom</option>
			<option value="R&D">R&D</option>
			<option value="Self/Mini Storage">Self/Mini Storage</option>
			<option value="Truck Terminal/Hub">Truck Terminal/Hub</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Distribution">Distribution</option>
			<option value="Cold Storage">Cold Storage</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Ceiling Height</label>
		<select id="jform_ceiling" name="jform[ceiling]">
			<?php 
				for($i=12; $i<=34; $i=$i+2){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Occupancy</label>
		<select id="jform_occupancy" name="jform[occupancy]">
			<option value="Undisclosed">Undisclosed</option>
			<?php 
				for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";	
				}
			?>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Retail
		case 12:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Community Center">Community Center</option>
			<option value="Strip Center">Strip Center</option>
			<option value="Outlet Center">Outlet Center</option>
			<option value="Power Center">Power Center</option>
			<option value="Anchor">Anchor</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Service Station">Service Station</option>
			<option value="Retail Pad">Retail Pad</option>
			<option value="Free Standing">Free Standing</option>
			<option value="Day Care/Nursery">Day Care/Nursery</option>
			<option value="Post Office">Post Office</option>
			<option value="Vehicle">Vehicle</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Occupancy</label>
		<select id="jform_occupancy" name="jform[occupancy]">
			<option value="Undisclosed">Undisclosed</option>
			<?php 
				for($i=100; $i>=0; $i=$i-5){
			echo "<option value=\"$i\">$i</option>";	
				}
			?>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Motel/Hotel
		case 13:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Economy">Economy</option>
			<option value="Full Service">Full Service</option>
			<option value="Land">Land</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label>
		<select id="jform_roomcount" name="jform[roomcount]">
			<option value="1-9">1-9</option>
			<option value="10-19">10-19</option>
			<option value="20-29">20-29</option>
			<option value="30-39">30-39</option>
			<option value="40-49">40-49</option>
			<option value="50-99">50-99</option>
			<option value="100-149">100-149</option>
			<option value="150-199">150-199</option>
			<option value="200+">200+</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Restaurant">Restaurant</option>
			<option value="Bar">Bar</option>
			<option value="Pool">Pool</option>
			<option value="Banquet Room">Banquet Room</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Restaurant">Restaurant</option>
			<option value="Bar">Bar</option>
			<option value="Pool">Pool</option>
			<option value="Banquet Room">Banquet Room</option>
			<option value="Seller Carry">Seller Carry</option>
		</select>
	</div>	
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Assisted Care
		case 14:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Assisted">Assisted</option>
			<option value="Acute Care">Acute Care</option>
			<option value="Land">Land</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Room Count</label>
		<select id="jform_roomcount" name="jform[roomcount]">
			<option value="1-9">1-9</option>
			<option value="10-19">10-19</option>
			<option value="20-29">20-29</option>
			<option value="30-39">30-39</option>
			<option value="40-49">40-49</option>
			<option value="50-99">50-99</option>
			<option value="100-149">100-149</option>
			<option value="150-199">150-199</option>
			<option value="200+">200+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL + Special Purpose
		case 15:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Golf">Golf</option>
			<option value="Marina">Marina</option>
			<option value="Theater">Theater</option>
			<option value="Religious">Religious</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Cap Rate</label>
		<select id="jform_cap" name="jform[cap]">
			<option value="0-.9">0-.9</option>
			<option value="1-1.9">1-1.9</option>
			<option value="2-2.9">2-2.9</option>
			<option value="3-3.9">3-3.9</option>
			<option value="4-4.9">4-4.9</option>
			<option value="5-5.9">5-5.9</option>
			<option value="6-6.9">6-6.9</option>
			<option value="7-7.9">7-7.9</option>
			<option value="8-8.9">8-8.9</option>
			<option value="9-9.9">9-9.9</option>
			<option value="10-10.9">10-10.9</option>
			<option value="11-11.9">11-11.9</option>
			<option value="12-12.9">12-12.9</option>
			<option value="13-13.9">13-13.9</option>
			<option value="14-14.9">14-14.9</option>
			<option value="15+">15+</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL LEASE + OFFICE
		case 16:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Office">Office</option>
			<option value="Institutional">Institutional</option>
			<option value="Medical">Medical</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Condo">Condo</option>
			<option value="R&D">R&D</option>
			<option value="Business Park">Business Park</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Class</label>
		<select id="jform_class" name="jform[class]">
			<option value="A">A</option>
			<option value="B">B</option>
			<option value="C">C</option>
			<option value="D">D</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label>
		<select id="jform_typelease" name="jform[typelease]">
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Available Sq.Ft.</label>
		<select id="jform_available" name="jform[available]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL LEASE + INDUSTRIAL
		case 17:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Flex Space">Flex Space</option>
			<option value="Business Park">Business Park</option>
			<option value="Condo">Condo</option>
			<option value="Manufacturing">Manufacturing</option>
			<option value="Office Showroom">Office Showroom</option>
			<option value="R&D">R&D</option>
			<option value="Truck Terminal/Hub">Truck Terminal/Hub</option>
			<option value="Warehouse">Warehouse</option>
			<option value="Distribution">Distribution</option>
			<option value="Cold Storage">Cold Storage</option>
			<option value="Land">Land</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label>
		<select id="jform_typelease" name="jform[typelease]">
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Available Sq.Ft.</label>
		<select id="jform_available" name="jform[available]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Ceiling Height</label>
		<select id="jform_ceiling" name="jform[ceiling]">
			<?php 
				for($i=12; $i<=34; $i=$i+2){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="36+">36+</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
			
		// COMMERCIAL LEASE + RETAIL
		case 18:
?>
	<div class="left ys push-top2">
		<label>Type</label>
		<select id="jform_type" name="jform[type]">
			<option value="Community Center">Community Center</option>
			<option value="Strip Center">Strip Center</option>
			<option value="Outlet Center">Outlet Center</option>
			<option value="Power Center">Power Center</option>
			<option value="Anchor">Anchor</option>
			<option value="Restaurant">Restaurant</option>
			<option value="Service Station">Service Station</option>
			<option value="Retail Pad">Retail Pad</option>
			<option value="Free Standing">Free Standing</option>
			<option value="Day Care/Nursery">Day Care/Nursery</option>
			<option value="Post Office ">Post Office </option>
			<option value="Vehicle">Vehicle</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label>
		<select id="jform_typelease" name="jform[typelease]">
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Type Lease</label>
		<select id="jform_typelease2" name="jform[typelease2]">
			<option value="NNN">NNN</option>
			<option value="FSG">FSG</option>
			<option value="MG">MG</option>
			<option value="Modified Net">Modified Net</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Bldg. Sq. Ft.</label>
		<select id="jform_bldgsqft" name="jform[bldgsqft]">
			<option value="1-499">1-499</option>
			<option value="500-999">500-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-2,499">2,000-2,499</option>
			<option value="2,500,2,999">2,500,2,999</option>
			<option value="3,000-3,999">3,000-3,999</option>
			<option value="4,000-4,999">4,000-4,999</option>
			<option value="7,500 -10,000">7,500 -10,000</option>
			<option value="10,001-19,999">10,001-19,999</option>
			<option value="20,000+">20,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Stories</label>
		<select id="jform_stories" name="jform[stories]">
			<?php 
				for($i=1; $i<=5; $i++){
					echo "<option value=\"$i\">$i</option>";
				}
			?>
			<option value="6+">6+</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Lot Sq.Ft.</label>
		<select id="jform_lotsqft" name="jform[lotsqft]">
			<option value="">-</option>
			<option value="0-999">0-999</option>
			<option value="1,000-1,999">1,000-1,999</option>
			<option value="2,000-4,999">2,000-4,999</option>
			<option value="5,000-9,999">5,000-9,999</option>
			<option value="10,000-19,999">10,000-19,999</option>
			<option value="20,000-29,999">20,000-29,999</option>
			<option value="30,000-49,999">30,000-49,999</option>
			<option value="50,000-74,999">50,000-74,999</option>
			<option value="75,000-149,999">75,000-149,999</option>
			<option value="150,000+">150,000+</option>
			<option value="Undisclosed">Undisclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Year Built</label>
		<select id="jform_yearbuilt" name="jform[yearbuilt]">
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009-2000">2009-2000</option>
			<option value="1999-1990">1999-1990</option>
			<option value="1989-1980">1989-1980</option>
			<option value="1979-1970">1979-1970</option>
			<option value="1969-1960">1969-1960</option>
			<option value="1959-1950">1959-1950</option>
			<option value="1949-1940">1949-1940</option>
			<option value="1939-1930">1939-1930</option>
			<option value="1929-1920">1929-1920</option>
			<option value="&lt; 1919">&lt; 1919</option>
			<option value="Not Disclosed">Not Disclosed</option>
		</select>
	</div>
	<div class="left ys push-top2">
		<label>Parking Ratio</label>
		<select id="jform_parking" name="jform[parking]">
			<option value="1/1000">1/1000</option>
			<option value="1.5/1000">1.5/1000</option>
			<option value="2/1000">2/1000</option>
			<option value="2.5/1000">2.5/1000</option>
			<option value="3/1000">3/1000</option>
			<option value="3.5/1000">3.5/1000</option>
			<option value="4/1000">4/1000</option>
			<option value="4.5/1000">4.5/1000</option>
			<option value="5/1000">5/1000</option>
			<option value="other">other</option>
		</select>
	</div>
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features1" name="jform[features1]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="left ys push-top2">
		<label>Features</label>
		<select id="jform_features2" name="jform[features2]">
			<option value="Mixed use">Mixed use</option>
			<option value="Single Tenant">Single Tenant</option>
			<option value="Multiple Tenant">Multiple Tenant</option>
			<option value="Seller Carry">Seller Carry</option>
			<option value="Net-Leased">Net-Leased</option>
			<option value="Owner User">Owner User</option>
			<option value="Vacant">Vacant</option>
		</select>
	</div>	
	<div class="clear-float"></div>
	<!----------------------------------------------------------------->
<?php
			break;
	}

?>
</div>